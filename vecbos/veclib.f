*
      FUNCTION FXN(X,WGT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION X(16)
      COMMON /BVEG1/  XL(16),XU(16),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON /EFFIC/  NIN,NOUT
      COMMON /SUBPRO/ SFQ2,SFQ4,SFQ6
      COMMON /PROCES/ ISUB(3),ISTAT(3)
      common /imppar/ alpha,etmaxi,is,iyes
      SAVE /SUBPRO/,/BVEG1/
*
      if (iyes.eq.1) then
         call impsam(x,wt,wgt,idump)
      else
         CALL EVENT(X,WT,WGT,IDUMP)
      endif
*
      IF(IDUMP.EQ.0) THEN
         FXN=0.D0
         NOUT=NOUT+1
         RETURN
      ENDIF
*
      FXN=WT/WGT
*
      NIN=NIN+1
      CALL BIN(WT/ITMX,1)
      IF (ISUB(1).EQ.1) CALL BIN(SFQ2/ITMX,41)
      IF (ISUB(2).EQ.1) CALL BIN(SFQ4/ITMX,81)
      IF (ISUB(3).EQ.1) CALL BIN(SFQ6/ITMX,121)
*
      END
*
************************************************************************
* EVENT generates an event with a vector boson decaying into leptons,  *
* calculates the various weights and the matrix elements in necessary. *
* Remarks: Mappings to increase the efficiency of vegas and breit      *
* wigner distributions for the vector boson to assist rambo.           *
************************************************************************
* The event is constructed as follows:                                 *
*      PLAB convention: 1 + 2 incoming partons                         *
*                       3...2+NJET outgoing partons                    *
*                       8 Outgoing Vector boson                        *
*                       9 lepton, 10 antilepton                        *
************************************************************************
      SUBROUTINE EVENT(Y,WEIGHT,WGT,IPASS)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 Y(16),AM(100),PSYS(4),PS(4,100)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /CUTVAL/ PTMINJ,PTMINL,PTMIS,ETAMAJ,ETAMAL,
     .                DELRJJ,DELRJL,PTVCUT
      COMMON /BINVAR/ PT(10),ETA(10),DR(10,10),PTEL,PTNU,ETAEL1,ETAEL2,
     .                ANGLP,ANGNP,PTVEC,PZVEC
      SAVE INIT,GEVNB,/BINVAR/,/MOM/
      DATA INIT,GEVNB/1,389385.73D0/
      IF (INIT.EQ.1) THEN
        INIT=0
        TWOPI=(2.D0*3.1415926)**(4.D0-3.D0*(NJET+2.D0))
        DO 5 I=1,100
          AM(I)=0.D0
    5   CONTINUE
        RNWID=10.D0
        RMV=RMW
        IF (IVEC.EQ.2) RMV=RMZ
        RGV=RGW
        IF (IVEC.EQ.2) RGV=RGZ
        X1=(RMV+RNWID*RGV)**2 - RMV**2
        X0=(RMV-RNWID*RGV)**2 - RMV**2
        A=RMV*RGV
        Y1=DATAN(X1/A)/A
        Y0=DATAN(X0/A)/A
        WTY=Y1-Y0
      END IF
*
      WEIGHT=0D0
* Generate a Breit Wigner Vector boson mass
      YW=WTY*RN(1)+Y0
      XX=A*DTAN(A*YW)
      RMSQ=XX+RMV**2
      RM=DSQRT(RMSQ)
      WTBW=WTY*(XX**2+A**2)
* Minimum Final state mass (production of W and jets)
      EPS=((RM+FLOAT(NJET)*PTMINJ)/W)**2
      IF (NJET.EQ.0) THEN
        X1=EPS**Y(1)
        X2=EPS/X1
        WT12=-LOG(EPS)
      ELSE
        X1=EPS**(Y(2)*Y(1))
        X2=EPS**(Y(2)*(1.D0-Y(1)))
        WT12=Y(2)*EPS**Y(2)*LOG(EPS)**2
      ENDIF
      WHAT=SQRT(X1*X2*W**2)
* Assign Momenta: 1 and 2 are incoming
      PLAB(1,1)=0.D0
      PLAB(2,1)=0.D0
      PLAB(3,1)=.5*W*X1
      PLAB(4,1)=.5*W*X1
      PLAB(1,2)=0.D0
      PLAB(2,2)=0.D0
      PLAB(3,2)=-.5*W*X2
      PLAB(4,2)=.5*W*X2
* Generate the vector boson and jets.
      DO 20 I=1,4
        PSYS(I)=PLAB(I,1)+PLAB(I,2)
  20  CONTINUE
* invariant mass
      IF (NJET.EQ.0) THEN
        DO 32 I=1,4
          PLAB(I,8)=PSYS(I)
   32   CONTINUE
        WTWJ=1.D0/W**2
      ELSE
        AM(1)=RM
        CALL RAMBOO(NJET+1,WHAT,PSYS,AM,PS,WTWJ)
        DO 33 I=1,4
          PLAB(I,8)=PS(I,1)
   33   CONTINUE
        DO 35 I=1,4
          DO 35 K=1,NJET
            PLAB(I,2+K)=PS(I,K+1)
   35   CONTINUE
      ENDIF
* generate vector boson decay products.
      DO 40 J=1,4
        PSYS(J)=PLAB(J,8)
   40 CONTINUE
      AM(1)=0.D0
      CALL RAMBOO(2,RM,PSYS,AM,PS,WTENU)
      DO 45 I=1,4
        DO 45 K=1,2
          PLAB(I,K+8)=PS(I,K)
   45 CONTINUE
*
      CALL CUT(IPASS)
      IF (IPASS.EQ.0) RETURN
* Calculate the dynamical scale
      ETOT=0.D0
      ETMAX=0.D0
      DO 60 J=1,NJET
        IF(PT(J).GT.ETMAX) ETMAX = PT(J)
        ETOT=ETOT+PT(J)
   60 CONTINUE
      IF (IQCDDS.EQ.1) Q=ETOT/FLOAT(NJET)
      IF (IQCDDS.EQ.2) Q=WHAT
      IF (IQCDDS.EQ.3) Q=AVEMIJ(PLAB,NJET)
      IF (IQCDDS.EQ.4) Q=RMV
      IF (IQCDDS.EQ.5) Q=ETMAX
* Flux factor
      FLUX=.5D0/(X1*X2*W*W)
* Combine weights
      WTMC=WTBW*WT12*WTWJ*WTENU*TWOPI*GEVNB*FLUX*WGT
*
      WEIGHT=WZMAT(WTMC)
      END
*
************************************************************************
* WZMAT determines a matrix element squared of the event in PLAB(4,10) *
* Input:  PLAB(4,10) : 1 proton particle. 2 (anti) proton parton.      *
*                      3...2+N outpgoing partons                       *
*                      8 vector boson in lab frame                     *
*                      9 lepton, 10 anti-lepton                        *
*         N          : Number of QCD partons in the final state.       *
*         RMV,RMG    : Mass and width of the vector boson.             *
*         WEIGHT     : Monte Carlo weight (Phase space etc.. )         *
* Remark: Comments above not yet valid!!! (Intention: Main entry)      *
************************************************************************
      FUNCTION WZMAT(WEIGHT)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      IF (IVEC.EQ.1) WZMAT=SFMEW(WEIGHT)
      IF (IVEC.EQ.2) WZMAT=SFMEZ(WEIGHT)
      END
*
      FUNCTION AVEMIJ(PLAB,NJET)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION PLAB(4,*)
*
      ICOUNT=0
      RMTOT=0D0
      DO 10 I=3,2+NJET
        DO 10 J=I+1,2+NJET
          RMIJ=(PLAB(4,I)+PLAB(4,J))**2
     .        -(PLAB(3,I)+PLAB(3,J))**2
     .        -(PLAB(2,I)+PLAB(2,J))**2
     .        -(PLAB(1,I)+PLAB(1,J))**2
          RMTOT=RMTOT+SQRT(RMIJ)
          ICOUNT=ICOUNT+1
   10 CONTINUE
*
      AVEMIJ=RMTOT/ICOUNT
*
      END
*
      SUBROUTINE RAMBOO(N,ET,PSYS,AM,PS,WT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION PSYS(4),AM(100),PS(4,100),PL(4),PC(4),P(4,100)
      CALL RAMBO(N,ET,AM,P,WT)
      DO 10 I=1,N
        DO 20 J=1,4
          PC(J)=P(J,I)
   20   CONTINUE
        CALL BOOST(ET,PSYS,PC,PL)
        DO 30 J=1,4
          PS(J,I)=PL(J)
   30   CONTINUE
   10 CONTINUE
      END
*
      SUBROUTINE BOOST(Q,PBOO,PCM,PLB)
      REAL*8 PBOO(4),PCM(4),PLB(4),Q,FACT
         PLB(4)=(PBOO(4)*PCM(4)+PBOO(3)*PCM(3)
     &             +PBOO(2)*PCM(2)+PBOO(1)*PCM(1))/Q
         FACT=(PLB(4)+PCM(4))/(Q+PBOO(4))
         DO 10 J=1,3
  10     PLB(J)=PCM(J)+FACT*PBOO(J)
      RETURN
      END
*
      FUNCTION R(I,J)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /MOM/ PLAB(4,10),WHAT
      SAVE /MOM/
      R1= (PLAB(4,I)+PLAB(3,I))*(PLAB(4,J)-PLAB(3,J))/
     .   ((PLAB(4,J)+PLAB(3,J))*(PLAB(4,I)-PLAB(3,I)))
      DELY=.5*LOG(R1)
      R2= (PLAB(1,I)*PLAB(1,J)+PLAB(2,I)*PLAB(2,J))
     .   /SQRT((PLAB(1,I)**2+PLAB(2,I)**2)*(PLAB(1,J)**2+PLAB(2,J)**2))
      IF(R2.LT.-0.999999999D0) R2=-1D0
      DELFI=ACOS(R2)
      R=SQRT(DELY**2+DELFI**2)
      RETURN
      END
*
************************************************************************
* SUBROUTINE CUT(IPASS) applies the simple phase space cuts to the     *
* event stored in PLAB. The cut values are stored in the common CUTVAL *
* Convention PLAB: 1,2    p + p(bar) partons.                          *
*                  3..?   outgoing partons                             *
*                  8      vector boson                                 *
*                  9,10   lepton,anti-lepton                           *
************************************************************************
      SUBROUTINE CUT(IPASS)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /CUTVAL/ PTMINJ,PTMINL,PTMIS,ETAMAJ,ETAMAL,
     .                DELRJJ,DELRJL,PTVCUT
      COMMON /BINVAR/ PT(10),ETA(10),DR(10,10),PTEL,PTNU,ETAEL1,ETAEL2,
     .                ANGLP,ANGNP,PTVEC,PZVEC
      common /sort/ jp(10)
      SAVE /MOM/,/BINVAR/
*
      IPASS=0
* Check PT of vector boson
      PTVEC=SQRT(PLAB(1,8)**2+PLAB(2,8)**2)
      IF (PTVEC.LT.PTVCUT) RETURN
* check minimun PT of leptons.
      PTEL=DSQRT(PLAB(1,9)**2+PLAB(2,9)**2)
      IF (PTEL.LT.PTMINL) RETURN
      PTNU=SQRT(PLAB(1,10)**2+PLAB(2,10)**2)
      IF (PTNU.LT.PTMIS) RETURN
* check rapidity of leptons
      ETAEL1=.5*LOG((PLAB(4,9)+PLAB(3,9))
     .             /(PLAB(4,9)-PLAB(3,9)))
      IF (ABS(ETAEL1).GT.ETAMAL) RETURN
* if vecbos=Z then test positron too
      IF (IVEC.EQ.2) THEN
        ETAEL2=.5*LOG((PLAB(4,10)+PLAB(3,10))
     .               /(PLAB(4,10)-PLAB(3,10)))
        IF (ABS(ETAEL2).GT.ETAMAL) RETURN
      END IF
* Jet cuts
      DO 10 I=1,NJET
* Jet PT's
        PT(I)=SQRT(PLAB(1,2+I)**2+PLAB(2,2+I)**2)
        IF (PT(I).LT.PTMINJ) RETURN
* Jet rapidity
        ETA(I)=.5*LOG((PLAB(4,2+I)+PLAB(3,2+I))
     .               /(PLAB(4,2+I)-PLAB(3,2+I)))
        IF (ABS(ETA(I)).GT.ETAMAJ) RETURN
* Lepton - Jet separation
        IF (R(9,2+I).LT.DELRJL) RETURN
        IF ((IVEC.EQ.2).AND.(R(10,2+I).LT.DELRJL)) RETURN
* Jet - Jet separation
        DO 10 J=I+1,NJET
          DR(I,J)=R(I+2,J+2)
          IF (DR(I,J).LT.DELRJJ) RETURN
          DR(J,I)=DR(I,J)
  10  CONTINUE
* event is accepted.
      IPASS=1
* determine additional histo variables
* angle between proton and charged lepton(W) or lepton(Z)
      ANGLP=PLAB(3,1)*PLAB(3,9)/PLAB(4,1)/PLAB(4,9)
* angle between proton and neutrino(W) or anti-lepton(Z)
      ANGNP=PLAB(3,1)*PLAB(3,10)/PLAB(4,1)/PLAB(4,10)
* Pz of vector boson
      PZVEC=PLAB(3,8)
* sort jets according to most energetic.
      do 20 i=1,njet
         jp(i)=i
 20   continue
      do 30 i=1,njet-1
         do 30 j=i+1,njet
            if (pt(jp(i)).lt.pt(jp(j))) then
               jt   =jp(i)
               jp(i)=jp(j)
               jp(j)=jt
            endif
 30   continue
      END
*
      SUBROUTINE CLRSTS
      IMPLICIT REAL*8 (A-H,O-Z)
      COMMON /QSTAT/  TQ(2,9,6)
      SAVE /QSTAT/
*
      DO 10 I=1,6
        DO 10 J=1,9
          TQ(1,J,I)=0D0
          TQ(2,J,I)=0D0
   10 CONTINUE
*
      END
*
************************************************************************
* PARDEN calculates the parton density functions.                      *
* F are particle densities of the proton, G for the (anti)-proton.     *
* Combinations:  x(1) = UP                                             *
*                x(2) = DOWN                                           *
*                x(3) = STRANGE                                        *
*                x(4) = CHARM                                          *
*                x(5) = BOTTOM                                         *
*                x(6) = ANTI-UP                                        *
*                x(7) = ANTI-DOWN                                      *
*                x(8) = ANTI-STRANGE                                   *
*                x(9) = ANTI-CHARM                                     *
*               x(10) = ANTI-BOTTOM                                    *
*               x(11) = GLUON                                          *
* When IPPBAR=0(1), collider is proton-(anti)proton type               *
************************************************************************
      SUBROUTINE PARDEN(IPPBAR)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      SAVE /STRUCF/
*
      DO 10 I=1,6
        IF (ITRY(I).NE.0) THEN
          I1=ITRY(I)
          CALL STRUCT(X1,Q,I1,U1,D1,SEAU1,SEAD1,STR1,CHM1,bot1,G1)
          CALL STRUCT(X2,Q,I1,U2,D2,SEAU2,SEAD2,STR2,CHM2,bot2,G2)
          F(1,I)=U1+SEAU1
          F(2,I)=D1+SEAD1
          F(3,I)=STR1
          F(4,I)=CHM1
          F(5,I)=bot1
          F(6,I)=SEAU1
          F(7,I)=SEAD1
          F(8,I)=STR1
          F(9,I)=CHM1
          F(10,I)=bot1
          F(11,I)=G1
          G(1,I)=SEAU2
          IF (IPPBAR.EQ.0) G(1,I)=U2+SEAU2
          G(2,I)=SEAD2
          IF (IPPBAR.EQ.0) G(2,I)=D2+SEAD2
          G(3,I)=STR2
          G(4,I)=CHM2
          G(5,I)=bot2
          G(6,I)=U2+SEAU2
          IF (IPPBAR.EQ.0) G(6,I)=SEAU2
          G(7,I)=D2+SEAD2
          IF (IPPBAR.EQ.0) G(7,I)=SEAD2
          G(8,I)=STR2
          G(9,I)=CHM2
          G(10,I)=bot2
          G(11,I)=G2
        END IF
   10 CONTINUE
*
      END
*
************************************************************************
* REAL*8 FUNCTION SFMEW(WTMC) calls the various matrix elements for    *
* the event in PLAB. It returns the common /SUBPRO/ and the Monte      *
* Carlo weight as the function value.                                  *
************************************************************************
      FUNCTION SFMEW(WTMC)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /BVEG1/  XL(16),XU(16),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON /PROCES/ ISUB(3),ISTAT(3)
      COMMON /SUBPRO/ SFQ2,SFQ4,SFQ6
      COMMON /QSTAT/  TQ(2,9,6)
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      DIMENSION TQL(2,9,6)
      SAVE FORPI,/MOM/,/BVEG1/,/SUBPRO/,/QSTAT/,/STRUCF/
      DATA FORPI /1.2566371D1/
*
      CALL PARDEN(IPPBAR)
* Weak coupling, spin average, strong coupling, W-prop**2
      PROP2=((PLAB(4,8)**2-PLAB(1,8)**2-PLAB(2,8)**2-PLAB(3,8)**2)
     .         -RMW**2)**2 + RMW**2*RGW**2
      DO 10 I=1,6
        ALPHA=1.5*FORPI/((33.-2.*NF)*LOG(Q/QCDSCA(I)))
        ALPHAS=(FORPI*ALPHA)**NJET/X1/X2/4D0
        FACTOR(I)=GW**4*ALPHAS/PROP2
        IF (IMANNR.GT.1) FACTOR(I)=1/X1/X2
   10 CONTINUE
*
      SFQ2=0D0
      SFQ4=0D0
      SFQ6=0D0
*
      IF (ISUB(1).EQ.1) THEN
        do i=1,9
           do j=1,6
              tql(1,i,j)=0d0
              tql(2,i,j)=0d0
           enddo
        enddo
        CALL MEQ2W(TOT,TQL)
        DO 20 I=1,6
          IF (ITRY(I).NE.0) THEN
            DO 11 J=1,9
              TQ(1,J,I)=TQ(1,J,I)+WTMC*FACTOR(I)*TQL(1,J,I)/ITMX
              TQ(2,J,I)=TQ(2,J,I)+WTMC*FACTOR(I)*TQL(2,J,I)/ITMX
   11       CONTINUE
          END IF
   20   CONTINUE
        SFQ2=WTMC*TOT*FACTOR(IUSE)
      END IF
*
      IF (ISUB(2).EQ.1) THEN
        do i=1,9
           do j=1,6
              tql(1,i,j)=0d0
              tql(2,i,j)=0d0
           enddo
        enddo
        CALL MEQ4W(TOT,TQL)
        DO 40 I=1,6
          IF (ITRY(I).NE.0) THEN
            DO 31 J=1,9
              TQ(1,J,I)=TQ(1,J,I)+WTMC*FACTOR(I)*TQL(1,J,I)/ITMX
              TQ(2,J,I)=TQ(2,J,I)+WTMC*FACTOR(I)*TQL(2,J,I)/ITMX
   31       CONTINUE
          END IF
   40   CONTINUE
        SFQ4=WTMC*TOT*FACTOR(IUSE)
      END IF
*
      IF (ISUB(3).EQ.1) THEN
        do i=1,9
           do j=1,6
              tql(1,i,j)=0d0
              tql(2,i,j)=0d0
           enddo
        enddo
        CALL MEQ6W(TOT,TQL)
        DO 50 I=1,6
          IF (ITRY(I).NE.0) THEN
            DO 41 J=1,9
              TQ(1,J,I)=TQ(1,J,I)+WTMC*FACTOR(I)*TQL(1,J,I)/ITMX
              TQ(2,J,I)=TQ(2,J,I)+WTMC*FACTOR(I)*TQL(2,J,I)/ITMX
   41       CONTINUE
          END IF
   50   CONTINUE
        SFQ6=WTMC*TOT*FACTOR(IUSE)
      END IF
*
      SFMEW=SFQ2+SFQ4+SFQ6
*
      END
*
************************************************************************
* REAL*8 FUNCTION SFMEZ(WTMC) calls the various matrix elements for    *
* the event in PLAB. It returns the common /SUBPRO/ and the Monte      *
* Carlo weight as the function value.                                  *
************************************************************************
      FUNCTION SFMEZ(WTMC)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /BVEG1/  XL(16),XU(16),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON /PROCES/ ISUB(3),ISTAT(3)
      COMMON /SUBPRO/ SFQ2,SFQ4,SFQ6
      COMMON /QSTAT/  TQ(2,9,6)
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      DIMENSION TQL(2,9,6)
      SAVE FORPI,/MOM/,/BVEG1/,/SUBPRO/,/QSTAT/,/STRUCF/
      DATA FORPI /1.2566371D1/
*
      CALL PARDEN(IPPBAR)
* Weak coupling, spin average, strong coupling, Z-prop**2
      PROP2=((PLAB(4,8)**2-PLAB(1,8)**2-PLAB(2,8)**2-PLAB(3,8)**2)
     .         -RMZ**2)**2 + RMZ**2*RGZ**2
      DO 10 I=1,5
        ALPHA=1.5*FORPI/((33.-2.*NF)*LOG(Q/QCDSCA(I)))
        ALPHAS=(FORPI*ALPHA)**NJET/X1/X2/4D0
        FACTOR(I)=GZ**4*ALPHAS/PROP2
   10 CONTINUE
*
      SFQ2=0D0
      SFQ4=0D0
      SFQ6=0D0
*
      IF (ISUB(1).EQ.1) THEN
        CALL MEQ2Z(TOT,TQL)
        DO 20 I=1,6
          IF (ITRY(I).NE.0) THEN
            DO 11 J=5,9
              TQ(1,J,I)=TQ(1,J,I)+WTMC*FACTOR(I)*TQL(1,J,I)/ITMX
   11       CONTINUE
          END IF
   20   CONTINUE
        SFQ2=WTMC*TOT*FACTOR(IUSE)
      END IF
*
      IF (ISUB(2).EQ.1) THEN
        CALL MEQ4Z(TOT,TQL)
        DO 40 I=1,6
          IF (ITRY(I).NE.0) THEN
            DO 31 J=5,9
              TQ(1,J,I)=TQ(1,J,I)+WTMC*FACTOR(I)*TQL(1,J,I)/ITMX
   31       CONTINUE
          END IF
   40   CONTINUE
        SFQ4=WTMC*TOT*FACTOR(IUSE)
      END IF
*
      SFMEZ=SFQ2+SFQ4+SFQ6
*
      END
*
************************************************************************
* STAT(ISTAT) reports Monte Carlo output in separate file VECBOS.OUT   *
*                                                                      *
* This includes:                                                       *
*  - Input parameters for integration, results of integration.         *
*  - Table of the relevant subprocesses for 6 parton densities.        *
*  - Distributions.                                                    *
*  - ISTAT determines whether to print histograms for 2,4,6 quarks.    *
*  - The relative contribution of each process.                        *
*                                                                      *
************************************************************************
      SUBROUTINE STAT(ISUB,ISTAT)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION ISTAT(3),ISUB(3)
      CHARACTER*20 SQWP(2),SQWM(2),MANNR(3),QCDSC(5),FL(5)
      character*20 sstru
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /CUTVAL/ PTMINJ,PTMINL,PTMIS,ETAMAJ,ETAMAL,
     .                DELRJJ,DELRJL,PTVCUT
      COMMON /QSTAT/  TQ(2,9,6)
      COMMON /EFFIC/  NIN,NOUT
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /INTER/  ITMX1,NCALL1,IMANN1,ITMX2,NCALL2,IMANN2
      COMMON /MCRES/  AVGI1,SD1,CHI2A1,AVGI2,SD2,CHI2A2
      SAVE /QSTAT/,SQWP,SQWM,FL,MANNR,QCDSC
      DATA SQWP/' U DB -> W+ ',' C SB -> W+ '/
      DATA SQWM/' UB D -> W- ',' CB S -> W- '/
      DATA FL/
     . ' TOTAL GG        ',' TOTAL GQ + GQB  ',' TOTAL QG + QBG  ',
     . ' TOTAL QQ + QBQB ',' TOTAL QQB       '/
      DATA MANNR/ ' EXACT ',' M^2=1 ',' SPHEL '/
      DATA QCDSC/ ' PT-AVERAGE ',' CM-ENERGY ',
     .            ' Mij-AVERAGE ',' BOSON MASS ',' PT-max '/
*
* Heading: Author + addresses
*
  311 FORMAT(A)
      WRITE(*,311)
     .' ==============================================================='
      WRITE(*,311)
     .' VECBOS is a Monte Carlo for W,Z + JETS written by H. Kuijf.'
      WRITE(*,311)
     .  ' Current Version of VECBOS: 3.0, dated 03-16-93. '
      WRITE(*,311)
     .  ' E-MAIL address: giele@fnth11.fnal.gov '
      WRITE(*,311)
     .' ==============================================================='
      WRITE(*,*)
*
* General information
*
      WRITE(*,311) ' General Monte Carlo parameters.'
      WRITE(*,311) ' ==============================='
      IF (IVEC.EQ.1) WRITE(*,71) RMW,RGW
   71   FORMAT(' Vector boson is a W with mass ',1D11.4,' and width ',
     .           1D11.4)
      IF (IVEC.EQ.2) WRITE(*,72) RMZ,RGZ
   72   FORMAT(' Vector boson is a Z with mass ',1D11.4,' and width ',
     .           1D11.4)
      WRITE(*,91) INT(W)
   91 FORMAT(' Hadronic CM energy:          ',1I5,' GeV.')
      WRITE(*,92) NF
   92 FORMAT(' # flavours in Alpha:         ',1I4)
      WRITE(*,93) NJET
   93 FORMAT(' # final state partons:       ',1I4)
      WRITE(*,94) PTMINJ
   94 FORMAT(' Minimum transverse momentum of the jets  : ',1D11.4)
      WRITE(*,84) PTMINL
   84 FORMAT(' Minimum transverse momentum of the lepton: ',1D11.4)
      IF (IVEC.EQ.1) WRITE(*,77) PTMIS
   77 FORMAT(' Minimum missing transverse momentum: ',1D11.4)
      WRITE(*,74) PTVCUT
   74 FORMAT(' Minimum transverse momentum of the IVB   : ',1D11.4)
      WRITE(*,95) ETAMAJ
   95 FORMAT(' |Rapidity(j)| < ',1D11.4)
      WRITE(*,85) ETAMAL
   85 FORMAT(' |Rapidity(l)| < ',1D11.4)
      WRITE(*,96) DELRJJ
   96 FORMAT(' Delta R(j,j)  > ',1D11.4)
      WRITE(*,86) DELRJL
   86 FORMAT(' Delta R(j,l)  > ',1D11.4)
      IF (IMQ.EQ.1) WRITE(*,99)
   99 FORMAT(' Tagging for quark pair in final state (b bbar).')
      WRITE(*,81) QCDSC(IQCDDS)
   81 FORMAT(' Dynamical QCD scale:     ',A15)
      IF (ISUB(1).EQ.1) WRITE(*,73)
   73 FORMAT(' Processes with 2 quarks are included. ')
      IF (ISUB(2).EQ.1) WRITE(*,174)
  174 FORMAT(' Processes with 4 quarks are included. ')
      IF (ISUB(3).EQ.1) WRITE(*,175)
  175 FORMAT(' Processes with 6 quarks are included. ')
      IF(IHELRN.EQ.1) WRITE(*,82)
   82 FORMAT(' ==> Integration is a Monte Carlo over hel. configs.!')
      WRITE(*,*)
      WRITE(*,311) ' Input parameters of integration.'
      WRITE(*,311) ' ================================'
      IF (ITMX1.GT.0) WRITE(*,101) ITMX1,NCALL1,MANNR(IMANN1)
  101 FORMAT(' Initializing iterations: ',1I3,' of ',1I7,' points ',
     . 'with method ',1A10)
      WRITE(*,102) ITMX2,NCALL2,MANNR(IMANN2)
  102 FORMAT(' Integrating  iterations: ',1I3,' of ',1I7,' points ',
     . 'with method ',1A10)
*
      WRITE(*,*)
      WRITE(*,311) ' Results of Monte Carlo run.'
      WRITE(*,311) ' ==========================='
      WRITE(*,105) NJET,AVGI1,SD1,CHI2A1
  105 FORMAT('      Init: SIGMA(',I2,') =',1D11.4,' +/- ',1D11.4,
     .   ' nb.   ( CHI2/DOF=',1F7.3,') ')
      WRITE(*,106) NJET,AVGI2,SD2,CHI2A2
  106 FORMAT(' ==> Final: SIGMA(',I2,') =',1D11.4,' +/- ',1D11.4,
     .   ' nb.   ( CHI2/DOF=',1F7.3,')')
      EFF=1.0D0*NIN/(NIN+NOUT)
      WRITE(*,115) NIN
  115 FORMAT(' # accepted events:   ',1I6)
      WRITE(*,117) NOUT
  117 FORMAT(' # rejected events:   ',1I6)
      WRITE(*,118) EFF
  118 FORMAT(' Efficiency of event generation: ',1D11.4)
      WRITE(*,*)
*
* Statistics for various subprocesses and parton densities
*
* W statistics.
      IF (IVEC.EQ.1) THEN
      IF (IWHICH.NE.2) THEN
      WRITE(*,311) ' Statistics for the W+ subprocesses. '
      WRITE(*,311) ' Subprocess         col 1    col 2    col 3    col 4
     .     col 5    col 6  CONT(%)'
        DO 37 I=1,2
          WRITE(*,111) SQWP(I),(TQ(1,I,J),J=1,6),
     .                        TQ(1,I,IUSE)*100.0/AVGI2
   37   CONTINUE
        DO 38 I=5,9
          WRITE(*,111) FL(I-4),(TQ(1,I,J),J=1,6),
     .                        TQ(1,I,IUSE)*100.0/AVGI2
   38   CONTINUE
        END IF
        IF (IWHICH.NE.1) THEN
      WRITE(*,*)
      WRITE(*,311) ' Statistics for the W- subprocesses. '
      WRITE(*,311) ' Subprocess         col 1    col 2    col 3    col 4
     .     col 5    col 6  CONT(%)'
        DO 47 I=3,4
          WRITE(*,111) SQWM(I-2),(TQ(2,I,J),J=1,6),
     .                        TQ(2,I,IUSE)*100.0/AVGI2
   47   CONTINUE
        DO 48 I=5,9
          WRITE(*,111) FL(I-4),(TQ(2,I,J),J=1,6),
     .                        TQ(2,I,IUSE)*100.0/AVGI2
   48   CONTINUE
        END IF
      WRITE(*,*)
      WRITE(*,311) ' Statistics for the complete process. '
      WRITE(*,311) ' Subprocess         col 1    col 2    col 3    col 4
     .     col 5    col 6  CONT(%)'
        DO 58 I=5,9
          DO 59 J=1,6
            IF (IWHICH.EQ.1) TQ(2,I,J)=0D0
            IF (IWHICH.EQ.2) TQ(1,I,J)=0D0
   59     CONTINUE
          WRITE(*,111) FL(I-4),(TQ(1,I,J)+TQ(2,I,J),J=1,6),
     .                      (TQ(1,I,IUSE)+TQ(2,I,IUSE))*100.0/AVGI2
   58   CONTINUE
  111 FORMAT(1A18,6D9.2,1F7.2)
      ELSE
* Z Statistics.
      WRITE(*,311) ' Statistics for the Z processes. '
      WRITE(*,311) ' Subprocess         col 1    col 2    col 3    col 4
     .     col 5    col 6  CONT(%)'
        DO 68 I=5,9
          WRITE(*,111) FL(I-4),(TQ(1,I,J),J=1,6),
     .                        TQ(1,I,IUSE)*100.0/AVGI2
   68   CONTINUE
      END IF
      write(*,*)
      DO I=1,6
         IF (ITRY(I).NE.0) THEN
            ISTRUC=ITRY(I)
            sstru='non existing ?'
            if (istruc.eq.1)  sstru='mrse    (msbar)'
            if (istruc.eq.2)  sstru='mrsb    (msbar)'
            if (istruc.eq.3)  sstru='kmrshb  (msbar)'
            if (istruc.eq.4)  sstru='kmrsb0  (msbar)'
            if (istruc.eq.5)  sstru='kmrsb-  (msbar)'
            if (istruc.eq.6)  sstru='kmrsb-5 (msbar)'
            if (istruc.eq.7)  sstru='kmrsb-2 (msbar)'
            if (istruc.eq.8)  sstru='mts1    (msbar)'
            if (istruc.eq.9)  sstru='mte1    (msbar)'
            if (istruc.eq.10) sstru='mtb1    (msbar)'
            if (istruc.eq.11) sstru='mtb2    (msbar)'
            if (istruc.eq.12) sstru='mtsn1   (msbar)'
            if (istruc.eq.13) sstru='kmrss0  (msbar)'
            if (istruc.eq.14) sstru='kmrsd0  (msbar)'
            if (istruc.eq.15) sstru='kmrsd-  (msbar)'
            if (istruc.eq.16) sstru='mrss0   (msbar)'
            if (istruc.eq.17) sstru='mrsd0   (msbar)'
            if (istruc.eq.18) sstru='mrsd-   (msbar)'
            if (istruc.eq.19) sstru='cteq1l  (msbar)'
            if (istruc.eq.20) sstru='cteq1m  (msbar)'
            if (istruc.eq.21) sstru='cteq1ml (msbar)'
            if (istruc.eq.22) sstru='cteq1ms (msbar)'
            if (istruc.eq.23) sstru='grv     (msbar)'
            if (istruc.eq.24) sstru='cteq2l  (msbar)'
            if (istruc.eq.25) sstru='cteq2m  (msbar)'
            if (istruc.eq.26) sstru='cteq2ml (msbar)'
            if (istruc.eq.27) sstru='cteq2ms (msbar)'
            if (istruc.eq.28) sstru='cteq2mf (msbar)'
            if (istruc.eq.29) sstru='mrsh    (msbar)'
            if (istruc.eq.30) sstru='mrsa    (msbar)'
            if (istruc.eq.31) sstru='mrsg    (msbar)'
            if (istruc.eq.32) sstru='mrsap   (msbar)'
            if (istruc.eq.33) sstru='grv94   (msbar)'
            if (istruc.eq.34) sstru='cteq3l  (msbar)'
            if (istruc.eq.35) sstru='cteq3m  (msbar)'
            if (istruc.eq.36) sstru='mrsj    (msbar)'
            if (istruc.eq.37) sstru='mrsjp   (msbar)'
            if (istruc.eq.38) sstru='cteqjet (msbar)'
            if (istruc.eq.39) sstru='mrsr1   (msbar)'
            if (istruc.eq.40) sstru='mrsr2   (msbar)'
            if (istruc.eq.41) sstru='mrsr3   (msbar)'
            if (istruc.eq.42) sstru='mrsr4   (msbar)'
            if (istruc.eq.43) sstru='cteq4l  (msbar)'
            if (istruc.eq.44) sstru='cteq4m  (msbar)'
            if (istruc.eq.45) sstru='cteq4lq (msbar)'
            if (istruc.eq.46) sstru='cteq4lq (msbar)'
            if (istruc.eq.101) sstru='mrsap (Lambda=0.216)'
            if (istruc.eq.102) sstru='mrsap (Lambda=0.284)'
            if (istruc.eq.103) sstru='mrsap (Lambda=0.366)'
            if (istruc.eq.104) sstru='mrsap (Lambda=0.458)'
            if (istruc.eq.105) sstru='mrsap (Lambda=0.564)'
            if (istruc.eq.111) sstru='grv94 (Lambda=0.150)'
            if (istruc.eq.112) sstru='grv94 (Lambda=0.200)'
            if (istruc.eq.113) sstru='grv94 (Lambda=0.250)'
            if (istruc.eq.114) sstru='grv94 (Lambda=0.300)'
            if (istruc.eq.115) sstru='grv94 (Lambda=0.350)'
            if (istruc.eq.116) sstru='grv94 (Lambda=0.400)'
      if (istruc.eq.121) sstru='cteq3m (msbar)'
      if (istruc.eq.122) sstru='cteq3m (msbar)'
      if (istruc.eq.123) sstru='cteq3m (msbar)'
      if (istruc.eq.124) sstru='cteq3m (msbar)'
      if (istruc.eq.125) sstru='cteq3m (msbar)'
      if (istruc.eq.126) sstru='cteq3m (msbar)'
      if (istruc.eq.127) sstru='cteq3m (msbar)'
      if (istruc.eq.128) sstru='cteq3m (msbar)'
      if (istruc.eq.129) sstru='cteq3m (msbar)'
      if (istruc.eq.130) sstru='cteq4m (msbar)'
      if (istruc.eq.131) sstru='cteq4m (msbar)'
      if (istruc.eq.132) sstru='cteq4m (msbar)'
      if (istruc.eq.133) sstru='cteq4m (msbar)'
      if (istruc.eq.134) sstru='cteq4m (msbar)'
            if (i.eq.iuse) then
               write(*,*) 'column',i,' uses ',sstru,
     .                    '(-> used during integration)'
            else
               write(*,*) 'column',i,' uses ',sstru
            endif
         endif
      enddo
      write(*,*)
      write(*,*) 'distributions'
      write(*,*)
*
* Output histograms
*
      CALL HISOUT(1,1)
      IF (ISTAT(1).EQ.1) CALL HISOUT(41,1)
      IF (ISTAT(2).EQ.1) CALL HISOUT(81,2)
      IF (ISTAT(3).EQ.1) CALL HISOUT(121,3)
*
      END
*
************************************************************************
* HISOUT puts the histograms in the file VECBOS.OUT.                   *
* convention: IHIST=1  total process.                                  *
*             IHIST=41 2 q stuff.                                      *
*             IHIST=81 4 q stuff.                                      *
*             IHIST=121 6 q stuff.                                     *
************************************************************************
      SUBROUTINE HISOUT(IHIST,ISUB)
      IMPLICIT REAL*8 (A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      CHARACTER*40 PROC(4)
      SAVE PROC
      DATA PROC / ' ==> All processes together. ',
     .            ' ==> Two quark processes only. ',
     .            ' ==> Four quark processes only. ',
     .            ' ==> Six quark processes only. '/
      ICOUNT=0
*
      WRITE(*,*) ' average jet transverse energy ',PROC(ISUB)
      CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,2,' ',4,1)
      write(*,*) '-----------------------------------------'
      ICOUNT=ICOUNT+1
      WRITE(*,*) ' et electron  ',PROC(ISUB)
      CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      write(*,*) '-----------------------------------------'
      ICOUNT=ICOUNT+1
      WRITE(*,*) 'transverse W mass ',PROC(ISUB)
      CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      write(*,*) '-----------------------------------------'
      ICOUNT=ICOUNT+1
      WRITE(*,*) 'weights ',PROC(ISUB)
      CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      write(*,*) '-----------------------------------------'
      ICOUNT=ICOUNT+1
*
      END
*
************************************************************************
* BIN(WT,1) generates histograms for the complete NJET process         *
* Histograms for different subprocesses: 41=2q, 81=4q, 121=4q          *
* IHISTO-IHISTO+X    : CM, PT, PTEL, WEIGHT, X1, X2, ANGLP, ANGNP,     *
*                      PTVEC, PZVEC                                    *
************************************************************************
      SUBROUTINE BIN(WT,IHISTO)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /HISSCA/ CMMAX,PTMAX,PTELMA,PWEIGH,ANLEPR,PTVMAX,PZVMAX,
     .                ICM,IPT,IPTEL,IWEIGH,IANLEP,IPTVEC,IPZVEC
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /BINVAR/ PT(10),ETA(10),DR(10,10),PTEL,PTNU,ETAEL1,ETAEL2,
     .                ANGLP,ANGNP,PTVEC,PZVEC
      COMMON /MOM/    PLAB(4,10),WHAT
      common /sort/ jp(10)
      SAVE /BINVAR/,/MOM/
*
      ICOUNT=0
* average pt jets
      DO 10 J=1,NJET
        CALL HISTO(IHISTO+ICOUNT,1,PT(j),0.D0,100d0,WT/4d0,2,' ',4,50)
        sumet=sumet+pt(j)
   10 CONTINUE
* electron pt
      icount=icount+1
      call histo(ihisto+icount,1,ptel,0.D0,150d0,WT,1,' ',4,30)
* transverse mass
      icount=icount+1
      rm=sqrt(2d0*(ptel*ptnu-plab(1,9)*plab(1,10)
     .                      -plab(2,9)*plab(2,10)))
      CALL HISTO(IHISTO+ICOUNT,1,rm,0d0,150d0,WT,2,' ',4,50)
* weights
      icount=icount+1
      IF (WT.LT.1D-30) THEN
        CALL HISTO(IHISTO+ICOUNT,1,LOG(1D-30),-PWEIGH,PWEIGH
     .                                    ,1D0,1,' ',4,IWEIGH)
      ELSE
        CALL HISTO(IHISTO+ICOUNT,1,LOG10(WT),-PWEIGH,PWEIGH
     .                                    ,1D0,1,' ',4,IWEIGH)
      END IF
*
      END
 
************************************************************************
*                                                                      *
* This includes                                                        *
*    VEGAS   - Integration routine by Lepage                           *
*    STRUCT  - Parton densities from Stirling group                    *
*              Uses files : MRSEB1.DAT and MRSEB2.DAT                  *
*    RAMBO   - Momenta generator, uniformly distributed                *
*    HISTO   - Steven van der Marcks HISTOGRAM routine                 *
*    RMARIN  - Random number generator called by the function RN       *
*                                                                      *
************************************************************************
      SUBROUTINE VEGAS(FXN,AVGI,SD,CHI2A)
C
C   SUBROUTINE PERFORMS N-DIMENSIONAL MONTE CARLO INTEG'N
C      - BY G.P. LEPAGE   SEPT 1976/(REV)APR 1978
C
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/BVEG1/XL(16),XU(16),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON/BVEG2/XI(50,16),SI,SI2,SWGT,SCHI,NDO,IT
      DIMENSION D(50,16),DI(50,16),XIN(50),R(50),DX(16),DT(16),X(16)
     1   ,KG(16),IA(16)
      REAL*8 QRAN(16)
      EXTERNAL FXN
      SAVE /BVEG1/,/BVEG2/,NDMX,ALPH,ONE,MDS
      DATA NDMX/50/,ALPH/1.5D0/,ONE/1D0/,MDS/1/
C
      NDO=1
      DO 1 J=1,NDIM
1     XI(1,J)=ONE
C
      ENTRY VEGAS1(FXN,AVGI,SD,CHI2A)
C         - INITIALIZES CUMMULATIVE VARIABLES, BUT NOT GRID
      IT=0
      SI=0.
      SI2=SI
      SWGT=SI
      SCHI=SI
C
      ENTRY VEGAS2(FXN,AVGI,SD,CHI2A)
C         - NO INITIALIZATION
      ND=NDMX
      NG=1
      IF(MDS.EQ.0) GO TO 2
      NG=(NCALL/2.)**(1./NDIM)
      MDS=1
      IF((2*NG-NDMX).LT.0) GO TO 2
      MDS=-1
      NPG=NG/NDMX+1
      ND=NG/NPG
      NG=NPG*ND
2     K=NG**NDIM
      NPG=NCALL/K
      IF(NPG.LT.2) NPG=2
      CALLS=NPG*K
      DXG=ONE/NG
      DV2G=(CALLS*DXG**NDIM)**2/NPG/NPG/(NPG-ONE)
      XND=ND
      NDM=ND-1
      DXG=DXG*XND
      XJAC=ONE/CALLS
      DO 3 J=1,NDIM
      DX(J)=XU(J)-XL(J)
3     XJAC=XJAC*DX(J)
C
C   REBIN PRESERVING BIN DENSITY
C
      IF(ND.EQ.NDO) GO TO 8
      RC=NDO/XND
      DO 7 J=1,NDIM
      K=0
      XN=0.
      DR=XN
      I=K
4     K=K+1
      DR=DR+ONE
      XO=XN
      XN=XI(K,J)
5     IF(RC.GT.DR) GO TO 4
      I=I+1
      DR=DR-RC
      XIN(I)=XN-(XN-XO)*DR
      IF(I.LT.NDM) GO TO 5
      DO 6 I=1,NDM
6     XI(I,J)=XIN(I)
7     XI(ND,J)=ONE
      NDO=ND
C
8     IF(NPRN.NE.0) WRITE(6,200) NDIM,CALLS,IT,ITMX,ACC,MDS,ND
     1                           ,(XL(J),XU(J),J=1,NDIM)
C
      ENTRY VEGAS3(FXN,AVGI,SD,CHI2A)
C         - MAIN INTEGRATION LOOP
9     IT=IT+1
      TI=0.
      TSI=TI
      DO 10 J=1,NDIM
      KG(J)=1
      DO 10 I=1,ND
      D(I,J)=TI
10    DI(I,J)=TI
C
11    FB=0.
      F2B=FB
      K=0
12    K=K+1
      CALL ARAN9(QRAN,NDIM)
      WGT=XJAC
      DO 15 J=1,NDIM
      XN=(KG(J)-QRAN(J))*DXG+ONE
      IA(J)=XN
      IF(IA(J).GT.1) GO TO 13
      XO=XI(IA(J),J)
      RC=(XN-IA(J))*XO
      GO TO 14
13    XO=XI(IA(J),J)-XI(IA(J)-1,J)
      RC=XI(IA(J)-1,J)+(XN-IA(J))*XO
14    X(J)=XL(J)+RC*DX(J)
15    WGT=WGT*XO*XND
C
      F=WGT
      F=F*FXN(X,WGT)
      F2=F*F
      FB=FB+F
      F2B=F2B+F2
      DO 16 J=1,NDIM
      DI(IA(J),J)=DI(IA(J),J)+F
16    IF(MDS.GE.0) D(IA(J),J)=D(IA(J),J)+F2
      IF(K.LT.NPG) GO TO 12
C
      F2B=SQRT(F2B*NPG)
      F2B=(F2B-FB)*(F2B+FB)
      TI=TI+FB
      TSI=TSI+F2B
      IF(MDS.GE.0) GO TO 18
      DO 17 J=1,NDIM
17    D(IA(J),J)=D(IA(J),J)+F2B
18    K=NDIM
19    KG(K)=MOD(KG(K),NG)+1
      IF(KG(K).NE.1) GO TO 11
      K=K-1
      IF(K.GT.0) GO TO 19
C
C   FINAL RESULTS FOR THIS ITERATION
C
      TSI=TSI*DV2G
      TI2=TI*TI
      WGT=TI2/TSI
      SI=SI+TI*WGT
      SI2=SI2+TI2
      SWGT=SWGT+WGT
      SCHI=SCHI+TI2*WGT
      AVGI=SI/SWGT
      SD=SWGT*IT/SI2
      CHI2A=SD*(SCHI/SWGT-AVGI*AVGI)/(IT-.999)
      SD=SQRT(ONE/SD)
C
      IF(NPRN.EQ.0) GO TO 21
      TSI=SQRT(TSI)
      WRITE(6,201) IT,TI,TSI,AVGI,SD,CHI2A
      IF(NPRN.GE.0) GO TO 21
      DO 20 J=1,NDIM
20    WRITE(6,202) J,(XI(I,J),DI(I,J),D(I,J),I=1,ND)
C
C   REFINE GRID
C
21    DO 23 J=1,NDIM
      XO=D(1,J)
      XN=D(2,J)
      D(1,J)=(XO+XN)/2.
      DT(J)=D(1,J)
      DO 22 I=2,NDM
      D(I,J)=XO+XN
      XO=XN
      XN=D(I+1,J)
      D(I,J)=(D(I,J)+XN)/3.
22    DT(J)=DT(J)+D(I,J)
      D(ND,J)=(XN+XO)/2.
23    DT(J)=DT(J)+D(ND,J)
C
      DO 28 J=1,NDIM
      RC=0.
      DO 24 I=1,ND
      R(I)=0.
      IF(D(I,J).LE.0.) GO TO 24
      XO=DT(J)/D(I,J)
      R(I)=((XO-ONE)/XO/LOG(XO))**ALPH
24    RC=RC+R(I)
      RC=RC/XND
      K=0
      XN=0.
      DR=XN
      I=K
25    K=K+1
      DR=DR+R(K)
      XO=XN
      XN=XI(K,J)
26    IF(RC.GT.DR) GO TO 25
      I=I+1
      DR=DR-RC
      XIN(I)=XN-(XN-XO)*DR/R(K)
      IF(I.LT.NDM) GO TO 26
      DO 27 I=1,NDM
27    XI(I,J)=XIN(I)
28    XI(ND,J)=ONE
C
      IF(IT.LT.ITMX.AND.ACC*ABS(AVGI).LT.SD) GO TO 9
200   FORMAT('0INPUT PARAMETERS FOR VEGAS:  NDIM=',I3,'  NCALL=',F8.0
     1    /28X,'  IT=',I5,'  ITMX=',I5/28X,'  ACC=',G9.3
     2    /28X,'  MDS=',I3,'   ND=',I4/28X,'  (XL,XU)=',
     3    (T40,'( ',G12.6,' , ',G12.6,' )'))
201   FORMAT(///' INTEGRATION BY VEGAS' / '0ITERATION NO.',I3,
     1    ':   INTEGRAL =',G14.8/21X,'STD DEV  =',G10.4 /
     2    ' ACCUMULATED RESULTS:   INTEGRAL =',G14.8 /
     3    24X,'STD DEV  =',G10.4 / 24X,'CHI**2 PER IT''N =',G10.4)
202   FORMAT('0DATA FOR AXIS',I2 / ' ',6X,'X',7X,'  DELT I  ',
     1    2X,' CONV''CE  ',11X,'X',7X,'  DELT I  ',2X,' CONV''CE  '
     2   ,11X,'X',7X,'  DELT I  ',2X,' CONV''CE  ' /
     2    (' ',3G12.4,5X,3G12.4,5X,3G12.4))
      RETURN
      END
      SUBROUTINE SAVE(NDIM)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/BVEG2/XI(50,16),SI,SI2,SWGT,SCHI,NDO,IT
      SAVE /BVEG2/
C
C   STORES VEGAS DATA (UNIT 7) FOR LATER RE-INITIALIZATION
C
      WRITE(7,200) NDO,IT,SI,SI2,SWGT,SCHI,
     1             ((XI(I,J),I=1,NDO),J=1,NDIM)
      RETURN
      ENTRY RESTR(NDIM)
C
C   ENTERS INITIALIZATION DATA FOR VEGAS
C
      READ(7,200) NDO,IT,SI,SI2,SWGT,SCHI,
     1            ((XI(I,J),I=1,NDO),J=1,NDIM)
200   FORMAT(2I8,4Z16/(5Z16))
      RETURN
      END
 
      SUBROUTINE ARAN9(QRAN,NDIM)
      REAL*8 QRAN(16),RN
      DO I=1,NDIM
         QRAN(I)=RN(I)
      enddo
      END
*
      SUBROUTINE RAMBO(N,ET,XM,P,WT)
C------------------------------------------------------
C
C                       RAMBO
C
C    RA(NDOM)  M(OMENTA)  B(EAUTIFULLY)  O(RGANIZED)
C
C    A DEMOCRATIC MULTI-PARTICLE PHASE SPACE GENERATOR
C    AUTHORS:  S.D. ELLIS,  R. KLEISS,  W.J. STIRLING
C    THIS IS VERSION 1.0 -  WRITTEN BY R. KLEISS
C
C    N  = NUMBER OF PARTICLES (>1, IN THIS VERSION <101)
C    ET = TOTAL CENTRE-OF-MASS ENERGY
C    XM = PARTICLE MASSES ( DIM=100 )
C    P  = PARTICLE MOMENTA ( DIM=(4,100) )
C    WT = WEIGHT OF THE EVENT
C
C------------------------------------------------------
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION XM(100),P(4,100),Q(4,100),Z(100),R(4),
     .   B(3),P2(100),XM2(100),E(100),V(100),IWARN(5)
      SAVE ACC,ITMAX,IBEGIN,IWARN
      DATA ACC/1.D-14/,ITMAX/6/,IBEGIN/0/,IWARN/5*0/
C
C INITIALIZATION STEP: FACTORIALS FOR THE PHASE SPACE WEIGHT
      IF(IBEGIN.NE.0) GOTO 103
      IBEGIN=1
      TWOPI=8.*DATAN(1.D0)
      PO2LOG=LOG(TWOPI/4.)
      Z(2)=PO2LOG
      DO 101 K=3,100
  101 Z(K)=Z(K-1)+PO2LOG-2.*LOG(DFLOAT(K-2))
      DO 102 K=3,100
  102 Z(K)=(Z(K)-LOG(DFLOAT(K-1)))
C
C CHECK ON THE NUMBER OF PARTICLES
  103 IF(N.GT.1.AND.N.LT.101) GOTO 104
      PRINT 1001,N
      STOP
C
C CHECK WHETHER TOTAL ENERGY IS SUFFICIENT; COUNT NONZERO MASSES
  104 XMT=0.
      NM=0
      DO 105 I=1,N
      IF(XM(I).NE.0.D0) NM=NM+1
  105 XMT=XMT+ABS(XM(I))
      IF(XMT.LE.ET) GOTO 201
      PRINT 1002,XMT,ET
      STOP
C
C THE PARAMETER VALUES ARE NOW ACCEPTED
C
C GENERATE N MASSLESS MOMENTA IN INFINITE PHASE SPACE
  201 DO 202 I=1,N
      C=2.*RN(1)-1.
      S=SQRT(1.-C*C)
      F=TWOPI*RN(2)
      Q(4,I)=-LOG(RN(3)*RN(4))
      Q(3,I)=Q(4,I)*C
      Q(2,I)=Q(4,I)*S*COS(F)
  202 Q(1,I)=Q(4,I)*S*SIN(F)
C
C CALCULATE THE PARAMETERS OF THE CONFORMAL TRANSFORMATION
      DO 203 I=1,4
  203 R(I)=0.
      DO 204 I=1,N
      DO 204 K=1,4
  204 R(K)=R(K)+Q(K,I)
      RMAS=SQRT(R(4)**2-R(3)**2-R(2)**2-R(1)**2)
      DO 205 K=1,3
  205 B(K)=-R(K)/RMAS
      G=R(4)/RMAS
      A=1./(1.+G)
      X=ET/RMAS
C
C TRANSFORM THE Q'S CONFORMALLY INTO THE P'S
      DO 207 I=1,N
      BQ=B(1)*Q(1,I)+B(2)*Q(2,I)+B(3)*Q(3,I)
      DO 206 K=1,3
  206 P(K,I)=X*(Q(K,I)+B(K)*(Q(4,I)+A*BQ))
  207 P(4,I)=X*(G*Q(4,I)+BQ)
C
C CALCULATE WEIGHT AND POSSIBLE WARNINGS
      WT=PO2LOG
      IF(N.NE.2) WT=(2.*N-4.)*LOG(ET)+Z(N)
      IF(WT.GE.-180.D0) GOTO 208
      IF(IWARN(1).LE.5) PRINT 1004,WT
      IWARN(1)=IWARN(1)+1
  208 IF(WT.LE. 174.D0) GOTO 209
      IF(IWARN(2).LE.5) PRINT 1005,WT
      IWARN(2)=IWARN(2)+1
C
C RETURN FOR WEIGHTED MASSLESS MOMENTA
  209 IF(NM.NE.0) GOTO 210
      WT=EXP(WT)
      RETURN
C
C MASSIVE PARTICLES: RESCALE THE MOMENTA BY A FACTOR X
  210 XMAX=SQRT(1.-(XMT/ET)**2)
      DO 301 I=1,N
      XM2(I)=XM(I)**2
  301 P2(I)=P(4,I)**2
      ITER=0
      X=XMAX
      ACCU=ET*ACC
  302 F0=-ET
      G0=0.
      X2=X*X
      DO 303 I=1,N
      E(I)=SQRT(XM2(I)+X2*P2(I))
      F0=F0+E(I)
  303 G0=G0+P2(I)/E(I)
      IF(ABS(F0).LE.ACCU) GOTO 305
      ITER=ITER+1
      IF(ITER.LE.ITMAX) GOTO 304
      PRINT 1006,ITMAX
      GOTO 305
  304 X=X-F0/(X*G0)
      GOTO 302
  305 DO 307 I=1,N
      V(I)=X*P(4,I)
      DO 306 K=1,3
  306 P(K,I)=X*P(K,I)
  307 P(4,I)=E(I)
C
C CALCULATE THE MASS-EFFECT WEIGHT FACTOR
      WT2=1.
      WT3=0.
      DO 308 I=1,N
      WT2=WT2*V(I)/E(I)
  308 WT3=WT3+V(I)**2/E(I)
      WTM=(2.*N-3.)*LOG(X)+LOG(WT2/WT3*ET)
C
C RETURN FOR  WEIGHTED MASSIVE MOMENTA
      WT=WT+WTM
      IF(WT.GE.-180.D0) GOTO 309
      IF(IWARN(3).LE.5) PRINT 1004,WT
      IWARN(3)=IWARN(3)+1
  309 IF(WT.LE. 174.D0) GOTO 310
      IF(IWARN(4).LE.5) PRINT 1005,WT
      IWARN(4)=IWARN(4)+1
  310 WT=EXP(WT)
      RETURN
C
 1001 FORMAT(' RAMBO FAILS: # OF PARTICLES =',I5,' IS NOT ALLOWED')
 1002 FORMAT(' RAMBO FAILS: TOTAL MASS =',D15.6,' IS NOT',
     . ' SMALLER THAN TOTAL ENERGY =',D15.6)
 1004 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY UNDERFLOW')
 1005 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY  OVERFLOW')
 1006 FORMAT(' RAMBO WARNS:',I3,' ITERATIONS DID NOT GIVE THE',
     . ' DESIRED ACCURACY =',D15.6)
      END
*
      SUBROUTINE HISTO(I,ISTAT,X,X0,X1,WEIGHT,LINLOG,TITLE,IUNIT,NX)
*     ----------------
* Steven van der Marck, February 22nd, 1990.
* I      = number of this particular histogram
* ISTAT  = 0    : clear the arrays
*          1    : fill  the arrays
*          2    : print a histogram
*          3    : output the data to HISTO.GRA - to be used for a
*                 'home made' graphics program.
*          4    : same as 3, but the whole histogram is divided by
*                 the number of points.
*          5    : save all relevant information to a file HISTO.DAT
*          6    : read all relevant information from file HISTO.DAT
*          ELSE : rescale all histo's by a factor X
* X      = x-value to be placed in a bin of the histogram
*          If ISTAT=2 and LINLOG<>1, x = the number of decades (def=3).
* X0     = the minimum for x in the histogram
* X1     = the maximum for x in the histogram
* WEIGHT = If ISTAT=1: the weight assigned to this value of x.
*          If ISTAT=2: the number of divisions on the y-axis (def=20),
*                     should not be <5 or >79 (because of screenwidth).
* LINLOG = 1    : linear      histogram
*          ELSE : logarithmic histogram
*          If a linear histo has only one peak that is too sharp,
*          this routine will automatically switch to a log. histo.
* TITLE  = title of this particular histogram ( character*(*) )
* IUNIT  = unit number to write this histogram to
* NX     = the number of divisions on the x-axis for this histogram
*          NX should not be greater than NXMAX (=50 in this version).
*
* When ISTAT = 0,5,6  : no other variables are used.
*            = 1      : I, X, X0, X1, WEIGHT, and NX are used.
*            = 2      : I, X, WEIGHT, LINLOG, TITLE and IUNIT are used.
*            = 3,4    : I, LINLOG and TITLE are used. The user should
*         not be using unit nr 11 when invoking HISTO with this option!
*            = ELSE   : only X is used.
      IMPLICIT LOGICAL(A-Z)
      INTEGER N,NX,NXMAX,I,ISTAT,LINLOG,IUNIT
* N = the maximum number of histo's allowed; can be changed on its own.
* NXMAX = the maximum number (NX) of divisions allowed on the x-axis.
      PARAMETER( N = 200, NXMAX = 50 )
      INTEGER J1,J2,IX,IY,NYDIV
      REAL*8 X,X0,X1,WEIGHT, Z,WEISUM,WEISU2,FACTOR
      REAL*8 Y(N,NXMAX), YMAX(N), BOUND0(N), BOUND1(N),
     +                 XMIN(N), XMAX(N), YSQUAR(N,NXMAX), YOUT(N)
      INTEGER IUNDER(N), IIN(N), IOVER(N), NRBINS(N), IBIN(N,NXMAX)
      CHARACTER TITLE*(*),LINE(132),BLANK,STAR,TEXT*13
      CHARACTER FORM1*80,F*80,STRP*1,FH*30,F2(1:3)*12
      SAVE
      DATA STRP/'-'/F2/'(1X,4I10)','(1X,10I10)','(1X,10D12.5)'/
      DATA BLANK/' '/STAR/'*'/FH/'(A),''I'',3X,G11.4,2X,G11.4,I12)'/
      IF(ISTAT .EQ. 0) THEN
        DO 20 J1 = 1 , N
          BOUND0(J1) = 0D0
          BOUND1(J1) = 0D0
          XMIN  (J1) = 0D0
          XMAX  (J1) = 0D0
          YMAX  (J1) = 0D0
          YOUT  (J1) = 0D0
          IUNDER(J1) = 0
          IIN   (J1) = 0
          IOVER (J1) = 0
          NRBINS(J1) = 0
          DO 10 J2 = 1 , NXMAX
            Y     (J1,J2) = 0D0
            YSQUAR(J1,J2) = 0D0
            IBIN  (J1,J2) = 0
   10     CONTINUE
   20   CONTINUE
        WRITE(*,'(1X,A,I3,A)')'HISTO initialization is done.',N,
     +    ' histograms are available.'
      ELSEIF(ISTAT.EQ.1) THEN
        BOUND0(I) = X0
        BOUND1(I) = X1
        IF(NRBINS(I) .EQ. 0) THEN
          IF(NX.GT.0 .AND. NX.LE.NXMAX) THEN
            NRBINS(I) = NX
          ELSE
            NRBINS(I) = NXMAX
          ENDIF
        ENDIF
        IF(X.LT.X0)THEN
          IUNDER(I) = IUNDER(I) + 1
          YOUT  (I) = YOUT  (I) + WEIGHT
          IF(X.LT.XMIN(I).OR.IUNDER(I).EQ.1) XMIN(I) = X
        ELSEIF(X.GT.X1)THEN
          IOVER(I) = IOVER(I) + 1
          YOUT (I) = YOUT (I) + WEIGHT
          IF(X.GT.XMAX(I).OR. IOVER(I).EQ.1) XMAX(I) = X
        ELSE
          IIN(I) = IIN(I) + 1
          IX     = INT((X-X0)/(X1-X0)*NRBINS(I)) + 1
          IF(IX.EQ.NRBINS(I)+1) IX = NRBINS(I)
          IBIN  (I,IX) = IBIN  (I,IX) + 1
          Y     (I,IX) = Y     (I,IX) + WEIGHT
          YSQUAR(I,IX) = YSQUAR(I,IX) + WEIGHT**2
          IF(Y(I,IX).GT.YMAX(I)) YMAX(I) = Y(I,IX)
        ENDIF
      ELSEIF(ISTAT .EQ. 2) THEN
        WRITE(*,'(//,A,I2,A,I10,A,I8,A,I8)')' Histogram nr.',I,
     +  '  Points in:',IIN(I),'  under:',IUNDER(I),'  over:',IOVER(I)
        WRITE(*,*)' ',TITLE
        IF(YMAX(I).LE.0D0) RETURN
        NYDIV = INT(WEIGHT)
        IF(NYDIV .LT. 5 .OR. NYDIV .GT. 79) NYDIV = 20
        IF(LINLOG .EQ. 1) THEN
          IX = 0
          DO 30 J1 = 1 , NRBINS(I)
            IF(Y(I,J1)/YMAX(I)*NYDIV .GT. 1D0) IX = IX + 1
   30     CONTINUE
        ENDIF
        IF(IX .LE. 2 .OR. LINLOG .NE. 1) THEN
          IX = 2
          FACTOR = 1D3/YMAX(I)
          IF(X.GE.1D0 .AND. X.LE.10D0) FACTOR = 10D0**X/YMAX(I)
        ELSE
          IX = 1
        ENDIF
        WRITE(FORM1,'(A,I3,A)') '(''  '',G11.4,1X,',NYDIV,'(A),A)'
        WRITE(F,'(A,I3,A)') '('' '',A13,',NYDIV,FH
        WRITE(*,FORM1) BOUND0(I),(STRP,J1=1,NYDIV),
     +    '   bin boundary   bin ''area''    # points'
        WEISUM = 0D0
        WEISU2 = 0D0
        DO 50 J1 = 1 , NRBINS(I)
          IF(IX.EQ.1) THEN
            IY=INT(Y(I,J1)/YMAX(I)*FLOAT(NYDIV))+1
          ELSE
            IF(FACTOR*Y(I,J1).LE.1D0) THEN
              IY=1
            ELSE
              IY=INT(LOG(FACTOR*Y(I,J1))/LOG(FACTOR*YMAX(I))*
     +           FLOAT(NYDIV))+1
            ENDIF
          ENDIF
          IF(IY .EQ. NYDIV+1) IY = NYDIV
          DO 40 J2 = 1 , NYDIV
            LINE(J2)=BLANK
            IF(J2.EQ.IY) LINE(J2)=STAR
   40     CONTINUE
          Z = BOUND0(I) + J1/FLOAT(NRBINS(I))*(BOUND1(I)-BOUND0(I))
          WEISUM = WEISUM + Y(I,J1)
          WEISU2 = WEISU2 + YSQUAR(I,J1)
          IF(J1.EQ.INT(FLOAT(NRBINS(I))/2.D0))THEN
            TEXT = 'logarithmic I'
            IF(IX.EQ.1) TEXT = '  linear    I'
          ELSE
            TEXT = '            I'
          ENDIF
          WRITE(*,F)TEXT,(LINE(J2),J2=1,NYDIV),Z,Y(I,J1),IBIN(I,J1)
   50   CONTINUE
        WRITE(*,FORM1) BOUND1(I),(STRP,J1=1,NYDIV),' '
        Z=SQRT(ABS(WEISU2-WEISUM**2/FLOAT(IIN(I))))/FLOAT(IIN(I))
        WRITE(*,'(13X,''The average of the entries amounts to '',
     +    G11.4,'' +- '',G11.4,/,13X,
     +    ''The fraction inside the histo bounds: '',G11.4)')WEISUM/
     +    FLOAT(IIN(I)+IUNDER(I)+IOVER(I)),Z,WEISUM/(WEISUM+YOUT(I))
        IF(IUNDER(I).GE.1) WRITE(*,60)'minimum',XMIN(I)
        IF( IOVER(I).GE.1) WRITE(*,60)'maximum',XMAX(I)
   60   FORMAT(12X,' The ',A,' value that occurred was   ',G11.4)
      ELSEIF(ISTAT .EQ. 3 .OR. ISTAT .EQ. 4) THEN
        IF(YMAX(I) .LE. 0D0) RETURN
        FACTOR = NRBINS(I)/(BOUND1(I)-BOUND0(I))
        IF(ISTAT .EQ. 4) FACTOR=FACTOR/FLOAT(IIN(I)+IUNDER(I)+IOVER(I))
        IF(LINLOG .EQ. 1) THEN
          WRITE(*,110) BOUND0(I),BOUND1(I),1.1D0*YMAX(I)*FACTOR
  110     FORMAT('*B',/,'VH 3.0',/,'LX 14.0',/,'LY 14.0',/,
     +       'XM ',D12.4,2X,D12.4,' 10',/,'YM 0. ',D12.4,' 10',/,'//')
        ELSE
          Z = YMAX(I)*FACTOR
          DO 120 J1 = 2 , NRBINS(I)
            IF(FACTOR*Y(I,J1).LT.Z.AND.Y(I,J1).GT.0D0) Z=Y(I,J1)*FACTOR
  120     CONTINUE
          WEISUM = .8D0*Z
          WRITE(*,130) BOUND0(I),BOUND1(I)
  130     FORMAT('*B',/,'VH 3.0',/,'LX 14.0',/,'LY 14.0',/,'XM ',D12.4,
     +      2X,D12.4,' 10',/,'YL ',/,'//')
        ENDIF
        WRITE(*,*)' ',TITLE
        WRITE(*,'(///,''//'',/,''*P'',/,''SN -1'',/,''CL'')')
        IF(Y(I,1).GT.0D0 .OR. LINLOG.EQ.1)
     +    WRITE(*,150) BOUND0(I),Y(I,1)*FACTOR
        DO 140 J1 = 1 , NRBINS(I)-1
          Z = BOUND0(I) + J1/FLOAT(NRBINS(I))*(BOUND1(I)-BOUND0(I))
          IF((Y(I,J1).GT.0D0.AND.Y(I,J1+1).GT.0D0).OR.LINLOG.EQ.1)THEN
            WRITE(*,150) Z,Y(I,J1  )*FACTOR
            WRITE(*,150) Z,Y(I,J1+1)*FACTOR
          ELSEIF(Y(I,J1).GT.0D0) THEN
            WRITE(*,150) Z,Y(I,J1)*FACTOR
            WRITE(*,150) Z, WEISUM
            WRITE(*,'(''/'')')
          ELSEIF(Y(I,J1+1).GT.0D0) THEN
            WRITE(*,150) Z, WEISUM
            WRITE(*,150) Z,Y(I,J1+1)*FACTOR
          ENDIF
  140   CONTINUE
        J1 = NRBINS(I)
        IF(Y(I,J1).GT.0D0 .OR. LINLOG.EQ.1)
     +    WRITE(*,150) BOUND1(I),Y(I,J1)*FACTOR
  150   FORMAT(' ',2G12.4)
        WRITE(*,'(''//'',/,''*E'')')
        DO 200 J1 = 1 , N
          YMAX(J1) = X * YMAX(J1)
          DO 190 J2 = 1 , NXMAX
            Y     (J1,J2) = X    * Y     (J1,J2)
            YSQUAR(J1,J2) = X**2 * YSQUAR(J1,J2)
  190     CONTINUE
  200   CONTINUE
      ENDIF
      END
*
      FUNCTION RN(IDUMMY)
      REAL*8 RN,RAN
      SAVE INIT
      DATA INIT /1/
      IF (INIT.EQ.1) THEN
        INIT=0
        CALL RMARIN(1802,9373)
      END IF
*
  10  CALL RANMAR(RAN)
      IF (RAN.LT.1D-16) GOTO 10
      RN=RAN
*
      END
*
      SUBROUTINE RANMAR(RVEC)
*     -----------------
* Universal random number generator proposed by Marsaglia and Zaman
* in report FSU-SCRI-87-50
* In this version RVEC is a double precision variable.
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/ RASET1 / RANU(97),RANC,RANCD,RANCM
      COMMON/ RASET2 / IRANMR,JRANMR
      SAVE /RASET1/,/RASET2/
      UNI = RANU(IRANMR) - RANU(JRANMR)
      IF(UNI .LT. 0D0) UNI = UNI + 1D0
      RANU(IRANMR) = UNI
      IRANMR = IRANMR - 1
      JRANMR = JRANMR - 1
      IF(IRANMR .EQ. 0) IRANMR = 97
      IF(JRANMR .EQ. 0) JRANMR = 97
      RANC = RANC - RANCD
      IF(RANC .LT. 0D0) RANC = RANC + RANCM
      UNI = UNI - RANC
      IF(UNI .LT. 0D0) UNI = UNI + 1D0
      RVEC = UNI
      END
 
      SUBROUTINE RMARIN(IJ,KL)
*     -----------------
* Initializing routine for RANMAR, must be called before generating
* any pseudorandom numbers with RANMAR. The input values should be in
* the ranges 0<=ij<=31328 ; 0<=kl<=30081
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/ RASET1 / RANU(97),RANC,RANCD,RANCM
      COMMON/ RASET2 / IRANMR,JRANMR
      SAVE /RASET1/,/RASET2/
* This shows correspondence between the simplified input seeds IJ, KL
* and the original Marsaglia-Zaman seeds I,J,K,L.
* To get the standard values in the Marsaglia-Zaman paper (i=12,j=34
* k=56,l=78) put ij=1802, kl=9373
      I = MOD( IJ/177 , 177 ) + 2
      J = MOD( IJ     , 177 ) + 2
      K = MOD( KL/169 , 178 ) + 1
      L = MOD( KL     , 169 )
      DO 300 II = 1 , 97
        S =  0D0
        T = .5D0
        DO 200 JJ = 1 , 24
          M = MOD( MOD(I*J,179)*K , 179 )
          I = J
          J = K
          K = M
          L = MOD( 53*L+1 , 169 )
          IF(MOD(L*M,64) .GE. 32) S = S + T
          T = .5D0*T
  200   CONTINUE
        RANU(II) = S
  300 CONTINUE
      RANC  =   362436D0 / 16777216D0
      RANCD =  7654321D0 / 16777216D0
      RANCM = 16777213D0 / 16777216D0
      IRANMR = 97
      JRANMR = 33
      END
*
      subroutine impsam(y,weight,wgt,ipass)
      implicit real*8 (a-h,o-z)
      real*8 y(16),am(100),pw(4),ps(4,100)
      common /genera/ w,rmw,rgw,rmz,rgz,sw2,gw,gz,njet,nf,ivec,idecay,
     .                iwhich,imannr,ippbar,ihelrn,iqcdds,imq
      common /struc/  x1,x2,q,qcdsca(6),itry(6),iuse
      common /mom/    plab(4,10),what
      common /cutval/ ptminj,ptminl,ptmis,etamaj,etamal,
     .                delrjj,delrjl,ptvcut
      common /binvar/ pt(10),eta(10),dr(10,10),ptel,ptnu,etael1,etael2,
     .                anglp,angnp,ptvec,pzvec
      common /imppar/ alpha,etmaxi,is,iyes
      save init,gevnb,/binvar/,/mom/
      data init,gevnb/1,389385.73d0/

      if (init.eq.1) then
         write(*,*) 'important sampling phase space version 2.0 
     .(8/17/92)'
         if (is.eq.1) 
     .write(*,*) 'important sampling in Et^-',alpha
         if (is.eq.2)
     .write(*,*) 'important sampling in EXP(-',alpha,'Et)/Et'
         init=0
         twopi=2.d0*3.1415926
         fac=twopi**(4.d0-3.d0*(njet+2.d0))*gevnb
         do 5 i=1,100
            am(i)=0.d0
 5       continue
         rnwid=10.d0
         rmv=rmw
         if (ivec.eq.2) rmv=rmz
         rgv=rgw
         if (ivec.eq.2) rgv=rgz
         x1=(rmv+rnwid*rgv)**2 - rmv**2
         x0=(rmv-rnwid*rgv)**2 - rmv**2
         a=rmv*rgv
         y1=datan(x1/a)/a
         y0=datan(x0/a)/a
         wty=y1-y0
      end if
*
      ipass=0
      weight=0d0
* generate a breit wigner vector boson mass
      yw=wty*rn(1)+y0
      xx=a*dtan(a*yw)
      rmsq=xx+rmv**2
      rm=dsqrt(rmsq)
      wtbw=wty*(xx**2+a**2)
* generate jet momenta
      wtjet=1d0
      etmin=ptminj
      if (etmaxi.le.etmin) then
         etmax=w/2d0-rm
      else
         etmax=etmaxi
      endif
      plab(1,8)=0d0
      plab(2,8)=0d0
      etot=0d0
      pltot=0d0
      omalp=1d0-alpha
      if (abs(omalp).lt.1d-1) then 
         ialpha=1
      else
         ialpha=0
         e1=etmin**omalp
         e2=etmax**omalp
      endif
      do i=1,njet
         r1=y(       i)
         r2=y(  njet+i)
         r3=y(2*njet+i)
         if (is.eq.1) then
            if (ialpha.eq.1) then
               etj=(etmax)**r1*(etmin)**(1d0-r1)
               wtet=dlog(etmax/etmin)*etj**2/2d0
            else
               etj=(r1*e2+(1d0-r1)*e1)**(1d0/omalp)
               wtet=(e2-e1)/omalp*etj**(1d0+alpha)/2d0
            endif
         endif
         if (is.eq.2) then
            exmin=dexp(-alpha*etmin)
            exmax=dexp(-alpha*etmax)
            etj=-dlog((1d0-r1)*exmin+r1*exmax)/alpha
            wtet=(exmin-exmax)/alpha/2d0*etj*dexp(alpha*etj)
*            write(*,*) etj,wtet
         endif
 
         rap=etamaj*(2d0*r2-1d0)
         wtrap=2d0*etamaj
         theta=twopi*r3
         wtaz=twopi
         pt(i)=etj
         eta(i)=rap
         plab(1,i+2)=etj*cos(theta)
         plab(2,i+2)=etj*sin(theta)
         plab(3,i+2)=etj*sinh(rap)
         plab(4,i+2)=etj*cosh(rap)
         plab(1,8)=plab(1,8)-plab(1,i+2)
         plab(2,8)=plab(2,8)-plab(2,i+2)
         pltot=pltot+plab(3,i+2)
         etot=etot+plab(4,i+2)
         wtjet=wtjet*wtet*wtrap*wtaz
         if (etmaxi.le.etmin) then
            etmax=etmax-(pt(i)-ptminj)
            if (ialpha.eq.0) e2=etmax**omalp
         endif
      enddo
* calculate w momentum & the (x1,x2) parton fractions
      r1=y(3*njet+1)
      if (njet.eq.0) then
         wh2=rm**2
         srat=wh2/w**2
         x1=srat**r1
         x2=srat**(1-r1)
         wtw=-dlog(srat)*x1*x2/wh2
         plab(1,8)=0d0
         plab(2,8)=0d0
         plab(3,8)=(x1-x2)*w/2d0
         plab(4,8)=(x1+x2)*w/2d0
      else
         ptw=sqrt(plab(1,8)**2+plab(2,8)**2)
         etamaw=dlog(w/ptw)
         etaw=etamaw*(2d0*r1-1d0)
         plab(3,8)=ptw*sinh(etaw)
         pltot=pltot+plab(3,8)
         ew=sqrt(rm**2+plab(1,8)**2+plab(2,8)**2+plab(3,8)**2)
         plab(4,8)=ew
         etot=etot+plab(4,8)
         x1=(etot+pltot)/w
         if ((x1.lt.0d0).or.(x1.gt.1d0)) return
         x2=(etot-pltot)/w
         if ((x2.lt.0d0).or.(x2.gt.1d0)) return
         wtw=(ptw*cosh(etaw)/ew)*2d0*etamaw/w**2
      endif
* construct initial state vectors
      e1=x1*w/2d0
      plab(1,1)=0d0
      plab(2,1)=0d0
      plab(3,1)=e1
      plab(4,1)=e1
      e2=x2*w/2d0
      plab(1,2)=0d0
      plab(2,2)=0d0
      plab(3,2)=-e2
      plab(4,2)=e2
      what=sqrt(x1*x2)*w
* generate vectorboson decay products
      do i=1,4
         pw(i)=plab(i,8)
      enddo
      call ramboo(2,rm,pw,am,ps,wtenu)
      do i=1,4
         plab(i, 9)=ps(i,1)
         plab(i,10)=ps(i,2)
      enddo
* check event against cuts
      call cut(ipass)
      if (ipass.eq.0) return
* calculate the dynamical scale
      etot=0.d0
      etmax=0.d0
      do j=1,njet
         if(pt(j).gt.etmax) etmax = pt(j)
         etot=etot+pt(j)
      enddo
      if (iqcdds.eq.1) q=etot/float(njet)
      if (iqcdds.eq.2) q=what
      if (iqcdds.eq.3) q=avemij(plab,njet)
      if (iqcdds.eq.4) q=rmv
      if (iqcdds.eq.5) q=etmax
* flux factor
      flux=.5d0/what**2
* calculate event weight and exit
      wtmc=fac*flux*wtbw*wtjet*wtw*wtenu*wgt
      weight=wzmat(wtmc)
*
      end
*
