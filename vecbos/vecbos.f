************************************************************************
* VECBOS   determines cross sections: p p(bar) -> W,Z + 0,1,2,3,4 jets *
*          by means of a simple Monte Carlo over the phase space.      *
* Options: Collider type, Dynamic QCD scales, Structure functions      *
* Output:  Total cross section according to process types.             *
*          Various distributions for Ecm, Pt, Mij                      *
************************************************************************
* You can use the matrix elements separately from the VECBOS shell.    *
* See FUNCTION WZMAT(..) how to remove all unnecessary routines.       *
************************************************************************
* VECBOS VERSION 3.0                                                   *
* E-mail address: giele@fnal.gov                                       *
************************************************************************
      PROGRAM VECBOS
      IMPLICIT REAL*8 (A-H,O-Z)
      COMMON /BVEG1/  XL(16),XU(16),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON /EFFIC/  NIN,NOUT
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /INTER/  ITMX1,NCALL1,IMANN1,ITMX2,NCALL2,IMANN2
      COMMON /PROCES/ ISUB(3),ISTAT(3)
      COMMON /CUTVAL/ PTMINJ,PTMINL,PTMIS,ETAMAJ,ETAMAL,
     .                DELRJJ,DELRJL,PTVCUT
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /HISSCA/ CMMAX,PTMAX,PTELMA,PWEIGH,ANLEPR,PTVMAX,PZVMAX,
     .                ICM,IPT,IPTEL,IWEIGH,IANLEP,IPTVEC,IPZVEC
      COMMON /MCRES/  AVGI1,SD1,CHI2A1,AVGI2,SD2,CHI2A2
      common /imppar/ alpha,etmaxi,is,iyes
*
      DIMENSION COLLID(30,4)
      EXTERNAL FXN
*
*     DATA   COLLID  ( SPS, TEVATRON, LHC, SSC )
*    . / PPBAR , ECM   , PTVCUT, PTMINJ, PTMINL,  PTMIS,
*    .   ETAMAJ, ETAMAL, DELRJJ, DELRJL, ??????, ??????,
*    .    CMMAX,    ICM, PTMAXJ,    IPT, PTELMA,  IPTEL,
*    .  PWEIGHT,IWEIGHT, ANLEPR, IANLEP, PTVMAX, IPTVEC,
*    .   PZVMAX, IPZVEC, ??????, ??????, ??????,  ?????/
*
      DATA   COLLID /1D0,  630D0,    0D0,   10D0,   15D0,   15D0,
     .    2.5D0,  2.5D0,  1.0D0,    0D0,    0D0,    0D0,
     .    500D0,   25D0,  200D0,   20D0,  125D0,   25D0,
     .     10D0,   20D0,    1D0,   20D0,  100D0,   20D0,
     .    500D0,   20D0,    0D0,    0D0,    0D0,    0D0,
*
     .      1D0, 1800D0,    0D0,   15D0,   25D0,   25D0,
     .    2.0D0,  3.0D0,  0.5D0,  0.0D0,    0D0,    0D0,
     .    800D0,   40D0,  100D0,   20D0,  125D0,   25D0,
     .     10D0,   20D0,    1D0,   20D0,  100D0,   20D0,
     .    500D0,   20D0,    0D0,    0D0,    0D0,    0D0,
*
     .      0D0,   16D3,    0D0,   50D0,   50D0,   50D0,
     .    3.0D0,  3.0D0,  0.4D0,  0.4D0,    0D0,    0D0,
     .   2000D0,   20D0,  400D0,   20D0,  500D0,   25D0,
     .     10D0,   20D0,    1D0,   20D0,  100D0,   20D0,
     .   1000D0,   20D0,    0D0,    0D0,    0D0,    0D0,
*
     .      0D0,   40D3,    0D0,   50D0,   50D0,   50D0,
     .    3.0D0,  3.0D0,  0.4D0,  0.4D0,    0D0,    0D0,
     .   2000D0,   20D0,  400D0,   20D0,  500D0,   25D0,
     .     10D0,   20D0,    1D0,   20D0,  100D0,   20D0,
     .   1000D0,   20D0,    0D0,    0D0,    0D0,    0D0 /
*
* Input parameters
* Number of final state jets (0, 1, 2, 3 or 4)
      NJET=4
* What vector boson: 1=W, 2=Z
      IVEC=1
* What decay mode: W:1,2= el nu  Z:1=e+e-   2=neutrinos
      IDECAY=1
* Which vector boson in case of W: 1=W+, 2=W-, 3=both
      IWHICH=3
* Mass of W
      RMW=80.14D0
* Width of W
      RGW=2.1D0
* Mass of Z
      RMZ=91.175D0
* Width of Z
      RGZ=2.55D0
* Sin2 of the weak angle
      SW2=0.225D0
* Weak coupling definition.
      GW=SQRT(4.D0*3.141592654/(127.8*8.0*SW2))
      GZ=SQRT(4.D0*3.141592654/(127.8D0*SW2*(1.D0-SW2)*4.D0))
* Set of structure function
* Type of structure function to use in VEGAS integration:
* istruc=0  -> test        x*all(x) = x*(1-x)
* istruc=1  -> mrse        (msbar), Lambda = 0.1  GeV
* istruc=2  -> mrsb        (msbar), Lambda = 0.2  GeV
* istruc=3  -> kmrshb      (msbar), Lambda = 0.19 GeV
* istruc=4  -> kmrsb0      (msbar), Lambda = 0.19 GeV
* istruc=5  -> kmrsb-      (msbar), Lambda = 0.19 GeV
* istruc=6  -> kmrsh- (5)  (msbar), Lambda = 0.19 GeV
* istruc=7  -> kmrsh- (2)  (msbar), Lambda = 0.19 GeV
* istruc=8  -> mts1        (msbar), Lambda = 0.2  GeV
* istruc=9  -> mte1        (msbar), Lambda = 0.2  GeV
* istruc=10 -> mtb1        (msbar), Lambda = 0.2  GeV
* istruc=11 -> mtb2        (msbar), Lambda = 0.2  GeV
* istruc=12 -> mtsn1       (msbar), Lambda = 0.2  GeV
* istruc=13 -> kmrss0      (msbar), Lambda = 0.215 GeV
* istruc=14 -> kmrsd0      (msbar), Lambda = 0.215 GeV
* istruc=15 -> kmrsd-      (msbar), Lambda = 0.215 GeV
* istruc=16 -> mrss0       (msbar), Lambda = 0.230 GeV
* istruc=17 -> mrsd0       (msbar), Lambda = 0.230 GeV
* istruc=18 -> mrsd-       (msbar), Lambda = 0.230 GeV
* istruc=19 -> cteq1l      (msbar), Lambda = 0.168 GeV
* istruc=20 -> cteq1m      (msbar), Lambda = 0.231 GeV
* istruc=21 -> cteq1ml     (msbar), Lambda = 0.322 GeV
* istruc=22 -> cteq1ms     (msbar), Lambda = 0.231 GeV
* istruc=23 -> grv         (msbar), Lambda = 0.200 GeV
* istruc=24 -> cteq2l      (msbar), Lambda = 0.190 GeV
* istruc=25 -> cteq2m      (msbar), Lambda = 0.213 GeV
* istruc=26 -> cteq2ml     (msbar), Lambda = 0.322 GeV
* istruc=27 -> cteq2ms     (msbar), Lambda = 0.208 GeV
* istruc=28 -> cteq2mf     (msbar), Lambda = 0.208 GeV
* istruc=29 -> mrsh        (msbar), Lambda = 0.230 GeV
* istruc=30 -> mrsa        (msbar), Lambda = 0.230 GeV
* istruc=31 -> mrsg        (msbar), Lambda = 0.255 GeV
* istruc=32 -> mrsap       (msbar), Lambda = 0.231 GeV
* istruc=33 -> grv94       (msbar), Lambda = 0.200 GeV
* istruc=34 -> cteq3l      (msbar), Lambda = 0.177 GeV
* istruc=35 -> cteq3m      (msbar), Lambda = 0.239 GeV
* istruc=36 -> mrsj        (msbar), Lambda = 0.366 GeV
* istruc=37 -> mrsjp       (msbar), Lambda = 0.542 GeV
* istruc=38 -> cteqjet     (msbar), Lambda = 0.286 GeV
* istruc=39 -> mrsr1       (msbar), Lambda = 0.241 GeV
* istruc=40 -> mrsr2       (msbar), Lambda = 0.344 GeV
* istruc=41 -> mrsr3       (msbar), Lambda = 0.241 GeV 
* istruc=42 -> mrsr4       (msbar), Lambda = 0.344 GeV 
* istruc=43 -> cteq4l      (msbar), Lambda = 0.300 GeV
* istruc=44 -> cteq4m      (msbar), Lambda = 0.300 GeV
* istruc=45 -> cteq4hj     (msbar), Lambda = 0.300 GeV
* istruc=46 -> cteq4lq     (msbar), Lambda = 0.300 GeV
*
* extra structure functions with variable as/Lambda
*
* istruc=101 -> mrsap       (msbar), as(Mz) = 0.110 -> Lambda = 0.216
* istruc=102 -> mrsap       (msbar), as(Mz) = 0.115 -> Lambda = 0.284
* istruc=103 -> mrsap       (msbar), as(Mz) = 0.120 -> Lambda = 0.366
* istruc=104 -> mrsap       (msbar), as(Mz) = 0.125 -> Lambda = 0.458
* istruc=105 -> mrsap       (msbar), as(Mz) = 0.130 -> Lambda = 0.564
* istruc=111 -> grv94       (msbar), Lambda = 0.150 GeV
* istruc=112 -> grv94       (msbar), Lambda = 0.200 GeV = istruc33
* istruc=113 -> grv94       (msbar), Lambda = 0.250 GeV
* istruc=114 -> grv94       (msbar), Lambda = 0.300 GeV
* istruc=115 -> grv94       (msbar), Lambda = 0.350 GeV
* istruc=116 -> grv94       (msbar), Lambda = 0.400 GeV
* istruc=121 -> cteq3m      (msbar), Lambda = 0.100 GeV
* istruc=122 -> cteq3m      (msbar), Lambda = 0.120 GeV
* istruc=123 -> cteq3m      (msbar), Lambda = 0.140 GeV
* istruc=124 -> cteq3m      (msbar), Lambda = 0.180 GeV
* istruc=125 -> cteq3m      (msbar), Lambda = 0.200 GeV
* istruc=126 -> cteq3m      (msbar), Lambda = 0.220 GeV
* istruc=127 -> cteq3m      (msbar), Lambda = 0.260 GeV
* istruc=128 -> cteq3m      (msbar), Lambda = 0.300 GeV
* istruc=129 -> cteq3m      (msbar), Lambda = 0.340 GeV
* istruc=130 -> cteq4m      (msbar), Lambda = 0.215 GeV
* istruc=131 -> cteq4m      (msbar), Lambda = 0.255 GeV
* istruc=132 -> cteq4m      (msbar), Lambda = 0.300 GeV = istruc44
* istruc=133 -> cteq4m      (msbar), Lambda = 0.348 GeV
* istruc=134 -> cteq4m      (msbar), Lambda = 0.401 GeV
*
*
* select up to 6 PDF's in iuse1,...,iuse6. If set to 0 no PDF is choosen.
* set the variable iuse between 1 and 6 depending which PDF you want to
* use in the integration (iuse=# means use iuse# for integration).
      IUSE1=30
      IUSE2=101
      IUSE3=102
      IUSE4=103
      IUSE5=104
      IUSE6=105
      IUSE=1
* Collider, 1= CERN, 2= Fermilab, 3= LHC, 4= SSC
      ICOLLI=2
* MC over helicities, effects: Q2
      IHELRN=0
* Set up initializing and final integration parameters
* Matrix element to use: 1: Exact, 2: M^2=1, 3: M^2=1/SHAT**NJET
      ITMX1=5
      NCALL1=1000
      IMANN1=1
      ITMX2=5
      NCALL2=10000
      IMANN2=1
* Which subprocesses to take along, 2, 4 or 6 quarks.
      ISUB(1)=1
      ISUB(2)=1
      ISUB(3)=1
      ISTAT(1)=0
      ISTAT(2)=0
      ISTAT(3)=0
* To have a massless B Bbar pair in the final state set IMQ=1
      IMQ=0
* Dynamical scale: 1. PT-average  2: Total invariant mass
*                  3. Average invariant mass of two jets
*                  4. Mass of vector boson  5: Ptmax in the event
      IQCDDS=4
*
* Error trapping
*
      IF ((NJET.LT.2).AND.((ISUB(2).EQ.1).OR.(ISUB(3).EQ.1))) THEN
        WRITE(*,*) '  Nr. jets < 2 has no processes with Nr. Qs >= 4 '
        STOP
      END IF
      IF ((NJET.LT.4).AND.(ISUB(3).EQ.1)) THEN
        WRITE(*,*) '  Nr. jets < 4 has no processes with Nr. Qs = 6 '
        STOP
      END IF
      IF ((IQCDDS.EQ.3).AND.(NJET.LT.2)) THEN
        WRITE(*,*) '  No invariant masses of jets in final state! '
        STOP
      END IF
      IF ((IVEC.EQ.2).AND.(NJET.EQ.4)) THEN
        WRITE(*,*) '  Z and 4 jets not yet available! '
        STOP
      END IF
      IF ((IMANN1.GT.3).OR.(IMANN2.GT.3)) THEN
        WRITE(*,*) '  Illegal method of integration! '
        STOP
      END IF
      IF ((IMQ.EQ.1).AND.(ISUB(1).EQ.1)) THEN
        WRITE(*,*) '  No quark pair produced in the 2 q subprocess! '
        STOP
      END IF
* Number of final state flavours (also used in alphas)
      NF=5
* Which structure function should be examined besides the one used
* for the Monte Carlo integration
      ITRY(1)=IUSE1
      ITRY(2)=IUSE2
      ITRY(3)=IUSE3
      ITRY(4)=IUSE4
      ITRY(5)=IUSE5
      ITRY(6)=IUSE6
* Structure functions scales:
        DO I=1,6
           ISTRUC=ITRY(I)
           if (istruc.eq.0) qcdl=0D0
           if (istruc.eq.1) qcdl=0.1d0
           if (istruc.eq.2) qcdl=0.2d0
           if (istruc.eq.3) qcdl=0.19d0
           if (istruc.eq.4) qcdl=0.19d0
           if (istruc.eq.5) qcdl=0.19d0
           if (istruc.eq.6) qcdl=0.19d0
           if (istruc.eq.7) qcdl=0.19d0
           if (istruc.eq.8) qcdl=0.144d0
           if (istruc.eq.9) qcdl=0.155d0
           if (istruc.eq.10) qcdl=0.194d0
           if (istruc.eq.11) qcdl=0.191d0
           if (istruc.eq.12) qcdl=0.237d0
           if (istruc.eq.13) qcdl=0.215d0
           if (istruc.eq.14) qcdl=0.215d0
           if (istruc.eq.15) qcdl=0.215d0
           if (istruc.eq.16) qcdl=0.230d0
           if (istruc.eq.17) qcdl=0.230d0
           if (istruc.eq.18) qcdl=0.230d0
           if (istruc.eq.19) qcdl=0.168d0
           if (istruc.eq.20) qcdl=0.231d0
           if (istruc.eq.21) qcdl=0.322d0
           if (istruc.eq.22) qcdl=0.231d0
           if (istruc.eq.23) qcdl=0.200d0
           if (istruc.eq.24) qcdl=0.190d0
           if (istruc.eq.25) qcdl=0.213d0
           if (istruc.eq.26) qcdl=0.322d0
           if (istruc.eq.27) qcdl=0.208d0
           if (istruc.eq.28) qcdl=0.208d0
           if (istruc.eq.29) qcdl=0.230d0
           if (istruc.eq.30) qcdl=0.230d0
           if (istruc.eq.31) qcdl=0.255d0
           if (istruc.eq.32) qcdl=0.231d0
           if (istruc.eq.33) qcdl=0.200d0
           if (istruc.eq.34) qcdl=0.177d0
           if (istruc.eq.35) qcdl=0.239d0
           if (istruc.eq.36) qcdl=0.366d0
           if (istruc.eq.37) qcdl=0.542d0
           if (istruc.eq.38) qcdl=0.286d0
           if (istruc.eq.39) qcdl=0.241d0
           if (istruc.eq.40) qcdl=0.344d0
           if (istruc.eq.41) qcdl=0.241d0
           if (istruc.eq.42) qcdl=0.344d0
           if (istruc.eq.43) qcdl=0.300d0
           if (istruc.eq.44) qcdl=0.300d0
           if (istruc.eq.45) qcdl=0.300d0
           if (istruc.eq.46) qcdl=0.300d0
           if (istruc.eq.101) qcdl=0.216d0
           if (istruc.eq.102) qcdl=0.284d0
           if (istruc.eq.103) qcdl=0.366d0
           if (istruc.eq.104) qcdl=0.458d0
           if (istruc.eq.105) qcdl=0.564d0
           if (istruc.eq.111) qcdl=0.150d0
           if (istruc.eq.112) qcdl=0.200d0
           if (istruc.eq.113) qcdl=0.250d0
           if (istruc.eq.114) qcdl=0.300d0
           if (istruc.eq.115) qcdl=0.350d0
           if (istruc.eq.116) qcdl=0.400d0
           if (istruc.eq.121) qcdl=0.100d0
           if (istruc.eq.122) qcdl=0.120d0
           if (istruc.eq.123) qcdl=0.140d0
           if (istruc.eq.124) qcdl=0.180d0
           if (istruc.eq.125) qcdl=0.200d0
           if (istruc.eq.126) qcdl=0.220d0
           if (istruc.eq.127) qcdl=0.260d0
           if (istruc.eq.128) qcdl=0.300d0
           if (istruc.eq.129) qcdl=0.340d0
           if (istruc.eq.130) qcdl=0.215d0
           if (istruc.eq.131) qcdl=0.255d0
           if (istruc.eq.132) qcdl=0.300d0
           if (istruc.eq.133) qcdl=0.348d0
           if (istruc.eq.134) qcdl=0.401d0
           QCDSCA(I)=QCDL
        ENDDO
* Pick up Monte Carlo parameters.
      IPPBAR=INT(COLLID(1,ICOLLI))
      W=COLLID(2,ICOLLI)
      PTVCUT=COLLID( 3,ICOLLI)
      PTMINJ=COLLID( 4,ICOLLI)
      PTMINL=COLLID( 5,ICOLLI)
      PTMIS =COLLID( 6,ICOLLI)
      ETAMAJ=COLLID( 7,ICOLLI)
      ETAMAL=COLLID( 8,ICOLLI)
      DELRJJ=COLLID( 9,ICOLLI)
      DELRJL=COLLID(10,ICOLLI)
* Histo information
      CMMAX=COLLID(13,ICOLLI)
      ICM=INT(COLLID(14,ICOLLI))
      PTMAX=COLLID(15,ICOLLI)
      IPT=INT(COLLID(16,ICOLLI))
      PTELMA=COLLID(17,ICOLLI)
      IPTEL=INT(COLLID(18,ICOLLI))
      PWEIGH=COLLID(19,ICOLLI)
      IWEIGH=INT(COLLID(20,ICOLLI))
* Angle charged lepton,proton (in case of W)
      ANLEPR=COLLID(21,ICOLLI)
      IANLEP=INT(COLLID(22,ICOLLI))
* Vector boson Pt and Pz
      PTVMAX=COLLID(23,ICOLLI)
      IPTVEC=INT(COLLID(24,ICOLLI))
      PZVMAX=COLLID(25,ICOLLI)
      IPZVEC=INT(COLLID(26,ICOLLI))
*
* Issue message about start of program
      WRITE(*,*)
     .' VECBOS is a Monte Carlo for W,Z + jets.'
      WRITE(*,*)
     .  ' Current Version of VECBOS: 3.0. '
      WRITE(*,*)
     .  ' E-MAIL address giele@fnal.gov  '
      WRITE(*,*)
*
* Vegas global parameters
*
* if iyes=1 : generate jet transverse energies according to a function 
*             determined by is
*             is=1 : E_t^(-alpha) (alpha ~ 2) 
*             is=2 : Exp(-alpha*Et)/Et (alpha ~0.02)
*             with maximum transverse energy etmaxi, if etmaxi < etmin 
*             then maximum transverse energy is the kinematical allowed 
*             limit.
* if iyes=0 : use RAMBO (is identical to original VECBOS)
* 
      alpha=0.019d0
      etmaxi=0d0
      is=2
      iyes=0
      if (iyes.eq.1) then
         NDIM=3*njet+1
      else
         ndim=2
      endif
      if (ndim.gt.16) then
         write(*,*) 'disabled'
         stop
      endif
      DO 16 I=1,ndim
        XL(I)=0.D0
        XU(I)=1.D0
   16 CONTINUE
      ACC=-1.D0
      NPRN=1
*
* If ITMX1=0 then no initialisation
*
      IF (ITMX1.GT.0) THEN
* Init van der marcks histo routine
        CALL HISTO(1,0,0D0,0D0,0D0,0D0,0,' ',6,0)
        CALL CLRSTS
*
* Initialize grid
        ITMX=ITMX1
        NCALL=NCALL1
        IMANNR=IMANN1
        CALL VEGAS(FXN,AVGI1,SD1,CHI2A1)
      END IF
*
* Final itegrations
* Start counting statistics
      NIN=0
      NOUT=0
* Init van der marcks histo routine
      CALL HISTO(1,0,0D0,0D0,0D0,0D0,0,' ',6,0)
      ITMX=ITMX2
      NCALL=NCALL2
      IMANNR=IMANN2
*
* Clear statistics arrays
      CALL CLRSTS
*
      IF (ITMX1.GT.0) THEN
         CALL VEGAS1(FXN,AVGI2,SD2,CHI2A2)
      ELSE
         CALL VEGAS(FXN,AVGI2,SD2,CHI2A2)
      END IF
*
* Plot extensive output files and standard normal distributions
*
      CALL STAT(ISUB,ISTAT)
*
      END
