************************************************************************
* MEQ6W calls the various r sb t tb u ub subprocesses.                 *
************************************************************************
* Table of subprocesses T(2,1:4,6): with first index = W+, W-          *
* Second index:  1 = U DB     2 = C SB    3 = UB D    4 = CB S         *
************************************************************************
* Table of subprocesses T(2,5:9,6):   with first index = W+, W-        *
* Second index:  5 = GG   6= GQ + GQB  7= QG + QBG 8= QQ + QBQB 9=QQB  *
************************************************************************
      SUBROUTINE MEQ6W(TOT,T)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /MOM/    PLAB(4,10),WHAT
      DIMENSION T(2,9,6),R(2,7,30)
      SAVE /MOM/
*
      IF (IMANNR.GT.1) THEN
        DO 4 I1=1,2
          DO 4 I2=1,7
            DO 4 I3=1,30
              R(I1,I2,I3)=1D0
              IF (IMANNR.EQ.3) R(I1,I2,I3)=1D0/WHAT**NJET
    4   CONTINUE
      END IF
*
* Clear statistics arrays
*
      DO 5 I=1,2
        DO 5 J=1,9
          DO 5 K=1,6
            T(I,J,K)=0.0
    5 CONTINUE
*
* Set Delta functions for having a B Bbar pair in the final state.
*
      IMNOBB=1-IMQ
      IMBB=1
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ6W(NJET-4,PLAB,1,2,3,4,5,6,R(1,1, 1),IHELRN)
        CALL RQ6COP(R(1,1,1),R(1,1,16))
        CALL FQ6W(NJET-4,PLAB,1,3,2,4,5,6,R(1,1, 2),IHELRN)
        CALL RQ6COP(R(1,1,2),R(1,1,9))
        CALL FQ6W(NJET-4,PLAB,3,1,2,4,5,6,R(1,1, 3),IHELRN)
        CALL RQ6COP(R(1,1,3),R(1,1,13))
        CALL FQ6W(NJET-4,PLAB,3,1,4,2,5,6,R(1,1, 4),IHELRN)
        CALL RQ6COP(R(1,1,4),R(1,1,7))
        CALL FQ6W(NJET-4,PLAB,3,4,1,5,2,6,R(1,1, 5),IHELRN)
        CALL RQ6COP(R(1,1,5),R(1,1,27))
        CALL FQ6W(NJET-4,PLAB,1,3,4,2,5,6,R(1,1, 6),IHELRN)
        CALL RQ6COP(R(1,1,6),R(1,1,8))
        CALL FQ6W(NJET-4,PLAB,3,4,5,1,2,6,R(1,1,10),IHELRN)
        CALL RQ6COP(R(1,1,10),R(1,1,25))
        CALL FQ6W(NJET-4,PLAB,3,4,1,2,5,6,R(1,1,11),IHELRN)
        CALL RQ6COP(R(1,1,11),R(1,1,29))
        CALL FQ6W(NJET-4,PLAB,3,4,5,1,6,2,R(1,1,12),IHELRN)
        CALL RQ6COP(R(1,1,12),R(1,1,20))
        CALL FQ6W(NJET-4,PLAB,3,4,5,6,1,2,R(1,1,14),IHELRN)
        CALL RQ6COP(R(1,1,14),R(1,1,26))
        CALL FQ6W(NJET-4,PLAB,3,4,1,5,6,2,R(1,1,15),IHELRN)
        CALL RQ6COP(R(1,1,15),R(1,1,30))
        CALL FQ6W(NJET-4,PLAB,2,3,1,4,5,6,R(1,1,17),IHELRN)
        CALL RQ6COP(R(1,1,17),R(1,1,24))
        CALL FQ6W(NJET-4,PLAB,3,2,1,4,5,6,R(1,1,18),IHELRN)
        CALL RQ6COP(R(1,1,18),R(1,1,28))
        CALL FQ6W(NJET-4,PLAB,3,2,4,1,5,6,R(1,1,19),IHELRN)
        CALL RQ6COP(R(1,1,19),R(1,1,22))
        CALL FQ6W(NJET-4,PLAB,2,3,4,1,5,6,R(1,1,21),IHELRN)
        CALL RQ6COP(R(1,1,21),R(1,1,23))
      END IF
*
      DO 10 I=1,6
        IF (ITRY(I).NE.0) THEN
          CALL QQWQ6(R,T,I,IMNOBB,IMBB)
          CALL QQBWQ6(R,T,I,IMNOBB,IMBB)
          CALL QBQWQ6(R,T,I,IMNOBB,IMBB)
          CALL QBBWQ6(R,T,I,IMNOBB,IMBB)
        END IF
   10 CONTINUE
*
      TOT=0.0
      IF (IWHICH.NE.2) TOT=TOT+T(1,1,IUSE)+T(1,2,IUSE)
      IF (IWHICH.NE.1) TOT=TOT+T(2,3,IUSE)+T(2,4,IUSE)
*
      END
*
* Q Q
      SUBROUTINE QQWQ6(R,T,I,IMNOBB,IMBB)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(2,9,6),R(2,7,30),C(848)
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      SAVE IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG,/STRUCF/
      DATA IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG
     .    / 1, 2, 3, 4, 5,  6,  7,  8,  9, 10,11/
*
* Q Q W- UD
          C(  1) = R(2,6, 4) * F(ID ,I) * G(IU ,I) / 54.0 * IMNOBB
          C(  2) = R(2,6,12) * F(IU ,I) * G(IU ,I) / 54.0 * IMNOBB
          C(  3) = R(2,5, 4) * F(ID ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(  4) = R(2,5, 9) * F(ID ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(  5) = R(2,3, 4) * F(ID ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(  6) = R(2,3, 9) * F(ID ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(  7) = R(2,3,12) * F(IU ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(  8) = R(2,3, 4) * F(ID ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(  9) = R(2,3, 9) * F(ID ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 10) = R(2,3,12) * F(IU ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 11) = R(2,3, 4) * F(ID ,I) * G(IU ,I) / 18.0 * IMBB
          C( 12) = R(2,7, 4) * F(ID ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 13) = R(2,4, 4) * F(ID ,I) * G(IC ,I) /  9.0 * IMNOBB
          C( 14) = R(2,4, 9) * F(ID ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 15) = R(2,4, 4) * F(ID ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 16) = R(2,4, 9) * F(ID ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 17) = R(2,4, 9) * F(ID ,I) * G(ID ,I) /  9.0 * IMBB
          C( 18) = R(2,2, 4) * F(ID ,I) * G(IC ,I) / 18.0 * IMNOBB
          C( 19) = R(2,2,12) * F(IC ,I) * G(IC ,I) / 18.0 * IMNOBB
          C( 20) = R(2,1, 4) * F(ID ,I) * G(IC ,I) /  9.0 * IMNOBB
          C( 21) = R(2,1, 9) * F(ID ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 22) = R(2,1,12) * F(IC ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 23) = R(2,1, 4) * F(ID ,I) * G(IC ,I) /  9.0 * IMBB
          C( 24) = R(2,2, 4) * F(ID ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 25) = R(2,2,12) * F(IS ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 26) = R(2,1, 4) * F(ID ,I) * G(IS ,I) /  9.0 * IMBB
          C( 27) = R(2,6,19) * F(IU ,I) * G(ID ,I) / 54.0 * IMNOBB
          C( 28) = R(2,5,19) * F(IU ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 29) = R(2,3,19) * F(IU ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 30) = R(2,3,24) * F(IC ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 31) = R(2,3,27) * F(IC ,I) * G(IU ,I) / 18.0 * IMNOBB
          C( 32) = R(2,3,19) * F(IU ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 33) = R(2,3,24) * F(IS ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 34) = R(2,3,27) * F(IS ,I) * G(IU ,I) / 18.0 * IMNOBB
          C( 35) = R(2,3,19) * F(IU ,I) * G(ID ,I) / 18.0 * IMBB
          C( 36) = R(2,4,19) * F(IC ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 37) = R(2,4,19) * F(IS ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 38) = R(2,2,19) * F(IC ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 39) = R(2,1,19) * F(IC ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 40) = R(2,1,24) * F(IS ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 41) = R(2,1,27) * F(IS ,I) * G(IC ,I) /  9.0 * IMNOBB
          C( 42) = R(2,1,19) * F(IC ,I) * G(ID ,I) /  9.0 * IMBB
          C( 43) = R(2,2,19) * F(IS ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 44) = R(2,1,19) * F(IS ,I) * G(ID ,I) /  9.0 * IMBB
          DO 110 J=1,44
            T(2,3,I)=T(2,3,I)+C(J)
            T(2,8,I)=T(2,8,I)+C(J)
  110     CONTINUE
* Q Q W- CS
          C( 45) = R(2,2, 4) * F(IS ,I) * G(IU ,I) / 18.0 * IMNOBB
          C( 46) = R(2,2,12) * F(IU ,I) * G(IU ,I) / 18.0 * IMNOBB
          C( 47) = R(2,1, 4) * F(IS ,I) * G(IU ,I) /  9.0 * IMNOBB
          C( 48) = R(2,1, 9) * F(IS ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 49) = R(2,1,12) * F(IU ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 50) = R(2,3, 4) * F(IS ,I) * G(IC ,I) / 18.0 * IMNOBB
          C( 51) = R(2,3, 9) * F(IS ,I) * G(IU ,I) / 18.0 * IMNOBB
          C( 52) = R(2,3,12) * F(IC ,I) * G(IU ,I) / 18.0 * IMNOBB
          C( 53) = R(2,4, 4) * F(IS ,I) * G(IU ,I) /  9.0 * IMNOBB
          C( 54) = R(2,4, 9) * F(IS ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 55) = R(2,1, 4) * F(IS ,I) * G(IU ,I) /  9.0 * IMBB
          C( 56) = R(2,2, 4) * F(IS ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 57) = R(2,2,12) * F(ID ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 58) = R(2,3, 4) * F(IS ,I) * G(IC ,I) / 18.0 * IMNOBB
          C( 59) = R(2,3, 9) * F(IS ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 60) = R(2,3,12) * F(IC ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 61) = R(2,4, 4) * F(IS ,I) * G(ID ,I) /  9.0 * IMNOBB
          C( 62) = R(2,4, 9) * F(IS ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 63) = R(2,1, 4) * F(IS ,I) * G(ID ,I) /  9.0 * IMBB
          C( 64) = R(2,6, 4) * F(IS ,I) * G(IC ,I) / 54.0 * IMNOBB
          C( 65) = R(2,6,12) * F(IC ,I) * G(IC ,I) / 54.0 * IMNOBB
          C( 66) = R(2,5, 4) * F(IS ,I) * G(IC ,I) / 18.0 * IMNOBB
          C( 67) = R(2,5, 9) * F(IS ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 68) = R(2,3, 4) * F(IS ,I) * G(IC ,I) / 18.0 * IMBB
          C( 69) = R(2,7, 4) * F(IS ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 70) = R(2,4, 9) * F(IS ,I) * G(IS ,I) /  9.0 * IMBB
          C( 71) = R(2,2,19) * F(IU ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 72) = R(2,1,19) * F(IU ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 73) = R(2,1,24) * F(ID ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 74) = R(2,1,27) * F(ID ,I) * G(IU ,I) /  9.0 * IMNOBB
          C( 75) = R(2,3,19) * F(IC ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 76) = R(2,3,24) * F(IU ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 77) = R(2,3,27) * F(IU ,I) * G(IC ,I) / 18.0 * IMNOBB
          C( 78) = R(2,4,19) * F(IU ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 79) = R(2,1,19) * F(IU ,I) * G(IS ,I) /  9.0 * IMBB
          C( 80) = R(2,2,19) * F(ID ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 81) = R(2,3,19) * F(IC ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 82) = R(2,3,24) * F(ID ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 83) = R(2,3,27) * F(ID ,I) * G(IC ,I) / 18.0 * IMNOBB
          C( 84) = R(2,4,19) * F(ID ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 85) = R(2,1,19) * F(ID ,I) * G(IS ,I) /  9.0 * IMBB
          C( 86) = R(2,6,19) * F(IC ,I) * G(IS ,I) / 54.0 * IMNOBB
          C( 87) = R(2,5,19) * F(IC ,I) * G(IS ,I) / 18.0 * IMNOBB
          C( 88) = R(2,3,19) * F(IC ,I) * G(IS ,I) / 18.0 * IMBB
          DO 120 J=45,88
            T(2,4,I)=T(2,4,I)+C(J)
            T(2,8,I)=T(2,8,I)+C(J)
  120     CONTINUE
* Q Q W+ UD
          C( 89) = R(1,7, 4) * F(IU ,I) * G(IU ,I) / 18.0 * IMNOBB
          C( 90) = R(1,5, 4) * F(IU ,I) * G(ID ,I) / 18.0 * IMNOBB
          C( 91) = R(1,5, 9) * F(IU ,I) * G(IU ,I) / 18.0 * IMNOBB
          C( 92) = R(1,4, 4) * F(IU ,I) * G(IC ,I) /  9.0 * IMNOBB
          C( 93) = R(1,4, 9) * F(IU ,I) * G(IU ,I) /  9.0 * IMNOBB
          C( 94) = R(1,4, 4) * F(IU ,I) * G(IS ,I) /  9.0 * IMNOBB
          C( 95) = R(1,4, 9) * F(IU ,I) * G(IU ,I) /  9.0 * IMNOBB
          C( 96) = R(1,4, 9) * F(IU ,I) * G(IU ,I) /  9.0 * IMBB
          C( 97) = R(1,6, 4) * F(IU ,I) * G(ID ,I) / 54.0 * IMNOBB
          C( 98) = R(1,6,12) * F(ID ,I) * G(ID ,I) / 54.0 * IMNOBB
          C( 99) = R(1,3, 4) * F(IU ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(100) = R(1,3, 9) * F(IU ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(101) = R(1,3,12) * F(ID ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(102) = R(1,3, 4) * F(IU ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(103) = R(1,3, 9) * F(IU ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(104) = R(1,3,12) * F(ID ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(105) = R(1,3, 4) * F(IU ,I) * G(ID ,I) / 18.0 * IMBB
          C(106) = R(1,2, 4) * F(IU ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(107) = R(1,2,12) * F(IC ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(108) = R(1,1, 4) * F(IU ,I) * G(IC ,I) /  9.0 * IMNOBB
          C(109) = R(1,1, 9) * F(IU ,I) * G(IS ,I) /  9.0 * IMNOBB
          C(110) = R(1,1,12) * F(IC ,I) * G(IS ,I) /  9.0 * IMNOBB
          C(111) = R(1,1, 4) * F(IU ,I) * G(IC ,I) /  9.0 * IMBB
          C(112) = R(1,2, 4) * F(IU ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(113) = R(1,2,12) * F(IS ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(114) = R(1,1, 4) * F(IU ,I) * G(IS ,I) /  9.0 * IMBB
          C(115) = R(1,5,19) * F(ID ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(116) = R(1,4,19) * F(IC ,I) * G(IU ,I) /  9.0 * IMNOBB
          C(117) = R(1,4,19) * F(IS ,I) * G(IU ,I) /  9.0 * IMNOBB
          C(118) = R(1,6,19) * F(ID ,I) * G(IU ,I) / 54.0 * IMNOBB
          C(119) = R(1,3,19) * F(ID ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(120) = R(1,3,24) * F(IC ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(121) = R(1,3,27) * F(IC ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(122) = R(1,3,19) * F(ID ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(123) = R(1,3,24) * F(IS ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(124) = R(1,3,27) * F(IS ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(125) = R(1,3,19) * F(ID ,I) * G(IU ,I) / 18.0 * IMBB
          C(126) = R(1,2,19) * F(IC ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(127) = R(1,1,19) * F(IC ,I) * G(IU ,I) /  9.0 * IMNOBB
          C(128) = R(1,1,24) * F(IS ,I) * G(IU ,I) /  9.0 * IMNOBB
          C(129) = R(1,1,27) * F(IS ,I) * G(IC ,I) /  9.0 * IMNOBB
          C(130) = R(1,1,19) * F(IC ,I) * G(IU ,I) /  9.0 * IMBB
          C(131) = R(1,2,19) * F(IS ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(132) = R(1,1,19) * F(IS ,I) * G(IU ,I) /  9.0 * IMBB
          DO 130 J=89,132
            T(1,1,I)=T(1,1,I)+C(J)
            T(1,8,I)=T(1,8,I)+C(J)
  130     CONTINUE
* Q Q W+ CS
          C(133) = R(1,2, 4) * F(IC ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(134) = R(1,2,12) * F(IU ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(135) = R(1,1, 4) * F(IC ,I) * G(IU ,I) /  9.0 * IMNOBB
          C(136) = R(1,1, 9) * F(IC ,I) * G(ID ,I) /  9.0 * IMNOBB
          C(137) = R(1,1,12) * F(IU ,I) * G(ID ,I) /  9.0 * IMNOBB
          C(138) = R(1,4, 4) * F(IC ,I) * G(IU ,I) /  9.0 * IMNOBB
          C(139) = R(1,4, 9) * F(IC ,I) * G(IC ,I) /  9.0 * IMNOBB
          C(140) = R(1,3, 4) * F(IC ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(141) = R(1,3, 9) * F(IC ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(142) = R(1,3,12) * F(IS ,I) * G(IU ,I) / 18.0 * IMNOBB
          C(143) = R(1,1, 4) * F(IC ,I) * G(IU ,I) /  9.0 * IMBB
          C(144) = R(1,2, 4) * F(IC ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(145) = R(1,2,12) * F(ID ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(146) = R(1,4, 4) * F(IC ,I) * G(ID ,I) /  9.0 * IMNOBB
          C(147) = R(1,4, 9) * F(IC ,I) * G(IC ,I) /  9.0 * IMNOBB
          C(148) = R(1,3, 4) * F(IC ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(149) = R(1,3, 9) * F(IC ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(150) = R(1,3,12) * F(IS ,I) * G(ID ,I) / 18.0 * IMNOBB
          C(151) = R(1,1, 4) * F(IC ,I) * G(ID ,I) /  9.0 * IMBB
          C(152) = R(1,7, 4) * F(IC ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(153) = R(1,5, 4) * F(IC ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(154) = R(1,5, 9) * F(IC ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(155) = R(1,4, 9) * F(IC ,I) * G(IC ,I) /  9.0 * IMBB
          C(156) = R(1,6, 4) * F(IC ,I) * G(IS ,I) / 54.0 * IMNOBB
          C(157) = R(1,6,12) * F(IS ,I) * G(IS ,I) / 54.0 * IMNOBB
          C(158) = R(1,3, 4) * F(IC ,I) * G(IS ,I) / 18.0 * IMBB
          C(159) = R(1,2,19) * F(IU ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(160) = R(1,1,19) * F(IU ,I) * G(IC ,I) /  9.0 * IMNOBB
          C(161) = R(1,1,24) * F(ID ,I) * G(IC ,I) /  9.0 * IMNOBB
          C(162) = R(1,1,27) * F(ID ,I) * G(IU ,I) /  9.0 * IMNOBB
          C(163) = R(1,4,19) * F(IU ,I) * G(IC ,I) /  9.0 * IMNOBB
          C(164) = R(1,3,19) * F(IS ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(165) = R(1,3,24) * F(IU ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(166) = R(1,3,27) * F(IU ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(167) = R(1,1,19) * F(IU ,I) * G(IC ,I) /  9.0 * IMBB
          C(168) = R(1,2,19) * F(ID ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(169) = R(1,4,19) * F(ID ,I) * G(IC ,I) /  9.0 * IMNOBB
          C(170) = R(1,3,19) * F(IS ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(171) = R(1,3,24) * F(ID ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(172) = R(1,3,27) * F(ID ,I) * G(IS ,I) / 18.0 * IMNOBB
          C(173) = R(1,1,19) * F(ID ,I) * G(IC ,I) /  9.0 * IMBB
          C(174) = R(1,5,19) * F(IS ,I) * G(IC ,I) / 18.0 * IMNOBB
          C(175) = R(1,6,19) * F(IS ,I) * G(IC ,I) / 54.0 * IMNOBB
          C(176) = R(1,3,19) * F(IS ,I) * G(IC ,I) / 18.0 * IMBB
          DO 140 J=133,176
            T(1,2,I)=T(1,2,I)+C(J)
            T(1,8,I)=T(1,8,I)+C(J)
  140     CONTINUE
*
      END
*
* Q QB
      SUBROUTINE QQBWQ6(R,T,I,IMNOBB,IMBB)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(2,9,6),R(2,7,30),C(848)
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      SAVE IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG,/STRUCF/
      DATA IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG
     .    / 1, 2, 3, 4, 5,  6,  7,  8,  9, 10,11/
*
* Q QB W- UD
          C(177) = R(2,5, 8) * F(ID ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(178) = R(2,5,10) * F(IU ,I) * G(IDB,I) / 36.0 * IMNOBB
          C(179) = R(2,3, 8) * F(ID ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(180) = R(2,3,10) * F(IU ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(181) = R(2,3, 8) * F(ID ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(182) = R(2,3,10) * F(IU ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(183) = R(2,7, 3) * F(ID ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(184) = R(2,4, 3) * F(ID ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(185) = R(2,4, 8) * F(ID ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(186) = R(2,4,10) * F(IC ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(187) = R(2,4, 3) * F(ID ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(188) = R(2,4, 8) * F(ID ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(189) = R(2,4,10) * F(IS ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(190) = R(2,4, 8) * F(ID ,I) * G(IDB,I) /  9.0 * IMBB
          C(191) = R(2,2, 3) * F(ID ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(192) = R(2,1, 3) * F(ID ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(193) = R(2,1, 8) * F(ID ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(194) = R(2,1,10) * F(IC ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(195) = R(2,1, 3) * F(ID ,I) * G(ICB,I) /  9.0 * IMBB
          C(196) = R(2,2, 3) * F(ID ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(197) = R(2,1, 3) * F(ID ,I) * G(ISB,I) /  9.0 * IMBB
          C(198) = R(2,6,16) * F(ID ,I) * G(IUB,I) / 36.0 * IMNOBB
          C(199) = R(2,6,21) * F(IU ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(200) = R(2,5,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(201) = R(2,5,21) * F(IU ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(202) = R(2,3,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(203) = R(2,3,21) * F(IU ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(204) = R(2,3,28) * F(IC ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(205) = R(2,3,29) * F(IC ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(206) = R(2,3,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(207) = R(2,3,21) * F(IU ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(208) = R(2,3,28) * F(IS ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(209) = R(2,3,29) * F(IS ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(210) = R(2,3,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMBB
          C(211) = R(2,3,21) * F(IU ,I) * G(IUB,I) /  9.0 * IMBB
          C(212) = R(2,7,16) * F(ID ,I) * G(IUB,I) / 36.0 * IMNOBB
          C(213) = R(2,4,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(214) = R(2,4,21) * F(IC ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(215) = R(2,4,26) * F(IC ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(216) = R(2,4,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(217) = R(2,4,21) * F(IS ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(218) = R(2,4,26) * F(IS ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(219) = R(2,4,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMBB
          C(220) = R(2,2,16) * F(ID ,I) * G(IUB,I) / 36.0 * IMNOBB
          C(221) = R(2,2,21) * F(IC ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(222) = R(2,2,26) * F(IC ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(223) = R(2,1,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(224) = R(2,1,21) * F(IC ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(225) = R(2,1,28) * F(IS ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(226) = R(2,1,26) * F(IC ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(227) = R(2,1,30) * F(IS ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(228) = R(2,1,29) * F(IS ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(229) = R(2,1,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMBB
          C(230) = R(2,1,21) * F(IC ,I) * G(IUB,I) /  9.0 * IMBB
          C(231) = R(2,1,26) * F(IC ,I) * G(ICB,I) /  9.0 * IMBB
          C(232) = R(2,2,16) * F(ID ,I) * G(IUB,I) / 36.0 * IMNOBB
          C(233) = R(2,2,21) * F(IS ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(234) = R(2,2,26) * F(IS ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(235) = R(2,1,16) * F(ID ,I) * G(IUB,I) /  9.0 * IMBB
          C(236) = R(2,1,21) * F(IS ,I) * G(IUB,I) /  9.0 * IMBB
          C(237) = R(2,1,26) * F(IS ,I) * G(ISB,I) /  9.0 * IMBB
          C(238) = R(2,2,16) * F(ID ,I) * G(IUB,I) / 36.0 * IMBB
          DO 150 J=177,238
            T(2,3,I)=T(2,3,I)+C(J)
            T(2,9,I)=T(2,9,I)+C(J)
  150     CONTINUE
* Q QB W- CS
          C(239) = R(2,2, 3) * F(IS ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(240) = R(2,1, 3) * F(IS ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(241) = R(2,1, 8) * F(IS ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(242) = R(2,1,10) * F(IU ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(243) = R(2,3, 8) * F(IS ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(244) = R(2,3,10) * F(IC ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(245) = R(2,4, 3) * F(IS ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(246) = R(2,4, 8) * F(IS ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(247) = R(2,4,10) * F(IU ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(248) = R(2,1, 3) * F(IS ,I) * G(IUB,I) /  9.0 * IMBB
          C(249) = R(2,2, 3) * F(IS ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(250) = R(2,3, 8) * F(IS ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(251) = R(2,3,10) * F(IC ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(252) = R(2,4, 3) * F(IS ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(253) = R(2,4, 8) * F(IS ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(254) = R(2,4,10) * F(ID ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(255) = R(2,1, 3) * F(IS ,I) * G(IDB,I) /  9.0 * IMBB
          C(256) = R(2,5, 8) * F(IS ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(257) = R(2,5,10) * F(IC ,I) * G(ISB,I) / 36.0 * IMNOBB
          C(258) = R(2,7, 3) * F(IS ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(259) = R(2,4, 8) * F(IS ,I) * G(ISB,I) /  9.0 * IMBB
          C(260) = R(2,2,16) * F(IS ,I) * G(ICB,I) / 36.0 * IMNOBB
          C(261) = R(2,2,21) * F(IU ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(262) = R(2,2,26) * F(IU ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(263) = R(2,1,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(264) = R(2,1,21) * F(IU ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(265) = R(2,1,28) * F(ID ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(266) = R(2,1,26) * F(IU ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(267) = R(2,1,30) * F(ID ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(268) = R(2,1,29) * F(ID ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(269) = R(2,3,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(270) = R(2,3,21) * F(IC ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(271) = R(2,3,28) * F(IU ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(272) = R(2,3,29) * F(IU ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(273) = R(2,4,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(274) = R(2,4,21) * F(IU ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(275) = R(2,4,26) * F(IU ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(276) = R(2,1,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMBB
          C(277) = R(2,1,21) * F(IU ,I) * G(ICB,I) /  9.0 * IMBB
          C(278) = R(2,1,26) * F(IU ,I) * G(IUB,I) /  9.0 * IMBB
          C(279) = R(2,2,16) * F(IS ,I) * G(ICB,I) / 36.0 * IMNOBB
          C(280) = R(2,2,21) * F(ID ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(281) = R(2,2,26) * F(ID ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(282) = R(2,3,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(283) = R(2,3,21) * F(IC ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(284) = R(2,3,28) * F(ID ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(285) = R(2,3,29) * F(ID ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(286) = R(2,4,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(287) = R(2,4,21) * F(ID ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(288) = R(2,4,26) * F(ID ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(289) = R(2,1,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMBB
          C(290) = R(2,1,21) * F(ID ,I) * G(ICB,I) /  9.0 * IMBB
          C(291) = R(2,1,26) * F(ID ,I) * G(IDB,I) /  9.0 * IMBB
          C(292) = R(2,6,16) * F(IS ,I) * G(ICB,I) / 36.0 * IMNOBB
          C(293) = R(2,6,21) * F(IC ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(294) = R(2,5,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(295) = R(2,5,21) * F(IC ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(296) = R(2,3,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMBB
          C(297) = R(2,3,21) * F(IC ,I) * G(ICB,I) /  9.0 * IMBB
          C(298) = R(2,7,16) * F(IS ,I) * G(ICB,I) / 36.0 * IMNOBB
          C(299) = R(2,4,16) * F(IS ,I) * G(ICB,I) /  9.0 * IMBB
          C(300) = R(2,2,16) * F(IS ,I) * G(ICB,I) / 36.0 * IMBB
          DO 160 J=239,300
            T(2,4,I)=T(2,4,I)+C(J)
            T(2,9,I)=T(2,9,I)+C(J)
  160     CONTINUE
* Q QB W+ UD
          C(301) = R(1,7, 3) * F(IU ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(302) = R(1,5, 8) * F(IU ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(303) = R(1,5,10) * F(ID ,I) * G(IUB,I) / 36.0 * IMNOBB
          C(304) = R(1,4, 3) * F(IU ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(305) = R(1,4, 8) * F(IU ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(306) = R(1,4,10) * F(IC ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(307) = R(1,4, 3) * F(IU ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(308) = R(1,4, 8) * F(IU ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(309) = R(1,4,10) * F(IS ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(310) = R(1,4, 8) * F(IU ,I) * G(IUB,I) /  9.0 * IMBB
          C(311) = R(1,3, 8) * F(IU ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(312) = R(1,3,10) * F(ID ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(313) = R(1,3, 8) * F(IU ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(314) = R(1,3,10) * F(ID ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(315) = R(1,2, 3) * F(IU ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(316) = R(1,1, 3) * F(IU ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(317) = R(1,1, 8) * F(IU ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(318) = R(1,1,10) * F(IC ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(319) = R(1,1, 3) * F(IU ,I) * G(ICB,I) /  9.0 * IMBB
          C(320) = R(1,2, 3) * F(IU ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(321) = R(1,1, 3) * F(IU ,I) * G(ISB,I) /  9.0 * IMBB
          C(322) = R(1,7,16) * F(IU ,I) * G(IDB,I) / 36.0 * IMNOBB
          C(323) = R(1,5,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(324) = R(1,5,21) * F(ID ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(325) = R(1,4,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(326) = R(1,4,21) * F(IC ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(327) = R(1,4,26) * F(IC ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(328) = R(1,4,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(329) = R(1,4,21) * F(IS ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(330) = R(1,4,26) * F(IS ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(331) = R(1,4,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMBB
          C(332) = R(1,6,16) * F(IU ,I) * G(IDB,I) / 36.0 * IMNOBB
          C(333) = R(1,6,21) * F(ID ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(334) = R(1,3,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(335) = R(1,3,21) * F(ID ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(336) = R(1,3,28) * F(IC ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(337) = R(1,3,29) * F(IC ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(338) = R(1,3,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(339) = R(1,3,21) * F(ID ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(340) = R(1,3,28) * F(IS ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(341) = R(1,3,29) * F(IS ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(342) = R(1,3,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMBB
          C(343) = R(1,3,21) * F(ID ,I) * G(IDB,I) /  9.0 * IMBB
          C(344) = R(1,2,16) * F(IU ,I) * G(IDB,I) / 36.0 * IMNOBB
          C(345) = R(1,2,21) * F(IC ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(346) = R(1,2,26) * F(IC ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(347) = R(1,1,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(348) = R(1,1,21) * F(IC ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(349) = R(1,1,28) * F(IS ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(350) = R(1,1,26) * F(IC ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(351) = R(1,1,30) * F(IS ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(352) = R(1,1,29) * F(IS ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(353) = R(1,1,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMBB
          C(354) = R(1,1,21) * F(IC ,I) * G(IDB,I) /  9.0 * IMBB
          C(355) = R(1,1,26) * F(IC ,I) * G(ICB,I) /  9.0 * IMBB
          C(356) = R(1,2,16) * F(IU ,I) * G(IDB,I) / 36.0 * IMNOBB
          C(357) = R(1,2,21) * F(IS ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(358) = R(1,2,26) * F(IS ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(359) = R(1,1,16) * F(IU ,I) * G(IDB,I) /  9.0 * IMBB
          C(360) = R(1,1,21) * F(IS ,I) * G(IDB,I) /  9.0 * IMBB
          C(361) = R(1,1,26) * F(IS ,I) * G(ISB,I) /  9.0 * IMBB
          C(362) = R(1,2,16) * F(IU ,I) * G(IDB,I) / 36.0 * IMBB
          DO 170 J=301,362
            T(1,1,I)=T(1,1,I)+C(J)
            T(1,9,I)=T(1,9,I)+C(J)
  170     CONTINUE
* Q QB W+ CS
          C(363) = R(1,2, 3) * F(IC ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(364) = R(1,1, 3) * F(IC ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(365) = R(1,1, 8) * F(IC ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(366) = R(1,1,10) * F(IU ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(367) = R(1,4, 3) * F(IC ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(368) = R(1,4, 8) * F(IC ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(369) = R(1,4,10) * F(IU ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(370) = R(1,3, 8) * F(IC ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(371) = R(1,3,10) * F(IS ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(372) = R(1,1, 3) * F(IC ,I) * G(IUB,I) /  9.0 * IMBB
          C(373) = R(1,2, 3) * F(IC ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(374) = R(1,4, 3) * F(IC ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(375) = R(1,4, 8) * F(IC ,I) * G(ICB,I) /  9.0 * IMNOBB
          C(376) = R(1,4,10) * F(ID ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(377) = R(1,3, 8) * F(IC ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(378) = R(1,3,10) * F(IS ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(379) = R(1,1, 3) * F(IC ,I) * G(IDB,I) /  9.0 * IMBB
          C(380) = R(1,7, 3) * F(IC ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(381) = R(1,5, 8) * F(IC ,I) * G(ICB,I) / 18.0 * IMNOBB
          C(382) = R(1,5,10) * F(IS ,I) * G(ICB,I) / 36.0 * IMNOBB
          C(383) = R(1,4, 8) * F(IC ,I) * G(ICB,I) /  9.0 * IMBB
          C(384) = R(1,2,16) * F(IC ,I) * G(ISB,I) / 36.0 * IMNOBB
          C(385) = R(1,2,21) * F(IU ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(386) = R(1,2,26) * F(IU ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(387) = R(1,1,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(388) = R(1,1,21) * F(IU ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(389) = R(1,1,28) * F(ID ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(390) = R(1,1,26) * F(IU ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(391) = R(1,1,30) * F(ID ,I) * G(IUB,I) /  9.0 * IMNOBB
          C(392) = R(1,1,29) * F(ID ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(393) = R(1,4,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(394) = R(1,4,21) * F(IU ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(395) = R(1,4,26) * F(IU ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(396) = R(1,3,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(397) = R(1,3,21) * F(IS ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(398) = R(1,3,28) * F(IU ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(399) = R(1,3,29) * F(IU ,I) * G(IUB,I) / 18.0 * IMNOBB
          C(400) = R(1,1,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMBB
          C(401) = R(1,1,21) * F(IU ,I) * G(ISB,I) /  9.0 * IMBB
          C(402) = R(1,1,26) * F(IU ,I) * G(IUB,I) /  9.0 * IMBB
          C(403) = R(1,2,16) * F(IC ,I) * G(ISB,I) / 36.0 * IMNOBB
          C(404) = R(1,2,21) * F(ID ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(405) = R(1,2,26) * F(ID ,I) * G(IDB,I) /  9.0 * IMNOBB
          C(406) = R(1,4,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(407) = R(1,4,21) * F(ID ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(408) = R(1,4,26) * F(ID ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(409) = R(1,3,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(410) = R(1,3,21) * F(IS ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(411) = R(1,3,28) * F(ID ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(412) = R(1,3,29) * F(ID ,I) * G(IDB,I) / 18.0 * IMNOBB
          C(413) = R(1,1,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMBB
          C(414) = R(1,1,21) * F(ID ,I) * G(ISB,I) /  9.0 * IMBB
          C(415) = R(1,1,26) * F(ID ,I) * G(IDB,I) /  9.0 * IMBB
          C(416) = R(1,7,16) * F(IC ,I) * G(ISB,I) / 36.0 * IMNOBB
          C(417) = R(1,5,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMNOBB
          C(418) = R(1,5,21) * F(IS ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(419) = R(1,4,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMBB
          C(420) = R(1,6,16) * F(IC ,I) * G(ISB,I) / 36.0 * IMNOBB
          C(421) = R(1,6,21) * F(IS ,I) * G(ISB,I) / 18.0 * IMNOBB
          C(422) = R(1,3,16) * F(IC ,I) * G(ISB,I) /  9.0 * IMBB
          C(423) = R(1,3,21) * F(IS ,I) * G(ISB,I) /  9.0 * IMBB
          C(424) = R(1,2,16) * F(IC ,I) * G(ISB,I) / 36.0 * IMBB
          DO 180 J=363,424
            T(1,2,I)=T(1,2,I)+C(J)
            T(1,9,I)=T(1,9,I)+C(J)
  180     CONTINUE
*
      END
*
*QB Q
      SUBROUTINE QBQWQ6(R,T,I,IMNOBB,IMBB)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(2,9,6),R(2,7,30),C(848)
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      SAVE IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG,/STRUCF/
      DATA IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG
     .    / 1, 2, 3, 4, 5,  6,  7,  8,  9, 10,11/
*
*QB Q W- UD
          C(425) = R(2,6, 1) * F(IUB,I) * G(ID ,I) / 36.0 * IMNOBB
          C(426) = R(2,6, 6) * F(IUB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(427) = R(2,5, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(428) = R(2,5, 6) * F(IUB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(429) = R(2,3, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(430) = R(2,3, 6) * F(IUB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(431) = R(2,3,13) * F(IUB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(432) = R(2,3,14) * F(ICB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(433) = R(2,3, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(434) = R(2,3, 6) * F(IUB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(435) = R(2,3,13) * F(IUB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(436) = R(2,3,14) * F(ISB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(437) = R(2,3, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMBB
          C(438) = R(2,3, 6) * F(IUB,I) * G(IU ,I) /  9.0 * IMBB
          C(439) = R(2,7, 1) * F(IUB,I) * G(ID ,I) / 36.0 * IMNOBB
          C(440) = R(2,4, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(441) = R(2,4, 6) * F(IUB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(442) = R(2,4,11) * F(ICB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(443) = R(2,4, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(444) = R(2,4, 6) * F(IUB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(445) = R(2,4,11) * F(ISB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(446) = R(2,4, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMBB
          C(447) = R(2,2, 1) * F(IUB,I) * G(ID ,I) / 36.0 * IMNOBB
          C(448) = R(2,2, 6) * F(IUB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(449) = R(2,2,11) * F(ICB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(450) = R(2,1, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(451) = R(2,1, 6) * F(IUB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(452) = R(2,1,13) * F(IUB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(453) = R(2,1,11) * F(ICB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(454) = R(2,1,15) * F(ICB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(455) = R(2,1,14) * F(ISB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(456) = R(2,1, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMBB
          C(457) = R(2,1, 6) * F(IUB,I) * G(IC ,I) /  9.0 * IMBB
          C(458) = R(2,1,11) * F(ICB,I) * G(IC ,I) /  9.0 * IMBB
          C(459) = R(2,2, 1) * F(IUB,I) * G(ID ,I) / 36.0 * IMNOBB
          C(460) = R(2,2, 6) * F(IUB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(461) = R(2,2,11) * F(ISB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(462) = R(2,1, 1) * F(IUB,I) * G(ID ,I) /  9.0 * IMBB
          C(463) = R(2,1, 6) * F(IUB,I) * G(IS ,I) /  9.0 * IMBB
          C(464) = R(2,1,11) * F(ISB,I) * G(IS ,I) /  9.0 * IMBB
          C(465) = R(2,2, 1) * F(IUB,I) * G(ID ,I) / 36.0 * IMBB
          C(466) = R(2,5,23) * F(IDB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(467) = R(2,5,25) * F(IDB,I) * G(IU ,I) / 36.0 * IMNOBB
          C(468) = R(2,3,23) * F(ICB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(469) = R(2,3,25) * F(ICB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(470) = R(2,3,23) * F(ISB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(471) = R(2,3,25) * F(ISB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(472) = R(2,7,18) * F(IDB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(473) = R(2,4,18) * F(ICB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(474) = R(2,4,23) * F(IDB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(475) = R(2,4,25) * F(IDB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(476) = R(2,4,18) * F(ISB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(477) = R(2,4,23) * F(IDB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(478) = R(2,4,25) * F(IDB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(479) = R(2,4,23) * F(IDB,I) * G(ID ,I) /  9.0 * IMBB
          C(480) = R(2,2,18) * F(ICB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(481) = R(2,1,18) * F(ICB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(482) = R(2,1,23) * F(ISB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(483) = R(2,1,25) * F(ISB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(484) = R(2,1,18) * F(ICB,I) * G(ID ,I) /  9.0 * IMBB
          C(485) = R(2,2,18) * F(ISB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(486) = R(2,1,18) * F(ISB,I) * G(ID ,I) /  9.0 * IMBB
          DO 190 J=425,486
            T(2,3,I)=T(2,3,I)+C(J)
            T(2,9,I)=T(2,9,I)+C(J)
  190     CONTINUE
* QB Q W- CS
          C(487) = R(2,2, 1) * F(ICB,I) * G(IS ,I) / 36.0 * IMNOBB
          C(488) = R(2,2, 6) * F(ICB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(489) = R(2,2,11) * F(IUB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(490) = R(2,1, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(491) = R(2,1, 6) * F(ICB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(492) = R(2,1,13) * F(ICB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(493) = R(2,1,11) * F(IUB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(494) = R(2,1,15) * F(IUB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(495) = R(2,1,14) * F(IDB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(496) = R(2,3, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(497) = R(2,3, 6) * F(ICB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(498) = R(2,3,13) * F(ICB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(499) = R(2,3,14) * F(IUB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(500) = R(2,4, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(501) = R(2,4, 6) * F(ICB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(502) = R(2,4,11) * F(IUB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(503) = R(2,1, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMBB
          C(504) = R(2,1, 6) * F(ICB,I) * G(IU ,I) /  9.0 * IMBB
          C(505) = R(2,1,11) * F(IUB,I) * G(IU ,I) /  9.0 * IMBB
          C(506) = R(2,2, 1) * F(ICB,I) * G(IS ,I) / 36.0 * IMNOBB
          C(507) = R(2,2, 6) * F(ICB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(508) = R(2,2,11) * F(IDB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(509) = R(2,3, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(510) = R(2,3, 6) * F(ICB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(511) = R(2,3,13) * F(ICB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(512) = R(2,3,14) * F(IDB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(513) = R(2,4, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(514) = R(2,4, 6) * F(ICB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(515) = R(2,4,11) * F(IDB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(516) = R(2,1, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMBB
          C(517) = R(2,1, 6) * F(ICB,I) * G(ID ,I) /  9.0 * IMBB
          C(518) = R(2,1,11) * F(IDB,I) * G(ID ,I) /  9.0 * IMBB
          C(519) = R(2,6, 1) * F(ICB,I) * G(IS ,I) / 36.0 * IMNOBB
          C(520) = R(2,6, 6) * F(ICB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(521) = R(2,5, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(522) = R(2,5, 6) * F(ICB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(523) = R(2,3, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMBB
          C(524) = R(2,3, 6) * F(ICB,I) * G(IC ,I) /  9.0 * IMBB
          C(525) = R(2,7, 1) * F(ICB,I) * G(IS ,I) / 36.0 * IMNOBB
          C(526) = R(2,4, 1) * F(ICB,I) * G(IS ,I) /  9.0 * IMBB
          C(527) = R(2,2, 1) * F(ICB,I) * G(IS ,I) / 36.0 * IMBB
          C(528) = R(2,2,18) * F(IUB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(529) = R(2,1,18) * F(IUB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(530) = R(2,1,23) * F(IDB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(531) = R(2,1,25) * F(IDB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(532) = R(2,3,23) * F(IUB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(533) = R(2,3,25) * F(IUB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(534) = R(2,4,18) * F(IUB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(535) = R(2,4,23) * F(ISB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(536) = R(2,4,25) * F(ISB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(537) = R(2,1,18) * F(IUB,I) * G(IS ,I) /  9.0 * IMBB
          C(538) = R(2,2,18) * F(IDB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(539) = R(2,3,23) * F(IDB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(540) = R(2,3,25) * F(IDB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(541) = R(2,4,18) * F(IDB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(542) = R(2,4,23) * F(ISB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(543) = R(2,4,25) * F(ISB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(544) = R(2,1,18) * F(IDB,I) * G(IS ,I) /  9.0 * IMBB
          C(545) = R(2,5,23) * F(ISB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(546) = R(2,5,25) * F(ISB,I) * G(IC ,I) / 36.0 * IMNOBB
          C(547) = R(2,7,18) * F(ISB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(548) = R(2,4,23) * F(ISB,I) * G(IS ,I) /  9.0 * IMBB
          DO 200 J=487,548
            T(2,4,I)=T(2,4,I)+C(J)
            T(2,9,I)=T(2,9,I)+C(J)
  200     CONTINUE
* QB Q W+ UD
          C(549) = R(1,7, 1) * F(IDB,I) * G(IU ,I) / 36.0 * IMNOBB
          C(550) = R(1,5, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(551) = R(1,5, 6) * F(IDB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(552) = R(1,4, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(553) = R(1,4, 6) * F(IDB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(554) = R(1,4,11) * F(ICB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(555) = R(1,4, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(556) = R(1,4, 6) * F(IDB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(557) = R(1,4,11) * F(ISB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(558) = R(1,4, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMBB
          C(559) = R(1,6, 1) * F(IDB,I) * G(IU ,I) / 36.0 * IMNOBB
          C(560) = R(1,6, 6) * F(IDB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(561) = R(1,3, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(562) = R(1,3, 6) * F(IDB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(563) = R(1,3,13) * F(IDB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(564) = R(1,3,14) * F(ICB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(565) = R(1,3, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(566) = R(1,3, 6) * F(IDB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(567) = R(1,3,13) * F(IDB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(568) = R(1,3,14) * F(ISB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(569) = R(1,3, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMBB
          C(570) = R(1,3, 6) * F(IDB,I) * G(ID ,I) /  9.0 * IMBB
          C(571) = R(1,2, 1) * F(IDB,I) * G(IU ,I) / 36.0 * IMNOBB
          C(572) = R(1,2, 6) * F(IDB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(573) = R(1,2,11) * F(ICB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(574) = R(1,1, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(575) = R(1,1, 6) * F(IDB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(576) = R(1,1,13) * F(IDB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(577) = R(1,1,11) * F(ICB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(578) = R(1,1,15) * F(ICB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(579) = R(1,1,14) * F(ISB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(580) = R(1,1, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMBB
          C(581) = R(1,1, 6) * F(IDB,I) * G(IC ,I) /  9.0 * IMBB
          C(582) = R(1,1,11) * F(ICB,I) * G(IC ,I) /  9.0 * IMBB
          C(583) = R(1,2, 1) * F(IDB,I) * G(IU ,I) / 36.0 * IMNOBB
          C(584) = R(1,2, 6) * F(IDB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(585) = R(1,2,11) * F(ISB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(586) = R(1,1, 1) * F(IDB,I) * G(IU ,I) /  9.0 * IMBB
          C(587) = R(1,1, 6) * F(IDB,I) * G(IS ,I) /  9.0 * IMBB
          C(588) = R(1,1,11) * F(ISB,I) * G(IS ,I) /  9.0 * IMBB
          C(589) = R(1,2, 1) * F(IDB,I) * G(IU ,I) / 36.0 * IMBB
          C(590) = R(1,7,18) * F(IUB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(591) = R(1,5,23) * F(IUB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(592) = R(1,5,25) * F(IUB,I) * G(ID ,I) / 36.0 * IMNOBB
          C(593) = R(1,4,18) * F(ICB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(594) = R(1,4,23) * F(IUB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(595) = R(1,4,25) * F(IUB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(596) = R(1,4,18) * F(ISB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(597) = R(1,4,23) * F(IUB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(598) = R(1,4,25) * F(IUB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(599) = R(1,4,23) * F(IUB,I) * G(IU ,I) /  9.0 * IMBB
          C(600) = R(1,3,23) * F(ICB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(601) = R(1,3,25) * F(ICB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(602) = R(1,3,23) * F(ISB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(603) = R(1,3,25) * F(ISB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(604) = R(1,2,18) * F(ICB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(605) = R(1,1,18) * F(ICB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(606) = R(1,1,23) * F(ISB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(607) = R(1,1,25) * F(ISB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(608) = R(1,1,18) * F(ICB,I) * G(IU ,I) /  9.0 * IMBB
          C(609) = R(1,2,18) * F(ISB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(610) = R(1,1,18) * F(ISB,I) * G(IU ,I) /  9.0 * IMBB
          DO 210 J=549,610
            T(1,1,I)=T(1,1,I)+C(J)
            T(1,9,I)=T(1,9,I)+C(J)
  210     CONTINUE
* QB Q W+ CS
          C(611) = R(1,2, 1) * F(ISB,I) * G(IC ,I) / 36.0 * IMNOBB
          C(612) = R(1,2, 6) * F(ISB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(613) = R(1,2,11) * F(IUB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(614) = R(1,1, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(615) = R(1,1, 6) * F(ISB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(616) = R(1,1,13) * F(ISB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(617) = R(1,1,11) * F(IUB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(618) = R(1,1,15) * F(IUB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(619) = R(1,1,14) * F(IDB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(620) = R(1,4, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(621) = R(1,4, 6) * F(ISB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(622) = R(1,4,11) * F(IUB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(623) = R(1,3, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(624) = R(1,3, 6) * F(ISB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(625) = R(1,3,13) * F(ISB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(626) = R(1,3,14) * F(IUB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(627) = R(1,1, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMBB
          C(628) = R(1,1, 6) * F(ISB,I) * G(IU ,I) /  9.0 * IMBB
          C(629) = R(1,1,11) * F(IUB,I) * G(IU ,I) /  9.0 * IMBB
          C(630) = R(1,2, 1) * F(ISB,I) * G(IC ,I) / 36.0 * IMNOBB
          C(631) = R(1,2, 6) * F(ISB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(632) = R(1,2,11) * F(IDB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(633) = R(1,4, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(634) = R(1,4, 6) * F(ISB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(635) = R(1,4,11) * F(IDB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(636) = R(1,3, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(637) = R(1,3, 6) * F(ISB,I) * G(IS ,I) /  9.0 * IMNOBB
          C(638) = R(1,3,13) * F(ISB,I) * G(ID ,I) /  9.0 * IMNOBB
          C(639) = R(1,3,14) * F(IDB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(640) = R(1,1, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMBB
          C(641) = R(1,1, 6) * F(ISB,I) * G(ID ,I) /  9.0 * IMBB
          C(642) = R(1,1,11) * F(IDB,I) * G(ID ,I) /  9.0 * IMBB
          C(643) = R(1,7, 1) * F(ISB,I) * G(IC ,I) / 36.0 * IMNOBB
          C(644) = R(1,5, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(645) = R(1,5, 6) * F(ISB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(646) = R(1,4, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMBB
          C(647) = R(1,6, 1) * F(ISB,I) * G(IC ,I) / 36.0 * IMNOBB
          C(648) = R(1,6, 6) * F(ISB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(649) = R(1,3, 1) * F(ISB,I) * G(IC ,I) /  9.0 * IMBB
          C(650) = R(1,3, 6) * F(ISB,I) * G(IS ,I) /  9.0 * IMBB
          C(651) = R(1,2, 1) * F(ISB,I) * G(IC ,I) / 36.0 * IMBB
          C(652) = R(1,2,18) * F(IUB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(653) = R(1,1,18) * F(IUB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(654) = R(1,1,23) * F(IDB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(655) = R(1,1,25) * F(IDB,I) * G(IU ,I) /  9.0 * IMNOBB
          C(656) = R(1,4,18) * F(IUB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(657) = R(1,4,23) * F(ICB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(658) = R(1,4,25) * F(ICB,I) * G(IU ,I) / 18.0 * IMNOBB
          C(659) = R(1,3,23) * F(IUB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(660) = R(1,3,25) * F(IUB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(661) = R(1,1,18) * F(IUB,I) * G(IC ,I) /  9.0 * IMBB
          C(662) = R(1,2,18) * F(IDB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(663) = R(1,4,18) * F(IDB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(664) = R(1,4,23) * F(ICB,I) * G(IC ,I) /  9.0 * IMNOBB
          C(665) = R(1,4,25) * F(ICB,I) * G(ID ,I) / 18.0 * IMNOBB
          C(666) = R(1,3,23) * F(IDB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(667) = R(1,3,25) * F(IDB,I) * G(IS ,I) / 18.0 * IMNOBB
          C(668) = R(1,1,18) * F(IDB,I) * G(IC ,I) /  9.0 * IMBB
          C(669) = R(1,7,18) * F(ICB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(670) = R(1,5,23) * F(ICB,I) * G(IC ,I) / 18.0 * IMNOBB
          C(671) = R(1,5,25) * F(ICB,I) * G(IS ,I) / 36.0 * IMNOBB
          C(672) = R(1,4,23) * F(ICB,I) * G(IC ,I) /  9.0 * IMBB
          DO 220 J=611,672
            T(1,2,I)=T(1,2,I)+C(J)
            T(1,9,I)=T(1,9,I)+C(J)
  220     CONTINUE
*
      END
*
* QB QB
      SUBROUTINE QBBWQ6(R,T,I,IMNOBB,IMBB)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(2,9,6),R(2,7,30),C(848)
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      SAVE IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG,/STRUCF/
      DATA IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG
     .    / 1, 2, 3, 4, 5,  6,  7,  8,  9, 10,11/
*
* QB QB W- UD
          C(673) = R(2,6, 2) * F(IUB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(674) = R(2,5, 2) * F(IUB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(675) = R(2,5, 7) * F(IUB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(676) = R(2,3, 2) * F(IUB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(677) = R(2,3, 7) * F(IUB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(678) = R(2,3, 2) * F(IUB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(679) = R(2,3, 7) * F(IUB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(680) = R(2,3, 2) * F(IUB,I) * G(IUB,I) /  9.0 * IMBB
          C(681) = R(2,7, 2) * F(IUB,I) * G(IDB,I) / 54.0 * IMNOBB
          C(682) = R(2,7, 5) * F(IDB,I) * G(IDB,I) / 54.0 * IMNOBB
          C(683) = R(2,4, 2) * F(IUB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(684) = R(2,4, 7) * F(IUB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(685) = R(2,4, 5) * F(ICB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(686) = R(2,4, 2) * F(IUB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(687) = R(2,4, 7) * F(IUB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(688) = R(2,4, 5) * F(ISB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(689) = R(2,4, 7) * F(IUB,I) * G(IDB,I) / 18.0 * IMBB
          C(690) = R(2,2, 2) * F(IUB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(691) = R(2,2, 5) * F(ICB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(692) = R(2,1, 2) * F(IUB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(693) = R(2,1, 7) * F(IUB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(694) = R(2,1, 5) * F(ICB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(695) = R(2,1, 2) * F(IUB,I) * G(ICB,I) /  9.0 * IMBB
          C(696) = R(2,2, 2) * F(IUB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(697) = R(2,2, 5) * F(ISB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(698) = R(2,1, 2) * F(IUB,I) * G(ISB,I) /  9.0 * IMBB
          C(699) = R(2,5,22) * F(IDB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(700) = R(2,3,22) * F(ICB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(701) = R(2,3,22) * F(ISB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(702) = R(2,7,17) * F(IDB,I) * G(IUB,I) / 54.0 * IMNOBB
          C(703) = R(2,4,17) * F(ICB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(704) = R(2,4,22) * F(IDB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(705) = R(2,4,20) * F(IDB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(706) = R(2,4,17) * F(ISB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(707) = R(2,4,22) * F(IDB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(708) = R(2,4,20) * F(IDB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(709) = R(2,4,22) * F(IDB,I) * G(IUB,I) / 18.0 * IMBB
          C(710) = R(2,2,17) * F(ICB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(711) = R(2,1,17) * F(ICB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(712) = R(2,1,22) * F(ISB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(713) = R(2,1,20) * F(ISB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(714) = R(2,1,17) * F(ICB,I) * G(IUB,I) /  9.0 * IMBB
          C(715) = R(2,2,17) * F(ISB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(716) = R(2,1,17) * F(ISB,I) * G(IUB,I) /  9.0 * IMBB
          DO 230 J=673,716
            T(2,3,I)=T(2,3,I)+C(J)
            T(2,8,I)=T(2,8,I)+C(J)
  230     CONTINUE
* QB QB W- CS
          C(717) = R(2,2, 2) * F(ICB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(718) = R(2,2, 5) * F(IUB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(719) = R(2,1, 2) * F(ICB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(720) = R(2,1, 7) * F(ICB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(721) = R(2,1, 5) * F(IUB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(722) = R(2,3, 2) * F(ICB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(723) = R(2,3, 7) * F(ICB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(724) = R(2,4, 2) * F(ICB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(725) = R(2,4, 7) * F(ICB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(726) = R(2,4, 5) * F(IUB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(727) = R(2,1, 2) * F(ICB,I) * G(IUB,I) /  9.0 * IMBB
          C(728) = R(2,2, 2) * F(ICB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(729) = R(2,2, 5) * F(IDB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(730) = R(2,3, 2) * F(ICB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(731) = R(2,3, 7) * F(ICB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(732) = R(2,4, 2) * F(ICB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(733) = R(2,4, 7) * F(ICB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(734) = R(2,4, 5) * F(IDB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(735) = R(2,1, 2) * F(ICB,I) * G(IDB,I) /  9.0 * IMBB
          C(736) = R(2,6, 2) * F(ICB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(737) = R(2,5, 2) * F(ICB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(738) = R(2,5, 7) * F(ICB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(739) = R(2,3, 2) * F(ICB,I) * G(ICB,I) /  9.0 * IMBB
          C(740) = R(2,7, 2) * F(ICB,I) * G(ISB,I) / 54.0 * IMNOBB
          C(741) = R(2,7, 5) * F(ISB,I) * G(ISB,I) / 54.0 * IMNOBB
          C(742) = R(2,4, 7) * F(ICB,I) * G(ISB,I) / 18.0 * IMBB
          C(743) = R(2,2,17) * F(IUB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(744) = R(2,1,17) * F(IUB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(745) = R(2,1,22) * F(IDB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(746) = R(2,1,20) * F(IDB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(747) = R(2,3,22) * F(IUB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(748) = R(2,4,17) * F(IUB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(749) = R(2,4,22) * F(ISB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(750) = R(2,4,20) * F(ISB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(751) = R(2,1,17) * F(IUB,I) * G(ICB,I) /  9.0 * IMBB
          C(752) = R(2,2,17) * F(IDB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(753) = R(2,3,22) * F(IDB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(754) = R(2,4,17) * F(IDB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(755) = R(2,4,22) * F(ISB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(756) = R(2,4,20) * F(ISB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(757) = R(2,1,17) * F(IDB,I) * G(ICB,I) /  9.0 * IMBB
          C(758) = R(2,5,22) * F(ISB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(759) = R(2,7,17) * F(ISB,I) * G(ICB,I) / 54.0 * IMNOBB
          C(760) = R(2,4,22) * F(ISB,I) * G(ICB,I) / 18.0 * IMBB
          DO 240 J=717,760
            T(2,4,I)=T(2,4,I)+C(J)
            T(2,8,I)=T(2,8,I)+C(J)
  240     CONTINUE
* QB QB W+ UD
          C(761) = R(1,7, 2) * F(IDB,I) * G(IUB,I) / 54.0 * IMNOBB
          C(762) = R(1,7, 5) * F(IUB,I) * G(IUB,I) / 54.0 * IMNOBB
          C(763) = R(1,5, 2) * F(IDB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(764) = R(1,5, 7) * F(IDB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(765) = R(1,4, 2) * F(IDB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(766) = R(1,4, 7) * F(IDB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(767) = R(1,4, 5) * F(ICB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(768) = R(1,4, 2) * F(IDB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(769) = R(1,4, 7) * F(IDB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(770) = R(1,4, 5) * F(ISB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(771) = R(1,4, 7) * F(IDB,I) * G(IUB,I) / 18.0 * IMBB
          C(772) = R(1,6, 2) * F(IDB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(773) = R(1,3, 2) * F(IDB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(774) = R(1,3, 7) * F(IDB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(775) = R(1,3, 2) * F(IDB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(776) = R(1,3, 7) * F(IDB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(777) = R(1,3, 2) * F(IDB,I) * G(IDB,I) /  9.0 * IMBB
          C(778) = R(1,2, 2) * F(IDB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(779) = R(1,2, 5) * F(ICB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(780) = R(1,1, 2) * F(IDB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(781) = R(1,1, 7) * F(IDB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(782) = R(1,1, 5) * F(ICB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(783) = R(1,1, 2) * F(IDB,I) * G(ICB,I) /  9.0 * IMBB
          C(784) = R(1,2, 2) * F(IDB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(785) = R(1,2, 5) * F(ISB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(786) = R(1,1, 2) * F(IDB,I) * G(ISB,I) /  9.0 * IMBB
          C(787) = R(1,7,17) * F(IUB,I) * G(IDB,I) / 54.0 * IMNOBB
          C(788) = R(1,5,22) * F(IUB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(789) = R(1,4,17) * F(ICB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(790) = R(1,4,22) * F(IUB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(791) = R(1,4,20) * F(IUB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(792) = R(1,4,17) * F(ISB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(793) = R(1,4,22) * F(IUB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(794) = R(1,4,20) * F(IUB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(795) = R(1,4,22) * F(IUB,I) * G(IDB,I) / 18.0 * IMBB
          C(796) = R(1,3,22) * F(ICB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(797) = R(1,3,22) * F(ISB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(798) = R(1,2,17) * F(ICB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(799) = R(1,1,17) * F(ICB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(800) = R(1,1,22) * F(ISB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(801) = R(1,1,20) * F(ISB,I) * G(ICB,I) /  9.0 * IMNOBB
          C(802) = R(1,1,17) * F(ICB,I) * G(IDB,I) /  9.0 * IMBB
          C(803) = R(1,2,17) * F(ISB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(804) = R(1,1,17) * F(ISB,I) * G(IDB,I) /  9.0 * IMBB
          DO 250 J=761,804
            T(1,1,I)=T(1,1,I)+C(J)
            T(1,8,I)=T(1,8,I)+C(J)
  250     CONTINUE
* QB QB W+ CS
          C(805) = R(1,2, 2) * F(ISB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(806) = R(1,2, 5) * F(IUB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(807) = R(1,1, 2) * F(ISB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(808) = R(1,1, 7) * F(ISB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(809) = R(1,1, 5) * F(IUB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(810) = R(1,4, 2) * F(ISB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(811) = R(1,4, 7) * F(ISB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(812) = R(1,4, 5) * F(IUB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(813) = R(1,3, 2) * F(ISB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(814) = R(1,3, 7) * F(ISB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(815) = R(1,1, 2) * F(ISB,I) * G(IUB,I) /  9.0 * IMBB
          C(816) = R(1,2, 2) * F(ISB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(817) = R(1,2, 5) * F(IDB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(818) = R(1,4, 2) * F(ISB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(819) = R(1,4, 7) * F(ISB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(820) = R(1,4, 5) * F(IDB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(821) = R(1,3, 2) * F(ISB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(822) = R(1,3, 7) * F(ISB,I) * G(IDB,I) /  9.0 * IMNOBB
          C(823) = R(1,1, 2) * F(ISB,I) * G(IDB,I) /  9.0 * IMBB
          C(824) = R(1,7, 2) * F(ISB,I) * G(ICB,I) / 54.0 * IMNOBB
          C(825) = R(1,7, 5) * F(ICB,I) * G(ICB,I) / 54.0 * IMNOBB
          C(826) = R(1,5, 2) * F(ISB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(827) = R(1,5, 7) * F(ISB,I) * G(ICB,I) / 18.0 * IMNOBB
          C(828) = R(1,4, 7) * F(ISB,I) * G(ICB,I) / 18.0 * IMBB
          C(829) = R(1,6, 2) * F(ISB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(830) = R(1,3, 2) * F(ISB,I) * G(ISB,I) /  9.0 * IMBB
          C(831) = R(1,2,17) * F(IUB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(832) = R(1,1,17) * F(IUB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(833) = R(1,1,22) * F(IDB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(834) = R(1,1,20) * F(IDB,I) * G(IUB,I) /  9.0 * IMNOBB
          C(835) = R(1,4,17) * F(IUB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(836) = R(1,4,22) * F(ICB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(837) = R(1,4,20) * F(ICB,I) * G(IUB,I) / 18.0 * IMNOBB
          C(838) = R(1,3,22) * F(IUB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(839) = R(1,1,17) * F(IUB,I) * G(ISB,I) /  9.0 * IMBB
          C(840) = R(1,2,17) * F(IDB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(841) = R(1,4,17) * F(IDB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(842) = R(1,4,22) * F(ICB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(843) = R(1,4,20) * F(ICB,I) * G(IDB,I) / 18.0 * IMNOBB
          C(844) = R(1,3,22) * F(IDB,I) * G(ISB,I) /  9.0 * IMNOBB
          C(845) = R(1,1,17) * F(IDB,I) * G(ISB,I) /  9.0 * IMBB
          C(846) = R(1,7,17) * F(ICB,I) * G(ISB,I) / 54.0 * IMNOBB
          C(847) = R(1,5,22) * F(ICB,I) * G(ISB,I) / 18.0 * IMNOBB
          C(848) = R(1,4,22) * F(ICB,I) * G(ISB,I) / 18.0 * IMBB
          DO 260 J=805,848
            T(1,2,I)=T(1,2,I)+C(J)
            T(1,8,I)=T(1,8,I)+C(J)
  260     CONTINUE
*
      END
*
      SUBROUTINE RQ6COP(SOURCE,TARGET)
      REAL*8 SOURCE(2,7),TARGET(2,7)
*
      DO 10 IW=1,2
        TARGET(IW,1)=SOURCE(3-IW,1)
        TARGET(IW,2)=SOURCE(3-IW,2)
        TARGET(IW,3)=SOURCE(3-IW,4)
        TARGET(IW,4)=SOURCE(3-IW,3)
        TARGET(IW,5)=SOURCE(3-IW,5)
        TARGET(IW,6)=SOURCE(3-IW,7)
        TARGET(IW,7)=SOURCE(3-IW,6)
   10 CONTINUE
*
      END
*
************************************************************************
* MATRIX ELEMENT FOR:  NOTHING --> Q QB Q' QB' Q'' QB'' + V            *
* THE VECTOR BOSON IS A W+ OR W-                                       *
************************************************************************
* CALLING PROCEDURE: FQ6W(N,PLAB,I1,I2,I3,I4,I5,I6,RESULT,IHELRN)        *
*   PLAB(4,10) : momenta of event                                      *
*   N          : number of gluons                                      *
*   I1,I3,I5   : LABELS OF QUARKS IN PLAB ARRAY                        *
*   I2,I4,I6   : LABELS OF ANTI-QUARKS IN PLAB ARRAY                   *
*   IHELRN     : =1 IF MC OVER HEL AMPLITUDES IS WANTED                *
*                                                                      *
*   OUTPUT: THE MATRIX ELEMENTS FOR THE SUBPROCESSES RETURNED IN       *
*   RESULT(2,7): FIRST INDEX IS CHARGE BOSON                           *
*                SECOND INDEX IS TYPE OF FLAVOUR COMBINATION           *
*                1 = EG. U DB C CB S SB                                *
*                2 = EG. U DB C CB C CB                                *
*                3 = EG. U DB U UB C CB                                *
*                4 = EG. U DB C CB D DB                                *
*                5 = EG. U DB U UB D DB                                *
*                6 = EG. U DB U UB U UB                                *
*                7 = EG. U DB D DB D DB                                *
*   NOT INCLUDED: WEAK AND STRONG COUPLING CONSTANTS!                  *
*                 STATISTICS, SPIN AVERAGES, COLOUR AVERAGES           *
************************************************************************
* INFORMATION: THE PARTICLES IN PLAB ARE ORDERED AS FOLLOWS:           *
*             1,2 INCOMING PARTICLES FROM P, PBAR                      *
*             3..? QUARKS IN FINAL STATE                               *
*             9,10 LEPTONS                                             *
* PARTICLES 1 AND 2 ARE ALWAYS FLIPPED: WE CONSIDER ALL PARTICLES      *
* TO BE OUTGOING.                                                      *
************************************************************************
      SUBROUTINE FQ6W(N,PLAB,I1,I2,I3,I4,I5,I6,RESULT,IHELRN)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),RESULT(2,7)
      DIMENSION P(0:3,NM),ISIGN(10)
      COMMON /ZLCUR/ ZLCUR(2,2,2)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      SAVE /ZLCUR/,/DOTPRO/,ISIGN
      DATA ISIGN /-1,-1,1,1,1,1,1,1,1,1/
*
* TRANSFORM VECTORS
      DO 10 MU=0,3
        NU=MU
        IF (MU.EQ.0) NU=4
        P(MU,1)=ISIGN(I1)*REAL(PLAB(NU,I1))
        P(MU,2)=ISIGN(I2)*REAL(PLAB(NU,I2))
        P(MU,3)=ISIGN(I3)*REAL(PLAB(NU,I3))
        P(MU,4)=ISIGN(I4)*REAL(PLAB(NU,I4))
        P(MU,5)=ISIGN(I5)*REAL(PLAB(NU,I5))
        P(MU,6)=ISIGN(I6)*REAL(PLAB(NU,I6))
        P(MU,9)=ISIGN(9)*REAL(PLAB(NU,9))
        P(MU,10)=ISIGN(10)*REAL(PLAB(NU,10))
   10 CONTINUE
*
* INITIALIZED THE SPINORS AND THEIR INPRODUCTS
*
      CALL SETSPV(P,6,6)
*
* INIT THE TWO LEPTON CURRENTS, INDEX3=1 --> W-
*
      DO 30 IX=1,2
        DO 30 IY=1,2
          ZLCUR(IX,IY,1)=ZKOD(IX,10)*ZKO(IY, 9)
          ZLCUR(IX,IY,2)=ZKOD(IX, 9)*ZKO(IY,10)
   30 CONTINUE
*
      CALL HELQ6W(RESULT)
*
      END
*
      SUBROUTINE HELQ6W(RESULT)
      IMPLICIT REAL (A-H,M-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 RESULT(2,7)
      DIMENSION HM(5:8,2,7),ZA(6)
      DIMENSION ZA1(6,5:8,2),ZA2(6,5:8,2),ZA3(6,5:8,2),
     .    ZA4(6,5:8,2),ZA5(6,5:8,2),ZA6(6,5:8,2),
     .    ZA13(6,5:8,2),ZA14(6,5:8,2),ZA25(6,5:8,2),
     .    ZA26(6,5:8,2),ZA27(6,5:8,2)
*
      CALL A1P11(ZA1)
      CALL A2P12(ZA2)
      CALL A3P9(ZA3)
      CALL A4P10(ZA4)
      CALL A5P7(ZA5)
      CALL A6P8(ZA6)
      CALL A13P23(ZA13)
      CALL A14P24(ZA14)
      CALL A25P35(ZA25)
      CALL A26P36(ZA26)
      CALL A27P33(ZA27)
*
      DO 100 IWC=1,2
*
* U DB C CB S SB
*
        HM(5,IWC,1)=SQRQ6(ZA1(1,5,IWC))
        HM(6,IWC,1)=SQRQ6(ZA1(1,6,IWC))
        HM(7,IWC,1)=SQRQ6(ZA1(1,7,IWC))
        HM(8,IWC,1)=SQRQ6(ZA1(1,8,IWC))
*
* U DB C CB C CB
*
        HM(6,IWC,2)=HM(6,IWC,1)+SQRQ6(ZA2(1,6,IWC))
        HM(7,IWC,2)=HM(7,IWC,1)+SQRQ6(ZA2(1,7,IWC))
        DO 22 ITYPE=5,8,3
          DO 24 I=1,6
            ZA(I)=ZA1(I,ITYPE,IWC)+ZA2(I,ITYPE,IWC)
   24     CONTINUE
          HM(ITYPE,IWC,2)=SQRQ6(ZA)
   22   CONTINUE
*
* U DB U UB C CB
*
        HM(5,IWC,3)=HM(5,IWC,1)+SQRQ6(ZA4(1,5,IWC))
        HM(6,IWC,3)=HM(6,IWC,1)+SQRQ6(ZA4(1,6,IWC))
        DO 32 ITYPE=7,8
          DO 34 I=1,6
            ZA(I)=ZA1(I,ITYPE,IWC)+ZA4(I,ITYPE,IWC)
   34     CONTINUE
          HM(ITYPE,IWC,3)=SQRQ6(ZA)
   32   CONTINUE
*
* U DB C CB D DB
*
        HM(5,IWC,4)=HM(5,IWC,1)+SQRQ6(ZA26(1,5,IWC))
        HM(7,IWC,4)=HM(7,IWC,1)+SQRQ6(ZA26(1,7,IWC))
        DO 42 ITYPE=6,8,2
          DO 44 I=1,6
            ZA(I)=ZA1(I,ITYPE,IWC)+ZA26(I,ITYPE,IWC)
   44     CONTINUE
          HM(ITYPE,IWC,4)=SQRQ6(ZA)
   42   CONTINUE
*
* U DB U UB D DB
*
        HM(5,IWC,5)=-HM(5,IWC,1)+HM(5,IWC,3)+HM(5,IWC,4)
     .             +SQRQ6(ZA27(1,5,IWC))
        DO 50 I=1,6
          ZA(I)=ZA4(I,6,IWC)+ZA27(I,6,IWC)
   50   CONTINUE
        HM(6,IWC,5)=HM(6,IWC,4)+SQRQ6(ZA)
        DO 52 I=1,6
          ZA(I)=ZA26(I,7,IWC)+ZA27(I,7,IWC)
   52   CONTINUE
        HM(7,IWC,5)=HM(7,IWC,3)+SQRQ6(ZA)
        DO 54 I=1,6
          ZA(I)=ZA1(I,8,IWC)+ZA4(I,8,IWC)+ZA26(I,8,IWC)+ZA27(I,8,IWC)
   54   CONTINUE
        HM(8,IWC,5)=SQRQ6(ZA)
*
* U DB U UB U UB
*
        DO 60 I=1,6
          ZA(I)=ZA3(I,5,IWC)+ZA4(I,5,IWC)
   60   CONTINUE
        RM1=SQRQ6(ZA)
        DO 62 I=1,6
          ZA(I)=ZA5(I,5,IWC)+ZA6(I,5,IWC)
   62   CONTINUE
        HM(5,IWC,6)=HM(5,IWC,2)+RM1+SQRQ6(ZA)
        DO 64 I=1,6
          ZA(I)=ZA1(I,6,IWC)+ZA6(I,6,IWC)
   64   CONTINUE
        RM1=SQRQ6(ZA)
        DO 66 I=1,6
          ZA(I)=ZA2(I,6,IWC)+ZA3(I,6,IWC)
   66   CONTINUE
        RM2=SQRQ6(ZA)
        DO 68 I=1,6
          ZA(I)=ZA4(I,6,IWC)+ZA5(I,6,IWC)
   68   CONTINUE
        HM(6,IWC,6)=RM1+RM2+SQRQ6(ZA)
        DO 610 I=1,6
          ZA(I)=ZA3(I,7,IWC)+ZA6(I,7,IWC)
  610   CONTINUE
        RM1=SQRQ6(ZA)
        DO 612 I=1,6
          ZA(I)=ZA2(I,7,IWC)+ZA5(I,7,IWC)
  612   CONTINUE
        HM(7,IWC,6)=HM(7,IWC,3)+RM1+SQRQ6(ZA)
        DO 614 I=1,6
          ZA(I)=ZA1(I,8,IWC)+ZA2(I,8,IWC)+ZA3(I,8,IWC)
     .         +ZA4(I,8,IWC)+ZA5(I,8,IWC)+ZA6(I,8,IWC)
  614   CONTINUE
        HM(8,IWC,6)=SQRQ6(ZA)
*
* U DB D DB D DB
*
        DO 70 I=1,6
          ZA(I)=ZA13(I,5,IWC)+ZA14(I,5,IWC)
   70   CONTINUE
        RM1=SQRQ6(ZA)
        DO 72 I=1,6
          ZA(I)=ZA25(I,5,IWC)+ZA26(I,5,IWC)
   72   CONTINUE
        HM(5,IWC,7)=HM(5,IWC,2)+RM1+SQRQ6(ZA)
        DO 710 I=1,6
          ZA(I)=ZA2(I,7,IWC)+ZA13(I,6,IWC)
  710   CONTINUE
        RM1=SQRQ6(ZA)
        DO 712 I=1,6
          ZA(I)=ZA14(I,6,IWC)+ZA25(I,6,IWC)
  712   CONTINUE
        HM(6,IWC,7)=HM(6,IWC,4)+RM1+SQRQ6(ZA)
        DO 74 I=1,6
          ZA(I)=ZA1(I,7,IWC)+ZA14(I,7,IWC)
   74   CONTINUE
        RM1=SQRQ6(ZA)
        DO 76 I=1,6
          ZA(I)=ZA2(I,6,IWC)+ZA25(I,7,IWC)
   76   CONTINUE
        RM2=SQRQ6(ZA)
        DO 78 I=1,6
          ZA(I)=ZA13(I,7,IWC)+ZA26(I,7,IWC)
   78   CONTINUE
        HM(7,IWC,7)=RM1+RM2+SQRQ6(ZA)
        DO 714 I=1,6
          ZA(I)=ZA1(I,8,IWC)+ZA2(I,8,IWC)+ZA13(I,8,IWC)
     .         +ZA14(I,8,IWC)+ZA25(I,8,IWC)+ZA26(I,8,IWC)
  714   CONTINUE
        HM(8,IWC,7)=SQRQ6(ZA)
*
  100 CONTINUE
*
      DO 110 IWC=1,2
        DO 110 ICASE=1,7
          RESULT(IWC,ICASE)=0.0
          DO 120 I=5,8
            RESULT(IWC,ICASE)=RESULT(IWC,ICASE)+HM(I,IWC,ICASE)
  120     CONTINUE
  110 CONTINUE
*
      END
*
      FUNCTION SQRQ6(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(6)
*
      RNCOL=3.0
      FACTOR=4.0*(RNCOL)**3
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*  Add factor 16 to account for different convention for weak coupling
      FACTOR=FACTOR*16.0
      RM1=0.0
      DO 22 I=1,6
        RM1=RM1+REAL(ZA(I)*CONJG(ZA(I)))
   22 CONTINUE
*
      Z1=ZA(1)+ZA(3)+ZA(5)
      Z2=ZA(2)+ZA(4)+ZA(6)
      RM2=-2.0*CF1*REAL(Z1*CONJG(Z2))
*
      Z=ZA(1)*CONJG(ZA(3))+ZA(3)*CONJG(ZA(5))
     . +ZA(5)*CONJG(ZA(1))+ZA(2)*CONJG(ZA(4))
     . +ZA(4)*CONJG(ZA(6))+ZA(6)*CONJG(ZA(2))
      RM3=2.0*CF2*REAL(Z)
*
      SQRQ6=FACTOR*(RM1+RM2+RM3)
*
      END
*
      SUBROUTINE A1P11(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2)
      DIMENSION ZR1(2,2,4),ZR11(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /1,2,3,4,5,6/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR1)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR11)
*
        DO 10 IWC=1,2
          ZB1(1)=-ZR1(IWC,2,1)+ZR1(IWC,2,2)+ZR11(IWC,2,3)+ZR11(IWC,2,4)
          ZB2(1)=(0.0,0.0)
          ZB3(1)=(0.0,0.0)
*
          ZB1(2)=(0.0,0.0)
          ZB2(2)=ZR11(IWC,2,2)+ZR11(IWC,2,3)+ZR1(IWC,2,4)+ZR11(IWC,2,4)
          ZB3(2)=(0.0,0.0)
*
          ZB1(3)=-ZR11(IWC,2,1)+ZR11(IWC,2,2)+ZR1(IWC,2,3)+ZR1(IWC,2,4)
          ZB2(3)=(0.0,0.0)
          ZB3(3)=(0.0,0.0)
*
          ZB1(4)=(0.0,0.0)
          ZB2(4)=ZR1(IWC,2,2)+ZR11(IWC,2,2)+ZR1(IWC,2,3)+ZR11(IWC,2,3)
          ZB3(4)=(0.0,0.0)
*
          ZB1(5)=(0.0,0.0)
          ZB2(5)=(0.0,0.0)
          ZB3(5)=ZR1(IWC,2,2)+ZR11(IWC,2,2)+ZR1(IWC,2,3)+ZR11(IWC,2,3)
     .          +ZR1(IWC,2,4)+ZR11(IWC,2,4)
*
          ZB1(6)=(0.0,0.0)
          ZB2(6)=ZR1(IWC,2,2)+ZR1(IWC,2,3)+ZR1(IWC,2,4)+ZR11(IWC,2,4)
          ZB3(6)=(0.0,0.0)
*
          DO 20 IP=1,6
            ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20     CONTINUE
*
   10 CONTINUE
*
      END
*
      SUBROUTINE A2P12(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2),ZR2(2,2,4),ZR12(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /1,2,5,4,3,6/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR2)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR12)
*
        DO 10 IWC=1,2
*
          ZB1(1)=(0.0,0.0)
          ZB2(1)=ZR12(IWC,2,2)+ZR12(IWC,2,3)+ZR2(IWC,2,4)+ZR12(IWC,2,4)
          ZB3(1)=(0.0,0.0)
*
          ZB1(2)=-ZR2(IWC,2,1)+ZR2(IWC,2,2)+ZR12(IWC,2,3)+ZR12(IWC,2,4)
          ZB2(2)=(0.0,0.0)
          ZB3(2)=(0.0,0.0)
*
          ZB1(3)=(0.0,0.0)
          ZB2(3)=ZR2(IWC,2,2)+ZR2(IWC,2,3)+ZR2(IWC,2,4)+ZR12(IWC,2,4)
          ZB3(3)=(0.0,0.0)
*
          ZB1(4)=(0.0,0.0)
          ZB2(4)=(0.0,0.0)
          ZB3(4)=ZR2(IWC,2,2)+ZR12(IWC,2,2)+ZR2(IWC,2,3)+ZR12(IWC,2,3)
     .          +ZR2(IWC,2,4)+ZR12(IWC,2,4)
*
          ZB1(5)=(0.0,0.0)
          ZB2(5)=ZR2(IWC,2,2)+ZR12(IWC,2,2)+ZR2(IWC,2,3)+ZR12(IWC,2,3)
          ZB3(5)=(0.0,0.0)
*
          ZB1(6)=-ZR12(IWC,2,1)+ZR12(IWC,2,2)+ZR2(IWC,2,3)+ZR2(IWC,2,4)
          ZB2(6)=(0.0,0.0)
          ZB3(6)=(0.0,0.0)
*
          DO 20 IP=1,6
            ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20     CONTINUE
*
   10 CONTINUE
*
      END
*
      SUBROUTINE A3P9(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2),ZR3(2,2,4),ZR9(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /3,2,5,4,1,6/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR3)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR9)
*
        DO 10 IWC=1,2
*
          ZB1(1)=(0.0,0.0)
          ZB2(1)=(0.0,0.0)
          ZB3(1)=ZR3(IWC,2,2)+ZR9(IWC,2,2)+ZR3(IWC,2,3)+ZR9(IWC,2,3)
     .          +ZR3(IWC,2,4)+ZR9(IWC,2,4)
*
          ZB1(2)=(0.0,0.0)
          ZB2(2)=ZR3(IWC,2,2)+ZR3(IWC,2,3)+ZR3(IWC,2,4)+ZR9(IWC,2,4)
          ZB3(2)=(0.0,0.0)
*
          ZB1(3)=-ZR3(IWC,2,1)+ZR3(IWC,2,2)+ZR9(IWC,2,3)+ZR9(IWC,2,4)
          ZB2(3)=(0.0,0.0)
          ZB3(3)=(0.0,0.0)
*
          ZB1(4)=(0.0,0.0)
          ZB2(4)=ZR9(IWC,2,2)+ZR9(IWC,2,3)+ZR3(IWC,2,4)+ZR9(IWC,2,4)
          ZB3(4)=(0.0,0.0)
*
          ZB1(5)=-ZR9(IWC,2,1)+ZR9(IWC,2,2)+ZR3(IWC,2,3)+ZR3(IWC,2,4)
          ZB2(5)=(0.0,0.0)
          ZB3(5)=(0.0,0.0)
*
          ZB1(6)=(0.0,0.0)
          ZB2(6)=ZR3(IWC,2,2)+ZR9(IWC,2,2)+ZR3(IWC,2,3)+ZR9(IWC,2,3)
          ZB3(6)=(0.0,0.0)
*
          DO 20 IP=1,6
            ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20     CONTINUE
*
   10 CONTINUE
*
      END
*
      SUBROUTINE A4P10(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2),ZR4(2,2,4),ZR10(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /3,2,1,4,5,6/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR4)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR10)
*
        DO 10 IWC=1,2
*
          ZB1(1)=(0.0,0.0)
          ZB2(1)=ZR4(IWC,2,2)+ZR10(IWC,2,2)+ZR4(IWC,2,3)+ZR10(IWC,2,3)
          ZB3(1)=(0.0,0.0)
*
          ZB1(2)=-ZR10(IWC,2,1)+ZR10(IWC,2,2)+ZR4(IWC,2,3)+ZR4(IWC,2,4)
          ZB2(2)=(0.0,0.0)
          ZB3(2)=(0.0,0.0)
*
          ZB1(3)=(0.0,0.0)
          ZB2(3)=ZR10(IWC,2,2)+ZR10(IWC,2,3)+ZR4(IWC,2,4)+ZR10(IWC,2,4)
          ZB3(3)=(0.0,0.0)
*
          ZB1(4)=-ZR4(IWC,2,1)+ZR4(IWC,2,2)+ZR10(IWC,2,3)+ZR10(IWC,2,4)
          ZB2(4)=(0.0,0.0)
          ZB3(4)=(0.0,0.0)
*
          ZB1(5)=(0.0,0.0)
          ZB2(5)=ZR4(IWC,2,2)+ZR4(IWC,2,3)+ZR4(IWC,2,4)+ZR10(IWC,2,4)
          ZB3(5)=(0.0,0.0)
*
          ZB1(6)=(0.0,0.0)
          ZB2(6)=(0.0,0.0)
          ZB3(6)=ZR4(IWC,2,2)+ZR10(IWC,2,2)+ZR4(IWC,2,3)+ZR10(IWC,2,3)
     .          +ZR4(IWC,2,4)+ZR10(IWC,2,4)
*
          DO 20 IP=1,6
            ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20     CONTINUE
*
   10 CONTINUE
*
      END
*
      SUBROUTINE A5P7(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2),ZR5(2,2,4),ZR7(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /5,2,1,4,3,6/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR5)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR7)
*
        DO 10 IWC=1,2
*
          ZB1(1)=-ZR7(IWC,2,1)+ZR7(IWC,2,2)+ZR5(IWC,2,3)+ZR5(IWC,2,4)
          ZB2(1)=(0.0,0.0)
          ZB3(1)=(0.0,0.0)
*
          ZB1(2)=(0.0,0.0)
          ZB2(2)=ZR5(IWC,2,2)+ZR7(IWC,2,2)+ZR5(IWC,2,3)+ZR7(IWC,2,3)
          ZB3(2)=(0.0,0.0)
*
          ZB1(3)=(0.0,0.0)
          ZB2(3)=(0.0,0.0)
          ZB3(3)=ZR5(IWC,2,2)+ZR7(IWC,2,2)+ZR5(IWC,2,3)+ZR7(IWC,2,3)
     .          +ZR5(IWC,2,4)+ZR7(IWC,2,4)
*
          ZB1(4)=(0.0,0.0)
          ZB2(4)=ZR5(IWC,2,2)+ZR5(IWC,2,3)+ZR5(IWC,2,4)+ZR7(IWC,2,4)
          ZB3(4)=(0.0,0.0)
*
          ZB1(5)=-ZR5(IWC,2,1)+ZR5(IWC,2,2)+ZR7(IWC,2,3)+ZR7(IWC,2,4)
          ZB2(5)=(0.0,0.0)
          ZB3(5)=(0.0,0.0)
*
          ZB1(6)=(0.0,0.0)
          ZB2(6)=ZR7(IWC,2,2)+ZR7(IWC,2,3)+ZR5(IWC,2,4)+ZR7(IWC,2,4)
          ZB3(6)=(0.0,0.0)
*
          DO 20 IP=1,6
            ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20     CONTINUE
*
   10 CONTINUE
*
      END
*
      SUBROUTINE A6P8(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2),ZR6(2,2,4),ZR8(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /5,2,3,4,1,6/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR6)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR8)
*
        DO 10 IWC=1,2
*
          ZB1(1)=(0.0,0.0)
          ZB2(1)=ZR6(IWC,2,2)+ZR6(IWC,2,3)+ZR6(IWC,2,4)+ZR8(IWC,2,4)
          ZB3(1)=(0.0,0.0)
*
          ZB1(2)=(0.0,0.0)
          ZB2(2)=(0.0,0.0)
          ZB3(2)=ZR6(IWC,2,2)+ZR8(IWC,2,2)+ZR6(IWC,2,3)+ZR8(IWC,2,3)
     .            +ZR6(IWC,2,4)+ZR8(IWC,2,4)
*
          ZB1(3)=(0.0,0.0)
          ZB2(3)=ZR6(IWC,2,2)+ZR8(IWC,2,2)+ZR6(IWC,2,3)+ZR8(IWC,2,3)
          ZB3(3)=(0.0,0.0)
*
          ZB1(4)=-ZR8(IWC,2,1)+ZR8(IWC,2,2)+ZR6(IWC,2,3)+ZR6(IWC,2,4)
          ZB2(4)=(0.0,0.0)
          ZB3(4)=(0.0,0.0)
*
          ZB1(5)=(0.0,0.0)
          ZB2(5)=ZR8(IWC,2,2)+ZR8(IWC,2,3)+ZR6(IWC,2,4)+ZR8(IWC,2,4)
          ZB3(5)=(0.0,0.0)
*
          ZB1(6)=-ZR6(IWC,2,1)+ZR6(IWC,2,2)+ZR8(IWC,2,3)+ZR8(IWC,2,4)
          ZB2(6)=(0.0,0.0)
          ZB3(6)=(0.0,0.0)
*
          DO 20 IP=1,6
            ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20     CONTINUE
*
   10 CONTINUE
*
      END
*
      SUBROUTINE A13P23(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2),ZR13(2,2,4),ZR23(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /1,4,3,6,5,2/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR13)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR23)
*
        DO 10 IWC=1,2
*
          ZB1(1)=-ZR23(IWC,2,1)+ZR23(IWC,2,2)
     .           +ZR13(IWC,2,3)+ZR13(IWC,2,4)
          ZB2(1)=(0.0,0.0)
          ZB3(1)=(0.0,0.0)
*
          ZB1(2)=(0.0,0.0)
          ZB2(2)=ZR13(IWC,2,2)+ZR13(IWC,2,3)+ZR13(IWC,2,4)+ZR23(IWC,2,4)
          ZB3(2)=(0.0,0.0)
*
          ZB1(3)=(0.0,0.0)
          ZB2(3)=(0.0,0.0)
          ZB3(3)=ZR13(IWC,2,2)+ZR23(IWC,2,2)+ZR13(IWC,2,3)+ZR23(IWC,2,3)
     .          +ZR13(IWC,2,4)+ZR23(IWC,2,4)
*
          ZB1(4)=(0.0,0.0)
          ZB2(4)=ZR23(IWC,2,2)+ZR23(IWC,2,3)+ZR13(IWC,2,4)+ZR23(IWC,2,4)
          ZB3(4)=(0.0,0.0)
*
          ZB1(5)=-ZR13(IWC,2,1)+ZR13(IWC,2,2)
     .           +ZR23(IWC,2,3)+ZR23(IWC,2,4)
          ZB2(5)=(0.0,0.0)
          ZB3(5)=(0.0,0.0)
*
          ZB1(6)=(0.0,0.0)
          ZB2(6)=ZR13(IWC,2,2)+ZR23(IWC,2,2)+ZR13(IWC,2,3)+ZR23(IWC,2,3)
          ZB3(6)=(0.0,0.0)
*
          DO 20 IP=1,6
            ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20     CONTINUE
*
   10 CONTINUE
*
      END
*
      SUBROUTINE A14P24(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2)
      DIMENSION ZR14(2,2,4),ZR24(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /1,4,3,2,5,6/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR24)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR14)
*
        DO 10 IWC=1,2
*
          ZB1(1)=(0.0,0.0)
          ZB2(1)=ZR14(IWC,2,2)+ZR14(IWC,2,3)+ZR14(IWC,2,4)+ZR24(IWC,2,4)
          ZB3(1)=(0.0,0.0)
*
         ZB1(2)=-ZR24(IWC,2,1)+ZR24(IWC,2,2)+ZR14(IWC,2,3)+ZR14(IWC,2,4)
         ZB2(2)=(0.0,0.0)
         ZB3(2)=(0.0,0.0)
*
         ZB1(3)=(0.0,0.0)
         ZB2(3)=ZR14(IWC,2,2)+ZR24(IWC,2,2)+ZR14(IWC,2,3)+ZR24(IWC,2,3)
         ZB3(3)=(0.0,0.0)
*
         ZB1(4)=-ZR14(IWC,2,1)+ZR14(IWC,2,2)+ZR24(IWC,2,3)+ZR24(IWC,2,4)
         ZB2(4)=(0.0,0.0)
         ZB3(4)=(0.0,0.0)
*
         ZB1(5)=(0.0,0.0)
         ZB2(5)=ZR24(IWC,2,2)+ZR24(IWC,2,3)+ZR14(IWC,2,4)+ZR24(IWC,2,4)
         ZB3(5)=(0.0,0.0)
*
         ZB1(6)=(0.0,0.0)
         ZB2(6)=(0.0,0.0)
         ZB3(6)=ZR14(IWC,2,2)+ZR24(IWC,2,2)+ZR14(IWC,2,3)+ZR24(IWC,2,3)
     .            +ZR14(IWC,2,4)+ZR24(IWC,2,4)
*
         DO 20 IP=1,6
           ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20    CONTINUE
*
   10 CONTINUE
      END
*
      SUBROUTINE A25P35(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2)
      DIMENSION ZR25(2,2,4),ZR35(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /1,6,3,2,5,4/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR25)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR35)
*
        DO 10 IWC=1,2
         ZB1(1)=(0.0,0.0)
         ZB2(1)=(0.0,0.0)
         ZB3(1)=ZR25(IWC,2,2)+ZR35(IWC,2,2)+ZR25(IWC,2,3)+ZR35(IWC,2,3)
     .          +ZR25(IWC,2,4)+ZR35(IWC,2,4)
*
         ZB1(2)=(0.0,0.0)
         ZB2(2)=ZR25(IWC,2,2)+ZR35(IWC,2,2)+ZR25(IWC,2,3)+ZR35(IWC,2,3)
         ZB3(2)=(0.0,0.0)
*
         ZB1(3)=-ZR25(IWC,2,1)+ZR25(IWC,2,2)+ZR35(IWC,2,3)+ZR35(IWC,2,4)
         ZB2(3)=(0.0,0.0)
         ZB3(3)=(0.0,0.0)
*
         ZB1(4)=(0.0,0.0)
         ZB2(4)=ZR25(IWC,2,2)+ZR25(IWC,2,3)+ZR25(IWC,2,4)+ZR35(IWC,2,4)
         ZB3(4)=(0.0,0.0)
*
         ZB1(5)=-ZR35(IWC,2,1)+ZR35(IWC,2,2)+ZR25(IWC,2,3)+ZR25(IWC,2,4)
         ZB2(5)=(0.0,0.0)
         ZB3(5)=(0.0,0.0)
*
         ZB1(6)=(0.0,0.0)
         ZB2(6)=ZR35(IWC,2,2)+ZR35(IWC,2,3)+ZR25(IWC,2,4)+ZR35(IWC,2,4)
         ZB3(6)=(0.0,0.0)
*
         DO 20 IP=1,6
           ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20    CONTINUE
*
   10 CONTINUE
      END
*
      SUBROUTINE A26P36(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2)
      DIMENSION ZR26(2,2,4),ZR36(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /1,6,3,4,5,2/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR36)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR26)
*
        DO 10 IWC=1,2
*
         ZB1(1)=(0.0,0.0)
         ZB2(1)=ZR26(IWC,2,2)+ZR36(IWC,2,2)+ZR26(IWC,2,3)+ZR36(IWC,2,3)
         ZB3(1)=(0.0,0.0)
*
         ZB1(2)=(0.0,0.0)
         ZB2(2)=(0.0,0.0)
         ZB3(2)=ZR26(IWC,2,2)+ZR36(IWC,2,2)+ZR26(IWC,2,3)+ZR36(IWC,2,3)
     .          +ZR26(IWC,2,4)+ZR36(IWC,2,4)
*
         ZB1(3)=(0.0,0.0)
         ZB2(3)=ZR36(IWC,2,2)+ZR36(IWC,2,3)+ZR26(IWC,2,4)+ZR36(IWC,2,4)
         ZB3(3)=(0.0,0.0)
*
         ZB1(4)=-ZR36(IWC,2,1)+ZR36(IWC,2,2)+ZR26(IWC,2,3)+ZR26(IWC,2,4)
         ZB2(4)=(0.0,0.0)
         ZB3(4)=(0.0,0.0)
*
         ZB1(5)=(0.0,0.0)
         ZB2(5)=ZR26(IWC,2,2)+ZR26(IWC,2,3)+ZR26(IWC,2,4)+ZR36(IWC,2,4)
         ZB3(5)=(0.0,0.0)
*
         ZB1(6)=-ZR26(IWC,2,1)+ZR26(IWC,2,2)+ZR36(IWC,2,3)+ZR36(IWC,2,4)
         ZB2(6)=(0.0,0.0)
         ZB3(6)=(0.0,0.0)
*
         DO 20 IP=1,6
          ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20    CONTINUE
*
   10 CONTINUE
*
      END
*
      SUBROUTINE A27P33(ZA)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZA(1:6,5:8,2)
      DIMENSION ZR27(2,2,4),ZR33(2,2,4)
      DIMENSION ZB1(6),ZB2(6),ZB3(6)
      DIMENSION IP1(5:8),IP2(5:8)
      SAVE I1,I2,I3,I4,I5,I6,IP1,IP2,RNCOL
      DATA I1,I2,I3,I4,I5,I6 /3,6,1,4,5,2/
      DATA IP1 /4,3,2,1/
      DATA IP2 /4,2,3,1/
      DATA RNCOL /3.0/
*
      CF1=1.0/RNCOL
      CF2=CF1/RNCOL
*
      DO 10 ITYPE=5,8
*
        CALL Q6G0(I1,I2,I3,I4,I5,I6,IP1(ITYPE),ZR33)
        CALL Q6G0(I1,I2,I5,I6,I3,I4,IP2(ITYPE),ZR27)
*
        DO 10 IWC=1,2
*
         ZB1(1)=-ZR33(IWC,2,1)+ZR33(IWC,2,2)+ZR27(IWC,2,3)+ZR27(IWC,2,4)
         ZB2(1)=(0.0,0.0)
         ZB3(1)=(0.0,0.0)
*
         ZB1(2)=(0.0,0.0)
         ZB2(2)=ZR33(IWC,2,2)+ZR33(IWC,2,3)+ZR27(IWC,2,4)+ZR33(IWC,2,4)
         ZB3(2)=(0.0,0.0)
*
         ZB1(3)=(0.0,0.0)
         ZB2(3)=(0.0,0.0)
         ZB3(3)=ZR27(IWC,2,2)+ZR33(IWC,2,2)+ZR27(IWC,2,3)+ZR33(IWC,2,3)
     .          +ZR27(IWC,2,4)+ZR33(IWC,2,4)
*
         ZB1(4)=(0.0,0.0)
         ZB2(4)=ZR27(IWC,2,2)+ZR33(IWC,2,2)+ZR27(IWC,2,3)+ZR33(IWC,2,3)
         ZB3(4)=(0.0,0.0)
*
         ZB1(5)=-ZR27(IWC,2,1)+ZR27(IWC,2,2)+ZR33(IWC,2,3)+ZR33(IWC,2,4)
         ZB2(5)=(0.0,0.0)
         ZB3(5)=(0.0,0.0)
*
         ZB1(6)=(0.0,0.0)
         ZB2(6)=ZR27(IWC,2,2)+ZR27(IWC,2,3)+ZR27(IWC,2,4)+ZR33(IWC,2,4)
         ZB3(6)=(0.0,0.0)
*
         DO 20 IP=1,6
           ZA(IP,ITYPE,IWC)=ZB1(IP)+CF1*ZB2(IP)+CF2*ZB3(IP)
   20    CONTINUE
*
   10 CONTINUE
      END
*
************************************************************************
* SUBROUTINE Q6G0                                                      *
*             Q1  Q2  Q3  Q4  Q5  Q6                                   *
*   ITYPE=1 ; +   -   +   -   +   -                                    *
*   ITYPE=2 ; +   -   +   -   -   +                                    *
*   ITYPE=3 ; +   -   -   +   +   -                                    *
*   ITYPE=4 ; +   -   -   +   -   +                                    *
*                                                                      *
* ZRES(2,2,4) : W+/W- ; NORMAL/CC ; FUNCTION NUMBER (M1-M4)            *
************************************************************************
      SUBROUTINE Q6G0(I1,I2,I3,I4,I5,I6,ITYPE,ZRES)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /ZLCUR/ ZLCUR(2,2,2)
      DIMENSION Z(2,2),ZC(2,2),ZRES(2,2,4)
      SAVE /ZLCUR/
*
      CALL M1(I1,I2,I3,I4,I5,I6,Z,ZC,ITYPE)
      ZRES(2,1,1)=ZMUL(Z ,ZLCUR(1,1,1))
      ZRES(1,1,1)=ZMUL(Z ,ZLCUR(1,1,2))
      ZRES(2,2,1)=ZMUL(ZC,ZLCUR(1,1,1))
      ZRES(1,2,1)=ZMUL(ZC,ZLCUR(1,1,2))
*
      CALL M2(I1,I2,I3,I4,I5,I6,Z,ZC,ITYPE)
      ZRES(2,1,2)=ZMUL(Z ,ZLCUR(1,1,1))
      ZRES(1,1,2)=ZMUL(Z ,ZLCUR(1,1,2))
      ZRES(2,2,2)=ZMUL(ZC,ZLCUR(1,1,1))
      ZRES(1,2,2)=ZMUL(ZC,ZLCUR(1,1,2))
*
      CALL M3(I1,I2,I3,I4,I5,I6,Z,ZC,ITYPE)
      ZRES(2,1,3)=ZMUL(Z ,ZLCUR(1,1,1))
      ZRES(1,1,3)=ZMUL(Z ,ZLCUR(1,1,2))
      ZRES(2,2,3)=ZMUL(ZC,ZLCUR(1,1,1))
      ZRES(1,2,3)=ZMUL(ZC,ZLCUR(1,1,2))
*
      CALL M4(I1,I2,I3,I4,I5,I6,Z,ZC,ITYPE)
      ZRES(2,1,4)=ZMUL(Z ,ZLCUR(1,1,1))
      ZRES(1,1,4)=ZMUL(Z ,ZLCUR(1,1,2))
      ZRES(2,2,4)=ZMUL(ZC,ZLCUR(1,1,1))
      ZRES(1,2,4)=ZMUL(ZC,ZLCUR(1,1,2))
*
      END
*
      SUBROUTINE M1(I1,I2,I3,I4,I5,I6,Z,ZC,ITYPE)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION Z(2,2),ZC(2,2)
*
      IF (ITYPE.EQ.1) CALL M1FUN(I1,I2,I3,I4,I5,I6,Z)
      IF (ITYPE.EQ.2) CALL M1FUN(I1,I2,I3,I4,I6,I5,Z)
      IF (ITYPE.EQ.3) CALL M1FUN(I1,I2,I4,I3,I5,I6,Z)
      IF (ITYPE.EQ.4) CALL M1FUN(I1,I2,I4,I3,I6,I5,Z)
      CALL CONGAT(Z,ZC)
*
      END
*
      SUBROUTINE M1FUN(I1,I2,I3,I4,I5,I6,Z)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z(2,2),Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      P34=2.0*RR(I3,I4)
      P56=2.0*RR(I5,I6)
      P3456=P34+P56+2.0*(RR(I3,I5)+RR(I3,I6)+RR(I4,I5)+RR(I4,I6))
      CALL M1INT(I1,I2,I3,I4,I5,I6,Z1,P3456)
      CALL M1INT(I2,I1,I4,I3,I6,I5,Z2,P3456)
      P0=1.0/(P34*P56*P3456)
      DO 10 IAD=1,2
        DO 10 IB=1,2
          Z(IAD,IB)=P0*(Z1(IAD,IB)-CONJG(Z2(IB,IAD)))
   10 CONTINUE
*
      END
*
      SUBROUTINE M1INT(I1,I2,I3,I4,I5,I6,ZM,P3456)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION ZM(2,2),ZV1(2),ZV2(2),ZV3(3)
      SAVE /DOTPRO/
*
      P23456=P3456+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,I5)+RR(I2,I6))
      Z1= ZD(I3,I5)*ZUD(I4,I6)/P23456/2.0
      Z2= ZUD(I2,I6)*(ZUD(I5,I4)*ZD(I5,I3)+ZUD(I6,I4)*ZD(I6,I3))/P23456
      Z3=-ZUD(I2,I4)*(ZUD(I3,I6)*ZD(I3,I5)+ZUD(I4,I6)*ZD(I4,I5))/P23456
      DO 10 IB=1,2
        ZV1(IB)=(P23456-4.0*(RR(I2,I5)+RR(I2,I6)+RR(I5,I6)))
     .          *ZKO(IB,I2)
     .         -2.0*(ZD(I5,I3)*ZUD(I5,I2)+ZD(I6,I3)*ZUD(I6,I2))
     .          *ZKO(IB,I3)
     .         -2.0*(ZD(I5,I4)*ZUD(I5,I2)+ZD(I6,I4)*ZUD(I6,I2))
     .          *ZKO(IB,I4)
        ZV2(IB)=ZD(I2,I5)*ZKO(IB,I2)+ZD(I3,I5)*ZKO(IB,I3)
     .         +ZD(I4,I5)*ZKO(IB,I4)+ZD(I6,I5)*ZKO(IB,I6)
        ZV3(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .         +ZD(I5,I3)*ZKO(IB,I5)+ZD(I6,I3)*ZKO(IB,I6)
   10 CONTINUE
      DO 20 IB=1,2
        Z0=Z1*ZV1(IB)+Z2*ZV2(IB)+Z3*ZV3(IB)
        DO 20 IAD=1,2
          ZM(IAD,IB)=Z0*ZKOD(IAD,I1)
   20 CONTINUE
*
      END
*
      SUBROUTINE M2(I1,I2,I3,I4,I5,I6,Z,ZC,ITYPE)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION Z(2,2),ZC(2,2)
*
      IF (ITYPE.EQ.1) CALL M2FUN1(I1,I2,I3,I4,I5,I6,Z)
      IF (ITYPE.EQ.2) CALL M2FUN1(I1,I2,I3,I4,I6,I5,Z)
      IF (ITYPE.EQ.3) CALL M2FUN2(I1,I2,I3,I4,I5,I6,Z)
      IF (ITYPE.EQ.4) CALL M2FUN2(I1,I2,I3,I4,I6,I5,Z)
      CALL CONGAT(Z,ZC)
*
      END
*
      SUBROUTINE M2FUN1(I1,I2,I3,I4,I5,I6,ZM)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION ZM(2,2),ZV1(2),ZV2(2)
      SAVE /DOTPRO/
*
      P56=2.0*RR(I5,I6)
      P456=P56+2.0*(RR(I4,I5)+RR(I4,I6))
      P3456=P456+2.0*(RR(I3,I4)+RR(I3,I5)+RR(I3,I6))
      P23456=P3456+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,I5)+RR(I2,I6))
      P13456=P3456+2.0*(RR(I1,I3)+RR(I1,I4)+RR(I1,I5)+RR(I1,I6))
      Z0=ZUD(I6,I4)/(P56*P456*P3456)
      Z1=Z0*(ZD(I4,I5)*ZUD(I4,I2)+ZD(I6,I5)*ZUD(I6,I2))/P23456
      Z2=-Z0*ZD(I1,I3)/P13456
      DO 10 IB=1,2
        ZV1(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .         +ZD(I5,I3)*ZKO(IB,I5)+ZD(I6,I3)*ZKO(IB,I6)
   10 CONTINUE
      DO 20 IAD=1,2
        ZV2(IAD)=ZD(I5,I4)
     .           *(ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .           +ZUD(I5,I4)*ZKOD(IAD,I5)+ZUD(I6,I4)*ZKOD(IAD,I6))
     .           +ZD(I5,I6)
     .           *(ZUD(I1,I6)*ZKOD(IAD,I1)+ZUD(I3,I6)*ZKOD(IAD,I3)
     .           +ZUD(I4,I6)*ZKOD(IAD,I4)+ZUD(I5,I6)*ZKOD(IAD,I5))
   20 CONTINUE
      DO 30 IAD=1,2
        ZL1=Z1*ZKOD(IAD,I1)
        ZL2=Z2*ZV2(IAD)
        DO 30 IB=1,2
          ZM(IAD,IB)=ZL1*ZV1(IB)+ZL2*ZKO(IB,I2)
   30 CONTINUE
*
      END
*
      SUBROUTINE M2FUN2(I1,I2,I3,I4,I5,I6,ZM)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZM(2,2),Z1(2,2)
*
      CALL M2FUN1(I2,I1,I3,I4,I6,I5,Z1)
      DO 10 IAD=1,2
        DO 10 IB=1,2
          ZM(IAD,IB)=-CONJG(Z1(IB,IAD))
   10 CONTINUE
*
      END
*
      SUBROUTINE M3(I1,I2,I3,I4,I5,I6,Z,ZC,ITYPE)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION Z(2,2),ZC(2,2)
*
      IF (ITYPE.EQ.1) CALL M3FUN1(I1,I2,I3,I4,I5,I6,Z)
      IF (ITYPE.EQ.2) CALL M3FUN1(I1,I2,I3,I4,I6,I5,Z)
      IF (ITYPE.EQ.3) CALL M3FUN2(I1,I2,I3,I4,I5,I6,Z)
      IF (ITYPE.EQ.4) CALL M3FUN2(I1,I2,I3,I4,I6,I5,Z)
      CALL CONGAT(Z,ZC)
      END
*
      SUBROUTINE M3FUN1(I1,I2,I3,I4,I5,I6,ZM)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZM(2,2),Z1(2,2)
*
      CALL M2FUN2(I1,I2,I4,I3,I5,I6,Z1)
      DO 10 IAD=1,2
        DO 10 IB=1,2
          ZM(IAD,IB)=-Z1(IAD,IB)
   10 CONTINUE
*
      END
*
      SUBROUTINE M3FUN2(I1,I2,I3,I4,I5,I6,ZM)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZM(2,2),Z1(2,2)
*
      CALL M2FUN1(I1,I2,I4,I3,I5,I6,Z1)
      DO 10 IAD=1,2
        DO 10 IB=1,2
          ZM(IAD,IB)=-Z1(IAD,IB)
   10 CONTINUE
*
      END
*
      SUBROUTINE M4(I1,I2,I3,I4,I5,I6,Z,ZC,ITYPE)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION Z(2,2),ZC(2,2)
*
      IF (ITYPE.EQ.1) CALL M4FUN(I1,I2,I3,I4,I5,I6,Z)
      IF (ITYPE.EQ.2) CALL M4FUN(I1,I2,I3,I4,I6,I5,Z)
      IF (ITYPE.EQ.3) CALL M4FUN(I1,I2,I4,I3,I5,I6,Z)
      IF (ITYPE.EQ.4) CALL M4FUN(I1,I2,I4,I3,I6,I5,Z)
      CALL CONGAT(Z,ZC)
      END
*
      SUBROUTINE M4FUN(I1,I2,I3,I4,I5,I6,ZM)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION ZM(2,2),ZV1(2),ZV2(2),ZV3(2),ZV4(2)
      SAVE /DOTPRO/
*
      P34=2.0*RR(I3,I4)
      P56=2.0*RR(I5,I6)
      P134=P34+2.0*(RR(I1,I3)+RR(I1,I4))
      P256=P56+2.0*(RR(I2,I5)+RR(I2,I6))
      P13456=P134+P56+2.0*(RR(I1,I5)+RR(I1,I6)+RR(I3,I5)+RR(I3,I6)
     .                    +RR(I4,I5)+RR(I4,I6))
      P23456=P256+P34+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I3,I5)+RR(I3,I6)
     .                    +RR(I4,I5)+RR(I4,I6))
      Z0=1.0/(P34*P56)
      Z1=Z0*ZUD(I2,I6)*(ZD(I2,I5)*ZUD(I2,I4)+ZD(I6,I5)*ZUD(I6,I4))
     .  /(P256*P23456)
      Z2=-Z0*ZD(I1,I3)*ZUD(I2,I6)/(P134*P256)
      Z3=Z0*ZD(I1,I3)*(ZD(I1,I5)*ZUD(I1,I4)+ZD(I3,I5)*ZUD(I3,I4))
     .  /(P134*P13456)
      DO 10 IB=1,2
        ZV1(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I6,I3)*ZKO(IB,I6)
     .         +ZD(I4,I3)*ZKO(IB,I4)+ZD(I5,I3)*ZKO(IB,I5)
        ZV2(IB)=ZD(I2,I5)*ZKO(IB,I2)+ZD(I6,I5)*ZKO(IB,I6)
   10 CONTINUE
      DO 20 IAD=1,2
        ZV3(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
        ZV4(IAD)=ZUD(I1,I6)*ZKOD(IAD,I1)+ZUD(I3,I6)*ZKOD(IAD,I3)
     .          +ZUD(I4,I6)*ZKOD(IAD,I4)+ZUD(I5,I6)*ZKOD(IAD,I5)
   20 CONTINUE
      DO 30 IAD=1,2
        ZL1=Z1*ZKOD(IAD,I1)
        ZL2=Z2*ZV3(IAD)
        ZL3=Z3*ZV4(IAD)
        DO 30 IB=1,2
          ZM(IAD,IB)=ZL1*ZV1(IB)+ZL2*ZV2(IB)+ZL3*ZKO(IB,I2)
   30 CONTINUE
*
      END
