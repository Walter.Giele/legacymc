************************************************************************
* MEQ4W calls the various r sb t tb subprocesses.                      *
************************************************************************
* Table of subprocesses T(2,1:4,6): with first index = W+, W-          *
* Second index:  1 = U DB     2 = C SB    3 = UB D    4 = CB S         *
************************************************************************
* Table of subprocesses T(2,5:9,6):   with first index = W+, W-        *
* Second index:  5 = GG   6= GQ + GQB  7= QG + QBG 8= QQ + QBQB 9=QQB  *
************************************************************************
      SUBROUTINE MEQ4W(TOT,T)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      DIMENSION T(2,9,6),R(2,3,21),C(160)
      SAVE IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG,/MOM/,/STRUCF/
      DATA IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG
     .    / 1, 2, 3, 4, 5,  6,  7,  8,  9, 10,11/
*
      IF (IMANNR.GT.1) THEN
        DO 4 I1=1,2
          DO 4 I2=1,3
            DO 4 I3=1,21
              R(I1,I2,I3)=1D0
              IF (IMANNR.EQ.3) R(I1,I2,I3)=1D0/WHAT**NJET
    4   CONTINUE
      END IF
*
* Clear statistics arrays
*
      DO 5 I=1,2
        DO 5 J=1,9
          DO 5 K=1,6
            T(I,J,K)=0.0
    5 CONTINUE
*
* Set Delta functions for having a B Bbar pair in the final state.
*
      IMNOBB=1-IMQ
      IMBB=1
*
      IFACM0=IFAC(NJET-2)
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ4W(NJET-2,PLAB,3,1,4,2,R(1,1,1),IHELRN)
        CALL RCOPY(R(1,1,1),R(1,1,2))
        CALL FQ4W(NJET-2,PLAB,2,3,1,4,R(1,1,3),IHELRN)
        CALL RCOPY(R(1,1,3),R(1,1,4))
        CALL FQ4W(NJET-2,PLAB,1,2,3,4,R(1,1,5),IHELRN)
        CALL RCOPY(R(1,1,5),R(1,1,6))
        CALL FQ4W(NJET-2,PLAB,3,1,2,4,R(1,1,7),IHELRN)
        CALL RCOPY(R(1,1,7),R(1,1,8))
        CALL FQ4W(NJET-2,PLAB,3,2,1,4,R(1,1,9),IHELRN)
        CALL RCOPY(R(1,1,9),R(1,1,10))
        CALL FQ4W(NJET-2,PLAB,3,4,2,1,R(1,1,11),IHELRN)
        CALL RCOPY(R(1,1,11),R(1,1,12))
      END IF
*
      DO 10 I=1,6
        IF (ITRY(I).NE.0) THEN
* Q Q
          C(  1)=R(2,1,1) *F(ID ,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(  2)=R(2,1,1) *F(ID ,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(  3)=R(2,3,1) *F(ID ,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(  4)=R(2,2,1) *F(ID ,I)*G(IU ,I)/18.D0 / IFACM0 * IMNOBB
          C(  5)=R(2,2,4) *F(IU ,I)*G(ID ,I)/18.D0 / IFACM0 * IMNOBB
          C(  6)=R(2,1,4) *F(IC ,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(  7)=R(2,1,4) *F(IS ,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(  8)=R(2,1,4) *F(ID ,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(  9)=R(2,1,4) *F(IU ,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 10)=R(2,2,4) *F(IC ,I)*G(IS ,I)/18.D0 / IFACM0 * IMNOBB
          C( 11)=R(2,2,1) *F(IS ,I)*G(IC ,I)/18.D0 / IFACM0 * IMNOBB
          C( 12)=R(2,3,1) *F(IS ,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 13)=R(2,1,1) *F(IS ,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 14)=R(2,1,1) *F(IS ,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 15)=R(1,1,4) *F(IC ,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 16)=R(1,1,4) *F(IS ,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 17)=R(1,2,4) *F(ID ,I)*G(IU ,I)/18.D0 / IFACM0 * IMNOBB
          C( 18)=R(1,1,1) *F(IU ,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 19)=R(1,1,1) *F(IU ,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 20)=R(1,2,1) *F(IU ,I)*G(ID ,I)/18.D0 / IFACM0 * IMNOBB
          C( 21)=R(1,3,1) *F(IU ,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 22)=R(1,3,1) *F(IC ,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 23)=R(1,2,1) *F(IC ,I)*G(IS ,I)/18.D0 / IFACM0 * IMNOBB
          C( 24)=R(1,1,1) *F(IC ,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 25)=R(1,1,1) *F(IC ,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 26)=R(1,2,4) *F(IS ,I)*G(IC ,I)/18.D0 / IFACM0 * IMNOBB
          C( 27)=R(1,1,4) *F(ID ,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 28)=R(1,1,4) *F(IU ,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
* QB QB
          C( 29)=R(2,2,2) *F(IUB,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 30)=R(2,3,2) *F(IUB,I)*G(IDB,I)/18.D0 / IFACM0 * IMNOBB
          C( 31)=R(2,1,2) *F(IUB,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 32)=R(2,1,2) *F(IUB,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 33)=R(2,3,3) *F(IDB,I)*G(IUB,I)/18.D0 / IFACM0 * IMNOBB
          C( 34)=R(2,1,3) *F(ISB,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 35)=R(2,1,3) *F(ICB,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 36)=R(2,1,3) *F(IUB,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 37)=R(2,1,3) *F(IDB,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 38)=R(2,3,3) *F(ISB,I)*G(ICB,I)/18.D0 / IFACM0 * IMNOBB
          C( 39)=R(2,1,2) *F(ICB,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 40)=R(2,1,2) *F(ICB,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 41)=R(2,3,2) *F(ICB,I)*G(ISB,I)/18.D0 / IFACM0 * IMNOBB
          C( 42)=R(2,2,2) *F(ICB,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 43)=R(1,3,3) *F(IUB,I)*G(IDB,I)/18.D0 / IFACM0 * IMNOBB
          C( 44)=R(1,3,2) *F(IDB,I)*G(IUB,I)/18.D0 / IFACM0 * IMNOBB
          C( 45)=R(1,2,2) *F(IDB,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 46)=R(1,1,2) *F(IDB,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 47)=R(1,1,2) *F(IDB,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 48)=R(1,1,3) *F(ISB,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 49)=R(1,1,3) *F(ICB,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 50)=R(1,1,3) *F(IUB,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 51)=R(1,1,3) *F(IDB,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 52)=R(1,1,2) *F(ISB,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 53)=R(1,1,2) *F(ISB,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 54)=R(1,2,2) *F(ISB,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 55)=R(1,3,2) *F(ISB,I)*G(ICB,I)/18.D0 / IFACM0 * IMNOBB
          C( 56)=R(1,3,3) *F(ICB,I)*G(ISB,I)/18.D0 / IFACM0 * IMNOBB
* Q QB
          C( 57)=R(2,2,10)*F(IU ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 58)=R(2,1,6) *F(ID ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 59)=R(2,1,6) *F(ID ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 60)=R(2,1,6) *F(ID ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMBB
          C( 61)=R(2,3,6) *F(ID ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 62)=R(2,2,6) *F(ID ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 63)=R(2,3,7) *F(ID ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 64)=R(2,1,7) *F(ID ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 65)=R(2,1,7) *F(ID ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 66)=R(2,1,10)*F(IC ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 67)=R(2,1,11)*F(IC ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 68)=R(2,1,10)*F(IS ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 69)=R(2,1,11)*F(IS ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 70)=R(2,1,11)*F(IU ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 71)=R(2,1,10)*F(IU ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 72)=R(2,1,11)*F(ID ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 73)=R(2,1,10)*F(ID ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 74)=R(2,2,10)*F(IC ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 75)=R(2,1,7) *F(IS ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 76)=R(2,1,7) *F(IS ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 77)=R(2,3,7) *F(IS ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 78)=R(2,1,6) *F(IS ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 79)=R(2,2,6) *F(IS ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 80)=R(2,3,6) *F(IS ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 81)=R(2,1,6) *F(IS ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 82)=R(2,1,6) *F(IS ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMBB
          C( 83)=R(1,1,10)*F(IC ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 84)=R(1,1,11)*F(IC ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 85)=R(1,1,10)*F(IS ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 86)=R(1,1,11)*F(IS ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 87)=R(1,2,10)*F(ID ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 88)=R(1,3,7) *F(IU ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 89)=R(1,1,6) *F(IU ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 90)=R(1,1,6) *F(IU ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 91)=R(1,1,6) *F(IU ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMBB
          C( 92)=R(1,2,6) *F(IU ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 93)=R(1,3,6) *F(IU ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 94)=R(1,1,7) *F(IU ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 95)=R(1,1,7) *F(IU ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 96)=R(1,1,7) *F(IC ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 97)=R(1,1,7) *F(IC ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 98)=R(1,1,6) *F(IC ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C( 99)=R(1,3,6) *F(IC ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C(100)=R(1,2,6) *F(IC ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C(101)=R(1,1,6) *F(IC ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C(102)=R(1,1,6) *F(IC ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMBB
          C(103)=R(1,3,7) *F(IC ,I)*G(ICB,I)/ 9.D0 / IFACM0 * IMNOBB
          C(104)=R(1,2,10)*F(IS ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C(105)=R(1,1,11)*F(ID ,I)*G(IDB,I)/ 9.D0 / IFACM0 * IMNOBB
          C(106)=R(1,1,10)*F(ID ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
          C(107)=R(1,1,11)*F(IU ,I)*G(IUB,I)/ 9.D0 / IFACM0 * IMNOBB
          C(108)=R(1,1,10)*F(IU ,I)*G(ISB,I)/ 9.D0 / IFACM0 * IMNOBB
* QB Q
          C(109)=R(2,1,8) *F(IUB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(110)=R(2,1,8) *F(IUB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(111)=R(2,1,5) *F(IUB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(112)=R(2,1,5) *F(IUB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(113)=R(2,1,5) *F(IUB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMBB
          C(114)=R(2,3,5) *F(IUB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(115)=R(2,2,5) *F(IUB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(116)=R(2,2,8) *F(IUB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(117)=R(2,3,9) *F(IDB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(118)=R(2,1,12)*F(ISB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(119)=R(2,1,9) *F(ISB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(120)=R(2,1,12)*F(ICB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(121)=R(2,1,9) *F(ICB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(122)=R(2,1,9) *F(IUB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(123)=R(2,1,12)*F(IUB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(124)=R(2,1,9) *F(IDB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(125)=R(2,1,12)*F(IDB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(126)=R(2,3,9) *F(ISB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(127)=R(2,2,8) *F(ICB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(128)=R(2,1,5) *F(ICB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(129)=R(2,2,5) *F(ICB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(130)=R(2,3,5) *F(ICB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(131)=R(2,1,5) *F(ICB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(132)=R(2,1,5) *F(ICB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMBB
          C(133)=R(2,1,8) *F(ICB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(134)=R(2,1,8) *F(ICB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(135)=R(1,3,9) *F(IUB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(136)=R(1,1,8) *F(IDB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(137)=R(1,1,8) *F(IDB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(138)=R(1,2,8) *F(IDB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(139)=R(1,1,5) *F(IDB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(140)=R(1,1,5) *F(IDB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(141)=R(1,1,5) *F(IDB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMBB
          C(142)=R(1,2,5) *F(IDB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(143)=R(1,3,5) *F(IDB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(144)=R(1,1,12)*F(ISB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(145)=R(1,1,9) *F(ISB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(146)=R(1,1,12)*F(ICB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(147)=R(1,1,9) *F(ICB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(148)=R(1,1,9) *F(IUB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(149)=R(1,1,12)*F(IUB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(150)=R(1,1,9) *F(IDB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(151)=R(1,1,12)*F(IDB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(152)=R(1,1,5) *F(ISB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(153)=R(1,3,5) *F(ISB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(154)=R(1,2,5) *F(ISB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(155)=R(1,1,5) *F(ISB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(156)=R(1,1,5) *F(ISB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMBB
          C(157)=R(1,2,8) *F(ISB,I)*G(IS ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(158)=R(1,1,8) *F(ISB,I)*G(ID ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(159)=R(1,1,8) *F(ISB,I)*G(IU ,I)/ 9.D0 / IFACM0 * IMNOBB
          C(160)=R(1,3,9) *F(ICB,I)*G(IC ,I)/ 9.D0 / IFACM0 * IMNOBB
* SUM SUBPROCESS
          DO 20 J=1,7
            T(2,3,I)=T(2,3,I)+C(J)
            T(2,4,I)=T(2,4,I)+C(7+J)
            T(1,1,I)=T(1,1,I)+C(14+J)
            T(1,2,I)=T(1,2,I)+C(21+J)
            T(2,3,I)=T(2,3,I)+C(28+J)
            T(2,4,I)=T(2,4,I)+C(35+J)
            T(1,1,I)=T(1,1,I)+C(42+J)
            T(1,2,I)=T(1,2,I)+C(49+J)
            T(2,8,I)=T(2,8,I)+C(J)+C(7+J)+C(28+J)+C(35+J)
            T(1,8,I)=T(1,8,I)+C(14+J)+C(21+J)+C(42+J)+C(49+J)
   20     CONTINUE
          DO 30 J=1,13
            T(2,3,I)=T(2,3,I)+C(56+J)
            T(2,4,I)=T(2,4,I)+C(69+J)
            T(1,1,I)=T(1,1,I)+C(82+J)
            T(1,2,I)=T(1,2,I)+C(95+J)
            T(2,3,I)=T(2,3,I)+C(108+J)
            T(2,4,I)=T(2,4,I)+C(121+J)
            T(1,1,I)=T(1,1,I)+C(134+J)
            T(1,2,I)=T(1,2,I)+C(147+J)
            T(2,9,I)=T(2,9,I)+C(56+J)+C(69+J)+C(108+J)+C(121+J)
            T(1,9,I)=T(1,9,I)+C(82+J)+C(95+J)+C(134+J)+C(147+J)
   30     CONTINUE
        END IF
   10 CONTINUE
*
      IF (NJET.EQ.2) GOTO 999
*
      IFACM1=IFAC(NJET-3)
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ4W(NJET-2,PLAB,1,3,4,5,R(1,1,13),IHELRN)
        CALL RCOPY(R(1,1,13),R(1,1,14))
        CALL FQ4W(NJET-2,PLAB,3,4,1,5,R(1,1,15),IHELRN)
        CALL RCOPY(R(1,1,15),R(1,1,16))
        CALL FQ4W(NJET-2,PLAB,2,3,4,5,R(1,1,17),IHELRN)
        CALL RCOPY(R(1,1,17),R(1,1,18))
        CALL FQ4W(NJET-2,PLAB,3,4,2,5,R(1,1,19),IHELRN)
        CALL RCOPY(R(1,1,19),R(1,1,20))
      END IF
*
      DO 35 I=1,6
        IF (ITRY(I).NE.0) THEN
* Q G
          C(  1)=R(2,1,16)*F(IC ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(  2)=R(2,1,16)*F(IS ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(  3)=R(2,1,14)*F(ID ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(  4)=R(2,1,14)*F(ID ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(  5)=R(2,1,14)*F(ID ,I)*G(IG ,I)/24.D0/ IFACM1 * IMBB
          C(  6)=R(2,3,14)*F(ID ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(  7)=R(2,2,14)*F(ID ,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(  8)=R(2,2,16)*F(IU ,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(  9)=R(2,2,16)*F(IC ,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C( 10)=R(2,1,14)*F(IS ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 11)=R(2,2,14)*F(IS ,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C( 12)=R(2,3,14)*F(IS ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 13)=R(2,1,14)*F(IS ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 14)=R(2,1,14)*F(IS ,I)*G(IG ,I)/24.D0/ IFACM1 * IMBB
          C( 15)=R(2,1,16)*F(ID ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 16)=R(2,1,16)*F(IU ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 17)=R(1,1,16)*F(IC ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 18)=R(1,1,16)*F(IS ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 19)=R(1,2,16)*F(ID ,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C( 20)=R(1,1,14)*F(IU ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 21)=R(1,1,14)*F(IU ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 22)=R(1,1,14)*F(IU ,I)*G(IG ,I)/24.D0/ IFACM1 * IMBB
          C( 23)=R(1,2,14)*F(IU ,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C( 24)=R(1,3,14)*F(IU ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 25)=R(1,1,14)*F(IC ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 26)=R(1,3,14)*F(IC ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 27)=R(1,2,14)*F(IC ,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C( 28)=R(1,1,14)*F(IC ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 29)=R(1,1,14)*F(IC ,I)*G(IG ,I)/24.D0/ IFACM1 * IMBB
          C( 30)=R(1,2,16)*F(IS ,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C( 31)=R(1,1,16)*F(ID ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 32)=R(1,1,16)*F(IU ,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
* G Q
          C( 33)=R(2,1,20)*F(IG ,I)*G(IC ,I)/24.D0/ IFACM1 * IMNOBB
          C( 34)=R(2,1,20)*F(IG ,I)*G(IS ,I)/24.D0/ IFACM1 * IMNOBB
          C( 35)=R(2,1,18)*F(IG ,I)*G(ID ,I)/24.D0/ IFACM1 * IMNOBB
          C( 36)=R(2,1,18)*F(IG ,I)*G(ID ,I)/24.D0/ IFACM1 * IMNOBB
          C( 37)=R(2,1,18)*F(IG ,I)*G(ID ,I)/24.D0/ IFACM1 * IMBB
          C( 38)=R(2,3,18)*F(IG ,I)*G(ID ,I)/24.D0/ IFACM1 * IMNOBB
          C( 39)=R(2,2,18)*F(IG ,I)*G(ID ,I)/48.D0/ IFACM1 * IMNOBB
          C( 40)=R(2,2,20)*F(IG ,I)*G(IU ,I)/48.D0/ IFACM1 * IMNOBB
          C( 41)=R(2,2,20)*F(IG ,I)*G(IC ,I)/48.D0/ IFACM1 * IMNOBB
          C( 42)=R(2,1,18)*F(IG ,I)*G(IS ,I)/24.D0/ IFACM1 * IMNOBB
          C( 43)=R(2,2,18)*F(IG ,I)*G(IS ,I)/48.D0/ IFACM1 * IMNOBB
          C( 44)=R(2,3,18)*F(IG ,I)*G(IS ,I)/24.D0/ IFACM1 * IMNOBB
          C( 45)=R(2,1,18)*F(IG ,I)*G(IS ,I)/24.D0/ IFACM1 * IMNOBB
          C( 46)=R(2,1,18)*F(IG ,I)*G(IS ,I)/24.D0/ IFACM1 * IMBB
          C( 47)=R(2,1,20)*F(IG ,I)*G(ID ,I)/24.D0/ IFACM1 * IMNOBB
          C( 48)=R(2,1,20)*F(IG ,I)*G(IU ,I)/24.D0/ IFACM1 * IMNOBB
          C( 49)=R(1,1,20)*F(IG ,I)*G(IC ,I)/24.D0/ IFACM1 * IMNOBB
          C( 50)=R(1,1,20)*F(IG ,I)*G(IS ,I)/24.D0/ IFACM1 * IMNOBB
          C( 51)=R(1,2,20)*F(IG ,I)*G(ID ,I)/48.D0/ IFACM1 * IMNOBB
          C( 52)=R(1,1,18)*F(IG ,I)*G(IU ,I)/24.D0/ IFACM1 * IMNOBB
          C( 53)=R(1,1,18)*F(IG ,I)*G(IU ,I)/24.D0/ IFACM1 * IMNOBB
          C( 54)=R(1,1,18)*F(IG ,I)*G(IU ,I)/24.D0/ IFACM1 * IMBB
          C( 55)=R(1,2,18)*F(IG ,I)*G(IU ,I)/48.D0/ IFACM1 * IMNOBB
          C( 56)=R(1,3,18)*F(IG ,I)*G(IU ,I)/24.D0/ IFACM1 * IMNOBB
          C( 57)=R(1,1,18)*F(IG ,I)*G(IC ,I)/24.D0/ IFACM1 * IMNOBB
          C( 58)=R(1,3,18)*F(IG ,I)*G(IC ,I)/24.D0/ IFACM1 * IMNOBB
          C( 59)=R(1,2,18)*F(IG ,I)*G(IC ,I)/48.D0/ IFACM1 * IMNOBB
          C( 60)=R(1,1,18)*F(IG ,I)*G(IC ,I)/24.D0/ IFACM1 * IMNOBB
          C( 61)=R(1,1,18)*F(IG ,I)*G(IC ,I)/24.D0/ IFACM1 * IMBB
          C( 62)=R(1,2,20)*F(IG ,I)*G(IS ,I)/48.D0/ IFACM1 * IMNOBB
          C( 63)=R(1,1,20)*F(IG ,I)*G(ID ,I)/24.D0/ IFACM1 * IMNOBB
          C( 64)=R(1,1,20)*F(IG ,I)*G(IU ,I)/24.D0/ IFACM1 * IMNOBB
* G QB
          C( 65)=R(2,1,17)*F(IG ,I)*G(IUB,I)/24.D0/ IFACM1 * IMNOBB
          C( 66)=R(2,1,17)*F(IG ,I)*G(IUB,I)/24.D0/ IFACM1 * IMNOBB
          C( 67)=R(2,1,17)*F(IG ,I)*G(IUB,I)/24.D0/ IFACM1 * IMBB
          C( 68)=R(2,3,17)*F(IG ,I)*G(IUB,I)/48.D0/ IFACM1 * IMNOBB
          C( 69)=R(2,2,17)*F(IG ,I)*G(IUB,I)/24.D0/ IFACM1 * IMNOBB
          C( 70)=R(2,3,19)*F(IG ,I)*G(IDB,I)/48.D0/ IFACM1 * IMNOBB
          C( 71)=R(2,1,19)*F(IG ,I)*G(ISB,I)/24.D0/ IFACM1 * IMNOBB
          C( 72)=R(2,1,19)*F(IG ,I)*G(ICB,I)/24.D0/ IFACM1 * IMNOBB
          C( 73)=R(2,1,19)*F(IG ,I)*G(IUB,I)/24.D0/ IFACM1 * IMNOBB
          C( 74)=R(2,1,19)*F(IG ,I)*G(IDB,I)/24.D0/ IFACM1 * IMNOBB
          C( 75)=R(2,3,19)*F(IG ,I)*G(ISB,I)/48.D0/ IFACM1 * IMNOBB
          C( 76)=R(2,1,17)*F(IG ,I)*G(ICB,I)/24.D0/ IFACM1 * IMNOBB
          C( 77)=R(2,2,17)*F(IG ,I)*G(ICB,I)/24.D0/ IFACM1 * IMNOBB
          C( 78)=R(2,3,17)*F(IG ,I)*G(ICB,I)/48.D0/ IFACM1 * IMNOBB
          C( 79)=R(2,1,17)*F(IG ,I)*G(ICB,I)/24.D0/ IFACM1 * IMNOBB
          C( 80)=R(2,1,17)*F(IG ,I)*G(ICB,I)/24.D0/ IFACM1 * IMBB
          C( 81)=R(1,3,19)*F(IG ,I)*G(IUB,I)/48.D0/ IFACM1 * IMNOBB
          C( 82)=R(1,1,17)*F(IG ,I)*G(IDB,I)/24.D0/ IFACM1 * IMNOBB
          C( 83)=R(1,1,17)*F(IG ,I)*G(IDB,I)/24.D0/ IFACM1 * IMNOBB
          C( 84)=R(1,1,17)*F(IG ,I)*G(IDB,I)/24.D0/ IFACM1 * IMBB
          C( 85)=R(1,2,17)*F(IG ,I)*G(IDB,I)/24.D0/ IFACM1 * IMNOBB
          C( 86)=R(1,3,17)*F(IG ,I)*G(IDB,I)/48.D0/ IFACM1 * IMNOBB
          C( 87)=R(1,1,19)*F(IG ,I)*G(ISB,I)/24.D0/ IFACM1 * IMNOBB
          C( 88)=R(1,1,19)*F(IG ,I)*G(ICB,I)/24.D0/ IFACM1 * IMNOBB
          C( 89)=R(1,1,19)*F(IG ,I)*G(IUB,I)/24.D0/ IFACM1 * IMNOBB
          C( 90)=R(1,1,19)*F(IG ,I)*G(IDB,I)/24.D0/ IFACM1 * IMNOBB
          C( 91)=R(1,1,17)*F(IG ,I)*G(ISB,I)/24.D0/ IFACM1 * IMNOBB
          C( 92)=R(1,3,17)*F(IG ,I)*G(ISB,I)/48.D0/ IFACM1 * IMNOBB
          C( 93)=R(1,2,17)*F(IG ,I)*G(ISB,I)/24.D0/ IFACM1 * IMNOBB
          C( 94)=R(1,1,17)*F(IG ,I)*G(ISB,I)/24.D0/ IFACM1 * IMNOBB
          C( 95)=R(1,1,17)*F(IG ,I)*G(ISB,I)/24.D0/ IFACM1 * IMBB
          C( 96)=R(1,3,19)*F(IG ,I)*G(ICB,I)/48.D0/ IFACM1 * IMNOBB
* QB G
          C( 97)=R(2,1,13)*F(IUB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 98)=R(2,1,13)*F(IUB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C( 99)=R(2,1,13)*F(IUB,I)*G(IG ,I)/24.D0/ IFACM1 * IMBB
          C(100)=R(2,3,13)*F(IUB,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(101)=R(2,2,13)*F(IUB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(102)=R(2,3,15)*F(IDB,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(103)=R(2,1,15)*F(ISB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(104)=R(2,1,15)*F(ICB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(105)=R(2,1,15)*F(IUB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(106)=R(2,1,15)*F(IDB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(107)=R(2,3,15)*F(ISB,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(108)=R(2,1,13)*F(ICB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(109)=R(2,2,13)*F(ICB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(110)=R(2,3,13)*F(ICB,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(111)=R(2,1,13)*F(ICB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(112)=R(2,1,13)*F(ICB,I)*G(IG ,I)/24.D0/ IFACM1 * IMBB
          C(113)=R(1,3,15)*F(IUB,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(114)=R(1,1,13)*F(IDB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(115)=R(1,1,13)*F(IDB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(116)=R(1,1,13)*F(IDB,I)*G(IG ,I)/24.D0/ IFACM1 * IMBB
          C(117)=R(1,2,13)*F(IDB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(118)=R(1,3,13)*F(IDB,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(119)=R(1,1,15)*F(ISB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(120)=R(1,1,15)*F(ICB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(121)=R(1,1,15)*F(IUB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(122)=R(1,1,15)*F(IDB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(123)=R(1,1,13)*F(ISB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(124)=R(1,3,13)*F(ISB,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
          C(125)=R(1,2,13)*F(ISB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(126)=R(1,1,13)*F(ISB,I)*G(IG ,I)/24.D0/ IFACM1 * IMNOBB
          C(127)=R(1,1,13)*F(ISB,I)*G(IG ,I)/24.D0/ IFACM1 * IMBB
          C(128)=R(1,3,15)*F(ICB,I)*G(IG ,I)/48.D0/ IFACM1 * IMNOBB
* SUM SUBPROCESS
          DO 40 J=1,8
            T(2,3,I)=T(2,3,I)+C(   J) +C( 64+J)
            T(2,4,I)=T(2,4,I)+C( 8+J) +C( 72+J)
            T(1,1,I)=T(1,1,I)+C(16+J) +C( 80+J)
            T(1,2,I)=T(1,2,I)+C(24+J) +C( 88+J)
            T(2,3,I)=T(2,3,I)+C(32+J) +C( 96+J)
            T(2,4,I)=T(2,4,I)+C(40+J) +C(104+J)
            T(1,1,I)=T(1,1,I)+C(48+J) +C(112+J)
            T(1,2,I)=T(1,2,I)+C(56+J) +C(120+J)
            T(2,6,I)=T(2,6,I)+C(32+J)+C(40+J)+C(64+J)+C(72+J)
            T(1,6,I)=T(1,6,I)+C(48+J)+C(56+J)+C(80+J)+C(88+J)
            T(2,7,I)=T(2,7,I)+C(   J)+C( 8+J)+C(96+J)+C(104+J)
            T(1,7,I)=T(1,7,I)+C(16+J)+C(24+J)+C(112+J)+C(120+J)
   40     CONTINUE
        END IF
   35 CONTINUE
*
      IF (NJET.EQ.3) GOTO 999
*
      IFACM2=IFAC(NJET-4)
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ4W(NJET-2,PLAB,3,4,5,6,R(1,1,21),IHELRN)
      END IF
*
      DO 45 I=1,6
        IF (ITRY(I).NE.0) THEN
* G G
          C( 1)=R(2,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMNOBB
          C( 2)=R(2,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMNOBB
          C( 3)=R(2,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMBB
          C( 4)=R(2,2,21)*F(IG ,I)*G(IG ,I)/128.D0/ IFACM2 * IMNOBB
          C( 5)=R(2,3,21)*F(IG ,I)*G(IG ,I)/128.D0/ IFACM2 * IMNOBB
          C( 6)=R(2,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMNOBB
          C( 7)=R(2,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMNOBB
          C( 8)=R(2,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMBB
          C( 9)=R(2,2,21)*F(IG ,I)*G(IG ,I)/128.D0/ IFACM2 * IMNOBB
          C(10)=R(2,3,21)*F(IG ,I)*G(IG ,I)/128.D0/ IFACM2 * IMNOBB
          C(11)=R(1,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMNOBB
          C(12)=R(1,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMNOBB
          C(13)=R(1,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMBB
          C(14)=R(1,3,21)*F(IG ,I)*G(IG ,I)/128.D0/ IFACM2 * IMNOBB
          C(15)=R(1,2,21)*F(IG ,I)*G(IG ,I)/128.D0/ IFACM2 * IMNOBB
          C(16)=R(1,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMNOBB
          C(17)=R(1,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMNOBB
          C(18)=R(1,1,21)*F(IG ,I)*G(IG ,I)/64.D0/  IFACM2 * IMBB
          C(19)=R(1,3,21)*F(IG ,I)*G(IG ,I)/128.D0/ IFACM2 * IMNOBB
          C(20)=R(1,2,21)*F(IG ,I)*G(IG ,I)/128.D0/ IFACM2 * IMNOBB
* SUM SUBPROCESS
          T(2,3,I)=T(2,3,I)+C( 1)+C( 2)+C( 3)+C( 4)+C( 5)
          T(2,4,I)=T(2,4,I)+C( 6)+C( 7)+C( 8)+C( 9)+C(10)
          T(1,1,I)=T(1,1,I)+C(11)+C(12)+C(13)+C(14)+C(15)
          T(1,2,I)=T(1,2,I)+C(16)+C(17)+C(18)+C(19)+C(20)
          T(2,5,I)=T(2,5,I)+C( 1)+C( 2)+C( 3)+C( 4)+C( 5)
     .                     +C( 6)+C( 7)+C( 8)+C( 9)+C(10)
          T(1,5,I)=T(1,5,I)+C(11)+C(12)+C(13)+C(14)+C(15)
     .                     +C(16)+C(17)+C(18)+C(19)+C(20)
        END IF
   45 CONTINUE
*
  999 CONTINUE
*
      TOT=0.0
      IF (IWHICH.NE.2) TOT=TOT+T(1,1,IUSE)+T(1,2,IUSE)
      IF (IWHICH.NE.1) TOT=TOT+T(2,3,IUSE)+T(2,4,IUSE)
*
      END
*
      SUBROUTINE RCOPY(SOURCE,TARGET)
      REAL*8 SOURCE(2,3),TARGET(2,3)
*
      TARGET(2,1)=SOURCE(1,1)
      TARGET(2,2)=SOURCE(1,3)
      TARGET(2,3)=SOURCE(1,2)
      TARGET(1,1)=SOURCE(2,1)
      TARGET(1,2)=SOURCE(2,3)
      TARGET(1,3)=SOURCE(2,2)
*
      END
*
************************************************************************
* MEQ4Z calls the various r rb s sb subprocesses.                      *
* For Calling conventions of matrix elements -- see FQ4Z               *
************************************************************************
* Table of subprocesses T(2,5:9,6):   with first index = 1             *
* Second index:  5 = GG   6= GQ + GQB  7= QG + QBG 8= QQ + QBQB 9=QQB  *
************************************************************************
      SUBROUTINE MEQ4Z(TOT,T)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      DIMENSION T(2,9,6),R(6,7),C(96)
      SAVE IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG,/MOM/,/STRUCF/
      DATA IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG
     .    / 1, 2, 3, 4, 5,  6,  7,  8,  9, 10,11/
*
      IF (IMANNR.GT.1) THEN
        DO 4 I1=1,6
          DO 4 I2=1,7
            R(I1,I2)=1D0
            IF (IMANNR.EQ.3) R(I1,I2)=1D0/WHAT**NJET
    4   CONTINUE
      END IF
*
      DO 10 J=5,9
        DO 10 K=1,6
          T(1,J,K)=0.0
   10 CONTINUE
*
* Set Delta functions for having a B Bbar pair in the final state.
*
      IMNOBB=1-IMQ
      IMBB=1
*
* Common factor in all subprocesses
*
      IFACM0=IFAC(NJET-2)
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ4Z(NJET-2,PLAB,1,2,3,4,IDECAY,IHELRN,SW2,R(1,1))
        CALL FQ4Z(NJET-2,PLAB,2,1,3,4,IDECAY,IHELRN,SW2,R(1,2))
        CALL FQ4Z(NJET-2,PLAB,1,3,2,4,IDECAY,IHELRN,SW2,R(1,3))
        CALL FQ4Z(NJET-2,PLAB,3,1,2,4,IDECAY,IHELRN,SW2,R(1,4))
        CALL FQ4Z(NJET-2,PLAB,3,2,1,4,IDECAY,IHELRN,SW2,R(1,6))
        CALL FQ4Z(NJET-2,PLAB,3,1,4,2,IDECAY,IHELRN,SW2,R(1,7))
      END IF
*
      DO 20 I=1,6
        IF (ITRY(I).NE.0) THEN
* Q Q
          C( 1)=R(1,7)*F(IC ,I)*G(IC ,I)/18.0/ IFACM0 * IMNOBB
          C( 2)=R(5,7)*F(IC ,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C( 3)=R(5,7)*F(IC ,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C( 4)=R(3,7)*F(IC ,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C( 5)=R(6,7)*F(IS ,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C( 6)=R(2,7)*F(IS ,I)*G(IS ,I)/18.0/ IFACM0 * IMNOBB
          C( 7)=R(4,7)*F(IS ,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C( 8)=R(6,7)*F(IS ,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C( 9)=R(6,7)*F(ID ,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(10)=R(4,7)*F(ID ,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(11)=R(2,7)*F(ID ,I)*G(ID ,I)/18.0/ IFACM0 * IMNOBB
          C(12)=R(6,7)*F(ID ,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C(13)=R(3,7)*F(IU ,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(14)=R(5,7)*F(IU ,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(15)=R(5,7)*F(IU ,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C(16)=R(1,7)*F(IU ,I)*G(IU ,I)/18.0/ IFACM0 * IMNOBB
* Q QB
          C(17)=R(3,4)*F(IC ,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(18)=R(5,4)*F(IC ,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(19)=R(5,4)*F(IC ,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(20)=R(5,2)*F(IC ,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(21)=R(1,2)*F(IC ,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(22)=R(5,2)*F(IC ,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(23)=R(5,2)*F(IC ,I)*G(ICB,I)/ 9.0/ IFACM0 * IMBB
          C(24)=R(3,2)*F(IC ,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(25)=R(6,4)*F(IS ,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(26)=R(4,4)*F(IS ,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(27)=R(4,2)*F(IS ,I)*G(ISB,I)/ 9.0/ IFACM0 * IMBB
          C(28)=R(6,2)*F(IS ,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(29)=R(2,2)*F(IS ,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(30)=R(4,2)*F(IS ,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(31)=R(6,2)*F(IS ,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(32)=R(6,4)*F(IS ,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(33)=R(6,4)*F(ID ,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(34)=R(4,2)*F(ID ,I)*G(IDB,I)/ 9.0/ IFACM0 * IMBB
          C(35)=R(6,2)*F(ID ,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(36)=R(4,2)*F(ID ,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(37)=R(2,2)*F(ID ,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(38)=R(6,2)*F(ID ,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(39)=R(4,4)*F(ID ,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(40)=R(6,4)*F(ID ,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(41)=R(5,2)*F(IU ,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(42)=R(3,2)*F(IU ,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(43)=R(5,2)*F(IU ,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(44)=R(5,2)*F(IU ,I)*G(IUB,I)/ 9.0/ IFACM0 * IMBB
          C(45)=R(1,2)*F(IU ,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(46)=R(5,4)*F(IU ,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(47)=R(5,4)*F(IU ,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(48)=R(3,4)*F(IU ,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
* QB Q
          C(49)=R(3,6)*F(IUB,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(50)=R(6,6)*F(IUB,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(51)=R(6,6)*F(IUB,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C(52)=R(5,1)*F(IUB,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C(53)=R(3,1)*F(IUB,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C(54)=R(5,1)*F(IUB,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C(55)=R(5,1)*F(IUB,I)*G(IU ,I)/ 9.0/ IFACM0 * IMBB
          C(56)=R(1,1)*F(IUB,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C(57)=R(5,6)*F(IDB,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(58)=R(4,6)*F(IDB,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(59)=R(4,1)*F(IDB,I)*G(ID ,I)/ 9.0/ IFACM0 * IMBB
          C(60)=R(6,1)*F(IDB,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C(61)=R(4,1)*F(IDB,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C(62)=R(2,1)*F(IDB,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C(63)=R(6,1)*F(IDB,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C(64)=R(5,6)*F(IDB,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C(65)=R(5,6)*F(ISB,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(66)=R(4,1)*F(ISB,I)*G(IS ,I)/ 9.0/ IFACM0 * IMBB
          C(67)=R(6,1)*F(ISB,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(68)=R(2,1)*F(ISB,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(69)=R(4,1)*F(ISB,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(70)=R(6,1)*F(ISB,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(71)=R(4,6)*F(ISB,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C(72)=R(5,6)*F(ISB,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
          C(73)=R(5,1)*F(ICB,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(74)=R(1,1)*F(ICB,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(75)=R(5,1)*F(ICB,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(76)=R(5,1)*F(ICB,I)*G(IC ,I)/ 9.0/ IFACM0 * IMBB
          C(77)=R(3,1)*F(ICB,I)*G(IC ,I)/ 9.0/ IFACM0 * IMNOBB
          C(78)=R(6,6)*F(ICB,I)*G(IS ,I)/ 9.0/ IFACM0 * IMNOBB
          C(79)=R(6,6)*F(ICB,I)*G(ID ,I)/ 9.0/ IFACM0 * IMNOBB
          C(80)=R(3,6)*F(ICB,I)*G(IU ,I)/ 9.0/ IFACM0 * IMNOBB
* QB QB
          C(81)=R(1,3)*F(IUB,I)*G(IUB,I)/18.0/ IFACM0 * IMNOBB
          C(82)=R(5,3)*F(IUB,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(83)=R(5,3)*F(IUB,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(84)=R(3,3)*F(IUB,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(85)=R(6,3)*F(IDB,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(86)=R(2,3)*F(IDB,I)*G(IDB,I)/18.0/ IFACM0 * IMNOBB
          C(87)=R(4,3)*F(IDB,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(88)=R(6,3)*F(IDB,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(89)=R(6,3)*F(ISB,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(90)=R(4,3)*F(ISB,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(91)=R(2,3)*F(ISB,I)*G(ISB,I)/18.0/ IFACM0 * IMNOBB
          C(92)=R(6,3)*F(ISB,I)*G(ICB,I)/ 9.0/ IFACM0 * IMNOBB
          C(93)=R(3,3)*F(ICB,I)*G(IUB,I)/ 9.0/ IFACM0 * IMNOBB
          C(94)=R(5,3)*F(ICB,I)*G(IDB,I)/ 9.0/ IFACM0 * IMNOBB
          C(95)=R(5,3)*F(ICB,I)*G(ISB,I)/ 9.0/ IFACM0 * IMNOBB
          C(96)=R(1,3)*F(ICB,I)*G(ICB,I)/18.0/ IFACM0 * IMNOBB
*
          DO 30 J=1,16
            T(1,8,I)=T(1,8,I)+C(J)+C(80+J)
            T(1,9,I)=T(1,9,I)+C(16+J)+C(32+J)+C(48+J)+C(64+J)
   30     CONTINUE
        END IF
   20 CONTINUE
*
      IF (NJET.EQ.2) GOTO 999
*
      IFACM1=IFAC(NJET-3)
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ4Z(NJET-2,PLAB,1,3,4,5,IDECAY,IHELRN,SW2,R(1,1))
        CALL FQ4Z(NJET-2,PLAB,3,1,4,5,IDECAY,IHELRN,SW2,R(1,2))
        CALL FQ4Z(NJET-2,PLAB,2,3,4,5,IDECAY,IHELRN,SW2,R(1,4))
        CALL FQ4Z(NJET-2,PLAB,3,2,4,5,IDECAY,IHELRN,SW2,R(1,5))
      END IF
*
      DO 40 I=1,6
        IF (ITRY(I).NE.0) THEN
* Q G
          C( 1)=R(5,2)*F(IC ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C( 2)=R(1,2)*F(IC ,I)*G(IG ,I)/48.0/ IFACM1 * IMNOBB
          C( 3)=R(5,2)*F(IC ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C( 4)=R(5,2)*F(IC ,I)*G(IG ,I)/24.0/ IFACM1 * IMBB
          C( 5)=R(3,2)*F(IC ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C( 6)=R(4,2)*F(IS ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C( 7)=R(6,2)*F(IS ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C( 8)=R(2,2)*F(IS ,I)*G(IG ,I)/48.0/ IFACM1 * IMNOBB
          C( 9)=R(4,2)*F(IS ,I)*G(IG ,I)/24.0/ IFACM1 * IMBB
          C(10)=R(6,2)*F(IS ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(11)=R(4,2)*F(ID ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(12)=R(6,2)*F(ID ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(13)=R(4,2)*F(ID ,I)*G(IG ,I)/24.0/ IFACM1 * IMBB
          C(14)=R(2,2)*F(ID ,I)*G(IG ,I)/48.0/ IFACM1 * IMNOBB
          C(15)=R(6,2)*F(ID ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(16)=R(5,2)*F(IU ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(17)=R(3,2)*F(IU ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(18)=R(5,2)*F(IU ,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(19)=R(5,2)*F(IU ,I)*G(IG ,I)/24.0/ IFACM1 * IMBB
          C(20)=R(1,2)*F(IU ,I)*G(IG ,I)/48.0/ IFACM1 * IMNOBB
* G Q
          C(21)=R(5,5)*F(IG ,I)*G(IC ,I)/24.0/ IFACM1 * IMNOBB
          C(22)=R(1,5)*F(IG ,I)*G(IC ,I)/48.0/ IFACM1 * IMNOBB
          C(23)=R(5,5)*F(IG ,I)*G(IC ,I)/24.0/ IFACM1 * IMNOBB
          C(24)=R(5,5)*F(IG ,I)*G(IC ,I)/24.0/ IFACM1 * IMBB
          C(25)=R(3,5)*F(IG ,I)*G(IC ,I)/24.0/ IFACM1 * IMNOBB
          C(26)=R(4,5)*F(IG ,I)*G(IS ,I)/24.0/ IFACM1 * IMNOBB
          C(27)=R(6,5)*F(IG ,I)*G(IS ,I)/24.0/ IFACM1 * IMNOBB
          C(28)=R(2,5)*F(IG ,I)*G(IS ,I)/48.0/ IFACM1 * IMNOBB
          C(29)=R(4,5)*F(IG ,I)*G(IS ,I)/24.0/ IFACM1 * IMBB
          C(30)=R(6,5)*F(IG ,I)*G(IS ,I)/24.0/ IFACM1 * IMNOBB
          C(31)=R(4,5)*F(IG ,I)*G(ID ,I)/24.0/ IFACM1 * IMNOBB
          C(32)=R(6,5)*F(IG ,I)*G(ID ,I)/24.0/ IFACM1 * IMNOBB
          C(33)=R(4,5)*F(IG ,I)*G(ID ,I)/24.0/ IFACM1 * IMBB
          C(34)=R(2,5)*F(IG ,I)*G(ID ,I)/48.0/ IFACM1 * IMNOBB
          C(35)=R(6,5)*F(IG ,I)*G(ID ,I)/24.0/ IFACM1 * IMNOBB
          C(36)=R(5,5)*F(IG ,I)*G(IU ,I)/24.0/ IFACM1 * IMNOBB
          C(37)=R(3,5)*F(IG ,I)*G(IU ,I)/24.0/ IFACM1 * IMNOBB
          C(38)=R(5,5)*F(IG ,I)*G(IU ,I)/24.0/ IFACM1 * IMNOBB
          C(39)=R(5,5)*F(IG ,I)*G(IU ,I)/24.0/ IFACM1 * IMBB
          C(40)=R(1,5)*F(IG ,I)*G(IU ,I)/48.0/ IFACM1 * IMNOBB
* G QB
          C(41)=R(5,4)*F(IG ,I)*G(IUB,I)/24.0/ IFACM1 * IMNOBB
          C(42)=R(3,4)*F(IG ,I)*G(IUB,I)/24.0/ IFACM1 * IMNOBB
          C(43)=R(5,4)*F(IG ,I)*G(IUB,I)/24.0/ IFACM1 * IMNOBB
          C(44)=R(5,4)*F(IG ,I)*G(IUB,I)/24.0/ IFACM1 * IMBB
          C(45)=R(1,4)*F(IG ,I)*G(IUB,I)/48.0/ IFACM1 * IMNOBB
          C(46)=R(4,4)*F(IG ,I)*G(IDB,I)/24.0/ IFACM1 * IMNOBB
          C(47)=R(6,4)*F(IG ,I)*G(IDB,I)/24.0/ IFACM1 * IMNOBB
          C(48)=R(4,4)*F(IG ,I)*G(IDB,I)/24.0/ IFACM1 * IMBB
          C(49)=R(2,4)*F(IG ,I)*G(IDB,I)/48.0/ IFACM1 * IMNOBB
          C(50)=R(6,4)*F(IG ,I)*G(IDB,I)/24.0/ IFACM1 * IMNOBB
          C(51)=R(4,4)*F(IG ,I)*G(ISB,I)/24.0/ IFACM1 * IMNOBB
          C(52)=R(6,4)*F(IG ,I)*G(ISB,I)/24.0/ IFACM1 * IMNOBB
          C(53)=R(2,4)*F(IG ,I)*G(ISB,I)/48.0/ IFACM1 * IMNOBB
          C(54)=R(4,4)*F(IG ,I)*G(ISB,I)/24.0/ IFACM1 * IMBB
          C(55)=R(6,4)*F(IG ,I)*G(ISB,I)/24.0/ IFACM1 * IMNOBB
          C(56)=R(5,4)*F(IG ,I)*G(ICB,I)/24.0/ IFACM1 * IMNOBB
          C(57)=R(1,4)*F(IG ,I)*G(ICB,I)/48.0/ IFACM1 * IMNOBB
          C(58)=R(5,4)*F(IG ,I)*G(ICB,I)/24.0/ IFACM1 * IMNOBB
          C(59)=R(5,4)*F(IG ,I)*G(ICB,I)/24.0/ IFACM1 * IMBB
          C(60)=R(3,4)*F(IG ,I)*G(ICB,I)/24.0/ IFACM1 * IMNOBB
* QB G
          C(61)=R(5,1)*F(IUB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(62)=R(3,1)*F(IUB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(63)=R(5,1)*F(IUB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(64)=R(5,1)*F(IUB,I)*G(IG ,I)/24.0/ IFACM1 * IMBB
          C(65)=R(1,1)*F(IUB,I)*G(IG ,I)/48.0/ IFACM1 * IMNOBB
          C(66)=R(4,1)*F(IDB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(67)=R(6,1)*F(IDB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(68)=R(4,1)*F(IDB,I)*G(IG ,I)/24.0/ IFACM1 * IMBB
          C(69)=R(2,1)*F(IDB,I)*G(IG ,I)/48.0/ IFACM1 * IMNOBB
          C(70)=R(6,1)*F(IDB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(71)=R(4,1)*F(ISB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(72)=R(6,1)*F(ISB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(73)=R(2,1)*F(ISB,I)*G(IG ,I)/48.0/ IFACM1 * IMNOBB
          C(74)=R(4,1)*F(ISB,I)*G(IG ,I)/24.0/ IFACM1 * IMBB
          C(75)=R(6,1)*F(ISB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(76)=R(5,1)*F(ICB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(77)=R(1,1)*F(ICB,I)*G(IG ,I)/48.0/ IFACM1 * IMNOBB
          C(78)=R(5,1)*F(ICB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          C(79)=R(5,1)*F(ICB,I)*G(IG ,I)/24.0/ IFACM1 * IMBB
          C(80)=R(3,1)*F(ICB,I)*G(IG ,I)/24.0/ IFACM1 * IMNOBB
          DO 50 J=1,20
            T(1,6,I)=T(1,6,I)+C(20+J)+C(40+J)
            T(1,7,I)=T(1,7,I)+C(J)+C(60+J)
   50     CONTINUE
        END IF
   40 CONTINUE
*
  999 CONTINUE
*
      TOT=T(1,5,IUSE)+T(1,6,IUSE)+T(1,7,IUSE)+T(1,8,IUSE)+T(1,9,IUSE)
*
      END
*
************************************************************************
* SUBROUTINE HPMPM calculates the H-function when the helicities of    *
* the quarks are +-+-. The calling procedure is explained in CALCZS(). *
* Z1 and Z2 are the 2*2 complex matrices which are the H-vectors       *
* in spinor language. Z2 is always the c.c. of Z1. When there          *
* are an odd number of quarks with negative energy we have to add      *
* an extra minus sign when complex conjugating. This sign is           *
* in the common /DOTPRO/ NEGAEN and is calculated before through       *
* the call SETSPV(P,4,4) in SQUAR2                                     *
************************************************************************
      SUBROUTINE HPMPM(I1,I2,I3,I4,SIGN,Z1,Z2)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
C
      PRO34=2.0*RR(I3,I4)
      PRO134=PRO34+2.0*(RR(I1,I3)+RR(I1,I4))
      PRO234=PRO34+2.0*(RR(I2,I3)+RR(I2,I4))
C
      ZF1=-SIGN*ZD(I1,I3)/(PRO34*PRO134)
      ZF2=-SIGN*ZUD(I2,I4)/(PRO34*PRO234)
      DO 10 IX=1,2
        DO 10 IY=1,2
          Z1(IX,IY)=+ZF1*ZKO(IY,I2)*(ZUD(I1,I4)*ZKOD(IX,I1)+
     .                               ZUD(I3,I4)*ZKOD(IX,I3))
     .              -ZF2*ZKOD(IX,I1)*(ZD(I2,I3)*ZKO(IY,I2)+
     .                                ZD(I4,I3)*ZKO(IY,I4))
   10 CONTINUE
      CALL CONGAT(Z1,Z2)
*
      END
*
************************************************************************
** SUBROUTINE HPMMP calculates the H-function when the helicities of   *
* the quarks are +--+.  See also SUBROUTINE HPMPM                      *
************************************************************************
      SUBROUTINE HPMMP(I1,I2,I3,I4,SIGN,Z1,Z2)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
C
      PRO34=2.0*RR(I3,I4)
      PRO134=PRO34+2.0*(RR(I1,I3)+RR(I1,I4))
      PRO234=PRO34+2.0*(RR(I2,I3)+RR(I2,I4))
C
      ZF1=-SIGN*ZD(I1,I4)/(PRO34*PRO134)
      ZF2=-SIGN*ZUD(I2,I3)/(PRO34*PRO234)
      DO 10 IX=1,2
        DO 10 IY=1,2
          Z1(IX,IY)=+ZF1*ZKO(IY,I2)*(ZUD(I1,I3)*ZKOD(IX,I1)+
     .                               ZUD(I4,I3)*ZKOD(IX,I4))
     .              -ZF2*ZKOD(IX,I1)*(ZD(I2,I4)*ZKO(IY,I2)+
     .                                ZD(I3,I4)*ZKO(IY,I3))
   10 CONTINUE
      CALL CONGAT(Z1,Z2)
*
      END
*
************************************************************************
* SUBROUTINE BADB calculates various sub-amplitudes on the             *
* the level of Bi's (see article)                                      *
* Indices 1,2,3 en 4 denote the quark permutation.                     *
* Index 5 is the sign of the subamplitude.                             *
* The subroutine calculates 4 sub-amplitudes which are put             *
* in the data structure /ZEVENS/ ZS(2,2,16,12), explained in           *
* FUNCTION SQUARE3.                                                    *
* They are put in consecutive order starting at ZS(.,.,I1A,12A)        *
* through ZS(.,.,I1A+3,I2A) for the normal helicities and              *
* in ZS(.,.,I1B,I2B) through ZS(.,.,I1B+3,I2B) for its c.c.            *
************************************************************************
* The helicity of the four quarks and the gluon comes through ITYPE.   *
* The conventions for ITYPE are: (ZiA spinor tensors)                  *
*             q1  q2   q3   q4    g                                    *
*   Itype=1 ;  +  -    +    -     +                                    *
*   Itype=2 ;  +  -    +    -     -                                    *
*   Itype=3 ;  +  -    -    +     +                                    *
*   Itype=4 ;  +  -    -    +     -                                    *
* And in ZiB the complex conjugated (i.e. flipped) amplitudes.         *
************************************************************************
      SUBROUTINE BADB(I1,I2,I3,I4,SIGN,ITYPE,I1A,I2A,I1B,I2B)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      PARAMETER(TWO32=2.82842712474)
      COMMON /ZEVENS/ ZS(2,2,16,12)
      DIMENSION Z1A(2,2),Z1B(2,2),Z2A(2,2),Z2B(2,2),
     .          Z3A(2,2),Z3B(2,2),Z4A(2,2),Z4B(2,2),
     .          Z5A(2,2),Z5B(2,2),Z6A(2,2),Z6B(2,2),
     .          Z7A(2,2),Z7B(2,2),Z8A(2,2),Z8B(2,2),
     .          Z9A(2,2),Z9B(2,2)
      DIMENSION ZA1(2,2),ZB1(2,2),ZA2(2,2),ZB2(2,2),
     .          ZA3(2,2),ZB3(2,2),ZA4(2,2),ZB4(2,2)
      SAVE /ZEVENS/
*
      CALL C1ADB(I1,I2,I3,I4,Z1A,Z1B,ITYPE)
      CALL C2ADB(I1,I2,I3,I4,Z2A,Z2B,ITYPE)
      CALL C3ADB(I1,I2,I3,I4,Z3A,Z3B,ITYPE)
      CALL C4ADB(I1,I2,I3,I4,Z4A,Z4B,ITYPE)
      CALL C5ADB(I1,I2,I3,I4,Z5A,Z5B,ITYPE)
      CALL C6ADB(I1,I2,I3,I4,Z6A,Z6B,ITYPE)
      CALL C7ADB(I1,I2,I3,I4,Z7A,Z7B,ITYPE)
      CALL C8ADB(I1,I2,I3,I4,Z8A,Z8B,ITYPE)
      CALL C9ADB(I1,I2,I3,I4,Z9A,Z9B,ITYPE)
*
      DO 10 IX=1,2
        DO 10 IY=1,2
          ZA1(IX,IY)=0.5*(Z1A(IX,IY)+Z2A(IX,IY)+Z3A(IX,IY)+Z4A(IX,IY))
          ZB1(IX,IY)=0.5*(Z1B(IX,IY)+Z2B(IX,IY)+Z3B(IX,IY)+Z4B(IX,IY))
          ZA2(IX,IY)=0.5*(-Z3A(IX,IY)-Z4A(IX,IY)+Z5A(IX,IY)+Z6A(IX,IY))
          ZB2(IX,IY)=0.5*(-Z3B(IX,IY)-Z4B(IX,IY)+Z5B(IX,IY)+Z6B(IX,IY))
          ZA3(IX,IY)=0.5*(Z7A(IX,IY))
          ZB3(IX,IY)=0.5*(Z7B(IX,IY))
          ZA4(IX,IY)=0.5*(Z8A(IX,IY)+Z9A(IX,IY))
          ZB4(IX,IY)=0.5*(Z8B(IX,IY)+Z9B(IX,IY))
   10 CONTINUE
*
      DO 20 IX=1,2
        DO 20 IY=1,2
          ZS(IX,IY,I1A,I2A)=SIGN*TWO32*ZA1(IX,IY)
          ZS(IX,IY,I1B,I2B)=SIGN*TWO32*ZB1(IX,IY)
          ZS(IX,IY,I1A+1,I2A)=SIGN*TWO32*ZA2(IX,IY)
          ZS(IX,IY,I1B+1,I2B)=SIGN*TWO32*ZB2(IX,IY)
          ZS(IX,IY,I1A+2,I2A)=SIGN*TWO32*ZA3(IX,IY)
          ZS(IX,IY,I1B+2,I2B)=SIGN*TWO32*ZB3(IX,IY)
          ZS(IX,IY,I1A+3,I2A)=SIGN*TWO32*ZA4(IX,IY)
          ZS(IX,IY,I1B+3,I2B)=SIGN*TWO32*ZB4(IX,IY)
   20 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE C1ADB through C9ADB calculate various sub-amplitudes      *
* Indices 1,2,3 en 4 are the quark permutation.                        *
* Index 5 is the spinor-tensor that is going to contain the            *
* amplitude. Index 6 is its complex conjugation. Take care of          *
* the extra minus sign through NEGAEN (See SUBROUTINE CONGAT).         *
************************************************************************
* The helicity of the four quarks and the gluon comes through ITYPE.   *
* The conventions for ITYPE are:                                       *
*             q1  q2   q3   q4    g                                    *
*   Itype=1 ;  +  -    +    -     +                                    *
*   Itype=2 ;  +  -    +    -     -                                    *
*   Itype=3 ;  +  -    -    +     +                                    *
*   Itype=4 ;  +  -    -    +     -                                    *
* And in Z2 the complex conjugated (i.e. flipped) amplitudes.          *
************************************************************************
      SUBROUTINE C1ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2),ZH1(2),ZH2(2)
      SAVE /DOTPRO/
C
      PRO34=2.0*RR(I3,I4)
      PRO34K=PRO34+2.0*(RR(5,I3)+RR(5,I4))
      PR234K=PRO34K+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,5))
      DO 5 I=1,2
        ZH1(I)=ZD(I2,I3)*ZKO(I,I2)+ZD(I4,I3)*ZKO(I,I4)+ZD(5,I3)*ZKO(I,5)
        ZH2(I)=ZD(I2,5)*ZKO(I,I2)+ZD(I3,5)*ZKO(I,I3)+ZD(I4,5)*ZKO(I,I4)
    5 CONTINUE
      IF (ITYPE.EQ.1) THEN
        ZF1=-ZUD(I2,I4)/(ZUD(I3,5)*ZUD(5,I2)*PRO34K*PR234K)
        ZF2=ZUD(I2,I4)*(ZD(5,I3)*ZUD(I2,I3)
     .       +ZD(5,I4)*ZUD(I2,I4))/(ZUD(5,I2)*PRO34*PRO34K*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*(ZH1(IY)*ZUD(I2,I3)+
     .                                  ZH2(IY)*ZUD(I2,5))
     .                +ZF2*ZKOD(IX,I1)*ZH1(IY)
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=-(ZD(I3,I2)*ZUD(I4,I2)+ZD(I3,5)*ZUD(I4,5))/
     .         (ZD(I3,5)*ZD(5,I2)*PRO34*PR234K)
        ZF2=-ZUD(5,I4)*ZUD(I2,I4)/
     .         (ZD(I3,5)*ZUD(I3,I4)*PRO34K*PR234K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*ZH1(IY)
     .                +ZF2*ZKOD(IX,I1)*ZH1(IY)
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=-ZUD(I2,I3)**2/(ZUD(I3,5)*ZUD(5,I2)*PRO34*PR234K)
     .      +ZD(I4,5)*ZUD(I2,I3)/(ZUD(I3,5)*ZD(I3,I4)*PRO34K*PR234K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .       (+ZD(I2,I4)*ZKO(IY,I2)+ZD(I3,I4)*ZKO(IY,I3)
     .        +ZD(5,I4)*ZKO(IY,5))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=-(ZUD(I2,I3)*ZD(I2,I3)+ZUD(5,I3)*ZD(5,I3))/
     .         (ZD(I2,5)*ZD(5,I3)*PRO34*PR234K)
     .      +ZUD(I2,I4)*ZUD(I3,5)/(ZD(I3,5)*ZUD(I3,I4)*PRO34K*PR234K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .       (+ZD(I2,I4)*ZKO(IY,I2)+ZD(I3,I4)*ZKO(IY,I3)
     .        +ZD(5,I4)*ZKO(IY,5))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
*
      SUBROUTINE C2ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      PRO34=2.0*RR(I3,I4)
      PRO34K=PRO34+2.0*(RR(5,I3)+RR(5,I4))
      PRO134=PRO34+2.0*(RR(I1,I3)+RR(I1,I4))
      PR134K=PRO34K+2.0*(RR(I1,5)+RR(I1,I4)+RR(I1,I3))
      IF (ITYPE.EQ.1) THEN
        ZF1=ZD(I1,I3)*(ZD(5,I1)*ZUD(I4,I1)+
     .         ZD(5,I3)*ZUD(I4,I3))/(ZUD(I2,5)*PRO34*PRO134*PR134K)
        ZF2=(ZD(I1,I3)*ZUD(I2,I3)+ZD(I1,5)*ZUD(I2,5))/
     .         (ZUD(I3,5)*ZUD(5,I2)*PRO34K*PR134K)
     .     +ZD(I1,I3)*(ZD(5,I3)*ZUD(I2,I3)
     .         +ZD(5,I4)*ZUD(I2,I4))/(ZUD(I2,5)*PRO34*PRO34K*PR134K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .       (ZUD(I1,I2)*ZKOD(IX,I1)+ZUD(I3,I2)*ZKOD(IX,I3)+
     .        ZUD(I4,I2)*ZKOD(IX,I4)+ZUD(5,I2)*ZKOD(IX,5))
     .                +ZF2*ZKO(IY,I2)*
     .       (ZUD(I1,I4)*ZKOD(IX,I1)+ZUD(I3,I4)*ZKOD(IX,I3)
     .        +ZUD(5,I4)*ZKOD(IX,5))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZD(I1,I3)/(ZD(I3,5)*ZD(5,I2)*PRO34*PRO134)
        ZF2=ZD(I1,I3)**2*ZUD(I1,I4)/(ZD(I3,5)*PRO34*PRO134*PR134K)
        ZF3=ZUD(5,I4)*ZD(I1,I3)/(ZD(I3,5)*ZUD(I3,I4)*PRO34K*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*
     .          (ZUD(I1,I4)*ZKOD(IX,I1)+ZUD(I3,I4)*ZKOD(IX,I3))*
     .          (ZD(I2,I3)*ZKO(IY,I2)+ZD(5,I3)*ZKO(IY,5))
     .                +ZF2*ZKO(IY,I2)*
     .          (+ZUD(I1,5)*ZKOD(IX,I1)
     .           +ZUD(I3,5)*ZKOD(IX,I3)
     .           +ZUD(I4,5)*ZKOD(IX,I4))
     .                +ZF3*ZKO(IY,I2)*
     .          (+ZUD(I1,I4)*ZKOD(IX,I1)
     .           +ZUD(I3,I4)*ZKOD(IX,I3)
     .           +ZUD(5,I4)*ZKOD(IX,5))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=ZUD(I2,I3)*ZD(I1,I4)/(ZUD(I3,5)*ZUD(5,I2)*PRO34*PRO134)
        ZF2=ZD(I1,I4)*(ZD(5,I1)*ZUD(I3,I1)+
     .      ZD(5,I4)*ZUD(I3,I4))/(ZUD(I3,5)*PRO34*PRO134*PR134K)
     .     -ZD(I1,I4)*ZD(I4,5)/(ZUD(I3,5)*ZD(I3,I4)*PRO34K*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .          (ZUD(I1,I3)*ZKOD(IX,I1)+ZUD(I4,I3)*ZKOD(IX,I4))
     .                +ZF2*ZKO(IY,I2)*
     .          (+ZUD(I1,I3)*ZKOD(IX,I1)
     .           +ZUD(I4,I3)*ZKOD(IX,I4)
     .           +ZUD(5,I3)*ZKOD(IX,5))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=ZD(I1,I4)/(ZD(I2,5)*ZD(5,I3)*PRO34*PRO134)
        ZF2=ZD(I1,I4)*(ZD(I3,I1)*ZUD(I3,I1)+
     .      ZD(I3,I4)*ZUD(I3,I4))/(ZD(I3,5)*PRO34*PRO134*PR134K)
     .     -ZD(I1,I4)/(ZD(I3,5)*PRO34K*PR134K)
        ZF3=-ZD(I1,I4)*ZUD(I4,5)/(ZD(I3,5)*ZUD(I3,I4)*PRO34K*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*
     .          (ZUD(I1,I3)*ZKOD(IX,I1)+ZUD(I4,I3)*ZKOD(IX,I4))*
     .          (ZD(I2,I3)*ZKO(IY,I2)+ZD(5,I3)*ZKO(IY,5))
     .                +ZF2*ZKO(IY,I2)*
     .          (+ZUD(I1,5)*ZKOD(IX,I1)
     .           +ZUD(I3,5)*ZKOD(IX,I3)
     .           +ZUD(I4,5)*ZKOD(IX,I4))
     .                +ZF3*ZKO(IY,I2)*
     .          (+ZUD(I1,I3)*ZKOD(IX,I1)+ZUD(I4,I3)*ZKOD(IX,I4)
     .           +ZUD(5,I3)*ZKOD(IX,5))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
*
      END
*
      SUBROUTINE C3ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      PRO34=2.0*RR(I3,I4)
      PRO34K=PRO34+2.0*(RR(5,I3)+RR(5,I4))
      PR234K=PRO34K+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,5))
      IF (ITYPE.EQ.1) THEN
        ZF1=ZUD(I2,I4)*ZD(I3,5)/(PRO34*PRO34K*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .         (+ZD(I2,5)*ZKO(IY,I2)
     .          +ZD(I3,5)*ZKO(IY,I3)
     .          +ZD(I4,5)*ZKO(IY,I4))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZUD(I2,5)*ZUD(I4,5)/(PRO34*PRO34K*PR234K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .         (+ZD(I2,I3)*ZKO(IY,I2)
     .          +ZD(I4,I3)*ZKO(IY,I4)
     .          +ZD(5,I3)*ZKO(IY,5))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=ZUD(I2,I3)*ZD(I4,5)/(PRO34*PRO34K*PR234K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .         (+ZD(I2,5)*ZKO(IY,I2)
     .          +ZD(I3,5)*ZKO(IY,I3)
     .          +ZD(I4,5)*ZKO(IY,I4))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=ZUD(I2,5)*ZUD(I3,5)/(PRO34*PRO34K*PR234K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .         (+ZD(I2,I4)*ZKO(IY,I2)
     .          +ZD(I3,I4)*ZKO(IY,I3)
     .          +ZD(5,I4)*ZKO(IY,5))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
*
      END
*
      SUBROUTINE C4ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      PRO34=2.0*RR(I3,I4)
      PRO34K=PRO34+2.0*(RR(5,I3)+RR(5,I4))
      PR134K=PRO34K+2.0*(RR(I1,5)+RR(I1,I4)+RR(I1,I3))
      IF (ITYPE.EQ.1) THEN
        ZF1=-ZD(I1,5)*ZD(I3,5)/(PRO34*PRO34K*PR134K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .       (+ZUD(I1,I4)*ZKOD(IX,I1)
     .        +ZUD(I3,I4)*ZKOD(IX,I3)
     .        +ZUD(5,I4)*ZKOD(IX,5))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=-ZUD(I4,5)*ZD(I1,I3)/(PRO34*PRO34K*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .       (+ZUD(I1,5)*ZKOD(IX,I1)
     .        +ZUD(I3,5)*ZKOD(IX,I3)
     .        +ZUD(I4,5)*ZKOD(IX,I4))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=-ZD(I1,5)*ZD(I4,5)/(PRO34*PRO34K*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .       (+ZUD(I1,I3)*ZKOD(IX,I1)
     .        +ZUD(I4,I3)*ZKOD(IX,I4)
     .        +ZUD(5,I3)*ZKOD(IX,5))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=-ZD(I1,I4)*ZUD(I3,5)/(PRO34*PRO34K*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .       (+ZUD(I1,5)*ZKOD(IX,I1)
     .        +ZUD(I3,5)*ZKOD(IX,I3)
     .        +ZUD(I4,5)*ZKOD(IX,I4))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
*
      END
*
      SUBROUTINE C5ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      PRO34=2.0*RR(I3,I4)
      PRO34K=PRO34+2.0*(RR(5,I3)+RR(5,I4))
      PRO234=PRO34+2.0*(RR(I2,I3)+RR(I2,I4))
      PR234K=PRO34K+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,5))
      IF (ITYPE.EQ.1) THEN
        ZF1=ZUD(I2,I4)**2*ZD(I2,I3)/(ZUD(I4,5)*PRO34*PRO234*PR234K)
        ZF2=ZUD(I2,I4)/(ZUD(I1,5)*ZUD(5,I4)*PRO34*PRO234)
        ZF3=ZUD(I2,I4)*ZD(I3,5)/(ZD(I3,I4)*ZUD(I4,5)*PRO34K*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .        (+ZD(I2,5)*ZKO(IY,I2)+ZD(I3,5)*ZKO(IY,I3)
     .         +ZD(I4,5)*ZKO(IY,I4))
     .                +ZF2*
     .        (ZUD(I1,I4)*ZKOD(IX,I1)+ZUD(5,I4)*ZKOD(IX,5))*
     .        (ZD(I2,I3)*ZKO(IY,I2)+ZD(I4,I3)*ZKO(IY,I4))
     .                +ZF3*ZKOD(IX,I1)*
     .        (+ZD(I2,I3)*ZKO(IY,I2)+ZD(I4,I3)*ZKO(IY,I4)
     .         +ZD(5,I3)*ZKO(IY,5))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZUD(I2,I4)*(ZD(I3,I2)*ZUD(5,I2)+
     .      ZD(I3,I4)*ZUD(5,I4))/(ZD(I1,5)*PRO34*PRO234*PR234K)
        ZF2=(ZD(I1,I4)*ZUD(I2,I4)+ZD(I1,5)*ZUD(I2,5))/
     .      (ZD(I1,5)*ZD(5,I4)*PRO34K*PR234K)
     .     +ZUD(I2,I4)*(ZD(I1,I3)*ZUD(5,I3)+
     .      ZD(I1,I4)*ZUD(5,I4))/(ZD(I1,5)*PRO34*PRO34K*PR234K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .         (+ZD(I2,I1)*ZKO(IY,I2)
     .          +ZD(I3,I1)*ZKO(IY,I3)
     .          +ZD(I4,I1)*ZKO(IY,I4)
     .          +ZD(5,I1)*ZKO(IY,5))
     .                +ZF2*ZKOD(IX,I1)*
     .         (+ZD(I2,I3)*ZKO(IY,I2)
     .          +ZD(I4,I3)*ZKO(IY,I4)
     .          +ZD(5,I3)*ZKO(IY,5))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=+ZUD(I2,I3)*(PRO34+ZD(I4,I2)*ZUD(I4,I2))/
     .      (ZUD(I4,5)*PRO34*PRO234*PR234K)
     .      -ZUD(I2,I3)/(ZUD(I4,5)*PRO34K*PR234K)
        ZF2=+ZUD(I2,I3)/(ZUD(I1,5)*ZUD(5,I4)*PRO34*PRO234)
        ZF3=-ZUD(I2,I3)*ZD(5,I3)/(ZUD(I4,5)*ZD(I3,I4)*PRO34K*PR234K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .         (+ZD(I2,5)*ZKO(IY,I2)
     .          +ZD(I3,5)*ZKO(IY,I3)
     .          +ZD(I4,5)*ZKO(IY,I4))
     .                +ZF2*
     .         (ZUD(I1,I4)*ZKOD(IX,I1)+ZUD(5,I4)*ZKOD(IX,5))*
     .         (ZD(I2,I4)*ZKO(IY,I2)+ZD(I3,I4)*ZKO(IY,I3))
     .                +ZF3*ZKOD(IX,I1)*
     .         (+ZD(I2,I4)*ZKO(IY,I2)
     .          +ZD(I3,I4)*ZKO(IY,I3)
     .          +ZD(5,I4)*ZKO(IY,5))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=+ZUD(I2,I3)*(ZD(I4,I2)*ZUD(5,I2)+
     .      ZD(I4,I3)*ZUD(5,I3))/(ZD(I4,5)*PRO34*PRO234*PR234K)
     .      +ZUD(I2,I3)*ZUD(I3,5)/(ZD(I4,5)*ZUD(I3,I4)*PRO34K*PR234K)
        ZF2=+ZUD(I2,I3)*ZD(I1,I4)/(ZD(I1,5)*ZD(5,I4)*PRO34*PRO234)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .         (+ZD(I2,I4)*ZKO(IY,I2)
     .          +ZD(I3,I4)*ZKO(IY,I3)
     .          +ZD(5,I4)*ZKO(IY,5))
     .                +ZF2*ZKOD(IX,I1)*
     .         (ZD(I2,I4)*ZKO(IY,I2)+ZD(I3,I4)*ZKO(IY,I3))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
*
      END
*
      SUBROUTINE C6ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      PRO34=2.0*RR(I3,I4)
      PRO34K=PRO34+2.0*(RR(5,I3)+RR(5,I4))
      PR134K=PRO34K+2.0*(RR(I1,I3)+RR(I1,I4)+RR(I1,5))
      IF (ITYPE.EQ.1) THEN
        ZF1=(ZD(I3,I1)*ZUD(I4,I1)+ZD(I3,5)*ZUD(I4,5))/
     .      (ZUD(I4,5)*ZUD(I1,5)*PRO34*PR134K)
     .     -ZD(I1,I3)*ZD(I3,5)/(ZUD(I4,5)*ZD(I3,I4)*PRO34K*PR134K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .        (+ZUD(I1,I4)*ZKOD(IX,I1)
     .         +ZUD(I3,I4)*ZKOD(IX,I3)
     .         +ZUD(5,I4)*ZKOD(IX,5))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=-ZD(I1,I3)/(ZD(I1,5)*ZD(5,I4)*PRO34K*PR134K)
        ZF2=-ZD(I1,I3)*(ZD(I1,I3)*ZUD(5,I3)+
     .        ZD(I1,I4)*ZUD(5,I4))/(ZD(I1,5)*PRO34*PRO34K*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .        (+(ZUD(I1,I4)*ZD(I1,I4)+ZUD(I1,5)*ZD(I1,5))*ZKOD(IX,I1)
     .         +(ZUD(I3,I4)*ZD(I1,I4)+ZUD(I3,5)*ZD(I1,5))*ZKOD(IX,I3)
     .         +(ZUD(I4,I4)*ZD(I1,I4)+ZUD(I4,5)*ZD(I1,5))*ZKOD(IX,I4)
     .         +(ZUD(5,I4)*ZD(I1,I4)+ZUD(5,5)*ZD(I1,5))*ZKOD(IX,5))
     .                +ZF2*ZKO(IY,I2)*
     .         (+ZUD(I1,I4)*ZKOD(IX,I1)
     .          +ZUD(I3,I4)*ZKOD(IX,I3)
     .          +ZUD(5,I4)*ZKOD(IX,5))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=-(ZD(I1,I4)*ZUD(I1,I4)+ZD(5,I4)*ZUD(5,I4))/
     .      (ZUD(I1,5)*ZUD(5,I4)*PRO34*PR134K)
     .      +ZD(I1,5)/(ZUD(I4,5)*PRO34K*PR134K)
     .      -ZD(I1,I4)*ZD(I3,5)/(ZUD(I4,5)*ZD(I3,I4)*PRO34K*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .         (+ZUD(I1,I3)*ZKOD(IX,I1)
     .          +ZUD(I4,I3)*ZKOD(IX,I4)
     .          +ZUD(5,I3)*ZKOD(IX,5))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=-ZD(I1,I4)**2/(ZD(I1,5)*ZD(5,I4)*PRO34*PR134K)
     .      -ZD(I1,I4)*ZUD(I3,5)/(ZD(I4,5)*ZUD(I3,I4)*PRO34K*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .         (+ZUD(I1,I3)*ZKOD(IX,I1)
     .          +ZUD(I4,I3)*ZKOD(IX,I4)
     .          +ZUD(5,I3)*ZKOD(IX,5))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
*
      END
*
      SUBROUTINE C7ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      PRO34=2.0*RR(I3,I4)
      PRO34K=PRO34+2.0*(RR(5,I3)+RR(5,I4))
      PR134K=PRO34K+2.0*(RR(I1,I3)+RR(I1,I4)+RR(I1,5))
      PR234K=PRO34K+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,5))
      IF (ITYPE.EQ.1) THEN
        ZF1=(ZD(I1,I3)*ZUD(I4,I3)+ZD(I1,5)*ZUD(I4,5))/
     .      (ZUD(I3,5)*ZUD(5,I4)*PRO34K*PR134K)
        ZF2=-ZUD(I2,I4)/(ZUD(I3,5)*ZUD(5,I4)*PRO34K*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .        (+ZUD(I1,I4)*ZKOD(IX,I1)
     .         +ZUD(I3,I4)*ZKOD(IX,I3)
     .         +ZUD(5,I4)*ZKOD(IX,5))
     .                +ZF2*ZKOD(IX,I1)*
     .        (+ZKO(IY,I2)*(ZD(I2,I3)*ZUD(I4,I3)+ZD(I2,5)*ZUD(I4,5))
     .         +ZKO(IY,I3)*(ZD(I3,I3)*ZUD(I4,I3)+ZD(I3,5)*ZUD(I4,5))
     .         +ZKO(IY,I4)*(ZD(I4,I3)*ZUD(I4,I3)+ZD(I4,5)*ZUD(I4,5))
     .         +ZKO(IY,5)*(ZD(5,I3)*ZUD(I4,I3)+ZD(5,5)*ZUD(I4,5)))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=(ZD(I3,I4)*ZUD(I2,I4)+ZD(I3,5)*ZUD(I2,5))/
     .      (ZD(I3,5)*ZD(5,I4)*PRO34K*PR234K)
        ZF2=-ZD(I1,I3)/(ZD(I3,5)*ZD(5,I4)*PRO34K*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .        (+ZD(I2,I3)*ZKO(IY,I2)+ZD(I4,I3)*ZKO(IY,I4)
     .         +ZD(5,I3)*ZKO(IY,5))
     .                +ZF2*ZKO(IY,I2)*
     .        (+(ZUD(I1,I4)*ZD(I3,I4)+ZUD(I1,5)*ZD(I3,5))*ZKOD(IX,I1)
     .         +(ZUD(I3,I4)*ZD(I3,I4)+ZUD(I3,5)*ZD(I3,5))*ZKOD(IX,I3)
     .         +(ZUD(I4,I4)*ZD(I3,I4)+ZUD(I4,5)*ZD(I3,5))*ZKOD(IX,I4)
     .         +(ZUD(5,I4)*ZD(I3,I4)+ZUD(5,5)*ZD(I3,5))*ZKOD(IX,5))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=ZUD(I2,I3)/(ZUD(I3,5)*ZUD(5,I4)*PRO34K*PR234K)
        ZF2=-(ZD(I1,I4)*ZUD(I3,I4)+ZD(I1,5)*ZUD(I3,5))/
     .      (ZUD(I3,5)*ZUD(5,I4)*PRO34K*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .        (+(ZUD(I3,I4)*ZD(I2,I4)+ZUD(I3,5)*ZD(I2,5))*ZKO(IY,I2)
     .         +(ZUD(I3,I4)*ZD(I3,I4)+ZUD(I3,5)*ZD(I3,5))*ZKO(IY,I3)
     .         +(ZUD(I3,I4)*ZD(I4,I4)+ZUD(I3,5)*ZD(I4,5))*ZKO(IY,I4)
     .         +(ZUD(I3,I4)*ZD(5,I4)+ZUD(I3,5)*ZD(5,5))*ZKO(IY,5))
     .                +ZF2*ZKO(IY,I2)*
     .        (ZUD(I1,I3)*ZKOD(IX,I1)+ZUD(I4,I3)*ZKOD(IX,I4)+
     .         ZUD(5,I3)*ZKOD(IX,5))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=-(ZD(I4,I3)*ZUD(I2,I3)+ZD(I4,5)*ZUD(I2,5))/
     .      (ZD(I3,5)*ZD(5,I4)*PRO34K*PR234K)
        ZF2=+ZD(I1,I4)/(ZD(I3,5)*ZD(5,I4)*PRO34K*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .        (ZD(I2,I4)*ZKO(IY,I2)+ZD(I3,I4)*ZKO(IY,I3)
     .         +ZD(5,I4)*ZKO(IY,5))
     .                +ZF2*ZKO(IY,I2)*
     .        (+ZKOD(IX,I1)*(ZUD(I1,I3)*ZD(I4,I3)+ZUD(I1,5)*ZD(I4,5))
     .         +ZKOD(IX,I3)*(ZUD(I3,I3)*ZD(I4,I3)+ZUD(I3,5)*ZD(I4,5))
     .         +ZKOD(IX,I4)*(ZUD(I4,I3)*ZD(I4,I3)+ZUD(I4,5)*ZD(I4,5))
     .         +ZKOD(IX,5) *(ZUD(5,I3) *ZD(I4,I3)+ZUD(5,5)*ZD(I4,5)))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
*
      END
*
      SUBROUTINE C8ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      PRO34=2.0*RR(I3,I4)
      PRO234=PRO34+2.0*(RR(I2,I3)+RR(I2,I4))
      PR234K=PRO234+2.0*(RR(5,I2)+RR(5,I3)+RR(5,I4))
      IF (ITYPE.EQ.1) THEN
        ZF1=ZUD(I2,I4)/(ZUD(I1,5)*ZUD(5,I2)*PRO34*PRO234)
        ZF2=ZUD(I2,I4)**2/
     .      (ZUD(I2,5)*ZUD(I3,I4)*PRO234*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*
     .        (+ZKOD(IX,I1)*ZUD(I1,I2)+ZKOD(IX,5)*ZUD(5,I2))*
     .        (+ZKO(IY,I2)*ZD(I2,I3)+ZKO(IY,I4)*ZD(I4,I3))
     .                +ZF2*ZKOD(IX,I1)*
     .        (+ZKO(IY,I2)*ZD(I2,5)+ZKO(IY,I3)*ZD(I3,5)
     .         +ZKO(IY,I4)*ZD(I4,5))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZUD(I2,I4)*(ZD(I3,I2)*ZUD(5,I2)+
     .      ZD(I3,I4)*ZUD(5,I4))/(ZD(I1,5)*PRO34*PRO234*PR234K)
        ZF2=-(ZD(I1,I2)*ZUD(I4,I2)+ZD(I1,5)*ZUD(I4,5))/
     .      (ZD(I1,5)*ZD(5,I2)*PRO34*PR234K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .        (+ZD(I2,I1)*ZKO(IY,I2)+ZD(I3,I1)*ZKO(IY,I3)
     .         +ZD(I4,I1)*ZKO(IY,I4)+ZD(5,I1)*ZKO(IY,5))
     .                +ZF2*ZKOD(IX,I1)*
     .        (+ZD(I2,I3)*ZKO(IY,I2)+ZD(I4,I3)*ZKO(IY,I4)
     .         +ZD(5,I3)*ZKO(IY,5))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=ZUD(I2,I3)/(ZUD(I1,5)*ZUD(5,I2)*PRO34*PRO234)
        ZF2=-ZUD(I2,I3)**2/
     .      (ZUD(I2,5)*ZUD(I3,I4)*PRO234*PR234K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*
     .        (+ZUD(I1,I2)*ZKOD(IX,I1)+ZUD(5,I2)*ZKOD(IX,5))*
     .        (+ZD(I2,I4)*ZKO(IY,I2)+ZD(I3,I4)*ZKO(IY,I3))
     .                +ZF2*ZKOD(IX,I1)*
     .        (+ZD(I2,5)*ZKO(IY,I2)+ZD(I3,5)*ZKO(IY,I3)
     .         +ZD(I4,5)*ZKO(IY,I4))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=ZUD(I2,I3)*(ZD(I4,I2)*ZUD(5,I2)+
     .      ZD(I4,I3)*ZUD(5,I3))/(ZD(I1,5)*PRO34*PRO234*PR234K)
        ZF2=-(ZD(I1,I2)*ZUD(I3,I2)+ZD(I1,5)*ZUD(I3,5))/
     .      (ZD(I1,5)*ZD(5,I2)*PRO34*PR234K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(IX,I1)*
     .        (+ZD(I2,I1)*ZKO(IY,I2)+ZD(I3,I1)*ZKO(IY,I3)
     .         +ZD(I4,I1)*ZKO(IY,I4)+ZD(5,I1)*ZKO(IY,5))
     .                +ZF2*ZKOD(IX,I1)*
     .        (+ZD(I2,I4)*ZKO(IY,I2)+ZD(I3,I4)*ZKO(IY,I3)
     .         +ZD(5,I4)*ZKO(IY,5))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
*
      END
*
      SUBROUTINE C9ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
*
      PRO34=2.0*RR(I3,I4)
      PRO134=PRO34+2.0*(RR(I1,I3)+RR(I1,I4))
      PR134K=PRO134+2.0*(RR(5,I1)+RR(5,I3)+RR(5,I4))
      IF (ITYPE.EQ.1) THEN
        ZF1=-(ZD(I3,I1)*ZUD(I2,I1)+ZD(I3,5)*ZUD(I2,5))/
     .      (ZUD(I1,5)*ZUD(5,I2)*PRO34*PR134K)
        ZF2=ZD(I1,I3)*(ZD(5,I1)*ZUD(I4,I1)
     .      +ZD(5,I3)*ZUD(I4,I3))/(ZUD(I2,5)*PRO34*PRO134*PR134K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .        (+ZKOD(IX,I1)*ZUD(I1,I4)+ZKOD(IX,I3)*ZUD(I3,I4)
     .         +ZKOD(IX,5)*ZUD(5,I4))
     .                +ZF2*ZKO(IY,I2)*
     .        (+ZKOD(IX,I1)*ZUD(I1,I2)+ZKOD(IX,I3)*ZUD(I3,I2)
     .         +ZKOD(IX,I4)*ZUD(I4,I2)+ZKOD(IX,5)*ZUD(5,I2))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZD(I1,I3)/(ZD(I1,5)*ZD(5,I2)*PRO34*PRO134)
        ZF2=-ZD(I1,I3)**2/(ZD(I1,5)*ZD(I3,I4)*PRO134*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*
     .        (+ZUD(I1,I4)*ZKOD(IX,I1)+ZUD(I3,I4)*ZKOD(IX,I3))*
     .        (+ZD(I2,I1)*ZKO(IY,I2)+ZD(5,I1)*ZKO(IY,5))
     .                +ZF2*ZKO(IY,I2)*
     .        (+ZUD(I1,5)*ZKOD(IX,I1)+ZUD(I3,5)*ZKOD(IX,I3)
     .         +ZUD(I4,5)*ZKOD(IX,I4))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=-(ZD(I4,I1)*ZUD(I2,I1)+ZD(I4,5)*ZUD(I2,5))/
     .      (ZUD(I1,5)*ZUD(5,I2)*PRO34*PR134K)
        ZF2=ZD(I1,I4)*(ZD(5,I1)*ZUD(I3,I1)+
     .      ZD(5,I4)*ZUD(I3,I4))/(ZUD(I2,5)*PRO34*PRO134*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(IY,I2)*
     .        (ZUD(I1,I3)*ZKOD(IX,I1)+ZUD(I4,I3)*ZKOD(IX,I4)+
     .         ZUD(5,I3)*ZKOD(IX,5))
     .                +ZF2*ZKO(IY,I2)*
     .        (+ZUD(I1,I2)*ZKOD(IX,I1)+ZUD(I3,I2)*ZKOD(IX,I3)
     .         +ZUD(I4,I2)*ZKOD(IX,I4)+ZUD(5,I2)*ZKOD(IX,5))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=ZD(I1,I4)/(ZD(I1,5)*ZD(5,I2)*PRO34*PRO134)
        ZF2=ZD(I1,I4)**2/(ZD(I1,5)*ZD(I3,I4)*PRO134*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*
     .        (ZKOD(IX,I1)*ZUD(I1,I3)+ZKOD(IX,I4)*ZUD(I4,I3))*
     .        (ZKO(IY,I2)*ZD(I2,I1)+ZKO(IY,5)*ZD(5,I1))
     .                +ZF2*ZKO(IY,I2)*
     .        (+ZUD(I1,5)*ZKOD(IX,I1)+ZUD(I3,5)*ZKOD(IX,I3)
     .         +ZUD(I4,5)*ZKOD(IX,I4))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
*
      END
*
************************************************************************
* SUBROUTINE CONGAT(Z1,Z2) takes the complex conjugate                 *
* spinor-tensor of Z1 into Z2. Use NEGAEN to correct for               *
* an odd number of quarks with negative energy.                        *
************************************************************************
      SUBROUTINE CONGAT(Z1,Z2)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
      SAVE /DOTPRO/
C
      Z2(1,1)=NEGAEN*CONJG(Z1(1,1))
      Z2(2,1)=NEGAEN*CONJG(Z1(1,2))
      Z2(1,2)=NEGAEN*CONJG(Z1(2,1))
      Z2(2,2)=NEGAEN*CONJG(Z1(2,2))
*
      END
*
************************************************************************
* SUBROUTINE SETSPV(P,N,NRQUAR) determines the co-variant Weyl Waerden *
* spinors and all the possible spinor-inproducts.                      *
* Special treatment for particles 6 and 7 (leptons)                    *
* Particles with negative energy have an artificial minus              *
* sign in their dotted spinors.                                        *
************************************************************************
* Calling conventions:                                                 *
*   N = Number of of vectors (N must be <= NMAX )                      *
*   P = Array of covariant vectors (+,-,-,-) metric                    *
*       with decay products as particles 9 & 10                        *
*       Note: None of the vectors should be parallel to the y-axis.    *
************************************************************************
* Output conventions:                                                  *
*      PARAMETER(NM=10)                                                *
*      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),      *
*      .                RR(NM,NM),NEGAEN                               *
* With:                                                                *
*   ZUD(i,j)    = Undotted spinor inproduct                            *
*   ZD(i,j)     = Dotted spinor inproduct                              *
*   ZKO(2,i)    = Undotted co-variant spinors                          *
*   ZKOD(2,i)   = Dotted co-variant spinors                            *
*   RR(i,j)     = Minkowski inner products.                            *
*   NEGAEN      = Number of quarks (particles 1-NRQUAR) with negative  *
*                 energy. Needed when complex-conjugating.             *
*                 NEGAEN=1,-1 when that number is even,odd             *
************************************************************************
      SUBROUTINE SETSPV(P,N,NRQUAR)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION P(0:3,*)
      SAVE /DOTPRO/
*
      NEGAEN=1
      DO 10 I=1,10
        IF((I.LE.N).OR.(I.GE.9))THEN
          ZKO(1,I)=(P(1,I)-(0.0,1.0)*P(3,I))
     .             /SQRT(CMPLX(P(0,I)-P(2,I)))
          ZKO(2,I)=SQRT(CMPLX(P(0,I)-P(2,I)))
          ZKOD(1,I)=CONJG(ZKO(1,I))
          ZKOD(2,I)=CONJG(ZKO(2,I))
          IF ((P(0,I).LT.0D0).AND.(I.LT.(NRQUAR+1))) THEN
            NEGAEN=-NEGAEN
          END IF
          IF (P(0,I).LT.0.0) THEN
            ZKOD(1,I)=-ZKOD(1,I)
            ZKOD(2,I)=-ZKOD(2,I)
          END IF
        END IF
   10 CONTINUE
*
      DO 20 I=1,N-1
        DO 20 J=I,N
          ZUD(I,J)=-ZKO(1,I)*ZKO(2,J)+ZKO(2,I)*ZKO(1,J)
          ZUD(J,I)=-ZUD(I,J)
          ZD(I,J)=-ZKOD(1,I)*ZKOD(2,J)+ZKOD(2,I)*ZKOD(1,J)
          ZD(J,I)=-ZD(I,J)
          RR(I,J)=P(0,I)*P(0,J)-P(1,I)*P(1,J)
     .           -P(2,I)*P(2,J)-P(3,I)*P(3,J)
          RR(J,I)=RR(I,J)
   20 CONTINUE
*
      END
*
************************************************************************
* MATRIX ELEMENT FOR:  NOTHING --> Q QB Q QB + V + 0,1,2 GLUONS        *
* THE VECTOR BOSON IS A W+ OR W-                                       *
************************************************************************
* CALLING PROCEDURE: FQ4(N,PLAB,I1,I2,I3,I4,RESULT,IHELRN)             *
*   PLAB(4,10) : momenta of event                                      *
*   N          : number of gluons                                      *
*   I1,I3      : LABELS OF QUARKS IN PLAB ARRAY                        *
*   I2,I4      : LABELS OF ANTI-QUARKS IN PLAB ARRAY                   *
*   IHELRN     : =1 IF MC OVER HEL AMPLITUDES IS WANTED                *
*                                                                      *
*   OUTPUT: THE MATRIX ELEMENTS FOR THE SUBPROCESSES RETURNED IN       *
*   RESULT(2,3): FIRST INDEX IS CHARGE BOSON                           *
*                SECOND INDEX 1 = NO SAME QUARKS                       *
*                             2 = SAME QUARKS                          *
*                             3 = SAME ANTI-QUARKS                     *
*   NOT INCLUDED: WEAK AND STRONG COUPLING CONSTANTS!                  *
*                 STATISTICS, SPIN AVERAGES, COLOR AVERAGES.           *
************************************************************************
* INFORMATION: THE PARTICLES IN PLAB ARE ORDERED AS FOLLOWS:           *
*             1,2 INCOMING PARTICLES FROM P, PBAR                      *
*             3..? QUARKS IN FINAL STATE                               *
*             9,10 LEPTONS                                             *
* PARTICLES 1 AND 2 ARE ALWAYS FLIPPED: WE CONSIDER ALL PARTICLE       *
* TO BE OUTGOING.                                                      *
************************************************************************
* MOST OF THE FORMULAE IN THIS PROGRAM CAN BE FOUND IN:                *
*   EXACT EXPRESSIONS FOR PROCESSES INVOLVING A VECTOR BOSON           *
*   AND UP TO FIVE PARTONS. (BERENDS, GIELE, KUIJF)                    *
************************************************************************
      SUBROUTINE FQ4W(N,PLAB,I1,I2,I3,I4,RESULT,IHELRN)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),RESULT(2,3)
      COMMON /KINEMA/ P(0:3,NM)
      DIMENSION AMMAT0(4,4,6),APMAT0(4,4,6)
      DIMENSION AMMAT1(4,4,12),APMAT1(4,4,12)
      SAVE /KINEMA/
************************************************************************
      TOTEN=0.0
      DO 5 I=1,10
        IF ((I.LE.(N+4)).OR.(I.GE.9))
     .  TOTEN=TOTEN+REAL(PLAB(4,I))
    5 CONTINUE
      SCALE=TOTEN/(6+N)
 
**************************************
* TRANSFORM VECTORS
      DO 10 I=1,10
        IF ((I.LE.(N+4)).OR.(I.GE.9)) THEN
          DO 20 MU=0,3
            NU=MU
            IF(MU.EQ.0) NU=4
            P(MU,I) =REAL(PLAB(NU,I))/SCALE
            IF (I.LT.3) P(MU,I)=-P(MU,I)
   20     CONTINUE
        END IF
   10 CONTINUE
*
      CALL RESHQS(P,I1,I2,I3,I4,4)
*
* Specialise to N=0 (four quarks only)
      IF (N.EQ.0) THEN
        CALL SQUAR2(P,AMMAT0,APMAT0)
* NO SAME QUARKS
        RESULT(2,1)=(AMMAT0(1,1,3)+AMMAT0(1,1,6))
        RESULT(1,1)=(APMAT0(1,1,3)+APMAT0(1,1,6))
* SAME QUARKS
        RESULT(2,2)=(AMMAT0(1,1,3)+AMMAT0(1,1,6)+AMMAT0(2,2,1)
     .              +AMMAT0(4,4,6)+AMMAT0(1,4,6)+AMMAT0(4,1,6))
        RESULT(1,2)=(APMAT0(1,1,3)+APMAT0(1,1,6)+APMAT0(2,2,1)
     .              +APMAT0(4,4,6)+APMAT0(1,4,6)+APMAT0(4,1,6))
* SAME ANTI QUARKS
        RESULT(2,3)=(AMMAT0(1,1,3)+AMMAT0(1,1,6)+AMMAT0(2,2,6)
     .              +AMMAT0(1,1,4)+AMMAT0(1,2,6)+AMMAT0(2,1,6))
        RESULT(1,3)=(APMAT0(1,1,3)+APMAT0(1,1,6)+APMAT0(2,2,6)
     .              +APMAT0(1,1,4)+APMAT0(1,2,6)+APMAT0(2,1,6))
      END IF
*
* Specialise to N=1 (four quarks only)
      IF (N.EQ.1) THEN
        CALL SQUAR3(P,AMMAT1,APMAT1)
* NO SAME QUARKS
        RESULT(2,1)=
     .     (AMMAT1(1,1,5)+AMMAT1(1,1,6)+AMMAT1(1,1,11)+AMMAT1(1,1,12))
        RESULT(1,1)=
     .     (APMAT1(1,1,5)+APMAT1(1,1,6)+APMAT1(1,1,11)+APMAT1(1,1,12))
* SAME QUARKS
        RESULT(2,2)=(AMMAT1(2,2,1) +AMMAT1(2,2,2) +AMMAT1(1,1,5)+
     .               AMMAT1(1,1,6) +AMMAT1(1,1,11)+AMMAT1(1,1,12)+
     .               AMMAT1(1,4,11)+AMMAT1(4,1,11)+AMMAT1(4,4,11)+
     .               AMMAT1(1,4,12)+AMMAT1(4,1,12)+AMMAT1(4,4,12))
        RESULT(1,2)=(APMAT1(2,2,1) +APMAT1(2,2,2) +APMAT1(1,1,5)+
     .               APMAT1(1,1,6) +APMAT1(1,1,11)+APMAT1(1,1,12)+
     .               APMAT1(1,4,11)+APMAT1(4,1,11)+APMAT1(4,4,11)+
     .               APMAT1(1,4,12)+APMAT1(4,1,12)+APMAT1(4,4,12))
* SAME ANTI QUARKS
        RESULT(2,3)=(AMMAT1(1,1,5) +AMMAT1(1,1,6) +AMMAT1(1,1,7)+
     .               AMMAT1(1,1,8) +AMMAT1(1,1,11)+AMMAT1(1,1,12)+
     .               AMMAT1(1,2,11)+AMMAT1(2,1,11)+AMMAT1(2,2,11)+
     .               AMMAT1(1,2,12)+AMMAT1(2,1,12)+AMMAT1(2,2,12))
        RESULT(1,3)=(APMAT1(1,1,5) +APMAT1(1,1,6) +APMAT1(1,1,7)+
     .               APMAT1(1,1,8) +APMAT1(1,1,11)+APMAT1(1,1,12)+
     .               APMAT1(1,2,11)+APMAT1(2,1,11)+APMAT1(2,2,11)+
     .               APMAT1(1,2,12)+APMAT1(2,1,12)+APMAT1(2,2,12))
      END IF
*
* Specialise to N=2 (four quarks only)
* Subroutine MQ4G2W by courtesy of W. T. GIELE, adapted and corrected
* by H. KUIJF & B. Tausk to work with the VECBOS shell.
      IF (N.EQ.2) THEN
        CALL MQ4G2W(P,RESULT)
      END IF
*
      DO 30 I=1,2
        DO 30 J=1,3
        RESULT(I,J)=SCALE**(-2*N)*RESULT(I,J)
   30 CONTINUE
      END
*
************************************************************************
* FUNCTION SQUAR2 CALCULATES THE MATRIX ELEMENT SQUARED OF THE         *
* VECTOR BOSON --> Q + QB + Q + QB.                                    *
* FULLY OPTIMIZED FOR A W+/W-                                          *
* THE INPUT VECTORS ARE IN THE /KINEMA/ P(0:3,1:NUP), WHERE THE        *
* FIRST AND THE THIRD ARE THE QUARKS.                                  *
************************************************************************
* DESCRIPTION OF THE ALGORITHM:                                        *
*   FIRST CALL CALCZS() TO CALCULATE THE H-FUNCTIONS, WHICH ARE PUT    *
*   IN THE COMMON STRUCTURE /ZESSEN/ ZS22(2,2,2,4),ZS44(2,2,4,2)       *
*   NOW EXPLAINED IN SOME DETAIL.                                      *
*     THE FIRST TWO INDICES ARE SPINOR INDICES.                        *
*     THE THIRD IS THE PERMUTATION NON-ZERO H-FUNCTIONS FOR THE        *
*     HELICITY GIVEN BY INDEX FOUR.                                    *
*       INDEX 4=1 -->  ++--, =2 --> +--+, =3 --> -++-, =4 --> --++     *
*       FOR THE ZS22 ARRAY AND                                         *
*       INDEX 4=1 -->  +-+-, =2 --> -+-+ FOR THE ZS44 ARRAY.           *
*     IN PRINCIPLE THE MATRIX ELEMENT IS                               *
*       H(Q1,Q2,Q3,Q4)-H(Q1,Q4,Q3,Q2)+H(Q3,Q4,Q1,Q2)-H(Q3,Q2,Q1,Q4)    *
*     BUT WITH THE HELICITIES OF ZS22 TWO OF THEM ARE ZERO.            *
*                                                                      *
* THE COLOR STRUCTURE OF THIS PROCESS IS QUITE SIMPLE.                 *
* ONLY TWO DIFFERENT PRODUCTS C1=N**2-1 AND C2=-(N**2-1)/N             *
* THE TWO COLOR MATRICES COL22 AND COL44 CONTAIN THE                   *
* COLOR INTERFERENCE PRODUCTS OF THE DIFFERENT H-FUNCTIONS             *
************************************************************************
      SUBROUTINE SQUAR2(P,AMMAT,APMAT)
      PARAMETER(NM=10,C1=8.0,C2=-8.0/3.0)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION P(0:3,*)
      COMMON /ZESSEN/ ZS22(2,2,2,4),ZS44(2,2,4,2)
      DIMENSION COL44(4,4),COL22(2,2),AMMAT(4,4,6),APMAT(4,4,6)
      DIMENSION ZLCUR(2,2,2),ZMATM(6,4),ZMATP(6,4)
      SAVE COL44,COL22,/DOTPRO/,/ZESSEN/
      DATA COL44/C1,C2,C1,C2,C2,C1,C2,C1,C1,C2,C1,C2,C2,C1,C2,C1/
      DATA COL22/C1,C1,C1,C1/
*
* OVERALL FACTOR I AND FACTOR 4 FOR VERTEX CONVENTION
      ZFAC=4*(0.0,1.0)
* INITIALIZED THE SPINORS AND THEIR INPRODUCTS
      CALL SETSPV(P,4,4)
*
* INIT THE TWO LEPTON CURRENTS, INDEX3=1 --> W-
      DO 10 IX=1,2
        DO 10 IY=1,2
          ZLCUR(IX,IY,1)=ZKOD(IX,10)*ZKO(IY, 9)
          ZLCUR(IX,IY,2)=ZKOD(IX, 9)*ZKO(IY,10)
   10 CONTINUE
*
      CALL CALCZS
*
* HELICITIES 2 AND 5 ARE NOT NEEDED FOR W'S
      ZMATM(1,2)=2.0*ZFAC*ZMUL(ZS22(1,1,2,1),ZLCUR(1,1,1))
      ZMATP(1,2)=2.0*ZFAC*ZMUL(ZS22(1,1,2,1),ZLCUR(1,1,2))
      ZMATM(3,1)=2.0*ZFAC*ZMUL(ZS22(1,1,1,3),ZLCUR(1,1,1))
      ZMATP(3,1)=2.0*ZFAC*ZMUL(ZS22(1,1,1,3),ZLCUR(1,1,2))
      ZMATM(4,1)=2.0*ZFAC*ZMUL(ZS22(1,1,1,4),ZLCUR(1,1,1))
      ZMATP(4,1)=2.0*ZFAC*ZMUL(ZS22(1,1,1,4),ZLCUR(1,1,2))
      ZMATM(6,1)=2.0*ZFAC*ZMUL(ZS44(1,1,1,2),ZLCUR(1,1,1))
      ZMATP(6,1)=2.0*ZFAC*ZMUL(ZS44(1,1,1,2),ZLCUR(1,1,2))
      ZMATM(6,2)=2.0*ZFAC*ZMUL(ZS44(1,1,2,2),ZLCUR(1,1,1))
      ZMATP(6,2)=2.0*ZFAC*ZMUL(ZS44(1,1,2,2),ZLCUR(1,1,2))
      ZMATM(6,4)=2.0*ZFAC*ZMUL(ZS44(1,1,4,2),ZLCUR(1,1,1))
      ZMATP(6,4)=2.0*ZFAC*ZMUL(ZS44(1,1,4,2),ZLCUR(1,1,2))
C
      AMMAT(2,2,1)=COL22(2,2)*ZMATM(1,2)*CONJG(ZMATM(1,2))
      APMAT(2,2,1)=COL22(2,2)*ZMATP(1,2)*CONJG(ZMATP(1,2))
      AMMAT(1,1,3)=COL22(1,1)*ZMATM(3,1)*CONJG(ZMATM(3,1))
      APMAT(1,1,3)=COL22(1,1)*ZMATP(3,1)*CONJG(ZMATP(3,1))
      AMMAT(1,1,4)=COL22(1,1)*ZMATM(4,1)*CONJG(ZMATM(4,1))
      APMAT(1,1,4)=COL22(1,1)*ZMATP(4,1)*CONJG(ZMATP(4,1))
C
      DO 30 IX=1,4
        DO 20 IY=IX,4
        IF((IX.NE.3).AND.(IY.NE.3)) THEN
          AMMAT(IX,IY,6)=COL44(IX,IY)*ZMATM(6,IX)*CONJG(ZMATM(6,IY))
          APMAT(IX,IY,6)=COL44(IX,IY)*ZMATP(6,IX)*CONJG(ZMATP(6,IY))
          AMMAT(IY,IX,6)=AMMAT(IX,IY,6)
          APMAT(IY,IX,6)=APMAT(IX,IY,6)
        END IF
   20   CONTINUE
   30 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE CALCZS CALCULATES THE VARIOUS H-FUNCTION AND PUTS THEM    *
* IN THE COMMON STRUCTURE /ZESSEN/ ZS22,ZS44.                          *
* THE INPUT VECTORS ARE SUPPOSED TO BE IN /KINEMA/ P(0:3,1:NUP)        *
* HOW TO CALL THE H-FUNCTIONS:                                         *
*  ARGUMENT 1-4 IS THE QUARK PERMUTATION                               *
*  ARGUMENT 5 IS THE SIGN OF THE H-FUNCTION (EXCHANGE OF TWO           *
*    IDENTICAL FERMIONS)                                               *
*  ARGUMENT 6 IS WHERE TO PUT IT IN ZESSEN                             *
*  ARGUMENT 7 IS WHERE TO PUT ITS COMPLEX CONJUGATE.                   *
*    THE PROBLEM OF AN ODD NUMBER OF QUARK-PARTICLES WITH NEGATIVE     *
*    ENERGY IS HANDLED WITHIN THE H-FUNCTIONS.                         *
************************************************************************
      SUBROUTINE CALCZS
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /ZESSEN/ ZS22(2,2,2,4),ZS44(2,2,4,2)
      SAVE /ZESSEN/
*
      CALL HPMMP(1,4,3,2,-1.0,ZS22(1,1,1,1),ZS22(1,1,1,4))
      CALL HPMMP(3,2,1,4,-1.0,ZS22(1,1,2,4),ZS22(1,1,2,1))
* HANDLE PMMP AND MPPM
      CALL HPMMP(1,2,3,4,1.0,ZS22(1,1,1,2),ZS22(1,1,1,3))
* HANDLE PMPM AND MPMP
      CALL HPMPM(1,2,3,4,1.0,ZS44(1,1,1,1),ZS44(1,1,1,2))
      CALL HPMPM(1,4,3,2,-1.0,ZS44(1,1,2,1),ZS44(1,1,2,2))
      CALL HPMPM(3,2,1,4,-1.0,ZS44(1,1,4,1),ZS44(1,1,4,2))
*
      END
*
************************************************************************
* FUNCTION SQUAR3 CALCULATES THE MATRIX ELEMENT SQUARED OF THE         *
* VECTOR BOSON --> Q + QB + Q + QB  + G.                               *
* FULLY OPTIMIZED FOR THE W+/W-                                        *
************************************************************************
* DESCRIPTION OF THE ALGORITHM:                                        *
*   FIRST CALL FILLZS() TO CALCULATE THE H-FUNCTIONS, WHICH ARE PUT    *
*   IN THE COMMON STRUCTURE /ZEVENS/ ZS(2,2,16,12)                     *
*   NOW EXPLAINED IN SOME DETAIL.                                      *
*     THE FIRST TWO INDICES ARE SPINOR INDICES.                        *
*     THE THIRD IS THE PERMUTATION NON-ZERO H-FUNCTIONS FOR THE        *
*     HELICITY GIVEN BY INDEX FOUR.                                    *
*     THE LAST INDEX ARE THE DIFFERENT SUBAMPLITUDES (12)              *
*      1: ++--;+   2:++--;-   3:+--+;+   4:+--+;-                      *
*      8: --++;-   7:--++;+   6:-++-;-   5:-++-;+                      *
*      9: +-+-;+  10:+-+-;-                                            *
*     12: -+-+;-  11:-+-+;+                                            *
*     IN PRINCIPLE THE THE MATRIX ELEMENT IS IS BUILD UP FROM 16       *
*     DIFFERENT SUB-AMPLITUDES, BUT IN THE FIRST 8 CASES HALF          *
*     OF THEM IS EQUAL TO ZERO.                                        *
*                                                                      *
*   THE COLOR MATRIX CONSISTS OF 5 DIFFERENT PRODUCTS:                 *
*     C1=N*(N^2-1)/2, C2=-(N^2-1)/(2N), C3=1/2*(N^2-1),                *
*     C4=0.0, C5=1/2*(N^2-1)/N^2.                                      *
*   WHEN SQUARING TYPE 1..8 ONLY PART OF THE MATRIX COLMAT IS USED.    *
************************************************************************
      SUBROUTINE SQUAR3(P,AMMAT,APMAT)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      PARAMETER(NM=10)
      PARAMETER(C1=12.0,C2=-8.0/6.0,C3=4.0,C4=0.0,C5=4.0/9.0)
      PARAMETER(SC2=8.0/6.0,SC3=-4.0)
      DIMENSION P(0:3,*)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /ZEVENS/ ZS(2,2,16,12)
      DIMENSION COLMAT(16,16),AMMAT(4,4,12),APMAT(4,4,12)
      DIMENSION ZMATM(12,16),ZMATP(12,16),TP(16,16,12),TM(16,16,12)
      DIMENSION ZLCUR(2,2,2)
      SAVE COLMAT,/DOTPRO/,/ZEVENS/
      DATA COLMAT
     ./ C1, C4, C2, C2, C3, C3,SC3, C4, C4, C1, C2, C2, C3, C3, C4,SC3,
     .  C4, C1, C2, C2, C3, C3, C4,SC3, C1, C4, C2, C2, C3, C3,SC3, C4,
     .  C2, C2,SC2, C4,SC3, C4, C5, C5, C2, C2, C4,SC2, C4,SC3, C5, C5,
     .  C2, C2, C4,SC2, C4,SC3, C5, C5, C2, C2,SC2, C4,SC3, C4, C5, C5,
     .  C3, C3,SC3, C4, C1, C4, C2, C2, C3, C3, C4,SC3, C4, C1, C2, C2,
     .  C3, C3, C4,SC3, C4, C1, C2, C2, C3, C3,SC3, C4, C1, C4, C2, C2,
     . SC3, C4, C5, C5, C2, C2,SC2, C4, C4,SC3, C5, C5, C2, C2, C4,SC2,
     .  C4,SC3, C5, C5, C2, C2, C4,SC2,SC3, C4, C5, C5, C2, C2,SC2, C4,
     .  C4, C1, C2, C2, C3, C3, C4,SC3, C1, C4, C2, C2, C3, C3,SC3, C4,
     .  C1, C4, C2, C2, C3, C3,SC3, C4, C4, C1, C2, C2, C3, C3, C4,SC3,
     .  C2, C2, C4,SC2, C4,SC3, C5, C5, C2, C2,SC2, C4,SC3, C4, C5, C5,
     .  C2, C2,SC2, C4,SC3, C4, C5, C5, C2, C2, C4,SC2, C4,SC3, C5, C5,
     .  C3, C3, C4,SC3, C4, C1, C2, C2, C3, C3,SC3, C4, C1, C4, C2, C2,
     .  C3, C3,SC3, C4, C1, C4, C2, C2, C3, C3, C4,SC3, C4, C1, C2, C2,
     .  C4,SC3, C5, C5, C2, C2, C4,SC2,SC3, C4, C5, C5, C2, C2,SC2, C4,
     . SC3, C4, C5, C5, C2, C2,SC2, C4, C4,SC3, C5, C5, C2, C2, C4,SC2/
*
      ZFAC=4*(0.0,1.0)
* INITIALIZE THE SPINOR AND THEIR INPRODUCTS.
      CALL SETSPV(P,5,4)
*
* INIT THE TWO LEPTON CURRENTS INDEX3=1 --> W-
      DO 10 IX=1,2
        DO 10 IY=1,2
          ZLCUR(IX,IY,1)=ZKOD(IX,10)*ZKO(IY,9)
          ZLCUR(IX,IY,2)=ZKOD(IX,9) *ZKO(IY,10)
   10 CONTINUE
*
      CALL FILLZS
*
      DO 20 I=1,2
        DO 20 J=5,8
          ZMATM(I,J)=2.0*ZFAC*ZMUL(ZS(1,1,J,I),ZLCUR(1,1,1))
          ZMATP(I,J)=2.0*ZFAC*ZMUL(ZS(1,1,J,I),ZLCUR(1,1,2))
   20 CONTINUE
*
      DO 30 I=5,8
        DO 30 J=1,4
          ZMATM(I,J)=2.0*ZFAC*ZMUL(ZS(1,1,J,I),ZLCUR(1,1,1))
          ZMATP(I,J)=2.0*ZFAC*ZMUL(ZS(1,1,J,I),ZLCUR(1,1,2))
   30 CONTINUE
*
      DO 40 I=11,12
        DO 40 J=1,16
          IF ((J.LT.9).OR.(J.GT.12)) THEN
            ZMATM(I,J)=2.0*ZFAC*ZMUL(ZS(1,1,J,I),ZLCUR(1,1,1))
            ZMATP(I,J)=2.0*ZFAC*ZMUL(ZS(1,1,J,I),ZLCUR(1,1,2))
          END IF
   40 CONTINUE
*
      DO 52 I=1,2
        DO 51 IX=5,8
          DO 50 IY=5,8
            TM(IX,IY,I)=COLMAT(IX+4,IY+4)*ZMATM(I,IX)*CONJG(ZMATM(I,IY))
            TP(IX,IY,I)=COLMAT(IX+4,IY+4)*ZMATP(I,IX)*CONJG(ZMATP(I,IY))
   50     CONTINUE
   51   CONTINUE
   52 CONTINUE
*
      DO 54 I=1,2
        AMMAT(2,2,I)=0.0
        APMAT(2,2,I)=0.0
        DO 53 IX=5,8
         DO 53 IY=5,8
          AMMAT(2,2,I)=AMMAT(2,2,I)+TM(IX,IY,I)
          APMAT(2,2,I)=APMAT(2,2,I)+TP(IX,IY,I)
   53   CONTINUE
   54 CONTINUE
*
      DO 62 I=5,8
        DO 61 IX=1,4
          DO 60 IY=1,4
            TM(IX,IY,I)=COLMAT(IX,IY)*ZMATM(I,IX)*CONJG(ZMATM(I,IY))
            TP(IX,IY,I)=COLMAT(IX,IY)*ZMATP(I,IX)*CONJG(ZMATP(I,IY))
   60     CONTINUE
   61   CONTINUE
   62 CONTINUE
*
      DO 64 I=5,8
        AMMAT(1,1,I)=0.0
        APMAT(1,1,I)=0.0
        DO 63 IX=1,4
         DO 63 IY=1,4
          AMMAT(1,1,I)=AMMAT(1,1,I)+TM(IX,IY,I)
          APMAT(1,1,I)=APMAT(1,1,I)+TP(IX,IY,I)
   63   CONTINUE
   64 CONTINUE
*
      DO 72 I=11,12
        DO 71 IX=1,16
          IF ((IX.LT.9).OR.(IX.GT.12)) THEN
          DO 70 IY=1,16
            IF ((IY.LT.9).OR.(IY.GT.12)) THEN
              TM(IX,IY,I)=COLMAT(IX,IY)*ZMATM(I,IX)*CONJG(ZMATM(I,IY))
              TP(IX,IY,I)=COLMAT(IX,IY)*ZMATP(I,IX)*CONJG(ZMATP(I,IY))
            END IF
   70     CONTINUE
          END IF
   71   CONTINUE
   72 CONTINUE
*
      DO 90 I=11,12
        DO 80 IX=1,4
          DO 80 IY=1,4
            IF ((IX.NE.3).AND.(IY.NE.3)) THEN
              AMMAT(IX,IY,I)=0.0
              APMAT(IX,IY,I)=0.0
            END IF
   80   CONTINUE
        DO 85 IX=1,16
         DO 85 IY=1,16
          IXA=(IX+3)/4
          IYA=(IY+3)/4
          IF ((IXA.NE.3).AND.(IYA.NE.3)) THEN
            AMMAT(IXA,IYA,I)=AMMAT(IXA,IYA,I)+TM(IX,IY,I)
            APMAT(IXA,IYA,I)=APMAT(IXA,IYA,I)+TP(IX,IY,I)
          END IF
   85   CONTINUE
   90 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE FILLZS CALCULATES THE VARIOUS H-FUNCTION AND PUTS THEM    *
* IN THE COMMON STRUCTURE /ZEVENS/ ZS(2,2,16,12).                      *
* HOW TO CALL THE H-FUNCTIONS:                                         *
*  ARGUMENT 1-4 IS THE QUARK PERMUTATION                               *
*  ARGUMENT 5 IS THE SIGN OF THE H-FUNCTION (EXCHANGE OF TWO           *
*    IDENTICAL FERMIONS)                                               *
*  ARGUMENT 6 DETERMINES WHAT HELICITY COMBINATION WE LOOK FOR.        *
*            Q1  Q2   Q3   Q4    G                                     *
*   TYPE=1 ;  +  -    +    -     +                                     *
*   TYPE=2 ;  +  -    +    -     -                                     *
*   TYPE=3 ;  +  -    -    +     +                                     *
*   TYPE=4 ;  +  -    -    +     -                                     *
*  ARGUMENTS 7,8 IS WHERE TO PUT IT IN ZEVENS FIRST PLACE              *
*    OF FOUR CONSECUTIVE ELEMENTS. (THE FOUR BIADB FUNCTIONS)          *
*  ARGUMENT 9,10 IS WHERE TO PUT THE COMPLEX CONJUGATES.               *
*    THE PROBLEM OF AN ODD NUMBER OF QUARK-PARTICLES WITH NEGATIVE     *
*    ENERGY IS HANDLED WITHIN THE C-FUNCTIONS.                         *
************************************************************************
      SUBROUTINE FILLZS
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* HANDLE ++--;+/- AND --++;+/-
      CALL BADB(1,4,3,2,-1.0,3,1,1,1,8)
      CALL BADB(3,2,1,4,-1.0,4,5,8,5,1)
      CALL BADB(1,4,3,2,-1.0,4,1,2,1,7)
      CALL BADB(3,2,1,4,-1.0,3,5,7,5,2)
*
* HANDLE +--+;+/- AND -++-;+/-
      CALL BADB(1,2,3,4,1.0,3,1,3,1,6)
      CALL BADB(1,2,3,4,1.0,4,1,4,1,5)
*
* HANDLE +-+-;+ AND -+-+;-
      CALL BADB(1,2,3,4,1.0,1,1,9,1,12)
      CALL BADB(1,4,3,2,-1.0,1,5,9,5,12)
      CALL BADB(3,2,1,4,-1.0,1,13,9,13,12)
*
* HANDLE +-+-;- AND -+-+;+
      CALL BADB(1,2,3,4,1.0,2,1,10,1,11)
      CALL BADB(1,4,3,2,-1.0,2,5,10,5,11)
      CALL BADB(3,2,1,4,-1.0,2,13,10,13,11)
*
      END
*
************************************************************************
* FUNCTION SQUARZ calculates the matrix element squared of the         *
* VECTOR BOSON --> Q + QB + Q + QB (+ gluon)                           *
* Optimized for a Z0                                                   *
* Input invariants are in /ZQQQQ/                                      *
************************************************************************
*                                                                      *
* Case of 4 quarks only:                                               *
*     The first and the second are indices in the squared matrix       *
*       Index 3=1 -->  ++--, =2 --> +--+, =3 --> -++-, =4 --> --++     *
*       for the ZS22 array and                                         *
*       Index 3=1 -->  +-+-, =2 --> -+-+ for the ZS44 array.           *
*                                                                      *
* Case of 4 quarks and a gluon                                         *
*     The first and the second are indices in the squared matrix       *
*     The last index are the different subamplitudes (12)              *
*      1: ++--;+   2:++--;-   3:+--+;+   4:+--+;-                      *
*      8: --++;-   7:--++;+   6:-++-;-   5:-++-;+                      *
*      9: +-+-;+  10:+-+-;-                                            *
*     12: -+-+;-  11:-+-+;+                                            *
************************************************************************
      SUBROUTINE SQUARZ(ITYP1,ITYP2,IHELRN,SW2,RESULT)
      PARAMETER(CUP=2.0/3.0,CDOWN=-1.0/3.0)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 RESULT
      COMMON /ZQQQQ/ ZS22R(2,2,8),ZS22L(2,2,8),ZS44R(4,4,4),ZS44L(4,4,4)
     .               ,IAMP22,IAMP44,FACTOR
      DIMENSION S2(4,2),S4(2,4),CHARGE(4),CISOSP(4),
     .  INDEX1(8,2),INDEX2(4,2)
      SAVE /ZQQQQ/,CHARGE,CISOSP,INDEX1,INDEX2
      DATA CHARGE/CUP,CDOWN,CUP,CDOWN/
      DATA CISOSP/0.5,-0.5,0.5,-0.5/
      DATA INDEX1/1,2,3,4,0,0,0,0,1,1,2,2,3,3,4,4/
      DATA INDEX2/1,2,0,0,1,1,2,2/
*
      CL1=CISOSP(ITYP1)-SW2*CHARGE(ITYP1)
      CR1=-SW2*CHARGE(ITYP1)
      CL2=CISOSP(ITYP2)-SW2*CHARGE(ITYP2)
      CR2=-SW2*CHARGE(ITYP2)
      IF(ITYP1.EQ.ITYP2)THEN
        CR=CR1
        CL=CL1
      ELSE
        CR=0.0
        CL=0.0
      END IF
*
* S2(1,.) IS PPMM
      S2(1,1)=CR
      S2(1,2)=CL
* S2(2,.) IS PMMP
      S2(2,1)=CR1
      S2(2,2)=CL2
* S2(3,.) IS MPPM
      S2(3,1)=CL1
      S2(3,2)=CR2
* S2(4,.) IS MMPP
      S2(4,1)=CL
      S2(4,2)=CR
*
      ZMR=(0.0,0.0)
      ZML=(0.0,0.0)
*
      IDIV=IAMP22/4
*
      DO 10 I=1,IAMP22
        IJ=INDEX1(I,IDIV)
        DO 11 IX=1,2
          DO 12 IY=1,2
            ZMR=ZMR+S2(IJ,IX)*S2(IJ,IY)*ZS22R(IX,IY,I)
            ZML=ZML+S2(IJ,IX)*S2(IJ,IY)*ZS22L(IX,IY,I)
   12     CONTINUE
   11   CONTINUE
   10 CONTINUE
*
* S4(1,.) IS PMPM
      S4(1,1)=CR1
      S4(1,2)=CR
      S4(1,3)=CR2
      S4(1,4)=CR
* S4(2,.) IS MPMP
      S4(2,1)=CL1
      S4(2,2)=CL
      S4(2,3)=CL2
      S4(2,4)=CL
*
      IDIV=IAMP44/2
*
      DO 20 I=1,IAMP44
        IJ=INDEX2(I,IDIV)
        DO 21 IX=1,4
          DO 22 IY=1,4
            ZMR=ZMR+S4(IJ,IX)*S4(IJ,IY)*ZS44R(IX,IY,I)
            ZML=ZML+S4(IJ,IX)*S4(IJ,IY)*ZS44L(IX,IY,I)
   22     CONTINUE
   21   CONTINUE
   20 CONTINUE
*
      RESULT=FACTOR*REAL(ZMR+ZML)
*
      END
*
************************************************************************
* SUBROUTINE FILLZZ calculates the various invariants needed to compute*
* 4q+g croos-sections. The results are squared with the                *
* the color matrix and brought back to a minimal form in /ZQQQQ/.      *
************************************************************************
*  Argument 1-4 are the quark labels.                                  *
*  Argument 5 is the sign of the B-function (exchange of two           *
*    identical fermions)                                               *
*  Argument 6 determines what helicity combination we look for.        *
*            q1  q2   q3   q4    g                                     *
*   type=1 ;  +  -    +    -     +                                     *
*   type=2 ;  +  -    +    -     -                                     *
*   type=3 ;  +  -    -    +     +                                     *
*   type=4 ;  +  -    -    +     -                                     *
*  Arguments 7,8 is where to put it in ZEVENS first place              *
*    of four consecutive elements. (The four BiADB functions)          *
*  Argument 9,10 is where to put the complex conjugates.               *
*    The problem of an odd number of quark-particles with negative     *
*    energy is handled within the C-functions.                         *
************************************************************************
      SUBROUTINE FILLZZ(P,ITYP,IHELRN,SW2)
      PARAMETER(NM=10)
      PARAMETER(C1=12.0,C2=-8.0/6.0,C3=4.0,C4=0.0,C5=4.0/9.0)
      PARAMETER(SC2=8.0/6.0,SC3=-4.0)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION P(0:3,*)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /ZQQQQ/ ZS22R(2,2,8),ZS22L(2,2,8),ZS44R(4,4,4),ZS44L(4,4,4)
     .               ,IAMP22,IAMP44,FACTOR
      COMMON /ZEVENS/ ZS(2,2,16,12)
      DIMENSION ZLCUR(2,2),ZRCUR(2,2)
      DIMENSION ZSR(16,12),ZSL(16,12)
      DIMENSION COL44(16,16),COL22(8,8)
      SAVE COL44,COL22,/DOTPRO/,/ZQQQQ/,/ZEVENS/
      DATA COL44
     ./ C1, C4, C2, C2, C3, C3,SC3, C4, C4, C1, C2, C2, C3, C3, C4,SC3,
     .  C4, C1, C2, C2, C3, C3, C4,SC3, C1, C4, C2, C2, C3, C3,SC3, C4,
     .  C2, C2,SC2, C4,SC3, C4, C5, C5, C2, C2, C4,SC2, C4,SC3, C5, C5,
     .  C2, C2, C4,SC2, C4,SC3, C5, C5, C2, C2,SC2, C4,SC3, C4, C5, C5,
     .  C3, C3,SC3, C4, C1, C4, C2, C2, C3, C3, C4,SC3, C4, C1, C2, C2,
     .  C3, C3, C4,SC3, C4, C1, C2, C2, C3, C3,SC3, C4, C1, C4, C2, C2,
     . SC3, C4, C5, C5, C2, C2,SC2, C4, C4,SC3, C5, C5, C2, C2, C4,SC2,
     .  C4,SC3, C5, C5, C2, C2, C4,SC2,SC3, C4, C5, C5, C2, C2,SC2, C4,
     .  C4, C1, C2, C2, C3, C3, C4,SC3, C1, C4, C2, C2, C3, C3,SC3, C4,
     .  C1, C4, C2, C2, C3, C3,SC3, C4, C4, C1, C2, C2, C3, C3, C4,SC3,
     .  C2, C2, C4,SC2, C4,SC3, C5, C5, C2, C2,SC2, C4,SC3, C4, C5, C5,
     .  C2, C2,SC2, C4,SC3, C4, C5, C5, C2, C2, C4,SC2, C4,SC3, C5, C5,
     .  C3, C3, C4,SC3, C4, C1, C2, C2, C3, C3,SC3, C4, C1, C4, C2, C2,
     .  C3, C3,SC3, C4, C1, C4, C2, C2, C3, C3, C4,SC3, C4, C1, C2, C2,
     .  C4,SC3, C5, C5, C2, C2, C4,SC2,SC3, C4, C5, C5, C2, C2,SC2, C4,
     . SC3, C4, C5, C5, C2, C2,SC2, C4, C4,SC3, C5, C5, C2, C2, C4,SC2/
      DATA COL22
     ./ C1, C4, C2, C2, C4, C1, C2, C2,
     .  C4, C1, C2, C2, C1, C4, C2, C2,
     .  C2, C2,SC2, C4, C2, C2, C4,SC2,
     .  C2, C2, C4,SC2, C2, C2,SC2, C4,
     .  C4, C1, C2, C2, C1, C4, C2, C2,
     .  C1, C4, C2, C2, C4, C1, C2, C2,
     .  C2, C2, C4,SC2, C2, C2,SC2, C4,
     .  C2, C2,SC2, C4, C2, C2, C4,SC2/
C
C The 5 vectors must be in P(mu,1) .. P(mu,5)
      CALL SETSPV(P,5,4)
C
      IF (ITYP.EQ.1) THEN
        CL=-0.5+SW2
        CR=SW2
      ELSE
        CL=0.5
        CR=0.0
      END IF
C
      DO 6 IX=1,2
        DO 6 IY=1,2
          ZRCUR(IX,IY)=ZKOD(IX,9) *ZKO(IY,10)*CR
          ZLCUR(IX,IY)=ZKOD(IX,10)*ZKO(IY, 9)*CL
    6 CONTINUE
*
* Handle ++--;+/- and --++;+/-
      CALL BADB(1,4,3,2,-1.0,3,1,1,1,8)
      CALL BADB(3,2,1,4,-1.0,4,5,8,5,1)
      CALL BADB(1,4,3,2,-1.0,4,1,2,1,7)
      CALL BADB(3,2,1,4,-1.0,3,5,7,5,2)
*
* Handle +--+;+/- and -++-;+/-
      CALL BADB(1,2,3,4,1.0,3,1,3,1,6)
      CALL BADB(3,4,1,2,1.0,4,5,6,5,3)
      CALL BADB(1,2,3,4,1.0,4,1,4,1,5)
      CALL BADB(3,4,1,2,1.0,3,5,5,5,4)
*
* Handle +-+-;+ and -+-+;-
      CALL BADB(1,2,3,4,1.0,1,1,9,1,12)
      CALL BADB(1,4,3,2,-1.0,1,5,9,5,12)
      CALL BADB(3,4,1,2,1.0,1,9,9,9,12)
      CALL BADB(3,2,1,4,-1.0,1,13,9,13,12)
*
* Handle +-+-;- and -+-+;+
      CALL BADB(1,2,3,4,1.0,2,1,10,1,11)
      CALL BADB(1,4,3,2,-1.0,2,5,10,5,11)
      CALL BADB(3,4,1,2,1.0,2,9,10,9,11)
      CALL BADB(3,2,1,4,-1.0,2,13,10,13,11)
*
      DO 8 I=1,8
        DO 8 J=1,8
          ZSR(J,I)=2.0*ZMUL(ZRCUR(1,1),ZS(1,1,J,I))
          ZSL(J,I)=2.0*ZMUL(ZLCUR(1,1),ZS(1,1,J,I))
    8 CONTINUE
*
      DO 9 I=9,12
        DO 9 J=1,16
          ZSR(J,I)=2.0*ZMUL(ZRCUR(1,1),ZS(1,1,J,I))
          ZSL(J,I)=2.0*ZMUL(ZLCUR(1,1),ZS(1,1,J,I))
    9 CONTINUE
*
      DO 12 I=1,8
        DO 10 J=1,2
          DO 10 K=1,2
            ZS22R(J,K,I)=(0.0,0.0)
            ZS22L(J,K,I)=(0.0,0.0)
   10   CONTINUE
        DO 11 J=1,8
          DO 11 K=1,8
            J1=(J+3)/4
            K1=(K+3)/4
            ZS22R(J1,K1,I)=ZS22R(J1,K1,I)+
     .                       COL22(J,K)*ZSR(J,I)*CONJG(ZSR(K,I))
            ZS22L(J1,K1,I)=ZS22L(J1,K1,I)+
     .                       COL22(J,K)*ZSL(J,I)*CONJG(ZSL(K,I))
   11   CONTINUE
   12 CONTINUE
*
      DO 22 I=9,12
        DO 20 J=1,4
          DO 20 K=1,4
            ZS44R(J,K,I-8)=(0.0,0.0)
            ZS44L(J,K,I-8)=(0.0,0.0)
   20   CONTINUE
        DO 21 J=1,16
          DO 21 K=1,16
            J1=(J+3)/4
            K1=(K+3)/4
            ZS44R(J1,K1,I-8)=ZS44R(J1,K1,I-8)+
     .                       COL44(J,K)*ZSR(J,I)*CONJG(ZSR(K,I))
            ZS44L(J1,K1,I-8)=ZS44L(J1,K1,I-8)+
     .                       COL44(J,K)*ZSL(J,I)*CONJG(ZSL(K,I))
   21   CONTINUE
   22 CONTINUE
*
      END
*
************************************************************************
* MATRIX ELEMENT FOR:  NOTHING --> Q QB Q QB + V + GLUONS              *
* THE VECTOR BOSON IS A Z                                              *
************************************************************************
* CALLING PROCEDURE:                                                   *
*   PLAB(4,10) : MOMENTA OF THE EVENT                                  *
*   N          : NUMBER OF GLUONS                                      *
*   I1,I3      : LABELS OF QUARKS IN PLAB ARRAY                        *
*   I2,I4      : LABELS OF ANTI-QUARKS IN PLAB ARRAY                   *
*   IHELRN     : IF =1 THEN A MC OVER HEL CONFIGS IS WANTED            *
*   ITYP       : 1 IS ELECTRON PAIR, 2 IS NEUTRINO PAIR                *
*   SW2        : SQUARE OF SIN(WEAK ANGLE)                             *
*   OUTPUT: THE MATRIX ELEMENTS FOR THE SUBPROCESSES RETURNED IN       *
*   RESULT(6)  :        1 = SAME QUARKS UP-TYPE                        *
*                       2 = SAME QUARKS DOWN-TYPE                      *
*                       3 = DIFFERENT QUARKS UP-TYPE                   *
*                       4 = DIFFERENT QUARKS DOWN-TYPE                 *
*                       5 = DIFFERENT QUARKS UP - DOWN PAIRS           *
*                       6 = DIFFERENT QUARKS DOWN - UP PAIRS           *
*   ONLY PART OF THE WEAK COUPLING IS INCLUDED. (LEFT OUT 1/SW/CW)     *
*   NOT INCLUDED: WEAK AND STRONG COUPLING CONSTANTS!                  *
*                 STATISTICS, SPIN AVERAGES, COLOR AVERAGES.           *
************************************************************************
* INFORMATION: THE PARTICLES IN PLAB ARE ORDERED AS FOLLOWS:           *
*             1,2 INCOMING PARTICLES FROM P, PBAR                      *
*             3,..4+N FINAL STATE PARTONS                              *
*             9,10 LEPTONS                                             *
************************************************************************
* MOST OF THE FORMULAE IN THIS PROGRAM CAN BE FOUND IN:                *
*   EXACT EXPRESSIONS FOR PROCESSES INVOLVING A VECTOR BOSON           *
*   AND UP TO FIVE PARTONS. (BERENDS, GIELE, KUIJF)                    *
************************************************************************
      SUBROUTINE FQ4Z(N,PLAB,I1,I2,I3,I4,ITYP,IHELRN,SW2,RESULT)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,10),SW2,RESULT(6)
      DIMENSION P(0:3,NM)
      COMMON /ZQQQQ/ ZS22R(2,2,8),ZS22L(2,2,8),ZS44R(4,4,4),ZS44L(4,4,4)
     .               ,IAMP22,IAMP44,FACTOR
      SAVE /ZQQQQ/
*
* TRANSFORM VECTORS
      DO 10 I=1,10
        IF ((I.LE.(N+4)).OR.(I.GE.9)) THEN
          DO 20 MU=0,3
            NU=MU
            IF(MU.EQ.0) NU=4
            P(MU,I) =REAL(PLAB(NU,I))
            IF (I.LT.3) P(MU,I)=-P(MU,I)
   20     CONTINUE
        END IF
   10 CONTINUE
*
      CALL RESHQS(P,I1,I2,I3,I4,4)
*
* Specialise to N=0 (four quarks only)
      IF (N.EQ.0) THEN
        CALL CALCZZ(P,ITYP,IHELRN,REAL(SW2))
        IAMP22=4
        IAMP44=2
        FACTOR=16.0
*
        CALL SQUARZ(1,1,IHELRN,REAL(SW2),RESULT(1))
        CALL SQUARZ(2,2,IHELRN,REAL(SW2),RESULT(2))
        CALL SQUARZ(1,3,IHELRN,REAL(SW2),RESULT(3))
        CALL SQUARZ(2,4,IHELRN,REAL(SW2),RESULT(4))
        CALL SQUARZ(1,2,IHELRN,REAL(SW2),RESULT(5))
        CALL SQUARZ(2,1,IHELRN,REAL(SW2),RESULT(6))
      END IF
*
* Specialise to N=1 (four quarks only)
      IF (N.EQ.1) THEN
        CALL FILLZZ(P,ITYP,IHELRN,REAL(SW2))
        IAMP22=8
        IAMP44=4
        FACTOR=16.0
*
        CALL SQUARZ(1,1,IHELRN,REAL(SW2),RESULT(1))
        CALL SQUARZ(2,2,IHELRN,REAL(SW2),RESULT(2))
        CALL SQUARZ(1,3,IHELRN,REAL(SW2),RESULT(3))
        CALL SQUARZ(2,4,IHELRN,REAL(SW2),RESULT(4))
        CALL SQUARZ(1,2,IHELRN,REAL(SW2),RESULT(5))
        CALL SQUARZ(2,1,IHELRN,REAL(SW2),RESULT(6))
      ENDIF
*
      END
*
************************************************************************
* SUBROUTINE CALCZZ CALCULATES THE VARIOUS INVARIANTS AND PUT THE      *
* QUARED MATRIX IN /ZQQQQ/                                             *
* THE INPUT VECTORS ARE SUPPOSED TO BE IN /KINEMA/ P(0:3,1:NUP)        *
* HOW TO CALL THE H-FUNCTIONS:                                         *
*  ARGUMENT 1-4 IS THE QUARK PERMUTATION                               *
*  ARGUMENT 5 IS THE SIGN OF THE H-FUNCTION (EXCHANGE OF TWO           *
*    IDENTICAL FERMIONS)                                               *
*  ARGUMENT 6 IS WHERE TO PUT IT IN ZESSEN                             *
*  ARGUMENT 7 IS WHERE TO PUT ITS COMPLEX CONJUGATE.                   *
*    THE PROBLEM OF AN ODD NUMBER OF QUARK-PARTICLES WITH NEGATIVE     *
*    ENERGY IS HANDLED WITHIN THE H-FUNCTIONS.                         *
************************************************************************
      SUBROUTINE CALCZZ(P,ITYP,IHELRN,SW2)
      PARAMETER(NM=10)
      PARAMETER(C1=8.0,C2=-8.0/3.0)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION P(0:3,*)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION COL44(4,4),COL22(2,2)
      COMMON /ZQQQQ/ ZS22R(2,2,8),ZS22L(2,2,8),ZS44R(4,4,4),ZS44L(4,4,4)
     .               ,IAMP22,IAMP44,FACTOR
      DIMENSION ZS22(2,2,2,4),ZS44(2,2,4,2),ZLCUR(2,2),ZRCUR(2,2)
      DIMENSION ZS2R(2,4),ZS2L(2,4),ZS4R(4,2),ZS4L(4,2)
      SAVE COL44,COL22,/DOTPRO/,/ZQQQQ/
      DATA COL44/C1,C2,C1,C2,C2,C1,C2,C1,C1,C2,C1,C2,C2,C1,C2,C1/
      DATA COL22/C1,C1,C1,C1/
*
* THE 4 VECTORS MUST BE IN P(MU,1) .. P(MU,4)
      CALL SETSPV(P,4,4)
*
      IF (ITYP.EQ.1) THEN
        CL=-0.5+SW2
        CR=SW2
      ELSE
        CL=0.5
        CR=0.0
      END IF
*
      DO 10 IX=1,2
        DO 10 IY=1,2
          ZRCUR(IX,IY)=ZKOD(IX,9) *ZKO(IY,10)*CR
          ZLCUR(IX,IY)=ZKOD(IX,10)*ZKO(IY,9) *CL
   10 CONTINUE
*
* HANDLE PPMM AND MMPP
      CALL HPMMP(1,4,3,2,-1.0,ZS22(1,1,1,1),ZS22(1,1,1,4))
      ZS2R(1,1)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,1,1))
      ZS2L(1,1)=2.0*ZMUL(ZLCUR(1,1),ZS22(1,1,1,1))
      ZS2R(1,4)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,1,4))
      ZS2L(1,4)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,1,4))
      CALL HPMMP(3,2,1,4,-1.0,ZS22(1,1,2,4),ZS22(1,1,2,1))
      ZS2R(2,4)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,2,4))
      ZS2L(2,4)=2.0*ZMUL(ZLCUR(1,1),ZS22(1,1,2,4))
      ZS2R(2,1)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,2,1))
      ZS2L(2,1)=2.0*ZMUL(ZLCUR(1,1),ZS22(1,1,2,1))
*
* HANDLE PMMP AND MPPM
      CALL HPMMP(1,2,3,4,1.0,ZS22(1,1,1,2),ZS22(1,1,1,3))
      ZS2R(1,2)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,1,2))
      ZS2L(1,2)=2.0*ZMUL(ZLCUR(1,1),ZS22(1,1,1,2))
      ZS2R(1,3)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,1,3))
      ZS2L(1,3)=2.0*ZMUL(ZLCUR(1,1),ZS22(1,1,1,3))
      CALL HPMMP(3,4,1,2,1.0,ZS22(1,1,2,3),ZS22(1,1,2,2))
      ZS2R(2,3)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,2,3))
      ZS2L(2,3)=2.0*ZMUL(ZLCUR(1,1),ZS22(1,1,2,3))
      ZS2R(2,2)=2.0*ZMUL(ZRCUR(1,1),ZS22(1,1,2,2))
      ZS2L(2,2)=2.0*ZMUL(ZLCUR(1,1),ZS22(1,1,2,2))
*
      DO 28 I=1,4
        DO 26 IX=1,2
          DO 24 IY=1,2
            ZS22R(IX,IY,I)=COL22(IX,IY)*ZS2R(IX,I)*CONJG(ZS2R(IY,I))
            ZS22L(IX,IY,I)=COL22(IX,IY)*ZS2L(IX,I)*CONJG(ZS2L(IY,I))
   24     CONTINUE
   26   CONTINUE
   28 CONTINUE
*
* HANDLE PMPM AND MPMP
      CALL HPMPM(1,2,3,4,1.0,ZS44(1,1,1,1),ZS44(1,1,1,2))
      ZS4R(1,1)=2.0*ZMUL(ZRCUR(1,1),ZS44(1,1,1,1))
      ZS4L(1,1)=2.0*ZMUL(ZLCUR(1,1),ZS44(1,1,1,1))
      ZS4R(1,2)=2.0*ZMUL(ZRCUR(1,1),ZS44(1,1,1,2))
      ZS4L(1,2)=2.0*ZMUL(ZLCUR(1,1),ZS44(1,1,1,2))
      CALL HPMPM(1,4,3,2,-1.0,ZS44(1,1,2,1),ZS44(1,1,2,2))
      ZS4R(2,1)=2.0*ZMUL(ZRCUR(1,1),ZS44(1,1,2,1))
      ZS4L(2,1)=2.0*ZMUL(ZLCUR(1,1),ZS44(1,1,2,1))
      ZS4R(2,2)=2.0*ZMUL(ZRCUR(1,1),ZS44(1,1,2,2))
      ZS4L(2,2)=2.0*ZMUL(ZLCUR(1,1),ZS44(1,1,2,2))
      CALL HPMPM(3,4,1,2,1.0,ZS44(1,1,3,1),ZS44(1,1,3,2))
      ZS4R(3,1)=2.0*ZMUL(ZRCUR(1,1),ZS44(1,1,3,1))
      ZS4L(3,1)=2.0*ZMUL(ZLCUR(1,1),ZS44(1,1,3,1))
      ZS4R(3,2)=2.0*ZMUL(ZRCUR(1,1),ZS44(1,1,3,2))
      ZS4L(3,2)=2.0*ZMUL(ZLCUR(1,1),ZS44(1,1,3,2))
      CALL HPMPM(3,2,1,4,-1.0,ZS44(1,1,4,1),ZS44(1,1,4,2))
      ZS4R(4,1)=2.0*ZMUL(ZRCUR(1,1),ZS44(1,1,4,1))
      ZS4L(4,1)=2.0*ZMUL(ZLCUR(1,1),ZS44(1,1,4,1))
      ZS4R(4,2)=2.0*ZMUL(ZRCUR(1,1),ZS44(1,1,4,2))
      ZS4L(4,2)=2.0*ZMUL(ZLCUR(1,1),ZS44(1,1,4,2))
*
      DO 38 I=1,2
        DO 36 IX=1,4
          DO 34 IY=1,4
            ZS44R(IX,IY,I)=COL44(IX,IY)*ZS4R(IX,I)*CONJG(ZS4R(IY,I))
            ZS44L(IX,IY,I)=COL44(IX,IY)*ZS4L(IX,I)*CONJG(ZS4L(IY,I))
   34     CONTINUE
   36   CONTINUE
   38 CONTINUE
*
      END
*
************************************************************************
* MQ4G2W determines the matrix elements for q qb' r rb g g.            *
* Input: P(0:3,1:10) the set of 6 parton momenta (1-6)                 *
*                    the decay products (9-10)                         *
* Not included: coupling constants, statistics, colour averaging       *
************************************************************************
* Original concept: W. T. Giele.  (1990)                               *
* 'Technical' improvements: H. Kuijf & B. Tausk. (may 1990)            *
************************************************************************
      SUBROUTINE MQ4G2W(P,RESULT)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 RESULT(2,3)
      DIMENSION P(0:3,NM)
      DIMENSION ZBW(48,9:16,2),
     .          ZM1(2),ZM2(2),ZM3(2),
     .          ICA(12,12),ICB(12,12),ICD(12,12)
      PARAMETER (IP1= 216,IP2= 27,IP3= 3,IP4= 24,
     .           IP5= 72 ,IP6= 9 ,IP7= 8,IP8= 1)
      PARAMETER (IM1=-216,IM2=-27,IM3=-3,IM4=-24,
     .           IM5=-72 ,IM6=-9 ,IM7=-8,IM8=-1)
      SAVE ICA,ICB,ICD
      DATA ICA /IP1,  0,IP2,IM4,IP3,IM4,IM2,  0,IP2,IP3,IM4,IP3,
     .            0,IP1,  0,IP3,IM4,IM4,  0,IP2,  0,IM4,IM4,IP3,
     .          IP2,  0,IP1,IM4,IM4,IM4,IP2,  0,IM2,IP3,IP3,IP3,
     .          IM4,IP3,IM4,IP4,  0,IP3,IP3,IM4,IP3,IM3,  0,IP3,
     .          IP3,IM4,IM4,  0,IP4,  0,IM4,IM4,IP3,  0,IP3,  0,
     .          IM4,IM4,IM4,IP3,  0,IP4,IP3,IP3,IP3,IP3,  0,IM3,
     .          IM2,  0,IP2,IP3,IM4,IP3,IP1,  0,IP2,IM4,IP3,IM4,
     .            0,IP2,  0,IM4,IM4,IP3,  0,IP1,  0,IP3,IM4,IM4,
     .          IP2,  0,IM2,IP3,IP3,IP3,IP2,  0,IP1,IM4,IM4,IM4,
     .          IP3,IM4,IP3,IM3,  0,IP3,IM4,IP3,IM4,IP4,  0,IP3,
     .          IM4,IM4,IP3,  0,IP3,  0,IP3,IM4,IM4,  0,IP4,  0,
     .          IP3,IP3,IP3,IP3,  0,IM3,IM4,IM4,IM4,IP3,  0,IP4/
      DATA ICB /IM5,IM5,IM5,IP6,  0,IP5,IP6,IP6,IP6,IP6,  0,IM6,
     .          IM5,IM5,IP6,  0,IP6,  0,IP6,IM5,IM5,  0,IP5,  0,
     .          IM5,IP6,IM5,IP5,  0,IP6,IP6,IM5,IP6,IM6,  0,IP6,
     .          IP6,  0,IP5,IM7,IM7,IM7,IP6,  0,IM6,IP8,IP8,IP8,
     .            0,IP6,  0,IM7,IM7,IP8,  0,IP5,  0,IP8,IM7,IM7,
     .          IP5,  0,IP6,IM7,IP8,IM7,IM6,  0,IP6,IP8,IM7,IP8,
     .          IP6,IP6,IP6,IP6,  0,IM6,IM5,IM5,IM5,IP6,  0,IP5,
     .          IP6,IM5,IM5,  0,IP5,  0,IM5,IM5,IP6,  0,IP6,  0,
     .          IP6,IM5,IP6,IM6,  0,IP6,IM5,IP6,IM5,IP5,  0,IP6,
     .          IP6,  0,IM6,IP8,IP8,IP8,IP6,  0,IP5,IM7,IM7,IM7,
     .            0,IP5,  0,IP8,IM7,IM7,  0,IP6,  0,IM7,IM7,IP8,
     .          IM6,  0,IP6,IP8,IM7,IP8,IP5,  0,IP6,IM7,IP8,IM7/
      DATA ICD /IM5,IP6,IM5,IP5,  0,IP6,IP6,IM5,IP6,IM6,  0,IP6,
     .          IP6,IM5,IM5,  0,IP5,  0,IM5,IM5,IP6,  0,IP6,  0,
     .          IM5,IM5,IM5,IP6,  0,IP5,IP6,IP6,IP6,IP6,  0,IM6,
     .          IP5,  0,IP6,IM7,IP8,IM7,IM6,  0,IP6,IP8,IM7,IP8,
     .            0,IP5,  0,IP8,IM7,IM7,  0,IP6,  0,IM7,IM7,IP8,
     .          IP6,  0,IP5,IM7,IM7,IM7,IP6,  0,IM6,IP8,IP8,IP8,
     .          IP6,IM5,IP6,IM6,  0,IP6,IM5,IP6,IM5,IP5,  0,IP6,
     .          IM5,IM5,IP6,  0,IP6,  0,IP6,IM5,IM5,  0,IP5,  0,
     .          IP6,IP6,IP6,IP6,  0,IM6,IM5,IM5,IM5,IP6,  0,IP5,
     .          IM6,  0,IP6,IP8,IM7,IP8,IP5,  0,IP6,IM7,IP8,IM7,
     .            0,IP6,  0,IM7,IM7,IP8,  0,IP5,  0,IP8,IM7,IM7,
     .          IP6,  0,IM6,IP8,IP8,IP8,IP6,  0,IP5,IM7,IM7,IM7/
*
      CALL SETSPV(P,6,4)
      CALL CALCBW(ZBW)
      FACTOR=16.0*32.0/27.0
      ZM1(1)=(0.0,0.0)
      ZM1(2)=(0.0,0.0)
      DO 11 I=1,12
        DO 11 J=I,12
          COLOR=FACTOR*ICA(I,J)
          IF (I.LT.J) COLOR=2.0*COLOR
          DO 21 IHEL=9,16
            ZM1(1)=ZM1(1)+COLOR*ZBW(I,IHEL,1)*CONJG(ZBW(J,IHEL,1))
            ZM1(2)=ZM1(2)+COLOR*ZBW(I,IHEL,2)*CONJG(ZBW(J,IHEL,2))
   21     CONTINUE
   11 CONTINUE
      RESULT(1,1)=REAL(ZM1(1))
      RESULT(2,1)=REAL(ZM1(2))
*
      ZM2(2)=ZM1(2)
      ZM2(1)=ZM1(1)
      DO 12 I=37,48
        DO 12 J=I,48
          COLOR=FACTOR*ICA(I-36,J-36)
          IF (I.LT.J) COLOR=2.0*COLOR
          DO 22 IHEL=9,16
            ZM2(2)=ZM2(2)+COLOR*ZBW(I,IHEL,2)*CONJG(ZBW(J,IHEL,2))
            ZM2(1)=ZM2(1)+COLOR*ZBW(I,IHEL,1)*CONJG(ZBW(J,IHEL,1))
   22     CONTINUE
   12 CONTINUE
*
      DO 13 I=1,12
        DO 13 J=37,48
          COLOR=FACTOR*ICD(I,J-36)
          DO 23 IHEL=9,12
            ZM2(1)=ZM2(1)+2.0*COLOR*ZBW(I,IHEL,1)*CONJG(ZBW(J,IHEL,1))
            ZM2(2)=ZM2(2)+2.0*COLOR*ZBW(I,IHEL,2)*CONJG(ZBW(J,IHEL,2))
   23     CONTINUE
   13 CONTINUE
      RESULT(1,3)=REAL(ZM2(1))
      RESULT(2,3)=REAL(ZM2(2))
*
      ZM3(1)=ZM1(1)
      ZM3(2)=ZM1(2)
      DO 14 I=13,24
        DO 14 J=I,24
          COLOR=FACTOR*ICA(I-12,J-12)
          IF (I.LT.J) COLOR=2.0*COLOR
          DO 14 IHEL=9,16
            ZM3(1)=ZM3(1)+COLOR*ZBW(I,IHEL,1)*CONJG(ZBW(J,IHEL,1))
            ZM3(2)=ZM3(2)+COLOR*ZBW(I,IHEL,2)*CONJG(ZBW(J,IHEL,2))
   14 CONTINUE
*
      DO 15 I=1,12
        DO 15 J=13,24
          COLOR=FACTOR*ICB(I,J-12)
          DO 15 IHEL=9,12
            ZM3(1)=ZM3(1)+2.0*COLOR*ZBW(I,IHEL,1)*CONJG(ZBW(J,IHEL,1))
            ZM3(2)=ZM3(2)+2.0*COLOR*ZBW(I,IHEL,2)*CONJG(ZBW(J,IHEL,2))
   15 CONTINUE
      RESULT(1,2)=REAL(ZM3(1))
      RESULT(2,2)=REAL(ZM3(2))
*
      END
*
      SUBROUTINE CALCBW(ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION ZB(48,9:16,2),ZBI(2,2,6),ZBIC(2,2,6),
     .          ZLCUR(2,2,2),IPERM(8)
      SAVE IPERM,I1,I2,I3,I4,IG1,IG2,IEL,INE,/DOTPRO/
      DATA IPERM/1,3,2,4,5,7,6,8/
      DATA I1,I2,I3,I4,IG1,IG2,IEL,INE
     .    / 1, 2, 3, 4,  5,  6,  9, 10/
*
      DO 1 IAD=1,2
        DO 1 IB=1,2
          ZLCUR(IAD,IB,2)=ZKOD(IAD,INE)*ZKO(IB,IEL)
          ZLCUR(IAD,IB,+1)=ZKOD(IAD,IEL)*ZKO(IB,INE)
    1 CONTINUE
      DO 10 ITYPE=1,8
        IPGLU=IPERM(ITYPE)
        CALL Q4G2V(I1,I2,I3,I4,IG1,IG2,ZBI,ZBIC,ITYPE)
        DO 11 I=1,6
          ZB(I,ITYPE+8,2)=ZMUL(ZBIC(1,1,I),ZLCUR(1,1,2))
          ZB(I,ITYPE+8,1)=ZMUL(ZBIC(1,1,I),ZLCUR(1,1,1))
   11   CONTINUE
        CALL Q4G2V(I1,I2,I3,I4,IG2,IG1,ZBI,ZBIC,IPGLU)
        DO 12 I=7,12
          ZB(I,ITYPE+8,2)=ZMUL(ZBIC(1,1,I-6),ZLCUR(1,1,2))
          ZB(I,ITYPE+8,1)=ZMUL(ZBIC(1,1,I-6),ZLCUR(1,1,1))
   12   CONTINUE
        CALL Q4G2V(I3,I2,I1,I4,IG1,IG2,ZBI,ZBIC,ITYPE)
        DO 13 I=13,18
          ZB(I,ITYPE+8,2)=ZMUL(ZBIC(1,1,I-12),ZLCUR(1,1,2))
          ZB(I,ITYPE+8,1)=ZMUL(ZBIC(1,1,I-12),ZLCUR(1,1,1))
   13   CONTINUE
        CALL Q4G2V(I3,I2,I1,I4,IG2,IG1,ZBI,ZBIC,IPGLU)
        DO 14 I=19,24
          ZB(I,ITYPE+8,2)=ZMUL(ZBIC(1,1,I-18),ZLCUR(1,1,2))
          ZB(I,ITYPE+8,1)=ZMUL(ZBIC(1,1,I-18),ZLCUR(1,1,1))
   14   CONTINUE
        CALL Q4G2V(I1,I4,I3,I2,IG1,IG2,ZBI,ZBIC,ITYPE)
        DO 15 I=37,42
          ZB(I,ITYPE+8,2)=ZMUL(ZBIC(1,1,I-36),ZLCUR(1,1,2))
          ZB(I,ITYPE+8,1)=ZMUL(ZBIC(1,1,I-36),ZLCUR(1,1,1))
   15   CONTINUE
        CALL Q4G2V(I1,I4,I3,I2,IG2,IG1,ZBI,ZBIC,IPGLU)
        DO 16 I=43,48
          ZB(I,ITYPE+8,2)=ZMUL(ZBIC(1,1,I-42),ZLCUR(1,1,2))
          ZB(I,ITYPE+8,1)=ZMUL(ZBIC(1,1,I-42),ZLCUR(1,1,1))
   16   CONTINUE
   10 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE Q4G2V calls the basic colour decomposed kinematical       *
* invariants. The ITYPE denotes the helicity combination.              *
*                          Q1  Q2  Q3  Q4  G1  G2                      *
*                ITYPE=1 ; +   -   +   -   +   +                       *
*                ITYPE=2 ; +   -   +   -   +   -                       *
*                ITYPE=3 ; +   -   +   -   -   +                       *
*                ITYPE=4 ; +   -   +   -   -   -                       *
*                ITYPE=5 ; +   -   -   +   +   +                       *
*                ITYPE=6 ; +   -   -   +   +   -                       *
*                ITYPE=7 ; +   -   -   +   -   +                       *
*                ITYPE=8 ; +   -   -   +   -   -                       *
************************************************************************
      SUBROUTINE Q4G2V(I1,I2,I3,I4,IG1,IG2,ZB,ZBC,ITYPE)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZB(2,2,6),ZBC(2,2,6)
*
      CALL B1(I1,I2,I3,I4,IG1,IG2,ZB(1,1,1),ZBC(1,1,1),ITYPE)
      CALL B2(I1,I2,I3,I4,IG1,IG2,ZB(1,1,2),ZBC(1,1,2),ITYPE)
      CALL B3(I1,I2,I3,I4,IG1,IG2,ZB(1,1,3),ZBC(1,1,3),ITYPE)
      CALL B4(I1,I2,I3,I4,IG1,IG2,ZB(1,1,4),ZBC(1,1,4),ITYPE)
      CALL B5(I1,I2,I3,I4,IG1,IG2,ZB(1,1,5),ZBC(1,1,5),ITYPE)
      CALL B6(I1,I2,I3,I4,IG1,IG2,ZB(1,1,6),ZBC(1,1,6),ITYPE)
*
      END
*
      SUBROUTINE B1(I1,I2,I3,I4,IG1,IG2,ZB,ZBC,ITYPE)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZB(2,2),ZBC(2,2)
      DIMENSION ICONV(8)
      SAVE ICONV
      DATA ICONV /4,2,3,1,8,6,7,5/
*
      CALL B3H(I2,I1,I4,I3,IG2,IG1,ZBC,ICONV(ITYPE))
      CALL CONGAT(ZBC,ZB)
*
      DO 10 IAD=1,2
        DO 10 IB=1,2
          ZB(IAD,IB)=-ZB(IAD,IB)
          ZBC(IAD,IB)=-ZBC(IAD,IB)
   10 CONTINUE
*
      END
C
      SUBROUTINE B2(I1,I2,I3,I4,IG1,IG2,ZB,ZBC,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZBC(2,2)
      SAVE /DOTPRO/,/B2PROP/
*
      P34=2.0*RR(I3,I4)
      P345=P34+2.0*(RR(I3,IG1)+RR(I4,IG1))
      P346=P34+2.0*(RR(I3,IG2)+RR(I4,IG2))
      P1345=P345+2.0*(RR(I3,I1)+RR(I4,I1)+RR(IG1,I1))
      P2346=P346+2.0*(RR(I3,I2)+RR(I4,I2)+RR(IG2,I2))
      P3456=P345+2.0*(RR(I3,IG2)+RR(I4,IG2)+RR(IG1,IG2))
      P13456=P1345+2.0*(RR(I1,IG2)+RR(I3,IG2)+RR(I4,IG2)
     .                 +RR(IG1,IG2))
      P23456=P2346+2.0*(RR(I2,IG1)+RR(I3,IG1)+RR(I4,IG1)
     .                 +RR(IG2,IG1))
*
      IF (ITYPE.EQ.1) CALL B2H1(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.2) CALL B2H2(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.3) CALL B2H3(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.4) CALL B2H4(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.5) CALL B2H5(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.6) CALL B2H6(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.7) CALL B2H7(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.8) CALL B2H8(I1,I2,I3,I4,IG1,IG2,ZB)
*
      CALL CONGAT(ZB,ZBC)
*
      END
*
      SUBROUTINE B2H1(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),ZM6(2)
      SAVE /DOTPRO/,/B2PROP/
*
      Z0=ZUD(I2,I4)/(ZUD(I2,IG2)*ZUD(I4,IG1))
      Z1=ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2)
      Z11=-Z0/(ZUD(I1,IG1)*ZUD(I3,IG2)*P2346*P346)
      Z12=Z0/(ZUD(I3,IG2)*P2346*P23456*P346)
      Z13=Z0*(ZD(I3,IG1)*ZUD(I3,I4)+ZD(IG2,IG1)*ZUD(IG2,I4))
     .    /(ZUD(I3,IG2)*P23456*P346*P3456)
      Z14=Z0*Z1/(ZUD(I1,IG1)*P34*P2346*P346)
      Z15=-Z0*Z1*(ZD(I2,I3)*ZUD(I2,I4)+ZD(IG2,I3)*ZUD(IG2,I4))
     .    /(P34*P2346*P23456*P346)
      Z16=-Z0*ZUD(I3,I4)*ZD(I3,IG1)*(ZD(I3,IG2)*ZUD(I3,I2)
     .    +ZD(I4,IG2)*ZUD(I4,I2)+ZD(IG1,IG2)*ZUD(IG1,I2))
     .    /(P34*P23456*P345*P3456)
      Z17=-Z0*ZUD(I3,I4)*ZD(I3,IG2)*(ZD(I3,IG1)*ZUD(I3,I2)
     .    +ZD(I4,IG1)*ZUD(I4,I2)+ZD(IG2,IG1)*ZUD(IG2,I2))
     .    /(P34*P23456*P346*P3456)
      DO 11 IAD=1,2
        ZM1(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(IG1,I4)*ZKOD(IAD,IG1)
   11 CONTINUE
      DO 12 IB=1,2
        ZM2(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .         +ZD(IG1,I3)*ZKO(IB,IG1)+ZD(IG2,I3)*ZKO(IB,IG2)
        ZM3(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
   12 CONTINUE
      DO 13 IAD=1,2
        Z0=ZKOD(IAD,I1)
        DO 13 IB =1,2
          ZB(IAD,IB)=Z11*ZM1(IAD)
     .      *(ZUD(I2,I3)*(ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .                  +ZD(IG2,I3)*ZKO(IB,IG2))
     .      +ZUD(I2,IG2)*(ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .                  +ZD(I4,IG2)*ZKO(IB,I4)))
          ZB(IAD,IB)=ZB(IAD,IB)+Z12*Z0*ZM3(IB)
     .      *(ZUD(I2,I3)*(ZD(I2,I3)*ZUD(I2,I4)+ZD(IG2,I3)*ZUD(IG2,I4))
     .      +ZUD(I2,IG2)*(ZD(I2,IG2)*ZUD(I2,I4)+ZD(I3,IG2)*ZUD(I3,I4)))
          ZB(IAD,IB)=ZB(IAD,IB)+Z13*Z0
     .      *(ZUD(I2,I3)*(ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .                  +ZD(IG1,I3)*ZKO(IB,IG1)+ZD(IG2,I3)*ZKO(IB,IG2))
     .      +ZUD(I2,IG2)*(ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .                  +ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1)))
          ZB(IAD,IB)=ZB(IAD,IB)+Z14*ZM1(IAD)
     .      *(ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .      +ZD(IG2,I3)*ZKO(IB,IG2))
          ZB(IAD,IB)=ZB(IAD,IB)+Z0*(Z15*ZM3(IB)+(Z16+Z17)*ZM2(IB))
   13 CONTINUE
      Z0=1/(ZUD(I2,IG2)*ZUD(I4,IG1)*P13456)
      Z1=ZD(I1,I3)*ZUD(I1,I4)+ZD(IG1,I3)*ZUD(IG1,I4)
      Z2=ZD(I1,IG2)*ZUD(I1,I4)+ZD(I3,IG2)*ZUD(I3,I4)
     .  +ZD(IG1,IG2)*ZUD(IG1,I4)
      Z11=-Z0*Z1*Z2/(ZUD(I1,IG1)*P1345*P34)
      Z12=Z0/(ZUD(I1,IG1)*ZUD(I3,IG2)*P346)
      Z13=Z0*Z2*ZD(I1,I3)*ZD(I3,IG1)*ZUD(I3,I4)
     .  /(P1345*P34*P345)
      Z14=-Z0*Z1*(ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2))
     .  /(ZUD(I1,IG1)*P34*P346)
      Z15=-Z0*(ZD(I3,IG1)*ZUD(I3,I4)+ZD(IG2,IG1)*ZUD(IG2,I4))
     .       *(ZD(I3,I1)*ZUD(I3,I2)+ZD(IG2,I1)*ZUD(IG2,I2))
     .     /(ZUD(I3,IG2)*P346*P3456)
      Z16=Z0*ZD(I1,I3)*ZD(I3,IG1)*ZUD(I3,I4)
     .   *(ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2)
     .   +ZD(IG1,IG2)*ZUD(IG1,I2))/(P34*P345*P3456)
      Z17=Z0*ZD(I1,I3)*ZD(I3,IG2)*ZUD(I3,I4)
     .  *(ZD(I3,IG1)*ZUD(I3,I2)+ZD(I4,IG1)*ZUD(I4,I2)
     .  +ZD(IG2,IG1)*ZUD(IG2,I2))/(P34*P346*P3456)
      DO 14 IAD=1,2
        ZM4(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .          +ZUD(IG1,I4)*ZKOD(IAD,IG1)+ZUD(IG2,I4)*ZKOD(IAD,IG2)
        ZM5(IAD)=ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(I3,I2)*ZKOD(IAD,I3)
     .          +ZUD(I4,I2)*ZKOD(IAD,I4)+ZUD(IG1,I2)*ZKOD(IAD,IG1)
     .          +ZUD(IG2,I2)*ZKOD(IAD,IG2)
   14 CONTINUE
      DO 15 IB=1,2
        Z0=ZKO(IB,I2)
        DO 15 IAD=1,2
          ZB(IAD,IB)=ZB(IAD,IB)+Z0*Z12*ZM4(IAD)
     .      *(ZUD(I2,I3)*(ZD(I1,I3)*ZUD(I1,I4)+ZD(IG1,I3)*ZUD(IG1,I4))
     .     +ZUD(I2,IG2)*(ZD(I1,IG2)*ZUD(I1,I4)+ZD(IG1,IG2)*ZUD(IG1,I4)))
          ZB(IAD,IB)=ZB(IAD,IB)+Z0*((Z14+Z15+Z16+Z17)*ZM4(IAD)
     .                            +(Z11+Z13)*ZM5(IAD))
   15 CONTINUE
      Z0=ZUD(I2,I4)
      Z11=Z0*ZD(I3,IG2)/(ZUD(I1,IG1)*ZUD(IG1,I4)*P34*P346*P2346)
      Z12=-Z0*ZD(I3,IG2)*(ZD(I2,IG2)*ZUD(I2,I4)+ZD(I3,IG2)*ZUD(I3,I4))
     .  /(ZUD(IG1,I4)*P2346*P23456*P34*P346)
      Z13=-Z0*(ZD(I3,IG1)*ZUD(I3,I2)+ZD(IG2,IG1)*ZUD(IG2,I2))
     .  /(ZUD(I2,IG2)*ZUD(IG2,I3)*P23456*P346*P3456)
      Z1=-Z0/(ZUD(I2,IG2)*ZUD(I4,IG1)*P34*P23456*P345*P3456)
      Z14=Z1*ZUD(I4,IG1)*ZUD(I2,IG2)*ZD(IG1,IG2)*ZD(I3,IG1)
      Z15=-Z1*ZUD(I4,IG1)*ZD(I3,IG1)
     .  *(ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2)
     .  +ZD(IG1,IG2)*ZUD(IG1,I2))
      Z16=-Z1*ZUD(I2,IG2)*ZUD(I3,I4)*ZD(I3,IG1)*ZD(I3,IG2)
      Z1=-Z0/(ZUD(I2,IG2)*ZUD(I4,IG1)*P34*P23456*P346*P3456)
      Z17=Z1*ZUD(I4,IG1)*ZUD(I2,IG2)*ZD(IG2,IG1)*ZD(I3,IG2)
      Z18=-Z1*ZUD(I4,IG1)*ZD(I3,IG1)
     .  *(ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2))
      Z19=-Z1*ZUD(I2,IG2)*ZD(I3,IG2)
     .  *(ZD(I3,IG1)*ZUD(I3,I4)+ZD(IG2,IG1)*ZUD(IG2,I4))
      DO 16 IB=1,2
        ZM6(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .           +ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1)
   16 CONTINUE
      DO 17 IAD=1,2
        DO 17 IB=1,2
          ZB(IAD,IB)=ZB(IAD,IB)+Z11*ZM1(IAD)
     .      *(ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .      +ZD(I4,IG2)*ZKO(IB,I4))
          ZB(IAD,IB)=ZB(IAD,IB)+ZKOD(IAD,I1)
     .      *(ZM6(IB)*(Z14+Z16+Z19)+ZM3(IB)*(Z12+Z13+Z15+Z17+Z18))
   17 CONTINUE
      Z0=1.0/P13456
      Z11=-Z0*ZD(I3,IG2)
     .  *(ZD(I1,IG2)*ZUD(I1,I4)+ZD(IG1,IG2)*ZUD(IG1,I4))
     .  /(ZUD(I1,IG1)*ZUD(IG1,I4)*P346*P34)
      Z12=Z0*ZD(I1,IG1)*ZD(I3,IG1)
     .  *(ZD(I1,IG2)*ZUD(I1,I4)+ZD(I3,IG2)*ZUD(I3,I4)
     .  +ZD(IG1,IG2)*ZUD(IG1,I4))
     .  /(ZUD(IG2,I2)*P1345*P345*P34)
      Z13=Z0*ZD(I1,IG1)
     .   *(ZD(I3,IG1)*ZUD(I3,I2)+ZD(IG2,IG1)*ZUD(IG2,I2))
     .   /(ZUD(I2,IG2)*ZUD(IG2,I3)*P346*P3456)
      Z1=Z0/(ZUD(I4,IG1)*ZUD(I2,IG2)*P34*P345*P3456)
      Z14=Z1*ZUD(I2,IG2)*ZUD(I4,IG1)*ZD(IG1,IG2)*ZD(I3,IG1)*ZD(I1,IG2)
      Z14=Z14-Z1*ZUD(I2,IG2)*ZD(I1,IG2)*ZD(I3,IG2)
     .          *ZD(I3,IG1)*ZUD(I3,I4)
      Z14=Z14-Z1*ZUD(I4,IG1)*ZD(I1,IG1)*ZD(I3,IG1)
     .   *(ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2)
     .   +ZD(IG1,IG2)*ZUD(IG1,I2))
      Z1=Z0/(ZUD(I4,IG1)*ZUD(I2,IG2)*P34*P346*P3456)
      Z15=Z1*ZUD(I2,IG2)*ZUD(I4,IG1)*ZD(IG2,IG1)*ZD(I3,IG2)*ZD(I1,IG1)
      Z15=Z15-Z1*ZUD(I2,IG2)*ZD(I1,IG2)*ZD(I3,IG2)
     .  *(ZD(I3,IG1)*ZUD(I3,I4)+ZD(IG2,IG1)*ZUD(IG2,I4))
      Z15=Z15-Z1*ZUD(I4,IG1)*ZD(I1,IG1)*ZD(I3,IG1)
     .  *(ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2))
      DO 18 IB=1,2
        Z0=ZKO(IB,I2)
        DO 18 IAD=1,2
          ZB(IAD,IB)=ZB(IAD,IB)+Z0
     .      *(Z12*ZM5(IAD)+(Z11+Z13+Z14+Z15)*ZM4(IAD))
   18 CONTINUE
*
      END
*
      SUBROUTINE B2H2(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2)
     .         ,ZM5(2),ZM6(2),ZM7(2),ZM8(2)
      SAVE /DOTPRO/,/B2PROP/
*
      ZC1=ZD(I2,I3)*ZUD(I2,I4)+ZD(IG2,I3)*ZUD(IG2,I4)
      ZC2=ZD(I1,I3)*ZUD(I1,I4)+ZD(IG1,I3)*ZUD(IG1,I4)
      ZC3=ZD(I4,I3)*ZUD(I4,IG2)+ZD(IG1,I3)*ZUD(IG1,IG2)
      ZC4=ZD(I3,IG1)*ZUD(I3,I4)+ZD(IG2,IG1)*ZUD(IG2,I4)
      ZC5=ZD(I3,IG1)*ZUD(I3,IG2)+ZD(I4,IG1)*ZUD(I4,IG2)
      Z0=1.0/(ZUD(I4,IG1)*ZD(I3,IG2)*P34)
      Z11=-Z0*ZC1/(ZUD(I1,IG1)*ZD(I2,IG2)*P2346)
      Z12=Z0*ZC1*ZC1/(ZD(I2,IG2)*P2346*P23456)
      Z13=-Z0*ZUD(I2,I4)*ZUD(I4,IG2)*ZD(I3,I4)
     .   /(ZUD(I1,IG1)*P2346*P346)
      Z14=Z0*ZC1*ZUD(I2,I4)*ZUD(I4,IG2)*ZD(I3,I4)/(P2346*P23456*P346)
      Z15=Z0*ZC1*ZUD(I3,I4)*ZD(I3,IG1)/(ZD(I2,IG2)*P23456*P345)
      Z16=-Z0*ZC5*ZUD(I2,I4)*ZUD(I4,IG2)*ZD(I3,IG2)
     .   /(P23456*P346*P3456)
      Z17=-Z0*ZC3*ZUD(I2,I4)*ZUD(I3,I4)*ZD(I3,IG1)/(P23456*P345*P3456)
      Z0=1.0/(ZD(I3,IG2)*ZUD(I4,IG1)*P34)
      Z21=Z0*ZC2/(ZD(I2,IG2)*ZUD(I1,IG1)*P1345)
      Z22=-Z0*ZC2*ZC2/(ZUD(I1,IG1)*P1345*P13456)
      Z23=-Z0*ZD(I1,I3)*ZD(I3,IG1)*ZUD(I3,I4)/(ZD(I2,IG2)*P1345*P345)
      Z24=Z0*ZC2*ZD(I1,I3)*ZD(I3,IG1)*ZUD(I3,I4)/(P1345*P13456*P345)
      Z25=Z0*ZC2*ZD(I3,I4)*ZUD(I4,IG2)/(ZUD(I1,IG1)*P13456*P346)
      Z26=Z0*ZC5*ZD(I1,I3)*ZD(I3,IG1)*ZUD(I4,IG1)
     .   /(P13456*P345*P3456)
      Z27=-Z0*ZC4*ZD(I1,I3)*ZD(I3,I4)*ZUD(I4,IG2)/(P13456*P346*P3456)
      Z0=1.0/P34
      Z31=Z0*ZUD(I4,IG2)*ZUD(I2,IG2)
     .   /(ZUD(I1,IG1)*ZUD(IG1,I4)*P2346*P346)
      Z32=-Z0*ZC1*ZUD(I4,IG2)*ZUD(I2,IG2)
     .   /(ZUD(IG1,I4)*P2346*P23456*P346)
      Z33=Z0*ZC1*ZD(I3,IG1)/(ZD(I3,IG2)*ZD(IG2,I2)*P23456*P345)
      Z1=-Z0*ZD(I3,IG1)/(ZUD(IG1,I4)*ZD(IG2,I3)*P345*P3456*P23456)
      Z34=Z1*ZC3*ZUD(IG1,I4)*ZUD(I2,I4)
      Z35=Z1*ZD(IG2,I3)*ZUD(I3,I4)*ZUD(I4,IG2)*ZUD(I2,IG2)
      Z36=Z1*ZD(IG2,I3)*ZUD(IG1,I4)*ZUD(I4,IG2)*ZUD(I2,IG2)
      Z1=-Z0*ZUD(I4,IG2)/(ZUD(IG1,I4)*ZD(IG2,I3)*P346*P3456*P23456)
      Z37=Z1*ZC4*ZD(IG2,I3)*ZUD(I2,IG2)
      Z38=Z1*ZUD(IG1,I4)*ZD(I4,I3)*ZD(I3,IG1)*ZUD(I2,I4)
      Z39=Z1*ZUD(IG1,I4)*ZD(IG2,I3)*ZD(I3,IG1)*ZUD(I2,IG2)
      Z41=-Z0*ZC2*ZUD(I4,IG2)/(ZUD(I1,IG1)*ZUD(IG1,I4)*P13456*P346)
      Z42=-Z0*ZD(I1,IG1)*ZD(I3,IG1)
     .   /(ZD(I3,IG2)*ZD(IG2,I2)*P1345*P345)
      Z43=Z0*ZC2*ZD(I1,IG1)*ZD(I3,IG1)/(ZD(IG2,I3)*P1345*P13456*P345)
      Z1=Z0*ZD(I3,IG1)/(ZD(IG2,I3)*ZUD(IG1,I4)*P345*P13456*P3456)
      Z44=Z1*ZC3*ZUD(IG1,I4)*ZD(I1,IG1)
      Z45=Z1*ZD(IG2,I3)*ZUD(I4,IG2)*ZD(I1,I3)*ZUD(I3,I4)
      Z46=Z1*ZD(IG2,I3)*ZUD(IG1,I4)*ZUD(I4,IG2)*ZD(I1,IG1)
      Z1=Z0*ZUD(I4,IG2)/(ZD(IG2,I3)*ZUD(IG1,I4)*P346*P13456*P3456)
      Z47=Z1*ZC4*ZD(IG2,I3)*ZD(I1,I3)
      Z48=Z1*ZUD(I4,IG1)*ZD(I3,I4)*ZD(I3,IG1)*ZD(I1,IG1)
      Z49=Z1*ZUD(IG1,I4)*ZD(IG2,I3)*ZD(I3,IG1)*ZD(I1,IG1)
      ZC1=Z11+Z13+Z31
      ZC2=Z12+Z14+Z32+Z33+Z34+Z36+Z38+Z39
      ZC3=Z15+Z16+Z17+Z35+Z37
      ZC4=Z21+Z23+Z42
      ZC5=Z22+Z24+Z41+Z43+Z45+Z46+Z47+Z49
      ZC6=Z25+Z26+Z27+Z44+Z48
      DO 21 IB=1,2
        ZM8(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(IG2,I3)*ZKO(IB,IG2)
        ZM1(IB)=ZM8(IB)+ZD(I4,I3)*ZKO(IB,I4)
        ZM3(IB)=ZM1(IB)+ZD(IG1,I3)*ZKO(IB,IG1)
        ZM2(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
   21 CONTINUE
      DO 22 IAD=1,2
        ZM4(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(IG1,I4)*ZKOD(IAD,IG1)
        ZM5(IAD)=ZM4(IAD)+ZUD(I3,I4)*ZKOD(IAD,I3)
        ZM7(IAD)=ZM5(IAD)+ZUD(IG2,I4)*ZKOD(IAD,IG2)
        ZM6(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
   22 CONTINUE
      DO 23 IAD=1,2
      Z0=ZKOD(IAD,I1)
        DO 23 IB=1,2
          ZB(IAD,IB)=ZC1*ZM4(IAD)*ZM1(IB)+ZC4*ZM5(IAD)*ZM8(IB)
     .     +Z0*(ZC2*ZM2(IB)+ZC3*ZM3(IB))
     .     +ZKO(IB,I2)*(ZC5*ZM6(IAD)+ZC6*ZM7(IAD))
   23 CONTINUE
*
      END
*
      SUBROUTINE B2H3(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2)
     .         ,ZM5(2),ZM6(2),ZM7(2),ZM8(2)
      SAVE /DOTPRO/,/B2PROP/
*
      ZC1=ZD(I4,I1)*ZUD(I4,I2)+ZD(IG1,I1)*ZUD(IG1,I2)
      ZC7=ZD(I3,I1)*ZUD(I3,I2)+ZD(IG2,I1)*ZUD(IG2,I2)
      ZC2=ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2)
      ZC3=ZC2+ZD(IG1,IG2)*ZUD(IG1,I2)
      ZC4=ZD(I3,I1)*ZUD(I3,IG1)+ZD(I4,I1)*ZUD(I4,IG1)
      ZC5=ZC4+ZD(IG2,I1)*ZUD(IG2,IG1)
      ZC6=ZD(I3,IG2)*ZUD(I3,IG1)+ZD(I4,IG2)*ZUD(I4,IG1)
      Z0=1.0/(ZUD(IG2,I2)*ZD(I1,IG1)*P23456)
      Z11=-Z0*ZC1/(ZUD(I3,IG2)*ZD(IG1,I4)*P3456)
      Z12=-Z0*ZUD(I2,I4)
     .   *(ZUD(I2,I3)*(ZD(I2,I3)*ZUD(I2,IG1)+ZD(I4,I3)*ZUD(I4,IG1)
     .                +ZD(IG2,I3)*ZUD(IG2,IG1))
     .   +ZUD(I2,IG2)*(ZD(I2,IG2)*ZUD(I2,IG1)+ZD(I3,IG2)*ZUD(I3,IG1)
     .                +ZD(I4,IG2)*ZUD(I4,IG1)))
     .   /(ZUD(I3,IG2)*P2346*P346)
      Z13=Z0*ZC2*ZUD(I2,I4)
     .   *(ZD(I2,I3)*ZUD(I2,IG1)+ZD(I4,I3)*ZUD(I4,IG1)
     .   +ZD(IG2,I3)*ZUD(IG2,IG1))
     .   /(P34*P346*P2346)
      Z14=-Z0*ZC5*ZUD(I2,I4)/(ZUD(I3,IG2)*P346*P3456)
      Z15=Z0*ZC1*ZC3/(ZD(IG1,I4)*P345*P3456)
      Z16=Z0*ZC4*ZC3*ZUD(I2,I4)/(P34*P345*P3456)
      Z17=Z0*ZC6*(ZC7+ZD(I4,I1)*ZUD(I4,I2))*ZUD(I2,I4)
     .   /(P34*P346*P3456)
      Z0=1.0/(ZUD(I2,IG2)*ZD(IG1,I1)*P13456)
      Z21=Z0*ZC7/(ZD(I4,IG1)*ZUD(IG2,I3)*P3456)
      Z22=Z0*ZD(I1,I3)
     .   *(ZD(I1,I4)*(ZD(I1,IG2)*ZUD(I1,I4)+ZD(I3,IG2)*ZUD(I3,I4)
     .               +ZD(IG1,IG2)*ZUD(IG1,I4))
     .   +ZD(I1,IG1)*(ZD(I1,IG2)*ZUD(I1,IG1)+ZD(I3,IG2)*ZUD(I3,IG1)
     .               +ZD(I4,IG2)*ZUD(I4,IG1)))
     .   /(ZD(I4,IG1)*P1345*P345)
      Z23=-Z0*ZC4*ZD(I1,I3)
     .   *(ZD(I1,IG2)*ZUD(I1,I4)+ZD(I3,IG2)*ZUD(I3,I4)
     .    +ZD(IG1,IG2)*ZUD(IG1,I4))/(P34*P345*P1345)
      Z24=Z0*ZC3*ZD(I1,I3)/(ZD(I4,IG1)*P345*P3456)
      Z25=-Z0*ZC7*ZC5/(ZUD(IG2,I3)*P346*P3456)
      Z26=-Z0*ZC2*ZC5*ZD(I1,I3)/(P34*P346*P3456)
      Z27=-Z0*ZC6*(ZC1+ZD(I3,I1)*ZUD(I3,I2))*ZD(I1,I3)
     .   /(P34*P345*P3456)
      Z0=1.0/P23456
      Z31=-Z0*ZUD(I2,I4)*ZD(I3,IG2)*(ZC6+ZD(I2,IG2)*ZUD(I2,IG1))
     .   /(ZD(IG1,I1)*P34*P346*P2346)
      Z32=Z0*ZUD(I4,IG1)*ZUD(I2,IG1)
     .   /(ZUD(I3,IG2)*ZUD(IG2,I2)*P346*P3456)
      Z33=Z0*ZC1*ZD(I3,IG2)/(ZD(I1,IG1)*ZD(IG1,I4)*P345*P3456)
      Z1=-Z0/(ZD(IG1,I1)*ZUD(IG2,I2)*P34*P346*P3456)
      Z34=Z1*ZC5*ZUD(IG2,I2)*ZUD(I2,I4)*ZD(I3,IG2)
      Z35=Z1*ZC2*ZD(IG1,I1)*ZUD(I2,IG1)*ZUD(I4,IG1)
      Z36=Z1*ZD(IG1,I1)*ZUD(IG2,I2)
     .   *ZUD(I4,IG1)*ZD(I3,IG2)*ZUD(I2,IG1)
      Z1=-Z0/(ZD(IG1,I1)*ZUD(IG2,I2)*P34*P345*P3456)
      Z37=Z1*ZC3*ZD(IG1,I1)*ZUD(I4,IG1)*ZUD(I2,IG1)
      Z38=Z1*ZC4*ZUD(IG2,I2)*ZD(I3,IG2)*ZUD(I2,I4)
      Z39=Z1*ZUD(IG2,I2)*ZD(IG1,I1)
     .   *ZD(I3,IG2)*ZUD(I4,IG1)*ZUD(I2,IG1)
      Z0=1.0/P13456
      Z41=Z0*ZD(I1,I3)*ZUD(I4,IG1)*(ZC6+ZD(I1,IG2)*ZUD(I1,IG1))
     .   /(ZUD(IG2,I2)*P34*P345*P1345)
      Z42=-Z0*ZC7*ZUD(I4,IG1)/(ZUD(I3,IG2)*ZUD(IG2,I2)*P346*P3456)
      Z43=-Z0*ZD(I3,IG2)*ZD(I1,IG2)
     .   /(ZD(I1,IG1)*ZD(IG1,I4)*P345*P3456)
      Z1=Z0/(ZD(IG1,I1)*ZUD(IG2,I2)*P34*P346*P3456)
      Z44=Z1*ZC5*ZUD(IG2,I2)*ZD(I3,IG2)*ZD(I1,IG2)
      Z45=Z1*ZC2*ZD(IG1,I1)*ZUD(I4,IG1)*ZD(I1,I3)
      Z46=Z1*ZD(IG1,I1)*ZUD(IG2,I2)
     .   *ZD(I3,IG2)*ZUD(I4,IG1)*ZD(I1,IG2)
      Z1=Z0/(ZD(IG1,I1)*ZUD(IG2,I2)*P34*P345*P3456)
      Z47=Z1*ZC3*ZD(IG1,I1)*ZUD(I4,IG1)*ZD(I1,I3)
      Z48=Z1*ZC4*ZUD(IG2,I2)*ZD(I3,IG2)*ZD(I1,IG2)
      Z49=Z1*ZUD(IG2,I2)*ZD(IG1,I1)
     .   *ZD(I3,IG2)*ZUD(I4,IG1)*ZD(I1,IG2)
      ZC1=Z11+Z14+Z32
      ZC2=Z12+Z13+Z31
      ZC3=Z15+Z16+Z17+Z35+Z37
      ZC4=Z33+Z34+Z36+Z38+Z39
      ZC5=Z21+Z24+Z43
      ZC6=Z22+Z23+Z41
      ZC7=Z25+Z26+Z27+Z44+Z48
      ZC8=Z42+Z45+Z46+Z47+Z49
      DO 31 IB=1,2
        ZM1(IB)=
     .     ZUD(I2,I3)*(ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .                +ZD(IG1,I3)*ZKO(IB,IG1)+ZD(IG2,I3)*ZKO(IB,IG2))
     .    +ZUD(I2,IG2)*(ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .                +ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1))
        ZM2(IB)=ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .           +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG1,I1)*ZKO(IB,IG1)
     .           +ZD(IG2,I1)*ZKO(IB,IG2)
        ZM3(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .           +ZD(IG1,I3)*ZKO(IB,IG1)+ZD(IG2,I3)*ZKO(IB,IG2)
        ZM4(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .           +ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1)
   31 CONTINUE
      DO 32 IAD=1,2
        ZM5(IAD)=
     .     ZD(I1,I4)*(ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .          +ZUD(IG1,I4)*ZKOD(IAD,IG1)+ZUD(IG2,I4)*ZKOD(IAD,IG2))
     .    +ZD(I1,IG1)*(ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG1)*ZKOD(IAD,I4)+ZUD(IG2,IG1)*ZKOD(IAD,IG2))
        ZM6(IAD)=ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(I3,I2)*ZKOD(IAD,I3)
     .            +ZUD(I4,I2)*ZKOD(IAD,I4)+ZUD(IG1,I2)*ZKOD(IAD,IG1)
     .            +ZUD(IG2,I2)*ZKOD(IAD,IG2)
        ZM7(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .            +ZUD(IG1,I4)*ZKOD(IAD,IG1)+ZUD(IG2,I4)*ZKOD(IAD,IG2)
        ZM8(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .            +ZUD(I4,IG1)*ZKOD(IAD,I4)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
   32 CONTINUE
      DO 33 IAD=1,2
        Z0=ZKOD(IAD,I1)
        DO 33 IB=1,2
          ZB(IAD,IB)=Z0*(ZC1*ZM1(IB)+ZC2*ZM2(IB)
     .                  +ZC3*ZM3(IB)+ZC4*ZM4(IB))
     .     +ZKO(IB,I2)*(ZC5*ZM5(IAD)+ZC6*ZM6(IAD)
     .                 +ZC7*ZM7(IAD)+ZC8*ZM8(IAD))
   33 CONTINUE
*
      END
*
      SUBROUTINE B2H4(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2)
     .         ,ZM5(2),ZM6(2),ZM7(2),ZM8(2),ZM9(2),ZM10(2)
      SAVE /DOTPRO/,/B2PROP/
*
      ZC8=ZD(I2,I3)*ZUD(I2,IG1)+ZD(IG2,I3)*ZUD(IG2,IG1)
      ZC1=ZC8+ZD(I4,I3)*ZUD(I4,IG1)
      ZC2=ZD(I2,I3)*ZUD(I2,I4)+ZD(IG2,I3)*ZUD(IG2,I4)
      ZC3=ZD(I3,I1)*ZUD(I3,IG1)+ZD(I4,I1)*ZUD(I4,IG1)
      ZC4=ZC3+ZD(IG2,I1)*ZUD(IG2,IG1)
      ZC7=ZD(I4,I1)*ZUD(I4,IG2)+ZD(IG1,I1)*ZUD(IG1,IG2)
      ZC5=ZC7+ZD(I3,I1)*ZUD(I3,IG2)
      ZC6=ZD(I4,I3)*ZUD(I4,IG2)+ZD(IG1,I3)*ZUD(IG1,IG2)
      Z0=1.0/(ZD(I1,IG1)*ZD(I3,IG2)*P23456)
      Z11=Z0*ZC1*ZC2/(ZD(I2,IG2)*P2346*P34)
      Z12=Z0*(ZD(I4,I1)*ZC2+ZD(IG1,I1)*ZC8)
     .    /(ZD(I2,IG2)*ZD(I4,IG1)*P345)
      Z13=Z0*ZC1*ZUD(I2,I4)*ZUD(I4,IG2)*ZD(I3,I4)
     .    /(P34*P346*P2346)
      Z14=Z0*ZC2*ZC3/(ZD(I2,IG2)*P34*P345)
      Z15=Z0*ZC6*(ZD(I4,I1)*ZUD(I4,I2)+ZD(IG1,I1)*ZUD(IG1,I2))
     .    /(ZD(I4,IG1)*P345*P3456)
      Z16=Z0*ZC4*ZUD(I2,I4)*ZUD(I4,IG2)*ZD(I3,I4)
     .    /(P34*P346*P3456)
      Z17=Z0*ZC5*ZUD(I2,I4)*ZUD(I4,IG1)*ZD(I3,I4)
     .    /(P34*P345*P3456)
      Z0=ZD(I1,I3)/(ZD(I1,IG1)*ZD(I3,IG2))
      Z21=Z0/(ZD(I2,IG2)*ZD(I4,IG1)*P345*P1345)
      Z22=-Z0*(ZD(I1,I4)*(ZD(I1,I3)*ZUD(I1,I4)+ZD(IG1,I3)*ZUD(IG1,I4))
     .    +ZD(I1,IG1)*(ZD(I1,I3)*ZUD(I1,IG1)+ZD(I4,I3)*ZUD(I4,IG1)))
     .    /(ZD(I4,IG1)*P1345*P13456*P345)
      Z23=-Z0*ZC6/(ZD(I4,IG1)*P13456*P345*P3456)
      Z24=-Z0*ZC3/(ZD(I2,IG2)*P34*P1345*P345)
      Z25=Z0*ZC3*(ZD(I1,I3)*ZUD(I1,I4)+ZD(IG1,I3)*ZUD(IG1,I4))
     .    /(P34*P1345*P13456*P345)
      Z26=Z0*ZC4*ZD(I4,I3)*ZUD(I4,IG2)/(P34*P13456*P346*P3456)
      Z27=Z0*ZC5*ZD(I4,I3)*ZUD(I4,IG1)/(P34*P13456*P345*P3456)
      Z0=1.0/P23456
      Z31=-Z0*ZC1*ZUD(I2,IG2)*ZUD(I4,IG2)/(ZD(IG1,I1)*P2346*P34*P346)
      Z32=Z0*ZC8*ZUD(I4,IG1)/(ZD(I3,IG2)*ZD(IG2,I2)*P34*P345)
      Z33=-Z0*ZC7*ZUD(I2,IG2)/(ZD(I1,IG1)*ZD(IG1,I4)*P345*P3456)
      Z34=-Z0*(ZC4*ZD(IG2,I3)*ZUD(I4,IG2)*ZUD(I2,IG2)
     .    +ZD(IG1,I1)*ZD(I3,I4)*ZUD(IG2,I4)*ZUD(I4,IG1)*ZUD(I2,IG1)
     .    +ZD(IG1,I1)*ZD(IG2,I3)*ZUD(IG2,IG1)*ZUD(I4,IG2)*ZUD(I2,IG1))
     .    /(ZD(IG1,I1)*ZD(IG2,I3)*P34*P346*P3456)
      Z35=-Z0*(ZC6*ZUD(I4,IG1)*ZUD(I2,IG1)*ZD(IG1,I1)
     .          +ZC3*ZUD(I4,IG2)*ZUD(I2,IG2)*ZD(IG2,I3)
     .    +ZD(IG2,I3)*ZD(IG1,I1)*ZUD(IG1,IG2)*ZUD(I4,IG1)*ZUD(I2,IG2))
     .    /(ZD(IG1,I1)*ZD(IG2,I3)*P34*P345*P3456)
      Z0=ZD(I1,I3)
      Z41=-Z0*ZUD(I4,IG1)/(ZD(I3,IG2)*ZD(IG2,I2)*P1345*P34*P345)
      Z42=Z0*ZUD(I4,IG1)*(ZD(I1,I3)*ZUD(I1,IG1)+ZD(I4,I3)*ZUD(I4,IG1))
     .    /(ZD(IG2,I3)*P1345*P13456*P34*P345)
      Z43=Z0*ZC7/(ZD(I1,IG1)*ZD(IG1,I4)*P13456*P345*P3456)
      Z1=Z0/(ZD(IG1,I1)*ZD(IG2,I3)*P34*P13456*P346*P3456)
      Z44=Z1*ZC4*ZD(IG2,I3)*ZUD(I4,IG2)
      Z45=Z1*ZD(IG1,I1)*ZUD(I4,IG1)*ZD(I3,I4)*ZUD(IG2,I4)
      Z46=Z1*ZD(IG1,I1)*ZD(IG2,I3)*ZUD(IG2,IG1)*ZUD(I4,IG2)
      Z1=Z0/(ZD(IG1,I1)*ZD(IG2,I3)*P34*P13456*P345*P3456)
      Z47=Z1*ZC6*ZD(IG1,I1)*ZUD(I4,IG1)
      Z48=Z1*ZC3*ZUD(I4,IG2)*ZD(IG2,I3)
      Z49=Z1*ZD(IG2,I3)*ZD(IG1,I1)*ZUD(IG1,IG2)*ZUD(I4,IG1)
      ZC1=Z11+Z13+Z31
      ZC2=Z12+Z14+Z15+Z16+Z17+Z32+Z33+Z34+Z35
      ZC3=Z22+Z25+Z42+Z43+Z44+Z48+Z49
      ZC4=Z26+Z27
      ZC5=Z45+Z46+Z47
      DO 41 IB=1,2
        ZM1(IB)=ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .         +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG1,I1)*ZKO(IB,IG1)
     .         +ZD(IG2,I1)*ZKO(IB,IG2)
        ZM8(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(IG2,I3)*ZKO(IB,IG2)
        ZM2(IB)=ZM8(IB)+ZD(I4,I3)*ZKO(IB,I4)+ZD(IG1,I3)*ZKO(IB,IG1)
   41 CONTINUE
      DO 42 IAD=1,2
        ZM3(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM4(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .          +ZUD(IG1,I4)*ZKOD(IAD,IG1)
        ZM5(IAD)=ZM4(IAD)+ZUD(IG2,I4)*ZKOD(IAD,IG2)
        ZM9(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG1)*ZKOD(IAD,I4)
        ZM6(IAD)=ZD(I1,I4)*ZM4(IAD)+ZD(I1,IG1)*ZM9(IAD)
        ZM7(IAD)=ZM6(IAD)+ZKOD(IAD,IG2)
     .          *(ZD(I4,I1)*ZUD(I4,IG2)+ZD(IG1,I1)*ZUD(IG1,IG2))
        ZM10(IAD)=ZM9(IAD)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
   42 CONTINUE
      DO 43 IB=1,2
        Z0=ZKO(IB,I2)
        Z1=ZM8(IB)
        DO 43 IAD=1,2
          ZB(IAD,IB)=ZKOD(IAD,I1)*(ZC1*ZM1(IB)+ZC2*ZM2(IB))
     .      +Z1*(Z21*ZM6(IAD)+Z24*ZM4(IAD)+Z41*ZM9(IAD))
     .      +Z0*(Z23*ZM7(IAD)+ZC3*ZM3(IAD)+ZC4*ZM5(IAD)+ZC5*ZM10(IAD))
   43 CONTINUE
*
      END
*
      SUBROUTINE B2H5(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2)
     .         ,ZM5(2),ZM6(2),ZM7(2),ZM8(2)
      SAVE /DOTPRO/,/B2PROP/
*
      ZC1=2.0*(RR(I4,I2)+RR(I4,I3)+RR(I4,IG2))
      ZC2=ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2)
      ZC3=ZC2+ZD(IG1,IG2)*ZUD(IG1,I2)
      ZC4=ZD(I3,IG1)*ZUD(I3,I4)+ZD(IG2,IG1)*ZUD(IG2,I4)
      ZC5=ZD(I3,IG1)*ZUD(I3,I2)+ZD(I4,IG1)*ZUD(I4,I2)
     .     +ZD(IG2,IG1)*ZUD(IG2,I2)
      ZC6=ZD(I1,IG2)*ZUD(I1,I3)+ZD(I4,IG2)*ZUD(I4,I3)
     .     +ZD(IG1,IG2)*ZUD(IG1,I3)
      ZC7=ZD(I2,IG2)*ZUD(I2,I4)+ZD(I3,IG2)*ZUD(I3,I4)
      ZC8=ZD(I1,IG2)*ZUD(I1,I4)+ZD(IG1,IG2)*ZUD(IG1,I4)
      ZC9=2.0*(RR(I4,I1)+RR(I4,IG1))
      Z0=ZUD(I2,I3)/(ZUD(IG2,I2)*ZUD(IG1,I4))
      Z11=-Z0*ZUD(I2,I3)/(ZUD(I3,IG2)*P23456*P3456)
      Z12=-Z0*ZUD(I2,I3)/(ZUD(I3,IG2)*ZUD(I1,IG1)*P2346*P346)
      Z13=Z0*ZC1*ZUD(I2,I3)
     .    /(ZUD(I3,IG2)*P2346*P23456*P346)
      Z14=Z0*ZC2/(ZUD(I1,IG1)*P2346*P34*P346)
      Z15=-Z0*ZC2*ZC1/(P2346*P23456*P34*P346)
      Z16=Z0*ZC4*ZUD(I2,I3)/(ZUD(I3,IG2)*P23456*P346*P3456)
      Z17=Z0*ZC3/(P23456*P345*P3456)
      Z18=-Z0*ZC5*ZUD(I4,I3)*ZD(IG2,I3)/(P23456*P346*P34*P3456)
      Z19=-Z0*ZC3*ZUD(I4,I3)*ZD(IG1,I3)/(P23456*P345*P34*P3456)
      Z0=1.0/(ZUD(IG2,I2)*ZUD(IG1,I4)*P13456)
      Z21=Z0*ZC6*ZC9/(ZUD(IG1,I1)*P1345*P34)
      Z22=-Z0*ZUD(I2,I3)*ZD(I1,IG1)/(ZUD(IG2,I3)*P3456)
      Z23=-Z0*ZC6*ZD(I1,IG1)/(P1345*P345)
      Z24=Z0*ZC9*ZUD(I2,I3)/(ZUD(IG1,I1)*ZUD(IG2,I3)*P346)
      Z25=Z0*ZC6*ZD(I1,I4)*ZUD(I4,I3)*ZD(IG1,I3)
     .    /(P1345*P34*P345)
      Z26=Z0*ZC9*ZC2/(ZUD(IG1,I1)*P34*P346)
      Z27=-Z0*ZC3*ZD(I1,IG1)/(P345*P3456)
      Z28=Z0*ZC4*ZD(I1,I4)*ZUD(I2,I3)/(ZUD(IG2,I3)*P346*P3456)
      Z29=Z0*ZC5*ZD(I1,I4)*ZUD(I4,I3)*ZD(IG2,I3)/(P34*P346*P3456)
      Z210=Z0*ZC3*ZD(I1,I4)*ZUD(I4,I3)*ZD(IG1,I3)/(P34*P345*P3456)
      Z0=ZUD(I2,I3)
      Z31=Z0*ZD(I4,IG2)/(ZUD(I1,IG1)*ZUD(IG1,I4)*P34*P346*P2346)
      Z32=-Z0*ZC7*ZD(I4,IG2)/(ZUD(IG1,I4)*P2346*P23456*P34*P346)
      Z33=Z0*ZD(I4,IG1)*ZUD(I2,I3)
     .    /(ZUD(I3,IG2)*ZUD(IG2,I2)*P23456*P346*P3456)
      Z34=Z0*ZD(IG1,IG2)/(ZUD(IG1,I4)*P23456*P345*P3456)
      Z1=-Z0/(ZUD(IG1,I4)*ZUD(IG2,I2)*P23456*P34*P3456*P346)
      Z35=Z1*ZC4*ZUD(IG2,I2)*ZD(I4,IG2)
      Z36=-Z1*ZC2*2.0*RR(I4,IG1)
      Z37=Z1*ZUD(IG1,I4)*ZUD(IG2,I2)*ZD(IG2,IG1)*ZD(I4,IG2)
      Z1=-Z0/(ZUD(IG1,I4)*ZUD(IG2,I2)*P23456*P34*P3456*P345)
      Z38=-Z1*ZC3*2.0*RR(I4,IG1)
      Z39=Z1*ZD(I4,IG2)*ZUD(IG2,I2)*ZD(IG1,I3)*ZUD(I4,I3)
      Z310=-Z1*ZUD(IG2,I2)*ZD(IG1,IG2)*2.0*RR(I4,IG1)
      Z0=1.0/P13456
      Z41=-Z0*ZC8*ZD(I4,IG2)/(ZUD(I1,IG1)*ZUD(IG1,I4)*P34*P346)
      Z42=Z0*ZC6*ZD(I1,IG1)*ZD(I4,IG1)/(ZUD(IG2,I2)*P1345*P34*P345)
      Z43=-Z0*ZD(I4,IG1)*ZD(I1,IG1)*ZUD(I2,I3)
     .    /(ZUD(I3,IG2)*ZUD(IG2,I2)*P346*P3456)
      Z44=-Z0*ZD(IG1,IG2)*ZD(I1,IG2)/(ZUD(IG1,I4)*P345*P3456)
      Z45=Z0/(ZUD(IG1,I4)*ZUD(IG2,I2)*P34*P3456*P346)
     .    *(ZC4*ZUD(IG2,I2)*ZD(I4,IG2)*ZD(I1,IG2)
     .     -ZC2*ZD(I1,IG1)*2.0*RR(I4,IG1)
     .     +ZUD(IG1,I4)*ZUD(IG2,I2)*ZD(IG2,IG1)*ZD(I4,IG2)*ZD(I1,IG1))
      Z46=Z0/(ZUD(IG1,I4)*ZUD(IG2,I2)*P34*P3456*P345)
     .    *(-ZC3*ZD(I1,IG1)*2.0*RR(I4,IG1)
     .     +ZUD(IG2,I2)*ZD(I3,IG1)*ZUD(I3,I4)*ZD(I4,IG2)*ZD(I1,IG2)
     .     -ZD(IG1,IG2)*ZUD(IG2,I2)*ZD(I1,IG2)*2.0*RR(I4,IG1))
      ZC1=Z21+Z23+Z25+Z42
      ZC2=Z22+Z24+Z26+Z27+Z28+Z29+Z210+Z41+Z43+Z44+Z45+Z46
      ZC3=Z11+Z13+Z15+Z17+Z32+Z33+Z36+Z37+Z38
      ZC4=Z16+Z18+Z19
      ZC5=Z34+Z35+Z39+Z310
      ZC6=Z12+Z14
      DO 51 IB=1,2
        ZM1(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
        ZM2(IB)=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
     .         +ZD(IG2,I4)*ZKO(IB,IG2)
        ZM3(IB)=ZM2(IB)+ZD(IG1,I4)*ZKO(IB,IG1)
        ZM4(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .         +ZD(I4,IG2)*ZKO(IB,I4)
        ZM5(IB)=ZM4(IB)+ZD(IG1,IG2)*ZKO(IB,IG1)
   51 CONTINUE
      DO 52 IAD=1,2
        ZM6(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(IG1,I4)*ZKOD(IAD,IG1)
        ZM7(IAD)=ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(I3,I2)*ZKOD(IAD,I3)
     .          +ZUD(I4,I2)*ZKOD(IAD,I4)+ZUD(IG1,I2)*ZKOD(IAD,IG1)
     .          +ZUD(IG2,I2)*ZKOD(IAD,IG2)
        ZM8(IAD)=ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(I4,I3)*ZKOD(IAD,I4)
     .          +ZUD(IG1,I3)*ZKOD(IAD,IG1)+ZUD(IG2,I3)*ZKOD(IAD,IG2)
   52 CONTINUE
      DO 53 IAD=1,2
        Z0=ZKOD(IAD,I1)
        Z1=ZM6(IAD)
        DO 53 IB=1,2
          ZB(IAD,IB)=ZKO(IB,I2)*(ZC1*ZM7(IAD)+ZC2*ZM8(IAD))
     .              +Z0*(ZC3*ZM1(IB)+ZC4*ZM3(IB)+ZC5*ZM5(IB))
     .              +Z1*(ZC6*ZM2(IB)+Z31*ZM4(IB))
   53 CONTINUE
*
      END
*
      SUBROUTINE B2H6(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2)
     .         ,ZM5(2),ZM6(2),ZM7(2),ZM8(2)
      SAVE /DOTPRO/,/B2PROP/
*
      ZC1=2.0*(RR(I3,I2)+RR(I3,IG2))
      ZC2=ZD(I3,IG1)*ZUD(I3,I4)+ZD(IG2,IG1)*ZUD(IG2,I4)
      ZC3=ZD(I4,I3)*ZUD(I4,IG2)+ZD(IG1,I3)*ZUD(IG1,IG2)
      ZC4=ZD(I3,IG1)*ZUD(I3,IG2)+ZD(I4,IG1)*ZUD(I4,IG2)
      ZC5=2.0*(RR(I4,I1)+RR(I4,IG1))
      ZC6=2.0*(RR(I4,I2)+RR(I4,I3)+RR(I4,IG2))
      ZC7=2.0*(RR(I3,I1)+RR(I3,I4)+RR(I3,IG1))
      Z0=1.0/(ZUD(IG1,I4)*ZD(IG2,I3))
      Z11=Z0*ZC1/(ZUD(I1,IG1)*ZD(IG2,I2)*P2346*P34)
      Z12=-Z0*ZC1*ZC6/(ZD(IG2,I2)*P2346*P23456*P34)
      Z13=-Z0*ZUD(I2,IG2)/(P23456*P3456)
      Z14=-Z0*ZUD(I2,IG2)/(ZUD(I1,IG1)*P2346*P346)
      Z15=Z0*ZUD(I2,IG2)*ZC6/(P2346*P23456*P346)
      Z16=Z0*ZC1/(ZD(IG2,I2)*P345*P23456)
      Z17=Z0*ZUD(I2,I3)*ZUD(IG2,I4)*ZD(I3,I4)
     .    /(ZUD(I1,IG1)*P2346*P34*P346)
      Z18=-Z0*ZUD(I2,I3)*ZUD(IG2,I4)*ZD(I3,I4)*ZC6
     .    /(P2346*P23456*P34*P346)
      Z19=-Z0*ZD(I3,IG1)*ZUD(I3,I4)*ZC1/(ZD(IG2,I2)*P23456*P34*P345)
      Z110=Z0*ZUD(I2,IG2)*ZC2/(P23456*P346*P3456)
      Z111=Z0*ZUD(I2,I3)*ZC3/(P23456*P345*P3456)
      Z112=-Z0*ZUD(I2,I3)*ZUD(I4,I3)*ZD(IG1,I3)*ZC3
     .    /(P23456*P34*P3456*P345)
      Z113=-Z0*ZUD(I2,I3)*ZUD(I4,IG2)*ZD(I3,IG2)*ZC4
     .    /(P23456*P34*P3456*P346)
      Z0=1.0/(ZD(IG2,I3)*ZUD(IG1,I4))
      Z21=-Z0*ZC5/(ZD(I2,IG2)*ZUD(IG1,I1)*P1345*P34)
      Z22=Z0*ZC5*ZC7/(ZUD(IG1,I1)*P1345*P13456*P34)
      Z23=Z0*ZD(I1,IG1)/(P13456*P3456)
      Z24=Z0*ZD(I1,IG1)/(ZD(I2,IG2)*P1345*P345)
      Z25=-Z0*ZD(I1,IG1)*ZC7/(P1345*P13456*P345)
      Z26=-Z0*ZC5/(ZUD(IG1,I1)*P346*P13456)
      Z27=-Z0*ZD(I1,I4)*ZD(IG1,I3)*ZUD(I4,I3)
     .    /(ZD(I2,IG2)*P1345*P34*P345)
      Z28=Z0*ZD(I1,I4)*ZD(IG1,I3)*ZUD(I4,I3)*ZC7
     .    /(P1345*P13456*P34*P345)
      Z29=Z0*ZUD(I4,IG2)*ZD(I4,I3)*ZC5/(ZUD(IG1,I1)*P13456*P34*P346)
      Z210=-Z0*ZD(I1,IG1)*ZC3/(P13456*P345*P3456)
      Z211=-Z0*ZD(I1,I4)*ZC2/(P13456*P346*P3456)
      Z212=Z0*ZD(I1,I4)*ZD(I3,I4)*ZUD(IG2,I4)*ZC2
     .    /(P13456*P34*P3456*P346)
      Z213=Z0*ZD(I1,I4)*ZD(I3,IG1)*ZUD(I4,IG1)*ZC4
     .    /(P13456*P34*P3456*P345)
      Z31=ZUD(I3,IG2)*ZUD(I2,IG2)
     .    /(ZUD(I1,IG1)*ZUD(IG1,I4)*P34*P346*P2346)
      Z32=-ZUD(I3,IG2)*ZUD(I2,IG2)*ZC6
     .    /(ZUD(IG1,I4)*P34*P346*P2346*P23456)
      Z33=ZD(I4,IG1)*ZC1/(ZD(I3,IG2)*ZD(IG2,I2)*P34*P345*P23456)
      Z34=ZD(I4,IG1)*ZUD(I2,IG2)/(ZD(IG2,I3)*P346*P3456*P23456)
      Z35=ZUD(I3,IG2)*ZUD(I2,IG2)/(ZUD(IG1,I4)*P3456*P345*P23456)
      Z1=-2.0/(ZUD(IG1,I4)*ZD(IG2,I3)*P34*P3456*P23456*P346)
      Z36=-Z1*RR(I3,IG2)*ZUD(I2,IG2)*ZC2
      Z37=-Z1*RR(I4,IG1)*ZD(I3,I4)*ZUD(IG2,I4)*ZUD(I2,I3)
      Z38=Z1*2.0*RR(I3,IG2)*RR(I4,IG1)*ZUD(I2,IG2)
      Z1=-2.0/(ZUD(IG1,I4)*ZD(IG2,I3)*P34*P3456*P23456*P345)
      Z39=-Z1*RR(I4,IG1)*ZUD(I2,I3)*ZC3
      Z310=-Z1*RR(I3,IG2)*ZD(I3,IG1)*ZUD(I3,I4)*ZUD(I2,IG2)
      Z311=Z1*2.0*RR(I3,IG2)*RR(I4,IG1)*ZUD(I2,IG2)
      Z41=-ZUD(I3,IG2)*ZC5/(ZUD(I1,IG1)*ZUD(IG1,I4)*P34*P346*P13456)
      Z42=-ZD(I4,IG1)*ZD(I1,IG1)
     .    /(ZD(I3,IG2)*ZD(IG2,I2)*P34*P345*P1345)
      Z43=-ZD(I4,IG1)*ZD(I1,IG1)*ZC7
     .    /(ZD(I3,IG2)*P34*P345*P1345*P13456)
      Z44=-ZD(I1,IG1)*ZD(I4,IG1)/(ZD(IG2,I3)*P346*P3456*P13456)
      Z45=-ZD(I1,IG1)*ZUD(I3,IG2)/(ZUD(IG1,I4)*P345*P3456*P13456)
      Z1=2.0/(ZUD(IG1,I4)*ZD(IG2,I3)*P34*P346*P3456*P13456)
      Z46=-Z1*RR(I3,IG2)*ZD(I1,I4)*ZC2
      Z47=-Z1*RR(I4,IG1)*ZD(I3,I4)*ZUD(IG2,I4)*ZD(I1,IG1)
      Z48=Z1*2.0*RR(I3,IG2)*RR(I4,IG1)*ZD(I1,IG1)
      Z1=2.0/(ZUD(IG1,I4)*ZD(IG2,I3)*P34*P345*P3456*P13456)
      Z49=-Z1*RR(I4,IG1)*ZD(I1,IG1)*ZC3
      Z410=-Z1*RR(I3,IG2)*ZD(I3,IG1)*ZUD(I3,I4)*ZD(I1,I4)
      Z411=Z1*2.0*RR(I3,IG2)*RR(I4,IG1)*ZD(I1,IG1)
      ZC1=Z12+Z13+Z15+Z16+Z18+Z111
     .     +Z32+Z33+Z34+Z35+Z37+Z38+Z39+Z311
      ZC2=Z19+Z110+Z112+Z113+Z36+Z310
      ZC3=Z22+Z23+Z25+Z26+Z28+Z211
     .     +Z41+Z43+Z44+Z45+Z46+Z48+Z410+Z411
      ZC4=Z29+Z210+Z212+Z213+Z47+Z49
      ZC5=Z11+Z14+Z17+Z31
      ZC6=Z21+Z24+Z27+Z42
      DO 61 IB=1,2
        ZM1(IB)=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
     .         +ZD(IG2,I4)*ZKO(IB,IG2)
        ZM2(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
        ZM3(IB)=ZM1(IB)+ZD(IG1,I4)*ZKO(IB,IG1)
        ZM4(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(IG2,I3)*ZKO(IB,IG2)
   61 CONTINUE
      DO 62 IAD=1,2
        ZM5(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(IG1,I4)*ZKOD(IAD,IG1)
        ZM6(IAD)=ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(I4,I3)*ZKOD(IAD,I4)
     .          +ZUD(IG1,I3)*ZKOD(IAD,IG1)
        ZM7(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM8(IAD)=ZM6(IAD)+ZUD(IG2,I3)*ZKOD(IAD,IG2)
   62 CONTINUE
      DO 63 IAD=1,2
        Z0=ZKOD(IAD,I1)
        Z1=ZM5(IAD)
        Z2=ZM6(IAD)
        DO 63 IB=1,2
          ZB(IAD,IB)=Z0*(ZC1*ZM2(IB)+ZC2*ZM3(IB))
     .      +ZKO(IB,I2)*(ZC3*ZM7(IAD)+ZC4*ZM8(IAD))
     .      +Z1*ZC5*ZM1(IB)+Z2*ZC6*ZM4(IB)
   63 CONTINUE
*
      END
*
      SUBROUTINE B2H7(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),ZM6(2)
      SAVE /DOTPRO/,/B2PROP/
*
      ZC1=ZD(I2,I4)*ZUD(I2,IG1)+ZD(I3,I4)*ZUD(I3,IG1)
     .   +ZD(IG2,I4)*ZUD(IG2,IG1)
      ZC2=ZD(I3,IG2)*ZUD(I3,I2)+ZD(I4,IG2)*ZUD(I4,I2)
      ZC5=ZD(I3,I1)*ZUD(I3,IG1)+ZD(I4,I1)*ZUD(I4,IG1)
      ZC3=ZC5+ZD(IG2,I1)*ZUD(IG2,IG1)
      ZC4=ZC2+ZD(IG1,IG2)*ZUD(IG1,I2)
      Z0=ZD(I3,I1)*ZUD(I3,I2)+ZD(I4,I1)*ZUD(I4,I2)
      ZC6=Z0+ZD(IG2,I1)*ZUD(IG2,I2)
      ZC9=Z0+ZD(IG1,I1)*ZUD(IG1,I2)
      ZC7=ZD(I3,IG2)*ZUD(I3,IG1)+ZD(I4,IG2)*ZUD(I4,IG1)
      ZC8=ZD(I1,IG2)*ZUD(I1,I3)+ZD(I4,IG2)*ZUD(I4,I3)
     .    +ZD(IG1,IG2)*ZUD(IG1,I3)
      ZC10=ZC7+ZD(I2,IG2)*ZUD(I2,IG1)
      ZC11=ZC7+ZD(I1,IG2)*ZUD(I1,IG1)
      Z0=ZUD(I2,I3)/(ZD(IG1,I1)*ZUD(IG2,I2)*P23456)
      Z11=Z0*ZUD(I2,I3)*ZD(I1,I4)/(ZUD(I3,IG2)*ZD(IG1,I4)*P3456)
      Z12=Z0*ZUD(I2,I3)*ZC1/(ZUD(I3,IG2)*P2346*P346)
      Z13=-Z0*ZC2*ZC1/(P34*P346*P2346)
      Z14=Z0*ZUD(I2,I3)*ZC3/(ZUD(I3,IG2)*P346*P3456)
      Z15=-Z0*ZD(I1,I4)*ZC4/(ZD(IG1,I4)*P345*P3456)
      Z16=-Z0*ZC4*ZC5/(P34*P345*P3456)
      Z17=-Z0*ZC6*ZC7/(P34*P346*P3456)
      Z0=ZD(I1,I4)/(ZUD(IG2,I2)*ZD(IG1,I1)*P13456)
      Z21=-Z0*ZD(I1,I4)*ZUD(I2,I3)/(ZD(I4,IG1)*ZUD(IG2,I3)*P3456)
      Z22=-Z0*ZD(I1,I4)*ZC8/(ZD(I4,IG1)*P1345*P345)
      Z23=Z0*ZC5*ZC8/(P34*P345*P1345)
      Z24=-Z0*ZD(I1,I4)*ZC4/(ZD(I4,IG1)*P345*P3456)
      Z25=Z0*ZUD(I2,I3)*ZC3/(ZUD(IG2,I3)*P346*P3456)
      Z26=Z0*ZC3*ZC2/(P34*P346*P3456)
      Z27=Z0*ZC9*ZC7/(P34*P345*P3456)
      Z0=1.0/P23456
      Z31=-Z0*ZUD(I2,I3)*ZD(I4,IG2)*ZC10/(ZD(IG1,I1)*P2346*P34*P346)
      Z32=Z0*ZUD(I2,IG1)*ZUD(I2,I3)*ZUD(I3,IG1)
     .  /(ZUD(I3,IG2)*ZUD(IG2,I2)*P346*P3456)
      Z33=-Z0*ZUD(I2,I3)*ZD(I1,I4)*ZD(I4,IG2)
     .  /(ZD(IG1,I1)*ZD(IG1,I4)*P345*P3456)
      Z1=-Z0/(ZD(IG1,I1)*ZUD(IG2,I2)*P34*P346*P3456)
      Z34=Z1*ZUD(IG2,I2)*ZD(I4,IG2)*ZUD(I2,I3)*ZC3
      Z35=Z1*ZD(IG1,I1)*ZUD(I3,IG1)*ZUD(I2,IG1)*ZC2
      Z36=Z1*ZD(IG1,I1)*ZUD(IG2,I2)*ZD(I4,IG2)*ZUD(I3,IG1)*ZUD(I2,IG1)
      Z1=-Z0/(ZD(IG1,I1)*ZUD(IG2,I2)*P34*P345*P3456)
      Z37=Z1*ZUD(I3,IG1)*ZD(IG1,I1)*ZUD(I2,IG1)*ZC4
      Z38=Z1*ZUD(IG2,I2)*ZD(I4,IG2)*ZUD(I2,I3)*ZC5
      Z39=Z1*ZUD(IG2,I2)*ZD(IG1,I1)*ZD(I4,IG2)*ZUD(I3,IG1)*ZUD(I2,IG1)
      Z0=1.0/P13456
      Z41=Z0*ZD(I1,I4)*ZUD(I3,IG1)*ZC11/(ZUD(IG2,I2)*P1345*P345*P34)
      Z42=-Z0*ZD(I1,I4)*ZUD(I2,I3)*ZUD(I3,IG1)
     .  /(ZUD(I3,IG2)*ZUD(IG2,I2)*P346*P3456)
      Z43=Z0*ZD(I1,IG2)*ZD(I1,I4)*ZD(I4,IG2)
     .  /(ZD(IG1,I1)*ZD(IG1,I4)*P3456*P345)
      Z1=Z0/(ZD(IG1,I1)*ZUD(IG2,I2)*P34*P346*P3456)
      Z44=Z1*ZUD(IG2,I2)*ZD(I4,IG2)*ZD(I1,IG2)*ZC3
      Z45=Z1*ZD(IG1,I1)*ZUD(I3,IG1)*ZD(I1,I4)*ZC2
      Z46=Z1*ZD(IG1,I1)*ZUD(IG2,I2)*ZD(I4,IG2)*ZUD(I3,IG1)*ZD(I1,IG2)
      Z1=Z0/(ZD(IG1,I1)*ZUD(IG2,I2)*P34*P345*P3456)
      Z47=Z1*ZD(IG1,I1)*ZUD(I3,IG1)*ZD(I1,I4)*ZC4
      Z48=Z1*ZUD(IG2,I2)*ZD(I4,IG2)*ZD(I1,IG2)*ZC5
      Z49=Z1*ZUD(IG2,I2)*ZD(IG1,I1)*ZD(I4,IG2)*ZUD(I3,IG1)*ZD(I1,IG2)
      ZC1=Z11+Z14+Z15+Z16+Z17+Z32+Z35+Z37
      ZC2=Z12+Z13+Z31
      ZC3=Z33+Z34+Z36+Z38+Z39
      ZC4=Z21+Z24+Z25+Z26+Z27+Z43+Z44+Z48
      ZC5=Z22+Z23+Z41
      ZC6=Z42+Z45+Z46+Z47+Z49
      DO 71 IB=1,2
        ZM1(IB)=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
     .         +ZD(IG1,I4)*ZKO(IB,IG1)+ZD(IG2,I4)*ZKO(IB,IG2)
        ZM2(IB)=ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .         +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG1,I1)*ZKO(IB,IG1)
     .         +ZD(IG2,I1)*ZKO(IB,IG2)
        ZM3(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .         +ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1)
   71 CONTINUE
      DO 72 IAD=1,2
        ZM4(IAD)=ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(I4,I3)*ZKOD(IAD,I4)
     .          +ZUD(IG1,I3)*ZKOD(IAD,IG1)+ZUD(IG2,I3)*ZKOD(IAD,IG2)
        ZM5(IAD)=ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(I3,I2)*ZKOD(IAD,I3)
     .          +ZUD(I4,I2)*ZKOD(IAD,I4)+ZUD(IG1,I2)*ZKOD(IAD,IG1)
     .          +ZUD(IG2,I2)*ZKOD(IAD,IG2)
        ZM6(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG1)*ZKOD(IAD,I4)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
   72 CONTINUE
      DO 73 IAD=1,2
        Z0=ZKOD(IAD,I1)
        DO 73 IB=1,2
          ZB(IAD,IB)=Z0*(ZC1*ZM1(IB)+ZC2*ZM2(IB)+ZC3*ZM3(IB))
     .      +ZKO(IB,I2)*(ZC4*ZM4(IAD)+ZC5*ZM5(IAD)+ZC6*ZM6(IAD))
   73 CONTINUE
*
      END
*
      SUBROUTINE B2H8(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B2PROP/ P34,P345,P346,P1345,P2346,P3456,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2)
     .         ,ZM5(2),ZM6(2),ZM7(2),ZM8(2)
      SAVE /DOTPRO/,/B2PROP/
*
      ZC1=ZD(I2,I4)*ZUD(I2,IG1)+ZD(I3,I4)*ZUD(I3,IG1)
     .     +ZD(IG2,I4)*ZUD(IG2,IG1)
      ZC2=2.0*(RR(I3,I2)+RR(I3,IG2))
      ZC3=ZD(I3,I1)*ZUD(I3,IG1)+ZD(I4,I1)*ZUD(I4,IG1)
      ZC4=ZC3+ZD(IG2,I1)*ZUD(IG2,IG1)
      ZC5=ZD(I4,I3)*ZUD(I4,IG2)+ZD(IG1,I3)*ZUD(IG1,IG2)
      ZC6=ZD(I3,I1)*ZUD(I3,IG2)+ZD(I4,I1)*ZUD(I4,IG2)
     .     +ZD(IG1,I1)*ZUD(IG1,IG2)
      ZC7=ZD(I1,I3)*ZUD(I1,IG1)+ZD(I4,I3)*ZUD(I4,IG1)
      ZC8=ZD(I2,I3)*ZUD(I2,IG1)+ZD(IG2,I3)*ZUD(IG2,IG1)
      ZC9=2.0*(RR(I3,I1)+RR(I3,I4)+RR(I3,IG1))
      Z0=1.0/(ZD(IG1,I1)*ZD(IG2,I3)*P23456)
      Z11=-Z0*ZC1*ZC2/(ZD(IG2,I2)*P2346*P34)
      Z12=Z0*ZD(I1,I4)*ZUD(I2,IG2)/(ZD(IG1,I4)*P3456)
      Z13=Z0*ZC1*ZUD(I2,IG2)/(P2346*P346)
      Z14=-Z0*ZC2*ZD(I1,I4)/(ZD(IG2,I2)*ZD(IG1,I4)*P345)
      Z15=-Z0*ZC1*ZUD(I2,I3)*ZD(I3,I4)*ZUD(IG2,I4)/(P2346*P34*P346)
      Z16=-Z0*ZC3*ZC2/(ZD(IG2,I2)*P34*P345)
      Z17=Z0*ZC4*ZUD(I2,IG2)/(P346*P3456)
      Z18=-Z0*ZC5*ZUD(I2,I3)*ZD(I1,I4)/(ZD(IG1,I4)*P345*P3456)
      Z19=-Z0*ZC6*ZUD(I2,I3)*ZD(I3,I4)*ZUD(IG1,I4)/(P34*P345*P3456)
      Z110=-Z0*ZC4*ZUD(I2,I3)*ZD(I3,I4)*ZUD(IG2,I4)/(P34*P346*P3456)
      Z0=ZD(I1,I4)/(ZD(IG1,I1)*ZD(IG2,I3))
      Z21=Z0*ZD(I1,I4)/(ZD(I4,IG1)*P13456*P3456)
      Z22=Z0*ZD(I1,I4)/(ZD(I4,IG1)*ZD(I2,IG2)*P1345*P345)
      Z23=-Z0*ZC9*ZD(I1,I4)/(ZD(I4,IG1)*P1345*P13456*P345)
      Z24=-Z0*ZC3/(ZD(I2,IG2)*P1345*P34*P345)
      Z25=Z0*ZC3*ZC9/(P1345*P13456*P34*P345)
      Z26=-Z0*ZC5*ZD(I1,I4)/(ZD(I4,IG1)*P13456*P345*P3456)
      Z27=-Z0*ZC4/(P13456*P346*P3456)
      Z28=Z0*ZC6*ZD(I3,I4)*ZUD(IG1,I4)/(P13456*P345*P34*P3456)
      Z29=Z0*ZC4*ZD(I3,I4)*ZUD(IG2,I4)/(P13456*P346*P34*P3456)
      Z0=1.0/P23456
      Z31=-Z0*ZC1*ZUD(I2,IG2)*ZUD(I3,IG2)/(ZD(IG1,I1)*P2346*P346*P34)
      Z32=Z0*ZC8*ZUD(I3,IG1)/(ZD(I3,IG2)*ZD(IG2,I2)*P345*P34)
      Z33=Z0*ZUD(IG2,IG1)*ZUD(I2,IG1)/(ZD(IG2,I3)*P346*P3456)
      Z34=-Z0*ZUD(I2,IG2)*ZUD(I3,IG2)*ZD(I1,I4)
     .  /(ZD(IG1,I1)*ZD(IG1,I4)*P345*P3456)
      Z35=-Z0/(ZD(IG1,I1)*ZD(IG2,I3)*P34*P3456*P346)
     .  *(-2.0*RR(I3,IG2)*(ZUD(IG2,IG1)*ZD(IG1,I1)*ZUD(I2,IG1)
     .  +ZC4*ZUD(I2,IG2))
     .  +ZD(I3,I4)*ZUD(IG2,I4)*ZUD(I3,IG1)*ZUD(I2,IG1)*ZD(IG1,I1))
      Z36=-Z0/(ZD(IG1,I1)*ZD(IG2,I3)*P34*P3456*P345)
     .  *(ZC5*ZUD(I2,IG1)*ZUD(I3,IG1)*ZD(IG1,I1)
     .  -2.0*RR(I3,IG2)*ZC3*ZUD(I2,IG2)
     .  +ZUD(IG1,IG2)*ZUD(I3,IG1)*ZUD(I2,IG2)*ZD(IG2,I3)*ZD(IG1,I1))
      Z0=ZD(I1,I4)
      Z41=-Z0*ZUD(I3,IG1)/(ZD(I3,IG2)*ZD(IG2,I2)*P1345*P34*P345)
      Z42=Z0*ZC7*ZUD(I3,IG1)/(ZD(IG2,I3)*P1345*P13456*P34*P345)
      Z43=-Z0*ZUD(IG2,IG1)/(ZD(IG2,I3)*P13456*P346*P3456)
      Z44=Z0*ZUD(I3,IG2)*ZD(I1,I4)
     .    /(ZD(IG1,I1)*ZD(IG1,I4)*P13456*P345*P3456)
      Z1=Z0/(ZD(IG1,I1)*ZD(IG2,I3)*P13456*P34*P346*P3456)
      Z45=-Z1*ZC4*2.0*RR(I3,IG2)
      Z46=Z1*ZD(IG1,I1)*ZD(I4,I3)*ZUD(I4,IG2)*ZUD(I3,IG1)
      Z47=-Z1*2.0*RR(I3,IG2)*ZUD(IG2,IG1)*ZD(IG1,I1)
      Z1=Z0/(ZD(IG1,I1)*ZD(IG2,I3)*P13456*P34*P345*P3456)
      Z48=Z1*ZC5*ZD(IG1,I1)*ZUD(I3,IG1)
      Z49=-Z1*ZC3*2.0*RR(I3,IG2)
      Z410=Z1*ZUD(IG1,IG2)*ZUD(I3,IG1)*ZD(IG2,I3)*ZD(IG1,I1)
      ZC1=Z11+Z13+Z15+Z31
      ZC2=Z12+Z14+Z16+Z17+Z18+Z19+Z110+Z32+Z33+Z34+Z35+Z36
      ZC4=Z21+Z23+Z25+Z27+Z42+Z44+Z45+Z49+Z410
      ZC5=Z26+Z28+Z29
      ZC6=Z43+Z46+Z47+Z48
      ZC7=Z22+Z24
      DO 81 IB=1,2
        ZM1(IB)=ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .         +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG1,I1)*ZKO(IB,IG1)
     .         +ZD(IG2,I1)*ZKO(IB,IG2)
        ZM2(IB)=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
     .         +ZD(IG1,I4)*ZKO(IB,IG1)+ZD(IG2,I4)*ZKO(IB,IG2)
        ZM3(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(IG2,I3)*ZKO(IB,IG2)
   81 CONTINUE
      DO 82 IAD=1,2
        ZM4(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG1)*ZKOD(IAD,I4)
        ZM5(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM6(IAD)=ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(I4,I3)*ZKOD(IAD,I4)
     .          +ZUD(IG1,I3)*ZKOD(IAD,IG1)
        ZM7(IAD)=ZM6(IAD)+ZUD(IG2,I3)*ZKOD(IAD,IG2)
        ZM8(IAD)=ZM4(IAD)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
   82 CONTINUE
      DO 83 IB=1,2
        Z0=ZKO(IB,I2)
        Z1=ZM3(IB)
        DO 83 IAD=1,2
          ZB(IAD,IB)=ZKOD(IAD,I1)*(ZC1*ZM1(IB)+ZC2*ZM2(IB))
     .     +Z0*(ZC4*ZM5(IAD)+ZC5*ZM7(IAD)+ZC6*ZM8(IAD))
     .     +Z1*(ZC7*ZM6(IAD)+Z41*ZM4(IAD))
   83 CONTINUE
*
      END
*
      SUBROUTINE B3(I1,I2,I3,I4,IG1,IG2,ZB,ZBC,ITYPE)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZB(2,2),ZBC(2,2)
*
      CALL B3H(I1,I2,I3,I4,IG1,IG2,ZB,ITYPE)
      CALL CONGAT(ZB,ZBC)
*
      END
*
      SUBROUTINE B3H(I1,I2,I3,I4,IG1,IG2,ZB,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2)
      SAVE /DOTPRO/,/B3PROP/
*
      P34=2.0*RR(I3,I4)
      P56=2.0*RR(IG1,IG2)
      P234=P34+2.0*(RR(I2,I3)+RR(I2,I4))
      P346=P34+2.0*(RR(IG2,I3)+RR(IG2,I4))
      P156=P56+2.0*(RR(I1,IG1)+RR(I1,IG2))
      P456=P56+2.0*(RR(I4,IG1)+RR(I4,IG2))
      P3456=P346+2.0*(RR(IG1,I3)+RR(IG1,I4)+RR(IG1,IG2))
      P2346=P234+2.0*(RR(IG2,I2)+RR(IG2,I3)+RR(IG2,I4))
      P13456=P3456+2.0*(RR(I1,I3)+RR(I1,I4)+RR(I1,IG1)+RR(I1,IG2))
      P23456=P3456+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,IG1)+RR(I2,IG2))
*
      IF (ITYPE.EQ.1) CALL B3H1(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.2) CALL B3H2(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.3) CALL B3H3(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.4) CALL B3H4(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.5) CALL B3H5(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.6) CALL B3H6(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.7) CALL B3H7(I1,I2,I3,I4,IG1,IG2,ZB)
      IF (ITYPE.EQ.8) CALL B3H8(I1,I2,I3,I4,IG1,IG2,ZB)
*
      END
*
      SUBROUTINE B3H1(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),
     .          ZM6(2),ZM7(2),ZM8(2),ZM9(2),ZM10(2),ZM11(2),ZM12(2)
      SAVE /DOTPRO/,/B3PROP/
*
      ZC2=ZD(I2,I3)*ZUD(I2,I4)+ZD(IG2,I3)*ZUD(IG2,I4)
      ZC1=ZC2*ZD(IG2,I4)
     .   +ZD(IG2,I3)*2.0*(RR(I3,I2)+RR(I3,I4)+RR(I3,IG2))
      ZC3=ZD(I1,I3)*ZUD(I1,I4)+ZD(IG1,I3)*ZUD(IG1,I4)
      ZC4=ZC3*ZD(IG2,I4)
     .   +ZD(IG2,I3)*2.0*(RR(I3,I1)+RR(I3,IG1))
      ZC6=2.0*(RR(IG2,I2)+RR(IG2,I3)+RR(IG2,I4))
      ZC5=ZC6*ZD(IG1,IG2)
     .   +ZD(IG1,I3)*(ZD(I2,IG2)*ZUD(I2,I3)+ZD(I4,IG2)*ZUD(I4,I3))
     .   +ZD(IG1,I4)*(ZD(I2,IG2)*ZUD(I2,I4)+ZD(I3,IG2)*ZUD(I3,I4))
      ZC7=2.0*(RR(IG2,I1)+RR(IG2,IG1))
      ZC8=ZC7*ZD(IG1,IG2)
     .   +ZD(IG1,I3)*(ZD(I1,IG2)*ZUD(I1,I3)+ZD(IG1,IG2)*ZUD(IG1,I3))
     .   +ZD(IG1,I4)*(ZD(I1,IG2)*ZUD(I1,I4)+ZD(IG1,IG2)*ZUD(IG1,I4))
      Z0=ZUD(I2,I4)/ZUD(IG2,IG1)
      Z11=Z0*ZC1/(P23456*P2346*P346*P34)
      Z12=-Z0/(ZUD(I1,IG1)*P2346*P346*P34)
      Z13=-Z0*ZC2/(ZUD(I4,IG2)*P23456*P2346*P346)
      Z14=Z0/(ZUD(I1,IG1)*ZUD(I4,IG2)*P2346*P346)
      Z15=Z0*ZD(IG2,IG1)/(P23456*P3456*P346)
      Z16=-Z0*ZD(IG1,I3)*ZUD(I4,I3)/(ZUD(I4,IG2)*P23456*P3456*P346)
      Z17=-Z0*ZD(IG2,I3)/(P2346*P34*P23456)
      Z18=-Z0/(ZUD(I1,IG1)*P2346*P234*P34)
      Z19=-Z0/(ZUD(I1,IG1)*P234*P34)
      Z0=1.0/(ZUD(IG2,IG1)*P13456)
      Z21=-Z0*ZD(I3,IG2)/(ZUD(I1,IG1)*P34)
      Z22=-Z0*ZC3/(ZUD(I1,IG1)*ZUD(I4,IG2)*P346)
      Z23=-Z0*ZD(I1,I3)*ZD(IG2,IG1)/(P3456*P346)
      Z24=Z0*ZC4/(ZUD(I1,IG1)*P346*P34)
      Z25=Z0*ZD(I1,I3)*ZD(I3,IG1)*ZUD(I3,I4)/(ZUD(I4,IG2)*P346*P3456)
      Z0=1.0/(P34*P3456)
      Z31=Z0*ZD(I3,IG2)*ZUD(I2,I4)*ZC5/(P23456*P2346*P346)
      Z32=Z0*ZD(I3,IG2)*ZUD(I2,I4)*ZC6/(ZUD(IG1,IG2)*P23456*P2346)
      Z33=-Z0*ZD(I3,IG2)*ZUD(I2,I4)/(ZUD(I1,IG1)*P2346*P346)
      Z34=-Z0*ZD(I3,IG2)*ZUD(I2,I4)/(ZUD(I1,IG1)*ZUD(IG1,IG2)*P2346)
      Z35=Z0*ZD(I3,IG2)*ZC8/(ZUD(I1,IG1)*P13456*P346)
      Z36=Z0*ZD(I3,IG2)*ZC7/(ZUD(I1,IG1)*ZUD(IG1,IG2)*P13456)
      Z1=-Z0*ZD(I3,IG1)
     .  *(ZUD(IG1,IG2)*ZUD(I3,I4)*ZD(I3,IG2)+ZUD(I4,IG1)*P346)
     .  /(ZUD(IG1,IG2)*ZUD(I4,IG2)*P346)
      Z37=Z1*ZUD(I2,I4)/P23456
      Z38=-Z1*ZD(I1,IG1)/P13456
      Z39=Z0*ZUD(I2,I4)*ZD(I3,IG2)*(2.0*RR(IG1,IG2)+P346)
     .   /(ZUD(IG2,IG1)*P346*P23456)
      Z310=Z0*ZUD(I2,I4)*ZD(I3,IG1)/(ZUD(IG2,IG1)*P23456)
      Z311=-Z0*(2.0*RR(IG1,IG2)*ZD(I3,IG2)*ZD(I1,IG1)
     .         +P346*(ZD(I3,IG2)*ZD(I1,IG1)+ZD(I3,IG1)*ZD(I1,IG2)))
     .    /(ZUD(IG2,IG1)*P13456*P346)
      ZC1=Z11+Z13+Z17+Z31+Z32+Z37+Z39
      ZC2=Z15+Z16
      ZC3=Z21+Z22+Z23+Z24+Z25+Z35+Z36+Z38+Z311
      DO 11 IB=1,2
        ZM1(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
        ZM5(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
        ZM2(IB)=ZM5(IB)+ZD(IG2,I3)*ZKO(IB,IG2)
        ZM3(IB)=ZM2(IB)+ZD(IG1,I3)*ZKO(IB,IG1)
        ZM4(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .         +ZD(I4,IG2)*ZKO(IB,I4)
        ZM6(IB)=ZM4(IB)+ZD(IG1,IG2)*ZKO(IB,IG1)
   11 CONTINUE
      DO 12 IAD=1,2
        ZM8(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(IG1,I4)*ZKOD(IAD,IG1)
        ZM7(IAD)=ZD(IG2,I4)*ZM8(IAD)+ZD(IG2,I3)
     .      *(ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(IG1,I3)*ZKOD(IAD,IG1))
        ZM9(IAD)=ZD(I3,I4)*ZM8(IAD)+ZD(I3,I2)
     .      *(ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(IG1,I2)*ZKOD(IAD,IG1))
        ZM10(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .           +ZUD(IG1,I4)*ZKOD(IAD,IG1)+ZUD(IG2,I4)*ZKOD(IAD,IG2)
        ZM12(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM11(IAD)=ZD(IG1,IG2)*ZM12(IAD)
     .  +ZD(IG1,I3)*(ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(IG1,I3)*ZKOD(IAD,IG1))
     .  +ZD(IG1,I4)*(ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(IG1,I4)*ZKOD(IAD,IG1))
   12 CONTINUE
      DO 13 IB=1,2
        Z0=ZKO(IB,I2)
        Z1=ZM2(IB)
        Z2=ZM4(IB)
        DO 13 IAD=1,2
          ZB(IAD,IB)=Z0*ZM10(IAD)*ZC3+ZKOD(IAD,IG2)*ZM5(IB)*Z19
     .        +Z1*(ZM7(IAD)*Z12+ZM8(IAD)*Z14)
     .        +Z2*(ZM11(IAD)*Z33+ZM12(IAD)*Z34+ZM9(IAD)*Z18)
     .        +ZKOD(IAD,I1)*(ZM1(IB)*ZC1+ZM3(IB)*ZC2+ZM6(IB)*Z310)
   13 CONTINUE
*
      END
*
      SUBROUTINE B3H2(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),
     .          ZM6(2),ZM7(2),ZM8(2),ZM9(2),ZM10(2)
      SAVE /DOTPRO/,/B3PROP/
*
      ZC1=ZD(I3,IG1)*ZUD(I3,IG2)+ZD(I4,IG1)*ZUD(I4,IG2)
      ZC2=ZD(I4,IG1)*ZUD(I4,I2)+ZD(IG2,IG1)*ZUD(IG2,I2)
      ZC3=ZD(I2,I3)*ZUD(I2,IG2)+ZD(I4,I3)*ZUD(I4,IG2)
      ZC4=ZC1+ZD(I2,IG1)*ZUD(I2,IG2)
      ZC5=ZD(I2,I3)*ZUD(I2,IG2)+ZD(I4,I3)*ZUD(I4,IG2)
      ZC6=ZD(I1,I3)*ZUD(I1,IG2)+ZD(IG1,I3)*ZUD(IG1,IG2)
      ZC7=ZD(I2,I3)*(ZD(I3,IG1)*ZUD(I3,I2)+ZD(I4,IG1)*ZUD(I4,I2)
     .              +ZD(IG2,IG1)*ZUD(IG2,I2))+ZD(IG1,I3)*P346
      ZC8=ZD(I1,I3)*(ZD(I3,IG1)*ZUD(I3,I1)+ZD(I4,IG1)*ZUD(I4,I1)
     .              +ZD(IG2,IG1)*ZUD(IG2,I1))+ZD(IG1,I3)*(P3456-P346)
      ZC9=ZUD(I2,I4)*ZD(I3,I4)*ZUD(I3,IG2)+ZUD(I2,IG2)*(P346-P34)
      Z0=1.0/P56
      Z1=Z0*(ZD(I4,IG2)*ZUD(I2,I4)*ZC1-ZC2*P34)/(ZD(I4,IG2)*P34*P346)
      Z11=-Z1*ZC3/(P2346*P23456)
      Z12=Z1/(ZUD(I1,IG1)*P2346)
      Z13=-Z1*ZC1/(P23456*P3456)
      Z1=Z0*ZUD(I2,I4)/(P234*P34)
      Z14=-Z1*ZC4*ZC5/(P2346*P23456)
      Z15=Z1*ZC5/(ZUD(I1,IG1)*P2346)
      Z16=-Z1*ZD(IG1,I1)*ZUD(IG2,I1)/(ZUD(I1,IG1)*P156)
      Z17=Z0*ZD(IG1,I4)*ZUD(IG2,I4)*ZC2/(ZD(I4,IG2)*P23456*P456*P3456)
      Z0=1.0/(P13456*P56)
      Z21=Z0*ZD(I1,IG1)*ZUD(I1,IG2)*ZC6/(ZUD(I1,IG1)*P34*P156)
      Z22=-Z0*ZD(I1,I3)*ZD(I4,IG1)*ZUD(I4,IG2)/(ZD(I4,IG2)*P456*P3456)
      Z23=Z0*ZC6/(ZUD(I1,IG1)*ZD(I4,IG2)*P346)
      Z24=Z0*ZD(I1,I3)*ZC1**2/(P34*P3456*P346)
      Z25=-Z0*ZC6*ZC1/(ZUD(I1,IG1)*P34*P346)
      Z26=-Z0*ZD(I1,I3)*ZC1/(ZD(I4,IG2)*P346*P3456)
      Z0=1.0/(P34*P3456)
      Z31=Z0*ZUD(I4,IG2)*ZUD(I2,IG2)*ZC7/(P2346*P23456*P346)
      Z32=-Z0*ZUD(I4,IG2)*ZUD(I2,IG2)/(ZUD(I1,IG1)*P2346*P346)
      Z33=Z0*ZUD(I4,IG2)*ZC8/(ZUD(I1,IG1)*P13456*P346)
      Z34=-Z0*ZUD(I2,IG2)*ZUD(I4,IG2)*ZC5/(ZUD(IG2,IG1)*P2346*P23456)
      Z35=Z0*ZUD(I4,IG2)*ZUD(I2,IG2)/(ZUD(I1,IG1)*ZUD(IG2,IG1)*P2346)
      Z36=-Z0*ZUD(I4,IG2)*ZC6/(ZUD(I1,IG1)*ZUD(IG2,IG1)*P13456)
      Z37=-Z0*ZD(I3,IG1)*ZC9/(ZD(I4,IG2)*P346*P23456)
      Z38=Z0*ZD(I3,IG1)*ZC2/(ZD(I4,IG2)*ZD(IG1,IG2)*P23456)
      Z39=Z0*ZD(I3,IG1)*ZD(I1,IG1)/(ZD(I4,IG2)*P13456*P346)
      Z310=-Z0*ZD(I3,IG1)*ZD(I1,IG1)/(ZD(IG1,IG2)*ZD(I4,IG2)*P13456)
      Z1=Z0*ZD(I3,IG1)*ZUD(I4,IG2)/P346
      Z311=Z1*ZUD(I2,IG2)/P23456
      Z312=-Z1*ZD(I1,IG1)/P13456
      ZC1=Z11+Z14+Z31+Z34+Z37+Z38+Z311
      ZC2=Z13+Z17
      ZC3=Z21+Z24+Z25
      ZC4=Z22+Z23+Z26+Z310
      ZC5=Z33+Z36+Z312
      ZC6=Z12+Z35
      DO 21 IB=1,2
        ZM1(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
        ZM4(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
        ZM2(IB)=ZM4(IB)+ZD(IG2,I3)*ZKO(IB,IG2)
        ZM3(IB)=ZM2(IB)+ZD(IG1,I3)*ZKO(IB,IG1)
   21 CONTINUE
      DO 22 IAD=1,2
        ZM5(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM7(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .          +ZUD(IG1,I4)*ZKOD(IAD,IG1)+ZUD(IG2,I4)*ZKOD(IAD,IG2)
        ZM9(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM6(IAD)=ZD(IG1,I4)*ZM7(IAD)+ZD(IG1,IG2)*ZM9(IAD)
        ZM8(IAD)=ZD(IG1,IG2)*ZM5(IAD)
     .  +ZD(IG1,I3)*(ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(IG1,I3)*ZKOD(IAD,IG1))
     .  +ZD(IG1,I4)*(ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(IG1,I4)*ZKOD(IAD,IG1))
        ZM10(IAD)=(P346-P34)*ZM9(IAD)+ZD(I3,I4)*ZUD(I3,IG2)*ZM7(IAD)
   22 CONTINUE
      DO 23 IAD=1,2
        Z0=ZKOD(IAD,I1)
        Z1=ZM7(IAD)*ZC3+ZM6(IAD)*ZC4+ZM9(IAD)*ZC5+ZM10(IAD)*Z39
        Z2=ZM5(IAD)
        Z3=ZM8(IAD)*Z32
        DO 23 IB=1,2
          ZB(IAD,IB)=Z0*(ZM1(IB)*ZC1+ZM3(IB)*ZC2)+ZKO(IB,I2)*Z1
     .        +Z2*(ZM2(IB)*ZC6+ZM1(IB)*Z15+ZM4(IB)*Z16)+Z3*ZM2(IB)
   23 CONTINUE
*
      END
*
      SUBROUTINE B3H3(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),
     .          ZM6(2),ZM7(2),ZM8(2),ZM9(2),ZM11(2)
      SAVE /DOTPRO/,/B3PROP/
*
      ZC1=ZD(I3,IG2)*ZUD(I3,IG1)+ZD(I4,IG2)*ZUD(I4,IG1)
      ZC4=ZD(I2,I3)*ZUD(I2,IG1)+ZD(I4,I3)*ZUD(I4,IG1)
      ZC2=ZC4+ZD(IG2,I3)*ZUD(IG2,IG1)
      ZC3=ZD(I4,IG2)*ZUD(I4,I2)+ZD(IG1,IG2)*ZUD(IG1,I2)
      ZC5=ZD(I2,IG2)*ZUD(I2,IG1)+ZD(I3,IG2)*ZUD(I3,IG1)
     .   +ZD(I4,IG2)*ZUD(I4,IG1)
      ZC7=ZD(I1,I3)*ZUD(I1,IG1)+ZD(IG2,I3)*ZUD(IG2,IG1)
      Z0=1.0/P56
      Z1=Z0*ZUD(I2,I4)*(ZUD(I4,IG2)*ZC1+ZUD(I4,IG1)*P34)
     .  /(ZUD(I4,IG2)*P34*P346*P2346)
      Z11=-Z1*ZC2/P23456
      Z12=Z1*ZD(I1,IG2)/ZD(I1,IG1)
      Z13=Z0*(-ZUD(I2,I4)*ZC1**2/(P346*P34)
     .       -ZUD(I2,I4)*ZUD(I4,IG1)*ZC1/(ZUD(I4,IG2)*P346)
     .       +ZC3*ZUD(I4,IG1)**2/(ZUD(I4,IG2)*P456))/(P3456*P23456)
      Z14=-Z0*ZUD(I2,I4)*ZC4*ZC5/(P34*P234*P2346*P23456)
      Z15=Z0*ZUD(I2,I4)*ZD(I1,IG2)*ZC4/(ZD(I1,IG1)*P234*P2346*P34)
      Z16=-Z0*ZUD(I2,I4)*ZD(I1,IG2)**2/(ZD(I1,IG1)*P234*P34*P156)
      Z0=1.0/(P56*P13456)
      Z21=Z0*ZD(I1,IG2)**2*ZC7/(ZD(I1,IG1)*P34*P156)
      Z22=-Z0*ZUD(I4,IG1)**2*ZD(I1,I3)/(ZUD(I4,IG2)*P456*P3456)
      Z23=-Z0*ZD(I1,IG2)*ZD(I1,I3)*ZUD(I4,IG1)
     .  /(ZD(I1,IG1)*ZUD(I4,IG2)*P346)
      Z24=Z0*ZD(I1,I3)*ZC1**2/(P34*P346*P3456)
      Z25=-Z0*ZD(I1,IG2)*ZD(I1,I3)*ZC1/(ZD(I1,IG1)*P34*P346)
      Z26=Z0*ZUD(I4,IG1)*ZD(I1,I3)*ZC1/(ZUD(I4,IG2)*P346*P3456)
      Z0=1.0/(P34*P3456)
      Z1=Z0*ZD(I3,IG2)/(ZD(IG1,IG2)*P346)
      Z31=Z1*ZUD(I2,I4)*ZC5*P3456/(P2346*P23456)
      Z32=-Z1*ZD(I1,IG2)*ZUD(I2,I4)*P3456/(ZD(I1,IG1)*P2346)
      Z33=Z1*ZD(I1,IG2)**2*P3456/(ZD(I1,IG1)*P13456)
      Z34=-Z1*ZUD(I2,I4)*ZC5*ZC1/(P2346*P23456)
      Z35=Z1*ZUD(I2,I4)*ZC1/P2346
      Z36=-Z1*ZD(I1,IG2)*ZC1/P13456
      Z1=Z0*ZUD(I4,IG1)*
     .  (ZUD(I4,I3)*ZD(I3,IG2)*ZUD(IG1,IG2)-ZUD(I4,IG1)*P346)
     .  /(ZUD(I4,IG2)*ZUD(IG1,IG2)*P346)
      Z37=Z1*ZUD(I2,IG1)/P23456
      Z38=-Z1*ZD(I1,I3)/P13456
      Z1=Z0*ZD(I3,IG2)*ZUD(I4,IG1)/P346
      Z39=Z1*ZUD(I2,IG1)/P23456
      Z310=-Z1*ZD(I1,IG2)/P13456
      ZC1=Z11+Z14+Z31+Z39
      ZC2=Z15+Z32+Z35
      ZC3=Z13+Z37
      ZC4=Z21+Z23+Z24+Z25+Z26+Z33+Z36
      ZC5=Z38+Z310
      DO 31 IB=1,2
        ZM4(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .         +ZD(I4,IG2)*ZKO(IB,I4)
        ZM5(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
        ZM6(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
        ZM1(IB)=ZM4(IB)+ZD(IG1,IG2)*ZKO(IB,IG1)
        ZM2(IB)=ZM5(IB)+ZD(IG2,I3)*ZKO(IB,IG2)
        ZM3(IB)=ZM2(IB)+ZD(IG1,I3)*ZKO(IB,IG1)
   31 CONTINUE
      DO 32 IAD=1,2
        ZM7(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
        ZM8(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .          +ZUD(IG1,I4)*ZKOD(IAD,IG1)+ZUD(IG2,I4)*ZKOD(IAD,IG2)
        ZM11(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .           +ZUD(I4,IG1)*ZKOD(IAD,I4)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
        ZM9(IAD)=ZD(IG2,I4)*ZM8(IAD)+ZD(IG2,IG1)*ZM11(IAD)
   32 CONTINUE
      DO 33 IB=1,2
        Z0=ZM1(IB)*ZC1+ZM2(IB)*Z12+ZM3(IB)*ZC3+ZM4(IB)*ZC2
     .    +ZM6(IB)*Z34
        Z1=ZKO(IB,I2)
        Z2=ZM5(IB)*Z16
        DO 33 IAD=1,2
          ZB(IAD,IB)=Z0*ZKOD(IAD,I1)+Z2*ZM7(IAD)
     .        +Z1*(ZM8(IAD)*ZC4+ZM9(IAD)*Z22+ZM11(IAD)*ZC5)
   33 CONTINUE
*
      END
*
      SUBROUTINE B3H4(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),
     .          ZM6(2),ZM7(2),ZM8(2),ZM9(2),ZM10(2),ZM11(2),ZM12(2)
      SAVE /DOTPRO/,/B3PROP/
*
      ZC1=ZD(I3,IG2)*ZUD(I3,IG1)+ZD(I4,IG2)*ZUD(I4,IG1)
      ZC2=ZD(I2,I3)*ZUD(I2,IG1)+ZD(I4,I3)*ZUD(I4,IG1)
     .   +ZD(IG2,I3)*ZUD(IG2,IG1)
      ZC3=ZD(I3,I1)*ZUD(I3,IG2)+ZD(I4,I1)*ZUD(I4,IG2)
      ZC4=ZD(I4,I1)*ZUD(I4,I2)+ZD(IG2,I1)*ZUD(IG2,I2)
      ZC5=ZD(I2,I3)*ZUD(I2,IG2)+ZD(I4,I3)*ZUD(I4,IG2)
      ZC7=ZUD(I2,I4)*ZD(I3,I4)*ZUD(I3,IG1)+ZUD(I2,IG2)*ZC1
      Z0=1.0/ZD(IG2,IG1)
      Z11=Z0*ZUD(I2,I4)*ZC2/(P34*P346*P2346*P23456)
      Z12=-Z0*ZUD(I2,I4)*ZC3/(ZD(I1,IG1)*P34*P346*P2346)
      Z13=-Z0*ZC2/(ZD(I4,IG2)*P346*P2346*P23456)
      Z14=Z0*ZC4/(ZD(I1,IG1)*ZD(I4,IG2)*P2346*P346)
      Z15=Z0*ZUD(I2,I4)*ZUD(IG2,IG1)/(P346*P23456*P3456)
      Z16=-Z0*ZC7/(ZD(I4,IG2)*P346*P3456*P23456)
      Z17=Z0*ZUD(I2,I4)*ZC5/(P234*P2346*P34)
      Z18=-Z0*ZUD(I2,I4)*ZC5/(ZD(I1,IG1)*P234*P34*P2346)
      Z19=-Z0*ZUD(IG1,I2)/(ZD(I4,IG2)*P23456*P3456)
      Z0=ZD(I1,I3)/(P13456*ZD(IG2,IG1))
      Z21=-Z0/(ZD(I4,IG2)*P3456)
      Z22=-Z0/(ZD(I1,IG1)*ZD(I4,IG2)*P346)
      Z23=-Z0*ZUD(IG2,IG1)/(P346*P3456)
      Z24=Z0*ZC3/(ZD(I1,IG1)*P34*P346)
      Z25=Z0/(ZD(I4,IG2)*P346*P3456)
      Z0=1.0/(P34*P3456)
      Z1=Z0*ZUD(I4,IG2)/(ZD(IG1,IG2)*P346)
      Z31=Z1*ZUD(I2,IG2)*ZC2*P3456/(P2346*P23456)
      Z32=-Z1*ZUD(I2,IG2)*ZD(I1,IG2)*P3456/(ZD(I1,IG1)*P2346)
      Z33=Z1*ZD(I1,I3)*ZD(I1,IG2)*P3456/(ZD(I1,IG1)*P13456)
      Z34=-Z1*ZUD(I2,IG2)*ZC2*ZC1/(P2346*P23456)
      Z35=Z1*ZUD(I2,IG2)*ZC1/P2346
      Z36=-Z1*ZD(I1,I3)*ZC1/P13456
      Z1=-Z0*((ZUD(I4,IG1)*ZD(I3,I4)*ZUD(I3,IG2)
     .        +ZUD(IG2,IG1)*2.0*(RR(IG2,I3)+RR(IG2,I4)))/P346
     .       +2.0*(RR(IG1,I4)+RR(IG1,IG2))/ZD(IG1,IG2))/ZD(I4,IG2)
      Z1=Z1+Z0*ZUD(I4,IG2)*(P56+P346)/(ZD(IG2,IG1)*P346)
      Z37=Z1*ZUD(I2,IG1)/P23456
      Z38=-Z1*ZD(I1,I3)/P13456
      Z1=Z0*ZUD(I4,IG1)/ZD(IG2,IG1)
      Z39=Z1*ZUD(I2,IG2)/P23456
      Z310=-Z1*ZD(I1,I3)/P13456
      DO 41 IB=1,2
        ZM6(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .          +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
        ZM1(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .         +ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1)
        ZM2(IB)=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .         +ZD(IG2,I3)*ZKO(IB,IG2)
        ZM3(IB)=ZM2(IB)+ZD(IG1,I3)*ZKO(IB,IG1)
        ZM4(IB)=ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .         +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG2,I1)*ZKO(IB,IG2)
        Z0=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
     .    +ZD(IG1,I4)*ZKO(IB,IG1)+ZD(IG2,I4)*ZKO(IB,IG2)
        ZM5(IB)=ZUD(IG2,I4)*Z0+ZUD(IG2,I3)*ZM3(IB)
        ZM7(IB)=ZUD(I2,I4)*Z0+ZUD(I2,IG2)*ZM1(IB)
   41 CONTINUE
      DO 42 IAD=1,2
        ZM8(IAD)=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(I3,I4)*ZKOD(IAD,I3)
     .          +ZUD(IG1,I4)*ZKOD(IAD,IG1)+ZUD(IG2,I4)*ZKOD(IAD,IG2)
        ZM10(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .           +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM11(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .           +ZUD(I4,IG1)*ZKOD(IAD,I4)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
        ZM9(IAD)=ZD(I1,I4)*ZM8(IAD)+ZD(I1,IG2)*ZM10(IAD)
        ZM12(IAD)=ZD(I3,I4)*ZUD(I3,IG1)*ZM8(IAD)+ZC1*ZM10(IAD)
   42 CONTINUE
      ZC1=Z12+Z14+Z32+Z35
      ZC2=Z15+Z16+Z19+Z37+Z39
      ZC3=Z21+Z38
      ZC4=Z23+Z24
      ZC5=Z33+Z36+Z310
      DO 43 IB=1,2
        Z0=Z11*ZM5(IB)+ZC1*ZM2(IB)+Z13*ZM7(IB)+ZC2*ZM3(IB)
     .    +Z18*ZM4(IB)+Z34*ZM6(IB)+Z31*ZM1(IB)+Z17*ZKO(IB,IG1)
        Z1=ZKO(IB,I2)
        DO 43 IAD=1,2
          ZB(IAD,IB)=Z0*ZKOD(IAD,I1)
     .              +Z1*(ZC3*ZM11(IAD)+Z22*ZM9(IAD)+ZC4*ZM8(IAD)
     .                  +Z25*ZM12(IAD)+ZC5*ZM10(IAD))
   43 CONTINUE
*
      END
*
      SUBROUTINE B3H5(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),
     .          ZM6(2),ZM7(2),ZM8(2),ZM9(2),ZM10(2),ZM11(2),ZM12(2),
     .          ZDM(2,2)
      SAVE /DOTPRO/,/B3PROP/
*
      ZC1=ZD(IG2,I3)*(ZD(I2,I4)*ZUD(I2,I3)+ZD(IG2,I4)*ZUD(IG2,I3))
     .   +ZD(IG2,I4)*2.0*(RR(I2,I4)+RR(I3,I4)+RR(IG2,I4))
      ZC2=ZD(IG2,I3)*(ZD(I1,I4)*ZUD(I1,I3)+ZD(IG1,I4)*ZUD(IG1,I3))
     .   +ZD(IG2,I4)*2.0*(RR(I1,I4)+RR(IG1,I4))
      ZC3=ZD(IG1,I3)*(ZD(I4,I1)*ZUD(I4,I3)+ZD(IG2,I1)*ZUD(IG2,I3))
     .   +ZD(IG2,I1)*ZUD(IG2,I4)*ZD(IG1,I4)
      ZC4=ZD(I2,IG2)*(ZD(I3,IG1)*ZUD(I3,I2)+ZD(I4,IG1)*ZUD(I4,I2)
     .               +ZD(IG2,IG1)*ZUD(IG2,I2))+ZD(IG1,IG2)*P346
      ZC5=ZD(I1,IG2)*(ZD(I3,IG1)*ZUD(I3,I1)+ZD(I4,IG1)*ZUD(I4,I1)
     .               +ZD(IG2,IG1)*ZUD(IG2,I1))+ZD(IG1,IG2)*(P3456-P346)
      ZC6=ZD(I4,IG1)*ZUD(I3,IG2)*ZUD(I3,I4)+ZD(IG2,IG1)*(P346-P34)
      DO 51 IB=1,2
        ZM1(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
        ZM6(IB)=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
        ZM2(IB)=ZD(IG2,I4)*ZKO(IB,IG2)+ZM6(IB)
        ZM3(IB)=ZD(IG1,I4)*ZKO(IB,IG1)+ZM2(IB)
        ZM5(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .         +ZD(I4,IG2)*ZKO(IB,I4)
        ZM7(IB)=ZD(IG1,IG2)*ZKO(IB,IG1)+ZM5(IB)
        ZM4(IB)=ZD(I3,IG1)*ZUD(I3,I4)*ZM3(IB)
     .         +(ZD(I3,IG1)*ZUD(I3,IG2)+ZD(I4,IG1)*ZUD(I4,IG2))*ZM7(IB)
   51 CONTINUE
      DO 52 IAD=1,2
        ZM8(IAD)=ZKOD(IAD,I1)
     .           *(ZD(I3,IG2)*ZUD(I3,I1)+ZD(I4,IG2)*ZUD(I4,I1))
     .          +ZKOD(IAD,IG1)
     .           *(ZD(I3,IG2)*ZUD(I3,IG1)+ZD(I4,IG2)*ZUD(I4,IG1))
        ZM9(IAD)=ZKOD(IAD,I1)
     .           *(ZD(I2,I4)*ZUD(I2,I1)+ZD(I3,I4)*ZUD(I3,I1))
     .          +ZKOD(IAD,IG1)
     .           *(ZD(I2,I4)*ZUD(I2,IG1)+ZD(I3,I4)*ZUD(I3,IG1))
        ZM10(IAD)=ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(I4,I3)*ZKOD(IAD,I4)
     .           +ZUD(IG1,I3)*ZKOD(IAD,IG1)+ZUD(IG2,I3)*ZKOD(IAD,IG2)
        ZM12(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM11(IAD)=ZD(IG1,IG2)*ZM12(IAD)+ZKOD(IAD,IG1)
     .            *2.0*(RR(I3,IG1)+RR(I4,IG1))+ZKOD(IAD,I1)
     .            *(ZD(I3,IG1)*ZUD(I3,I1)+ZD(I4,IG1)*ZUD(I4,I1))
   52 CONTINUE
      DO 53 IAD=1,2
        Z0=ZUD(I1,I4)*ZKOD(IAD,I1)+ZUD(IG1,I4)*ZKOD(IAD,IG1)
        Z1=ZM12(IAD)
        DO 53 IB=1,2
          ZDM(IAD,IB)=Z0*ZM2(IB)+Z1*ZM5(IB)
   53 CONTINUE
      Z0=ZUD(I2,I3)/ZUD(IG2,IG1)
      Z11=Z0*ZC1/(P23456*P2346*P346*P34)
      Z12=-Z0/(ZUD(I1,IG1)*P2346*P34*P346)
      Z13=Z0*(P2346-P234+2.0*(RR(I2,I4)+RR(I3,I4)+RR(IG2,I4)))
     .   /(ZUD(IG2,I4)*P2346*P23456*P346)
      Z14=-Z0/(ZUD(I1,IG1)*ZUD(IG2,I4)*P2346*P346)
      Z15=Z0*ZD(IG2,IG1)/(P23456*P346*P3456)
      Z16=Z0/(ZUD(IG2,I4)*P346*P23456*P3456)
      Z17=Z0*ZD(I4,IG2)/(P34*P2346*P23456)
      Z18=-Z0/(ZUD(I1,IG1)*P234*P2346*P34)
      Z19=-Z0/(ZUD(I1,IG1)*P234*P34)
      Z110=-Z0/(ZUD(IG2,I4)*P23456*P3456)
      Z0=1.0/(ZUD(IG2,IG1)*P13456)
      Z21=Z0*(-ZD(I4,IG2)/(ZUD(I1,IG1)*P34)
     . +ZD(I1,IG1)/(ZUD(IG2,I4)*P3456)
     . +2.0*(RR(I1,I4)+RR(IG1,I4)+RR(I1,IG2)+RR(IG1,IG2))
     .  /(ZUD(I1,IG1)*ZUD(IG2,I4)*P346))
      Z21=Z21+Z0*(-ZD(I1,I4)*ZD(IG2,IG1)/(P346*P3456)
     . +ZC2/(ZUD(I1,IG1)*P34*P346)+ZC3/(ZUD(IG2,I4)*P346*P3456))
      Z0=1.0/(P34*P3456)
      Z31=Z0*(ZC4/P346+(P2346-P234)/ZUD(IG1,IG2))
     .   *ZD(I4,IG2)*ZUD(I2,I3)/(P23456*P2346)
      Z32=-Z0*ZD(I4,IG2)*ZUD(I2,I3)/(ZUD(I1,IG1)*P2346*P346)
      Z33=-Z0*ZD(I4,IG2)*ZUD(I2,I3)/(ZUD(I1,IG1)*ZUD(IG1,IG2)*P2346)
      Z34=Z0*(ZC5/P346+2.0*(RR(I1,IG2)+RR(IG1,IG2))/ZUD(IG1,IG2))
     .   *ZD(I4,IG2)/(ZUD(I1,IG1)*P13456)
      Z1=-Z0*(ZC6/P346+2.0*(RR(I4,IG1)+RR(IG2,IG1))/ZUD(IG1,IG2))
     .  /ZUD(I4,IG2)
      Z35=Z1*ZUD(I2,I3)/P23456
      Z36=-Z1*ZD(I1,IG1)/P13456
      Z37=Z0*ZUD(I2,I3)*ZD(I4,IG2)*(P56+P346)/(ZUD(IG2,IG1)*P23456*P346)
      Z38=Z0*ZUD(I2,I3)*ZD(I4,IG1)/(ZUD(IG2,IG1)*P23456)
      Z39=-Z0*(ZD(I4,IG2)*ZD(I1,IG1)*(P56+P346)/P346
     .        +ZD(I4,IG1)*ZD(I1,IG2))/(ZUD(IG2,IG1)*P13456)
      ZC1=Z11+Z13+Z17+Z110+Z31+Z35+Z37
      ZC3=Z21+Z34+Z36+Z39
      DO 54 IB=1,2
        Z0=ZC1*ZM1(IB)+Z15*ZM3(IB)+Z16*ZM4(IB)+Z38*ZM7(IB)
        Z1=ZKO(IB,I2)
        Z2=ZM5(IB)
        Z3=ZM2(IB)
        Z4=ZM6(IB)
        DO 54 IAD=1,2
          ZB(IAD,IB)=Z0*ZKOD(IAD,I1)+ZC3*Z1*ZM10(IAD)
     .              +Z2*(Z18*ZM9(IAD)+Z32*ZM11(IAD)+Z33*ZM12(IAD))
     .              +Z12*Z3*ZM8(IAD)+Z19*Z4*ZKOD(IAD,IG2)
     .              +Z14*ZDM(IAD,IB)
   54 CONTINUE
*
      END
*
      SUBROUTINE B3H6(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),
     .          ZM6(2),ZM7(2),ZM8(2),ZM9(2)
      SAVE /DOTPRO/,/B3PROP/
*
      ZC1=ZD(I2,I4)*ZUD(I2,IG2)+ZD(I3,I4)*ZUD(I3,IG2)
      ZC2=ZD(I3,IG1)*ZUD(I3,IG2)+ZD(I4,IG1)*ZUD(I4,IG2)
      ZC3=ZC2+ZD(I2,IG1)*ZUD(I2,IG2)
      ZC4=ZD(I1,I4)*ZUD(I1,IG2)+ZD(IG1,I4)*ZUD(IG1,IG2)
      ZC5=ZD(I4,I1)*ZUD(I4,IG2)+ZD(IG1,I1)*ZUD(IG1,IG2)
      ZC6=ZD(I2,I4)*(ZD(I3,IG1)*ZUD(I3,I2)+ZD(I4,IG1)*ZUD(I4,I2)
     .   +ZD(IG2,IG1)*ZUD(IG2,I2))+ZD(IG1,I4)*P346
      ZC7=ZD(IG1,I3)*(ZD(I1,I4)*ZUD(I1,I3)+ZD(IG1,I4)*ZUD(IG1,I3))
     .   +ZD(IG1,I4)*2.0*(RR(I4,I1)+RR(I4,IG1))+ZD(IG1,IG2)*ZC4
      DO 61 IB=1,2
        ZM1(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .         +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
        ZM4(IB)=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
        ZM2(IB)=ZM4(IB)+ZD(IG2,I4)*ZKO(IB,IG2)
        ZM3(IB)=ZM2(IB)+ZD(IG1,I4)*ZKO(IB,IG1)
        ZM5(IB)=ZUD(IG2,I4)*ZM3(IB)+ZUD(IG2,IG1)*ZM1(IB)
   61 CONTINUE
      DO 62 IAD=1,2
        ZM6(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM7(IAD)=ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(I4,I3)*ZKOD(IAD,I4)
     .          +ZUD(IG1,I3)*ZKOD(IAD,IG1)+ZUD(IG2,I3)*ZKOD(IAD,IG2)
        ZM8(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM9(IAD)=ZKOD(IAD,I1)*(ZD(I3,IG1)*ZUD(I3,I1)
     .            +ZD(I4,IG1)*ZUD(I4,I1)+ZD(IG2,IG1)*ZUD(IG2,I1))
     .          +ZKOD(IAD,IG1)*(P3456-P346)
   62 CONTINUE
      Z0=ZUD(I2,I3)/P56
      Z11=-Z0*ZC1*ZC2/(P34*P346*P2346*P23456)
      Z12=Z0*ZC2/(ZUD(I1,IG1)*P2346*P34*P346)
      Z13=-Z0*ZD(IG1,I4)*ZC1/(ZD(IG2,I4)*P346*P2346*P23456)
      Z14=Z0*ZD(IG1,I4)/(ZUD(I1,IG1)*ZD(IG2,I4)*P2346*P346)
      Z15=-Z0*ZC2**2/(P34*P346*P3456*P23456)
      Z16=-Z0*ZD(IG1,I4)*ZC2/(ZD(IG2,I4)*P346*P3456*P23456)
      Z17=-Z0*ZC3*ZC1/(P34*P234*P2346*P23456)
      Z18=Z0*ZC1/(ZUD(I1,IG1)*P234*P2346*P34)
      Z19=-Z0*ZD(I1,IG1)*ZUD(I1,IG2)/(ZUD(I1,IG1)*P234*P34*P156)
      Z110=-Z0*ZD(IG1,I4)**2/(ZD(IG2,I4)*P456*P3456*P23456)
      Z0=1.0/(P13456*P56)
      Z21=Z0*ZD(I1,IG1)*ZUD(I1,IG2)*ZC4/(ZUD(I1,IG1)*P34*P156)
      Z21=Z21+Z0*ZD(IG1,I4)**2*ZC5/(ZD(IG2,I4)*P456*P3456)
      Z21=Z21-Z0*ZD(IG1,I4)*ZC4/(ZUD(I1,IG1)*ZD(IG2,I4)*P346)
      Z21=Z21+Z0*ZD(I1,I4)*ZC2**2/(P34*P346*P3456)
      Z21=Z21-Z0*ZC4*ZC2/(ZUD(I1,IG1)*P34*P346)
      Z21=Z21+Z0*ZD(I1,I4)*ZD(IG1,I4)*ZC2/(ZD(IG2,I4)*P346*P3456)
      Z0=1.0/(P34*P3456)
      Z31=Z0*ZUD(I3,IG2)*ZUD(I2,IG2)/(P2346*P23456)
     .   *(ZC6/P346+ZC1/ZUD(IG1,IG2))
      Z32=-Z0*ZUD(I3,IG2)*ZUD(I2,IG2)/(ZUD(I1,IG1)*P2346*P346)
      Z33=-Z0*ZUD(I3,IG2)*ZUD(I2,IG2)/(ZUD(I1,IG1)*ZUD(IG1,IG2)*P2346)
      Z34=Z0*ZUD(I3,IG2)/(ZUD(I1,IG1)*P13456)
     .      *(ZC7/P346+ZC4/ZUD(IG1,IG2))
      Z1=-Z0*ZD(I4,IG1)/ZD(I4,IG2)
     .  *(ZD(I3,I4)*ZUD(I3,IG2)/P346+ZD(I4,IG1)/ZD(IG1,IG2))
      Z35=Z1*ZUD(I2,I3)/P23456
      Z36=-Z1*ZD(I1,IG1)/P13456
      Z1=Z0*ZD(I4,IG1)*ZUD(I3,IG2)/P346
      Z37=Z1*ZUD(I2,IG2)/P23456
      Z38=-Z1*ZD(I1,IG1)/P13456
      ZC1=Z11+Z13+Z17+Z31+Z35+Z37
      ZC2=Z15+Z16
      ZC3=Z21+Z36
      ZC4=Z34+Z38
      ZC5=Z12+Z14+Z33
      DO 63 IB=1,2
        Z0=ZC1*ZM1(IB)+ZC2*ZM3(IB)+Z110*ZM5(IB)
        Z1=ZKO(IB,I2)
        Z2=ZC5*ZM2(IB)+Z18*ZM1(IB)+Z19*ZM4(IB)
        Z3=ZM2(IB)
        DO 63 IAD=1,2
          ZB(IAD,IB)=Z0*ZKOD(IAD,I1)+Z1*(ZC3*ZM7(IAD)+ZC4*ZM8(IAD))
     .              +Z2*ZM6(IAD)+Z3*Z32*ZM9(IAD)
   63 CONTINUE
*
      END
*
      SUBROUTINE B3H7(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),
     .          ZM6(2),ZM7(2),ZM8(2),ZM9(2),ZM10(2),ZM11(2),ZM12(2)
      SAVE /DOTPRO/,/B3PROP/
*
      ZC1=ZD(I2,I4)*ZUD(I2,IG1)+ZD(I3,I4)*ZUD(I3,IG1)
      ZC9=ZC1+ZD(IG2,I4)*ZUD(IG2,IG1)
      ZC2=ZD(I3,IG2)*ZUD(I3,IG1)+ZD(I4,IG2)*ZUD(I4,IG1)
      ZC4=ZC2+ZD(I2,IG2)*ZUD(I2,IG1)
      ZC3=ZUD(IG1,I4)*(ZD(I4,IG2)*ZUD(IG1,IG2)+ZC1)+ZUD(IG1,IG2)*ZC4
      ZC5=ZD(I1,I4)*ZUD(I1,IG1)+ZD(IG2,I4)*ZUD(IG2,IG1)
      ZC6=ZD(I4,I1)*ZUD(I4,IG1)+ZD(IG2,I1)*ZUD(IG2,IG1)
      ZC7=ZC6+ZD(I3,I1)*ZUD(I3,IG1)
      ZC8=ZD(I1,I4)*ZUD(I3,I4)*ZD(I3,IG2)+ZD(I1,IG2)*(P346-P34)
      DO 71 IB=1,2
        ZM6(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .         +ZD(I4,IG2)*ZKO(IB,I4)
        ZM1(IB)=ZM6(IB)+ZD(IG1,IG2)*ZKO(IB,IG1)
        ZM7(IB)=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
        ZM2(IB)=ZM7(IB)+ZD(IG2,I4)*ZKO(IB,IG2)
        ZM3(IB)=ZUD(IG1,I4)*ZM2(IB)+ZUD(IG1,IG2)*ZM6(IB)
        ZM4(IB)=ZM2(IB)+ZD(IG1,I4)*ZKO(IB,IG1)
        ZM5(IB)=ZM3(IB)+2.0*(RR(IG1,I4)+RR(IG1,IG2))*ZKO(IB,IG1)
        ZM8(IB)=ZM5(IB)+ZUD(IG1,I3)*(ZD(I2,I3)*ZKO(IB,I2)
     .         +ZD(I4,I3)*ZKO(IB,I4)+ZD(IG1,I3)*ZKO(IB,IG1)
     .         +ZD(IG2,I3)*ZKO(IB,IG2))
        ZM9(IB)=ZUD(I3,I4)*ZD(I3,IG2)*ZM4(IB)
     .         +2.0*(RR(I3,IG2)+RR(I4,IG2))*ZM1(IB)
   71 CONTINUE
      DO 72 IAD=1,2
        ZM10(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
        ZM11(IAD)=ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(I4,I3)*ZKOD(IAD,I4)
     .           +ZUD(IG1,I3)*ZKOD(IAD,IG1)+ZUD(IG2,I3)*ZKOD(IAD,IG2)
        ZM12(IAD)=ZM10(IAD)
     .           +ZUD(I3,IG1)*ZKOD(IAD,I3)+ZUD(I4,IG1)*ZKOD(IAD,I4)
   72 CONTINUE
      Z0=ZUD(I2,I3)/P56
      Z11=-Z0*ZC9*ZC2/(P34*P2346*P346*P23456)
      Z12=Z0*ZD(I1,IG2)*ZC2/(ZD(I1,IG1)*P2346*P34*P346)
      Z13=-Z0*ZC3/(ZUD(IG2,I4)*P2346*P346*P23456)
      Z14=Z0*ZD(I1,IG2)/(ZD(I1,IG1)*ZUD(IG2,I4)*P2346*P346)
      Z15=-Z0*ZC2**2/(P23456*P3456*P346*P34)
      Z16=-Z0*ZC2/(ZUD(IG2,I4)*P346*P23456*P3456)
      Z17=-Z0*ZC4*ZC1/(P34*P234*P2346*P23456)
      Z18=Z0*ZD(I1,IG2)*ZC1/(ZD(I1,IG1)*P234*P2346*P34)
      Z19=-Z0*ZD(I1,IG2)**2/(ZD(I1,IG1)*P156*P234*P34)
      Z110=-Z0*ZD(I4,IG2)*ZUD(I4,IG1)/(ZUD(IG2,I4)*P23456*P456*P3456)
      Z0=1.0/(P13456*P56)
      Z21=Z0*ZD(I1,IG2)**2*ZC5/(ZD(I1,IG1)*P34*P156)
      Z21=Z21+Z0*ZD(I4,IG2)*ZUD(I4,IG1)*ZC6/(ZUD(IG2,I4)*P456*P3456)
      Z21=Z21-Z0*ZD(I1,IG2)*ZC6/(ZD(I1,IG1)*ZUD(IG2,I4)*P346)
      Z21=Z21+Z0*ZD(I1,I4)*ZC2**2/(P34*P346*P3456)
      Z21=Z21-Z0*ZD(I1,IG2)*ZD(I1,I4)*ZC2/(ZD(I1,IG1)*P34*P346)
      Z21=Z21+Z0*ZC6*ZC2/(ZUD(IG2,I4)*P346*P3456)
      Z0=1.0/(P34*P3456)
      Z1=Z0*ZUD(I2,I3)*ZD(I4,IG2)*ZC4/(P2346*P23456)
      Z31=Z1/P346
      Z32=Z1/ZD(IG1,IG2)
      Z1=Z0*ZD(I4,IG2)/ZD(I1,IG1)*(ZC7/P346+ZD(I1,IG2)/ZD(IG1,IG2))
      Z33=-Z1*ZUD(I2,I3)/P2346
      Z34=Z1*ZD(I1,IG2)/P13456
      Z1=Z0*ZUD(I3,IG1)*ZUD(I2,IG1)/(ZUD(I4,IG2)*P23456)
      Z35=-Z1/P346
      Z36=Z1/ZUD(IG1,IG2)
      Z37=-Z0*ZUD(I3,IG1)/(ZUD(I4,IG2)*P13456)
     .    *(-ZC8/P346+ZC6/ZUD(IG1,IG2))
      Z1=Z0*ZUD(I3,IG1)*ZD(I4,IG2)/P346
      Z38=Z1*ZUD(I2,IG1)/P23456
      Z39=-Z1*ZD(I1,IG2)/P13456
      ZC1=Z11+Z13+Z17+Z32+Z38
      ZC2=Z16+Z110+Z36
      ZC3=Z18+Z33
      ZC4=Z21+Z34
      ZC5=Z37+Z39
      DO 73 IB=1,2
        Z0=ZC1*ZM1(IB)+Z12*ZM2(IB)+Z14*ZM3(IB)+Z15*ZM4(IB)
     .    +ZC2*ZM5(IB)+ZC3*ZM6(IB)+Z31*ZM8(IB)+Z35*ZM9(IB)
        Z1=ZKO(IB,I2)
        Z2=Z19*ZM7(IB)
        DO 73 IAD=1,2
          ZB(IAD,IB)=Z0*ZKOD(IAD,I1)+Z1*(ZC4*ZM11(IAD)+ZC5*ZM12(IAD))
     .              +Z2*ZM10(IAD)
   73 CONTINUE
*
      END
*
      SUBROUTINE B3H8(I1,I2,I3,I4,IG1,IG2,ZB)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      COMMON /B3PROP/ P34,P56,P234,P346,P156,P456
     .             ,P3456,P2346,P13456,P23456
      DIMENSION ZB(2,2),ZM1(2),ZM2(2),ZM3(2),ZM4(2),ZM5(2),
     .          ZM6(2),ZM7(2),ZM8(2),ZM9(2)
      SAVE /DOTPRO/,/B3PROP/
*
      ZC4=ZD(I3,I4)*ZUD(I3,IG1)+ZD(IG2,I4)*ZUD(IG2,IG1)
      ZC1=ZC4+ZD(I2,I4)*ZUD(I2,IG1)
      ZC2=ZD(I3,I1)*ZUD(I3,IG2)+ZD(I4,I1)*ZUD(I4,IG2)
      ZC3=ZD(I2,I4)*ZUD(I2,IG2)+ZD(I3,I4)*ZUD(I3,IG2)
      ZC5=ZD(I3,I1)*ZUD(I3,IG1)+ZD(I4,I1)*ZUD(I4,IG1)
     .   +ZD(IG2,I1)*ZUD(IG2,IG1)
      DO 81 IB=1,2
        ZM1(IB)=ZD(I2,I4)*ZKO(IB,I2)+ZD(I3,I4)*ZKO(IB,I3)
     .         +ZD(IG2,I4)*ZKO(IB,IG2)
        ZM2(IB)=ZM1(IB)+ZD(IG1,I4)*ZKO(IB,IG1)
        ZM3(IB)=ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .         +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG2,I1)*ZKO(IB,IG2)
        Z0=ZD(I2,I3)*ZKO(IB,I2)+ZD(I4,I3)*ZKO(IB,I4)
     .    +ZD(IG1,I3)*ZKO(IB,IG1)+ZD(IG2,I3)*ZKO(IB,IG2)
        ZM4(IB)=ZUD(IG2,I4)*ZM2(IB)+ZUD(IG2,I3)*Z0
        ZM5(IB)=ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .         +ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1)
        ZM6(IB)=ZUD(IG1,I4)*ZM2(IB)+ZUD(IG1,IG2)*ZM5(IB)
     .         +ZUD(IG1,I3)*Z0
   81 CONTINUE
      DO 82 IAD=1,2
        ZM7(IAD)=ZUD(I1,I3)*ZKOD(IAD,I1)+ZUD(I4,I3)*ZKOD(IAD,I4)
     .          +ZUD(IG1,I3)*ZKOD(IAD,IG1)+ZUD(IG2,I3)*ZKOD(IAD,IG2)
        ZM8(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
        ZM9(IAD)=ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .          +ZUD(I4,IG1)*ZKOD(IAD,I4)+ZUD(IG2,IG1)*ZKOD(IAD,IG2)
   82 CONTINUE
      Z0=ZUD(I2,I3)/ZD(IG2,IG1)
      Z11=Z0*ZC1/(P34*P2346*P346*P23456)
      Z12=-Z0*ZC2/(ZD(I1,IG1)*P34*P2346*P346)
      Z13=Z0*ZC1/(ZD(IG2,I4)*P346*P2346*P23456)
      Z14=-Z0*ZD(I1,I4)/(ZD(I1,IG1)*ZD(IG2,I4)*P2346*P346)
      Z15=Z0*ZUD(IG2,IG1)/(P346*P3456*P23456)
      Z16=Z0*ZD(I3,I4)*ZUD(I3,IG1)/(ZD(IG2,I4)*P346*P3456*P23456)
      Z17=Z0*ZC3/(P234*P2346*P34)
      Z18=-Z0*ZC3/(ZD(I1,IG1)*P234*P2346*P34)
      Z0=ZD(I1,I4)/(ZD(IG2,IG1)*P13456)
      Z21=Z0*ZD(I1,I4)/(ZD(I1,IG1)*ZD(IG2,I4)*P346)
      Z21=Z21+Z0*ZC2/(ZD(I1,IG1)*P34*P346)
      Z21=Z21-Z0*ZC4/(ZD(IG2,I4)*P346*P3456)
      Z0=1.0/(P34*P3456)
      Z1=Z0*ZUD(I2,IG2)*ZUD(I3,IG2)*ZC1/(P2346*P23456)
      Z31=Z1/P346
      Z32=Z1/ZD(IG1,IG2)
      Z1=Z0*ZUD(I3,IG2)/ZD(I1,IG1)*(ZC5/P346+ZD(I1,IG2)/ZD(IG1,IG2))
      Z33=-Z1*ZUD(I2,IG2)/P2346
      Z34=Z1*ZD(I1,I4)/P13456
      Z1=Z0*ZUD(I3,IG1)/ZD(I4,IG2)
     .   *(ZD(IG1,I4)/ZD(IG1,IG2)-ZD(I3,I4)*ZUD(I3,IG2)/P346)
     .  +Z0*ZUD(I3,IG2)*(P346+P56)/(ZD(IG2,IG1)*P346)
      Z35=Z1*ZUD(I2,IG1)/P23456
      Z36=-Z1*ZD(I1,I4)/P13456
      Z1=Z0*ZUD(I3,IG1)/ZD(IG2,IG1)
      Z39=Z1*ZUD(I2,IG2)/P23456
      Z310=-Z1*ZD(I1,I4)/P13456
      ZC1=Z12+Z14+Z33
      ZC2=Z13+Z15+Z16+Z35+Z39
      ZC3=Z34+Z310
      DO 83 IB=1,2
        Z0=Z11*ZM4(IB)+ZC1*ZM1(IB)+ZC2*ZM2(IB)+Z18*ZM3(IB)
     .    +Z17*ZKO(IB,IG1)+Z32*ZM5(IB)+Z31*ZM6(IB)
        Z1=ZKO(IB,I2)
        DO 83 IAD=1,2
          ZB(IAD,IB)=Z0*ZKOD(IAD,I1)
     .              +Z1*(Z21*ZM7(IAD)+ZC3*ZM8(IAD)+Z36*ZM9(IAD))
   83 CONTINUE
*
      END
*
      SUBROUTINE B4(I1,I2,I3,I4,IG1,IG2,ZB,ZBC,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION ZB(2,2),ZBC(2,2),ZA(2,2),ZS(2,2),
     .          ZK1(2,2),ZK2(2,2)
      SAVE /DOTPRO/
*
      P56=2.0*RR(IG1,IG2)
      P356=P56+2.0*(RR(I3,IG1)+RR(I3,IG2))
      P456=P56+2.0*(RR(I4,IG1)+RR(I4,IG2))
      P3456=P456+2.0*(RR(I3,I4)+RR(I3,IG1)+RR(I3,IG2))
      P13456=P3456+2.0*(RR(I1,I3)+RR(I1,I4)+RR(I1,IG1)+RR(I1,IG2))
      P23456=P3456+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,IG1)+RR(I2,IG2))
*
      IF ((ITYPE.EQ.1).OR.(ITYPE.EQ.8)) THEN
        Z0=1/(ZUD(I3,IG1)*ZUD(IG1,IG2)*ZUD(IG2,I4))
        DO 10 IAD=1,2
          DO 10 IB =1,2
            ZS(IAD,IB)=Z0*ZKO(IB,I4)*
     .        (ZUD(I3 ,I4)*ZKOD(IAD,I3 )+ZUD(IG1,I4)*ZKOD(IAD,IG1)
     .        +ZUD(IG2,I4)*ZKOD(IAD,IG2))
   10   CONTINUE
      END IF
*
      IF ((ITYPE.EQ.2).OR.(ITYPE.EQ.7)) THEN
        Z1=ZD(I3,IG1)*ZUD(I3,IG2)/(ZUD(I3,IG1)*P356*P56)
        Z2=ZD(I4,IG1)*ZUD(I4,IG2)/(ZD(I4,IG2)*P456*P56)
        Z3=1/(ZUD(I3,IG1)*ZD(IG2,I4)*P56)
        DO 11 IAD=1,2
          DO 11 IB =1,2
            ZM1=ZUD(I3,IG2)*ZKOD(IAD,I3)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
            ZM2=ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2)
            ZS(IAD,IB)=-Z1*ZM1*ZKO(IB,I4)-Z2*ZM2*ZKOD(IAD,I3)-Z3*ZM1*ZM2
   11   CONTINUE
      END IF
*
      IF ((ITYPE.EQ.3).OR.(ITYPE.EQ.6)) THEN
        Z1=ZUD(I4,IG1)**2/(ZUD(I4,IG2)*P56*P456)
        Z2=ZD(I3,IG2)**2/(ZD(I3,IG1)*P56*P356)
        Z3=ZUD(I4,IG1)*ZD(I3,IG2)/(ZD(I3,IG1)*ZUD(I4,IG2)*P56)
        DO 12 IAD=1,2
          DO 12 IB =1,2
            ZS(IAD,IB)=-Z1*ZKOD(IAD,I3)*
     .        (ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1))
     .        -Z2*ZKO(IB,I4)*
     .        (ZUD(I3,IG1)*ZKOD(IAD,I3)+ZUD(IG2,IG1)*ZKOD(IAD,IG2))
     .        +Z3*ZKOD(IAD,I3)*ZKO(IB,I4)
   12   CONTINUE
      END IF
*
      IF ((ITYPE.EQ.4).OR.(ITYPE.EQ.5)) THEN
        Z0=1/(ZD(I3,IG1)*ZD(IG1,IG2)*ZD(IG2,I4))
        DO 13 IAD=1,2
          DO 13 IB =1,2
            ZS(IAD,IB)=-Z0*ZKOD(IAD,I3)*
     .        (ZD(I4 ,I3)*ZKO(IB,I4 )+ZD(IG1,I3)*ZKO(IB,IG1)
     .        +ZD(IG2,I3)*ZKO(IB,IG2))
   13   CONTINUE
      END IF
*
      CALL ADDZKS(ZK1,I1,I3,I4,IG1,IG2)
      CALL ADDZKS(ZK2,I2,I3,I4,IG1,IG2)
      IF (ITYPE.LE.4) THEN
        DO 3 IAD=1,2
          DO 3 IB =1,2
            DO 4 ICD=1,2
              DO 4 ID =1,2
                ZA(ICD,ID)=ZKOD(IAD,I1)*ZK2(ICD,IB)*ZKO(ID,I2)/P23456
     .                    -ZKOD(ICD,I1)*ZK1(IAD,ID)*ZKO(IB,I2)/P13456
    4       CONTINUE
            ZB(IAD,IB)=ZMUL(ZA,ZS)/P3456
    3   CONTINUE
        CALL CONGAT(ZB,ZBC)
      ELSE
        DO 5 IAD=1,2
          DO 5 IB =1,2
            DO 6 ICD=1,2
              DO 6 ID =1,2
                ZA(ICD,ID)=ZKO(IB,I1)*ZK2(IAD,ID)*ZKOD(ICD,I2)/P23456
     .                    -ZKO(ID,I1)*ZK1(ICD,IB)*ZKOD(IAD,I2)/P13456
    6       CONTINUE
            ZBC(IAD,IB)=ZMUL(ZA,ZS)/P3456
    5  CONTINUE
       CALL CONGAT(ZBC,ZB)
      END IF
*
      END
*
      SUBROUTINE B5(I1,I2,I3,I4,IG1,IG2,ZB,ZBC,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION ZB(2,2),ZBC(2,2),ZA(2,2),ZS(2,2),
     .          ZK1(2,2),ZK2(2,2),ZM1(2)
      SAVE /DOTPRO/
*
      P346=2.0*(RR(I3,I4)+RR(I3,IG2)+RR(I4,IG2))
      P2346=P346+2.0*(RR(I2,I3)+RR(I2,I4)+RR(I2,IG2))
      P1346=P346+2.0*(RR(I1,I3)+RR(I1,I4)+RR(I1,IG2))
      P23456=P2346+2.0*(RR(IG1,I2)+RR(IG1,I3)+RR(IG1,I4)+RR(IG1,IG2))
      P13456=P1346+2.0*(RR(IG1,I1)+RR(IG1,I3)+RR(IG1,I4)+RR(IG1,IG2))
      Z0=1.0/(ZUD(I3,IG2)*ZUD(IG2,I4))
      Z0C=1.0/(ZD(I3,IG2)*ZD(IG2,I4))
      IF ((ITYPE.EQ.1).OR.(ITYPE.EQ.3)) THEN
        DO 1 IAD=1,2
          DO 1 IB =1,2
            ZS(IAD,IB)=Z0*ZKO(IB,I4)*
     .        (ZUD(I3,I4)*ZKOD(IAD,I3)+ZUD(IG2,I4)*ZKOD(IAD,IG2))
    1   CONTINUE
      END IF
      IF ((ITYPE.EQ.2).OR.(ITYPE.EQ.4)) THEN
        DO 2 IAD=1,2
          DO 2 IB =1,2
            ZS(IAD,IB)=-Z0C*ZKOD(IAD,I3)*
     .        (ZD(I4,I3)*ZKO(IB,I4)+ZD(IG2,I3)*ZKO(IB,IG2))
    2   CONTINUE
      END IF
      IF ((ITYPE.EQ.5).OR.(ITYPE.EQ.7)) THEN
        DO 3 IAD=1,2
          DO 3 IB =1,2
            ZS(IAD,IB)=-Z0*ZKO(IB,I3)*
     .        (ZUD(I4,I3)*ZKOD(IAD,I4)+ZUD(IG2,I3)*ZKOD(IAD,IG2))
    3   CONTINUE
      END IF
      IF ((ITYPE.EQ.6).OR.(ITYPE.EQ.8)) THEN
        DO 4 IAD=1,2
          DO 4 IB =1,2
            ZS(IAD,IB)=Z0C*ZKOD(IAD,I4)*
     .        (ZD(I3,I4)*ZKO(IB,I3)+ZD(IG2,I4)*ZKO(IB,IG2))
    4   CONTINUE
      END IF
      IF ((ITYPE.EQ.1).OR.(ITYPE.EQ.2).OR.
     .    (ITYPE.EQ.5).OR.(ITYPE.EQ.6)) THEN
        CALL ADDZKS(ZK1,I1,I3,I4,IG1,IG2)
        CALL ADDZKS(ZK2,I2,I3,I4,IG2,0)
        DO 9 ID=1,2
          ZM1(ID)=ZUD(I1,I2)*ZKOD(ID,I1)+ZUD(IG1,I2)*ZKOD(ID,IG1)
    9   CONTINUE
        Z1=1.0/(ZUD(I1,IG1)*ZUD(IG1,I2))
        Z2=1.0/ZUD(IG1,I2)
        Z11=Z1/P2346
        Z12=Z2/(P2346*P23456)
        Z21=Z1/P13456
        Z22=Z2/(P1346*P13456)
        DO 5 IAD=1,2
          DO 5 IB =1,2
            DO 6 ICD=1,2
              DO 6 ID =1,2
                ZA(ICD,ID)=Z11*ZM1(IAD)*ZK2(ICD,IB)*ZKO(ID,I2)
     .            -Z21*ZM1(ICD)*ZK1(IAD,ID)*ZKO(IB,I2)
                ZA(ICD,ID)=ZA(ICD,ID)-Z12*ZKOD(IAD,I1)*ZKO(ID,I2)
     .            *(ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .            +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2))
     .            *(ZUD(I3,I2)*ZKOD(ICD,I3)+ZUD(I4,I2)*ZKOD(ICD,I4)
     .            +ZUD(IG2,I2)*ZKOD(ICD,IG2))
                ZA(ICD,ID)=ZA(ICD,ID)-Z22*ZKOD(ICD,I1)*ZKO(IB,I2)
     .            *(ZD(I1,IG1)*ZKO(ID,I1)+ZD(I3,IG1)*ZKO(ID,I3)
     .            +ZD(I4,IG1)*ZKO(ID,I4)+ZD(IG2,IG1)*ZKO(ID,IG2))
     .            *(ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(I3,I2)*ZKOD(IAD,I3)
     .            +ZUD(I4,I2)*ZKOD(IAD,I4)+ZUD(IG1,I2)*ZKOD(IAD,IG1)
     .            +ZUD(IG2,I2)*ZKOD(IAD,IG2))
    6       CONTINUE
            ZB(IAD,IB)=ZMUL(ZA,ZS)/P346
    5   CONTINUE
      ELSE
        CALL ADDZKS(ZK1,I1,I3,I4,IG2,0)
        CALL ADDZKS(ZK2,I2,I3,I4,IG1,IG2)
        DO 14 I=1,2
          ZM1(I)=ZD(I2,I1)*ZKO(I,I2)+ZD(IG1,I1)*ZKO(I,IG1)
   14   CONTINUE
        Z1=1.0/(ZD(I1,IG1)*ZD(IG1,I2))
        Z2=1.0/ZD(I1,IG1)
        Z11=Z2/(P2346*P23456)
        Z12=Z1/P23456
        Z21=Z2/(P1346*P13456)
        Z22=Z1/P1346
        DO 10 IAD=1,2
          DO 10 IB =1,2
            DO 11 ICD=1,2
              DO 11 ID =1,2
                ZA(ICD,ID)=-Z12*ZM1(ID)*ZK2(ICD,IB)*ZKOD(IAD,I1)
     .            +Z22*ZM1(IB)*ZK1(IAD,ID)*ZKOD(ICD,I1)
                ZA(ICD,ID)=ZA(ICD,ID)+Z11*ZKOD(IAD,I1)*ZKO(ID,I2)
     .            *(ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .            +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG1,I1)*ZKO(IB,IG1)
     .            +ZD(IG2,I1)*ZKO(IB,IG2))
     .            *(ZUD(I2,IG1)*ZKOD(ICD,I2)+ZUD(I3,IG1)*ZKOD(ICD,I3)
     .            +ZUD(I4,IG1)*ZKOD(ICD,I4)+ZUD(IG2,IG1)*ZKOD(ICD,IG2))
                ZA(ICD,ID)=ZA(ICD,ID)+Z21*ZKOD(ICD,I1)*ZKO(IB,I2)
     .            *(ZD(I3,I1)*ZKO(ID,I3)+ZD(I4,I1)*ZKO(ID,I4)
     .            +ZD(IG2,I1)*ZKO(ID,IG2))
     .            *(ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .            +ZUD(I4,IG1)*ZKOD(IAD,I4)+ZUD(IG2,IG1)*ZKOD(IAD,IG2))
   11       CONTINUE
            ZB(IAD,IB)=ZMUL(ZA,ZS)/P346
   10   CONTINUE
      END IF
      CALL CONGAT(ZB,ZBC)
*
      END
*
      SUBROUTINE B6(I1,I2,I3,I4,IG1,IG2,ZB,ZBC,ITYPE)
      PARAMETER(NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION ZB(2,2),ZBC(2,2),ZA(2,2),ZS(2,2),
     .          ZK1(2,2),ZK2(2,2),ZK3(2,2),ZK4(2,2),ZK5(2,2),ZK6(2,2),
     .          ZM1(2),ZM2(2)
      SAVE /DOTPRO/
*
      P34=2.0*RR(I3,I4)
      P56=2.0*RR(IG1,IG2)
      P134=P34+2.0*(RR(I1,I3)+RR(I1,I4))
      P234=P34+2.0*(RR(I2,I3)+RR(I2,I4))
      P156=P56+2.0*(RR(I1,IG1)+RR(I1,IG2))
      P256=P56+2.0*(RR(I2,IG1)+RR(I2,IG2))
      P1345=P134+2.0*(RR(I1,IG1)+RR(I3,IG1)+RR(I4,IG1))
      P2345=P234+2.0*(RR(I2,IG1)+RR(I3,IG1)+RR(I4,IG1))
      P2346=P234+2.0*(RR(I2,IG2)+RR(I3,IG2)+RR(I4,IG2))
      P13456=P1345+2.0*(RR(I1,IG2)+RR(I3,IG2)+RR(I4,IG2)+RR(IG1,IG2))
      P23456=P2345+2.0*(RR(I2,IG2)+RR(I3,IG2)+RR(I4,IG2)+RR(IG1,IG2))
      IF (ITYPE.LE.4) THEN
        DO 1 IAD=1,2
          DO 1 IB =1,2
            ZS(IAD,IB)=ZKOD(IAD,I3)*ZKO(IB,I4)
    1   CONTINUE
      ELSE
        DO 2 IAD=1,2
          DO 2 IB =1,2
            ZS(IAD,IB)=ZKOD(IAD,I4)*ZKO(IB,I3)
    2   CONTINUE
      END IF
      IF ((ITYPE.EQ.1).OR.(ITYPE.EQ.5)) THEN
        Z0=1.0/(ZUD(I1,IG1)*ZUD(IG1,IG2)*ZUD(IG2,I2))
        Z11=Z0/P234
        Z12=Z0/(P2346*P23456)
        Z13=Z0/(P234*P2346)
        Z21=Z0/P13456
        Z22=Z0/(P134*P1345)
        Z23=Z0/(P13456*P1345)
        CALL ADDZKS(ZK1,I2,I3,I4,0,0)
        CALL ADDZKS(ZK2,I1,I3,I4,IG1,IG2)
        DO 4 IAD=1,2
          ZM1(IAD)=ZUD(I3,I2)*ZKOD(IAD,I3)+ZUD(I4,I2)*ZKOD(IAD,I4)
    4     ZM2(IAD)=ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(I3,I2)*ZKOD(IAD,I3)
     .            +ZUD(I4,I2)*ZKOD(IAD,I4)+ZUD(IG1,I2)*ZKOD(IAD,IG1)
     .            +ZUD(IG2,I2)*ZKOD(IAD,IG2)
        DO 6 IAD=1,2
          DO 6 IB =1,2
            DO 5 ICD=1,2
              DO 5 ID =1,2
                Z0=ZKO(ID,I2)
                ZA(ICD,ID)=Z11*Z0*ZK1(ICD,IB)
     .            *(ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(IG1,I2)*ZKOD(IAD,IG1)
     .            +ZUD(IG2,I2)*ZKOD(IAD,IG2))
                ZA(ICD,ID)=ZA(ICD,ID)
     .            -Z12*Z0*ZUD(I1,IG1)*ZKOD(IAD,I1)*ZM1(ICD)
     .            *(ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .            +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2))
                ZA(ICD,ID)=ZA(ICD,ID)-Z13*Z0*ZM1(ICD)
     .            *(ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(IG1,IG2)*ZKOD(IAD,IG1))
     .            *(ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .            +ZD(I4,IG2)*ZKO(IB,I4))
                Z0=ZKO(IB,I2)
                ZA(ICD,ID)=ZA(ICD,ID)-Z21*Z0*ZK2(IAD,ID)
     .            *(ZUD(I1,I2)*ZKOD(ICD,I1)+ZUD(IG1,I2)*ZKOD(ICD,IG1)
     .            +ZUD(IG2,I2)*ZKOD(ICD,IG2))
                ZA(ICD,ID)=ZA(ICD,ID)
     .            -Z22*Z0*ZUD(I1,IG1)*ZKOD(ICD,I1)*ZM2(IAD)
     .            *(ZD(I1,IG1)*ZKO(ID,I1)+ZD(I3,IG1)*ZKO(ID,I3)
     .            +ZD(I4,IG1)*ZKO(ID,I4))
                ZA(ICD,ID)=ZA(ICD,ID)-Z23*Z0*ZM2(IAD)
     .            *(ZUD(I1,IG2)*ZKOD(ICD,I1)+ZUD(IG1,IG2)*ZKOD(ICD,IG1))
     .            *(ZD(I1,IG2)*ZKO(ID,I1)+ZD(I3,IG2)*ZKO(ID,I3)
     .            +ZD(I4,IG2)*ZKO(ID,I4)+ZD(IG1,IG2)*ZKO(ID,IG1))
    5       CONTINUE
            ZB(IAD,IB)=ZMUL(ZA,ZS)/P34
    6   CONTINUE
      END IF
      IF ((ITYPE.EQ.2).OR.(ITYPE.EQ.6)) THEN
        Z11=-ZD(I1,IG1)*ZUD(I1,IG2)/(ZUD(I1,IG1)*P56*P234*P156)
        Z12=1.0/(ZUD(I1,IG1)*P56*P234*P2346)
        Z13=1.0/(ZUD(I1,IG1)*ZD(I2,IG2)*P56*P2346)
        Z14=-ZD(I2,IG1)*ZUD(I2,IG2)/(ZD(I2,IG2)*P56*P23456*P256)
        Z15=-1.0/(ZD(I2,IG2)*P56*P2346*P23456)
        Z16=-(ZD(I2,IG1)*ZUD(I2,IG2)+ZD(I3,IG1)*ZUD(I3,IG2)
     .       +ZD(I4,IG1)*ZUD(I4,IG2))/(P56*P234*P2346*P23456)
        Z21=ZD(I1,IG1)*ZUD(I1,IG2)/(ZUD(I1,IG1)*P56*P13456*P156)
        Z22=1.0/(ZUD(I1,IG1)*P56*P13456*P1345)
        Z23=-1.0/(ZUD(I1,IG1)*ZD(I2,IG2)*P56*P1345)
        Z24=ZD(I2,IG1)*ZUD(I2,IG2)/(ZD(I2,IG2)*P56*P134*P256)
        Z25=-1.0/(ZD(I2,IG2)*P56*P1345*P134)
        Z26=(ZD(I1,IG1)*ZUD(I1,IG2)+ZD(I3,IG1)*ZUD(I3,IG2)
     .      +ZD(I4,IG1)*ZUD(I4,IG2))/(P56*P134*P1345*P13456)
        CALL ADDZKS(ZK1,I2,I3,I4,  0,  0)
        CALL ADDZKS(ZK2,I2,I3,I4,IG2,  0)
        CALL ADDZKS(ZK3,I2,I3,I4,IG1,IG2)
        CALL ADDZKS(ZK4,I1,I3,I4,  0,  0)
        CALL ADDZKS(ZK5,I1,I3,I4,IG1,  0)
        CALL ADDZKS(ZK6,I1,I3,I4,IG1,IG2)
        DO 12 IAD=1,2
          ZM1(IAD)=ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(IG1,IG2)*ZKOD(IAD,IG1)
   12   CONTINUE
        DO 13 IB=1,2
          ZM2(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(IG2,IG1)*ZKO(IB,IG2)
   13   CONTINUE
        DO 15 IAD=1,2
          DO 15 IB =1,2
            DO 14 ICD=1,2
              DO 14 ID =1,2
                Z0=(ZUD(I2,IG2)*ZKOD(ICD,I2)+ZUD(I3,IG2)*ZKOD(ICD,I3)
     .            +ZUD(I4,IG2)*ZKOD(ICD,I4))
     .            *(ZD(I2,IG1)*ZKO(IB,I2)+ZD(I3,IG1)*ZKO(IB,I3)
     .            +ZD(I4,IG1)*ZKO(IB,I4)+ZD(IG2,IG1)*ZKO(IB,IG2))
                ZA(ICD,ID)=Z11*ZM1(IAD)*ZK1(ICD,IB)*ZKO(ID,I2)
     .            +Z12*Z0*ZM1(IAD)*ZKO(ID,I2)
     .            +Z13*ZM1(IAD)*ZM2(ID)*ZK2(ICD,IB)
     .            +Z14*ZM2(ID)*ZK3(ICD,IB)*ZKOD(IAD,I1)
     .            +Z15*Z0*ZM2(ID)*ZKOD(IAD,I1)
     .            +Z16*Z0*ZKOD(IAD,I1)*ZKO(ID,I2)
                Z0=(ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .            +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1))
     .            *(ZD(I1,IG1)*ZKO(ID,I1)+ZD(I3,IG1)*ZKO(ID,I3)
     .            +ZD(I4,IG1)*ZKO(ID,I4))
                ZA(ICD,ID)=ZA(ICD,ID)+Z22*Z0*ZM1(ICD)*ZKO(IB,I2)
     .            +Z21*ZM1(ICD)*ZK6(IAD,ID)*ZKO(IB,I2)
     .            +Z23*ZM1(ICD)*ZM2(IB)*ZK5(IAD,ID)
     .            +Z24*ZM2(IB)*ZK4(IAD,ID)*ZKOD(ICD,I1)
     .            +Z25*Z0*ZM2(IB)*ZKOD(ICD,I1)
     .            +Z26*Z0*ZKOD(ICD,I1)*ZKO(IB,I2)
   14       CONTINUE
            ZB(IAD,IB)=ZMUL(ZA,ZS)/P34
   15   CONTINUE
      END IF
      IF ((ITYPE.EQ.3).OR.(ITYPE.EQ.7)) THEN
        Z0=1.0/(ZD(I1,IG1)*P56*ZUD(I2,IG2))
        Z11=-Z0*ZD(I1,IG2)**2*ZUD(I2,IG2)/(P234*P156)
        Z12=-Z0*ZUD(I2,IG1)**2*ZD(I1,IG1)/(P23456*P256)
        Z13=Z0*ZD(I1,IG2)*ZUD(I2,IG1)/P23456
        Z14=Z0/(P234*P23456)
        Z15=Z0*P56/(P234*P2346*P23456)
     .    *(ZD(I2,IG2)*ZUD(I2,IG1)+ZD(I3,IG2)*ZUD(I3,IG1)
     .     +ZD(I4,IG2)*ZUD(I4,IG1))
        Z21=Z0*ZD(I1,IG2)**2*ZUD(I2,IG2)/(P13456*P156)
        Z22=Z0*ZUD(I2,IG1)**2*ZD(I1,IG1)/(P134*P256)
        Z23=-Z0*ZD(I1,IG2)*ZUD(I2,IG1)/P134
        Z24=Z0/(P134*P13456)
        Z25=-Z0*P56/(P134*P1345*P13456)
     .    *(ZD(I1,IG2)*ZUD(I1,IG1)+ZD(I3,IG2)*ZUD(I3,IG1)
     .     +ZD(I4,IG2)*ZUD(I4,IG1))
        CALL ADDZKS(ZK1,I2,I3,I4,  0,  0)
        CALL ADDZKS(ZK2,I2,I3,I4,IG1,IG2)
        CALL ADDZKS(ZK3,I1,I3,I4,  0,  0)
        CALL ADDZKS(ZK4,I1,I3,I4,IG1,IG2)
        DO 8 IB=1,2
          ZM1(IB)=ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .            +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG1,I1)*ZKO(IB,IG1)
     .            +ZD(IG2,I1)*ZKO(IB,IG2)
    8     ZM2(IB)=ZD(I3,I1)*ZKO(IB,I3)+ZD(I4,I1)*ZKO(IB,I4)
        DO 10 IAD=1,2
          DO 10 IB =1,2
            DO 9 ICD=1,2
              DO 9 ID =1,2
                Z0=ZKOD(IAD,I1)*ZKO(ID,I2)
                ZA(ICD,ID)=Z11*ZK1(ICD,IB)*ZKO(ID,I2)
     .            *(ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(IG2,IG1)*ZKOD(IAD,IG2))
                ZA(ICD,ID)=ZA(ICD,ID)+Z12*ZK2(ICD,IB)*ZKOD(IAD,I1)
     .            *(ZD(I2,IG2)*ZKO(ID,I2)+ZD(IG1,IG2)*ZKO(ID,IG1))
                ZA(ICD,ID)=ZA(ICD,ID)+Z13*Z0*ZK2(ICD,IB)
                ZA(ICD,ID)=ZA(ICD,ID)+Z14*Z0
     .            *(ZUD(I2,IG1)*ZKOD(ICD,I2)+ZUD(I3,IG1)*ZKOD(ICD,I3)
     .            +ZUD(I4,IG1)*ZKOD(ICD,I4))
     .            *(ZUD(I2,IG1)*ZD(IG1,IG2)*ZM1(IB)
     .            +ZD(I1,IG2)*ZUD(I2,IG2)
     .            *(ZD(I2,IG2)*ZKO(IB,I2)+ZD(I3,IG2)*ZKO(IB,I3)
     .            +ZD(I4,IG2)*ZKO(IB,I4)+ZD(IG1,IG2)*ZKO(IB,IG1)))
                ZA(ICD,ID)=ZA(ICD,ID)+Z15*Z0*ZM1(IB)
     .            *(ZUD(I3,I2)*ZKOD(ICD,I3)+ZUD(I4,I2)*ZKOD(ICD,I4))
                Z0=ZKOD(ICD,I1)*ZKO(IB,I2)
                ZA(ICD,ID)=ZA(ICD,ID)+Z21*ZK4(IAD,ID)*ZKO(IB,I2)
     .            *(ZUD(I1,IG1)*ZKOD(ICD,I1)+ZUD(IG2,IG1)*ZKOD(ICD,IG2))
                ZA(ICD,ID)=ZA(ICD,ID)+Z22*ZK3(IAD,ID)*ZKOD(ICD,I1)
     .            *(ZD(I2,IG2)*ZKO(IB,I2)+ZD(IG1,IG2)*ZKO(IB,IG1))
                ZA(ICD,ID)=ZA(ICD,ID)+Z23*Z0*ZK3(IAD,ID)
                ZA(ICD,ID)=ZA(ICD,ID)+Z24*Z0
     .            *(ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .            +ZUD(I4,IG1)*ZKOD(IAD,I4)+ZUD(IG2,IG1)*ZKOD(IAD,IG2))
     .            *(ZUD(I2,IG1)*ZD(IG1,IG2)*ZM2(ID)
     .            +ZD(I1,IG2)*ZUD(I2,IG2)
     .            *(ZD(I1,IG2)*ZKO(ID,I1)+ZD(I3,IG2)*ZKO(ID,I3)
     .            +ZD(I4,IG2)*ZKO(ID,I4)))
                ZA(ICD,ID)=ZA(ICD,ID)+Z25*Z0*ZM2(ID)
     .            *(ZUD(I1,I2)*ZKOD(IAD,I1)+ZUD(I3,I2)*ZKOD(IAD,I3)
     .            +ZUD(I4,I2)*ZKOD(IAD,I4)+ZUD(IG1,I2)*ZKOD(IAD,IG1)
     .            +ZUD(IG2,I2)*ZKOD(IAD,IG2))
    9       CONTINUE
            ZB(IAD,IB)=ZMUL(ZA,ZS)/P34
   10   CONTINUE
      END IF
      IF ((ITYPE.EQ.4).OR.(ITYPE.EQ.8)) THEN
        Z0=1.0/(ZD(I1,IG1)*ZD(IG1,IG2)*ZD(IG2,I2))
        Z11=-Z0/P23456
        Z12=-Z0/(P2346*P23456)
        Z13=Z0/(P234*P2346)
        Z21=Z0/P134
        Z22=-Z0/(P134*P1345)
        Z23=Z0/(P1345*P13456)
        CALL ADDZKS(ZK1,I2,I3,I4,IG1,IG2)
        CALL ADDZKS(ZK2,I1,I3,I4,  0,  0)
        DO 21 IB=1,2
          ZM1(IB)=ZD(I2,I1)*ZKO(IB,I2)+ZD(IG1,I1)*ZKO(IB,IG1)
     .           +ZD(IG2,I1)*ZKO(IB,IG2)
          ZM2(IB)=ZD(I2,IG1)*ZKO(IB,I2)+ZD(IG2,IG1)*ZKO(IB,IG2)
   21   CONTINUE
        DO 23 IAD=1,2
          DO 23 IB =1,2
            DO 22 ICD=1,2
              DO 22 ID =1,2
                Z0=ZKOD(IAD,I1)
                Z1=ZD(I2,I1)*ZKO(IB,I2)+ZD(I3,I1)*ZKO(IB,I3)
     .            +ZD(I4,I1)*ZKO(IB,I4)+ZD(IG1,I1)*ZKO(IB,IG1)
     .            +ZD(IG2,I1)*ZKO(IB,IG2)
                ZA(ICD,ID)=Z11*Z0*ZM1(ID)*ZK1(ICD,IB)
                ZA(ICD,ID)=ZA(ICD,ID)+Z12*Z0*Z1*ZM2(ID)
     .            *(ZUD(I2,IG1)*ZKOD(ICD,I2)+ZUD(I3,IG1)*ZKOD(ICD,I3)
     .            +ZUD(I4,IG1)*ZKOD(ICD,I4)+ZUD(IG2,IG1)*ZKOD(ICD,IG2))
                ZA(ICD,ID)=ZA(ICD,ID)+Z13*Z0*Z1*ZD(IG2,I2)*ZKO(ID,I2)
     .            *(ZUD(I2,IG2)*ZKOD(ICD,I2)+ZUD(I3,IG2)*ZKOD(ICD,I3)
     .            +ZUD(I4,IG2)*ZKOD(ICD,I4))
                Z0=ZKOD(ICD,I1)
                Z1=ZD(I3,I1)*ZKO(ID,I3)+ZD(I4,I1)*ZKO(ID,I4)
                ZA(ICD,ID)=ZA(ICD,ID)+Z21*Z0*ZM1(IB)*ZK2(IAD,ID)
                ZA(ICD,ID)=ZA(ICD,ID)+Z22*Z0*Z1*ZM2(IB)
     .            *(ZUD(I1,IG1)*ZKOD(IAD,I1)+ZUD(I3,IG1)*ZKOD(IAD,I3)
     .            +ZUD(I4,IG1)*ZKOD(IAD,I4))
                ZA(ICD,ID)=ZA(ICD,ID)+Z23*Z0*Z1*ZD(IG2,I2)*ZKO(IB,I2)
     .            *(ZUD(I1,IG2)*ZKOD(IAD,I1)+ZUD(I3,IG2)*ZKOD(IAD,I3)
     .            +ZUD(I4,IG2)*ZKOD(IAD,I4)+ZUD(IG1,IG2)*ZKOD(IAD,IG1))
   22       CONTINUE
            ZB(IAD,IB)=ZMUL(ZA,ZS)/P34
   23   CONTINUE
      END IF
      CALL CONGAT(ZB,ZBC)
*
      END
*
************************************************************************
* ADDZKS adds a number of spinortensor momenta I1..I5 into ZK(2,2)     *
************************************************************************
      SUBROUTINE ADDZKS(ZK,I1,I2,I3,I4,I5)
      PARAMETER (NM=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPRO/ ZUD(NM,NM),ZD(NM,NM),ZKO(2,NM),ZKOD(2,NM),
     .                RR(NM,NM),NEGAEN
      DIMENSION ZK(2,2),INDEX(5)
      SAVE /DOTPRO/
*
      INDEX(1)=I1
      INDEX(2)=I2
      INDEX(3)=I3
      INDEX(4)=I4
      INDEX(5)=I5
      DO 10 I=1,5
        IF (INDEX(I).NE.0) N=I
   10 CONTINUE
*
      DO 20 IAD=1,2
        DO 20 IB=1,2
          ZK(IAD,IB)=(0.0,0.0)
          DO 20 I=1,N
            ZK(IAD,IB)=ZK(IAD,IB)+ZKOD(IAD,INDEX(I))*ZKO(IB,INDEX(I))
   20 CONTINUE
*
      END
