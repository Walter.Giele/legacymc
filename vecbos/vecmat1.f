************************************************************************
* MEQ2W calls the various r sb subprocesses.                           *
************************************************************************
* Table of subprocesses T(2,1:4,6): with first index = W+, W-          *
* Second index:  1 = U DB     2 = C SB    3 = UB D    4 = CB S         *
************************************************************************
* Table of subprocesses T(2,5:9,6):   with first index = W+, W-        *
* Second index:  5 = GG   6= GQ + GQB  7= QG + QBG 8= QQ + QBQB 9=QQB  *
************************************************************************
      SUBROUTINE MEQ2W(TOT,T)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      DIMENSION T(2,9,6),R(2,7),C(28)
      SAVE IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG,/MOM/,/STRUCF/
      DATA IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG
     .    / 1, 2, 3, 4, 5,  6,  7,  8,  9, 10,11/
*
* Common factor in all subprocesses
*
      IFACM0=IFAC(NJET)
*
      IF (IMANNR.GT.1) THEN
        RCON=1D0
        IF (IMANNR.EQ.3) RCON=1D0/WHAT**NJET
        DO 5 I=1,7
          R(1,I)=RCON
          R(2,I)=RCON
    5   CONTINUE
      ELSE
        CALL FQ2(NJET,PLAB,1,2,R(1,1),1,IHELRN,IDECAY,0D0,IMANNR)
        R(2,2)=R(1,1)
        R(1,2)=R(2,1)
      END IF
*
      DO 10 I=1,6
        IF (ITRY(I).NE.0) THEN
* Q QB
          C(1)=R(2,2)*F(ID ,I)*G(IUB,I)/  9D0/  IFACM0
          C(2)=R(2,2)*F(IS ,I)*G(ICB,I)/  9D0/  IFACM0
          C(3)=R(1,2)*F(IU ,I)*G(IDB,I)/  9D0/  IFACM0
          C(4)=R(1,2)*F(IC ,I)*G(ISB,I)/  9D0/  IFACM0
* QB Q
          C(5)=R(2,1)*F(IUB,I)*G(ID ,I)/  9D0/  IFACM0
          C(6)=R(2,1)*F(ICB,I)*G(IS ,I)/  9D0/  IFACM0
          C(7)=R(1,1)*F(IDB,I)*G(IU ,I)/  9D0/  IFACM0
          C(8)=R(1,1)*F(ISB,I)*G(IC ,I)/  9D0/  IFACM0
*
          T(1,1,I)=C(3)+C(7)
          T(1,2,I)=C(4)+C(8)
          T(2,3,I)=C(1)+C(5)
          T(2,4,I)=C(2)+C(6)
          T(1,9,I)=C(3)+C(4)+C(7)+C(8)
          T(2,9,I)=C(5)+C(6)+C(1)+C(2)
        END IF
   10 CONTINUE
*
      IF (NJET.EQ.0) GOTO 999
*
      IFACM1=IFAC(NJET-1)
      IF (IMANNR.EQ.1) THEN
        CALL FQ2(NJET,PLAB,3,2,R(1,3),1,IHELRN,IDECAY,0D0,IMANNR)
        R(2,4)=R(1,3)
        R(1,4)=R(2,3)
        CALL FQ2(NJET,PLAB,3,1,R(1,5),1,IHELRN,IDECAY,0D0,IMANNR)
        R(2,6)=R(1,5)
        R(1,6)=R(2,5)
      END IF
      DO 20 I=1,6
        IF (ITRY(I).NE.0) THEN
* Q G
          C( 9)=R(2,5)*F(ID ,I)*G(IG ,I)/  24D0/  IFACM1
          C(10)=R(2,5)*F(IS ,I)*G(IG ,I)/  24D0/  IFACM1
          C(11)=R(1,5)*F(IU ,I)*G(IG ,I)/  24D0/  IFACM1
          C(12)=R(1,5)*F(IC ,I)*G(IG ,I)/  24D0/  IFACM1
* G Q
          C(13)=R(2,3)*F(IG ,I)*G(ID ,I)/  24D0/  IFACM1
          C(14)=R(2,3)*F(IG ,I)*G(IS ,I)/  24D0/  IFACM1
          C(15)=R(1,3)*F(IG ,I)*G(IU ,I)/  24D0/  IFACM1
          C(16)=R(1,3)*F(IG ,I)*G(IC ,I)/  24D0/  IFACM1
* QB G
          C(17)=R(2,6)*F(IUB,I)*G(IG ,I)/  24D0/  IFACM1
          C(18)=R(2,6)*F(ICB,I)*G(IG ,I)/  24D0/  IFACM1
          C(19)=R(1,6)*F(IDB,I)*G(IG ,I)/  24D0/  IFACM1
          C(20)=R(1,6)*F(ISB,I)*G(IG ,I)/  24D0/  IFACM1
* G QB
          C(21)=R(2,4)*F(IG ,I)*G(IUB,I)/  24D0/  IFACM1
          C(22)=R(2,4)*F(IG ,I)*G(ICB,I)/  24D0/  IFACM1
          C(23)=R(1,4)*F(IG ,I)*G(IDB,I)/  24D0/  IFACM1
          C(24)=R(1,4)*F(IG ,I)*G(ISB,I)/  24D0/  IFACM1
*
          T(1,1,I)=T(1,1,I)+C(11)+C(15)+C(19)+C(23)
          T(1,2,I)=T(1,2,I)+C(12)+C(16)+C(20)+C(24)
          T(2,3,I)=T(2,3,I)+C(9)+C(13)+C(17)+C(21)
          T(2,4,I)=T(2,4,I)+C(10)+C(14)+C(18)+C(22)
          T(1,6,I)=C(15)+C(16)+C(23)+C(24)
          T(2,6,I)=C(13)+C(14)+C(21)+C(22)
          T(1,7,I)=C(11)+C(12)+C(19)+C(20)
          T(2,7,I)=C(9)+C(10)+C(17)+C(18)
        END IF
   20 CONTINUE
*
      IF (NJET.EQ.1) GOTO 999
 
      IFACM2=IFAC(NJET-2)
      IF (IMANNR.EQ.1) THEN
        CALL FQ2(NJET,PLAB,3,4,R(1,7),1,IHELRN,IDECAY,0D0,IMANNR)
      END IF
      DO 30 I=1,6
        IF (ITRY(I).NE.0) THEN
* G G
          C(25)=R(2,7)*F(IG ,I)*G( IG,I)/  64D0/  IFACM2
          C(26)=R(2,7)*F(IG ,I)*G( IG,I)/  64D0/  IFACM2
          C(27)=R(1,7)*F(IG ,I)*G( IG,I)/  64D0/  IFACM2
          C(28)=R(1,7)*F(IG ,I)*G( IG,I)/  64D0/  IFACM2
          T(1,1,I)=T(1,1,I)+C(27)
          T(1,2,I)=T(1,2,I)+C(28)
          T(2,3,I)=T(2,3,I)+C(25)
          T(2,4,I)=T(2,4,I)+C(26)
          T(1,5,I)=C(27)+C(28)
          T(2,5,I)=C(25)+C(26)
        END IF
   30 CONTINUE
*
  999 CONTINUE
*
      TOT=0.0
      IF (IWHICH.NE.2) TOT=TOT+T(1,1,IUSE)+T(1,2,IUSE)
      IF (IWHICH.NE.1) TOT=TOT+T(2,3,IUSE)+T(2,4,IUSE)
*
      END
*
************************************************************************
* MEQ2Z calls the various r sb subprocesses.                           *
************************************************************************
* Table of subprocesses T(2,5:9,6):   with first index = 1             *
* Second index:  5 = GG   6= GQ + GQB  7= QG + QBG 8= QQ + QBQB 9=QQB  *
************************************************************************
      SUBROUTINE MEQ2Z(TOT,T)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMW,RGW,RMZ,RGZ,SW2,GW,GZ,NJET,NF,IVEC,IDECAY,
     .                IWHICH,IMANNR,IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /STRUCF/ F(11,6),G(11,6),FACTOR(6)
      DIMENSION T(2,9,6),R(2,7),C(24)
      SAVE IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG,/MOM/,/STRUCF/
      DATA IU,ID,IS,IC,IB,IUB,IDB,ISB,ICB,IBB,IG
     .    / 1, 2, 3, 4, 5,  6,  7,  8,  9, 10,11/
*
      IF (IMANNR.GT.1) THEN
        RCON=1D0
        IF (IMANNR.EQ.3) RCON=1D0/WHAT**NJET
        DO 5 I=1,7
          R(1,I)=RCON
          R(2,I)=RCON
    5   CONTINUE
      END IF
*
      DO 10 J=5,9
        DO 10 K=1,6
          T(1,J,K)=0.0
   10 CONTINUE
*
* Common factor in all subprocesses
*
      IFACM0=IFAC(NJET)
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ2(NJET,PLAB,1,2,R(1,1),2,IHELRN,IDECAY,SW2,IMANNR)
        CALL FQ2(NJET,PLAB,2,1,R(1,2),2,IHELRN,IDECAY,SW2,IMANNR)
      END IF
*
      DO 20 I=1,6
        IF (ITRY(I).NE.0) THEN
* Q QB
          C(1)=R(2,2)*F(ID ,I)*G(IDB,I)/ 9D0/ IFACM0
          C(2)=R(1,2)*F(IU ,I)*G(IUB,I)/ 9D0/ IFACM0
          C(3)=R(2,2)*F(IS ,I)*G(ISB,I)/ 9D0/ IFACM0
          C(4)=R(1,2)*F(IC ,I)*G(ICB,I)/ 9D0/ IFACM0
* QB Q
          C(5)=R(2,1)*F(IDB,I)*G(ID ,I)/ 9D0/ IFACM0
          C(6)=R(1,1)*F(IUB,I)*G(IU ,I)/ 9D0/ IFACM0
          C(7)=R(2,1)*F(ISB,I)*G(IS ,I)/ 9D0/ IFACM0
          C(8)=R(1,1)*F(ICB,I)*G(IC ,I)/ 9D0/ IFACM0
          T(1,9,I)=C(1)+C(2)+C(3)+C(4)+C(5)+C(6)+C(7)+C(8)
        END IF
   20 CONTINUE
*
      IF (NJET.EQ.0) GOTO 999
*
      IFACM1=IFAC(NJET-1)
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ2(NJET,PLAB,3,2,R(1,3),2,IHELRN,IDECAY,SW2,IMANNR)
        CALL FQ2(NJET,PLAB,3,1,R(1,4),2,IHELRN,IDECAY,SW2,IMANNR)
        CALL FQ2(NJET,PLAB,1,3,R(1,5),2,IHELRN,IDECAY,SW2,IMANNR)
        CALL FQ2(NJET,PLAB,2,3,R(1,6),2,IHELRN,IDECAY,SW2,IMANNR)
      END IF
*
      DO 30 I=1,6
        IF (ITRY(I).NE.0) THEN
* Q+QB G
          C( 9)=R(1,5)*F(IUB,I)*G(IG ,I)/  24D0/ IFACM1
          C(10)=R(2,5)*F(IDB,I)*G(IG ,I)/  24D0/ IFACM1
          C(11)=R(1,5)*F(ICB,I)*G(IG ,I)/  24D0/ IFACM1
          C(12)=R(2,5)*F(ISB,I)*G(IG ,I)/  24D0/ IFACM1
          C(13)=R(1,4)*F(IU ,I)*G(IG ,I)/  24D0/ IFACM1
          C(14)=R(2,4)*F(ID ,I)*G(IG ,I)/  24D0/ IFACM1
          C(15)=R(1,4)*F(IC ,I)*G(IG ,I)/  24D0/ IFACM1
          C(16)=R(2,4)*F(IS ,I)*G(IG ,I)/  24D0/ IFACM1
* G Q+QB
          C(17)=R(1,6)*F(IG ,I)*G(IUB,I)/  24D0/ IFACM1
          C(18)=R(2,6)*F(IG ,I)*G(IDB,I)/  24D0/ IFACM1
          C(19)=R(1,6)*F(IG ,I)*G(ICB,I)/  24D0/ IFACM1
          C(20)=R(2,6)*F(IG ,I)*G(ISB,I)/  24D0/ IFACM1
          C(21)=R(1,3)*F(IG ,I)*G(IU ,I)/  24D0/ IFACM1
          C(22)=R(2,3)*F(IG ,I)*G(ID ,I)/  24D0/ IFACM1
          C(23)=R(1,3)*F(IG ,I)*G(IC ,I)/  24D0/ IFACM1
          C(24)=R(2,3)*F(IG ,I)*G(IS ,I)/  24D0/ IFACM1
*
          T(1,6,I)=C(17)+C(18)+C(19)+C(20)+C(21)+C(22)+C(23)+C(24)
          T(1,7,I)=C(9)+C(10)+C(11)+C(12)+C(13)+C(14)+C(15)+C(16)
        END IF
   30 CONTINUE
*
      IF (NJET.EQ.1) GOTO 999
*
      IFACM2=IFAC(NJET-2)
*
      IF (IMANNR.EQ.1) THEN
        CALL FQ2(NJET,PLAB,3,4,R(1,7),2,IHELRN,IDECAY,SW2,IMANNR)
      END IF
*
      DO 40 I=1,6
        IF (ITRY(I).NE.0) THEN
* G G
          C( 1)=R(2,7)*F(IG ,I)*G(IG ,I)/64.D0/ IFACM2
          C( 2)=R(1,7)*F(IG ,I)*G(IG ,I)/64.D0/ IFACM2
          C( 3)=R(2,7)*F(IG ,I)*G(IG ,I)/64.D0/ IFACM2
          C( 4)=R(2,7)*F(IG ,I)*G(IG ,I)/64.D0/ IFACM2
          C( 5)=R(1,7)*F(IG ,I)*G(IG ,I)/64.D0/ IFACM2
*
          T(1,5,I)=T(1,5,I)+C(1)+C(2)+C(3)+C(4)+C(5)
        END IF
   40 CONTINUE
*
  999 CONTINUE
*
      TOT=T(1,5,IUSE)+T(1,6,IUSE)+T(1,7,IUSE)+T(1,8,IUSE)+T(1,9,IUSE)
*
      END
*
************************************************************************
* SUBROUTINE FQ2(N,PLAB,IQ,IQB,RESULT,IW,IHRN,IDECAY,SW2,IM) determine *
* the matrix element squared for the process qqb+Ng -> W,Z -> l lb     *
************************************************************************
* SUBROUTINE FQ2 transforms an event to the locally used P(0:3,10).    *
* Conventions PLAB:   Lepton is particle 9, anti-lepton is 10          *
*    If IW=1          RESULT(1)=W+    RESULT(2)=W-                     *
*    If IW=2          RESULT(1)=Z(up) RESULT(2)=Z(down)                *
*    N = NUMBER OF GLUONS                                              *
*    If IHRN=1 then a MC over helicities is performed                  *
*    If IDECAY=1 (Z-only) E+, E- decay, 2 for neutrinos                *
*    SW2 is sin**2(weak angle)                                         *
************************************************************************
      SUBROUTINE FQ2(N,PLAB,IQ,IQB,RESULT,IW,IHRN,IDECAY,SW2,IM)
      PARAMETER(NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),RESULT(2),SW2
      DIMENSION P(0:3,10),COLMAT(NUPFAC,NUPFAC)
      SAVE COLMAT,INITQ2,NCOL,IORDER
      DATA INITQ2,NCOL,IORDER /1,3,99/
*
* General initialization
*
      IF (INITQ2.EQ.1) THEN
        CALL QQBCOL(COLMAT,N,NCOL,IORDER)
        CALL INIMAP(N)
        INITQ2=0
      END IF
*
* TRANSFORM VECTORS
      DO 10 I=1,N+4
        J=I
        IF (J.GT.N+2) J=8+J-N-2
        DO 10 MU=0,3
          NU=MU
          IF (MU.EQ.0) NU=4
          P(MU,I)=REAL(PLAB(NU,J))
          IF (I.LT.3) P(MU,I)=-P(MU,I)
   10 CONTINUE
*
      CALL RESHQS(P,IQ,IQB,0,0,2)
*
* Init spinors and propagators
*
      CALL INITS(P(0,3),N+2,P(0,1),2)
*
      CALL VECQ2(N,RESULT,COLMAT,IW,IHRN,IDECAY,REAL(SW2),IM)
*
      END
*
************************************************************************
* SUBROUTINE VECQ2(P,N,RESULT,COLMAT,IW,IHRN,IDECAY,IM) determines     *
* the matrix element squared for the process qqb+Ng -> W,Z -> l lb     *
************************************************************************
      SUBROUTINE VECQ2(N,RESULT,COLMAT,IW,IHRN,IDECAY,SW2,IM)
      PARAMETER(NUP=5,NGUP=10,NUPM1=5,NUPM2=20,NUPM3=60,
     .          NUPM4=120,NUPM5=120,NUPFAC=120,NCOL=3)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 RN,RESULT(2)
*
* Subroutine parameters
*
      DIMENSION COLMAT(NUPFAC,NUPFAC)
*
* Global variables
*
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /SPING/  ZKO(2,NGUP),ZKOD(2,NGUP),ZHVEC(2,2,2,NGUP)
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
*
* Local variables
*
      DIMENSION ZQ(2,2,NUPM5,0:NUP),ZQB(2,2,NUPM5,0:NUP),IHELI(NUP)
      DIMENSION ZSADB(2,2,2,NUPFAC),ZH(2,2,NUP),IP(NUP,NUPFAC)
      DIMENSION ZLCUR(2,2),ZRCUR(2,2),ZD(NUPFAC,4),ZT1(2,2),ZT2(2,2)
      SAVE /GLUOPR/,/SPING/,/MAPPIN/,IP,INIT
      DATA INIT /1/
      IF (INIT.EQ.1) THEN
        DO 3 I=1,IFAC(N)
          CALL INVID(I,IP(1,I),N)
   3    CONTINUE
        INIT=0
      END IF
*
      RESULT(1)=0.0
      RESULT(2)=0.0
      NFAC=IFAC(N)
*
* Init couplings
*
      IF (IW.EQ.1) THEN
        FRR1=0.0
        FRL1=1.0
        FRR2=1.0
        FRL2=0.0
      ELSE
        CI3=-0.5
        CQ=-1.0
        IF (IDECAY.EQ.2) THEN
          CI3=0.5
          CQ=0.0
        END IF
        FRR1=(((-SW2*CQ)*(-SW2*2.0/3.0))**2
     .       +((CI3-SW2*CQ)*(0.5-SW2*2.0/3.0))**2)
        FRL1=(((CI3-SW2*CQ)*(-SW2*2.0/3.0))**2
     .       +((-SW2*CQ)*(0.5-SW2*2.0/3.0))**2)
        FRR2=(((-SW2*CQ)*(SW2*1.0/3.0))**2
     .       +((CI3-SW2*CQ)*(-0.5+SW2*1.0/3.0))**2)
        FRL2=(((CI3-SW2*CQ)*(SW2*1.0/3.0))**2
     .       +((-SW2*CQ)*(-0.5+SW2*1.0/3.0))**2)
      END IF
 
      DO 5 IAD=1,2
        DO 5 IB=1,2
          ZLCUR(IAD,IB)=ZKOD(IAD,N+1)*ZKO(IB,N+2)
          ZRCUR(IAD,IB)=ZKOD(IAD,N+2)*ZKO(IB,N+1)
    5 CONTINUE
*
      IF ((IHRN.EQ.1).AND.(N.GT.0)) THEN
        IHELRN=1+INT(2**(N-1)*RN(1))
      ELSE
        IHELRN=0
      END IF
*
* Sum over half the helicity combinations of the gluons.
*
      IF (N.EQ.0) NRHELS=1
      IF (N.GT.0) NRHELS=2**(N-1)
      DO 10 I=1,NRHELS
*
* Check whether this helicity combination needs to be evaluated.
*
        IF ((IHRN.EQ.1).AND.(I.NE.IHELRN).AND.(N.GT.0)) GOTO 10
*
* Init helicity vectors before call to CURS
*
        IHEL=I-1
        DO 15 J=1,N
          IHELI(J)=MOD(IHEL,2)+1
          IHEL=IHEL/2
   15   CONTINUE
        DO 20 J=1,N
          CALL COPYTS(ZH(1,1,J),ZHVEC(1,1,IHELI(J),J),1)
   20   CONTINUE
*
* Init all gluonic currents up to length N, helicities in ZH
*
        CALL INITPR(ZP1,N)
        CALL CURS(N,1,ZH,0)
*
* Init all quark and anti-quark currents.
*
        CALL QCUR0(1,N,1,ZQ(1,1,1,0),ZQ(1,1,1,1),ZQ(1,1,1,2),
     .                       ZQ(1,1,1,3),ZQ(1,1,1,4),ZQ(1,1,1,5))
        CALL QBCUR0(2,N,1,ZQB(1,1,1,0),ZQB(1,1,1,1),ZQB(1,1,1,2),
     .                       ZQB(1,1,1,3),ZQB(1,1,1,4),ZQB(1,1,1,5))
*
        DO 70 J=1,NFAC
* Sum over gluons to left of vector boson, 1=+-   2=-+
          DO 60 IAD=1,2
            DO 60 IB=1,2
              IF (N.EQ.0) THEN
                I0=1
                ZT1(IAD,IB)=ZQ(IAD,2,I0,0)*ZQB(IB,1,I0,0)
                ZT2(IAD,IB)=ZQ(IB,1,I0,0)*ZQB(IAD,2,I0,0)
              ELSE IF (N.EQ.1) THEN
                I0=1
                I1=M1H(IP(1,J))
                ZT1(IAD,IB)=
     .            +ZQ(IAD,2,I0,0)*ZQB(IB,1,I1,1)
     .            +ZQ(IAD,2,I1,1)*ZQB(IB,1,I0,0)
                ZT2(IAD,IB)=
     .            +ZQ(IB,1,I0,0)*ZQB(IAD,2,I1,1)
     .            +ZQ(IB,1,I1,1)*ZQB(IAD,2,I0,0)
              ELSE IF (N.EQ.2) THEN
                I0=1
                I1L=M1H(IP(1,J))
                I1R=M1H(IP(2,J))
                I2=M2H(IP(1,J),IP(2,J))
                ZT1(IAD,IB)=
     .            +ZQ(IAD,2,I0,0)*ZQB(IB,1,I2,2)
     .            +ZQ(IAD,2,I1L,1)*ZQB(IB,1,I1R,1)
     .            +ZQ(IAD,2,I2,2)*ZQB(IB,1,I0,0)
                ZT2(IAD,IB)=
     .            +ZQ(IB,1,I0,0)*ZQB(IAD,2,I2,2)
     .            +ZQ(IB,1,I1L,1)*ZQB(IAD,2,I1R,1)
     .            +ZQ(IB,1,I2,2)*ZQB(IAD,2,I0,0)
              ELSE IF (N.EQ.3) THEN
                I0=1
                I1L=M1H(IP(1,J))
                I1R=M1H(IP(3,J))
                I2L=M2H(IP(1,J),IP(2,J))
                I2R=M2H(IP(2,J),IP(3,J))
                I3=M3H(IP(1,J),IP(2,J),IP(3,J))
                ZT1(IAD,IB)=
     .            +ZQ(IAD,2,I0,0)*ZQB(IB,1,I3,3)
     .            +ZQ(IAD,2,I1L,1)*ZQB(IB,1,I2R,2)
     .            +ZQ(IAD,2,I2L,2)*ZQB(IB,1,I1R,1)
     .            +ZQ(IAD,2,I3,3)*ZQB(IB,1,I0,0)
                ZT2(IAD,IB)=
     .            +ZQ(IB,1,I0,0)*ZQB(IAD,2,I3,3)
     .            +ZQ(IB,1,I1L,1)*ZQB(IAD,2,I2R,2)
     .            +ZQ(IB,1,I2L,2)*ZQB(IAD,2,I1R,1)
     .            +ZQ(IB,1,I3,3)*ZQB(IAD,2,I0,0)
              ELSE IF (N.EQ.4) THEN
                I0=1
                I1L=M1H(IP(1,J))
                I1R=M1H(IP(4,J))
                I2L=M2H(IP(1,J),IP(2,J))
                I2R=M2H(IP(3,J),IP(4,J))
                I3L=M3H(IP(1,J),IP(2,J),IP(3,J))
                I3R=M3H(IP(2,J),IP(3,J),IP(4,J))
                I4=M4H(IP(1,J),IP(2,J),IP(3,J),IP(4,J))
                ZT1(IAD,IB)=
     .            +ZQ(IAD,2,I0,0)*ZQB(IB,1,I4,4)
     .            +ZQ(IAD,2,I1L,1)*ZQB(IB,1,I3R,3)
     .            +ZQ(IAD,2,I2L,2)*ZQB(IB,1,I2R,2)
     .            +ZQ(IAD,2,I3L,3)*ZQB(IB,1,I1R,1)
     .            +ZQ(IAD,2,I4,4)*ZQB(IB,1,I0,0)
                ZT2(IAD,IB)=
     .            +ZQ(IB,1,I0,0)*ZQB(IAD,2,I4,4)
     .            +ZQ(IB,1,I1L,1)*ZQB(IAD,2,I3R,3)
     .            +ZQ(IB,1,I2L,2)*ZQB(IAD,2,I2R,2)
     .            +ZQ(IB,1,I3L,3)*ZQB(IAD,2,I1R,1)
     .            +ZQ(IB,1,I4,4)*ZQB(IAD,2,I0,0)
              ELSE IF (N.EQ.5) THEN
                I0=1
                I1L=M1H(IP(1,J))
                I1R=M1H(IP(5,J))
                I2L=M2H(IP(1,J),IP(2,J))
                I2R=M2H(IP(4,J),IP(5,J))
                I3L=M3H(IP(1,J),IP(2,J),IP(3,J))
                I3R=M3H(IP(3,J),IP(4,J),IP(5,J))
                I4L=M4H(IP(1,J),IP(2,J),IP(3,J),IP(4,J))
                I4R=M4H(IP(2,J),IP(3,J),IP(4,J),IP(5,J))
                I5=M5H(IP(1,J),IP(2,J),IP(3,J),IP(4,J),IP(5,J))
                ZT1(IAD,IB)=
     .            +ZQ(IAD,2,I0,0)*ZQB(IB,1,I5,5)
     .            +ZQ(IAD,2,I1L,1)*ZQB(IB,1,I4R,4)
     .            +ZQ(IAD,2,I2L,2)*ZQB(IB,1,I3R,3)
     .            +ZQ(IAD,2,I3L,3)*ZQB(IB,1,I2R,2)
     .            +ZQ(IAD,2,I4L,4)*ZQB(IB,1,I1R,1)
     .            +ZQ(IAD,2,I5,5)*ZQB(IB,1,I0,0)
                ZT2(IAD,IB)=
     .            +ZQ(IB,1,I0,0)*ZQB(IAD,2,I5,5)
     .            +ZQ(IB,1,I1L,1)*ZQB(IAD,2,I4R,4)
     .            +ZQ(IB,1,I2L,2)*ZQB(IAD,2,I3R,3)
     .            +ZQ(IB,1,I3L,3)*ZQB(IAD,2,I2R,2)
     .            +ZQ(IB,1,I4L,4)*ZQB(IAD,2,I1R,1)
     .            +ZQ(IB,1,I5,5)*ZQB(IAD,2,I0,0)
              END IF
   60     CONTINUE
*
* Transform spinortensors ZT1=AD(down) B(down)
*                         ZT2=AD(up)   B(up)
          ZSADB(1,1,2,J)=ZT2(2,2)
          ZSADB(1,2,2,J)=-ZT2(2,1)
          ZSADB(2,1,2,J)=-ZT2(1,2)
          ZSADB(2,2,2,J)=ZT2(1,1)
          ZD(J,1)=ZMUL(ZT1,ZLCUR)
          ZD(J,2)=ZMUL(ZSADB(1,1,2,J),ZLCUR)
          ZD(J,3)=ZMUL(ZT1,ZRCUR)
          ZD(J,4)=ZMUL(ZSADB(1,1,2,J),ZRCUR)
   70   CONTINUE
*
        R1=SQUARE(COLMAT,NUPFAC,ZD(1,1),NFAC)
        R2=SQUARE(COLMAT,NUPFAC,ZD(1,2),NFAC)
        R3=SQUARE(COLMAT,NUPFAC,ZD(1,3),NFAC)
        R4=SQUARE(COLMAT,NUPFAC,ZD(1,4),NFAC)
        RESULT(1)=RESULT(1)+FRR1*(R1+R4)+FRL1*(R2+R3)
        RESULT(2)=RESULT(2)+FRR2*(R1+R4)+FRL2*(R2+R3)
*
   10 CONTINUE
*
* Explanation of factors to correct the results:
* x16 : extracted Gw/sqrt(8) istead of Gw/sqrt(2) (2**4)
* x4  : spinor algebra of contracting two currents (2*2)
* x(Nc**2-1)/(2**N * Nc**(N-1) ) left out in colour matrix
* remark: parity trick doubles the result for N=0, corrected in COLMAT
      RESULT(1)=32.0*RESULT(1)*2.0*(NCOL**2-1.0)/(2.0**N*NCOL**(N-1.0))
      RESULT(2)=32.0*RESULT(2)*2.0*(NCOL**2-1.0)/(2.0**N*NCOL**(N-1.0))
*
      IF ((IHRN.EQ.1).AND.(N.GT.0)) THEN
        RESULT(1)=RESULT(1)*2**(N-1.0)
        RESULT(2)=RESULT(2)*2**(N-1.0)
      END IF
 
      END
*
**********************************************************************
* FUNCTION SQUARE(COLMAT,NMAT,Z,N) determines the square of a column *
* of N complex numbers Z with an NMAT*NMAT colour matrix.            *
* The matrix is supposed to be symmetric.                            *
**********************************************************************
      FUNCTION SQUARE(COLMAT,NMAT,Z,N)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
      DIMENSION COLMAT(NMAT,NMAT),Z(*)
*
      SQUARE=0.0
      DO 10 I=1,N
        Z1=0.5*COLMAT(I,I)*Z(I)
        DO 20 J=I+1,N
          Z1=Z1+COLMAT(I,J)*Z(J)
   20   CONTINUE
        SQUARE=SQUARE+2.0*REAL(Z1*CONJG(Z(I)))
   10 CONTINUE
*
      END
*
* specialised massless version of QCUR
      SUBROUTINE QCUR0(IQ,N,ILAST,Z0,Z1,Z2,Z3,Z4,Z5)
      PARAMETER(NUP=5,IQUP=4,NUPM1=5,NUPM2=20,NUPM3=60,
     .          NUPM4=120,NUPM5=120,NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters
*
      DIMENSION Z0(2,2),Z1(2,2,*),Z2(2,2,*),
     .          Z3(2,2,*),Z4(2,2,*),Z5(2,2,*)
*
* Global variables
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /SPINQ/ ZQKO(2,IQUP),ZQKOD(2,IQUP),
     .               ZQMO(2,IQUP),ZQMOD(2,IQUP)
      COMMON /FERMPR/ ZQP0(2,2,IQUP),ZQP1(2,2,IQUP,NUP),
     .                ZQP2(2,2,IQUP,NUPM2),ZQP3(2,2,IQUP,NUPM3),
     .                ZQP4(2,2,IQUP,NUPM4),ZQP5(2,2,IQUP,NUPM5),
     .                PQP1(IQUP,NUP),PQP2(IQUP,NUPM2),PQP3(IQUP,NUPM3),
     .                PQP4(IQUP,NUPM4),PQP5(IQUP,NUPM5)
*
* Local variables
*
      DIMENSION ZH1(2,2),ZH2(2,2),ZH3(2,2),ZH4(2,2),ZH5(2,2)
      DIMENSION IPERM(NUP,NUPFAC)
      SAVE /MAPPIN/,/GLUCUR/,/SPINQ/,/FERMPR/,IPERM,INIT
      DATA INIT /1/
*
      IF (INIT.EQ.1) THEN
        INIT=0
        DO 5 I=1,IFAC(N)
          CALL INVID(I,IPERM(1,I),N)
    5   CONTINUE
      END IF
*
* Init the currents of length 0, UB(+) and UB(-)
*
      Z0(1,2)=-(0.0,1.0)*ZQKOD(1,IQ)
      Z0(2,2)=-(0.0,1.0)*ZQKOD(2,IQ)
      Z0(1,1)=-(0.0,1.0)*ZQKO(2,IQ)
      Z0(2,1)=+(0.0,1.0)*ZQKO(1,IQ)
*
      IF (N.EQ.0) GOTO 999
*
* If N>0 then calculate all Z1-spinors.
*
      DO 10 J=1,N
        CALL QCURG0(Z0(1,1),ZJ1(1,1,J),Z1(1,1,J))
        IF ((N.NE.1).OR.(ILAST.EQ.1))  CALL QCURP0(Z1(1,1,J),
     .              ZQP1(1,1,IQ,J),PQP1(IQ,J),Z1(1,1,J))
   10 CONTINUE
*
      IF (N.EQ.1) GOTO 999
*
* If N>1 then calculate all Z2-spinors.
*
      DO 20 J1=1,N-1
        DO 20 J2=J1+1,N
          INDEX=M2H(J1,J2)
          CALL QCURG0(Z0(1,1),ZJ2(1,1,INDEX),ZH1(1,1))
          CALL QCURG0(Z1(1,1,J1),ZJ1(1,1,J2),Z2(1,1,INDEX))
          CALL ADDZH0(Z2(1,1,INDEX),ZH1)
          IF ((N.NE.2).OR.(ILAST.EQ.1)) CALL QCURP0(Z2(1,1,INDEX),
     .        ZQP2(1,1,IQ,INDEX),PQP2(IQ,INDEX),Z2(1,1,INDEX))
          INDEX=M2H(J2,J1)
          CALL QCURG0(Z1(1,1,J2),ZJ1(1,1,J1),Z2(1,1,INDEX))
          CALL SUBZH0(Z2(1,1,INDEX),ZH1)
          IF ((N.NE.2).OR.(ILAST.EQ.1)) CALL QCURP0(Z2(1,1,INDEX),
     .        ZQP2(1,1,IQ,INDEX),PQP2(IQ,INDEX),Z2(1,1,INDEX))
   20 CONTINUE
*
      IF (N.EQ.2) GOTO 999
*
* If N>2 then obtain all Z3-spinors.
*
      DO 30 J1=1,N-2
       DO 30 J2=J1+1,N-1
        DO 30 J3=J2+1,N
* 1, 2, 3
          INDEX=M3H(J1,J2,J3)
          CALL QCURG0(Z0(1,1),ZJ3(1,1,INDEX),ZH1(1,1))
          CALL QCURG0(Z1(1,1,J1),ZJ2(1,1,M2H(J2,J3)),ZH2(1,1))
          CALL QCURG0(Z2(1,1,M2H(J1,J2)),ZJ1(1,1,J3),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH2)
          CALL ADDZH0(Z3(1,1,INDEX),ZH1)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP0(Z3(1,1,INDEX),
     .        ZQP3(1,1,IQ,INDEX),PQP3(IQ,INDEX),Z3(1,1,INDEX))
* 1, 3, 2
          INDEX=M3H(J1,J3,J2)
          CALL QCURG0(Z0(1,1),ZJ3(1,1,INDEX),ZH4(1,1))
          CALL QCURG0(Z2(1,1,M2H(J1,J3)),ZJ1(1,1,J2),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH4)
          CALL SUBZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),PQP3(IQ,INDEX),Z3(1,1,INDEX))
* 2, 1, 3
          INDEX=M3H(J2,J1,J3)
          CALL QCURG0(Z0(1,1),ZJ3(1,1,INDEX),ZH5(1,1))
          CALL QCURG0(Z1(1,1,J2),ZJ2(1,1,M2H(J1,J3)),ZH2(1,1))
          CALL QCURG0(Z2(1,1,M2H(J2,J1)),ZJ1(1,1,J3),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH5)
          CALL ADDZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),PQP3(IQ,INDEX),Z3(1,1,INDEX))
* 2, 3, 1
          INDEX=M3H(J2,J3,J1)
          CALL QCURG0(Z2(1,1,M2H(J2,J3)),ZJ1(1,1,J1),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH4)
          CALL SUBZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),PQP3(IQ,INDEX),Z3(1,1,INDEX))
* 3, 1, 2
          INDEX=M3H(J3,J1,J2)
          CALL QCURG0(Z1(1,1,J3),ZJ2(1,1,M2H(J1,J2)),ZH2(1,1))
          CALL QCURG0(Z2(1,1,M2H(J3,J1)),ZJ1(1,1,J2),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH5)
          CALL ADDZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),PQP3(IQ,INDEX),Z3(1,1,INDEX))
* 3, 2, 1
          INDEX=M3H(J3,J2,J1)
          CALL QCURG0(Z2(1,1,M2H(J3,J2)),ZJ1(1,1,J1),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH1)
          CALL SUBZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),PQP3(IQ,INDEX),Z3(1,1,INDEX))
   30 CONTINUE
*
      IF (N.EQ.3) GOTO 999
*
* If N>3 then obtain all Z4-spinors.
*
      DO 40 J=1,IFAC(N)
        J1=IPERM(1,J)
        J2=IPERM(2,J)
        J3=IPERM(3,J)
        J4=IPERM(4,J)
        INDEX=M4H(J1,J2,J3,J4)
        CALL QCURG0(Z0(1,1),ZJ4(1,1,INDEX),ZH1(1,1))
        CALL QCURG0(Z1(1,1,J1),ZJ3(1,1,M3H(J2,J3,J4)),ZH2(1,1))
        CALL QCURG0(Z2(1,1,M2H(J1,J2)),ZJ2(1,1,M2H(J3,J4)),ZH3(1,1))
        CALL QCURG0(Z3(1,1,M3H(J1,J2,J3)),ZJ1(1,1,J4),Z4(1,1,INDEX))
        CALL ADDZH0(Z4(1,1,INDEX),ZH1)
        CALL ADDZH0(Z4(1,1,INDEX),ZH2)
        CALL ADDZH0(Z4(1,1,INDEX),ZH3)
        IF ((N.NE.4).OR.(ILAST.EQ.1)) CALL QCURP0(Z4(1,1,INDEX),
     .     ZQP4(1,1,IQ,INDEX),PQP4(IQ,INDEX),Z4(1,1,INDEX))
   40 CONTINUE
*
      IF (N.EQ.4) GOTO 999
*
* If N>4 then obtain all Z5-spinors. NO propagator for the N=5 case.
*
      DO 50 J=1,IFAC(N)
        J1=IPERM(1,J)
        J2=IPERM(2,J)
        J3=IPERM(3,J)
        J4=IPERM(4,J)
        J5=IPERM(5,J)
        INDEX=M5H(J1,J2,J3,J4,J5)
        CALL QCURG0(Z0(1,1),ZJ5(1,1,INDEX),Z5(1,1,INDEX))
        CALL QCURG0(Z1(1,1,J1),ZJ4(1,1,M4H(J2,J3,J4,J5)),ZH1(1,1))
        CALL QCURG0(Z2(1,1,M2H(J1,J2)),ZJ3(1,1,M3H(J3,J4,J5)),
     .                                                      ZH2(1,1))
        CALL QCURG0(Z3(1,1,M3H(J1,J2,J3)),ZJ2(1,1,M2H(J4,J5)),
     .                                                      ZH3(1,1))
        CALL QCURG0(Z4(1,1,M4H(J1,J2,J3,J4)),ZJ1(1,1,J5),ZH4(1,1))
        CALL ADDZH0(Z5(1,1,INDEX),ZH1)
        CALL ADDZH0(Z5(1,1,INDEX),ZH2)
        CALL ADDZH0(Z5(1,1,INDEX),ZH3)
        CALL ADDZH0(Z5(1,1,INDEX),ZH4)
        IF ((N.NE.5).OR.(ILAST.EQ.1)) CALL QCURP0(Z5(1,1,INDEX),
     .     ZQP5(1,1,IQ,INDEX),PQP5(IQ,INDEX),Z5(1,1,INDEX))
   50 CONTINUE
*
  999 CONTINUE
*
      END
*
************************************************************************
* QCURG0 calculates product of a quark spinor and a gluon current.     *
************************************************************************
      SUBROUTINE QCURG0(ZJ,ZG,ZANS)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters
*
      DIMENSION ZJ(2,2),ZG(2,2),ZANS(2,2)
*
* spinor current contracted with contra-variant helicity spinor tensor
*
      ZANS(1,1)=-ZJ(1,2)*ZG(2,2)+ZJ(2,2)*ZG(1,2)
      ZANS(2,1)=+ZJ(1,2)*ZG(2,1)-ZJ(2,2)*ZG(1,1)
      ZANS(1,2)=+ZJ(1,1)*ZG(1,1)+ZJ(2,1)*ZG(1,2)
      ZANS(2,2)=+ZJ(1,1)*ZG(2,1)+ZJ(2,1)*ZG(2,2)
*
      END
*
************************************************************************
* QCURP0 calculates product of quark spinor with the propagator.       *
************************************************************************
      SUBROUTINE QCURP0(ZJ,ZP,PROP,ZANS)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters,
*
      DIMENSION ZJ(2,2),ZP(2,2),ZANS(2,2)
*
* Spinor current contracted with propagator spinor tensor,
* include (0.0,1.0) for propagator.
* Note: ZJ and ZANS can be the same objects!
*
      Z1=( - ZJ(1,2)*ZP(2,2) + ZJ(2,2)*ZP(1,2) )/PROP
      Z2=( + ZJ(1,2)*ZP(2,1) - ZJ(2,2)*ZP(1,1) )/PROP
      Z3=( + ZJ(1,1)*ZP(1,1) + ZJ(2,1)*ZP(1,2) )/PROP
      Z4=( + ZJ(1,1)*ZP(2,1) + ZJ(2,1)*ZP(2,2) )/PROP
*
      ZANS(1,1)=Z1
      ZANS(2,1)=Z2
      ZANS(1,2)=Z3
      ZANS(2,2)=Z4
*
      END
*
* Order in Zi's of gluons is such that far right is gluon close to
* the anti-quark
*
      SUBROUTINE QBCUR0(IQB,N,ILAST,Z0,Z1,Z2,Z3,Z4,Z5)
      PARAMETER(NUP=5,IQUP=4,NUPM1=5,NUPM2=20,NUPM3=60,
     .          NUPM4=120,NUPM5=120,NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters
*
      DIMENSION Z0(2,2),Z1(2,2,*),Z2(2,2,*),
     .          Z3(2,2,*),Z4(2,2,*),Z5(2,2,*)
*
* Global variables
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /SPINQ/ ZQKO(2,IQUP),ZQKOD(2,IQUP),
     .               ZQMO(2,IQUP),ZQMOD(2,IQUP)
      COMMON /FERMPR/ ZQP0(2,2,IQUP),ZQP1(2,2,IQUP,NUP),
     .                ZQP2(2,2,IQUP,NUPM2),ZQP3(2,2,IQUP,NUPM3),
     .                ZQP4(2,2,IQUP,NUPM4),ZQP5(2,2,IQUP,NUPM5),
     .                PQP1(IQUP,NUP),PQP2(IQUP,NUPM2),PQP3(IQUP,NUPM3),
     .                PQP4(IQUP,NUPM4),PQP5(IQUP,NUPM5)
*
* Local variables
*
      DIMENSION ZH1(2,2),ZH2(2,2),ZH3(2,2),ZH4(2,2),ZH5(2,2)
      DIMENSION IPERM(NUP,NUPFAC)
      SAVE /MAPPIN/,/GLUCUR/,/SPINQ/,/FERMPR/,IPERM,INIT
      DATA INIT /1/
*
      IF (INIT.EQ.1) THEN
        INIT=0
        DO 5 I=1,IFAC(N)
          CALL INVID(I,IPERM(1,I),N)
    5   CONTINUE
      END IF
*
* Init the currents of length 0, V(+) and V(-)
*
      Z0(1,2)=-ZQKOD(2,IQB)
      Z0(2,2)=+ZQKOD(1,IQB)
      Z0(1,1)=+ZQKO(1,IQB)
      Z0(2,1)=+ZQKO(2,IQB)
*
      IF (N.EQ.0) GOTO 999
*
* If N>0 then calculate all Z1-spinors.
*
      DO 10 J=1,N
        CALL QBCUG0(Z0(1,1),ZJ1(1,1,J),Z1(1,1,J))
        IF ((N.NE.1).OR.(ILAST.EQ.1))  CALL QBCUP0(Z1(1,1,J),
     .              ZQP1(1,1,IQB,J),PQP1(IQB,J),Z1(1,1,J))
   10 CONTINUE
*
      IF (N.EQ.1) GOTO 999
*
* If N>1 then calculate all Z2-spinors.
*
      DO 20 J1=1,N-1
        DO 20 J2=J1+1,N
          INDEX=M2H(J2,J1)
          CALL QBCUG0(Z0(1,1),ZJ2(1,1,INDEX),ZH1(1,1))
          CALL QBCUG0(Z1(1,1,J1),ZJ1(1,1,J2),Z2(1,1,INDEX))
          CALL ADDZH0(Z2(1,1,INDEX),ZH1)
          IF ((N.NE.2).OR.(ILAST.EQ.1)) CALL QBCUP0(Z2(1,1,INDEX),
     .       ZQP2(1,1,IQB,INDEX),PQP2(IQB,INDEX),Z2(1,1,INDEX))
          INDEX=M2H(J1,J2)
          CALL QBCUG0(Z1(1,1,J2),ZJ1(1,1,J1),Z2(1,1,INDEX))
          CALL SUBZH0(Z2(1,1,INDEX),ZH1)
          IF ((N.NE.2).OR.(ILAST.EQ.1)) CALL QBCUP0(Z2(1,1,INDEX),
     .       ZQP2(1,1,IQB,INDEX),PQP2(IQB,INDEX),Z2(1,1,INDEX))
   20 CONTINUE
*
      IF (N.EQ.2) GOTO 999
*
* If N>2 then obtain all Z3-spinors.
*
      DO 30 J1=1,N-2
       DO 30 J2=J1+1,N-1
        DO 30 J3=J2+1,N
* 3, 2, 1
          INDEX=M3H(J3,J2,J1)
          CALL QBCUG0(Z0(1,1),ZJ3(1,1,INDEX),ZH1(1,1))
          CALL QBCUG0(Z1(1,1,J1),ZJ2(1,1,M2H(J3,J2)),ZH2(1,1))
          CALL QBCUG0(Z2(1,1,M2H(J2,J1)),ZJ1(1,1,J3),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH2)
          CALL ADDZH0(Z3(1,1,INDEX),ZH1)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCUP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),PQP3(IQB,INDEX),Z3(1,1,INDEX))
* 2, 3, 1
          INDEX=M3H(J2,J3,J1)
          CALL QBCUG0(Z0(1,1),ZJ3(1,1,INDEX),ZH4(1,1))
          CALL QBCUG0(Z2(1,1,M2H(J3,J1)),ZJ1(1,1,J2),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH4)
          CALL SUBZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCUP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),PQP3(IQB,INDEX),Z3(1,1,INDEX))
* 3, 1, 2
          INDEX=M3H(J3,J1,J2)
          CALL QBCUG0(Z0(1,1),ZJ3(1,1,INDEX),ZH5(1,1))
          CALL QBCUG0(Z1(1,1,J2),ZJ2(1,1,M2H(J3,J1)),ZH2(1,1))
          CALL QBCUG0(Z2(1,1,M2H(J1,J2)),ZJ1(1,1,J3),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH5)
          CALL ADDZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCUP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),PQP3(IQB,INDEX),Z3(1,1,INDEX))
* 1, 3, 2
          INDEX=M3H(J1,J3,J2)
          CALL QBCUG0(Z2(1,1,M2H(J3,J2)),ZJ1(1,1,J1),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH4)
          CALL SUBZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCUP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),PQP3(IQB,INDEX),Z3(1,1,INDEX))
* 2, 1, 3
          INDEX=M3H(J2,J1,J3)
          CALL QBCUG0(Z1(1,1,J3),ZJ2(1,1,M2H(J2,J1)),ZH2(1,1))
          CALL QBCUG0(Z2(1,1,M2H(J1,J3)),ZJ1(1,1,J2),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH5)
          CALL ADDZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCUP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),PQP3(IQB,INDEX),Z3(1,1,INDEX))
* 1, 2, 3
          INDEX=M3H(J1,J2,J3)
          CALL QBCUG0(Z2(1,1,M2H(J2,J3)),ZJ1(1,1,J1),Z3(1,1,INDEX))
          CALL ADDZH0(Z3(1,1,INDEX),ZH1)
          CALL SUBZH0(Z3(1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCUP0(Z3(1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),PQP3(IQB,INDEX),Z3(1,1,INDEX))
   30 CONTINUE
*
      IF (N.EQ.3) GOTO 999
*
* If N>3 then obtain all Z4-spinors.
*
      DO 40 J=1,IFAC(N)
        J1=IPERM(1,J)
        J2=IPERM(2,J)
        J3=IPERM(3,J)
        J4=IPERM(4,J)
        INDEX=M4H(J4,J3,J2,J1)
        CALL QBCUG0(Z0(1,1),ZJ4(1,1,INDEX),ZH1(1,1))
        CALL QBCUG0(Z1(1,1,J1),ZJ3(1,1,M3H(J4,J3,J2)),ZH2(1,1))
        CALL QBCUG0(Z2(1,1,M2H(J2,J1)),ZJ2(1,1,M2H(J4,J3)),ZH3(1,1))
        CALL QBCUG0(Z3(1,1,M3H(J3,J2,J1)),ZJ1(1,1,J4),Z4(1,1,INDEX))
        CALL ADDZH0(Z4(1,1,INDEX),ZH1)
        CALL ADDZH0(Z4(1,1,INDEX),ZH2)
        CALL ADDZH0(Z4(1,1,INDEX),ZH3)
        IF ((N.NE.4).OR.(ILAST.EQ.1)) CALL QBCUP0(Z4(1,1,INDEX),
     .     ZQP4(1,1,IQB,INDEX),PQP4(IQB,INDEX),Z4(1,1,INDEX))
   40 CONTINUE
*
      IF (N.EQ.4) GOTO 999
*
* If N>4 then obtain all Z5-spinors. NO propagator for the N=5 case.
*
      DO 50 J=1,IFAC(N)
        J1=IPERM(1,J)
        J2=IPERM(2,J)
        J3=IPERM(3,J)
        J4=IPERM(4,J)
        J5=IPERM(5,J)
        INDEX=M5H(J5,J4,J3,J2,J1)
        CALL QBCUG0(Z0(1,1),ZJ5(1,1,INDEX),Z5(1,1,INDEX))
        CALL QBCUG0(Z1(1,1,J1),ZJ4(1,1,M4H(J5,J4,J3,J2)),ZH2(1,1))
        CALL QBCUG0(Z2(1,1,M2H(J2,J1)),ZJ3(1,1,M3H(J5,J4,J3)),
     .                                                     ZH3(1,1))
        CALL QBCUG0(Z3(1,1,M3H(J3,J2,J1)),ZJ2(1,1,M2H(J5,J4)),
     .                                                     ZH4(1,1))
        CALL QBCUG0(Z4(1,1,M4H(J4,J3,J2,J1)),ZJ1(1,1,J5),ZH5(1,1))
        CALL ADDZH0(Z5(1,1,INDEX),ZH2)
        CALL ADDZH0(Z5(1,1,INDEX),ZH3)
        CALL ADDZH0(Z5(1,1,INDEX),ZH4)
        CALL ADDZH0(Z5(1,1,INDEX),ZH5)
        IF ((N.NE.5).OR.(ILAST.EQ.1)) CALL QBCUP0(Z5(1,1,INDEX),
     .     ZQP5(1,1,IQB,INDEX),PQP5(IQB,INDEX),Z5(1,1,INDEX))
   50 CONTINUE
*
  999 CONTINUE
      END
*
************************************************************************
* QBCUG0 calculates product of a antiquark spinor and a gluon current. *
************************************************************************
      SUBROUTINE QBCUG0(ZJ,ZG,ZANS)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters
*
      DIMENSION ZJ(2,2),ZG(2,2),ZANS(2,2)
*
* spinor current contracted with contra-variant helicity spinor tensor
*
      ZANS(1,1)=+ZJ(1,2)*ZG(1,1)+ZJ(2,2)*ZG(2,1)
      ZANS(2,1)=+ZJ(1,2)*ZG(1,2)+ZJ(2,2)*ZG(2,2)
      ZANS(1,2)=-ZJ(1,1)*ZG(2,2)+ZJ(2,1)*ZG(2,1)
      ZANS(2,2)=+ZJ(1,1)*ZG(1,2)-ZJ(2,1)*ZG(1,1)
*
      END
*
************************************************************************
* QBCUP0 calculates product of antiquark spinor with the propagator.   *
************************************************************************
      SUBROUTINE QBCUP0(ZJ,ZP,PROP,ZANS)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters,
*
      DIMENSION ZJ(2,2),ZP(2,2),ZANS(2,2)
*
* Spinor current contracted with propagator spinor tensor,
* include (0.0,-1.0) for propagator, massterm extra minus in numerator.
* Note: ZJ and ZANS can be the same objects!
*
      Z1=( - ZJ(1,2)*ZP(1,1) - ZJ(2,2)*ZP(2,1) )/PROP
      Z2=( - ZJ(1,2)*ZP(1,2) - ZJ(2,2)*ZP(2,2) )/PROP
      Z3=( + ZJ(1,1)*ZP(2,2) - ZJ(2,1)*ZP(2,1) )/PROP
      Z4=( - ZJ(1,1)*ZP(1,2) + ZJ(2,1)*ZP(1,1) )/PROP
*
      ZANS(1,1)=Z1
      ZANS(2,1)=Z2
      ZANS(1,2)=Z3
      ZANS(2,2)=Z4
*
      END
*
************************************************************************
* ADDZH0 adds two quark spinors. (ZH1=ZH1+ZH2)                         *
************************************************************************
      SUBROUTINE ADDZH0(ZH1,ZH2)
      COMPLEX ZH1(2,2),ZH2(2,2)
      DO 10 I=1,2
        DO 10 J=1,2
          ZH1(I,J)=ZH1(I,J)+ZH2(I,J)
   10 CONTINUE
      END
*
************************************************************************
* SUBZH0 subtracts two quark spinors. (ZH1=ZH1-ZH2)                    *
************************************************************************
      SUBROUTINE SUBZH0(ZH1,ZH2)
      COMPLEX ZH1(2,2),ZH2(2,2)
      DO 10 I=1,2
        DO 10 J=1,2
          ZH1(I,J)=ZH1(I,J)-ZH2(I,J)
   10 CONTINUE
      END
*
************************************************************************
* CURS calculates gluonic currents with possibly off shell particles   *
*                                                                      *
* In:   Set of N helicity vectors, helicities are implicit             *
*       ILAST=1 means to include last propagator                       *
*       Limitation N<=5                                                *
* Out:  All gluonic currents in spinor language                        *
************************************************************************
      SUBROUTINE CURS(N,ILAST,ZH,IOFFSH)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,
     .          NUPM5=120,NUPMFA=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters
*
      DIMENSION ZH(2,2,*)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
*
* local variables CURINT = common with internal variables
*
      COMMON /CURINT/ ZJ1J1(NUP,NUP),ZJ1J2(NUP,NUP,NUP),
     .   ZJ2J2(NUP,NUP,NUP,NUP),IPERM(NUP,NUPMFA)
      SAVE /MAPPIN/,/GLUCUR/,/GLUOPR/,/CURINT/,INIT5
      DATA INIT5 /1/
*
      IF (N.EQ.0) GOTO 999
*
* Calculate the 1-currents, copy vectors.
*
      CALL COPYTS(ZJ1,ZH,N)
*
      IF (N.EQ.1) GOTO 999
*
* Calculate the 2-currents and all JiDJj products
*
      DO 20 I1=1,N-1
       DO 20 I2=I1+1,N
         Z1=0.5*ZMUL(ZJ1(1,1,I1),ZJ1(1,1,I2))
         ZJ1J1(I1,I2)=Z1
         ZJ1J1(I2,I1)=Z1
         Z2=+ZMUL(ZP1(1,1,I2),ZJ1(1,1,I1))
         Z3=-ZMUL(ZP1(1,1,I1),ZJ1(1,1,I2))
         IF (IOFFSH.EQ.1) THEN
           Z2=Z2+0.5*ZMUL(ZP1(1,1,I1),ZJ1(1,1,I1))
           Z3=Z3-0.5*ZMUL(ZP1(1,1,I2),ZJ1(1,1,I2))
         END IF
         PROP=1.0
         IF( (N.NE.2).OR.(ILAST.EQ.1) ) PROP=PP2(M2H(I1,I2))
         DO 30 IAD=1,2
          DO 30 IB=1,2
            ZZ1=(+Z2*ZJ1(IAD,IB,I2)+Z3*ZJ1(IAD,IB,I1)
     .           +Z1*(ZP1(IAD,IB,I1)-ZP1(IAD,IB,I2)))/PROP
            ZJ2(IAD,IB,M2H(I1,I2))=ZZ1
            ZJ2(IAD,IB,M2H(I2,I1))=-ZZ1
   30    CONTINUE
   20 CONTINUE
*
      IF (N.EQ.2) GOTO 999
*
* Calculation of all J1DJ2 products
*
      DO 40 I1=1,N
       DO 40 I2=1,N-1
        DO 40 I3=I2+1,N
          ZZ1=0.5*ZMUL(ZJ1(1,1,I1),ZJ2(1,1,M2H(I2,I3)))
          ZJ1J2(I1,I2,I3)=ZZ1
          ZJ1J2(I1,I3,I2)=-ZZ1
   40 CONTINUE
*
* Calculation of the 3-currents
*
      DO 50 I1=1,N-2
       DO 50 I2=I1+1,N-1
        DO 50 I3=I2+1,N
          Z1=ZJ1J2(I1,I2,I3)
          Z2=ZMUL(ZP2(1,1,M2H(I2,I3)),ZJ1(1,1,I1))
          Z3=-ZMUL(ZP1(1,1,I1),ZJ2(1,1,M2H(I2,I3)))
          Z4=ZJ1J2(I3,I1,I2)
          Z5=+ZMUL(ZP1(1,1,I3),ZJ2(1,1,M2H(I1,I2)))
          Z6=-ZMUL(ZP2(1,1,M2H(I1,I2)),ZJ1(1,1,I3))
          Z7=ZJ1J2(I2,I1,I3)
          Z8=+ZMUL(ZP1(1,1,I2),ZJ2(1,1,M2H(I1,I3)))
          Z9=-ZMUL(ZP2(1,1,M2H(I1,I3)),ZJ1(1,1,I2))
          IF (IOFFSH.EQ.1) THEN
            Z2=Z2+0.5*ZMUL(ZP1(1,1,I1),ZJ1(1,1,I1))
            Z3=Z3-0.5*ZMUL(ZP2(1,1,M2H(I2,I3)),ZJ2(1,1,M2H(I2,I3)))
            Z5=Z5+0.5*ZMUL(ZP2(1,1,M2H(I1,I2)),ZJ2(1,1,M2H(I1,I2)))
            Z6=Z6-0.5*ZMUL(ZP1(1,1,I3),ZJ1(1,1,I3))
            Z8=Z8+0.5*ZMUL(ZP2(1,1,M2H(I1,I3)),ZJ2(1,1,M2H(I1,I3)))
            Z9=Z9-0.5*ZMUL(ZP1(1,1,I2),ZJ1(1,1,I2))
          END IF
          PROP=1.0
          IF( (N.NE.3).OR.(ILAST.EQ.1) ) PROP=PP3(M3H(I1,I2,I3))
          DO 60 IAD=1,2
           DO 60 IB=1,2
             ZHELP= +Z2*ZJ2(IAD,IB,M2H(I2,I3))+Z3*ZJ1(IAD,IB,I1)
     .              +(ZP1(IAD,IB,I1)-ZP2(IAD,IB,M2H(I2,I3)))*Z1
             ZZ1=(  +ZHELP
     .              +Z5*ZJ1(IAD,IB,I3)+Z6*ZJ2(IAD,IB,M2H(I1,I2))
     .              +(ZP2(IAD,IB,M2H(I1,I2))-ZP1(IAD,IB,I3))*Z4
     .              +2.0*ZJ1J1(I1,I3)*ZJ1(IAD,IB,I2)
     .              -ZJ1J1(I1,I2)*ZJ1(IAD,IB,I3)
     .              -ZJ1J1(I2,I3)*ZJ1(IAD,IB,I1)    )/PROP
             ZZ2=(  -ZHELP
     .              +Z8*ZJ1(IAD,IB,I2)+Z9*ZJ2(IAD,IB,M2H(I1,I3))
     .              +(ZP2(IAD,IB,M2H(I1,I3))-ZP1(IAD,IB,I2))*Z7
     .              +2.0*ZJ1J1(I1,I2)*ZJ1(IAD,IB,I3)
     .              -ZJ1J1(I1,I3)*ZJ1(IAD,IB,I2)
     .              -ZJ1J1(I3,I2)*ZJ1(IAD,IB,I1)    )/PROP
            ZJ3(IAD,IB,M3H(I1,I2,I3))=ZZ1
            ZJ3(IAD,IB,M3H(I3,I2,I1))=ZZ1
            ZJ3(IAD,IB,M3H(I1,I3,I2))=ZZ2
            ZJ3(IAD,IB,M3H(I2,I3,I1))=ZZ2
            ZJ3(IAD,IB,M3H(I2,I1,I3))=-ZZ1-ZZ2
            ZJ3(IAD,IB,M3H(I3,I1,I2))=-ZZ1-ZZ2
   60     CONTINUE
   50 CONTINUE
*
      IF (N.EQ.3) GOTO 999
*
* Calculation of 4-currents
*
      DO 70 I1=1,N-3
       DO 70 I2=I1+1,N-2
        DO 70 I3=I2+1,N-1
         DO 70 I4=I3+1,N
           PROP=0.0
           IF( (N.NE.4).OR.(ILAST.EQ.1)) PROP=PP4(M4H(I1,I2,I3,I4))
           CALL CUR4(I1,I2,I3,I4,PROP,IOFFSH)
           CALL CUR4(I1,I2,I4,I3,PROP,IOFFSH)
           CALL CUR4(I1,I3,I4,I2,PROP,IOFFSH)
           CALL CUR4(I1,I3,I2,I4,PROP,IOFFSH)
           CALL CUR4(I1,I4,I2,I3,PROP,IOFFSH)
           CALL CUR4(I1,I4,I3,I2,PROP,IOFFSH)
           CALL FULLZ4(I1,I2,I3,I4)
   70 CONTINUE
*
      IF (N.EQ.4) GOTO 999
*
* Store permutations of 2..NUP in IPERM for two reasons:
* 1. calling CUR5         2. unwrapping base of ZJ5's to full set
*
      IF (INIT5.EQ.1) THEN
        INIT5=0
        DO 80 I=1,IFAC(N-1)
          CALL INVID(I,IPERM(1,I),N)
   80   CONTINUE
      END IF
*
* Calculate all J2DJ2 products.
*
      DO 90 I1=1,N
       DO 90 I2=I1+1,N
        DO 90 I3=1,N
         DO 90 I4=I3+1,N
           IF ( (I1.NE.I3).AND.(I1.NE.I4).AND.
     .          (I2.NE.I3).AND.(I2.NE.I4) ) THEN
             ZZ2=0.5*ZMUL(ZJ2(1,1,M2H(I1,I2)),ZJ2(1,1,M2H(I3,I4)))
             ZJ2J2(I1,I2,I3,I4)=ZZ2
             ZJ2J2(I1,I2,I4,I3)=-ZZ2
             ZJ2J2(I2,I1,I3,I4)=-ZZ2
             ZJ2J2(I2,I1,I4,I3)=ZZ2
             ZJ2J2(I3,I4,I1,I2)=ZZ2
             ZJ2J2(I4,I3,I1,I2)=-ZZ2
             ZJ2J2(I3,I4,I2,I1)=-ZZ2
             ZJ2J2(I4,I3,I2,I1)=ZZ2
           END IF
   90 CONTINUE
*
* Calculation of 5-currents
*
      PROP=1.0
      IF( ILAST.EQ.1 )
     .  PROP=REAL( +ZMUL(ZP1(1,1,1),ZP4(1,1,M4H(2,3,4,5)))
     .         +0.5*ZMUL(ZP1(1,1,1),ZP1(1,1,1))
     .         +0.5*ZMUL(ZP4(1,1,M4H(2,3,4,5)),ZP4(1,1,M4H(2,3,4,5)))  )
      DO 100 I=1,IFAC(N-1)
        CALL CUR5(IPERM(1,I),IPERM(2,I),IPERM(3,I),
     .            IPERM(4,I),IPERM(5,I),PROP,IOFFSH)
  100 CONTINUE
      CALL FULLZ5(1,2,3,4,5)
*
  999 CONTINUE
      END
*
************************************************************************
* CUR4(I1,I2,I3,I4,PROP) returns the sum over the (curly) brackets     *
* terms which appear in the gluonic recursion relation for 4 gluons.   *
*                                                                      *
*   In:  I1,I2,I3,I4 gluon identifiers, PROP propagator of current.    *
*   Out: ZJ4(2,2,I1,I2,I3,I4) current.                                 *
************************************************************************
      SUBROUTINE CUR4(I1,I2,I3,I4,PROP,IOFFSH)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,
     .          NUPM5=120,NUPMFA=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /CURINT/ ZJ1J1(NUP,NUP),ZJ1J2(NUP,NUP,NUP),
     .                ZJ2J2(NUP,NUP,NUP,NUP),IPERM(NUP,NUPMFA)
*
* local variables
*
      DIMENSION ZT1(2,2),ZT2(2,2),ZT3(2,2),ZT4(2,2)
      SAVE /MAPPIN/,/GLUCUR/,/GLUOPR/,/CURINT/
*
* Square bracket terms
*
      CALL BRAC(ZJ1(1,1,I1),ZP1(1,1,I1),ZJ3(1,1,M3H(I2,I3,I4)),
     .          ZP3(1,1,M3H(I2,I3,I4)),ZT1,IOFFSH)
      CALL BRAC(ZJ2(1,1,M2H(I1,I2)),ZP2(1,1,M2H(I1,I2)),
     .          ZJ2(1,1,M2H(I3,I4)),ZP2(1,1,M2H(I3,I4)),ZT2,IOFFSH)
      CALL BRAC(ZJ3(1,1,M3H(I1,I2,I3)),ZP3(1,1,M3H(I1,I2,I3)),
     .          ZJ1(1,1,I4),ZP1(1,1,I4),ZT3,IOFFSH)
*
* Contribution of curly bracket terms
*
      Z1=+2.0*ZJ1J2(I1,I3,I4)
      Z2=-ZJ1J1(I1,I2)
      Z3=-(ZJ1J2(I2,I3,I4)+ZJ1J2(I4,I2,I3))
      Z4=+2.0*ZJ1J1(I1,I4)
      Z5=-(ZJ1J2(I1,I2,I3)+ZJ1J2(I3,I1,I2))
      Z6=+2.0*ZJ1J2(I4,I1,I2)
      Z7=-ZJ1J1(I3,I4)
      DO 10 IAD=1,2
       DO 10 IB=1,2
         ZT4(IAD,IB)=+Z1*ZJ1(IAD,IB,I2)   +Z2*ZJ2(IAD,IB,M2H(I3,I4))
     .               +Z3*ZJ1(IAD,IB,I1)   +Z4*ZJ2(IAD,IB,M2H(I2,I3))
     .               +Z5*ZJ1(IAD,IB,I4)   +Z6*ZJ1(IAD,IB,I3)
     .               +Z7*ZJ2(IAD,IB,M2H(I1,I2))
         ZJ4(IAD,IB,M4H(I1,I2,I3,I4))=(ZT1(IAD,IB)+ZT2(IAD,IB)
     .               +ZT3(IAD,IB)+ZT4(IAD,IB))/PROP
         ZJ4(IAD,IB,M4H(I4,I3,I2,I1))=-ZJ4(IAD,IB,M4H(I1,I2,I3,I4))
   10 CONTINUE
*
      END
*
************************************************************************
* FULLZ4(I1,I2,I3,I4) build up all 4-currents from a reduced set       *
*                     where (I1<I2,I3,I4).                             *
*                                                                      *
*   In:  I1,I2,I3,I4 gluon identifiers.                                *
*   Out: All ZJ4(2,2,P(I1,I2,I3,I4)) currents.                         *
************************************************************************
      SUBROUTINE FULLZ4(I1,I2,I3,I4)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      SAVE /MAPPIN/,/GLUCUR/
*
      DO 10 IAD=1,2
       DO 10 IB=1,2
         Z1234=ZJ4(IAD,IB,M4H(I1,I2,I3,I4))
         Z1243=ZJ4(IAD,IB,M4H(I1,I2,I4,I3))
         Z1324=ZJ4(IAD,IB,M4H(I1,I3,I2,I4))
         Z1342=ZJ4(IAD,IB,M4H(I1,I3,I4,I2))
         Z1423=ZJ4(IAD,IB,M4H(I1,I4,I2,I3))
         Z1432=ZJ4(IAD,IB,M4H(I1,I4,I3,I2))
         Z2134=-Z1234-Z1324-Z1342
         Z2143=-Z1243-Z1423-Z1432
         Z3124=-Z1324-Z1234-Z1243
         Z3142=-Z1342-Z1432-Z1423
         Z4123=-Z1423-Z1243-Z1234
         Z4132=-Z1432-Z1342-Z1324
         ZJ4(IAD,IB,M4H(I2,I1,I3,I4))=Z2134
         ZJ4(IAD,IB,M4H(I2,I1,I4,I3))=Z2143
         ZJ4(IAD,IB,M4H(I3,I1,I2,I4))=Z3124
         ZJ4(IAD,IB,M4H(I3,I1,I4,I2))=Z3142
         ZJ4(IAD,IB,M4H(I4,I1,I2,I3))=Z4123
         ZJ4(IAD,IB,M4H(I4,I1,I3,I2))=Z4132
         ZJ4(IAD,IB,M4H(I4,I3,I1,I2))=-Z2134
         ZJ4(IAD,IB,M4H(I3,I4,I1,I2))=-Z2143
         ZJ4(IAD,IB,M4H(I4,I2,I1,I3))=-Z3124
         ZJ4(IAD,IB,M4H(I2,I4,I1,I3))=-Z3142
         ZJ4(IAD,IB,M4H(I3,I2,I1,I4))=-Z4123
         ZJ4(IAD,IB,M4H(I2,I3,I1,I4))=-Z4132
   10 CONTINUE
*
      END
*
************************************************************************
* CUR5(I1,I2,I3,I4,I5,PROP) returns the sum over the (curly) brackets  *
* terms which appear in the gluonic recursion relation for 5 gluons    *
*                                                                      *
*   In:  I1,I2,I3,I4,I5 gluon identifiers, PROP= propagator            *
*   Out: ZJ5(2,2,I1,I2,I3,I4) current.                                 *
************************************************************************
      SUBROUTINE CUR5(I1,I2,I3,I4,I5,PROP,IOFFSH)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,
     .          NUPM5=120,NUPMFA=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /CURINT/ ZJ1J1(NUP,NUP),ZJ1J2(NUP,NUP,NUP),
     .   ZJ2J2(NUP,NUP,NUP,NUP),IPERM(NUP,NUPMFA)
*
* local variables
*
      DIMENSION ZT1(2,2),ZT2(2,2),ZT3(2,2),ZT4(2,2),ZT5(2,2)
      SAVE /MAPPIN/,/GLUCUR/,/GLUOPR/,/CURINT/
*
* Square bracket terms
*
      CALL BRAC(ZJ1(1,1,I1),ZP1(1,1,I1),ZJ4(1,1,M4H(I2,I3,I4,I5)),
     .        ZP4(1,1,M4H(I2,I3,I4,I5)),ZT1,IOFFSH)
      CALL BRAC(ZJ2(1,1,M2H(I1,I2)),ZP2(1,1,M2H(I1,I2)),
     .        ZJ3(1,1,M3H(I3,I4,I5)),ZP3(1,1,M3H(I3,I4,I5)),ZT2,IOFFSH)
      CALL BRAC(ZJ3(1,1,M3H(I1,I2,I3)),ZP3(1,1,M3H(I1,I2,I3)),
     .        ZJ2(1,1,M2H(I4,I5)),ZP2(1,1,M2H(I4,I5)),ZT3,IOFFSH)
      CALL BRAC(ZJ4(1,1,M4H(I1,I2,I3,I4)),ZP4(1,1,M4H(I1,I2,I3,I4)),
     .        ZJ1(1,1,I5),ZP1(1,1,I5),ZT4,IOFFSH)
*
* Contribution of curly bracket terms
*
      Z2D345=0.5*ZMUL(ZJ1(1,1,I2),ZJ3(1,1,M3H(I3,I4,I5)))
      Z5D234=0.5*ZMUL(ZJ1(1,1,I5),ZJ3(1,1,M3H(I2,I3,I4)))
      Z1D345=0.5*ZMUL(ZJ1(1,1,I1),ZJ3(1,1,M3H(I3,I4,I5)))
      Z5D123=0.5*ZMUL(ZJ1(1,1,I5),ZJ3(1,1,M3H(I1,I2,I3)))
      Z1D234=0.5*ZMUL(ZJ1(1,1,I1),ZJ3(1,1,M3H(I2,I3,I4)))
      Z4D123=0.5*ZMUL(ZJ1(1,1,I4),ZJ3(1,1,M3H(I1,I2,I3)))
*
      Z1=-(Z2D345+ZJ2J2(I2,I3,I4,I5)+Z5D234)
      Z2=-(ZJ1J2(I3,I4,I5)+ZJ1J2(I5,I3,I4))
      Z3=-ZJ1J1(I4,I5)
      Z4=2.0*Z1D345
      Z5=2.0*ZJ1J2(I1,I4,I5)
      Z6=2.0*ZJ1J1(I1,I5)
      Z7=2.0*ZJ2J2(I1,I2,I4,I5)
      Z8=2.0*ZJ1J2(I5,I1,I2)
      Z9=-ZJ1J1(I1,I2)
      ZA=2.0*Z5D123
      ZB=-(ZJ1J2(I1,I2,I3)+ZJ1J2(I3,I1,I2))
      ZC=-(Z1D234+ZJ2J2(I1,I2,I3,I4)+Z4D123)
      DO 10 IAD=1,2
       DO 10 IB=1,2
         ZT5(IAD,IB)=+Z1*ZJ1(IAD,IB,I1)
     .               +Z2*ZJ2(IAD,IB,M2H(I1,I2))
     .               +Z3*ZJ3(IAD,IB,M3H(I1,I2,I3))
     .               +Z4*ZJ1(IAD,IB,I2)
     .               +Z5*ZJ2(IAD,IB,M2H(I2,I3))
     .               +Z6*ZJ3(IAD,IB,M3H(I2,I3,I4))
     .               +Z7*ZJ1(IAD,IB,I3)
     .               +Z8*ZJ2(IAD,IB,M2H(I3,I4))
     .               +Z9*ZJ3(IAD,IB,M3H(I3,I4,I5))
     .               +ZA*ZJ1(IAD,IB,I4)
     .               +ZB*ZJ2(IAD,IB,M2H(I4,I5))
     .               +ZC*ZJ1(IAD,IB,I5)
   10 CONTINUE
*
* Answer in ZJ5(1,1,I1,I2,I3,I4,5) & ZJ5(1,1,I5,I4,I3,I2,I1)
*
      DO 20 IAD=1,2
       DO 20 IB=1,2
        ZJ5(IAD,IB,M5H(I1,I2,I3,I4,I5))=(ZT1(IAD,IB)+ZT2(IAD,IB)
     .              +ZT3(IAD,IB)+ZT4(IAD,IB)+ZT5(IAD,IB))/PROP
        ZJ5(IAD,IB,M5H(I5,I4,I3,I2,I1))=ZJ5(IAD,IB,M5H(I1,I2,I3,I4,I5))
   20 CONTINUE
*
      END
*
************************************************************************
* FULLZ5(I1,I2,I3,I4,I5) build up all 4-currents from a reduced set    *
*                     where (I1<I2,I3,I4,I5).                          *
*                                                                      *
*   In:  I1,I2,I3,I4,I5 gluon identifiers.                             *
*   Out: All ZJ5(2,2,P(I1,I2,I3,I4,I5)) currents.                      *
************************************************************************
      SUBROUTINE FULLZ5(I1,I2,I3,I4,I5)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,
     .          NUPM5=120,NUPMFA=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /CURINT/ ZJ1J1(NUP,NUP),ZJ1J2(NUP,NUP,NUP),
     .   ZJ2J2(NUP,NUP,NUP,NUP),IPERM(NUP,NUPMFA)
      DIMENSION IP(NUP)
      SAVE /MAPPIN/,/GLUCUR/,/CURINT/
*
      IP(1)=I1
      IP(2)=I2
      IP(3)=I3
      IP(4)=I4
      IP(5)=I5
      DO 20 IAD=1,2
       DO 20 IB=1,2
         DO 30 I=1,24
           J1=IP(IPERM(1,I))
           J2=IP(IPERM(2,I))
           J3=IP(IPERM(3,I))
           J4=IP(IPERM(4,I))
           J5=IP(IPERM(5,I))
           ZJ5(IAD,IB,M5H(J5,J1,J2,J3,J4))=
     .              -ZJ5(IAD,IB,M5H(J1,J2,J3,J4,J5))
     .              -ZJ5(IAD,IB,M5H(J1,J2,J3,J5,J4))
     .              -ZJ5(IAD,IB,M5H(J1,J2,J5,J3,J4))
     .              -ZJ5(IAD,IB,M5H(J1,J5,J2,J3,J4))
           ZJ5(IAD,IB,M5H(J4,J3,J2,J1,J5))=
     .              +ZJ5(IAD,IB,M5H(J5,J1,J2,J3,J4))
   30    CONTINUE
         DO 40 I=1,24
           J1=IP(IPERM(1,I))
           J2=IP(IPERM(2,I))
           J3=IP(IPERM(3,I))
           J4=IP(IPERM(4,I))
           J5=IP(IPERM(5,I))
           ZJ5(IAD,IB,M5H(J2,J3,J1,J4,J5))=
     .              -ZJ5(IAD,IB,M5H(J1,J2,J3,J4,J5))
     .              -ZJ5(IAD,IB,M5H(J2,J1,J3,J4,J5))
     .              -ZJ5(IAD,IB,M5H(J2,J3,J4,J1,J5))
     .              -ZJ5(IAD,IB,M5H(J2,J3,J4,J5,J1))
   40    CONTINUE
   20 CONTINUE
*
      END
*
************************************************************************
* BRAC(Z1,ZP1,Z2,ZP2,Z3,IOFF) is the bracket term                      *
*           which appears in the recursion relation.                   *
*                                                                      *
* In:  Z1 & ZP1, attributes ( hel,mom ) of current 1                   *
*      Z2 & ZP2, same for current 2                                    *
*      Currents need not be conserved ( eg Z1.ZP1 != 0 ) when IOFFSH=1 *
* Out: Z3(2,2)                                                         *
************************************************************************
      SUBROUTINE BRAC(Z1,ZP1,Z2,ZP2,Z3,IOFFSH)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters,
*
      DIMENSION Z1(2,2),ZP1(2,2),Z2(2,2),ZP2(2,2),Z3(2,2)
*
      ZA=ZMUL(ZP2,Z1)
      ZB=ZMUL(ZP1,Z2)
      IF (IOFFSH.EQ.1) THEN
        ZA=ZA+0.5*ZMUL(ZP1,Z1)
        ZB=ZB+0.5*ZMUL(ZP2,Z2)
      END IF
      ZC=0.5*ZMUL(Z1,Z2)
      DO 10 IAD=1,2
        DO 10 IB=1,2
          Z3(IAD,IB)=+ZA*Z2(IAD,IB)-ZB*Z1(IAD,IB)
     .               +ZC*(ZP1(IAD,IB)-ZP2(IAD,IB))
   10 CONTINUE
*
      END
*
************************************************************************
* INITS initializes all the spinors and propagators with quarks        *
*                                                                      *
* In:  P(1:NG) gluon momenta & Q(1:NQ) quark/antiquark momenta         *
* Out: Gluon spinors in /SPING/ and massive quark spinors in /SPINQ/   *
*      inverse gluon propagators in PPi(0,...)                         *
*      inverse quark+gluons propagators in PQPi(1,...)                 *
*      sum of gluon momenta in spinor language in ZPi(2,2,...)         *
*      and ZQ1(2,2,2) in common /KINEMA/                               *
*      Mass of quarks is implicit, i.e. deduced from momenta           *
*                                                                      *
* Designed to be general but applied only to work with quark currents! *
* ==> NQ=2 or NQ=4 (and not NQ=0)   (NUP=5 is the only possible value) *
************************************************************************
      SUBROUTINE INITS(P,N,Q,NQ)
      PARAMETER(NGUP=10,NUP=5,IQUP=4,NUPM1=5,NUPM2=20,NUPM3=60,
     .          NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters
*
      DIMENSION P(0:3,*),Q(0:3,*)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /SPING/  ZKO(2,NGUP),ZKOD(2,NGUP),ZHVEC(2,2,2,NGUP)
      COMMON /SPINQ/ ZQKO(2,IQUP),ZQKOD(2,IQUP),
     .               ZQMO(2,IQUP),ZQMOD(2,IQUP)
      COMMON /FERMPR/ ZQP0(2,2,IQUP),ZQP1(2,2,IQUP,NUP),
     .                ZQP2(2,2,IQUP,NUPM2),ZQP3(2,2,IQUP,NUPM3),
     .                ZQP4(2,2,IQUP,NUPM4),ZQP5(2,2,IQUP,NUPM5),
     .                PQP1(IQUP,NUP),PQP2(IQUP,NUPM2),PQP3(IQUP,NUPM3),
     .                PQP4(IQUP,NUPM4),PQP5(IQUP,NUPM5)
      SAVE /MAPPIN/,/GLUOPR/,/SPING/,/SPINQ/,/FERMPR/
*
* init gluon spinors and spinor momenta
*
      NG=N-2
      CALL SETSP(P,N,1,Q,NQ)
      DO 9 I1=1,NG
       DO 9 IAD=1,2
        DO 9 IB=1,2
          ZP1(IAD,IB,I1)=+ZKOD(IAD,I1)*ZKO(IB,I1)
    9 CONTINUE
      CALL INITPR(ZP1,NG)
*
*
* init sums of spinor momenta and inverse propagators, WITH quarks.
*
      DO 10 IQ=1,NQ
       DO 15 IAD=1,2
        DO 15 IB=1,2
         ZQP0(IAD,IB,IQ)=+ZQKOD(IAD,IQ)*ZQKO(IB,IQ)
   15  CONTINUE
*
       IF (NG.EQ.0) GOTO 999
       DO 30 I1=1,NG
         DO 20 IAD=1,2
          DO 20 IB=1,2
            ZQP1(IAD,IB,IQ,I1)=+ZQP0(IAD,IB,IQ)+ZP1(IAD,IB,I1)
   20    CONTINUE
         PQP1(IQ,I1)=
     .    0.5*REAL(ZMUL(ZQP1(1,1,IQ,I1),ZQP1(1,1,IQ,I1)))
   30  CONTINUE
*
       IF (NG.EQ.1) GOTO 999
*
       DO 40 I1=1,NG
        DO 40 I2=1,NG
         IF (M2H(I1,I2).NE.0) THEN
           DO 45 IAD=1,2
            DO 45 IB=1,2
              ZQP2(IAD,IB,IQ,M2H(I1,I2))=ZQP0(IAD,IB,IQ)
     .                                  +ZP2(IAD,IB,M2H(I1,I2))
   45      CONTINUE
           PQP2(IQ,M2H(I1,I2))=0.5*REAL(ZMUL(ZQP2(1,1,IQ,M2H(I1,I2)),
     .                         ZQP2(1,1,IQ,M2H(I1,I2))))
         END IF
   40  CONTINUE
*
       IF (NG.EQ.2) GOTO 999
*
       DO 50 I1=1,NG
        DO 50 I2=1,NG
         DO 50 I3=1,NG
          IF (M3H(I1,I2,I3).NE.0) THEN
            DO 55 IAD=1,2
             DO 55 IB=1,2
               ZQP3(IAD,IB,IQ,M3H(I1,I2,I3))=ZQP0(IAD,IB,IQ)
     .                                      +ZP3(IAD,IB,M3H(I1,I2,I3))
   55       CONTINUE
            PQP3(IQ,M3H(I1,I2,I3))=
     .       0.5*REAL(ZMUL(ZQP3(1,1,IQ,M3H(I1,I2,I3)),
     .                     ZQP3(1,1,IQ,M3H(I1,I2,I3))))
          END IF
   50  CONTINUE
*
       IF (NG.EQ.3) GOTO 999
*
       DO 60 I1=1,NG
        DO 60 I2=1,NG
         DO 60 I3=1,NG
          DO 60 I4=1,NG
           IF (M4H(I1,I2,I3,I4).NE.0) THEN
             DO 65 IAD=1,2
              DO 65 IB=1,2
               ZQP4(IAD,IB,IQ,M4H(I1,I2,I3,I4))=ZQP0(IAD,IB,IQ)
     .                                     +ZP4(IAD,IB,M4H(I1,I2,I3,I4))
   65        CONTINUE
             PQP4(IQ,M4H(I1,I2,I3,I4))=
     .        0.5*REAL(ZMUL(ZQP4(1,1,IQ,M4H(I1,I2,I3,I4)),
     .                  ZQP4(1,1,IQ,M4H(I1,I2,I3,I4))))
          END IF
   60  CONTINUE
*
       IF (NG.EQ.4) GOTO 999
*
       DO 70 I1=1,NG
        DO 70 I2=1,NG
         DO 70 I3=1,NG
          DO 70 I4=1,NG
           DO 70 I5=1,NG
             IF (M5H(I1,I2,I3,I4,I5).NE.0) THEN
              DO 75 IAD=1,2
               DO 75 IB=1,2
                ZQP5(IAD,IB,IQ,M5H(I1,I2,I3,I4,I5))=ZQP0(IAD,IB,IQ)
     .              +ZP2(IAD,IB,M2H(I1,I2))+ZP3(IAD,IB,M3H(I3,I4,I5))
   75        CONTINUE
             PQP5(IQ,M5H(I1,I2,I3,I4,I5))=
     .        0.5*REAL(ZMUL(ZQP5(1,1,IQ,M5H(I1,I2,I3,I4,I5)),
     .                  ZQP5(1,1,IQ,M5H(I1,I2,I3,I4,I5))))
          END IF
   70  CONTINUE
*
  999 CONTINUE
*
   10 CONTINUE
*
      END
************************************************************************
* SETSP(PGLUON,NGLUON,IHVEC,PQUARK,NQUARK) inits the spinors and       *
*   helicity vectors. Special version of SETQSP and SETGSP to work     *
*   for VECTOR bosons with massless quarks.                            *
*   Transformation to Weyl-vd Waerden spinor, (P1,P2,P3)-> (P2,P3,P1)  *
*                                                                      *
*   In:  Set of contra-variant momenta                                 *
*   Out: COMMON /SPING/ spinors and helicity vectors with random       *
*        spinor B, if flag IHVEC=1                                     *
*        COMMON /SPINQ/ spinors for quarks only ZQK's are filled!      *
************************************************************************
      SUBROUTINE SETSP(PGLUON,NGLUON,IHVEC,PQUARK,NQUARK)
      PARAMETER(NGUP=10,IQUP=4)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameter: gluon momenta
*
      DIMENSION PGLUON(0:3,*),PQUARK(0:3,*)
*
* Global variables
*
      COMMON /SPING/ ZKO(2,NGUP),ZKOD(2,NGUP),ZHVEC(2,2,2,NGUP)
      COMMON /DOTPR/ ZUD(NGUP,NGUP),ZD(NGUP,NGUP)
      COMMON /SPINQ/ ZQKO(2,IQUP),ZQKOD(2,IQUP),
     .               ZQMO(2,IQUP),ZQMOD(2,IQUP)
*
* Local variables
*
      DIMENSION ZB(2)
      REAL*8 RN
      SAVE /SPING/,/DOTPR/,/SPINQ/
*
* Transform contravariant momentum into covariant spinors, add extra
* minus sign when the energy<0 to preserve momentum conservation.
*
      rmin1=1e5
      rmin2=1e5
      do i=1,ngluon
         r=abs(pgluon(0,i)-pgluon(1,i))
         if (r.lt.rmin1) rmin1=r
         r=abs(pgluon(0,i)-pgluon(2,i))
         if (r.lt.rmin2) rmin2=r
      enddo
      do i=1,nquark
         r=abs(pquark(0,i)-pquark(1,i))
         if (r.lt.rmin1) rmin1=r
         r=abs(pquark(0,i)-pquark(2,i))
         if (r.lt.rmin2) rmin2=r
      enddo
      j1=1
      j2=2
      if (rmin2.gt.rmin1) then
         j1=2
         j2=1
      endif
      DO 10 I=1,NGLUON
        ZKO(1,I)=(PGLUON(j2,I)-(0.0,1.0)*PGLUON(3,I))
     .             /CSQRT(CMPLX(PGLUON(0,I)-PGLUON(j1,I)))
        ZKO(2,I)=CSQRT(CMPLX(PGLUON(0,I)-PGLUON(j1,I)))
        ZKOD(1,I)=CONJG(ZKO(1,I))
        ZKOD(2,I)=CONJG(ZKO(2,I))
        IF (PGLUON(0,I).LT.0.0) THEN
          ZKOD(1,I)=-ZKOD(1,I)
          ZKOD(2,I)=-ZKOD(2,I)
        END IF
   10 CONTINUE
*
      DO 15 I=1,NQUARK
        ZQKO(1,I)=(PQUARK(j2,I)-(0.0,1.0)*PQUARK(3,I))
     .             /CSQRT(CMPLX(PQUARK(0,I)-PQUARK(j1,I)))
        ZQKO(2,I)=CSQRT(CMPLX(PQUARK(0,I)-PQUARK(j1,I)))
        ZQKOD(1,I)=CONJG(ZQKO(1,I))
        ZQKOD(2,I)=CONJG(ZQKO(2,I))
        IF (PQUARK(0,I).LT.0.0) THEN
          ZQKOD(1,I)=-ZQKOD(1,I)
          ZQKOD(2,I)=-ZQKOD(2,I)
        END IF
   15 CONTINUE
*
* Helicity vectors
*
      IF (IHVEC.EQ.1) THEN
        SQR2=SQRT(2.0)
        DO 20 I=1,NGLUON-2
          ZB(1)=REAL(RN(1))+(0.0,1.0)*REAL(RN(2))
          ZB(2)=REAL(RN(3))+(0.0,1.0)*REAL(RN(4))
          ZN0=(ZKO(2,I)*ZB(1)-ZKO(1,I)*ZB(2))
          ZHVEC(1,1,1,I)=SQR2*ZKOD(1,I)*ZB(1)/ZN0
          ZHVEC(1,2,1,I)=SQR2*ZKOD(1,I)*ZB(2)/ZN0
          ZHVEC(2,1,1,I)=SQR2*ZKOD(2,I)*ZB(1)/ZN0
          ZHVEC(2,2,1,I)=SQR2*ZKOD(2,I)*ZB(2)/ZN0
          ZN1=(ZKOD(2,I)*CONJG(ZB(1))-ZKOD(1,I)*CONJG(ZB(2)))
          ZHVEC(1,1,2,I)=SQR2*CONJG(ZB(1))*ZKO(1,I)/ZN1
          ZHVEC(1,2,2,I)=SQR2*CONJG(ZB(1))*ZKO(2,I)/ZN1
          ZHVEC(2,1,2,I)=SQR2*CONJG(ZB(2))*ZKO(1,I)/ZN1
          ZHVEC(2,2,2,I)=SQR2*CONJG(ZB(2))*ZKO(2,I)/ZN1
   20   CONTINUE
      END IF
*
* Spinor innerproducts
*
      DO 30 I=1,NGLUON-3
        DO 30 J=I,NGLUON-2
          ZUD(I,J)=-ZKO(1,I)*ZKO(2,J)+ZKO(2,I)*ZKO(1,J)
          ZUD(J,I)=-ZUD(I,J)
          ZD(I,J)=-ZKOD(1,I)*ZKOD(2,J)+ZKOD(2,I)*ZKOD(1,J)
          ZD(J,I)=-ZD(I,J)
   30 CONTINUE
*
      END
*
************************************************************************
* INITPR(ZP,NG) initializes sums off gluon momenta and                 *
*    the corresponding gluonic propagators in COMMON /GLUOPR/          *
*                                                                      *
* In:  Array of gluon momenta (need not be on-shell)                   *
* Out: COMMON /GLUOPR/ see below, mapping used for compact storage     *
************************************************************************
      SUBROUTINE INITPR(ZP,NG)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters
*
      DIMENSION ZP(2,2,*)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      SAVE /MAPPIN/,/GLUOPR/
*
      CALL COPYTS(ZP1,ZP,NG)
*
      IF (NG.EQ.1) GOTO 999
*
      DO 40 I1=1,NG
       DO 40 I2=1,NG
        IF (M2H(I1,I2).NE.0) THEN
          DO 45 IAD=1,2
           DO 45 IB=1,2
             ZP2(IAD,IB,M2H(I1,I2))=ZP1(IAD,IB,I1)+ZP1(IAD,IB,I2)
   45     CONTINUE
          PP2(M2H(I1,I2))=
     .      0.5*REAL(ZMUL(ZP2(1,1,M2H(I1,I2)),ZP2(1,1,M2H(I1,I2))))
        END IF
   40 CONTINUE
*
      IF (NG.EQ.2) GOTO 999
*
      DO 50 I1=1,NG
       DO 50 I2=1,NG
        DO 50 I3=1,NG
         IF (M3H(I1,I2,I3).NE.0) THEN
           DO 55 IAD=1,2
            DO 55 IB=1,2
              ZP3(IAD,IB,M3H(I1,I2,I3))=ZP2(IAD,IB,M2H(I1,I2))
     .                                 +ZP1(IAD,IB,I3)
   55      CONTINUE
           PP3(M3H(I1,I2,I3))=0.5*REAL(ZMUL(ZP3(1,1,M3H(I1,I2,I3)),
     .                        ZP3(1,1,M3H(I1,I2,I3))))
         END IF
   50 CONTINUE
*
      IF (NG.EQ.3) GOTO 999
*
      DO 60 I1=1,NG
       DO 60 I2=1,NG
        DO 60 I3=1,NG
         DO 60 I4=1,NG
          IF (M4H(I1,I2,I3,I4).NE.0) THEN
            DO 65 IAD=1,2
             DO 65 IB=1,2
              ZP4(IAD,IB,M4H(I1,I2,I3,I4))=ZP3(IAD,IB,M3H(I1,I2,I3))
     .                                    +ZP1(IAD,IB,I4)
   65       CONTINUE
            PP4(M4H(I1,I2,I3,I4))=
     .       0.5*REAL(ZMUL(ZP4(1,1,M4H(I1,I2,I3,I4)),
     .                 ZP4(1,1,M4H(I1,I2,I3,I4))))
         END IF
   60 CONTINUE
*
  999 CONTINUE
*
      END
*
************************************************************************
* INIMAP initializes the mapping tables, used for easy reference and   *
* compact storage of all calculated currents.                          *
*                                                                      *
* Important feature of mapping is that the first I! of MIH are the     *
* permutations of 1..I (do not included higher gluon numbers)          *
*                                                                      *
* In:  NG number of gluons which can occur                             *
* Out: COMMON /MAPPIN/                                                 *
************************************************************************
* VERY IMPROTANT: INIMAP can be called more than once, however if NG   *
* gets smaller /MAPPIN/ should not be reinitialised!!                  *
************************************************************************
      SUBROUTINE INIMAP(NG)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      DIMENSION IP(NUP)
      SAVE /MAPPIN/,IP,NGOLD
      DATA NGOLD /0/
*
      IF (NG.LE.NGOLD) RETURN
      NGOLD=NG
*
      DO 10 I=1,IFAC(NUP)
        CALL INVID(I,IP,NUP)
        M1H(IP(1))=0
        M2H(IP(1),IP(2))=0
        M3H(IP(1),IP(2),IP(3))=0
        M4H(IP(1),IP(2),IP(3),IP(4))=0
        M5H(IP(1),IP(2),IP(3),IP(4),IP(5))=0
   10 CONTINUE
      M1UP=0
      M2UP=0
      M3UP=0
      M4UP=0
      M5UP=0
      DO 20 NTEL=1,NG
        DO 30 I=1,IFAC(NTEL)
          CALL INVID(I,IP,NTEL)
          I1=IP(1)
          I2=IP(2)
          I3=IP(3)
          I4=IP(4)
          I5=IP(5)
          IF (M1H(I1).EQ.0) THEN
            M1UP=M1UP+1
            M1H(I1)=M1UP
            M1T(M1UP,1)=I1
          END IF
          IF ((M2H(I1,I2).EQ.0).AND.(NTEL.GT.1)) THEN
            M2UP=M2UP+1
            M2H(I1,I2)=M2UP
            M2T(M2UP,1)=I1
            M2T(M2UP,2)=I2
          END IF
          IF ((M3H(I1,I2,I3).EQ.0).AND.(NTEL.GT.2)) THEN
            M3UP=M3UP+1
            M3H(I1,I2,I3)=M3UP
            M3T(M3UP,1)=I1
            M3T(M3UP,2)=I2
            M3T(M3UP,3)=I3
          END IF
          IF ((M4H(I1,I2,I3,I4).EQ.0).AND.(NTEL.GT.3)) THEN
            M4UP=M4UP+1
            M4H(I1,I2,I3,I4)=M4UP
            M4T(M4UP,1)=I1
            M4T(M4UP,2)=I2
            M4T(M4UP,3)=I3
            M4T(M4UP,4)=I4
          END IF
          IF ((M5H(I1,I2,I3,I4,I5).EQ.0).AND.(NTEL.GT.4)) THEN
            M5UP=M5UP+1
            M5H(I1,I2,I3,I4,I5)=M5UP
            M5T(M5UP,1)=I1
            M5T(M5UP,2)=I2
            M5T(M5UP,3)=I3
            M5T(M5UP,4)=I4
            M5T(M5UP,5)=I5
          END IF
   30   CONTINUE
   20 CONTINUE
*
      END
*
************************************************************************
* Subroutine RESHQS(P,IQ1,IQB1,IQ2,IQB2,NQ) reorders the momenta       *
* such that the first is a Q, the second is the corresponding QB, etc. *
* In:  P(0:3,*) single precision momenta.                              *
* Out: Reshuffled array P(0:3,*)                                       *
* When NQ=2 only the first two indices are used.                       *
************************************************************************
      SUBROUTINE RESHQS(P,IQ1,IQB1,IQ2,IQB2,NQ)
      IMPLICIT REAL (A-H,O-Y)
      DIMENSION P(0:3,*),INDEX(4)
*
* Make sure the swapping does not swap one momentum twice.
*
      INDEX(1)=IQ1
      INDEX(2)=IQB1
      IF (INDEX(2).EQ.1) INDEX(2)=INDEX(1)
      IF (NQ.EQ.4) THEN
        INDEX(3)=IQ2
        IF (INDEX(3).EQ.1) INDEX(3)=INDEX(1)
        IF (INDEX(3).EQ.2) INDEX(3)=INDEX(2)
        INDEX(4)=IQB2
        IF (INDEX(4).EQ.1) INDEX(4)=INDEX(1)
        IF (INDEX(4).EQ.2) INDEX(4)=INDEX(2)
        IF (INDEX(4).EQ.3) INDEX(4)=INDEX(3)
      END IF
*
      DO 10 I=1,NQ
        DO 20 MU=0,3
          TEMP=P(MU,I)
          P(MU,I)=P(MU,INDEX(I))
          P(MU,INDEX(I))=TEMP
   20   CONTINUE
   10 CONTINUE
*
      END
*
************************************************************************
* QQBCOL calculates the colour matrix for q qb --> N gluons.           *
*                                                                      *
* input: COLMAT(NUP! , NUP!)  real colour matrix.                      *
*        N                    number of gluons.                        *
*        NCOL                 number of SU(N) colours.                 *
*        IORDER               numbers of terms in colour polynomial    *
*                             to take into account.                    *
*                                                                      *
* colour matrix left out factor: NCOL * (NCOL**2 - 1)/ (2*NCOL)**N     *
************************************************************************
      SUBROUTINE QQBCOL(COLMAT,N,NCOL,IORDER)
      PARAMETER (NUP=5,NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      DIMENSION COLMAT(1:NUPFAC,1:NUPFAC)
      INTEGER POL(0:2*NUP),P(1:NUP),Q(1:NUP),PERS(NUP,NUPFAC/NUP)
*
* Exception when N=0
      IF (N.EQ.0) THEN
        COLMAT(1,1)=0.5/(NCOL**2-1.0)
        RETURN
      END IF
*
* calculate first row
*
      CALL INVID(1,P,N)
      DO 10 J=1,IFAC(N)
        CALL INVID(J,Q,N)
        CALL TRQQB(P,Q,N,POL)
        COLMAT(1,J)=0.0
        DO 20 K=N-1,0,-1
          COLMAT(1,J)=COLMAT(1,J)*NCOL*NCOL
          IF(IORDER.GE.(N-K)) COLMAT(1,J)=COLMAT(1,J)+POL(2*K)
   20   CONTINUE
   10 CONTINUE
*
* initialize other (n-1)! - 1 rows of colour matrix
*
      DO 30 I=1,IFAC(N-1)
        CALL INVID(I,P,N)
        DO 35 K=1,N
   35     PERS(P(K),I)=K
   30 CONTINUE
      DO 70 I=1,IFAC(N)
        CALL INVID(I,P,N)
        DO 60 J=1,IFAC(N-1)
          DO 50 K=1,N
   50       Q(K)=PERS(P(K),J)
          IDEE=ID(Q,N)
          COLMAT(J,I)=COLMAT(1,IDEE)
   60   CONTINUE
   70 CONTINUE
*
* fill rest of matrix by using the first (n-1)! rows
*
      DO 100 IROT=1,N-1
        IR=IFAC(N-1)*IROT
        IS=IFAC(N)-IR
        DO 90 I=1,IFAC(N-1)
          DO 19 K=1,IR
            COLMAT(IR+I,K)=COLMAT(I,K+IS)
   19     CONTINUE
          DO 21 K=1+IR,IFAC(N)
            COLMAT(IR+I,K)=COLMAT(I,K-IR)
   21     CONTINUE
   90   CONTINUE
  100 CONTINUE
*
      END
*
************************************************************************
* TRQQB calculates the colour trace occuring in q qb --> N gluons
*
* input: P,Q                  permutaion of D-functions
*        N                    number of gluons.
*        POL                  output colour polynomial
*
* method: eliminate fundamental T matrices with
*   T(a,i,j)*T(a,k,l) = 1/2 * ( D(i,l)*D(j,k)- 1/NCOL * D(i,j)*D(k,l))
* the delta functions arithmetic is done in the IT-array
*
* polynomial left out factor: NCOL * (NCOL**2-1) / (2*NCOL)**N
************************************************************************
      SUBROUTINE TRQQB(P,Q,N,POL)
      PARAMETER(NUP=6)
      INTEGER P(1:*),Q(1:*),N,POL(0:*)
      INTEGER PI(1:NUP),QI(1:NUP),IT(1:2*NUP)
*
      DO 10 I=0,2*N
   10   POL(I)=0
*
      DO 20 I=1,N
        PI(P(I))=I
        QI(Q(N+1-I))=I
   20 CONTINUE
      DO 80 I=1,2**N
        IBIT=I-1
        NPOW=N-1
        NSIGN=1
        DO 30 J=1,2*N
   30     IT(J)=J
        DO 60 J=1,N
          K1=PI(J)
          K3=N+QI(J)
          IF (MOD(IBIT,2).EQ.0) THEN
            K2=1+MOD(K1,2*N)
            K4=1+MOD(K3,2*N)
            NPOW=NPOW-1
            NSIGN=-NSIGN
          ELSE
            K2=1+MOD(K3,2*N)
            K4=1+MOD(K1,2*N)
          END IF
   40     L=K2
          K2=IT(L)
          IF (K2.NE.L) GOTO 40
          IT(K1)=K2
   50     L=K4
          K4=IT(L)
          IF (K4.NE.L) GOTO 50
          IT(K3)=K4
          IBIT=IBIT/2
   60   CONTINUE
        DO 70 J=1,2*N
   70     IF (IT(J).EQ.J) NPOW=NPOW+1
        POL(NPOW)=POL(NPOW)+NSIGN
   80 CONTINUE
*
* divide out a factor (NCOL**2-1)
*
      POL(0)=-POL(0)
      POL(1)=-POL(1)
      DO 90 I=2,2*N
   90   POL(I)=POL(I-2)-POL(I)
*
      END
*
************************************************************************
* FUNCTION ID(LPERM,NGLUON) maps permutation of (1...N) onto integers. *
*   Also works for NGLUON=0 (returns 1).  Inverse routine is INVID.    *
*   Limitation: NGLUON <= NGUP!                                        *
*                                                                      *
*   In:  LPERM = permutation of 1..NGLUON                              *
*   Out: Mapping according to subsyclic rotations                      *
************************************************************************
      FUNCTION ID(LPERM,NGLUON)
      PARAMETER(NGUP=10)
      DIMENSION LPERM(1:*),IW(1:NGUP)
*
      IF (NGUP.LT.NGLUON) STOP
*
      ID=1
      DO 10 I=1,NGLUON
        IW(I)=I-1
   10 CONTINUE
      DO 30 I=1,NGLUON-1
        K1=IW(LPERM(I))
        ID=ID+K1*IFAC(NGLUON-I)
        K2=K1+1
        K3=NGLUON-I-K1
        DO 20 J=1,NGLUON
          IF (IW(J).LT.K2) THEN
            IW(J)=IW(J)+K3
          ELSE
            IW(J)=IW(J)-K2
          END IF
   20   CONTINUE
   30 CONTINUE
      END
*
************************************************************************
* SUBROUTINE INVID(IDNUM,LPERM,NGLUON) creates a permutation           *
*   of (1...NGLUON) from IDNUM according to subcyclic numbering.       *
*   Result in LPERM. Also works for N=0 (returns nothing!).            *
*   Inverse routine of ID-function. Limitation: NGLUON <= NGUP         *
*                                                                      *
*   In:  IDNUM corresponding with a permutation of (1..NGLUON)         *
*   Out: LPERM permutation of (1..NGLUON)                              *
************************************************************************
      SUBROUTINE INVID(IDNUM,LPERM,NGLUON)
      PARAMETER(NGUP=10)
      DIMENSION LPERM(1:*),LHELP(1:NGUP*NGUP)
*
      IDNR=IDNUM-1
      DO 10 I=1,NGLUON
        LHELP(I)=I
   10 CONTINUE
      IB=1
*
      DO 40 I=1,NGLUON-1
        K1=IFAC(NGLUON-I)
        K2=IDNR/K1
        LPERM(I)=LHELP(IB+K2)
        K3=NGLUON-I+1
        DO 20 J=IB,IB+K2-1
          LHELP(J+K3)=LHELP(J)
   20   CONTINUE
        IB=IB+K2+1
        IDNR=MOD(IDNR,K1)
   40 CONTINUE
      IF (NGLUON.GT.0) LPERM(NGLUON)=LHELP(IB)
      END
*
***********************************************************************
* INTEGER FUNCTION IFAC(N) returns N! (N<11)                          *
***********************************************************************
      FUNCTION IFAC(N)
      INTEGER IFAK(0:10)
      SAVE IFAK
      DATA IFAK/1,1,2,6,24,120,720,5040,40320,362880,3628800/
      IFAC=IFAK(N)
      END
*
***********************************************************************
* FUNCTION ZMUL(Z1,Z2) calculates a SMU*SNU inproduct in              *
*   spinor tensor language.                                           *
*                                                                     *
*   In:  Z1(2,2),Z2(2,2) both co(ntra) variant spinor tensors         *
*   Out: Innerproduct  (2x normal minkowski innerprodcut              *
***********************************************************************
      FUNCTION ZMUL(Z1,Z2)
      COMPLEX ZMUL,Z1(2,2),Z2(2,2)
      ZMUL=Z1(1,1)*Z2(2,2)+Z1(2,2)*Z2(1,1)
     .    -Z1(1,2)*Z2(2,1)-Z1(2,1)*Z2(1,2)
*
      END
*
************************************************************************
* COPYTS(ZTARG,ZSOUR,N) copies N spinor tensors from ZSOUR to ZTARG.   *
*                                                                      *
* In:  ZSOUR(2,2,N)                                                    *
* Out: ZTARG(2,2,N) = ZSOUR(2,2,N)                                     *
************************************************************************
      SUBROUTINE COPYTS(ZTARG,ZSOUR,N)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
      DIMENSION ZTARG(2,2,*),ZSOUR(2,2,*)
*
      DO 10 I=1,N
        ZTARG(1,1,I)=ZSOUR(1,1,I)
        ZTARG(1,2,I)=ZSOUR(1,2,I)
        ZTARG(2,1,I)=ZSOUR(2,1,I)
        ZTARG(2,2,I)=ZSOUR(2,2,I)
   10 CONTINUE
*
      END










