************************************************************************
*                                                                      *
* compute cross section (sig) with standard deviation (sd) from        *
* itmx iterations                                                      *
*                                                                      *
************************************************************************
      subroutine cross(sig,sd)
      implicit double precision (a-h,o-z)
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /normal/exclusive
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      COMMON /BVEGA/NDIMA,NCALLA,NPRNA
      COMMON /BVEGB/NDIMB,NCALLB,NPRNB
      common /plots/plot
      external sig4,sig5
      logical exclusive,plot 
*
*
*
      nprna=1
      nprnb=1
*
* warm up 
*
*
* tree level or virtual cross section computed using vegasa
* bremstrahlung cross section using vegasb
*
      ndima=3
      if(nloop.eq.0.and.njets.eq.3)ndima=6
      ndimb=6
      ncalla=nshot1
      ncallb=nshot3
      plot=.false.
*
* initialize vegas
*
      call vegasa(1,sig4,avgi4,sd4,chi2a4)
      if(nloop.eq.1)call vegasb(1,sig5,avgi5,sd5,chi2a5)
*
* vegas sweeps
*
      do it=1,itmax1
        call vegasa(3,sig4,avgi4,sd4,chi2a4)
        if(nloop.eq.1)call vegasb(3,sig5,avgi5,sd5,chi2a5)
      enddo
*
* main run
*
      ncalla=nshot2
      ncallb=nshot4
      plot=.true.
      call bino(0,0d0,0)
*
* initialize vegas
*
      call vegasa(2,sig4,avgi4,sd4,chi2a4)
      if(nloop.eq.1)call vegasb(2,sig5,avgi5,sd5,chi2a5)
*
* vegas sweeps with frozen grid
*
      do it=1,itmax2
        call vegasa(4,sig4,avgi4,sd4,chi2a4)
        if(nloop.eq.1)call vegasb(4,sig5,avgi5,sd5,chi2a5)
        call bino(2,0d0,0)
      enddo
*
*
*
      call bino(3,0d0,0)
      if(nloop.eq.0)then
        sig=avgi4
        sd=sd4
      elseif(nloop.eq.1)then
        sig=avgi4+avgi5
        sd=sqrt(sd4**2+sd5**2)
      endif

      return
      end
*
************************************************************************
*
      function sig4(x,wgt)
      implicit double precision (a-h,o-z)
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /normal/exclusive
      common /plots/plot
      logical exclusive,plot 
      dimension x(10)
*
      wtnano=389385.73d0	
      sig4=0d0
* 
*  # partons = njets (nloop=0 --> l.o. , nloop=1 --> l.o. + virtual graphs)
*
      npar=njets
      if(njets.eq.1)npar=2
      call phase(x,npar,1,wtps,ipass)
      if (ipass.eq.0) return
      call clust(jets,npar)
      if(exclusive)then
	if (jets.ne.njets) return
      else	 
        if (jets.lt.njets) return
      endif
      call scale(jets)
      call qcdjet(npar,nloop,rint)
      wt=wtnano*wtps*rint   
      if(plot)then
        call bino(1,wt*wgt,npar)
      else
        call distrib(wtdis,jets)
        wt=wt/wtdis
      endif
      sig4=wt
      return
      end
*
************************************************************************
*
      function sig5(x,wgt)
      implicit double precision (a-h,o-z)
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /normal/exclusive
      common /plots/plot
      logical exclusive,plot 
      dimension x(10)
*
      wtnano=389385.73d0	
      sig5=0d0
* 
* # partons = njets + 1 (nloop=0 --> 0 , nloop=1 --> real graphs)
*
      if (nloop.eq.0) return
      npar=njets+1
      if(njets.eq.1)npar=3
      call phase(x,npar,2,wtps,ipass)
      if (ipass.eq.0) return
      call clust(jets,npar)
      if(exclusive)then
	if (jets.ne.njets) return
      else	 
        if (jets.lt.njets) return
      endif
      call scale(jets)
      call qcdjet(npar,nloop-1,rint)
      wt=wtnano*wtps*rint   
      if(plot)then
        call bino(1,wt*wgt,npar)
      else
        call distrib(wtdis,jets)
        wt=wt/wtdis
      endif
      sig5=wt
      return
      end
************************************************************************
*
      subroutine clust(jets,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /parmom/ ppar(4,10)
      common /jetmom/ pjet(8,10),jp(10)
      data init/0/
      if(init.eq.0) then
        init=1
        write(*,*)' jet clustering as of 23/4/96 '
      endif     
*
* pjet(5,j) = ET
* pjet(6,j) = pseudorapidity
* pjet(7,j) = azimuthal angle
* pjet(8,j) = 0 (possible mass entry)
*
      do i=1,2+npar
         do j=1,4
            pjet(j,i)=ppar(j,i)
         enddo
      enddo
      do i=1,npar
         jp(i)=i+2
         j=jp(i)
         if (jetalg.eq.1) then
            pjet(5,j)=sqrt(pjet(1,j)**2+pjet(2,j)**2)
            theta=atan2(sqrt(pjet(1,j)**2+pjet(2,j)**2),pjet(3,j))
            pjet(6,j)=-log(abs(tan(theta/2.0)))
            pjet(7,j)=atan2(pjet(1,j),pjet(2,j))
         endif
         if (jetalg.eq.2) then
            pp=sqrt(pjet(1,j)**2+pjet(2,j)**2+pjet(3,j)**2)
            rar=pjet(3,j)/pp
            if (rar.lt.-1d0) then 

               theta=pi
            elseif (rar.gt.1d0) then 

               theta=0d0
            else
               theta=acos(rar)
            endif
            pjet(5,j)=pjet(4,j)*sin(theta)
            pjet(6,j)=-log(abs(tan(theta/2.0)))
            pjet(7,j)=atan2(pjet(1,j),pjet(2,j))
         endif
      enddo
* cluster the partons
      if(npar.gt.2)then
 2    icol=0
      do  i=1,npar-1
        do j=i+1,npar
          j1=jp(i)
          j2=jp(j)
          if ((pjet(4,j1).gt.0.d0).and.(pjet(4,j2).gt.0.d0)) then
*
* recombination scheme
*       jetalg = 1 ; deltaR(i,j) < R; D0
*       jetalg = 2 ; deltaR(i,jet) < R and deltaR(j,jet) < R;  UA2 & CDF
*
               if (jetalg.eq.1) then
                  dely=pjet(6,j1)-pjet(6,j2)
                  delfi=pjet(7,j1)-pjet(7,j2)
                  delr=sqrt(dely**2+delfi**2)
                  if (delr.lt.delrjj) then
                     icol=1
                     do  k=1,5
                        pjet(k,j1)=pjet(k,j1)+pjet(k,j2)
                     enddo
                     etj=sqrt(pjet(1,j1)**2+pjet(2,j1)**2)
                     theta=atan2(etj,pjet(3,j1))
                     pjet(6,j1)=-log(abs(tan(theta/2.0)))
                     pjet(7,j1)=atan2(pjet(1,j1),pjet(2,j1))
                     pjet(4,j2)=-1d0
                  endif
               endif
               if (jetalg.eq.2) then
*
                  etjet=pjet(5,j1)+pjet(5,j2)
                  etajet=
     .             (pjet(6,j1)*pjet(5,j1)+pjet(6,j2)*pjet(5,j2))/etjet
                  phijet=
     .             (pjet(7,j1)*pjet(5,j1)+pjet(7,j2)*pjet(5,j2))/etjet
                  rar=(pjet(1,j1)*pjet(1,j2)+pjet(2,j1)*pjet(2,j2))
     .                /pjet(5,j1)/pjet(5,j2) 

                  if (rar.lt.-1d0) then 

                     delfi=pi
                  elseif (rar.gt.1d0) then 

                     delfi=0d0
                  else
                     delfi=acos(rar)
                  endif
                  delfi1= pjet(5,j2)*delfi/etjet ! phi_1 - phijet 

                  delfi2=-pjet(5,j1)*delfi/etjet ! phi_2 - phijet
                  dely1=pjet(6,j1)-etajet
                  dely2=pjet(6,j2)-etajet
                  dely12=pjet(6,j1)-pjet(6,j2)
                  delr1=sqrt(dely1**2+delfi1**2)
                  delr2=sqrt(dely2**2+delfi2**2)
                  delr12=sqrt(delfi**2+dely12**2)
                  if ((delr1.lt.delrjj).and.(delr2.lt.delrjj)) then
                   if (delr12.lt.rsep*delrjj) then
                     icol=1
                     eejet=exp(etajet)
                     cde=cos(delfi1)
                     sde=sin(delfi1)
                     if (pjet(2,j1)*pjet(1,j2).gt.pjet(2,j2)*pjet(1,j1))
     .                  sde=-sde
                     cphi=(pjet(1,j1)*cde-pjet(2,j1)*sde)/pjet(5,j1)
                     sphi=(pjet(2,j1)*cde+pjet(1,j1)*sde)/pjet(5,j1)
                     pjet(1,j1)=etjet*cphi
                     pjet(2,j1)=etjet*sphi
                     pjet(3,j1)=etjet/2d0*(eejet-1d0/eejet)
                     pjet(4,j1)=etjet/2d0*(eejet+1d0/eejet)
                     pjet(5,j1)=etjet
                     pjet(6,j1)=etajet
                     pjet(7,j1)=phijet
                     pjet(4,j2)=-1d0
                   endif
                  endif
               endif
            endif
         enddo
      enddo
      if (npar.gt.3.and.icol.eq.1) goto 2
      endif
*
* sort jets according to decreasing transverse energy
* j1=most energetic,...,j4=least energetic inside rapidity region
* then sort non-jets according to Et outside rap region
*
      do i=1,npar-1
         do j=i+1,npar
           j1=jp(i)
           j2=jp(j)
           if(pjet(4,j1).lt.0d0)pjet(5,j1)=0d0
           if(pjet(4,j2).lt.0d0)pjet(5,j2)=0d0
           if (pjet(5,j1).lt.pjet(5,j2)) then
               jt   =jp(i)
               jp(i)=jp(j)
               jp(j)=jt
           endif

         enddo
      enddo
* count number of observed jets 

*      etminj  <  et < etmaxj
*      rapminj < eta < rapmaxj

      jets=0
      do i=1,npar
         j=jp(i)
         if (pjet(4,j).gt.0d0) then
           if (pjet(5,j).ge.etminj
     .        .and.pjet(5,j).le.etmaxj
     .        .and.abs(pjet(6,j)).le.rapmaxj
     .        .and.abs(pjet(6,j)).ge.rapminj) jets=jets+1
         endif
      enddo
*
      end
*
*
************************************************************************
*
      subroutine scale(jets)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /phypar/ w,ipp1,ipp2,qcdl
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /fraction/ x1,x2,facscale,renscale
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /parmom/ ppar(4,10)
      common /jetmom/ pjet(8,10),jp(10)
*
* calculating renormalization and factorization scale
*
      sab=x1*x2*w**2
      pt=0d0
      ptmax=0d0
      do i=1,jets
         j=jp(i)
         if (pjet(4,j).gt.0d0) then
            pti=pjet(5,j)
            pt=pt+pti
            if(pti.gt.ptmax)ptmax=pti
         endif
      enddo
      if (irenorm.eq.1) renscale=sqrt(sab)*crenorm
      if (irenorm.eq.2) renscale=pt*crenorm 
      if (irenorm.eq.3) renscale=ptmax*crenorm
      if (irenorm.eq.4) renscale=100d0*crenorm
      if (ifact.eq.1) facscale=sqrt(sab)*cfact
      if (ifact.eq.2) facscale=pt*cfact
      if (ifact.eq.3) facscale=ptmax*cfact
      if (ifact.eq.4) facscale=100d0*cfact
      return
      end
*
************************************************************************
*
      subroutine phase(x,npar,itype,wtps,ipass)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /phypar/ w,ipp1,ipp2,qcdl
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /fraction/ x1,x2,facscale,renscale
      common /parmom/ ppar(4,10)
      dimension x(10)
*
* setup phase space generator
*
      do 20 j=1,4
        do 20 i=1,10
	  ppar(j,i)=0d0
   20 continue
*
* generate parton invariant masses      
*
      ipass=0
      if (itype.eq.1) call genhard(x,npar,wtps,ipass)
      if (itype.eq.2) call genpt(x,npar,wtps,ipass)
      if (ipass.eq.0) return
*
* add flux factor
*
      sab=x1*x2*w**2
      wtps=wtps/2d0/sab 
      ipass=1
*      if(npar.eq.2)then
*       open(unit=11,file='npar.2.DAT',status='unknown')
*      elseif(npar.eq.3)then
*       open(unit=12,file='npar.3.DAT',status='unknown')
*      endif
*      do i=1,npar+2
*        if(npar.eq.2)write(11,*)ppar(4,i),(ppar(j,i),j=1,3)
*        if(npar.eq.3)write(12,*)ppar(4,i),(ppar(j,i),j=1,3)
*   50   format(6e12.4)
*      enddo
*
      end
*
************************************************************************
*
      subroutine genhard(x,npar,wtps,ipass)
*
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /phypar/ w,ipp1,ipp2,qcdl
      common /fraction/ x1,x2,facscale,renscale
      common /parmom/ ppar(4,10)
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      dimension x(10)
*
      ipass=0      
      if (npar.eq.2) then
*
*     pt**2 integral
*      
        pt2max=dmin1((w/2d0)**2,etmaxj**2)
        pt2min=etminj**2
        if (pt2max.lt.pt2min) return
        call pick(pt2,pt2min,pt2max,x(1),wtpt2,2)
        pt=sqrt(pt2)
*
*     rapidity of parton 2 
*
        xt=2d0*pt/w
        y2lo=dlog((1d0-sqrt(1d0-xt**2))/xt)
        y2hi=dlog((1d0+sqrt(1d0-xt**2))/xt)
        if(njets.eq.2)then
          y2min=dmax1(y2lo,-rapmaxj)
          y2max=dmin1(y2hi,rapmaxj)
        elseif(njets.eq.1)then
          y2min=y2lo
          y2max=y2hi
        endif
        if (y2max.le.y2min) return
	call pick(y2,y2min,y2max,x(2),wty2,0)
*
*     rapidity of parton 1
*
        ey2=dexp(y2)
        y1lo=dlog(xt/(2d0-xt/ey2))
        y1hi=dlog((2d0-xt*ey2)/xt)
        if(njets.eq.2)then
          y1min=dmax1(y1lo,-rapmaxj)
          y1max=dmin1(y1hi,rapmaxj)
        elseif(njets.eq.1)then
          y1min=y1lo
          y1max=y1hi
        endif
        if (y1max.le.y1min) return
        call pick(y1,y1min,y1max,x(3),wty1,0)
*
        ey1=dexp(y1)
        x1=0.5d0*xt*(ey1+ey2)
        x2=0.5d0*xt*(1d0/ey1+1d0/ey2)
	if(x1.gt.1d0.or.x1.lt.0d0.or.x2.gt.1d0.or.x2.lt.0d0)return
	sab=x1*x2*w**2
        wtps=wtpt2*wty2*wty1/8d0/pi/sab
*
*     final state momentum 
*
        r1=rn(1.)
        phi=2d0*pi*r1
        i1=3
        i2=4
        r1=rn(1.)
        if(r1.gt.0.5d0)then
          i1=4
          i2=3
        endif
        ppar(1,i1)=pt*cos(phi)
        ppar(2,i1)=pt*sin(phi)
        ppar(3,i1)=0.5d0*pt*(ey2-1d0/ey2)
        ppar(4,i1)=0.5d0*pt*(ey2+1d0/ey2)
        ppar(1,i2)=-ppar(1,i1)
        ppar(2,i2)=-ppar(2,i1)
        ppar(3,i2)=0.5d0*pt*(ey1-1d0/ey1)
        ppar(4,i2)=0.5d0*pt*(ey1+1d0/ey1)
*
* incoming parton pair momenta
*
        ppar(1,1)=0.d0
        ppar(2,1)=0.d0
        ppar(3,1)=w/2d0*x1
        ppar(4,1)=w/2d0*x1
        ppar(1,2)=0.d0
        ppar(2,2)=0.d0
        ppar(3,2)=-w/2d0*x2
        ppar(4,2)=w/2d0*x2
      endif
      if (npar.eq.3) then
*
*     pt1**2 integral
*
        pt12mx=(w/2d0)**2
	pt12mn=etminj**2
	if (pt12mx.lt.pt12mn) return
	call pick(pt12,pt12mn,pt12mx,x(1),wtpt12,1)
        pt1=sqrt(pt12)
*
*     pt2**2 integral
*
	pt2max=dmin1(w-etminj-pt1,w/2d0)
	pt22mx=pt2max**2
	pt22mn=etminj**2
	if (pt22mx.lt.pt22mn) return
	call pick(pt22,pt22mn,pt22mx,x(2),wtpt22,1)
        pt2=sqrt(pt22)
*
*     phi(12) integral
*
        pt3mx=dmin1((w-pt1-pt2),w/2d0)
	pt32mx=pt3mx**2
	cphi=dmin1((pt32mx-pt12-pt22)/2d0/pt1/pt2,1d0)
        phimn=acos(cphi)
        pt3mn=etminj
	pt32mn=pt3mn**2
	cplo=dmax1((pt32mn-pt12-pt22)/2d0/pt1/pt2,-1d0)
	phimx=acos(cplo) 
	if (phimx.lt.phimn) return
	call pick(phi12,phimn,phimx,x(3),wtphi,0)
* symmetrize over phi
	r1=rn(1.)
	if (r1.gt.0.5d0) phi12=2d0*pi-phi12
	pt32=pt12+pt22+2d0*pt1*pt2*cos(phi12)
        pt3=sqrt(pt32) 
	if (pt3.gt.pt3mx.or.pt3.lt.pt3mn) return
*
        xt1=2d0*pt1/w
        xt2=2d0*pt2/w
        xt3=2d0*pt3/w
*
*
*     rapidity of parton 1 
*
        e1max=dmax1(w-pt2-pt3,w/2d0)
	root=sqrt(e1max**2-pt12)
	y1loa=log((e1max-root)/pt1)
	y1hia=log((e1max+root)/pt1)
	y1hib=log(2d0/xt1)
	y1lob=-y1hib
        y1min=dmax1(y1loa,y1lob,-rapmaxj)
        y1max=dmin1(y1hia,y1hib,rapmaxj)
        if (y1max.lt.y1min) return
	call pick(y1,y1min,y1max,x(4),wty1,0)
	ey1=exp(y1)
	e1=pt1*(ey1+1d0/ey1)/2d0
*	    	
*     rapidity of parton 2
*       
        e2max=dmax1(w-e1-pt3,w/2d0)
	root=sqrt(e2max**2-pt22)
	y2loa=log((e2max-root)/pt2)
	y2hia=log((e2max+root)/pt2)
	y2lob=log(xt2/(2d0-xt1/ey1))
	y2hib=log((2d0-xt1*ey1)/xt2)
        y2min=dmax1(y2loa,y2lob,-rapmaxj)
        y2max=dmin1(y2hia,y2hib,rapmaxj)
        if (y2max.le.y2min) return
        call pick(y2,y2min,y2max,x(5),wty2,0)
	ey2=exp(y2)
	e2=pt2*(ey2+1d0/ey2)/2d0
*
*     rapidity of parton 3
*
        e3max=dmax1(w-e1-e2,w/2d0)
	root=sqrt(e3max**2-pt32)
	y3loa=dlog((e3max-root)/pt3)
	y3hia=dlog((e3max+root)/pt3)
        y3lob=log(xt3/(2d0-xt1/ey1-xt2/ey2))
        y3hib=log((2d0-xt1*ey1-xt2*ey2)/xt3)
        y3min=dmax1(y3loa,y3lob,-rapmaxj)
        y3max=dmin1(y3hia,y3hib,rapmaxj)
        if (y3max.le.y3min) return
        call pick(y3,y3min,y3max,x(6),wty3,0)
*
        ey3=exp(y3)
        x1=0.5d0*(xt1*ey1+xt2*ey2+xt3*ey3)
        x2=0.5d0*(xt1/ey1+xt2/ey2+xt3/ey3)
	if (x1.gt.1d0.or.x1.lt.0d0.or.x2.gt.1d0.or.x2.lt.0d0) return
	sab=x1*x2*w**2
        wtps=wtpt12*wtpt22*wtphi*wty1*wty2*wty3/8d0/(2d0*pi)**4/sab	
*
*     final state momentum 
*
        r1=rn(1.)
        phi=2d0*pi*r1
        ppar(1,3)=pt1*cos(phi)
        ppar(2,3)=pt1*sin(phi)
        ppar(3,3)=0.5d0*pt1*(ey1-1d0/ey1)
        ppar(4,3)=0.5d0*pt1*(ey1+1d0/ey1)
        ppar(1,4)=pt2*cos(phi+phi12)
        ppar(2,4)=pt2*sin(phi+phi12)
        ppar(3,4)=0.5d0*pt2*(ey2-1d0/ey2)
        ppar(4,4)=0.5d0*pt2*(ey2+1d0/ey2)
        ppar(1,5)=-ppar(1,3)-ppar(1,4)
        ppar(2,5)=-ppar(2,3)-ppar(2,4)
        ppar(3,5)=0.5d0*pt3*(ey3-1d0/ey3)
        ppar(4,5)=0.5d0*pt3*(ey3+1d0/ey3)
*
* incoming parton pair momenta
*
        ppar(1,1)=0d0
        ppar(2,1)=0d0
        ppar(3,1)=w/2d0*x1
        ppar(4,1)=w/2d0*x1
        ppar(1,2)=0d0
        ppar(2,2)=0d0
        ppar(3,2)=-w/2d0*x2
        ppar(4,2)=w/2d0*x2
      endif
  100 continue
*
      ipass=1
*
      end
*
************************************************************************
*
      subroutine pick(x,xmin,xmax,r,wt,ial)
*      
*     select x on interval xmin to xmax according to dx / x**ial
*
      implicit double precision (a-h,o-z)
      al=float(ial)
      omal=1d0-al
      if(ial.eq.0)then
	x=xmin*(1d0-r)+xmax*r
	wt=xmax-xmin
      elseif(ial.eq.1)then
	x=xmin**(1d0-r)*xmax**r
	wt=dlog(xmax/xmin)*x
      else
	x=(xmin**omal*(1d0-r)+xmax**omal*r)**(1d0/omal)
	wt=(xmax**omal-xmin**omal)/omal*x**ial
      endif
*
      end
*
************************************************************************
*
      subroutine genpt(x,npar,wtps,ipass)
*
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /thcut/ smin,s0
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /phypar/ w,ipp1,ipp2,qcdl
      common /fraction/ x1,x2,facscale,renscale
      common /parmom/ ppar(4,10)
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      dimension x(10)
*
      shad=w**2
*      mjets=max0(2,njets)
*      scale=(mjets*etminj)**2/shad
*
* actually expect scale > [(1+ cos delrjj)*etminj]**2/shad
*
      if(njets.eq.1)then
*         scale=((1d0+dcos(delrjj))*etminj)**2/shad
         scale=(njets*etminj)**2/shad
      elseif(njets.ge.2)then
         scale=(njets*etminj)**2/shad
      endif
      ipass=0
      if(npar.ne.3)return
*
* generate x1 & x2
*
      r1=sqrt(x(1))
      r2=x(2)
      x1=scale**(1-r1)
      x2=scale**(r1*r2)
      wgt=log(scale)**2/2d0
      sab=x1*x2*shad
*
* incoming parton pair momenta
*
      ppar(1,1)=0d0
      ppar(2,1)=0d0
      ppar(3,1)=w/2d0*x1
      ppar(4,1)=w/2d0*x1
      ppar(1,2)=0d0
      ppar(2,2)=0d0
      ppar(3,2)=-w/2d0*x2
      ppar(4,2)= w/2d0*x2
      ymin=smin/sab
      y0=s0/sab 
      yab=1d0
*
*
*         if(dmin1(yb2,ya2,yb3,ya3,y23).lt.0d0)return
*         if(dmin1(ya1,yb1,y12,y13).lt.y0)return
* y23
*
         y23mn=0d0
	 y23mx=yab-2d0*y0     ! y23 = yab - y12 - y13
         if (y23mn.gt.y23mx) return
*
* dy23/(yab-y23)
*
	 r1=x(3)
	 y23=yab-(yab-y23mx)**r1*(yab-y23mn)**(1d0-r1)
	 wt23=log((yab-y23mn)/(yab-y23mx))*(yab-y23)
*
* y12
*
         y12mn=y0
	 y12mx=yab-y23-y0     ! = y13mn
         if (y12mn.gt.y12mx) return
*
* dy12/y12/(yab-y23-y12)
*
	 r1=x(4)
	 c1=(y12mx/y12mn)**(2d0*r1-1d0) 
	 y12=(yab-y23)*c1/(1d0+c1)
	 y13=yab-y12-y23
	 wt12=2d0*log(y12mx/y12mn)*y12*(yab-y23-y12)/(yab-y23) 
*
* yb1
*
         yb1mn=y0     
 	 yb1mx=yab-y23-y0       ! yb1=yab-y23-ya1   === ya1mn
         if (yb1mn.gt.yb1mx) return
*
* dyb1/yb1/(yab-y23-yb1)
*
 	 r1=x(5)
 	 c1=(yb1mx/yb1mn)**(2d0*r1-1d0) 
 	 yb1=(yab-y23)*c1/(1d0+c1)
	 ya1=yab-y23-yb1
 	 wtb1=2d0*log(yb1mx/yb1mn)*yb1*(yab-y23-yb1)/(yab-y23) 
*
* ya3
*
*         ya3 = yb1 + y23 - ya2
*         ya3 = yab - y12 - yb3
*         ya3 = yb1 + yb2 - y12
*	 ya3mx=min(yb1+y23-y0,yab-y12-y0)
*	 ya3mn=max(y0,yb1+y0-y12)
	 ya3mx=min(yb1+y23,yab-y12)
	 ya3mn=max(0d0,yb1-y12)
* constraint from the gram determinant
	 a=(yab-y23)/yab
	 b=y12/(yab-y23)
	 c=yb1/(yab-y23)
	 d0=+(1d0-a)*b*(1d0-c)+c*(1d0-b)
	 d1=2d0*sqrt((1d0-a)*b*(1d0-b)*c*(1d0-c))
	 ya3mn=max(ya3mn,d0-d1)
	 ya3mx=min(ya3mx,d0+d1)
         if (ya3mn.gt.ya3mx) return
*
* dya3/sqrt(d0+d1-ya3)/sqrt(ya3-(d0-d1))
*
         thmx=dacos(dmax1((ya3mn-d0)/d1,-1d0))
         thmn=dacos(dmin1((ya3mx-d0)/d1,1d0))
	 r1=x(6)
	 theta=thmn+r1*(thmx-thmn)
	 ya3=d0+d1*cos(theta)
	 wta3=(thmx-thmn)*sqrt((d0+d1-ya3)*(ya3-(d0-d1)))
* generate other 5 invariants	 
	 yb2=ya3-yb1+y12
	 ya2=yb1-ya3+y23
	 yb3=yab-y12-ya3
         if(dmin1(yb2,ya2,yb3,ya3,y23).lt.0d0)return
         if(dmin1(ya1,yb1,y12,y13).lt.y0)return
*	 
         delta=-(yab-y23)**2*(d0+d1-ya3)*(ya3-(d0-d1))/16d0
         if(delta.ge.0d0)return
         wtps=3d0*wgt*wt23*wt12*wtb1*wta3
     .  	 /sqrt(-delta)*pi/16d0*sab/(2d0*pi)**5
*	 
*
* reconstruct momenta from invariants
*
         xa1=sab*ya1/x1/w 
         xb1=sab*yb1/x2/w 
         et1=sqrt(xa1*xb1)
	 r1=rn(1.)
         phi=r1*2d0*pi
         cphi=cos(phi)
         sphi=sin(phi)
         xa2=sab*ya2/x1/w 
         xb2=sab*yb2/x2/w 
         et2=sqrt(xa2*xb2)
         c12=(xa1*xb2+xb1*xa2-y12*sab)/2d0/et1/et2
	 c12=dmax1(c12,-1d0)
	 c12=dmin1(c12,1d0)
         ex2=et2*c12
	 s12=sqrt(1d0-c12**2)
* allow for phi1 > phi2 or phi1 < phi2
	 if (rn(1.).gt.0.5d0) s12=-s12
         ey2=et2*s12
*
* symmetrize with respect to final state momenta
*
         r=rn(1.)
	 sixth=1d0/6d0 
	 if(r.lt.sixth)then
	   i1=1
	   i2=2
	   i3=3
	 elseif(r.ge.sixth.and.r.lt.2d0*sixth)then
	   i1=2
	   i2=3
	   i3=1
	 elseif(r.ge.2d0*sixth.and.r.lt.3d0*sixth)then
	   i1=3
	   i2=1
	   i3=2
	 elseif(r.ge.3d0*sixth.and.r.lt.4d0*sixth)then
	   i1=1
	   i2=3
	   i3=2
	 elseif(r.ge.4d0*sixth.and.r.lt.5d0*sixth)then
	   i1=3
	   i2=2
	   i3=1
	 elseif(r.ge.5d0*sixth.and.r.le.6d0*sixth)then
	   i1=2
	   i2=1
	   i3=3
	 endif
	 i1=i1+2
	 i2=i2+2
	 i3=i3+2	   
         ppar(4,i1)=(xa1+xb1)/2d0
         ppar(3,i1)=(xb1-xa1)/2d0
         ppar(1,i1)=cphi*et1
         ppar(2,i1)=sphi*et1
         ppar(4,i2)=(xa2+xb2)/2d0
         ppar(3,i2)=(xb2-xa2)/2d0
         ppar(1,i2)=cphi*ex2-sphi*ey2
         ppar(2,i2)=sphi*ex2+cphi*ey2
*
         do i=1,4
	   ppar(i,i3)=ppar(i,1)+ppar(i,2)-ppar(i,i1)-ppar(i,i2)
         enddo
*
      et3=sqrt(ppar(1,i3)**2+ppar(2,i3)**2)
      if(et1.gt.et2.or.et1.gt.et3)return
      ipass=1
*
      end
*
*
************************************************************************
*
      subroutine qcdjet(npar,nloop,rint)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /structure/ f1(13),f2(13),fx1(13),fx2(13)
      common /sigma/ sigma0(13,13),sigma1(13,13)
      dimension s(13)
      data s / 6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,16d0 /
*
* initialise structure functions
*
      call parden(nloop)
*
* initialise matrix elements
*
      rint=0d0
      call qcdmat(npar,nloop)
*
* multiply matrix elements and structure functions
*
      if (nloop.eq.0) then
        do  i=1,13
          do  j=1,13
 	    rint=rint+f1(i)*f2(j)*sigma0(i,j)/s(i)/s(j)
        enddo
      enddo
      elseif (nloop.eq.1) then
      	do  i=1,13
	  do  j=1,13
 	    rint=rint+(f1(i)*f2(j)*sigma1(i,j)
     .	              +(f1(i)*fx2(j)+fx1(i)*f2(j))*sigma0(i,j))
     .		     /s(i)/s(j)
          enddo
	enddo  
      endif
* 
      end
*
************************************************************************
*
* structure functions - links to struct.f 
*                            and cstruct.f
*
* computes parton densities and places them in f1, f2, fx1 and fx2
* according to partonid
*
      subroutine parden(nloop)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /thcut/ smin,s0
      common /phypar/ w,ipp1,ipp2,qcdl
      common /fraction/ x1,x2,facscale,renscale
      common /inppar/ crenorm,cfact,njets,mloop,istruc,irenorm,ifact
      common /structure/ f1(13),f2(13),fx1(13),fx2(13)
      common /partonid/ iu,id,is,ic,ib,it,iub,idb,isb,icb,ibb,itb,ig
      common /koppel/ nf,nc,b0
      common /order/ iorder
*
* setting up parton id numbers 
*
      data iu,id,is,ic,ib,it,iub,idb,isb,icb,ibb,itb,ig 
     .    / 1, 2, 3, 4, 5, 6,  7,  8,  9, 10, 11, 12,13/
*
      call struct(x1,facscale,istruc
     .              ,upv1,dnv1,ups1,dns1,str1,chm1,bot1,glu1)
      call struct(x2,facscale,istruc
     .              ,upv2,dnv2,ups2,dns2,str2,chm2,bot2,glu2)
*
* x1-hadron content  (ipp1=0 -> proton, ipp1=1 -> antiproton)
*
      if (ipp1.eq.0) then
        f1(iu )=upv1+ups1
        f1(iub)=ups1
	f1(id )=dnv1+dns1
	f1(idb)=dns1
      else
        f1(iu )=ups1
        f1(iub)=upv1+ups1
	f1(id )=dns1
	f1(idb)=dnv1+dns1
      endif
      f1(is )=str1
      f1(isb)=str1
      f1(ic )=chm1
      f1(icb)=chm1
      f1(ib )=bot1 
      f1(ibb)=bot1 
      f1(it )=0d0
      f1(itb)=0d0
      f1(ig )=glu1
*
* x2-hadron content (ipp2=0 -> proton, ipp2=1 -> anti proton)
*
      if (ipp2.eq.0) then
        f2(iu )=upv2+ups2
        f2(iub)=ups2
	f2(id )=dnv2+dns2
	f2(idb)=dns2
      else
        f2(iu )=ups2
        f2(iub)=upv2+ups2
	f2(id )=dns2
	f2(idb)=dnv2+dns2
      endif
      f2(is )=str2
      f2(isb)=str2
      f2(ic )=chm2
      f2(icb)=chm2
      f2(ib )=bot2 
      f2(ibb)=bot2 
      f2(it )=0d0
      f2(itb)=0d0
      f2(ig )=glu2
*     
* fill the xing functions (only if nloop=1)
*
      if (nloop.eq.1) then
      call cstruct(x1,facscale,istruc
     .               ,upa1,dna1,usa1,dsa1,sta1,cha1,boa1,gla1
     .               ,upb1,dnb1,usb1,dsb1,stb1,chb1,bob1,glb1)
      call cstruct(x2,facscale,istruc
     .               ,upa2,dna2,usa2,dsa2,sta2,cha2,boa2,gla2
     .               ,upb2,dnb2,usb2,dsb2,stb2,chb2,bob2,glb2)
*
* x1-xing function
*
         rlg=log(smin/facscale**2)
*
* coupling constant
*
         if(iorder.eq.0)as=alfas(facscale,qcdl,1)
         if(iorder.eq.1)as=alfas(facscale,qcdl,2) 
         al=as*nc/2d0/pi
         upv1=al*(rlg*upa1+upb1)
         dnv1=al*(rlg*dna1+dnb1)
         ups1=al*(rlg*usa1+usb1)
         dns1=al*(rlg*dsa1+dsb1)
         str1=al*(rlg*sta1+stb1)
         chm1=al*(rlg*cha1+chb1)
         bot1=al*(rlg*boa1+bob1) 
         glu1=al*(rlg*gla1+glb1)
*
         upv2=al*(rlg*upa2+upb2)
         dnv2=al*(rlg*dna2+dnb2)
         ups2=al*(rlg*usa2+usb2)
         dns2=al*(rlg*dsa2+dsb2)
         str2=al*(rlg*sta2+stb2)
         chm2=al*(rlg*cha2+chb2)
         bot2=al*(rlg*boa2+bob2) 
         glu2=al*(rlg*gla2+glb2)
*
      if (ipp1.eq.0) then
        fx1(iu )=upv1+ups1
        fx1(iub)=ups1
	fx1(id )=dnv1+dns1
	fx1(idb)=dns1
      else
        fx1(iu )=ups1
        fx1(iub)=upv1+ups1
	fx1(id )=dns1
	fx1(idb)=dnv1+dns1
      endif
      fx1(is )=str1
      fx1(isb)=str1
      fx1(ic )=chm1
      fx1(icb)=chm1
      fx1(ib )=bot1
      fx1(ibb)=bot1
      fx1(it )=0d0
      fx1(itb)=0d0
      fx1(ig )=glu1
*
* x2-hadron content (ipp2=0 -> proton, ipp2=1 -> anti proton)
*
      if (ipp2.eq.0) then
        fx2(iu )=upv2+ups2
        fx2(iub)=ups2
	fx2(id )=dnv2+dns2
	fx2(idb)=dns2
      else
        fx2(iu )=ups2
        fx2(iub)=upv2+ups2
	fx2(id )=dns2
	fx2(idb)=dnv2+dns2
      endif
      fx2(is )=str2
      fx2(isb)=str2
      fx2(ic )=chm2
      fx2(icb)=chm2
      fx2(ib )=bot2
      fx2(ibb)=bot2
      fx2(it )=0d0
      fx2(itb)=0d0
      fx2(ig )=glu2
      endif
*
      end
************************************************************************
*
* matrix elements for vacuum -> partons
*
* note that all momenta are treated as if in the final state
*
      subroutine qcdmat(npar,nloop)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /thcut/ smin,s0
      common /koppel/ nf,nc,b0
      common /sigma/ sigma0(13,13),sigma1(13,13)
      common /partonid/ iu,id,is,ic,ib,it,iub,idb,isb,icb,ibb,itb,ig
      common /fraction/ x1,x2,facscale,renscale
      common /strong/ as
      common /sin/ s(10,10),th(10,10)
      common /parmom/ ppar(4,10)
      common /phypar/ w,ipp1,ipp2,qcdl
      common /order/ iorder
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      dimension r0(0:1),r1(0:1),r2(0:1),r3(0:1)
      dimension idg(0:10)
*
* coupling constant
*
      if(iorder.eq.0)as=alfas(renscale,qcdl,1)
      if(iorder.eq.1)as=alfas(renscale,qcdl,2)
*
* initialise invariants
*
      do 1 i=1,npar+1
      do 1 j=i+1,npar+2
         s(i,j)=2d0*(ppar(4,i)*ppar(4,j)-ppar(1,i)*ppar(1,j)
     .              -ppar(2,i)*ppar(2,j)-ppar(3,i)*ppar(3,j))
	 if(i.eq.1.or.i.eq.2)s(i,j)=-s(i,j)
	 if(j.eq.1.or.j.eq.2)s(i,j)=-s(i,j)
	 s(j,i)=s(i,j)
         th(i,j)=theta(abs(s(i,j))-smin)
         th(j,i)=th(i,j)
    1 continue
*
* identical particle averaging  factors for gluons put in idg(), 
* for quarks/antiquarks put in line by line
* minus signs for qg initial state also in by hand
*
      idg(0)=1
      idg(1)=1
      do 2 i=2,npar
        idg(i)=idg(i-1)*i
    2 continue
*
* zero sigma
*
      do 5 i=1,13
        do 5 j=1,13
          sigma0(i,j)=0d0
          sigma1(i,j)=0d0
    5 continue
*
* q-qb and qb-q initial state
*
* q qb -> g g
      call gnq2(2,1,npar,nloop,r0)  
* qb q -> g g  = conjg (q qb -> g g)
      do 10 iq=1,6
	iqb=iq+6
        sigma0(iq, iqb)=sigma0( iq,iqb)+r0(0)/idg(npar)
        sigma0(iqb, iq)=sigma0(iqb, iq)+r0(0)/idg(npar)
    	if (nloop.eq.1) then
          sigma1( iq,iqb)=sigma1( iq,iqb)+r0(1)/idg(npar)
          sigma1(iqb, iq)=sigma1(iqb, iq)+r0(1)/idg(npar)
	endif
   10 continue
* q qb -> qp qbp unlike quarks
      call gnq2q2(2,1,3,4,npar,nloop,r0)  
* q qb -> q qb     like quarks
      call gnq4  (2,1,3,4,npar,nloop,r1)  
* qb q -> qp qbp unlike quarks = conjg (q qb -> qp qbp )
* qb q -> qb q     like quarks = conjg (q qb -> q qb )
      do 12 iq=1,6
        iqb=iq+6
        sigma0( iq,iqb)=sigma0( iq,iqb)+r0(0)*(nf-1)/idg(npar-2)
        sigma0( iq,iqb)=sigma0( iq,iqb)+r1(0)/idg(npar-2)
        sigma0(iqb, iq)=sigma0(iqb, iq)+r0(0)*(nf-1)/idg(npar-2)
        sigma0(iqb, iq)=sigma0(iqb, iq)+r1(0)/idg(npar-2)
    	if (nloop.eq.1) then
	  sigma1( iq,iqb)=sigma1( iq,iqb)+r0(1)*(nf-1)/idg(npar-2)
	  sigma1( iq,iqb)=sigma1( iq,iqb)+r1(1)/idg(npar-2)
	  sigma1(iqb, iq)=sigma1(iqb, iq)+r0(1)*(nf-1)/idg(npar-2)
	  sigma1(iqb, iq)=sigma1(iqb, iq)+r1(1)/idg(npar-2)
	endif
   12 continue
* q qbp -> q qbp
      call gnq2q2(3,1,2,4,npar,nloop,r0)  
* qb qp -> qb qp = conjg (q qbp -> q qbp)
      do 14 iq1=1,6
	do 14 iq2=1,6
	  iqb2=iq2+6
	  if(iq2.eq.iq1)goto 14
	    sigma0( iq1,iqb2)=sigma0( iq1,iqb2)+r0(0)/idg(npar-2)
	    sigma0(iqb2, iq1)=sigma0(iqb2, iq1)+r0(0)/idg(npar-2)
	    if(nloop.eq.1)then
	      sigma1( iq1,iqb2)=sigma1( iq1,iqb2)+r0(1)/idg(npar-2)
	      sigma1(iqb2, iq1)=sigma1(iqb2, iq1)+r0(1)/idg(npar-2)
	    endif
   14   continue
*
* q-g and g-q initial state
*
* q g -> q g
      call gnq2(3,1,npar,nloop,r0)
* g q -> g q
      call gnq2(4,2,npar,nloop,r1)
*
* qb-g and g-qb initial state
*
* qb g -> qb g = conjg (q g -> q g)
* g qb -> g qb = conjg (g q -> g q)
      do 20 iq=1,6
          iqb=iq+6
          sigma0( iq, ig)=sigma0( iq, ig)-r0(0)/idg(npar-1)
          sigma0( ig, iq)=sigma0( ig, iq)-r1(0)/idg(npar-1)
          sigma0(iqb, ig)=sigma0(iqb, ig)-r0(0)/idg(npar-1)
          sigma0( ig,iqb)=sigma0( ig,iqb)-r1(0)/idg(npar-1)
    	  if (nloop.eq.1) then
	    sigma1( iq, ig)=sigma1( iq, ig)-r0(1)/idg(npar-1)
            sigma1( ig, iq)=sigma1( ig, iq)-r1(1)/idg(npar-1)
	    sigma1(iqb, ig)=sigma1(iqb, ig)-r0(1)/idg(npar-1)
            sigma1( ig,iqb)=sigma1( ig,iqb)-r1(1)/idg(npar-1)
          endif
   20 continue
* allow q g -> q q qb in npar >= 3 and therefore nloop = 0
      if(npar.ge.3)then
* q g -> qp qbp q
        call gnq2q2(5,1,3,4,npar,nloop,r0)
* q g -> q qb q 
        call gnq4  (5,1,3,4,npar,nloop,r1)
* g q -> qp qbp q
        call gnq2q2(5,2,3,4,npar,nloop,r2)
* g q -> q qb q 
        call gnq4  (5,2,3,4,npar,nloop,r3)
*
* qb-g and g-qb initial state
*
* qb g -> qbp qp qb = conjg(q g -> qp qbp q)
* qb g -> q qb qb   = conjg(q g -> qb q q)
* g qb -> qp qbp qb = conjg(g q -> qbp qp q )
* g qb -> qb q qb   = conjg(g q -> q qb q)
        do 22 iq=1,6
          iqb=iq+6
	  sigma0( iq, ig)=sigma0( iq, ig)-r0(0)*(nf-1)/idg(npar-3)
	  sigma0( iq, ig)=sigma0( iq, ig)-r1(0)/idg(npar-3)/2
	  sigma0( ig, iq)=sigma0( ig, iq)-r2(0)*(nf-1)/idg(npar-3)
	  sigma0( ig, iq)=sigma0( ig, iq)-r3(0)/idg(npar-3)/2
	  sigma0(iqb, ig)=sigma0(iqb, ig)-r0(0)*(nf-1)/idg(npar-3)
	  sigma0(iqb, ig)=sigma0(iqb, ig)-r1(0)/idg(npar-3)/2
	  sigma0( ig,iqb)=sigma0( ig,iqb)-r2(0)*(nf-1)/idg(npar-3)
          sigma0( ig,iqb)=sigma0( ig,iqb)-r3(0)/idg(npar-3)/2
   22   continue
      endif
*
* q-q initial state
*
* q qp -> qp q
      call gnq2q2(4,1,3,2,npar,nloop,r0)
* q q  -> q q
      call gnq4  (4,1,3,2,npar,nloop,r1)
*
* qb-qb initial state
*
* qbp qb -> qb qbp = conjg(q qp -> qp q)
* qb qb  -> qb qb = conjg(q q  -> q q)

      do 40 iq1=1,6
        do 40 iq2=1,6
          iqb1=iq1+6
           iqb2=iq2+6
          if(iq1.ne.iq2)then
            sigma0( iq1, iq2)=sigma0( iq1, iq2)+r0(0)/idg(npar-2)
            sigma0(iqb1,iqb2)=sigma0(iqb1,iqb2)+r0(0)/idg(npar-2)
          elseif(iq1.eq.iq2)then
            sigma0( iq1, iq2)=sigma0( iq1, iq2)+r1(0)/idg(npar-2)/2
            sigma0(iqb1,iqb2)=sigma0(iqb1,iqb2)+r1(0)/idg(npar-2)/2
          endif
          if (nloop.eq.1) then
            if(iq1.ne.iq2)then
              sigma1( iq1, iq2)=sigma1( iq1, iq2)+r0(1)/idg(npar-2)
              sigma1(iqb1,iqb2)=sigma1(iqb1,iqb2)+r0(1)/idg(npar-2)
            elseif(iq1.eq.iq2)then
              sigma1( iq1, iq2)=sigma1( iq1, iq2)+r1(1)/idg(npar-2)/2
              sigma1(iqb1,iqb2)=sigma1(iqb1,iqb2)+r1(1)/idg(npar-2)/2
            endif
          endif
   40 continue
*
* g-g initial state
*
* g g -> q qb
      call gnq2(3,4,npar,nloop,r0)
* g g -> g g
      call gnq0(npar,nloop,r1)
      sigma0(ig,ig)=sigma0(ig,ig)+r0(0)*nf/idg(npar-2)
      sigma0(ig,ig)=sigma0(ig,ig)+r1(0)/idg(npar)
      if (nloop.eq.1) then
        sigma1(ig,ig)=sigma1(ig,ig)+r0(1)*nf/idg(npar-2)
        sigma1(ig,ig)=sigma1(ig,ig)+r1(1)/idg(npar)
      endif
*
      end
*
************************************************************************
*
* zero quark matrix elements
*
      subroutine gnq0(npar,nloop,r)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /koppel/ nf,nc,b0
      common /sin/ s(10,10),th(10,10)
      common /strong/ as
      common /fraction/ x1,x2,facscale,renscale
      common /thcut/ smin,s0
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /proc/ isub(4)
      dimension r(0:1),ig(10)
      r(0)=0d0
      r(1)=0d0
      if(isub(1).eq.0)return
*
* find where gluon momenta are stored
*
      j=0
      do 10 i=1,npar+2
	j=j+1
	ig(j)=i
   10 continue
* 
      s12=s(ig(1),ig(2))
      s13=s(ig(1),ig(3))
      s14=s(ig(1),ig(4))
      s23=s(ig(2),ig(3))
      s24=s(ig(2),ig(4))
      s34=s(ig(3),ig(4))
      s21=s12
      s31=s13
      s41=s14
      s32=s23
      s42=s24
      s43=s34  
      if(npar.eq.2)then
        hs=4d0*(s12**4+s13**4+s23**4)
	t123=1d0/s12**2/s23**2
	t132=1d0/s13**2/s23**2
	t213=1d0/s12**2/s13**2
	fac=(2d0*pi*as*nc)**2*(nc**2-1d0)*4d0
	r(0)=fac*hs*(t123+t132+t213)
 	if(nloop.eq.1)then
 	  renterm=4d0*b0*log(renscale**2/smin)*as
*
	  rk123=134d0/9d0-20d0*nf/9d0/nc-4d0*pi**2/3d0
     .       -2d0*log(abs(s12)/smin)**2-2d0*log(abs(s23)/smin)**2
     .       +2d0*(log(abs(s12)/renscale**2)**2)/2d0
     .       +2d0*(log(abs(s23)/renscale**2)**2)/2d0
	  rk132=134d0/9d0-20d0*nf/9d0/nc-4d0*pi**2/3d0
     .       -2d0*log(abs(s13)/smin)**2-2d0*log(abs(s23)/smin)**2  
     .       +2d0*(log(abs(s13)/renscale**2)**2)/2d0
     .       +2d0*(log(abs(s23)/renscale**2)**2)/2d0
	  rk213=134d0/9d0-20d0*nf/9d0/nc-4d0*pi**2/3d0
     .       -2d0*log(abs(s12)/smin)**2-2d0*log(abs(s13)/smin)**2
     .       +2d0*(log(abs(s12)/renscale**2)**2)/2d0
     .       +2d0*(log(abs(s13)/renscale**2)**2)/2d0
*	  
          rk123=as*nc/2d0/pi*rk123+renterm
 	  rk132=as*nc/2d0/pi*rk132+renterm
 	  rk213=as*nc/2d0/pi*rk213+renterm
*
          rf123=-67d0/9d0+pi**2+10d0*nf/9d0/nc 
          rf132=-67d0/9d0+pi**2+10d0*nf/9d0/nc 
          rf213=-67d0/9d0+pi**2+10d0*nf/9d0/nc 
*
          rf=as*nc/2d0/pi*
     .        (fd(s12,s23,s13)+fd(s23,s13,s12)+fd(s13,s12,s23)
     .         +rf123*hs*t123+rf132*hs*t132+rf213*hs*t213)
	  r(1)=fac*((1d0+rk123)*hs*t123
     .             +(1d0+rk132)*hs*t132
     .             +(1d0+rk213)*hs*t213 + rf )  
  	endif
      elseif(npar.eq.3)then
        s15 =s(ig(1),ig(5))
        s25 =s(ig(2),ig(5))
        s35 =s(ig(3),ig(5))
        s45 =s(ig(4),ig(5))
        s51=s15
	s52=s25
	s53=s35
	s54=s45
        th12=th(ig(1),ig(2))
        th13=th(ig(1),ig(3))
        th14=th(ig(1),ig(4))
        th23=th(ig(2),ig(3))
        th24=th(ig(2),ig(4))
        th34=th(ig(3),ig(4))
        th15=th(ig(1),ig(5))
        th25=th(ig(2),ig(5))
        th35=th(ig(3),ig(5))
        th45=th(ig(4),ig(5))
        th21=th12
        th31=th13
        th41=th14
        th51=th15
        th32=th23
        th42=th24
        th52=th25
        th43=th34
        th53=th35
        th54=th45

	hs=8d0*(s12**4+s13**4+s14**4+s15**4+s23**4
     .         +s24**4+s25**4+s34**4+s35**4+s45**4)
        
	t1234=1d0/s12/s23/s34/s45/s51*th12*th23*th34*th45*th51
	t1243=1d0/s12/s24/s43/s35/s51*th12*th24*th43*th35*th51
	t1423=1d0/s14/s42/s23/s35/s51*th14*th42*th23*th35*th51
	t1432=1d0/s14/s43/s32/s25/s51*th14*th43*th32*th25*th51
	t1342=1d0/s13/s34/s42/s25/s51*th13*th34*th42*th25*th51
	t1324=1d0/s13/s32/s24/s45/s51*th13*th32*th24*th45*th51
	t2134=1d0/s21/s13/s34/s45/s52*th21*th13*th34*th45*th52
	t2143=1d0/s21/s14/s43/s35/s52*th21*th14*th43*th35*th52
	t2413=1d0/s24/s41/s13/s35/s52*th24*th41*th13*th35*th52
	t2314=1d0/s23/s31/s14/s45/s52*th23*th31*th14*th45*th52
	t3124=1d0/s31/s12/s24/s45/s53*th31*th12*th24*th45*th53
	t3214=1d0/s32/s21/s14/s45/s53*th32*th21*th14*th45*th53

   	fac=(2d0*pi*as*nc)**3*(nc**2-1d0)*4d0
	r(0)=fac*hs*(t1234+t1243+t1423+t1432+t1342+t1324
     .              +t2134+t2143+t2413+t2314+t3124+t3214)
      endif
*
      end
*
************************************************************************
*
* four gluon matrix elements : finite part of virtual
*
      function fd(s12,s23,s13)
      implicit real*8(a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /koppel/ nf,nc,b0
      common /fraction/ x1,x2,facscale,renscale
      s=s12/renscale**2 
      t=s23/renscale**2 
      u=s13/renscale**2 
      finite = 
     .  nc*(2d0*(t**2+u**2)/t/u*(log(abs(s))**2-pi**2*theta(s))
     . +(4d0*s*(t**3+u**3)/u**2/t**2-6d0)*log(abs(t))*log(abs(u))
     . +(4d0/3d0*u*t/s**2-14d0/3d0*(t**2+u**2)/u/t
     . -14d0-8d0*(t**2/u**2+u**2/t**2))*log(abs(s))-1d0-pi**2)
     . +nf/2d0*((10d0/3d0*(t**2+u**2)/u/t
     .       +16d0/3d0*u*t/s**2-2d0)*log(abs(s))
     .       -(s**2+u*t)/u/t*(log(abs(s))**2-pi**2*theta(s))
     .       -2d0*(t**2+u**2)/u/t*log(abs(u))*log(abs(t)) 
     .       +2d0 -pi**2) 
      corr =
     . + 8d0*log(abs(s))*(3d0-2d0*u*t/s**2+(t**4+u**4)/t**2/u**2)
      fd=4d0*finite/nc+4d0*corr  
      return
      end
************************************************************************
*
* two quark matrix elements
*
      subroutine gnq2(iq,iqb,npar,nloop,r)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /thcut/ smin,s0
      common /koppel/ nf,nc,b0
      common /sin/ s(10,10),th(10,10)
      common /strong/ as
      common /fraction/ x1,x2,facscale,renscale
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /proc/ isub(4)
      dimension r(0:1),ig(10)
      r(0)=0d0
      r(1)=0d0
      if(isub(2).eq.0)return
*
* find where gluon momenta are stored
*
      j=0
      do 10 i=1,npar+2
        if(i.ne.iq.and.i.ne.iqb)then
	  j=j+1
	  ig(j)=i
	endif
   10 continue
*   
      sq1 =s(iq,ig(1))
      sq2 =s(iq,ig(2))
      s1qb=s(iqb,ig(1))
      s2qb=s(iqb,ig(2))
      s12 =s(ig(1),ig(2))
      s21 =s12
      sqqb=s(iq,iqb)
      if(npar.eq.2)then
        hs=8d0*(sq1**3*sq2+sq1*sq2**3)
	t12=1d0/sq1**2/s12**2 
	t21=1d0/sq2**2/s12**2 
	t0 =1d0/sq1**2/sq2**2 
	fac=(2d0*pi*as*nc)**2*(nc**2-1d0)/nc
	r(0)=fac*hs*(t12+t21-t0/nc**2)
 	if(nloop.eq.1)then
	  renterm=2d0*b0*log(renscale**2/smin)*as 
*
	  r12=197d0/18d0-10d0*nf/9d0/nc-pi**2 
     . 	     +3d0/2d0*log(renscale**2/smin)   
     .       -2d0*log(abs(sq1)/smin)**2-log(abs(s12)/smin)**2  
     .       +2d0*(log(abs(sq1)/renscale**2)**2)/2d0  
     .       +1d0*(log(abs(s12)/renscale**2)**2)/2d0  
	  r21=197d0/18d0-10d0*nf/9d0/nc-pi**2 
     . 	     +3d0/2d0*log(renscale**2/smin)   
     .       -2d0*log(abs(sq2)/smin)**2-log(abs(s12)/smin)**2  
     .       +2d0*(log(abs(sq2)/renscale**2)**2)/2d0  
     .       +1d0*(log(abs(s12)/renscale**2)**2)/2d0  
          r1 =130d0/18d0-5d0*nf/9d0/nc-2d0*pi**2/3d0
     . 	     +3d0/2d0*log(renscale**2/smin)   
     .       -log(abs(sq1)/smin)**2-log(abs(sq2)/smin)**2  
     .       +1d0*(log(abs(sq1)/renscale**2)**2)/2d0
     .       +1d0*(log(abs(sq2)/renscale**2)**2)/2d0
          r2 =130d0/18d0-5d0*nf/9d0/nc-2d0*pi**2/3d0
     . 	     +3d0/2d0*log(renscale**2/smin)   
     .       -log(abs(sq1)/smin)**2-log(abs(sq2)/smin)**2  
     .       +1d0*(log(abs(sq1)/renscale**2)**2)/2d0
     .       +1d0*(log(abs(sq2)/renscale**2)**2)/2d0
          r0 =63d0/18d0-pi**2/3d0 
     . 	     +3d0/2d0*log(renscale**2/smin)   
     .       -log(abs(s12)/smin)**2
     .       +1d0*(log(abs(s12)/renscale**2)**2)/2d0
*
	  rk12=r12-r0/nc**2
	  rk21=r21-r0/nc**2
	  rk0 =r1+r2-r0-r0/nc**2
*
          rf12=-(nc**2-1d0)/nc**2*7d0/2d0 
          rf21=-(nc**2-1d0)/nc**2*7d0/2d0 
          rf0 =-(nc**2-1d0)/nc**2*7d0/2d0 
*
          rk12=as*nc/2d0/pi*rk12+renterm
 	  rk21=as*nc/2d0/pi*rk21+renterm
 	  rk0 =as*nc/2d0/pi*rk0 +renterm
   	  rf=as*nc/2d0/pi*(
     .      hs*(t12*rf12+t21*rf21-t0*rf0/nc**2)
     .     +fc(s12,sq1,sq2)+fc(s12,sq2,sq1)     )
	  r(1)=fac*((1d0+rk12)*hs*t12
     .             +(1d0+rk21)*hs*t21
     .             -(1d0+rk0)*hs*t0/nc**2  +rf )
	endif
      elseif(npar.eq.3)then
        sq3 =s(iq,ig(3))
        s3qb=s(iqb,ig(3))
        s13 =s(ig(1),ig(3))
        s23 =s(ig(2),ig(3))
        s31 =s13
	s32 =s23

        thq1 =th(iq,ig(1))
        thq2 =th(iq,ig(2))
        th1qb=th(iqb,ig(1))
        th2qb=th(iqb,ig(2))
        th12 =th(ig(1),ig(2))
        thq3 =th(iq,ig(3))
        th3qb=th(iqb,ig(3))
        th13 =th(ig(1),ig(3))
        th23 =th(ig(2),ig(3))
        thqqb=th(iq,iqb)
        th21=th12
        th32=th23
        th31=th13

	hs=16d0*(sq1**3*s1qb+sq2**3*s2qb+sq3**3*s3qb
     .	        +sq1*s1qb**3+sq2*s2qb**3+sq3*s3qb**3)
	fac=(2d0*pi*as*nc)**3*(nc**2-1d0)/nc


    	t123=1d0/sq1/s12/s23/s3qb/sqqb*thq1*th12*th23*th3qb*thqqb
        t132=1d0/sq1/s13/s32/s2qb/sqqb*thq1*th13*th32*th2qb*thqqb
	t213=1d0/sq2/s21/s13/s3qb/sqqb*thq2*th21*th13*th3qb*thqqb
	t231=1d0/sq2/s23/s31/s1qb/sqqb*thq2*th23*th31*th1qb*thqqb
	t312=1d0/sq3/s31/s12/s2qb/sqqb*thq3*th31*th12*th2qb*thqqb
	t321=1d0/sq3/s32/s21/s1qb/sqqb*thq3*th32*th21*th1qb*thqqb
	t12 =1d0/sq1/s12/s2qb/sq3/s3qb*thq1*th12*th2qb*thq3*th3qb
	t21 =1d0/sq2/s21/s1qb/sq3/s3qb*thq2*th21*th1qb*thq3*th3qb
	t13 =1d0/sq1/s13/s3qb/sq2/s2qb*thq1*th13*th3qb*thq2*th2qb
	t31 =1d0/sq3/s31/s1qb/sq2/s2qb*thq3*th31*th1qb*thq2*th2qb
	t23 =1d0/sq2/s23/s3qb/sq1/s1qb*thq2*th23*th3qb*thq1*th1qb
	t32 =1d0/sq3/s32/s2qb/sq1/s1qb*thq3*th32*th2qb*thq1*th1qb
	t0  =1d0*sqqb/sq1/sq2/sq3/s1qb/s2qb/s3qb
     .       *thq1*thq2*thq3*th1qb*th2qb*th3qb
        r(0)=fac*hs*(t123+t132+t213+t231+t312+t321
     .             -(t12+t21+t13+t31+t23+t32)/nc**2
     .             +t0*(nc**2+1d0)/nc**4)
      endif
*
      end
*
************************************************************************
*
* two gluon matrix elements : finite part of virtual
*
      function fc(s12,s23,s13)
      implicit real*8(a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /koppel/ nf,nc,b0
      common /fraction/ x1,x2,facscale,renscale
      s=s12/renscale**2
      t=s23/renscale**2
      u=s13/renscale**2
      dlns=log(abs(s))
      dlnt=log(abs(t))
      dlnu=log(abs(u))
      finite = 
     .   +dlnt*dlnu/nc*(u**2+t**2)/2d0/t/u
     .   +(dlns**2-pi**2*theta(s))*(s**2/4d0/nc**3/u/t
     .                             +(1d0/2d0+(u**2+t**2)/u/t
     .                             -(u**2+t**2)/s**2)/4d0/nc
     .                             -nc*(u**2+t**2)/4d0/s**2)
     .   +dlns*((5d0*(nc**2-1d0)/8d0/nc-1d0/2d0/nc-1d0/nc**3)
     .               -(nc+1d0/nc**3)*(u**2+t**2)/2d0/t/u
     .               -(nc**2-1d0)/4d0/nc*(u**2+t**2)/s**2) 
     .   +pi**2*(1d0/8d0/nc
     .          +1d0/nc**3*(3d0*(u**2+t**2)/8d0/t/u+1d0/2d0)
     .          +nc*((u**2+t**2)/8d0/t/u-(u**2+t**2)/2d0/s**2)) 
     .   +(nc+1d0/nc)*(1d0/8d0-(u**2+t**2)/4d0/s**2)
     .   +(dlnt**2-pi**2*theta(t))*(nc*(s/4d0/t-u/s-1d0/4d0)
     .                             +1d0/nc*(t/2d0/u-u/4d0/s)
     .                             +1d0/nc**3*(u/4d0/t-s/2d0/u))
     .   +dlnt*(nc*((u**2+t**2)/s**2
     .               +3d0*t/4d0/s-5d0*u/4d0/t-1d0/4d0)
     .               -1d0/nc*(u/4d0/s+2d0*s/u+s/2d0/t)
     .               -1d0/nc**3*(3d0*s/4d0/t+1d0/4d0))
     .   +dlnt*dlns*(nc*((u**2+t**2)/s**2-u/2d0/t) 
     .                           +1d0/nc*(u/2d0/s-t/u)
     .                           +1d0/nc**3*(s/u-u/2d0/t))     
      corr =   
     .  2d0*(1d0-1d0/nc**2)*(u/t-u**2/s**2)*dlns
     . +1d0/nc**2*(1d0+1d0/nc**2)*(u/t+s**2/t/u/2d0)*dlns
     . +4d0*(u/t-u**2/s**2)*dlnt
     . -2d0/nc**2*(u/t+t/u+s**2/t/u)*dlnt 
      fc=16d0*finite/nc+8d0*corr
      return
      end
************************************************************************
*
* two distinct quark pair matrix elements
*
      subroutine gnq2q2(iq1,iqb1,iq2,iqb2,npar,nloop,r)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /thcut/ smin,s0
      common /koppel/ nf,nc,b0
      common /sin/ s(10,10),th(10,10)
      common /strong/ as
      common /fraction/ x1,x2,facscale,renscale
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /proc/ isub(4)
      dimension r(0:1),ig(10)
      r(0)=0d0
      r(1)=0d0
      if(isub(3).eq.0)return
*
* find where gluon momenta are stored
*
      j=0
      do 10 i=1,npar+2
        if(i.ne.iq1.and.i.ne.iqb1.and.i.ne.iq2.and.i.ne.iqb2)then
	  j=j+1
	  ig(j)=i
	endif
   10 continue
*   
      sq1q2  =s(iq1 ,iq2 )
      sq1qb1 =s(iq1 ,iqb1)
      sq1qb2 =s(iq1 ,iqb2)
      sq2qb1 =s(iq2 ,iqb1)       ! = sq1qb2 in 2 parton case
      sq2qb2 =s(iq2 ,iqb2)       ! = sq1qb1
      sqb1qb2=s(iqb1,iqb2)       ! = sq1q2
      if(npar.eq.2)then
        t=8d0*(sq1q2**2+sq1qb2**2)/sq1qb1**2
	fac=(2d0*pi*as*nc)**2*(nc**2-1d0)/nc**2
	r(0)=fac*t
 	if(nloop.eq.1)then
	  renterm=0d0*b0*log(renscale**2/smin)*as
*
          rq1q2=63d0/18d0-pi**2/3d0
     . 	   +3d0/2d0*log(renscale**2/smin)   
     .     -log(abs(sq1q2)/smin)**2
     .     +log(abs(sq1q2)/renscale**2)**2/2d0
          rq1qb1=63d0/18d0-pi**2/3d0
     . 	   +3d0/2d0*log(renscale**2/smin)   
     .     -log(abs(sq1qb1)/smin)**2
     .     +log(abs(sq1qb1)/renscale**2)**2/2d0
          rq1qb2=63d0/18d0-pi**2/3d0
     . 	   +3d0/2d0*log(renscale**2/smin)   
     .     -log(abs(sq1qb2)/smin)**2
     .     +log(abs(sq1qb2)/renscale**2)**2/2d0
          rq2qb1 =rq1qb2
          rq2qb2 =rq1qb1
          rqb1qb2=rq1q2
*
	  rk=(rq1qb2+rq2qb1+rq1qb1/nc**2+rq2qb2/nc**2)
     .     -2d0/nc**2*(rq1qb2+rq2qb1+rq1qb1+rq2qb2-rq1q2-rqb1qb2)
 	  rk=as*nc/2d0/pi*rk+renterm
   	  rf=as*nc/2d0/pi*fa(sq1q2,sq1qb1,sq1qb2)  
	  r(1)=fac*(1d0+rk+rf)*t  
	endif
      elseif(npar.eq.3)then
        sq11 = s(iq1, ig(1))
        s1qb1= s(iqb1,ig(1))
        sq21 = s(iq2, ig(1))
        s1qb2= s(iqb2,ig(1))
        thq1qb1 =th(iq1 ,iqb1)
        thq2qb2 =th(iq2 ,iqb2)        
        thq11  = th(iq1, ig(1))
        th1qb1 = th(iqb1,ig(1))
        thq21  = th(iq2, ig(1))
        th1qb2 = th(iqb2,ig(1))

	hs=16d0*(sq1q2**2+sqb1qb2**2+sq1qb2**2+sq2qb1**2)/sq1qb1/sq2qb2
	t1=hs*(sq1qb2/sq11/s1qb2*thq11*th1qb2
     .        +sq2qb1/sq21/s1qb1*thq21*th1qb1
     .        +sq1qb1/sq11/s1qb1/nc**2*thq11*th1qb1
     .        +sq2qb2/sq21/s1qb2/nc**2*thq21*th1qb2)*thq1qb1*thq2qb2
        t2=hs*(sq1qb2/sq11/s1qb2*thq11*th1qb2 
     .        +sq2qb1/sq21/s1qb1*thq21*th1qb1 
     .        +sq1qb1/sq11/s1qb1*thq11*th1qb1 
     .        +sq2qb2/sq21/s1qb2*thq21*th1qb2  
     .        -sq1q2/sq11/sq21*thq11*thq21 
     .        -sqb1qb2/s1qb1/s1qb2*th1qb1*th1qb2)*thq1qb1*thq2qb2
	fac=(2d0*pi*as*nc)**3*(nc**2-1d0)/nc**2
	r(0)=fac*(t1-2d0*t2/nc**2)
      endif
*
      end
************************************************************************
*
* two distinct quark pair matrix elements : finite part of virtual
*
      function fa(s12,s23,s13)
      implicit real*8(a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /koppel/ nf,nc,b0
      common /fraction/ x1,x2,facscale,renscale
      s=s12/renscale**2     ! sqQ
      t=s23/renscale**2     ! sqqbar
      u=s13/renscale**2     ! sqbarQ 
      V=nc**2-1d0
      finite =
     . +V/2d0/nc*(
     .    -16d0-2d0*(log(abs(t))**2-pi**2*theta(t))
     .    +log(abs(t))*(6d0+8d0*log(abs(s))-8d0*log(abs(u)))
     .    -2d0*(s**2-u**2)/(s**2+u**2)
     .   *(2d0*pi**2
     .   +(log(abs(t))-log(abs(s)))**2-pi**2*(theta(t)+theta(s))
     .   +(log(abs(t))-log(abs(u)))**2-pi**2*(theta(t)+theta(u)))
     .    +2d0*(s+u)/(s**2+u**2)
     .      *((s+u)*(log(abs(u))-log(abs(s)))
     .	     +(u-s)*(2d0*log(abs(t))-log(abs(s))-log(abs(u))))  )
     . +nc*(
     .    +85d0/9d0+pi**2
     .      +2d0*log(abs(t))*(log(abs(t))+log(abs(u))-2d0*log(abs(s)))
     .      -2d0*pi**2*theta(t)
     .    +(s**2-u**2)/2d0/(s**2+u**2)
     .      *(3d0*pi**2
     .   +2d0*((log(abs(t))-log(abs(s)))**2-pi**2*(theta(t)+theta(s)))
     .        +(log(abs(t))-log(abs(u)))**2-pi**2*(theta(t)+theta(u)))
     .    -s*t/(s**2+u**2)*(log(abs(t))-log(abs(u)))
     .    +2d0*u*t/(s**2+u**2)*(log(abs(t))-log(abs(s)))
     .    -11d0/3d0*log(abs(t))  )
     . +nf/2d0*(4d0/3d0*log(abs(t))-20d0/9d0)
      fa=finite/nc 
      return
      end  
************************************************************************
*
* four identical quark matrix elements
*
      subroutine gnq4(iq1,iqb1,iq2,iqb2,npar,nloop,r)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /thcut/ smin,s0
      common /koppel/ nf,nc,b0
      common /sin/ s(10,10),th(10,10)
      common /strong/ as
      common /fraction/ x1,x2,facscale,renscale
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /proc/ isub(4)
      dimension r(0:1),ig(10)
      r(0)=0d0
      r(1)=0d0
      if(isub(4).eq.0)return
*
* find where gluon momenta are stored
*
      j=0
      do 10 i=1,npar+2
        if(i.ne.iq1.and.i.ne.iqb1.and.i.ne.iq2.and.i.ne.iqb2)then
	  j=j+1
	  ig(j)=i
	endif
   10 continue
*   
      sq1q2  =s(iq1,iq2)
      sq1qb1 =s(iq1,iqb1)
      sq1qb2 =s(iq1,iqb2)
      sq2qb1 =s(iq2,iqb1)
      sq2qb2 =s(iq2,iqb2)
      sqb1qb2=s(iqb1,iqb2)
      if(npar.eq.2)then
	t1=8d0*(sq1q2**2+sq1qb2**2)/sq1qb1**2
	t2=8d0*(sq1q2**2+sq1qb1**2)/sq1qb2**2
	t3=8d0*sq1q2**2/sq1qb1/sq1qb2
	fac=(2d0*pi*as*nc)**2*(nc**2-1d0)/nc**2
	r(0)=fac*(t1+t2-2d0*t3/nc)
	if(nloop.eq.1)then
	  renterm=0d0*b0*log(renscale**2/smin)*as
*
          rq1q2=63d0/18d0-pi**2/3d0
     . 	   +3d0/2d0*log(renscale**2/smin)   
     .     -log(abs(sq1q2)/smin)**2
     .     +log(abs(sq1q2)/renscale**2)**2/2d0
          rq1qb1=63d0/18d0-pi**2/3d0
     . 	   +3d0/2d0*log(renscale**2/smin)   
     .     -log(abs(sq1qb1)/smin)**2
     .     +log(abs(sq1qb1)/renscale**2)**2/2d0
          rq1qb2=63d0/18d0-pi**2/3d0
     . 	   +3d0/2d0*log(renscale**2/smin)   
     .     -log(abs(sq1qb2)/smin)**2
     .     +log(abs(sq1qb2)/renscale**2)**2/2d0
          rq2qb1 =rq1qb2
          rq2qb2 =rq1qb1
          rqb1qb2=rq1q2
*
	  rk1=(rq1qb2+rq2qb1+rq1qb1/nc**2+rq2qb2/nc**2)
     .     -2d0/nc**2*(rq1qb2+rq2qb1+rq1qb1+rq2qb2-rq1q2-rqb1qb2)
	  rk2=(rq1qb1+rq2qb2+rq1qb2/nc**2+rq2qb1/nc**2)
     .     -2d0/nc**2*(rq1qb1+rq2qb2+rq1qb2+rq2qb1-rq1q2-rqb1qb2)
          rk3=(rq1qb1+rq2qb2+rq1qb2+rq2qb1)  
     .      -(nc**2+1d0)/nc**2
     .            *(rq1qb1+rq2qb2+rq1qb2+rq2qb1-rq1q2-rqb1qb2)
 	  rk1=as*nc/2d0/pi*rk1+renterm
 	  rk2=as*nc/2d0/pi*rk2+renterm
 	  rk3=as*nc/2d0/pi*rk3+renterm 
   	  rf1=as*nc/2d0/pi*fa(sq1q2,sq1qb1,sq1qb2)
          rf2=as*nc/2d0/pi*fa(sq1q2,sq1qb2,sq1qb1)   
          rf3=as*nc/2d0/pi*fb(sq1q2,sq1qb1,sq1qb2)    
 	  r(1)=fac*((1d0+rk1+rf1)*t1
     .    	   +(1d0+rk2+rf2)*t2 
     .         -2d0*(1d0+rk3+rf3)*t3/nc)
	endif
*      write(*,*)' gnq4   ',iq1,iqb1,iq2,iqb2,ig(1),r(0),r(1)
      elseif(npar.eq.3)then
        sq11 =s(iq1, ig(1))
        s1qb1=s(iqb1,ig(1))
        sq21 =s(iq2, ig(1))
        s1qb2=s(iqb2,ig(1))
        thq1qb1 =th(iq1,iqb1)
        thq1qb2 =th(iq1,iqb2)
        thq2qb1 =th(iq2,iqb1)
        thq2qb2 =th(iq2,iqb2)
        thq11 =th(iq1, ig(1))
        th1qb1=th(iqb1,ig(1))
        thq21 =th(iq2, ig(1))
        th1qb2=th(iqb2,ig(1))

	hs=16d0*(sq1q2**2+sqb1qb2**2+sq1qb2**2+sq2qb1**2)/sq1qb1/sq2qb2
	t1=hs*(sq1qb2/sq11/s1qb2*thq11*th1qb2
     .        +sq2qb1/sq21/s1qb1*thq21*th1qb1
     .        +sq1qb1/sq11/s1qb1/nc**2*thq11*th1qb1
     .        +sq2qb2/sq21/s1qb2/nc**2*thq21*th1qb2)*thq1qb1*thq2qb2
        t2=hs*(sq1qb2/sq11/s1qb2*thq11*th1qb2
     .        +sq2qb1/sq21/s1qb1*thq21*th1qb1
     .        +sq1qb1/sq11/s1qb1*thq11*th1qb1
     .        +sq2qb2/sq21/s1qb2*thq21*th1qb2
     .        -sq1q2/sq11/sq21*thq11*thq21
     .        -sqb1qb2/s1qb1/s1qb2*th1qb1*th1qb2)*thq1qb1*thq2qb2
	hs=16d0*(sq1q2**2+sqb1qb2**2+sq1qb1**2+sq2qb2**2)/sq1qb2/sq2qb1
	t3=hs*(sq1qb1/sq11/s1qb1*thq11*th1qb1
     .        +sq2qb2/sq21/s1qb2*thq21*th1qb2
     .        +sq1qb2/sq11/s1qb2/nc**2*thq11*th1qb2
     .        +sq2qb1/sq21/s1qb1/nc**2*thq21*th1qb1)*thq1qb2*thq2qb1
        t4=hs*(sq1qb1/sq11/s1qb1*thq11*th1qb1
     .        +sq2qb2/sq21/s1qb2*thq21*th1qb2
     .        +sq1qb2/sq11/s1qb2*thq11*th1qb2
     .        +sq2qb1/sq21/s1qb1*thq21*th1qb1
     .        -sq1q2/sq11/sq21*thq11*thq21
     .        -sqb1qb2/s1qb1/s1qb2*th1qb1*th1qb2)*thq1qb2*thq2qb1
        hs=16d0*(sq1q2**2+sqb1qb2**2)/sq1qb1/sq2qb2/sq1qb2/sq2qb1
     .        *(sq1qb1*sq2qb2+sq1qb2*sq2qb1-sq1q2*sqb1qb2)
	t5=hs*(sq1qb1/sq11/s1qb1*thq11*th1qb1
     .        +sq2qb2/sq21/s1qb2*thq21*th1qb2
     .        +sq1qb2/sq11/s1qb2*thq11*th1qb2
     .        +sq2qb1/sq21/s1qb1*thq21*th1qb1)
     .            *thq1qb1*thq1qb2*thq2qb2*thq2qb1
        t6=hs*(sq1qb1/sq11/s1qb1*thq11*th1qb1 
     .        +sq2qb2/sq21/s1qb2*thq21*th1qb2 
     .        +sq1qb2/sq11/s1qb2*thq11*th1qb2 
     .        +sq2qb1/sq21/s1qb1*thq21*th1qb1 
     .        -sq1q2/sq11/sq21*thq11*thq21 
     .        -sqb1qb2/s1qb1/s1qb2*th1qb1*th1qb2 )
     .            *thq1qb1*thq1qb2*thq2qb2*thq2qb1
	fac=(2d0*pi*as*nc)**3*(nc**2-1d0)/nc**2
	r(0)=fac*(t1-2d0*t2/nc**2+t3-2d0*t4/nc**2
     .              +t5/nc-(nc**2+1d0)*t6/nc**3)
*      write(*,*)' gnq4   ',iq1,iqb1,iq2,iqb2,ig(1),r(0)
      endif
*
      end
************************************************************************
*
* four identical quark matrix elements : finite part of virtual
*
      function fb(s12,s23,s13)
      implicit real*8(a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /koppel/ nf,nc,b0
      common /fraction/ x1,x2,facscale,renscale
      s=s12/renscale**2     ! sqQ
      t=s23/renscale**2     ! sqqbar
      u=s13/renscale**2     ! sqbarQ 
      V=nc**2-1d0
      finite =
     . +V/2d0/nc*( 
     .       -16d0
     .  -3d0/2d0
     .    *((log(abs(t))+log(abs(u)))**2-pi**2*(theta(t)+theta(u)))
     .	+2d0*log(abs(s))*(log(abs(t))+log(abs(u)))
     .	+2d0*(log(abs(t))+log(abs(u)))-pi**2/2d0)
     . +nc*(
     .     +85d0/9d0
     .  +5d0/4d0
     .    *((log(abs(t))+log(abs(u)))**2-pi**2*(theta(t)+theta(u)))
     .  -log(abs(t))*log(abs(u))
     .  -2d0*log(abs(s))*(log(abs(t))+log(abs(u)))
     .  -4d0/3d0*(log(abs(t))+log(abs(u)))+5d0/4d0*pi**2 )
     . +nf/2d0*(-20d0/9d0+2d0/3d0*(log(abs(t))+log(abs(u))))
     . +1d0/nc*(t*u/2d0/s**2*(pi**2
     .    +(log(abs(t))-log(abs(u)))**2-pi**2*(theta(t)+theta(u)) )
     .    +t/s*log(abs(u))+u/s*log(abs(t))  ) 
      fb=finite/nc
      return
      end  
************************************************************************
*
      function theta(x)
      implicit double precision (a-h,o-z)
      theta=0d0
      if(x.gt.0d0)theta=1d0
*
      end	      
************************************************************************
c
c two loop strong coupling constant at scale rq
c
       function alfas(mu,lam,nloops)   
c      alfas in terms of lambda of four flavors for one or two loops.
c      matching achieved using renorm group eqn. approximately 
c      above mu=mb,mu=mt
       implicit none
       integer nloops
       real*8 alfas
       real*8 mu,lam,mc,mb,mt
       real*8 b4,b4p,b5,b5p,b6,b6p,one,two
       real*8 atinv,abinv,atinv1,abinv1,asinv,xqc,xqb,xqt,xb,xt
       parameter(one=1d0,two=2d0)
*       parameter(b4=1.7507044d0,b5=1.7507044d0,b6=1.7507044d0)    
*       parameter(b4p=0.7379001d0,b5p=0.7379001d0,b6p=0.7379001d0)
       parameter(b4=1.326291192d0,b5=1.220187897d0,b6=1.114084601d0)    
       parameter(b4p=0.490197225d0,b5p=0.401347248d0,b6p=0.295573466d0)
c      b=(33.d0-2d0*nf)/6.d0/pi       
c      bp=(153.d0-19.d0*nf)/(2d0*pi*(33.d0-2d0*nf))
       parameter(mc=1.5d0,mb=5.0d0,mt=140.d0)
*       parameter(mc=1.5d0,mb=5.0d5,mt=140.d5)

       if (mu.lt.mc) then 
            write(6,*) 'unimplemented, mu too low',mu
            alfas=0.d0
            return
       endif

       xb=log(mb/lam)       
       abinv=b4*xb      

       if (mu.lt.mb) then
            xqc=log(mu/lam)  
            asinv=b4*xqc         
            alfas=one/asinv
       elseif (mu.lt.mt) then
            xqb=log(mu/mb)      
            asinv=abinv+b5*xqb
            alfas=one/asinv         
       else
            xqt=log(mu/mt)      
            xt=log(mt/mb)       
            atinv=abinv+b5*xt      
            asinv=atinv+b6*xqt
            alfas=one/asinv         
       endif
       
       if (nloops.eq.1) return         

       abinv1=abinv/(one-b4p*log(two*xb)/abinv)         
       if (mu.lt.mb) then
           asinv=asinv/(one-b4p*log(two*xqc)/asinv)
           alfas=one/asinv
       elseif (mu.lt.mt) then
           asinv=abinv1+b5*xqb+b5p*log((asinv+b5p)/(abinv+b5p))
           alfas=one/asinv
       else         
           atinv1=abinv1+b5*xt+b5p*log((b5p+atinv)/(b5p+abinv))
           asinv=atinv1+b6*xqt+b6p*log((b6p+asinv)/(atinv+b6p))
           alfas=one/asinv         
       endif    
       return
       end
*
      SUBROUTINE VEGASA(ISTAT,FXN,AVGI,SD,CHI2A)
C
C   SUBROUTINE PERFORMS N-DIMENSIONAL MONTE CARLO INTEG'N
C      - BY G.P. LEPAGE   SEPT 1976/(REV)APR 1978
C
      IMPLICIT REAL*8(A-H,O-Z)
      external FXN
      COMMON/BVEGA/NDIM,NCALL,NPRN
      DIMENSION XI(50,10),D(50,10),DI(50,10),XIN(50),R(50),DT(10),X(10)
     1   ,KG(10),IA(10)
      REAL*8 QRAN(10)
      DATA NDMX/50/,ALPH/1.5D0/,ONE/1D0/,MDS/1/
C
      if(istat.eq.1.or.istat.eq.2)then
c
c         initialize cumulative variables 
c
        IT=0
        SI  =0d0
        SI2 =0d0
        SWGT=0d0
        SCHI=0d0
        ND=NDMX
        NG=1
        IF(MDS.ne.0)then 
          NG=(NCALL/2.)**(1./NDIM)
          MDS=1
          IF((2*NG-NDMX).ge.0)then
            MDS=-1
            NPG=NG/NDMX+1
            ND=NG/NPG
            NG=NPG*ND
          endif
        endif
        K=NG**NDIM
        NPG=NCALL/K
        IF(NPG.LT.2) NPG=2
        CALLS=NPG*K
        DXG=ONE/NG
        DV2G=(CALLS*DXG**NDIM)**2/NPG/NPG/(NPG-ONE)
        XND=ND
        NDM=ND-1
        DXG=DXG*XND
        XJAC=ONE/CALLS
      IF(NPRN.NE.0) WRITE(6,200) NDIM,CALLS,IT,ITMX,MDS,ND
      endif
      if(istat.eq.1)then
C
C   construct uniform grid  
C
        RC=1d0/XND
        DO  J=1,NDIM
          xi(1,j)=1d0
          K=0
          XN=0d0
          DR=0d0
          I=0
4         K=K+1
          DR=DR+ONE
          XO=XN
          XN=XI(K,J)
5         IF(RC.GT.DR) GO TO 4
          I=I+1
          DR=DR-RC
          XIN(I)=XN-(XN-XO)*DR
          IF(I.LT.NDM) GO TO 5
          DO  I=1,NDM
            XI(I,J)=XIN(I)
          enddo
          XI(ND,J)=ONE
        enddo
        NDO=ND
        return
C
      elseif(istat.eq.2)then
C
C   rescale refined grid to new ND value - preserve bin density
C
        if(nd.ne.ndo)then
          RC=NDO/XND
          DO  J=1,NDIM
            K=0
            XN=0d0
            DR=0d0
            I=0
6           K=K+1
            DR=DR+ONE
            XO=XN
            XN=XI(K,J)
7           IF(RC.GT.DR) GO TO 6
            I=I+1
            DR=DR-RC
            XIN(I)=XN-(XN-XO)*DR
            IF(I.LT.NDM) GO TO 7
            DO  I=1,NDM
              XI(I,J)=XIN(I)
            enddo
            XI(ND,J)=ONE
          enddo
        endif
C
        return
C
      elseif(istat.eq.3.or.istat.eq.4)then
c
c    main integration loop
c         
        IT=IT+1
        TI =0d0
        TSI=0d0
        DO J=1,NDIM
          KG(J)=1
          DO I=1,ND
           D (I,J)=0d0
           DI(I,J)=0d0
          enddo
        enddo
C
11      FB=0d0
        F2B=0d0
        K=0
12      K=K+1
        do j=1,ndim
          qran(j)=rn(1.)
        enddo        
        WGT=XJAC
        DO J=1,NDIM
          XN=(KG(J)-QRAN(J))*DXG+ONE
          IA(J)=XN
          IF(IA(J).GT.1)then
            XO=XI(IA(J),J)-XI(IA(J)-1,J)
            RC=XI(IA(J)-1,J)+(XN-IA(J))*XO
          else
            XO=XI(IA(J),J)
            RC=(XN-IA(J))*XO
          endif
          X(J)=RC
          WGT=WGT*XO*XND
        enddo
C
        F=WGT
        F=F*FXN(X,WGT)
        F2=F*F
        FB=FB+F
        F2B=F2B+F2
        DO J=1,NDIM
          DI(IA(J),J)=DI(IA(J),J)+F
          IF(MDS.GE.0) D(IA(J),J)=D(IA(J),J)+F2
        enddo
        IF(K.LT.NPG) GO TO 12
C
        F2B=DSQRT(F2B*NPG)
        F2B=(F2B-FB)*(F2B+FB)
        TI=TI+FB
        TSI=TSI+F2B
        IF(MDS.lt.0) then
          DO J=1,NDIM
            D(IA(J),J)=D(IA(J),J)+F2B
          enddo
        endif
        K=NDIM
19      KG(K)=MOD(KG(K),NG)+1
        IF(KG(K).NE.1) GO TO 11
        K=K-1
        IF(K.GT.0) GO TO 19
C
C   FINAL RESULTS FOR THIS ITERATION
C
        TSI=TSI*DV2G
        TI2=TI*TI
        WGT=TI2/TSI
        SI=SI+TI*WGT
        SI2=SI2+TI2
        SWGT=SWGT+WGT
        SCHI=SCHI+TI2*WGT
        AVGI=SI/SWGT
        SD=SWGT*IT/SI2
        CHI2A=SD*(SCHI/SWGT-AVGI*AVGI)/(IT-.999)
        SD=DSQRT(ONE/SD)
C
        IF(NPRN.ne.0) then
          TSI=DSQRT(TSI)
          WRITE(6,201) IT,TI,TSI,AVGI,SD,CHI2A
        endif
        IF(NPRN.lt.0) then
          DO J=1,NDIM
            WRITE(6,202) J,(XI(I,J),DI(I,J),D(I,J),I=1,ND)
          enddo
        endif
      endif
C
C   REFINE GRID
C
      if(istat.eq.3)then
       DO J=1,NDIM
         XO=D(1,J)
         XN=D(2,J)
         D(1,J)=(XO+XN)/2.
         DT(J)=D(1,J)
         DO I=2,NDM
           D(I,J)=XO+XN
           XO=XN
           XN=D(I+1,J)
           D(I,J)=(D(I,J)+XN)/3.
           DT(J)=DT(J)+D(I,J)
        enddo
        D(ND,J)=(XN+XO)/2.
      DT(J)=DT(J)+D(ND,J)
      enddo
C
        DO 28 J=1,NDIM
        RC=0.
        DO 24 I=1,ND
        R(I)=0.
        IF(D(I,J).LE.0d0) GO TO 24
        XO=DT(J)/D(I,J)
        R(I)=((XO-ONE)/XO/DLOG(XO))**ALPH
24      RC=RC+R(I)
        RC=RC/XND
        K=0
        XN=0.
        DR=XN
        I=K
25      K=K+1
        DR=DR+R(K)
        XO=XN
        XN=XI(K,J)
26      IF(RC.GT.DR) GO TO 25
        I=I+1
        DR=DR-RC
        XIN(I)=XN-(XN-XO)*DR/R(K)
        IF(I.LT.NDM) GO TO 26
        DO 27 I=1,NDM
27      XI(I,J)=XIN(I)
28      XI(ND,J)=ONE
        return
      endif
C
200   FORMAT(///' INPUT PARAMETERS FOR VEGAS:  NDIM=',I3,'  NCALL=',
     1    F8.0/28X,'  IT=',I5,'  ITMX=',I5
     2    /28X,'  MDS=',I3,'   ND=',I4)
201   FORMAT(i4,4x,g15.7,g11.4,g15.7,g11.4,g11.4)
202   FORMAT(' DATA FOR AXIS',I2 / ' ',6X,'X',7X,'  DELT I  ',
     1    2X,' CONV''CE  ',11X,'X',7X,'  DELT I  ',2X,' CONV''CE  '
     2   ,11X,'X',7X,'  DELT I  ',2X,' CONV''CE  ' /
     2    (' ',3G12.4,5X,3G12.4,5X,3G12.4))
      RETURN
      END
      SUBROUTINE VEGASB(ISTAT,FXN,AVGI,SD,CHI2A)
C
C   SUBROUTINE PERFORMS N-DIMENSIONAL MONTE CARLO INTEG'N
C      - BY G.P. LEPAGE   SEPT 1976/(REV)APR 1978
C
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/BVEGB/NDIM,NCALL,NPRN
      DIMENSION XI(50,10),D(50,10),DI(50,10),XIN(50),R(50),DT(10),X(10)
     1   ,KG(10),IA(10)
      REAL*8 QRAN(10)
      external FXN
      DATA NDMX/50/,ALPH/1.5D0/,ONE/1D0/,MDS/1/
C
      if(istat.eq.1.or.istat.eq.2)then
c
c         initialize cumulative variables 
c
        IT=0
        SI  =0d0
        SI2 =0d0
        SWGT=0d0
        SCHI=0d0
        ND=NDMX
        NG=1
        IF(MDS.ne.0)then 
          NG=(NCALL/2.)**(1./NDIM)
          MDS=1
          IF((2*NG-NDMX).ge.0)then
            MDS=-1
            NPG=NG/NDMX+1
            ND=NG/NPG
            NG=NPG*ND
          endif
        endif
        K=NG**NDIM
        NPG=NCALL/K
        IF(NPG.LT.2) NPG=2
        CALLS=NPG*K
        DXG=ONE/NG
        DV2G=(CALLS*DXG**NDIM)**2/NPG/NPG/(NPG-ONE)
        XND=ND
        NDM=ND-1
        DXG=DXG*XND
        XJAC=ONE/CALLS
      IF(NPRN.NE.0) WRITE(6,200) NDIM,CALLS,IT,ITMX,MDS,ND
      endif
      if(istat.eq.1)then
C
C   construct uniform grid  
C
        RC=1d0/XND
        DO  J=1,NDIM
          xi(1,j)=1d0
          K=0
          XN=0d0
          DR=0d0
          I=0
4         K=K+1
          DR=DR+ONE
          XO=XN
          XN=XI(K,J)
5         IF(RC.GT.DR) GO TO 4
          I=I+1
          DR=DR-RC
          XIN(I)=XN-(XN-XO)*DR
          IF(I.LT.NDM) GO TO 5
          DO  I=1,NDM
            XI(I,J)=XIN(I)
          enddo
          XI(ND,J)=ONE
        enddo
        NDO=ND
        return
C
      elseif(istat.eq.2)then
C
C   rescale refined grid to new ND value - preserve bin density
C
        if(nd.ne.ndo)then
          RC=NDO/XND
          DO  J=1,NDIM
            K=0
            XN=0d0
            DR=0d0
            I=0
6           K=K+1
            DR=DR+ONE
            XO=XN
            XN=XI(K,J)
7           IF(RC.GT.DR) GO TO 6
            I=I+1
            DR=DR-RC
            XIN(I)=XN-(XN-XO)*DR
            IF(I.LT.NDM) GO TO 7
            DO  I=1,NDM
              XI(I,J)=XIN(I)
            enddo
            XI(ND,J)=ONE
          enddo
        endif
C
        return
C
      elseif(istat.eq.3.or.istat.eq.4)then
c
c    main integration loop
c         
        IT=IT+1
        TI =0d0
        TSI=0d0
        DO J=1,NDIM
          KG(J)=1
          DO I=1,ND
           D (I,J)=0d0
           DI(I,J)=0d0
          enddo
        enddo
C
11      FB=0d0
        F2B=0d0
        K=0
12      K=K+1
        do j=1,ndim
          qran(j)=rn(1.)
        enddo        
        WGT=XJAC
        DO J=1,NDIM
          XN=(KG(J)-QRAN(J))*DXG+ONE
          IA(J)=XN
          IF(IA(J).GT.1)then
            XO=XI(IA(J),J)-XI(IA(J)-1,J)
            RC=XI(IA(J)-1,J)+(XN-IA(J))*XO
          else
            XO=XI(IA(J),J)
            RC=(XN-IA(J))*XO
          endif
          X(J)=RC
          WGT=WGT*XO*XND
        enddo
C
        F=WGT
        F=F*FXN(X,WGT)
        F2=F*F
        FB=FB+F
        F2B=F2B+F2
        DO J=1,NDIM
          DI(IA(J),J)=DI(IA(J),J)+F
          IF(MDS.GE.0) D(IA(J),J)=D(IA(J),J)+F2
        enddo
        IF(K.LT.NPG) GO TO 12
C
        F2B=DSQRT(F2B*NPG)
        F2B=(F2B-FB)*(F2B+FB)
        TI=TI+FB
        TSI=TSI+F2B
        IF(MDS.lt.0) then
          DO J=1,NDIM
            D(IA(J),J)=D(IA(J),J)+F2B
          enddo
        endif
        K=NDIM
19      KG(K)=MOD(KG(K),NG)+1
        IF(KG(K).NE.1) GO TO 11
        K=K-1
        IF(K.GT.0) GO TO 19
C
C   FINAL RESULTS FOR THIS ITERATION
C
        TSI=TSI*DV2G
        TI2=TI*TI
        WGT=TI2/TSI
        SI=SI+TI*WGT
        SI2=SI2+TI2
        SWGT=SWGT+WGT
        SCHI=SCHI+TI2*WGT
        AVGI=SI/SWGT
        SD=SWGT*IT/SI2
        CHI2A=SD*(SCHI/SWGT-AVGI*AVGI)/(IT-.999)
        SD=DSQRT(ONE/SD)
C
        IF(NPRN.ne.0) then
          TSI=DSQRT(TSI)
          WRITE(6,201) IT,TI,TSI,AVGI,SD,CHI2A
        endif
        IF(NPRN.lt.0) then
          DO J=1,NDIM
            WRITE(6,202) J,(XI(I,J),DI(I,J),D(I,J),I=1,ND)
          enddo
        endif
      endif
C
C   REFINE GRID
C
      if(istat.eq.3)then
       DO J=1,NDIM
         XO=D(1,J)
         XN=D(2,J)
         D(1,J)=(XO+XN)/2.
         DT(J)=D(1,J)
         DO I=2,NDM
           D(I,J)=XO+XN
           XO=XN
           XN=D(I+1,J)
           D(I,J)=(D(I,J)+XN)/3.
           DT(J)=DT(J)+D(I,J)
        enddo
        D(ND,J)=(XN+XO)/2.
      DT(J)=DT(J)+D(ND,J)
      enddo
C
        DO 28 J=1,NDIM
        RC=0.
        DO 24 I=1,ND
        R(I)=0.
        IF(D(I,J).LE.0d0) GO TO 24
        XO=DT(J)/D(I,J)
        R(I)=((XO-ONE)/XO/DLOG(XO))**ALPH
24      RC=RC+R(I)
        RC=RC/XND
        K=0
        XN=0.
        DR=XN
        I=K
25      K=K+1
        DR=DR+R(K)
        XO=XN
        XN=XI(K,J)
26      IF(RC.GT.DR) GO TO 25
        I=I+1
        DR=DR-RC
        XIN(I)=XN-(XN-XO)*DR/R(K)
        IF(I.LT.NDM) GO TO 26
        DO 27 I=1,NDM
27      XI(I,J)=XIN(I)
28      XI(ND,J)=ONE
        return
      endif
C
200   FORMAT(///' INPUT PARAMETERS FOR VEGAS:  NDIM=',I3,'  NCALL=',
     1    F8.0/28X,'  IT=',I5,'  ITMX=',I5
     2    /28X,'  MDS=',I3,'   ND=',I4)
201   FORMAT(i4,4x,g15.7,g11.4,g15.7,g11.4,g11.4)
202   FORMAT(' DATA FOR AXIS',I2 / ' ',6X,'X',7X,'  DELT I  ',
     1    2X,' CONV''CE  ',11X,'X',7X,'  DELT I  ',2X,' CONV''CE  '
     2   ,11X,'X',7X,'  DELT I  ',2X,' CONV''CE  ' /
     2    (' ',3G12.4,5X,3G12.4,5X,3G12.4))
      RETURN
      END
*
* random number generator
*
      function rn(idummy)
      real*8 rn,ran
      save init
      data init /1/
      if (init.eq.1) then
        init=0
        call rmarin(1802,9373)
      end if
*
  10  call ranmar(ran)
      if (ran.lt.1d-16) goto 10
      rn=ran
*
      end
*
      subroutine ranmar(rvec)
*     -----------------
* universal random number generator proposed by marsaglia and zaman
* in report fsu-scri-87-50
* in this version rvec is a double precision variable.
      implicit real*8(a-h,o-z)
      common/ raset1 / ranu(97),ranc,rancd,rancm
      common/ raset2 / iranmr,jranmr
      save /raset1/,/raset2/
      uni = ranu(iranmr) - ranu(jranmr)
      if(uni .lt. 0d0) uni = uni + 1d0
      ranu(iranmr) = uni
      iranmr = iranmr - 1
      jranmr = jranmr - 1
      if(iranmr .eq. 0) iranmr = 97
      if(jranmr .eq. 0) jranmr = 97
      ranc = ranc - rancd
      if(ranc .lt. 0d0) ranc = ranc + rancm
      uni = uni - ranc
      if(uni .lt. 0d0) uni = uni + 1d0
      rvec = uni
      end
 
      subroutine rmarin(ij,kl)
*     -----------------
* initializing routine for ranmar, must be called before generating
* any pseudorandom numbers with ranmar. the input values should be in
* the ranges 0<=ij<=31328 ; 0<=kl<=30081
      implicit real*8(a-h,o-z)
      common/ raset1 / ranu(97),ranc,rancd,rancm
      common/ raset2 / iranmr,jranmr
      save /raset1/,/raset2/
* this shows correspondence between the simplified input seeds ij, kl
* and the original marsaglia-zaman seeds i,j,k,l.
* to get the standard values in the marsaglia-zaman paper (i=12,j=34
* k=56,l=78) put ij=1802, kl=9373
      i = mod( ij/177 , 177 ) + 2
      j = mod( ij     , 177 ) + 2
      k = mod( kl/169 , 178 ) + 1
      l = mod( kl     , 169 )
      do 300 ii = 1 , 97
        s =  0d0
        t = .5d0
        do 200 jj = 1 , 24
          m = mod( mod(i*j,179)*k , 179 )
          i = j
          j = k
          k = m
          l = mod( 53*l+1 , 169 )
          if(mod(l*m,64) .ge. 32) s = s + t
          t = .5d0*t
  200   continue
        ranu(ii) = s
  300 continue
      ranc  =   362436d0 / 16777216d0
      rancd =  7654321d0 / 16777216d0
      rancm = 16777213d0 / 16777216d0
      iranmr = 97
      jranmr = 33
      end
       double precision function rnnew(dummy)
*
*      random number function taken from knuth
*      (seminumerical algorithms).
*      method is x(n)=mod(x(n-55)-x(n-24),1/fmodul)
*      no provision yet for control over the seed number.
*
*      ranf gives one random number between 0 and 1.
*      irn55 generates 55 random numbers between 0 and 1/fmodul.
*      in55  initializes the 55 numbers and warms up the sequence.
*
       implicit double precision (a-h,o-z)
       parameter (fmodul=1.d-09)
       integer ia(55)
       save ia
       data ncall/0/
       data mcall/55/
       if( ncall.eq.0 ) then
           call in55 ( ia,234612947 )
           ncall = 1
       endif
       if ( mcall.eq.0 ) then
           call irn55(ia)
           mcall=55
       endif
       rnnew=ia(mcall)*fmodul
       mcall=mcall-1
       end

       subroutine in55(ia,ix)
       parameter (modulo=1000000000)
       integer ia(55)
       ia(55)=ix
       j=ix
       k=1
       do 10 i=1,54
       ii=mod(21*i,55)
       ia(ii)=k
       k=j-k
       if(k.lt.0)k=k+modulo
       j=ia(ii)
   10  continue
       do 20 i=1,10
       call irn55(ia)
   20  continue
       end

       subroutine irn55(ia)
       parameter (modulo=1000000000)
       integer ia(55)
       do 10 i=1,24
       j=ia(i)-ia(i+31)
       if(j.lt.0)j=j+modulo
       ia(i)=j
   10  continue
       do 20 i=25,55
       j=ia(i)-ia(i-24)
       if(j.lt.0)j=j+modulo
       ia(i)=j
   20  continue
       end

*
* The histo-handlers, makes easy interface between user routine 'bino'
* and general histogram manipulator 'ghiman'.
* 
* 'histoi' : sets up histogram 'idhis' with minimum bin value 'bmin',
*            maximum binvalue 'bmax' and number of bins 'nbin'
* 'histoa' : make specific entry in histogram 'idhis' for value 'val' and 
*            weight 'wgt'
* 'histoe' : calculate error request for histogram 'idhis', pipe through
* 'histow' : output request for histogram 'idhis', pipe through
*
      subroutine histoi(idhis,bmin,bmax,nbin)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* histogram initialization
*
      call ghiman(0,idhis,nbin,0d0,0d0)
      hmin(idhis)=bmin
      ibin(idhis)=nbin
      hwidth(idhis)=(bmax-bmin)/nbin
*
      return
      end
*
      subroutine histoa(idhis,val,wgt)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* histogram entry
*
      if(val.lt.hmin(idhis))return
      iloc=1+int((val-hmin(idhis))/hwidth(idhis))
      call ghiman(1,idhis,iloc,wgt,0d0)
*
      return
      end
*
      subroutine histoe(istat,idhis)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* event errors request, pipe through with correct 'ghiman' call
*
      call ghiman(istat,idhis,ibin(idhis),0d0,0d0)
*
      return
      end
*
      subroutine histow(idhis)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* output request, pipe through with correct 'ghiman' call
*
      call ghiman(4,idhis,ibin(idhis),hmin(idhis),hwidth(idhis))
*
      return
      end
*
*
* General histogram manipulator, has no knowledge about specific histograms.
*
* Maximum number of histograms is given by the nhisto parameter.
* Maximum number of bins       is given by the maxbin parameter.
*
* istat = 0 : set histogram 'idhis' to value 'entry1' from bin 1 to bin 'iloc'
*             and resets sweep counter 'itmx' to zero.
* istat = 1 : add in histogram 'idhis' weight 'entry1' in  bin location 'iloc'.
* istat = 2 : accumulate event errors in histogram 'idhis' from bin 1 to bin
*             'iloc' for this sweep and increases sweepcounter 'itmx' by 1.
* istat = 3 : calculate standard deviation of the 'itmx' sweeps per bin in 
*             histogram 'idhis' from bin 1 to bin 'iloc' as monte carlo error
*             estimate.
* istat = 4 : write final output to screen in histogram 'idhis' from bin 1 to 
*             bin 'iloc' with offset 'entry1' and binwidth 'entry2'
*            (format: 
*         from bin_number = 1 to 'iloc'  
*            write 'entry1'+'entry2'*(bin_number - 0.5), bin_value, bin_error
*         endfrom)
*
      subroutine ghiman(istat,idhis,iloc,entry1,entry2)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      dimension bin(nhisto,4,maxbin)
*
      if ((idhis.lt.1).or.(idhis.gt.nhisto)) return
      if ((iloc .lt.1).or.(iloc .gt.maxbin)) return
*
*       init histograms
*
      if (istat.eq.0) then
         do i=1,iloc
            do j=1,4
               bin(idhis,j,i)=entry1
            enddo
         enddo
         itmx=0
      endif
*
* write event into histogram
*
      if (istat.eq.1) then
         if ((iloc.ge.1).and.(iloc.le.maxbin)) then
            bin(idhis,1,iloc)=bin(idhis,1,iloc)+entry1
            bin(idhis,4,iloc)=bin(idhis,4,iloc)+1d0
         endif
      endif
*
* accumulate event errors
*
      if (istat.eq.2) then
         do i=1,iloc
*            write(*,*)bin(idhis,1,i),bin(idhis,4,i)
            bin(idhis,2,i)=bin(idhis,2,i)+bin(idhis,1,i)**2
            bin(idhis,3,i)=bin(idhis,3,i)+bin(idhis,1,i)
            bin(idhis,1,i)=0d0
         enddo
      endif
*
* calculate event errors
*
      if (istat.eq.3) then
        do i=1,iloc
          bin(idhis,2,i)=
     .  sqrt((bin(idhis,2,i)/itmax2-(bin(idhis,3,i)/itmax2)**2)
     .        /float(itmax2-1))
        enddo
      endif
*
* output distributions
*
      if (istat.eq.4) then
         sum=0d0
         do i=1,iloc
            x=bin(idhis,3,i)/itmax2 
	    sum=sum+x
            x2=bin(idhis,2,i)
            nn=bin(idhis,4,i)
            y=entry1+entry2*(dfloat(i)-.5d0)
            write(6,101) y,x/entry2,x2/entry2
 101        format(3x,1pe12.4,1pe12.4,1pe12.4)
         enddo
      endif
*     
      return
      end
