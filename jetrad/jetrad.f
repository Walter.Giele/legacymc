************************************************************************
*                                                                      *
*      p pbar -> 1 or 2 jets    at O(alphas**3)                        *
*                                                                      *
*      RELEASE 2.0, 01/17/97                                           *
*                                                                      *
************************************************************************
      program jetrad
      implicit real*8 (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      character*5  spp1,spp2
      character*32 sstru,sir,sex,son,sjet
      common /thcut/ smin,s0
      common /phypar/ w,ipp1,ipp2,qcdl
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /koppel/ nf,nc,b0
      common /proc/ isub(4)
      common /order/ iorder
      common /normal/exclusive
      logical exclusive
*
* input parameters
*
* number of jets and order : jets = 1, 2 or 3
*
      njets=1
      nloop=1
      exclusive = .false.
*
* warm up sweeps (itmax1) and main sweeps (itmax2)
* virtual   shots warm up sweep (nshot1) and main sweep (nshot2)
* radiative shots warm up sweep (nshot3) and main sweep (nshot4)
*
*   usually itmax1 = 5 and itmax2 = 10
*   I usually also have nshot4 = 10*nshot3 
*                       nshot3 =    nshot2
*                       nshot2 = 10*nshot1
*   nshot4 = 10**7 is not unreasonable!
*
      itmax1=10
      itmax2=20 
      nshot1=10000
      nshot2=100000
      nshot3=100000
      nshot4=1000000
*
* experimental jet cuts
*
* counts jets with 
*                      etminj  <      ETjet       < etmaxj (in GeV)
*                 and  rapminj < |pseudorapidity| < rapmaxj
*
      etminj=40d0
      etmaxj=900d0
      rapmaxj=0.7d0
      rapminj=0.1d0
*
* recombination scheme
*       jetalg = 1 ; deltaR(i,j) < delrjj
*       jetalg = 2 ; deltaR(i,jet) < delrjj, deltaR(j,jet) < delrjj
*                    and deltaR(i,j) < rsep*delrjj
*       jetalg = 3 ; deltaR(i,jet) < delrjj, deltaR(j,jet) < delrjj
*                    and deltaR(i,j) < rsep*delrjj
*
*       jetalg = 1 is usual Perturbative algorithm
*       jetalg = 2 is usual EKS jet algorithm
*       jetalg = 3 is usual D0  jet algorithm
*
      delrjj=0.7d0     ! size of cone
      rsep=1.3d0       ! rsep variable in EKS jet algorithems
      jetalg=2
*
* theoretical cut
*
      smin=10d0            ! ask if you want to change smin or safety
      safety=1d3           
      s0=smin/safety       
*
*
* structure functions  
* and proton/antiproton flag (ipp=0 -> proton, ipp=1 -> anti proton)
*
* you will need a .DAT files for the approppriate stfn and crossing fn
* in your current directory  
*
* picking structure function fixes lambda_QCD  (qcdl)
*
* istruc=1  -> mrse        (msbar), Lambda = 0.1  GeV
* istruc=2  -> mrsb        (msbar), Lambda = 0.2  GeV
* istruc=3  -> kmrshb      (msbar), Lambda = 0.19 GeV
* istruc=4  -> kmrsb0      (msbar), Lambda = 0.19 GeV
* istruc=5  -> kmrsb-      (msbar), Lambda = 0.19 GeV
* istruc=6  -> kmrsh- (5)  (msbar), Lambda = 0.19 GeV
* istruc=7  -> kmrsh- (2)  (msbar), Lambda = 0.19 GeV
* istruc=8  -> mts1        (msbar), Lambda = 0.2  GeV
* istruc=9  -> mte1        (msbar), Lambda = 0.2  GeV
* istruc=10 -> mtb1        (msbar), Lambda = 0.2  GeV
* istruc=11 -> mtb2        (msbar), Lambda = 0.2  GeV
* istruc=12 -> mtsn1       (msbar), Lambda = 0.2  GeV
* istruc=13 -> kmrss0      (msbar), Lambda = 0.215 GeV
* istruc=14 -> kmrsd0      (msbar), Lambda = 0.215 GeV
* istruc=15 -> kmrsd-      (msbar), Lambda = 0.215 GeV
* istruc=16 -> mrss0       (msbar), Lambda = 0.230 GeV
* istruc=17 -> mrsd0       (msbar), Lambda = 0.230 GeV
* istruc=18 -> mrsd-       (msbar), Lambda = 0.230 GeV
* istruc=19 -> cteq1l      (msbar), Lambda = 0.168 GeV
* istruc=20 -> cteq1m      (msbar), Lambda = 0.231 GeV
* istruc=21 -> cteq1ml     (msbar), Lambda = 0.322 GeV
* istruc=22 -> cteq1ms     (msbar), Lambda = 0.231 GeV
* istruc=23 -> grv         (msbar), Lambda = 0.200 GeV
* istruc=24 -> cteq2l      (msbar), Lambda = 0.190 GeV
* istruc=25 -> cteq2m      (msbar), Lambda = 0.213 GeV
* istruc=26 -> cteq2ml     (msbar), Lambda = 0.322 GeV
* istruc=27 -> cteq2ms     (msbar), Lambda = 0.208 GeV
* istruc=28 -> cteq2mf     (msbar), Lambda = 0.208 GeV
* istruc=29 -> mrsh        (msbar), Lambda = 0.230 GeV
* istruc=30 -> mrsa        (msbar), Lambda = 0.230 GeV
* istruc=31 -> mrsg        (msbar), Lambda = 0.255 GeV
* istruc=32 -> mrsap       (msbar), Lambda = 0.231 GeV
* istruc=33 -> grv94       (msbar), Lambda = 0.200 GeV
* istruc=34 -> cteq3l      (msbar), Lambda = 0.177 GeV
* istruc=35 -> cteq3m      (msbar), Lambda = 0.239 GeV
* istruc=36 -> mrsj        (msbar), Lambda = 0.366 GeV
* istruc=37 -> mrsjp       (msbar), Lambda = 0.542 GeV
* istruc=38 -> cteqjet     (msbar), Lambda = 0.286 GeV
* istruc=39 -> mrsr1       (msbar), Lambda = 0.241 GeV
* istruc=40 -> mrsr2       (msbar), Lambda = 0.344 GeV
* istruc=41 -> mrsr3       (msbar), Lambda = 0.241 GeV 
* istruc=42 -> mrsr4       (msbar), Lambda = 0.344 GeV 
* istruc=43 -> cteq4l      (msbar), Lambda = 0.300 GeV
* istruc=44 -> cteq4m      (msbar), Lambda = 0.300 GeV
* istruc=45 -> cteq4hj     (msbar), Lambda = 0.300 GeV
* istruc=46 -> cteq4lq     (msbar), Lambda = 0.300 GeV
*
*
* extra structure functions with variable as/Lambda
*
* istruc=101 -> mrsap       (msbar), as(Mz) = 0.110 -> Lambda = 0.216
* istruc=102 -> mrsap       (msbar), as(Mz) = 0.115 -> Lambda = 0.284
* istruc=103 -> mrsap       (msbar), as(Mz) = 0.120 -> Lambda = 0.366
* istruc=104 -> mrsap       (msbar), as(Mz) = 0.125 -> Lambda = 0.458
* istruc=105 -> mrsap       (msbar), as(Mz) = 0.130 -> Lambda = 0.564
* istruc=111 -> grv94       (msbar), Lambda = 0.150 GeV
* istruc=112 -> grv94       (msbar), Lambda = 0.200 GeV = istruc33
* istruc=113 -> grv94       (msbar), Lambda = 0.250 GeV
* istruc=114 -> grv94       (msbar), Lambda = 0.300 GeV
* istruc=115 -> grv94       (msbar), Lambda = 0.350 GeV
* istruc=116 -> grv94       (msbar), Lambda = 0.400 GeV
* istruc=121 -> cteq3m      (msbar), Lambda = 0.100 GeV
* istruc=122 -> cteq3m      (msbar), Lambda = 0.120 GeV
* istruc=123 -> cteq3m      (msbar), Lambda = 0.140 GeV
* istruc=124 -> cteq3m      (msbar), Lambda = 0.180 GeV
* istruc=125 -> cteq3m      (msbar), Lambda = 0.200 GeV
* istruc=126 -> cteq3m      (msbar), Lambda = 0.220 GeV
* istruc=127 -> cteq3m      (msbar), Lambda = 0.260 GeV
* istruc=128 -> cteq3m      (msbar), Lambda = 0.300 GeV
* istruc=129 -> cteq3m      (msbar), Lambda = 0.340 GeV
* istruc=130 -> cteq4m      (msbar), Lambda = 0.215 GeV
* istruc=131 -> cteq4m      (msbar), Lambda = 0.255 GeV
* istruc=132 -> cteq4m      (msbar), Lambda = 0.300 GeV = istruc44
* istruc=133 -> cteq4m      (msbar), Lambda = 0.348 GeV
* istruc=134 -> cteq4m      (msbar), Lambda = 0.401 GeV
*
*
*
      istruc=132
      ipp1=0
      ipp2=1
*
* renormalization and factorization scales (irenorm and ifact): 
*             1. total invariant mass                     * c    
*             2. summed ET                                * c 
*             3. ET of highest ET jet                     * c 
*             4. 100d0                                    * c 
*
      irenorm=3
      crenorm=1d0
      ifact=irenorm
      cfact=crenorm
*
* subprocesses to take along, 0, 2, or 4 quarks. 1 = yes, 0 = no
*
      isub(1)=1
      isub(2)=1
      isub(3)=1
      isub(4)=1       ! dont change these
*
* physics parameters :
*
* center of mass energy
*
      w=1800d0
*
* number of colours (nc) and number of light quarks (nf)
*
      nc=3
      nf=5
      b0=(33d0-2d0*nf)/12d0/pi
*
*  lambda qcd, determined by structure function choice
*
      if (istruc.eq.0) qcdl=0.2d0
      if (istruc.eq.1) qcdl=0.1d0
      if (istruc.eq.2) qcdl=0.2d0
      if (istruc.eq.3) qcdl=0.19d0
      if (istruc.eq.4) qcdl=0.19d0
      if (istruc.eq.5) qcdl=0.19d0
      if (istruc.eq.6) qcdl=0.19d0
      if (istruc.eq.7) qcdl=0.19d0
      if (istruc.eq.8) qcdl=0.144d0
      if (istruc.eq.9) qcdl=0.155d0
      if (istruc.eq.10) qcdl=0.194d0
      if (istruc.eq.11) qcdl=0.191d0
      if (istruc.eq.12) qcdl=0.237d0
      if (istruc.eq.13) qcdl=0.215d0
      if (istruc.eq.14) qcdl=0.215d0
      if (istruc.eq.15) qcdl=0.215d0
      if (istruc.eq.16) qcdl=0.230d0
      if (istruc.eq.17) qcdl=0.230d0
      if (istruc.eq.18) qcdl=0.230d0
      if (istruc.eq.19) qcdl=0.168d0
      if (istruc.eq.20) qcdl=0.231d0
      if (istruc.eq.21) qcdl=0.322d0
      if (istruc.eq.22) qcdl=0.231d0
      if (istruc.eq.23) qcdl=0.200d0
      if (istruc.eq.24) qcdl=0.190d0
      if (istruc.eq.25) qcdl=0.213d0
      if (istruc.eq.26) qcdl=0.322d0
      if (istruc.eq.27) qcdl=0.208d0
      if (istruc.eq.28) qcdl=0.208d0
      if (istruc.eq.29) qcdl=0.230d0
      if (istruc.eq.30) qcdl=0.230d0
      if (istruc.eq.31) qcdl=0.255d0
      if (istruc.eq.32) qcdl=0.231d0
      if (istruc.eq.33) qcdl=0.200d0
      if (istruc.eq.34) qcdl=0.177d0
      if (istruc.eq.35) qcdl=0.239d0
      if (istruc.eq.36) qcdl=0.366d0
      if (istruc.eq.37) qcdl=0.542d0
      if (istruc.eq.38) qcdl=0.286d0
      if (istruc.eq.39) qcdl=0.241d0
      if (istruc.eq.40) qcdl=0.344d0
      if (istruc.eq.41) qcdl=0.241d0
      if (istruc.eq.42) qcdl=0.344d0
      if (istruc.eq.43) qcdl=0.300d0
      if (istruc.eq.44) qcdl=0.300d0
      if (istruc.eq.45) qcdl=0.300d0
      if (istruc.eq.46) qcdl=0.300d0
      if (istruc.eq.101) qcdl=0.216d0
      if (istruc.eq.102) qcdl=0.284d0
      if (istruc.eq.103) qcdl=0.366d0
      if (istruc.eq.104) qcdl=0.458d0
      if (istruc.eq.105) qcdl=0.564d0
      if (istruc.eq.111) qcdl=0.150d0
      if (istruc.eq.112) qcdl=0.200d0
      if (istruc.eq.113) qcdl=0.250d0
      if (istruc.eq.114) qcdl=0.300d0
      if (istruc.eq.115) qcdl=0.350d0
      if (istruc.eq.116) qcdl=0.400d0
      if (istruc.eq.121) qcdl=0.100d0
      if (istruc.eq.122) qcdl=0.120d0
      if (istruc.eq.123) qcdl=0.140d0
      if (istruc.eq.124) qcdl=0.180d0
      if (istruc.eq.125) qcdl=0.200d0
      if (istruc.eq.126) qcdl=0.220d0
      if (istruc.eq.127) qcdl=0.260d0
      if (istruc.eq.128) qcdl=0.300d0
      if (istruc.eq.129) qcdl=0.340d0
      if (istruc.eq.130) qcdl=0.215d0
      if (istruc.eq.131) qcdl=0.255d0
      if (istruc.eq.132) qcdl=0.300d0
      if (istruc.eq.133) qcdl=0.348d0
      if (istruc.eq.134) qcdl=0.401d0
*
*
      iorder = nloop
*
* output settings of monte carlo
*
      write(*,*)
      write(*,*) '*************************************************',
     . '*****************' 
      write(*,*)
      write(*,*) ' release 2.0                                    ',
     . '         01/17/97'
      spp1=' '
      spp2=' '
      if (ipp1.eq.0) spp1='p'
      if (ipp1.eq.1) spp1='pbar'
      if (ipp2.eq.0) spp2='p'
      if (ipp2.eq.1) spp2='pbar'
      if (exclusive) then
        sex ='exclusive'
      else
        sex ='inclusive'
      endif
      if ((spp1.eq.' ').or.(spp2.eq.' ')) then
        write(*,*) '*** input error, ipp1, ipp2 =',ipp1,ipp2
	goto 999
      endif
      norder=max0(njets,2)+nloop
      nloopmx=1
      if(njets.ge.3)nloopmx=0
      if (nloop.gt.nloopmx) then
        write(*,*) '*** input error, njets,nloop =',njets,nloop
        goto 999
      endif
      write(*,11) spp1,spp2,njets,sex,norder,w
   11 format(/,1x,a4,a4,' --> ',i2,' jets ',a9,
     .      ' at O(alphas**',i1,') at ',f6.0,' GeV ')
      write(*,12) delrjj,rsep,etminj,etmaxj,rapminj,rapmaxj
   12 format(/,' jet defining cuts : delta Rjj = ',f4.2,', Rsep = '
     .        ,f4.2,/,/,
     .        '    ',f7.2,' GeV <   ETjet  < ',f7.2,' GeV ',/,/,
     .        '       ',f4.2,'     < |etajet| <    ',f4.2)
      sjet=' '
      if(jetalg.eq.1)sjet=' naive clustering'
      if(jetalg.eq.2)sjet=' CDF   clustering'
      if(jetalg.eq.3)sjet=' D0    clustering'
      write(*,13) sjet
   13 format(/,a7,' recombination scheme')
      sstru=' '
      if (istruc.eq.0)  sstru='test: (1d0-x)  '
      if (istruc.eq.1)  sstru='mrse    (msbar)'
      if (istruc.eq.2)  sstru='mrsb    (msbar)'
      if (istruc.eq.3)  sstru='kmrshb  (msbar)'
      if (istruc.eq.4)  sstru='kmrsb0  (msbar)'
      if (istruc.eq.5)  sstru='kmrsb-  (msbar)'
      if (istruc.eq.6)  sstru='kmrsb-5 (msbar)'
      if (istruc.eq.7)  sstru='kmrsb-2 (msbar)'
      if (istruc.eq.8)  sstru='mts1    (msbar)'
      if (istruc.eq.9)  sstru='mte1    (msbar)'
      if (istruc.eq.10) sstru='mtb1    (msbar)'
      if (istruc.eq.11) sstru='mtb2    (msbar)'
      if (istruc.eq.12) sstru='mtsn1   (msbar)'
      if (istruc.eq.13) sstru='kmrss0  (msbar)'
      if (istruc.eq.14) sstru='kmrsd0  (msbar)'
      if (istruc.eq.15) sstru='kmrsd-  (msbar)'
      if (istruc.eq.16) sstru='mrss0   (msbar)'
      if (istruc.eq.17) sstru='mrsd0   (msbar)'
      if (istruc.eq.18) sstru='mrsd-   (msbar)'
      if (istruc.eq.19) sstru='cteq1l  (msbar)'
      if (istruc.eq.20) sstru='cteq1m  (msbar)'
      if (istruc.eq.21) sstru='cteq1ml (msbar)'
      if (istruc.eq.22) sstru='cteq1ms (msbar)'
      if (istruc.eq.23) sstru='grv     (msbar)'
      if (istruc.eq.24) sstru='cteq2l  (msbar)'
      if (istruc.eq.25) sstru='cteq2m  (msbar)'
      if (istruc.eq.26) sstru='cteq2ml (msbar)'
      if (istruc.eq.27) sstru='cteq2ms (msbar)'
      if (istruc.eq.28) sstru='cteq2mf (msbar)'
      if (istruc.eq.29) sstru='mrsh    (msbar)'
      if (istruc.eq.30) sstru='mrsa    (msbar)'
      If (istruc.eq.31) sstru='mrsg    (msbar)'
      if (istruc.eq.32) sstru='mrsap   (msbar)'
      if (istruc.eq.33) sstru='grv94   (msbar)'
      if (istruc.eq.34) sstru='cteq3l  (msbar)'
      if (istruc.eq.35) sstru='cteq3m  (msbar)'
      if (istruc.eq.36) sstru='mrsj    (msbar)'
      if (istruc.eq.37) sstru='mrsjp   (msbar)'
      if (istruc.eq.38) sstru='cteqjet (msbar)'
      if (istruc.eq.39) sstru='mrsr1   (msbar)'
      if (istruc.eq.40) sstru='mrsr2   (msbar)'
      if (istruc.eq.41) sstru='mrsr3   (msbar)'
      if (istruc.eq.42) sstru='mrsr4   (msbar)'
      if (istruc.eq.43) sstru='cteq4l  (msbar)'
      if (istruc.eq.44) sstru='cteq4m  (msbar)'
      if (istruc.eq.45) sstru='cteq4lq (msbar)'
      if (istruc.eq.46) sstru='cteq4lq (msbar)'
      if (istruc.eq.101) sstru='mrsap (Lambda=0.216)'
      if (istruc.eq.102) sstru='mrsap (Lambda=0.284)'
      if (istruc.eq.103) sstru='mrsap (Lambda=0.366)'
      if (istruc.eq.104) sstru='mrsap (Lambda=0.458)'
      if (istruc.eq.105) sstru='mrsap (Lambda=0.564)'
      if (istruc.eq.111) sstru='grv94 (Lambda=0.150)'
      if (istruc.eq.112) sstru='grv94 (Lambda=0.200)'
      if (istruc.eq.113) sstru='grv94 (Lambda=0.250)'
      if (istruc.eq.114) sstru='grv94 (Lambda=0.300)'
      if (istruc.eq.115) sstru='grv94 (Lambda=0.350)'
      if (istruc.eq.116) sstru='grv94 (Lambda=0.400)'
      if (istruc.eq.121) sstru='cteq3m (msbar)'
      if (istruc.eq.122) sstru='cteq3m (msbar)'
      if (istruc.eq.123) sstru='cteq3m (msbar)'
      if (istruc.eq.124) sstru='cteq3m (msbar)'
      if (istruc.eq.125) sstru='cteq3m (msbar)'
      if (istruc.eq.126) sstru='cteq3m (msbar)'
      if (istruc.eq.127) sstru='cteq3m (msbar)'
      if (istruc.eq.128) sstru='cteq3m (msbar)'
      if (istruc.eq.129) sstru='cteq3m (msbar)'
      if (istruc.eq.130) sstru='cteq4m (msbar)'
      if (istruc.eq.131) sstru='cteq4m (msbar)'
      if (istruc.eq.132) sstru='cteq4m (msbar)'
      if (istruc.eq.133) sstru='cteq4m (msbar)'
      if (istruc.eq.134) sstru='cteq4m (msbar)'
*
      if (sstru.eq.' ') then
        write(*,*) '*** input error, istruc =',istruc
        goto 999
      endif
      write(*,15) sstru
   15 format(/,' structure function : ',a16)
      write(*,16) smin
   16 format(/,' parton resolution cut = ',g12.5,' GeV^2')
      write(*,17) safety,s0 
   17 format(/,' safety cut = smin / ',f8.2,' = ',g12.5,' GeV^2')
*
      sir=' '
      if (irenorm.eq.1) sir='parton centre of mass energy'
      if (irenorm.eq.2) sir='sum ET of obs. jets'
      if (irenorm.eq.3) sir='ET of max ET jet'
      if (irenorm.eq.4) sir='100 GeV'
      if (sir.eq.' ') then
        write(*,*) '*** input error, irenorm =',irenorm
	goto 999
      endif
      write(*,18) crenorm,sir
   18 format(/,' renormalization scale : ',f5.2,' * ',a32)
*
      sir=' '
      if (ifact.eq.1) sir='parton centre of mass energy'
      if (ifact.eq.2) sir='sum ET of obs. jets'
      if (ifact.eq.3) sir='ET of max ET jet'
      if (ifact.eq.4) sir='100 GeV'
      if (sir.eq.' ') then
        write(*,*) '*** input error, ifact =',ifact
	goto 999
      endif
      write(*,19) cfact,sir
   19 format(/,' factorization scale   : ',f5.2,' * ',a32)
*
      if(iorder.eq.0)then
        as=alfas(91.17d0,qcdl,1)
        sstru='one loop'
      elseif(iorder.eq.1)then
        as=alfas(91.17d0,qcdl,2)
        sstru=' two loop'
      endif
      write(*,20)nf,qcdl,sstru,as
   20 format(/,' active quark flavours              =   ',i6,/,/,
     .         ' QCD lambda for 4 flavours          =   ',f6.3,/,/,
     .         a9,' alphas(m_z)               = ',f8.5)
      write(*,22) itmax2,nshot2,nshot4 
   22 format(/,' number of events : ',i3,
     .         '*(',i8,' + ',i8,' )')
      if(isub(1).eq.1)son='on '
      if(isub(1).eq.0)son='off'
      write(*,23)son
   23 format(/,'         zero quark processes : ',a3)
      if(isub(2).eq.1)son='on '
      if(isub(2).eq.0)son='off'
      write(*,24)son
   24 format(/,'          two quark processes : ',a3)
      if(isub(3).eq.1)son='on '
      if(isub(3).eq.0)son='off'
      write(*,25)son
   25 format(/,'  unlike four quark processes : ',a3)
      if(isub(4).eq.1)son='on '
      if(isub(4).eq.0)son='off'
      write(*,26)son
   26 format(/,'    like four quark processes : ',a3)
      write(*,*)
      write(*,*) '=================================================',
     . '=================' 
      write(*,*) 

*
* calculate cross section
*
      call cross(sig,sd)
*
* output results and distributions
*
      write(*,30) njets,sig,sd
   30 format(/,'  sigma(',i2,' jets) = ',g12.5,'  +/-',g12.5,' nb ',/)
      call bino(4,0d0,0)
*
      write(*,*)
      write(*,*) '*************************************************',
     . '*****************' 
      write(*,*)
  999 continue

      end
*
************************************************************************
*                                                                      *
* write distributions                                                  *
*                                                                      *
************************************************************************
      subroutine bino(istat,wgt,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /fraction/ x1,x2,facscale,renscale
      common /jetmom/ pjet(8,10),jp(10)
      common /parmom/ ppar(4,10)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /sin/ s(10,10),th(10,10)
*
* pjet(5,j) = ET
* pjet(6,j) = pseudorapidity
*
* iplot = 1: CDF single jet inclusive distribution
* iplot = 2: D0 single jet inclusive distribution
* iplot = 3: CDF two jet dsigma/det/deta1/deta2 distribution
* iplot = 4: D0 two jet dsigma/det/deta1/deta2 distribution
* iplot = 5: CDF sameside/opposite side jet dsigma/det/deta1/deta2 
* iplot = 6: D0 signed jet dsigma/det/deta1/deta2 distribution
*
*
* init histograms
*
      iplot=1
      nhis=10
      if (istat.eq.0) then
      if(iplot.eq.1)then
*
* CDF single jet inclusive distribution
*
         call histoi(1,0d0,500d0,50)
         call histoi(2,0d0,500d0,50)
         call histoi(3,0d0,500d0,50)
         call histoi(4,0d0,500d0,50)
      elseif(iplot.eq.2)then
*
* D0 single jet inclusive distribution
*
         call histoi(1,0d0,500d0,50)
         call histoi(2,0d0,500d0,50)
         call histoi(3,0d0,500d0,50)
         call histoi(4,0d0,500d0,50)
         call histoi(5,0d0,500d0,50)
         call histoi(6,0d0,500d0,50)
         call histoi(7,0d0,500d0,50)
         call histoi(8,0d0,500d0,50)
      elseif(iplot.eq.3)then
*
* CDF two jet dsigma/det/deta1/deta2 distribution
*
         call histoi(1,-4d0,4d0,40)
         call histoi(2,-4d0,4d0,40)
         call histoi(3,-4d0,4d0,40)
         call histoi(4,-4d0,4d0,40)
         call histoi(5,-4d0,4d0,40)
         call histoi(6,-4d0,4d0,40)
      elseif(iplot.eq.4)then
*
* D0 two jet dsigma/det/deta1/deta2 distribution
*
         call histoi(1,-4d0,4d0,40)
         call histoi(2,-4d0,4d0,40)
         call histoi(3,-4d0,4d0,40)
         call histoi(4,-4d0,4d0,40)
      elseif(iplot.eq.5)then
*
* CDF sameside/opposite side jet dsigma/det/deta1/deta2 distribution
*
         call histoi(1,-4d0,4d0,40)
         call histoi(2,-4d0,4d0,40)
         call histoi(3,-4d0,4d0,40)
         call histoi(4,-4d0,4d0,40)
         call histoi(5,-4d0,4d0,40)
         call histoi(6,-4d0,4d0,40)
         call histoi(7,-4d0,4d0,40)
         call histoi(8,-4d0,4d0,40)
      elseif(iplot.eq.6)then
*
* D0 signed jet dsigma/det/deta1/deta2 distribution
*
         call histoi(1,-4d0,4d0,40)
         call histoi(2,-4d0,4d0,40)
      endif
      endif
*
* write event into histogram
*
      if (istat.eq.1) then
      if(iplot.eq.1)then
*
* CDF single jet inclusive distribution
*
        do i=1,npar          
          j=jp(i)
          if(pjet(4,j).gt.0d0)then
            et=pjet(5,j)
            etaj=abs(pjet(6,j))
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0d0).and.(etaj.lt.1d0))
     .           call histoa(1,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.1d0).and.(etaj.lt.2d0))
     .           call histoa(2,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.2d0).and.(etaj.lt.3d0))
     .           call histoa(3,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.3d0).and.(etaj.lt.4d0))
     .           call histoa(4,et,wgt/2d0)
          endif
        enddo
      elseif(iplot.eq.2)then
*
* D0 single jet inclusive distribution
*
        do i=1,npar          
          j=jp(i)
          if(pjet(4,j).gt.0d0)then
            et=pjet(5,j)
            etaj=abs(pjet(6,j))
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0d0).and.(etaj.lt.4d0))
     .           call histoa(1,et,wgt/8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0d0).and.(etaj.lt.0.9d0))
     .           call histoa(2,et,wgt/1.8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.1d0).and.(etaj.lt.2d0))
     .           call histoa(3,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.2d0).and.(etaj.lt.3d0))
     .           call histoa(4,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0.4d0).and.(etaj.lt.0.8d0))
     .           call histoa(5,et,wgt/0.8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0.8d0).and.(etaj.lt.1.2d0))
     .           call histoa(6,et,wgt/0.8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.1.2d0).and.(etaj.lt.1.6d0))
     .           call histoa(7,et,wgt/0.8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.1.6d0).and.(etaj.lt.2d0))
     .           call histoa(8,et,wgt/0.8d0)
          endif
        enddo
      elseif(iplot.eq.3)then
*
* CDF two jet dsigma/det/deta1/deta2 distribution
*
        j1=jp(1)
        j2=jp(2)
        if(pjet(4,j1).gt.0d0.and.pjet(4,j2).gt.0d0)then
          et1=pjet(5,j1)
          eta1=abs(pjet(6,j1))
          et2=pjet(5,j2)
          eta2=abs(pjet(6,j2))
          if(et1.gt.etminj.and.et1.lt.etmaxj.and.et2.gt.10d0)then
            if(eta1.gt.0.1d0.and.eta1.lt.0.7d0)then
              if(eta2.gt.0.1d0.and.eta2.lt.0.7d0) 
     .          call histoa(1,et1,wgt/1.2d0/1.2d0)
              if(eta2.gt.0.7d0.and.eta2.lt.1.2d0) 
     .          call histoa(2,et1,wgt/1.2d0/1.0d0)
              if(eta2.gt.1.2d0.and.eta2.lt.1.6d0) 
     .          call histoa(3,et1,wgt/1.2d0/0.8d0)
              if(eta2.gt.1.6d0.and.eta2.lt.2.0d0) 
     .          call histoa(4,et1,wgt/1.2d0/0.8d0)
              if(eta2.gt.2.0d0.and.eta2.lt.3.0d0) 
     .          call histoa(5,et1,wgt/1.2d0/2d0)
              if(eta2.gt.3.0d0.and.eta2.lt.4.0d0) 
     .          call histoa(6,et1,wgt/1.2d0/2d0)
             endif
          endif
          if(et2.gt.etminj.and.et2.lt.etmaxj.and.et1.gt.10d0)then
            if(eta2.gt.0.1d0.and.eta2.lt.0.7d0)then
              if(eta1.gt.0.1d0.and.eta1.lt.0.7d0)
     .          call histoa(1,et2,wgt/1.2d0/1.2d0)
              if(eta1.gt.0.7d0.and.eta1.lt.1.2d0)
     .          call histoa(2,et2,wgt/1.2d0/1.0d0)
              if(eta1.gt.1.2d0.and.eta1.lt.1.6d0)
     .          call histoa(3,et2,wgt/1.2d0/0.8d0)
              if(eta1.gt.1.6d0.and.eta1.lt.2.0d0)
     .          call histoa(4,et2,wgt/1.2d0/0.8d0)
              if(eta1.gt.2.0d0.and.eta1.lt.3.0d0)
     .          call histoa(5,et2,wgt/1.2d0/2d0)
              if(eta1.gt.3.0d0.and.eta1.lt.4.0d0)
     .          call histoa(6,et2,wgt/1.2d0/2d0)
            endif 
          endif
        endif
      elseif(iplot.eq.4)then
*
* D0 two jet dsigma/det/deta1/deta2 distribution
*
        j1=jp(1)
        j2=jp(2)
        if(pjet(4,j1).gt.0d0.and.pjet(4,j2).gt.0d0)then
          et1=pjet(5,j1)
          eta1=abs(pjet(6,j1))
          et2=pjet(5,j2)
          eta2=abs(pjet(6,j2))
          if(et1.gt.etminj.and.et1.lt.etmaxj.and.et2.gt.20d0)then
            if(eta1.gt.0d0.and.eta1.lt.1d0)then
              if(eta2.gt.0d0.and.eta2.lt.1d0) 
     .          call histoa(1,et1,wgt/2d0/2d0)
              if(eta2.gt.1d0.and.eta2.lt.2d0) 
     .          call histoa(2,et1,wgt/2d0/2d0)
              if(eta2.gt.2d0.and.eta2.lt.3d0) 
     .          call histoa(3,et1,wgt/2d0/2d0)
              if(eta2.gt.3d0.and.eta2.lt.4d0) 
     .          call histoa(4,et1,wgt/2d0/2d0)
             endif
          endif
          if(et2.gt.etminj.and.et2.lt.etmaxj.and.et1.gt.20d0)then
            if(eta2.gt.0d0.and.eta2.lt.1d0)then
              if(eta1.gt.0d0.and.eta1.lt.1d0)
     .          call histoa(1,et2,wgt/2d0/2d0)
              if(eta1.gt.1d0.and.eta1.lt.2d0)
     .          call histoa(2,et2,wgt/2d0/2d0)
              if(eta1.gt.2d0.and.eta1.lt.3d0)
     .          call histoa(3,et2,wgt/2d0/2d0)
              if(eta1.gt.3d0.and.eta1.lt.4d0)
     .          call histoa(4,et2,wgt/2d0/2d0)
            endif 
          endif
        endif
      elseif(iplot.eq.5)then
*
* CDF sameside/opposite side jet dsigma/det/deta1/deta2 distribution
*
        j1=jp(1)
        j2=jp(2)
        if(pjet(4,j1).gt.0d0.and.pjet(4,j2).gt.0d0)then
          et1=pjet(5,j1)
          eta1=pjet(6,j1)  
          et2=pjet(5,j2)
          eta2=pjet(6,j2)

          pt1=sqrt(pjet(1,j1)**2+pjet(2,j1)**2)
          pt2=sqrt(pjet(1,j2)**2+pjet(2,j2)**2)
          cs=(pjet(1,j1)*pjet(1,j2)+pjet(2,j1)*pjet(2,j2))/pt1/pt2
          phi12=acos(max(cs,-1d0))

          if (phi12.gt.(pi-0.7d0).and.et2.gt.10d0) then
            if(eta1.gt.0d0)ieta1=eta1/0.2d0+1
            if(eta1.lt.0d0)ieta1=eta1/0.2d0-1
            if(eta2.gt.0d0)ieta2=eta2/0.2d0+1
            if(eta2.lt.0d0)ieta2=eta2/0.2d0-1

            if(et1.gt.27d0.and.et1.lt.60d0)then
              if(ieta1.eq.-ieta2)call histoa(1,eta1,wgt)
              if(ieta1.eq.ieta2)call histoa(2,eta1,wgt)
            endif
            if(et1.gt.60d0.and.et1.lt.80d0)then
              if(ieta1.eq.-ieta2)call histoa(3,eta1,wgt)
              if(ieta1.eq.ieta2)call histoa(4,eta1,wgt)
            endif
            if(et1.gt.80d0.and.et1.lt.110d0)then
              if(ieta1.eq.-ieta2)call histoa(5,eta1,wgt)
              if(ieta1.eq.ieta2)call histoa(6,eta1,wgt)
            endif
            if(et1.gt.110d0.and.et1.lt.350d0)then
              if(ieta1.eq.-ieta2)call histoa(7,eta1,wgt)
              if(ieta1.eq.ieta2)call histoa(8,eta1,wgt)
            endif
            if(et2.gt.27d0.and.et2.lt.60d0)then
              if(ieta2.eq.-ieta1)call histoa(1,eta2,wgt)
              if(ieta2.eq.ieta1)call histoa(2,eta2,wgt)
            endif
            if(et2.gt.60d0.and.et2.lt.80d0)then
              if(ieta2.eq.-ieta1)call histoa(3,eta2,wgt)
              if(ieta2.eq.ieta1)call histoa(4,eta2,wgt)
            endif
            if(et2.gt.80d0.and.et2.lt.110d0)then
              if(ieta2.eq.-ieta1)call histoa(5,eta2,wgt)
              if(ieta2.eq.ieta1)call histoa(6,eta2,wgt)
            endif
            if(et2.gt.110d0.and.et2.lt.350d0)then
              if(ieta2.eq.-ieta1)call histoa(7,eta2,wgt)
              if(ieta2.eq.ieta1)call histoa(8,eta2,wgt)
            endif
          endif
        endif
      elseif(iplot.eq.6)then
*
* D0 signed jet dsigma/det/deta1/deta2 distribution
*
        j1=jp(1)
        j2=jp(2)
        if(pjet(4,j1).gt.0d0.and.pjet(4,j2).gt.0d0)then
          et1=pjet(5,j1)
          eta1=pjet(6,j1)
          et2=pjet(5,j2)
          eta2=pjet(6,j2)
          sign=eta1*eta2/abs(eta1)/abs(eta2)
          if(et1.gt.45d0.and.et1.lt.55d0.and.et2.gt.20d0)then
            if(abs(eta1).gt.0d0.and.abs(eta1).lt.0.5d0)then
              call histoa(1,abs(eta2)*sign,wgt)
            endif
          endif
          if(et2.gt.45d0.and.et2.lt.55d0.and.et1.gt.20d0)then
            if(abs(eta2).gt.0d0.and.abs(eta2).lt.0.5d0)then
              call histoa(1,abs(eta1)*sign,wgt)
            endif
          endif
          if(et1.gt.55d0.and.et1.lt.65d0.and.et2.gt.20d0)then
            if(abs(eta1).gt.2d0.and.abs(eta1).lt.2.5d0)then
              call histoa(2,abs(eta2)*sign,wgt)
            endif
          endif
          if(et2.gt.55d0.and.et2.lt.65d0.and.et1.gt.20d0)then
            if(abs(eta2).gt.2d0.and.abs(eta2).lt.2.5d0)then
              call histoa(2,abs(eta1)*sign,wgt)
            endif
          endif
        endif
      endif
      endif
*
* output distributions
*
      if (istat.eq.4) then
      if(iplot.eq.1)then
          write(*,*) ' CDF single jet inclusive  0.0 - 1.0'
          call histow(1)
          write(*,*) ' CDF single jet inclusive  1.0 - 2.0'
          call histow(2)
          write(*,*) ' CDF single jet inclusive  2.0 - 3.0'
          call histow(3)
          write(*,*) ' CDF single jet inclusive  3.0 - 4.0'
          call histow(4)
      elseif(iplot.eq.2)then
          write(*,*) ' D0 single jet inclusive  0.0 - 4.0'
          call histow(1)
          write(*,*) ' D0 single jet inclusive  0.0 - 0.9'
          call histow(2)
          write(*,*) ' D0 single jet inclusive  1.0 - 2.0'
          call histow(3)
          write(*,*) ' D0 single jet inclusive  2.0 - 3.0'
          call histow(4)
          write(*,*) ' D0 single jet inclusive  0.4 - 0.8'
          call histow(5)
          write(*,*) ' D0 single jet inclusive  0.8 - 1.2'
          call histow(6)
          write(*,*) ' D0 single jet inclusive  1.2 - 1.6'
          call histow(7)
          write(*,*) ' D0 single jet inclusive  1.6 - 2.0'
          call histow(8)
      elseif(iplot.eq.3)then
          write(*,*) ' CDF two jet   0.1 < eta2 < 0.7'
          call histow(1)
          write(*,*) ' CDF two jet   0.7 < eta2 < 1.2'
          call histow(2)
          write(*,*) ' CDF two jet   1.2 < eta2 < 1.6'
          call histow(3)
          write(*,*) ' CDF two jet   1.6 < eta2 < 2.0'
          call histow(4)
          write(*,*) ' CDF two jet   2.0 < eta2 < 3.0'
          call histow(5)
          write(*,*) ' CDF two jet   3.0 < eta2 < 4.0'
          call histow(6)
      elseif(iplot.eq.4)then
          write(*,*) ' D0 two jet    0.0 < eta2 < 1.0'
          call histow(1)
          write(*,*) ' D0 two jet    1.0 < eta2 < 2.0'
          call histow(2)
          write(*,*) ' D0 two jet    2.0 < eta2 < 3.0'
          call histow(3)
          write(*,*) ' D0 two jet    3.0 < eta2 < 4.0'
          call histow(4)
      elseif(iplot.eq.5)then
          write(*,*) ' CDF  opp side   27 < et < 60'
          call histow(1)
          write(*,*) ' CDF same side   27 < et < 60'
          call histow(2)
          write(*,*) ' CDF  opp side   60 < et < 80'
          call histow(3)
          write(*,*) ' CDF same side   60 < et < 80'
          call histow(4)
          write(*,*) ' CDF  opp side   80 < et < 110'
          call histow(5)
          write(*,*) ' CDF same side   80 < et < 110'
          call histow(6)
          write(*,*) ' CDF  opp side   110 < et < 350'
          call histow(7)
          write(*,*) ' CDF same side   110 < et < 350'
          call histow(8)
      elseif(iplot.eq.6)then
          write(*,*) ' D0 signed   45 < et < 55, 0.0 < eta < 0.5'
          call histow(1)
          write(*,*) ' D0 signed   55 < et < 65, 2.0 < eta < 2.5'
          call histow(2)
      endif
      endif
*
* event errors manipulation request, pipe through
*
      if ((istat.eq.2).or.(istat.eq.3)) then
         do i=1,nhis
            call histoe(istat,i)
         enddo
      endif
*
      return
      end
*
*
************************************************************************
*
      subroutine distrib(wtdis,jets)
      implicit real*8(a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /jetmom/ pjet(8,10),jp(10)
      data init/0/
*
* Events are generated uniformly relative to the function
* programmed in distrib. If this function is choosen unity
* events will be generated according to cross section
* density.
* Current function set to one-jet inclusinve Et distribution
* at 1800 GeV. If changing beam energies adjust function. If
* not clear what to do change it to unity (comment out the
* last statement wtdis=1). This guarantees correct result
* albeit poor statistics at high Et.
*
      if(init.eq.0)then
        write(*,*)' ***** WARNING ******'
        write(*,*)' Subroutine DISTRIB optimised for
     . one-jet inclusive Et distribution at 1800 GeV'
        write(*,*)' (Change subroutine DISTRIB for other
     . beam energies and/or other observables)'
        init=1
      endif      
      sa=4.1d0
      sb=0.019d0
      pt=0d0
      ptmax=0d0
      do i=1,jets
         j=jp(i)
         if (pjet(4,j).gt.0d0) then
            pti=pjet(5,j)
            if(pti.gt.ptmax)ptmax=pti
         endif
      enddo
      wtdis=1d0/ptmax**sa*exp(-ptmax*sb)
*
*      wtdis=1d0
*
      return
      end
*
