************************************************************************ 
*                                                                       
*
*      p pbar -> V + 0 or 1 jets  at up to O(alphas**2)                 
*
*                                                                       
*
*      RELEASE 4.1, 17/07/97                                            
*
*                                                                       
*
************************************************************************ 

      program dyrad
      implicit real*8 (a-h,o-z)
      character*5 svec,spp1,spp2
      character*128 sstru,sir,sreson,sex,sjet
      common /phypar/ w,ipp1,ipp2,rmw,rgw,rmz,rgz,sw2,qcdl
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /thcut/ smin,s0
      common /lepcut/ etminl,delrjl,rapmaxl,etmis,rlepmin,rlepmax,raphad
      common /koppel/ nf,b0,cru,clu,crd,cld,crl,cll,
     .                cru2,clu2,crd2,cld2,crl2,cll2
      common /normal/exclusive
      common /order/iorder
      logical exclusive
*
* input parameters
*
* number of jets and order
*
* if final state exclusive (exclusive=.true.)
* there are exactly njets clusters passing the
* jet selection cuts, all possible other clusters fail the jet selection cuts.
* if final state inclusive (exclusive=.false.)
* there is are least njets cluster passing the jet cuts, other clusters could
* pass the jet selection cuts. 
*
      njets=1
      nloop=1
      exclusive=.false.
* shots and iterations per sweep
*
      itmax1=5
      itmax2=10 
      nshot1=2000
      nshot2=10000
      nshot3=20000
      nshot4=100000
*
* experimental jet cuts
*
      etminj=10d0
      etmaxj=500d0
      rapmaxj=3.5d0
      rapminj=0d0
*
* clustering criterion
*       jalg1 = 1 ; deltaR(i,j)   < delrjj
*       jalg1 = 2 ; deltaR(i,jet) < delrjj and deltaR(j,jet) < delrjj
*       jalg1 = 3 ; kt algorithm; R = delrjj
*       jalg1 = 4 ; deltaR(i,jet) < delrjj and deltaR(j,jet) < delrjj
*                      but deltaR(i,j) < Rsep
*
      jalg1=4
* recombination scheme
*       jalg2 = 1 is D0 eta/phi
*       jalg2 = 2 is Snowmass
*       jalg2 = 3 is 4 momentum - ET = sqrt(px**2+py**2)
*       jalg2 = 4 is 4 momentum - ET = E sin(theta)
*
      jalg2=1
* conesize
      delrjj=0.7d0      

*
*     if jalg1 = 4, must set rsep
*
      rsep=1.3d0*delrjj
*
* experimental lepton cuts
*
      etminl=25d0
      etmis =25d0
      delrjl=0.4d0
      rapmaxl=1.1d0
      rlepmin =60d0
      rlepmax =100d0
*
* hadron rapidity coverage (missing E_t reconstruction)
*
      raphad=4d0
*
* theoretical cut
*
      smin=10d0         ! ask if you want to change smin or safety
      safety=1d3           
      s0=smin/safety       
*
* vector boson type (ivec=0 -> w- + w+ , ivec=1 -> w- , ivec=2 -> w+
*                    ivec=3 -> z0 )
* resonance (ireson=0 -> breit wigner, ireson=1 -> narrow width)
*
      ivec=3
      ireson=0
*
* structure functions 
*
* istruc=1  -> mrse        (msbar), Lambda = 0.1  GeV
* istruc=2  -> mrsb        (msbar), Lambda = 0.2  GeV
* istruc=3  -> kmrshb      (msbar), Lambda = 0.19 GeV
* istruc=4  -> kmrsb0      (msbar), Lambda = 0.19 GeV
* istruc=5  -> kmrsb-      (msbar), Lambda = 0.19 GeV
* istruc=6  -> kmrsh- (5)  (msbar), Lambda = 0.19 GeV
* istruc=7  -> kmrsh- (2)  (msbar), Lambda = 0.19 GeV
* istruc=8  -> mts1        (msbar), Lambda = 0.2  GeV
* istruc=9  -> mte1        (msbar), Lambda = 0.2  GeV
* istruc=10 -> mtb1        (msbar), Lambda = 0.2  GeV
* istruc=11 -> mtb2        (msbar), Lambda = 0.2  GeV
* istruc=12 -> mtsn1       (msbar), Lambda = 0.2  GeV
* istruc=13 -> kmrss0      (msbar), Lambda = 0.215 GeV
* istruc=14 -> kmrsd0      (msbar), Lambda = 0.215 GeV
* istruc=15 -> kmrsd-      (msbar), Lambda = 0.215 GeV
* istruc=16 -> mrss0       (msbar), Lambda = 0.230 GeV
* istruc=17 -> mrsd0       (msbar), Lambda = 0.230 GeV
* istruc=18 -> mrsd-       (msbar), Lambda = 0.230 GeV
* istruc=19 -> cteq1l      (msbar), Lambda = 0.168 GeV
* istruc=20 -> cteq1m      (msbar), Lambda = 0.231 GeV
* istruc=21 -> cteq1ml     (msbar), Lambda = 0.322 GeV
* istruc=22 -> cteq1ms     (msbar), Lambda = 0.231 GeV
* istruc=23 -> grv         (msbar), Lambda = 0.200 GeV
* istruc=24 -> cteq2l      (msbar), Lambda = 0.190 GeV
* istruc=25 -> cteq2m      (msbar), Lambda = 0.213 GeV
* istruc=26 -> cteq2ml     (msbar), Lambda = 0.322 GeV
* istruc=27 -> cteq2ms     (msbar), Lambda = 0.208 GeV
* istruc=28 -> cteq2mf     (msbar), Lambda = 0.208 GeV
* istruc=29 -> mrsh        (msbar), Lambda = 0.230 GeV
* istruc=30 -> mrsa        (msbar), Lambda = 0.230 GeV
* istruc=31 -> mrsg        (msbar), Lambda = 0.255 GeV
* istruc=32 -> mrsap       (msbar), Lambda = 0.231 GeV
* istruc=33 -> grv94       (msbar), Lambda = 0.200 GeV
* istruc=34 -> cteq3l      (msbar), Lambda = 0.177 GeV
* istruc=35 -> cteq3m      (msbar), Lambda = 0.239 GeV
* istruc=36 -> mrsj        (msbar), Lambda = 0.366 GeV
* istruc=37 -> mrsjp       (msbar), Lambda = 0.542 GeV
* istruc=38 -> cteqjet     (msbar), Lambda = 0.286 GeV
* istruc=39 -> mrsr1       (msbar), Lambda = 0.241 GeV
* istruc=40 -> mrsr2       (msbar), Lambda = 0.344 GeV
* istruc=41 -> mrsr3       (msbar), Lambda = 0.241 GeV 
* istruc=42 -> mrsr4       (msbar), Lambda = 0.344 GeV 
* istruc=43 -> cteq4l      (msbar), Lambda = 0.300 GeV
* istruc=44 -> cteq4m      (msbar), Lambda = 0.300 GeV
* istruc=45 -> cteq4hj     (msbar), Lambda = 0.300 GeV
* istruc=46 -> cteq4lq     (msbar), Lambda = 0.300 GeV
*
*
* extra structure functions with variable as/Lambda
*
* istruc=100 -> mrsap       (msbar), as(Mz) = 0.105 -> Lambda = 0.150
* istruc=101 -> mrsap       (msbar), as(Mz) = 0.110 -> Lambda = 0.216
* istruc=102 -> mrsap       (msbar), as(Mz) = 0.115 -> Lambda = 0.284
* istruc=103 -> mrsap       (msbar), as(Mz) = 0.120 -> Lambda = 0.366
* istruc=104 -> mrsap       (msbar), as(Mz) = 0.125 -> Lambda = 0.458
* istruc=105 -> mrsap       (msbar), as(Mz) = 0.130 -> Lambda = 0.564
* istruc=111 -> grv94       (msbar), Lambda = 0.150 GeV
* istruc=112 -> grv94       (msbar), Lambda = 0.200 GeV = istruc33
* istruc=113 -> grv94       (msbar), Lambda = 0.250 GeV
* istruc=114 -> grv94       (msbar), Lambda = 0.300 GeV
* istruc=115 -> grv94       (msbar), Lambda = 0.350 GeV
* istruc=116 -> grv94       (msbar), Lambda = 0.400 GeV
* istruc=121 -> cteq3m      (msbar), Lambda = 0.100 GeV
* istruc=122 -> cteq3m      (msbar), Lambda = 0.120 GeV
* istruc=123 -> cteq3m      (msbar), Lambda = 0.140 GeV
* istruc=124 -> cteq3m      (msbar), Lambda = 0.180 GeV
* istruc=125 -> cteq3m      (msbar), Lambda = 0.200 GeV
* istruc=126 -> cteq3m      (msbar), Lambda = 0.220 GeV
* istruc=127 -> cteq3m      (msbar), Lambda = 0.260 GeV
* istruc=128 -> cteq3m      (msbar), Lambda = 0.300 GeV
* istruc=129 -> cteq3m      (msbar), Lambda = 0.340 GeV
* istruc=130 -> cteq4m      (msbar), Lambda = 0.215 GeV
* istruc=131 -> cteq4m      (msbar), Lambda = 0.255 GeV
* istruc=132 -> cteq4m      (msbar), Lambda = 0.300 GeV = istruc44
* istruc=133 -> cteq4m      (msbar), Lambda = 0.348 GeV
* istruc=134 -> cteq4m      (msbar), Lambda = 0.401 GeV
*
*
* And proton/antiproton flag (ipp=0 -> proton, ipp=1 -> anti proton)
*
      istruc=44
      ipp1=0
      ipp2=1
*
* renormalization & factorization scale :
*     Q_ren=crenorm * scale
*     Q_fac=cfact   * scale
*     scale^2  = 1. (total invariant mass)^2
*                2. vector boson mass (dynamical)
*                3. vector boson mass (on-shell )
*                4. M(V)^2 + P_t(V)^2
*                5. leading jet Et
*                6. P_T(V)
*

      irenorm=3
      crenorm=1d0
      ifact  =irenorm
      cfact  =crenorm
*
* physics parameters :
*
* center of mass energy
*
      w=1800d0 
*
* w mass and width
*
      rmw=80.26d0
      rgw=2.07d0
*
* z mass and width
*
      rmz=91.187d0
      rgz=2.49d0
*
* sin^2 weak angle
*
      sw2=1d0-(rmw/rmz)**2
      sw2=0.2236d0
*
*
*  lambda qcd, determined by structure function choise
*
      if (istruc.eq.1) qcdl=0.1d0
      if (istruc.eq.2) qcdl=0.2d0
      if (istruc.eq.3) qcdl=0.19d0
      if (istruc.eq.4) qcdl=0.19d0
      if (istruc.eq.5) qcdl=0.19d0
      if (istruc.eq.6) qcdl=0.19d0
      if (istruc.eq.7) qcdl=0.19d0
      if (istruc.eq.8) qcdl=0.144d0
      if (istruc.eq.9) qcdl=0.155d0
      if (istruc.eq.10) qcdl=0.194d0
      if (istruc.eq.11) qcdl=0.191d0
      if (istruc.eq.12) qcdl=0.237d0
      if (istruc.eq.13) qcdl=0.215d0
      if (istruc.eq.14) qcdl=0.215d0
      if (istruc.eq.15) qcdl=0.215d0
      if (istruc.eq.16) qcdl=0.230d0
      if (istruc.eq.17) qcdl=0.230d0
      if (istruc.eq.18) qcdl=0.230d0
      if (istruc.eq.19) qcdl=0.168d0
      if (istruc.eq.20) qcdl=0.231d0
      if (istruc.eq.21) qcdl=0.322d0
      if (istruc.eq.22) qcdl=0.231d0
      if (istruc.eq.23) qcdl=0.200d0
      if (istruc.eq.24) qcdl=0.190d0
      if (istruc.eq.25) qcdl=0.213d0
      if (istruc.eq.26) qcdl=0.322d0
      if (istruc.eq.27) qcdl=0.208d0
      if (istruc.eq.28) qcdl=0.208d0
      if (istruc.eq.29) qcdl=0.230d0
      if (istruc.eq.30) qcdl=0.230d0
      if (istruc.eq.31) qcdl=0.255d0
      if (istruc.eq.32) qcdl=0.231d0
      if (istruc.eq.33) qcdl=0.200d0
      if (istruc.eq.34) qcdl=0.177d0
      if (istruc.eq.35) qcdl=0.239d0
      if (istruc.eq.36) qcdl=0.366d0
      if (istruc.eq.37) qcdl=0.542d0
      if (istruc.eq.38) qcdl=0.286d0
      if (istruc.eq.39) qcdl=0.241d0
      if (istruc.eq.40) qcdl=0.344d0
      if (istruc.eq.41) qcdl=0.241d0
      if (istruc.eq.42) qcdl=0.344d0
      if (istruc.eq.43) qcdl=0.300d0
      if (istruc.eq.44) qcdl=0.300d0
      if (istruc.eq.45) qcdl=0.300d0
      if (istruc.eq.46) qcdl=0.300d0
      if (istruc.eq.100) qcdl=0.160d0
      if (istruc.eq.101) qcdl=0.216d0
      if (istruc.eq.102) qcdl=0.284d0
      if (istruc.eq.103) qcdl=0.366d0
      if (istruc.eq.104) qcdl=0.458d0
      if (istruc.eq.105) qcdl=0.564d0
      if (istruc.eq.111) qcdl=0.150d0
      if (istruc.eq.112) qcdl=0.200d0
      if (istruc.eq.113) qcdl=0.250d0
      if (istruc.eq.114) qcdl=0.300d0
      if (istruc.eq.115) qcdl=0.350d0
      if (istruc.eq.116) qcdl=0.400d0
      if (istruc.eq.121) qcdl=0.100d0
      if (istruc.eq.122) qcdl=0.120d0
      if (istruc.eq.123) qcdl=0.140d0
      if (istruc.eq.124) qcdl=0.180d0
      if (istruc.eq.125) qcdl=0.200d0
      if (istruc.eq.126) qcdl=0.220d0
      if (istruc.eq.127) qcdl=0.260d0
      if (istruc.eq.128) qcdl=0.300d0
      if (istruc.eq.129) qcdl=0.340d0
      if (istruc.eq.130) qcdl=0.215d0
      if (istruc.eq.131) qcdl=0.255d0
      if (istruc.eq.132) qcdl=0.300d0
      if (istruc.eq.133) qcdl=0.348d0
      if (istruc.eq.134) qcdl=0.401d0
*
      iorder=nloop
*
* setup variables used in monte carlo
      call setup
*
* output settings of monte carlo
*
      write(*,*)
      write(*,*) '*************************************************'
      write(*,*) '                                  r 4.1 d 17/7/97'
      spp1=' '
      spp2=' '
      if (ipp1.eq.0) spp1='p'
      if (ipp1.eq.1) spp1='pbar'
      if (ipp2.eq.0) spp2='p'
      if (ipp2.eq.1) spp2='pbar'
      if (exclusive) then
        sex ='exclusive'
      else
        sex ='inclusive'
      endif
      if ((spp1.eq.' ').or.(spp2.eq.' ')) then
        write(*,*) '*** input error, ipp1, ipp2 =',ipp1,ipp2
	goto 999
      endif
      svec=' '
      if (ivec.eq.0) svec='W'
      if (ivec.eq.1) svec='W-'
      if (ivec.eq.2) svec='W+'
      if (ivec.eq.3) svec='Z'
      if (svec.eq.' ') then
        write(*,*) '*** input error, ivec =',ivec
        goto 999
      endif
      if (njets+nloop.gt.2) then
        write(*,*) '*** input error, njets+nloop =',njets+nloop
        goto 999
      endif
      write(*,11) spp1,spp2,svec,njets,sex,(njets+nloop),w
   11 format(/,1x,a4,a4,'--> ',a2,' + ',i2,' jets ',a9,
     .      ' at O(alphas**',i1,') at ',f8.0,' GeV ')
      write(*,12) delrjj,rsep,etminj,etmaxj,rapminj,rapmaxj
   12 format(/,' jet defining cuts ',/,/,
     .         ' delta Rjj = ',f4.2,',     Rsep = ',f4.2,/,/,
     .        '    ',f7.2,' GeV <   ETjet  < ',f7.2,' GeV ',/,/,
     .        '       ',f4.2,'     < |etajet| <    ',f4.2)
      sjet=' '
      if(jalg1.eq.1)sjet=' deltaR(i,j)   < delrjj  '
      if(jalg1.eq.2)sjet=' deltaR(i,jet) < delrjj  '
      if(jalg1.eq.3)sjet=' kt algorithm; R = delrjj'
      if(jalg1.eq.4)sjet=' deltaR(i,jet) < delrjj , deltaR(i,j) < rsep'
      write(*,13) sjet
   13 format(/,a47,' clustering')
      if(jalg2.eq.1)sjet=' D0 eta/phi'
      if(jalg2.eq.2)sjet=' Snowmass  '
      if(jalg2.eq.3)sjet=' 4 momentum'
      if(jalg2.eq.4)sjet=' 4 momentum'
      write(*,14) sjet
   14 format(/,a12,' recombination')
      if (ivec.eq.3) then
        write(*,15) etminl,delrjl,rapmaxl
      else
        write(*,16) etminl,etmis,delrjl,rapmaxl
      endif
   15 format(/,' lepton cuts [et,r,eta] : ['
     .       ,f5.2,',',f5.2,',',f5.2,']')
   16 format(/,' lepton cuts [et,etmis,r,eta] : ['
     .       ,f5.2,',',f5.2,',',f5.2,',',f5.2,']')
      write(*,17) raphad
   17 format(/,' hadron rapidity coverage : ',f5.2)
      if (ireson.eq.0) sreson='breit wigner'
      if (ireson.eq.1) sreson='narrow width'
      if (sreson.eq.' ') then
        write(*,*) '*** input error, ireson =',ireson
        goto 999
      endif
      sstru=' '
      if (istruc.eq.1)  sstru='mrse    (msbar)'
      if (istruc.eq.2)  sstru='mrsb    (msbar)'
      if (istruc.eq.3)  sstru='kmrshb  (msbar)'
      if (istruc.eq.4)  sstru='kmrsb0  (msbar)'
      if (istruc.eq.5)  sstru='kmrsb-  (msbar)'
      if (istruc.eq.6)  sstru='kmrsb-5 (msbar)'
      if (istruc.eq.7)  sstru='kmrsb-2 (msbar)'
      if (istruc.eq.8)  sstru='mts1    (msbar)'
      if (istruc.eq.9)  sstru='mte1    (msbar)'
      if (istruc.eq.10) sstru='mtb1    (msbar)'
      if (istruc.eq.11) sstru='mtb2    (msbar)'
      if (istruc.eq.12) sstru='mtsn1   (msbar)'
      if (istruc.eq.13) sstru='kmrss0  (msbar)'
      if (istruc.eq.14) sstru='kmrsd0  (msbar)'
      if (istruc.eq.15) sstru='kmrsd-  (msbar)'
      if (istruc.eq.16) sstru='mrss0   (msbar)'
      if (istruc.eq.17) sstru='mrsd0   (msbar)'
      if (istruc.eq.18) sstru='mrsd-   (msbar)'
      if (istruc.eq.19) sstru='cteq1l  (msbar)'
      if (istruc.eq.20) sstru='cteq1m  (msbar)'
      if (istruc.eq.21) sstru='cteq1ml (msbar)'
      if (istruc.eq.22) sstru='cteq1ms (msbar)'
      if (istruc.eq.23) sstru='grv     (msbar)'
      if (istruc.eq.24) sstru='cteq2l  (msbar)'
      if (istruc.eq.25) sstru='cteq2m  (msbar)'
      if (istruc.eq.26) sstru='cteq2ml (msbar)'
      if (istruc.eq.27) sstru='cteq2ms (msbar)'
      if (istruc.eq.28) sstru='cteq2mf (msbar)'
      if (istruc.eq.29) sstru='mrsh    (msbar)'
      if (istruc.eq.30) sstru='mrsa    (msbar)'
      if (istruc.eq.31) sstru='mrsg    (msbar)'
      if (istruc.eq.32) sstru='mrsap   (msbar)'
      if (istruc.eq.33) sstru='grv94   (msbar)'
      if (istruc.eq.34) sstru='cteq3l  (msbar)'
      if (istruc.eq.35) sstru='cteq3m  (msbar)'
      if (istruc.eq.36) sstru='mrsj    (msbar)'
      if (istruc.eq.37) sstru='mrsjp   (msbar)'
      if (istruc.eq.38) sstru='cteqjet (msbar)'
      if (istruc.eq.39) sstru='mrsr1   (msbar)'
      if (istruc.eq.40) sstru='mrsr2   (msbar)'
      if (istruc.eq.41) sstru='mrsr3   (msbar)'
      if (istruc.eq.42) sstru='mrsr4   (msbar)'
      if (istruc.eq.43) sstru='cteq4l  (msbar)'
      if (istruc.eq.44) sstru='cteq4m  (msbar)'
      if (istruc.eq.45) sstru='cteq4hj (msbar)'
      if (istruc.eq.46) sstru='cteq4lq (msbar)'
      if (istruc.eq.100) sstru='mrsap  (0.105)'
      if (istruc.eq.101) sstru='mrsap  (0.110)'
      if (istruc.eq.102) sstru='mrsap  (0.115)'
      if (istruc.eq.103) sstru='mrsap  (0.120)'
      if (istruc.eq.104) sstru='mrsap  (0.125)'
      if (istruc.eq.105) sstru='mrsap  (0.130)'
      if (istruc.eq.111) sstru='grv94  (msbar)'
      if (istruc.eq.112) sstru='grv94  (msbar)'
      if (istruc.eq.113) sstru='grv94  (msbar)'
      if (istruc.eq.114) sstru='grv94  (msbar)'
      if (istruc.eq.115) sstru='grv94  (msbar)'
      if (istruc.eq.116) sstru='grv94  (msbar)'
      if (istruc.eq.121) sstru='cteq3m (msbar)'
      if (istruc.eq.122) sstru='cteq3m (msbar)'
      if (istruc.eq.123) sstru='cteq3m (msbar)'
      if (istruc.eq.124) sstru='cteq3m (msbar)'
      if (istruc.eq.125) sstru='cteq3m (msbar)'
      if (istruc.eq.126) sstru='cteq3m (msbar)'
      if (istruc.eq.127) sstru='cteq3m (msbar)'
      if (istruc.eq.128) sstru='cteq3m (msbar)'
      if (istruc.eq.129) sstru='cteq3m (msbar)'
      if (istruc.eq.130) sstru='cteq4m (msbar)'
      if (istruc.eq.131) sstru='cteq4m (msbar)'
      if (istruc.eq.132) sstru='cteq4m (msbar)'
      if (istruc.eq.133) sstru='cteq4m (msbar)'
      if (istruc.eq.134) sstru='cteq4m (msbar)'
      if (sstru.eq.' ') then
        write(*,*) '*** input error, istruc =',istruc
        goto 999
      endif
      write(*,18) sstru
   18 format(/,' structure function : ',a32)
      write(*,19) smin
   19 format(/,' parton resolution cut : ',g12.5)
      write(*,20) safety,s0 

   20 format(/,' safety cut = smin / ',f8.2,' = ',g12.5,' GeV^2')
      if (ivec.eq.3) then
        write(*,21) svec,rmz,rgz,sreson
      else
        write(*,21) svec,rmw,rgw,sreson
      endif
   21 format(/,1x,a1,' mass = ',f5.2,' and width = ',f5.2
     .     ,'  ( ',a13,')')
      sir=' '
      if (irenorm.eq.1) sir='total invariant mass'
      if (irenorm.eq.2) sir='vector boson mass (dynamical)'
      if (irenorm.eq.3) sir='vector boson mass (on-shell )'
      if (irenorm.eq.4) sir='sqrt(M(V)^2+P_T(V)^2) '
      if (irenorm.eq.5) sir='E_T of leading jet '
      if (irenorm.eq.6) sir='P_T(V) '
      if (sir.eq.' ') then
        write(*,*) '*** input error, irenorm =',irenorm
	goto 999
      endif
      write(*,22) crenorm,sir
   22 format(/,' renormalization scale is ',f5.2,' * ',a32)
      sir=' '
      if (ifact.eq.1) sir='total invariant mass'
      if (ifact.eq.2) sir='vector boson mass (dynamical)'
      if (ifact.eq.3) sir='vector boson mass (on-shell )'
      if (ifact.eq.4) sir='sqrt(M(V)^2+P_t(V)^2) '
      if (ifact.eq.5) sir='E_T of leading jet '
      if (ifact.eq.6) sir='P_T(V) '
      if (sir.eq.' ') then
        write(*,*) '*** input error, ifact =',ifact
	goto 999
      endif
      write(*,23) cfact,sir
   23 format(/,' factorization scale is   ',f5.2,' * ',a32)
      if(iorder.eq.0)then
        as=alfas(rmz,qcdl,1)
        sstru='one loop'
      elseif(iorder.eq.1)then
        as=alfas(rmz,qcdl,2)
        sstru=' two loop'
      endif
      write(*,24)nf,qcdl,sstru,as
   24 format(/,' active quark flavours              =   ',i6,/,/,
     .         ' QCD lambda for 4 flavours          =   ',f6.3,/,/,
     .         a9,' alphas(m_z)               = ',f8.5)
      write(*,25) itmax2,nshot2,nshot4 
   25 format(/,' number of events : ',i3,
     .         '*(',i8,' + ',i8,' )')

      write(*,26) sw2 
   26 format(/,' sin^2  = ',f5.2)
      write(*,*)
      write(*,*) '=================================================',
     . '=================' 
      write(*,*) 
*
* calculate cross section
*
      call cross(sig,sd)
*
* output results and distributions
*
      write(*,30) svec,njets,sig,sd
   30 format(/,'  sigma(',a2,' + ',i2,' jets) = ',g12.5,'  +/-',g12.5,
     . ' nb ',/)
      call bino(4,0d0,0)
*
  999 continue
      write(*,*)
      write(*,*) '*************************************************',
     . '*****************' 
      write(*,*)
*
 1000 continue
      end
*
************************************************************************ 
*
      subroutine lepcuts(ipass,jets,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /lepcut/ etminl,delrjl,rapmaxl,etmis,rlepmin,rlepmax,raphad
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     .                ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /jetmom/ pjet(8,10),jp(10)
      common /parmom/ ppar(4,10)
      dimension ret(4)
*
* lepton cuts
*
      ipass=0
      etal1=0.5d0*log((pjet(3,3)+pjet(4,3))/(pjet(4,3)-pjet(3,3)))
      etal2=0d0
      if (ivec.ge.3)
     .etal2=0.5d0*log((pjet(3,4)+pjet(4,4))/(pjet(4,4)-pjet(3,4)))
      if (max(abs(etal1),abs(etal2)).ge.rapmaxl) return
      et1=sqrt(pjet(1,3)**2+pjet(2,3)**2)
      if (et1.lt.etminl) return
      et2=sqrt(pjet(1,4)**2+pjet(2,4)**2)
      if (ivec.ge.3) then
         if (et2.lt.etminl) return
      else
         ret(1)=-pjet(1,3)
         ret(2)=-pjet(2,3)
         ret(3)=0d0
         ret(4)=0d0
         do i=1,npar
            j=i+4
            rap=0.5d0*log((ppar(3,j)+ppar(4,j))/(ppar(4,j)-ppar(3,j)))
            if (abs(rap).le.raphad) then
               ret(1)=ret(1)-ppar(1,j)
               ret(2)=ret(2)-ppar(2,j)
            endif
         enddo
         et2=sqrt(ret(1)**2+ret(2)**2)
         if (et2.lt.etmis) return
         pjet(1,4)=ret(1)
         pjet(2,4)=ret(2)
         pjet(3,4)=ret(3)
         pjet(4,4)=ret(4)
      endif
*
* jet-lepton cuts
*
      if(jets.gt.0)then 
      do i=1,jets
         if (r(jp(i),3).lt.delrjl) return
         if ((ivec.ge.3).and.(r(jp(i),4).lt.delrjl)) return
      enddo
      endif
*
      ipass=1
*
      end
*
************************************************************************ 
*
      subroutine clust(jets,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /jetcom/ icol,ji,jj,jk
      common /parmom/ ppar(4,10)
      common /jetmom/ pjet(8,10),jp(10)
      common /invar/ sab,ya1,yb1,ya2,yb2,y12
      common /phypar/ w,ipp1,ipp2,rmw,rgw,rmz,rgz,sw2,qcdl
      data init/0/
      if(init.eq.0) then
        init=1
        write(*,*)' jet clustering as of 12/8/95 '
      endif	
*
* pjet(5,j) = ET
* pjet(6,j) = pseudorapidity
* pjet(7,j) = azimuthal angle
* pjet(8,j) = 0 (possible mass entry)
*
      do i=1,4+npar
         do j=1,4
            pjet(j,i)=ppar(j,i)
         enddo
      enddo
      if (npar.eq.0) then
         jets=0 
         return
      endif
      icol=0
      ji=-1
      jj=-1
      dij=w**2
      dib=w**2
      do i=1,npar
         jp(i)=i+4
         j=jp(i)
         pjet(5,j)=sqrt(pjet(1,j)**2+pjet(2,j)**2)
         theta=atan2(pjet(5,j),pjet(3,j))
         pjet(6,j)=-log(abs(tan(theta/2.0)))
         pjet(7,j)=atan2(pjet(1,j),pjet(2,j))
      enddo
* cluster the partons
      if(npar.gt.1)then
      do  i=1,npar-1
        do j=i+1,npar
          j1=jp(i)
          j2=jp(j)
          if ((pjet(4,j1).gt.0.d0).and.(pjet(4,j2).gt.0.d0)) then
*
* clustering criterion
*       jalg1 = 1 ; deltaR(i,j)   < delrjj
*       jalg1 = 2 ; deltaR(i,jet) < delrjj and deltaR(j,jet) < delrjj
*       jalg1 = 3 ; kt algorithm; R = delrjj
*       jalg1 = 4 ; deltaR(i,jet) < delrjj and deltaR(j,jet) < delrjj
*                      but deltaR(i,j) < Rsep
*
               if (jalg1.eq.1) then
                  dely=pjet(6,j1)-pjet(6,j2)
                  rar=(pjet(1,j1)*pjet(1,j2)+pjet(2,j1)*pjet(2,j2))
     .                 /pjet(5,j1)/pjet(5,j2) 
                  if (rar.lt.-1d0) then 
                      delfi=pi
                  elseif (rar.gt.1d0) then 
                      delfi=0d0
                  else
                      delfi=acos(rar)
                  endif
                  delr=sqrt(dely**2+delfi**2)
                  if (delr.lt.delrjj) then
                     icol=1
                     ji=j1
                     jj=j2
                  endif
               endif
               if (jalg1.eq.2.or.jalg1.eq.4) then
*
                 if(jalg1.eq.4)then
                   dely=pjet(6,j1)-pjet(6,j2)
                   rar=(pjet(1,j1)*pjet(1,j2)+pjet(2,j1)*pjet(2,j2))
     .                 /pjet(5,j1)/pjet(5,j2) 
                   if (rar.lt.-1d0) then 
                      delfi=pi
                   elseif (rar.gt.1d0) then 
                      delfi=0d0
                   else
                      delfi=acos(rar)
                   endif
                   delr=sqrt(dely**2+delfi**2)
                 endif
                 if(jalg2.eq.1.or.jalg2.eq.3.or.jalg2.eq.4)then
                   pt1=pjet(5,j1)
                   pt2=pjet(5,j2)
                   px=pjet(1,j1)+pjet(1,j2)
                   py=pjet(2,j1)+pjet(2,j2)
                   pz=pjet(3,j1)+pjet(3,j2)
                   ee=pjet(4,j1)+pjet(4,j2)
                   pt=sqrt(px**2+py**2)
                   theta=atan2(pt,pz)
                   if(jalg2.eq.1)then
                     etjet=pt1+pt2
                   elseif(jalg2.eq.3)then 
                     etjet=pt
                   elseif(jalg2.eq.4)then 
                     etjet=ee*sin(theta)
                   endif
                   etajet=-log(abs(tan(theta/2.0)))
                   phijet=atan2(px,py)
                   rar=(pjet(1,j1)*px+pjet(2,j1)*py)
     .                 /pt1/pt
                   if (rar.lt.-1d0) then 
                      delfi1=pi
                   elseif (rar.gt.1d0) then 
                      delfi1=0d0
                   else
                      delfi1=acos(rar)
                   endif
                   rar=(pjet(1,j2)*px+pjet(2,j2)*py)
     .                 /pt2/pt
                   if (rar.lt.-1d0) then 
                      delfi2=pi
                   elseif (rar.gt.1d0) then 
                      delfi2=0d0
                   else
                      delfi2=acos(rar)
                   endif
                 endif
                 if(jalg2.eq.2)then
                   etjet=pjet(5,j1)+pjet(5,j2)
                   etajet=(pjet(6,j1)*pjet(5,j1)
     .                    +pjet(6,j2)*pjet(5,j2))/etjet
                   phijet=(pjet(7,j1)*pjet(5,j1)
     .                    +pjet(7,j2)*pjet(5,j2))/etjet
                   rar=(pjet(1,j1)*pjet(1,j2)+pjet(2,j1)*pjet(2,j2))
     .                 /pjet(5,j1)/pjet(5,j2) 
                   if (rar.lt.-1d0) then 
                      delfi=pi
                   elseif (rar.gt.1d0) then 
                      delfi=0d0
                   else
                      delfi=acos(rar)
                   endif
                   delfi1= pjet(5,j2)*delfi/etjet ! phi_1 - phijet 
                   delfi2=-pjet(5,j1)*delfi/etjet ! phi_2 - phijet
                 endif
                 dely1=pjet(6,j1)-etajet
                 dely2=pjet(6,j2)-etajet
                 delr1=sqrt(dely1**2+delfi1**2)
                 delr2=sqrt(dely2**2+delfi2**2)
                 if ((delr1.lt.delrjj).and.(delr2.lt.delrjj)) then
                   if(jalg1.eq.2)then
                     icol=1
                     ji=j1
                     jj=j2
                   elseif((jalg1.eq.4).and.(delr.lt.rsep)) then
                     icol=1
                     ji=j1
                     jj=j2
                   endif
                 endif
               endif
               if (jalg1.eq.3) then
                  dely=pjet(6,j1)-pjet(6,j2)
                  rar=(pjet(1,j1)*pjet(1,j2)+pjet(2,j1)*pjet(2,j2))
     .                /pjet(5,j1)/pjet(5,j2) 
                  if (rar.lt.-1d0) then 
                     delfi=pi
                  elseif (rar.gt.1d0) then 
                     delfi=0d0
                  else
                     delfi=acos(rar)
                  endif
                  delr=sqrt(dely**2+delfi**2)
                  et=dmin1(pjet(5,j1),pjet(5,j2))
                  if(et**2*delr**2.lt.dij)then
                    ji=j1
                    jj=j2
                    dij=et**2*delr**2
                  endif
                  if(et**2*delrjj**2.lt.dib)dib=et**2*delrjj**2
               endif
            endif
         enddo
      enddo
      if(jalg1.eq.3)then
         if(dij.lt.dib)then
             icol=1
          endif
      endif
      endif
*
*
      if(icol.eq.1)then
        jk=ji
*    pjet(.,jk) is made of ppar(.,ji)+ppar(.,jj)
        if(jalg2.eq.1.or.jalg2.eq.3.or.jalg2.eq.4)then
             do  k=1,4
               pjet(k,ji)=pjet(k,ji)+pjet(k,jj)
             enddo
             pp=sqrt(pjet(1,ji)**2+pjet(2,ji)**2)
             theta=atan2(pp,pjet(3,ji))
             if(jalg2.eq.1)then
               pjet(5,ji)=pjet(5,ji)+pjet(5,jj)
               pjet(4,ji)=pjet(5,ji)/sin(theta)
             elseif(jalg2.eq.3)then
               pjet(5,ji)=sqrt(pjet(1,ji)**2+pjet(2,ji)**2)
             elseif(jalg2.eq.4)then
               pjet(5,ji)=pjet(4,ji)*sin(theta)
             endif
             pjet(6,ji)=-log(abs(tan(theta/2.0)))
             pjet(7,ji)=atan2(pjet(1,ji),pjet(2,ji))
             pjet(4,jj)=-1d0
        endif
        if(jalg2.eq.2)then
             etjet=pjet(5,ji)+pjet(5,jj)
             etajet=
     .          (pjet(6,ji)*pjet(5,ji)+pjet(6,jj)*pjet(5,jj))/etjet
             phijet=
     .          (pjet(7,ji)*pjet(5,ji)+pjet(7,jj)*pjet(5,jj))/etjet
             eejet=exp(etajet)
             rar=(pjet(1,ji)*pjet(1,jj)+pjet(2,ji)*pjet(2,jj))
     .           /pjet(5,ji)/pjet(5,jj) 
             if (rar.lt.-1d0) then 
                  delfi=pi
             elseif (rar.gt.1d0) then 
                  delfi=0d0
             else
                  delfi=acos(rar)
             endif
             delfi1= pjet(5,jj)*delfi/etjet ! phi_1 - phijet 
             cde=cos(delfi1)
             sde=sin(delfi1)
             if (pjet(2,ji)*pjet(1,jj).gt.pjet(2,jj)*pjet(1,ji))
     .       sde=-sde
             cphi=(pjet(1,ji)*cde-pjet(2,ji)*sde)/pjet(5,ji)
             sphi=(pjet(2,ji)*cde+pjet(1,ji)*sde)/pjet(5,ji)
             pjet(1,ji)=etjet*cphi
             pjet(2,ji)=etjet*sphi
             pjet(3,ji)=etjet/2d0*(eejet-1d0/eejet)
             pjet(4,ji)=etjet/2d0*(eejet+1d0/eejet)
             pjet(5,ji)=etjet
             pjet(6,ji)=etajet
             pjet(7,ji)=phijet
             pjet(4,jj)=-1d0
        endif
      endif
*
* sort jets according to decreasing transverse energy
* j1=most energetic,...,j4=least energetic inside rapidity region
* then sort non-jets according to Et outside rap region
*
      do i=1,npar-1
         do j=i+1,npar
           j1=jp(i)
           j2=jp(j)
	   if(pjet(4,j1).lt.0d0)pjet(5,j1)=0d0
	   if(pjet(4,j2).lt.0d0)pjet(5,j2)=0d0
           if (pjet(5,j1).lt.pjet(5,j2)) then
               jt   =jp(i)
               jp(i)=jp(j)
               jp(j)=jt
           endif
         enddo
      enddo
* count number of observed jets 
*      etminj  <  et < etmaxj
*      rapminj < eta < rapmaxj
      jets=0
      do i=1,npar
         j=jp(i)
         if (pjet(4,j).gt.0d0) then
           if (pjet(5,j).ge.etminj
     .        .and.pjet(5,j).le.etmaxj
     .        .and.abs(pjet(6,j)).le.rapmaxj
     .        .and.abs(pjet(6,j)).ge.rapminj) jets=jets+1
         endif
      enddo
*
      end
*
************************************************************************ 

*
      double precision function r(i,j)
      implicit real*8(a-h,o-z)
      common /jetmom/ pjet(8,10),jp(10)
*
      vs1=sqrt(pjet(1,i)**2+pjet(2,i)**2+pjet(3,i)**2)
      h1=acos(pjet(3,i)/vs1)
      vs2=sqrt(pjet(1,j)**2+pjet(2,j)**2+pjet(3,j)**2)
      h2=acos(pjet(3,j)/vs2)
      dely=-log(abs(tan(h1/2d0)/tan(h2/2d0)))
      delfi=acos((pjet(1,i)*pjet(1,j)+pjet(2,i)*pjet(2,j))
     . /sqrt((pjet(1,i)**2+pjet(2,i)**2)*(pjet(1,j)**2+pjet(2,j)**2)))
      r=sqrt(dely**2+delfi**2)
*
      end
*
************************************************************************ 

*
      subroutine bino(istat,wgt,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     .                ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /jetmom/ pjet(8,10),jp(10)
      common /parmom/ ppar(4,10)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /lepcut/ etminl,delrjl,rapmaxl,etmis,rlepmin,rlepmax,raphad
*
* jets are Et-ordered, jp(1) points to highest Et, jp(2) second highest,...
* pjet(4,jp(i)) is energy, if pjet(4,jp(i))<0 it is not a legitimate cluster
* Note: clusters not passing jet selection cuts, are retained and have
*       pjet(4,jp(i) > 0. So eg if one selects 0 jet exclusive all clusters
*       with pjet(4,jp(i))>=0 will have et<etminj and/or rap>rapmaxj. 
* pjet(5,jp(i)) is Et of jet jp(i) 
* pjet(6,jp(i)) is pseudo-rapidity of jet jp(i) 
*
* init histograms
*
      nhis=10
*
      if (istat.eq.0) then
         call histoi(1,0d0,200d0,40)
         call histoi(2,0d0,200d0,40)
         call histoi(3,0d0,200d0,40)
         call histoi(4,0d0,200d0,40)
         call histoi(5,0d0,200d0,40)
         call histoi(6,0d0,200d0,40)
         call histoi(7,0d0,200d0,40)
      endif
*
* write event into histogram
*
      if (istat.eq.1) then
        ethi=0d0
        etlo=0d0
        do i=1,npar
          j=jp(i)
          if(pjet(4,j).gt.0d0)then
            et=pjet(5,j)
            etaj=pjet(6,j)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .   (abs(etaj).gt.rapminj).and.(abs(etaj).lt.rapmaxj))then
            if(et.gt.etlo.and.et.lt.ethi)then
              etlo=et
            elseif(et.gt.ethi)then
              etlo=ethi
              ethi=et
            endif
            endif
          endif
        enddo
        call histoa(1,ethi,wgt)
        call histoa(2,etlo,wgt)
        call histoa(3,ethi,wgt)
        call histoa(3,etlo,-wgt)
        ptv=sqrt((ppar(1,3)+ppar(1,4))**2+(ppar(2,3)+ppar(2,4))**2)
        call histoa(7,ptv,wgt)
        do i=0,39
        ettest=i*5d0
        etty=ettest+2.5d0
        if(ethi.gt.ettest)then
          call histoa(4,etty,wgt*5d0)
        endif
        if(ethi.gt.ettest.and.etlo.lt.ettest)then
          call histoa(5,etty,wgt*5d0)
        endif
        if(ptv.gt.ettest)then
          call histoa(6,etty,wgt*5d0)
        endif
        enddo
      endif
*
* output distributions
*
      if (istat.eq.4) then
         write(*,*)
         write(*,*) 'first jet Et distribution'
         write(*,*)
         call histow(1)
         write(*,*)
         write(*,*) 'second jet Et distribution'
         write(*,*)
         call histow(2)
         write(*,*)
         write(*,*) 'exclusive jet Et distribution'
         write(*,*)
         call histow(3)
         write(*,*)
         write(*,*) 'inclusive'
         write(*,*)
         call histows(4)
         write(*,*)
         write(*,*) 'exclusive'
         write(*,*)
         call histows(5)
         write(*,*)
         write(*,*) 'Rw'
         write(*,*)
         call histows(6)
         write(*,*)
         write(*,*) 'p_T distribution of W'
         write(*,*)
         call histow(7)
      endif
*
*
*
* event errors manipulation request, pipe through
*
      if ((istat.eq.2).or.(istat.eq.3)) then
         do i=1,nhis
            call histoe(istat,i)
         enddo
      endif
*
      return
      end
*
************************************************************************ 
*
      subroutine check(ipass,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /jetmom/ pjet(8,10),jp(10)
      common /parmom/ ppar(4,10)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /lepcut/ etminl,delrjl,rapmaxl,etmis,rlepmin,rlepmax,raphad
      ipass=0
*
* jets are Et-ordered, jp(1) points to highest Et, jp(2) second highest,...
* pjet(4,jp(i)) is energy, if pjet(4,jp(i))<0 it is not a legitimate cluster
* Note: clusters not passing jet selection cuts, are retained and have
*       pjet(4,jp(i) > 0. So eg if one selects 0 jet exclusive all clusters
*       with pjet(4,jp(i))>=0 will have et<etminj and/or rap>rapmaxj. 
* pjet(5,jp(i)) is Et of jet jp(i) 
* pjet(6,jp(i)) is pseudo-rapidity of jet jp(i) 
*
        ethi=0d0
        etlo=0d0
        do i=1,npar
          j=jp(i)
          if(pjet(4,j).gt.0d0)then
            et=pjet(5,j)
            etaj=pjet(6,j)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .   (abs(etaj).gt.rapminj).and.(abs(etaj).lt.rapmaxj))then
            if(et.gt.etlo.and.et.lt.ethi)then
              etlo=et
            elseif(et.gt.ethi)then
              etlo=ethi
              ethi=et
            endif
            endif
          endif
        enddo
        ptv=sqrt((ppar(1,3)+ppar(1,4))**2+(ppar(2,3)+ppar(2,4))**2)
*
* here, we try to keep all events with a hard enough jet or V 
*
        if(ethi.gt.20d0.or.ptv.gt.20d0)ipass=1
*
* if you look here, you should ask EWNG
*
        ipass=1
        if(njets.eq.0)ipass=1
*
      return
      end
*
************************************************************************ 

*
      subroutine distrib(wtdis,npar)
      implicit real*8(a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /jetmom/ pjet(8,10),jp(10)
      common /parmom/ ppar(4,10)
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
*
        ptv=sqrt((ppar(1,3)+ppar(1,4))**2+(ppar(2,3)+ppar(2,4))**2)
*
* transverse pt distribution - tuned for D0 W
*
* if you look here, you should ask EWNG
*
      a=-7.36d0
      b=1.36d0
      c=2.15d0
      wtdis=exp(a)*exp(-b*ptv/rm)/(ptv/rm)**c
      wtdis=1d0
      if(njets.eq.0)wtdis=1d0
      return
      end
*
************************************************************************ 
*                                                                       
*
* compute cross section (sig) with standard deviation (sd) from         
*
* itmx iterations                                                       
*
*                                                                       
*
************************************************************************ 

      subroutine cross(sig,sd)
      implicit double precision (a-h,o-z)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /normal/ exclusive
      COMMON /BVEGA/NDIMA,NCALLA,NPRNA
      COMMON /BVEGB/NDIMB,NCALLB,NPRNB
      common /plots/plot
      external sigV,sigR
      logical exclusive,plot 
*
*
*
      nprna=1
      nprnb=1
*
* warm up 
*
*
* tree level or virtual cross section computed using vegasa
* bremstrahlung cross section using vegasb
*
      if(njets.eq.0)then
        ndima=1
        ndimb=3
      elseif(njets.eq.1)then
        ndima=3
        ndimb=6
      endif
      if(njets.eq.2.and.nloop.eq.0)ndima=6
      ncalla=nshot1
      ncallb=nshot3
      plot=.false.
*
* initialize vegas
*
      call vegasa(1,sigV,avgiV,sdV,chi2aV)
      if(nloop.eq.1)call vegasb(1,sigR,avgiR,sdR,chi2aR)
*
* vegas sweeps
*
      do it=1,itmax1
        call vegasa(3,sigV,avgiV,sdV,chi2aV)
        if(nloop.eq.1)call vegasb(3,sigR,avgiR,sdR,chi2aR)
      enddo
*
* main run
*
      ncalla=nshot2
      ncallb=nshot4
      plot=.true.
      call bino(0,0d0,0)
*
* initialize vegas
*
      call vegasa(2,sigV,avgiV,sdV,chi2aV)
      if(nloop.eq.1)call vegasb(2,sigR,avgiR,sdR,chi2aR)
*
* vegas sweeps with frozen grid
*
      do it=1,itmax2
        call vegasa(4,sigV,avgiV,sdV,chi2aV)
        if(nloop.eq.1)call vegasb(4,sigR,avgiR,sdR,chi2aR)
        call bino(2,0d0,0)
      enddo
*
*
*
      call bino(3,0d0,0)
      if(nloop.eq.0)then
        sig=avgiV
        sd=sdV
      elseif(nloop.eq.1)then
        sig=avgiV+avgiR
        sd=sqrt(sdV**2+sdR**2)
      endif
      return
      end
*
************************************************************************ 

*
      function sigV(x,wgt)
      implicit double precision (a-h,o-z)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /normal/exclusive
      common /plots/plot
      logical exclusive,plot 

      dimension x(10)
*
      wtnano=389385.73d0	
      sigV=0d0
* 
*  # partons = njets (nloop=0 --> l.o. , nloop=1 --> l.o. + virtual graphs)
*
      npar=njets
      call phase(x,npar,1,wtps,ipass)
      if (ipass.eq.0) return
      call clust(jets,npar)
      if(exclusive)then
	if (jets.ne.njets) return
      else	 
        if (jets.lt.njets) return
      endif
*      call check(ipass,npar)
      call lepcuts(ipass,jets,npar)
      if (ipass.eq.0) return
      call scale(npar)
      call dy(npar,nloop,rint)
      wt=wtnano*wtps*rint   
      if(plot)then
        call bino(1,wt*wgt,npar)
      else
*        call distrib(wtdis,npar)
*        wt=wt/wtdis
      endif
      sigV=wt
      return
      end
*
************************************************************************ 

*
      function sigR(x,wgt)
      implicit double precision (a-h,o-z)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     .                ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /normal/exclusive
      common /plots/plot
      logical exclusive,plot 
      dimension x(10)
*
      wtnano=389385.73d0	
      sigR=0d0
* 
* # partons = njets + 1 (nloop=0 --> 0 , nloop=1 --> real graphs)
*
      if (nloop.eq.0) return
      npar=njets+1
      call phase(x,npar,0,wtps,ipass)
      if (ipass.eq.0) return
      call clust(jets,npar)
      if(exclusive)then
	if (jets.ne.njets) return
      else	 
        if (jets.lt.njets) return
      endif
*      call check(ipass,npar)
      call lepcuts(ipass,jets,npar)
      if (ipass.eq.0) return
      call scale(npar)
      call dy(npar,nloop-1,rint)
      wt=wtnano*wtps*rint   
      if(plot)then
        call bino(1,wt*wgt,npar)
      else
*        call distrib(wtdis,npar)
*        wt=wt/wtdis
      endif
      sigR=wt
      return
      end
*
************************************************************************ 
*
      subroutine setup
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /phypar/ w,ipp1,ipp2,rmw,rgw,rmz,rgz,sw2,qcdl
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /lepcut/ etminl,delrjl,rapmaxl,etmis,rlepmin,rlepmax,raphad
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,beam,
     .                breitmax,breitmin,breitwgt
      common /koppel/  nf,b0,cru,clu,crd,cld,crl,cll,
     .                      cru2,clu2,crd2,cld2,crl2,cll2
      common /parmom/ ppar(4,10)
      common /partonid/ iu,id,is,ic,ib,it,iub,idb,isb,icb,ibb,itb,ig,
     .                  iq(6),ip(6),il(6)
      common /structure/ f1(13),f2(13),cross1(13),cross2(13)
      common /sigma/ sigma0(13,13),sigma1(13,13)
      common /blocks/ rb(3,3),rf(3,3),rk(3,3)
      dimension itz(6),ipz(6),itw(6),itwp(6),itwm(6),ipw(6),ich(6)
*
* setting up parton id numbers, clearing structure & cross section arrays
*
      data iu,id,is,ic,ib,it,iub,idb,isb,icb,ibb,itb,ig 
     .    / 1, 2, 4, 3, 5, 6,  7,  8, 10,  9, 11, 12,13/
      data itz / 1,2,1,2,2,3 /
      data ipz / 7,8,9,10,11,12 /
      data itw / 1,1,1,1,3,3 /
      data itwp/ 1,3,1,3,3,3 /
      data itwm/ 3,1,3,1,3,3 /
      data ipw /8,7,10,9,12,11 /
      data ich /1,0,1,0,1,0/
*
      do 10 i=1,13
        f1(i)=0d0
        f2(i)=0d0
        cross1(i)=0d0
        cross2(i)=0d0
        do 10 j=1,13
             sigma0(j,i)=0d0
             sigma1(j,i)=0d0
 10     continue
*
* setup left and right-handed couplings of vector bosons
*
      sw=sqrt(sw2)
      cw=sqrt(1-sw2)
      csw=sw*cw
      qed=4d0*pi/127.8d0
      sqed=sqrt(qed)
      if (ivec.eq.3) then
*
* electron-z-coupling(=crl,cll) -> charge=-1(=qf), weak isospin=-1/2(=wis)
*
        qf=-1d0
        wis=-1d0/2d0
        cre=sqed*(-sw2*qf/csw)
        cle=sqed*(wis-sw2*qf)/csw
        cgre=-sqed*qf
        cgle=-sqed*qf
*
* neutrino-z-coupling(=crl,cll) -> charge=0(=qf), weak isospin=1/2(=wis)
*
        qf=0d0
        wis=1d0/2d0
        crn=sqed*(-sw2*qf/csw)
        cln=sqed*(wis-sw2*qf)/csw
        cgrn=-sqed*qf
        cgln=-sqed*qf
*
* up quark-z-coupling(=cru,clu) -> charge=2/3(=qf), weak isospin=1/2(=wis)
*
        qf=2d0/3d0
        wis=1d0/2d0
        cru=sqed*(-sw2*qf/csw)
        clu=sqed*((wis-sw2*qf)/csw)
        cgru=-sqed*qf
        cglu=-sqed*qf
*
* down quark-z-coupling(=crd,cld) -> charge=-1/3(=qf), weak isospin=-1/2(=wis)
*
        qf=-1d0/3d0
        wis=-1d0/2d0
        crd=sqed*(-sw2*qf/csw)
        cld=sqed*(wis-sw2*qf)/csw
        cgrd=-sqed*qf
        cgld=-sqed*qf
*
        nf=5
        do 2 i=1,6
           iq(i)=itz(i)
           ip(i)=ipz(i)
           il(i)=0d0
 2      continue
      endif
*
* fermion-w-coupling -> crl=cru=crd=0, cll=clu=cld=cl
*
      if (ivec.le.2) then
        cre=0d0
        crn=0d0
        cru=0d0
        crd=0d0
        cl=sqed/sqrt(2d0)/sw
        cle=cl
        cln=cl
        clu=cl
        cld=cl
*
        do 4 i=1,6
             ip(i)=ipw(i)
               il(i)=ich(i)
          if (ivec.eq.0) iq(i)=itw(i)
          if (ivec.eq.1) iq(i)=itwm(i)
          if (ivec.eq.2) iq(i)=itwp(i)
 4           continue
        nf=5
      endif
*
      crl=cre
      cll=cle
      crl2=crl**2
      cll2=cll**2
      cru2=cru**2
      clu2=clu**2
      crd2=crd**2
      cld2=cld**2

*
* setup phase space generator
*
      do 20 j=1,4
        do 20 i=1,10
	  ppar(j,i)=0d0
   20 continue
*	  
      if (ivec.eq.3) then
        rmv=rmz
	rgv=rgz
      else
        rmv=rmw
	rgv=rgw
      endif
* calculate b0
      b0=(33d0-2d0*nf)/12d0/pi
* setup breit wigner parameters
      rmv2=rmv**2
      rmg=rmv*rgv
      deltad=rlepmin
      deltau=rlepmax
      breitmax=atan((deltau**2-rmv2)/rmg)
      breitmin=atan((deltad**2-rmv2)/rmg)
      breitwgt=(breitmax-breitmin)/rmg/2d0/pi
      beam=w
*
      end
*
************************************************************************ 

*
      subroutine phase(x,npar,itype,wtps,ipass)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /phypar/ w,ipp1,ipp2,rmw,rgw,rmz,rgz,sw2,qcdl
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,beam,
     .                breitmax,breitmin,breitwgt
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     .                ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /parmom/ ppar(4,10)
      common /invar/ sab,ya1,yb1,ya2,yb2,y12
      common /order/ iorder
      dimension x(10)
*
* generate breit-wigner
*
      ipass=0
      if (ireson.eq.0) then
         r1=rn(1.)
         rm2=rmv2+rmg*tan((1-r1)*breitmin+r1*breitmax)
         rm=dsqrt(rm2)
         wtps=breitwgt
      else
         rm=rmv
         rm2=rmv2
         wtps=1d0/2d0/rmg
      endif
*
* generate parton invariant masses      

*
      if (itype.eq.0) call geninv (x,npar,wgthad,ifail)
      if (itype.eq.1) call genhard(x,npar,wgthad,ifail)
      if (ifail.eq.0) return
      wtps=wtps*wgthad
*
* generate explicit momenta in ppar(*,i)
*
      call genmom(npar,wgtgen)
      wtps=wtps*wgtgen
*
* add fluxfactor
*
      wtps=wtps/2d0/sab
*
      ipass=1
*
      end
*
************************************************************************ 

*
      subroutine geninv(x,npar,wgt,ifail)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 

      common /clusdef/ rsep,jalg1,jalg2
      common /thcut/ smin,s0
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,beam,
     .                breitmax,breitmin,breitwgt
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /parmom/ ppar(4,10)
      common /invar/ sab,ya1,yb1,ya2,yb2,y12
      dimension x(10)
*
      rm2=rm**2
      shad=beam**2
      wgt=1d0
      ifail=0
* 
* 0 parton generation
*
      if (npar.eq.0) then
         sab=rm2
         wgt=2d0*pi
* generate x1 & x2
         call fixx(x(1),sab,shad,x1,x2,wgt)
      endif
*
* 1 parton generation
*
      if (npar.eq.1) then
         smax=shad-rm2
         if (smin.gt.smax) return
         call inpinv(x(1),smin,smax,sa1,wgt)
         smax=smax-sa1
         if (smin.gt.smax) return
         call inpinv(x(2),smin,smax,sb1,wgt)
         sab=sa1+sb1+rm2
         ya1=sa1/sab
         yb1=sb1/sab
         wgt=wgt/8d0/pi/sab
* generate x1 & x2
         call fixx(x(3),sab,shad,x1,x2,wgt)
      endif
*
* 2 parton generation
*
      if (npar.eq.2) then
* generate x1 & x2
         rmscale=(rm+etminj)**2/shad
         call freex(x(1),x(2),sab,shad,rmscale,x1,x2,wgt)
         yab=1d0
         ymin=smin/sab
         ym2=rm**2/sab
*
         wgt=wgt*2d0*sab/49873.45461d0
*
         blow=ymin
         bup=yab-2d0*ymin-ym2
         if (blow.gt.bup) return
         call inpinv(x(3),blow,bup,y12,wgt)
*
         xx=yab*y12
*
         bup=bup-ymin+y12
         if (blow.gt.bup) return
         call inpinv(x(4),blow,bup,ya1,wgt)
*
         bup=bup+ymin-ya1
         if (blow.gt.bup) return
         call inpinv(x(5),blow,bup,yb2,wgt)
*
         yy=ya1*yb2
*
         sx=sqrt(xx)
         sy=sqrt(yy)
         c=yab-ym2+y12-ya1-yb2
         zmin=max(ymin**2,(sx-sy)**2)
         zmax=min(c**2/4d0,(sx+sy)**2)
         if (zmin.gt.zmax) return
         sqr=sqrt(abs(c**2-4d0*zmin))
         fmin=(c-sqr)/(c+sqr)
         sqr=sqrt(abs(c**2-4d0*zmax))
         fmax=(c-sqr)/(c+sqr)
         r=x(6)
         a=(fmin**r)*(fmax**(1d0-r))
         zz=a*(c/(1+a))**2
         sqr=sqrt(c**2-4d0*zz)
         if (rn(1.).ge.0.5d0) then
            ya2=(c-sqr)/2d0
            yb1=(c+sqr)/2d0
         else
            ya2=(c+sqr)/2d0
            yb1=(c-sqr)/2d0
         endif
         if ((ya2.lt.ymin).or.(yb1.lt.ymin)) return
         wgt=wgt*zz*log(fmax/fmin)/c
*
         delta=(xx**2+yy**2+zz**2-2d0*(xx*yy+xx*zz+yy*zz))/16d0
         wgt=wgt/sqrt(-delta)
         if (rn(1.).ge.0.5d0) then
            atm=ya1
            ya1=ya2
            ya2=atm
            atm=yb1
            yb1=yb2
            yb2=atm
         endif
      endif
*
      ifail=1
*
      end
*
************************************************************************ 

*
      subroutine fixx(r,sab,scale,x1,x2,wgt)
      implicit double precision (a-h,o-z)
*
      rmscale=sab/scale
      x1=rmscale**r
      x2=rmscale**(1-r)
      wgt=-wgt*log(rmscale)/sab
*
      end
*
************************************************************************ 

*
      subroutine freex(r1,r2,sab,shad,scale,x1,x2,wgt)
      implicit double precision (a-h,o-z)
*
      r=sqrt(r1)
      x1=scale**(1-r)
      x2=scale**(r*r2)
      wgt=wgt*log(scale)**2/2d0
      sab=x1*x2*shad
*
      end
*
************************************************************************ 

*
      subroutine inpinv(r,rmin,rmax,s,wgt)
      implicit double precision (a-h,o-z)
*
      s=(rmin**r)*(rmax**(1d0-r))
      wgt=wgt*log(rmax/rmin)*s
*
      end
*
************************************************************************ 
*
      subroutine genhard(x,npar,wgt,ifail)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /thcut/ smin,s0
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,beam,
     .                breitmax,breitmin,breitwgt
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /parmom/ ppar(4,10)
      common /invar/ sab,ya1,yb1,ya2,yb2,y12
      dimension x(10)
*
* 0 parton generation
*
      if (npar.eq.0) call geninv(x,npar,wgt,ifail)
*
* 1 parton generation (custom made hard)
*
      if (npar.eq.1) then
*
* generate x1 & x2
*
         ifail=1
         wgt=1d0
         rm2=rm**2
         shad=beam**2
         rmscale=(rm+etminj)**2/shad
         call freex(x(1),x(2),sab,shad,rmscale,x1,x2,wgt)
*
         r1=x(3)
         rap=rapmaxj*(2d0*r1-1)
         etp=(sab-rm2)/(beam*(x1*exp(-rap)+x2*exp(rap)))
         wgt=wgt*rapmaxj/(2d0*pi*sab*(sab-rm2))
         sa1=beam*x1*etp*exp(-rap)
         sb1=beam*x2*etp*exp(rap)
         wgt=wgt*sa1*sb1
         ya1=sa1/sab
         yb1=sb1/sab
      endif
*
* 2 parton generation (use geninv with minimum hard invariant mass)
* 
      if (npar.eq.2) then
         call geninv(x,npar,wgt,ifail)
      endif
*
      if (npar.ge.3) wgt=-1d0
*
      end
*
************************************************************************ 

*
      subroutine genmom(npar,wgt)
      implicit double precision (a-h,o-y)
      implicit complex*16 (z)
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,beam,
     .                breitmax,breitmin,breitwgt
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /parmom/ ppar(4,10)
      common /invar/ sab,ya1,yb1,ya2,yb2,y12
      common /invariant/ sba,siv(10,10)
      common /spinor/ zu(10,10),zd(10,10)
      dimension rv(4),cm(4)
      parameter(pi=3.141592653589793238d0)
*
* incoming partons, 1 <-> a, 2 < - > b.
*
      comp=beam*x1/2d0
      ppar(1,1)=0d0
      ppar(2,1)=0d0
      ppar(3,1)=comp
      ppar(4,1)=comp
      comp=beam*x2/2d0
      ppar(1,2)=0d0
      ppar(2,2)=0d0
      ppar(3,2)=-comp
      ppar(4,2)=comp
*
* generate outgoing partons (if any)
*
* 0 partons
*
      if (npar.eq.0) then
        rv(1)=ppar(1,1)+ppar(1,2)
        rv(2)=ppar(2,1)+ppar(2,2)
        rv(3)=ppar(3,1)+ppar(3,2)
        rv(4)=ppar(4,1)+ppar(4,2)
      endif
*
* 1 parton
*
      if (npar.eq.1) then
        xa1=sab*ya1/x1/beam
	xb1=sab*yb1/x2/beam
	xab=sqrt(xa1*xb1)
        r1=rn(1.)
        ph=2d0*pi*r1
	ppar(1,5)=xab*cos(ph)
	ppar(2,5)=xab*sin(ph)
	ppar(3,5)=(xb1-xa1)/2d0
	ppar(4,5)=(xa1+xb1)/2d0
        rv(1)=ppar(1,1)+ppar(1,2)-ppar(1,5)
        rv(2)=ppar(2,1)+ppar(2,2)-ppar(2,5)
        rv(3)=ppar(3,1)+ppar(3,2)-ppar(3,5)
        rv(4)=ppar(4,1)+ppar(4,2)-ppar(4,5)
      endif
*
* 2 partons
*
      if (npar.eq.2) then
         xa1=sab*ya1/x1/beam
         xa2=sab*ya2/x1/beam
         xb1=sab*yb1/x2/beam
         xb2=sab*yb2/x2/beam
         ppar(3,5)=(xb1-xa1)/2d0
         ppar(4,5)=(xb1+xa1)/2d0
         ppar(3,6)=(xb2-xa2)/2d0
         ppar(4,6)=(xb2+xa2)/2d0
         r1=rn(1.)
         ph=r1*2d0*pi
         cpi=cos(ph)
         spi=sin(ph)
         et1=sqrt(xa1*xb1)
         et2=sqrt(xa2*xb2)
         c12=(xa1*xb2+xb1*xa2-y12*sab)/2d0/et1/et2
         ppar(1,5)=cpi*et1
         ppar(2,5)=spi*et1
         ex2=et2*c12
         ey2=et2*sqrt(1d0-c12**2)
         ppar(1,6)=cpi*ex2-spi*ey2
         ppar(2,6)=spi*ex2+cpi*ey2
         rv(1)=ppar(1,1)+ppar(1,2)-ppar(1,5)-ppar(1,6)
         rv(2)=ppar(2,1)+ppar(2,2)-ppar(2,5)-ppar(2,6)
         rv(3)=ppar(3,1)+ppar(3,2)-ppar(3,5)-ppar(3,6)
         rv(4)=ppar(4,1)+ppar(4,2)-ppar(4,5)-ppar(4,6)
      endif
*
* generate lepton momenta in cm frame of vector boson
*
      r1=rn(1.)
      r1=2d0*pi*r1
      r2=rn(1.)
      r2=2d0*r2-1d0
      wgt=1d0/8d0/pi
      sp=sin(r1)
      cp=cos(r1)
      ct=r2
      st=sqrt(1d0-ct**2)
      ecm=rm/2d0
      cm(4)=ecm
      cm(1)=ecm*st*cp
      cm(2)=ecm*st*sp
      cm(3)=ecm*ct
*
* boost to lab frame
*
      ppar(4,3)=(rv(4)*cm(4)+rv(1)*cm(1)+rv(2)*cm(2)+rv(3)*cm(3))/rm
      ppar(4,4)=rv(4)-ppar(4,3)
      gam=(ppar(4,3)+ecm)/(rv(4)+rm)
      ppar(1,3)=cm(1)+gam*rv(1)
      ppar(1,4)=rv(1)-ppar(1,3)
      ppar(2,3)=cm(2)+gam*rv(2)
      ppar(2,4)=rv(2)-ppar(2,3)
      ppar(3,3)=cm(3)+gam*rv(3)
      ppar(3,4)=rv(3)-ppar(3,3)
*
* construct invariant mass & phase angle matrices
*
      do 10 i=1,npar+3
         do 10 j=i+1,npar+4
            siv(i,j)=2d0*(ppar(4,i)*ppar(4,j)-ppar(1,i)*ppar(1,j)
     .                   -ppar(2,i)*ppar(2,j)-ppar(3,i)*ppar(3,j))/sab
            siv(j,i)=siv(i,j)
 10   continue
      sba=sab
      do 20 j=3,npar+4
         siv(1,j)=-siv(1,j)
         siv(j,1)=-siv(j,1)
         siv(2,j)=-siv(2,j)
         siv(j,2)=-siv(j,2)
 20   continue
      sqsab=sqrt(abs(sab))
      do 30 i=1,npar+3
         do 30 j=i+1,npar+4
            s=siv(i,j)
            z2i=csqrt(cmplx(ppar(4,i)-ppar(1,i)))
            z1i=(ppar(2,i)-(0d0,1d0)*ppar(3,i))/z2i
            z2j=csqrt(cmplx(ppar(4,j)-ppar(1,j)))
            z1j=(ppar(2,j)-(0d0,1d0)*ppar(3,j))/z2j
            zu(i,j)=(z2i*z1j-z2j*z1i)/sqsab
            zd(i,j)=conjg(zu(i,j))
            if (s.le.0) then
               zu(i,j)=cmplx(0d0,1d0)*zu(i,j)
               zd(i,j)=cmplx(0d0,1d0)*zd(i,j)
            endif
            zu(j,i)=-zu(i,j)
            zd(j,i)=-zd(i,j)
 30   continue
      zu(1,2)=-zu(1,2)
      zd(1,2)=-zd(1,2)
      zu(2,1)=-zu(2,1)
      zd(2,1)=-zd(2,1)
*
      end
*
************************************************************************ 
*
      subroutine scale(jets)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /phypar/ w,ipp1,ipp2,rmw,rgw,rmz,rgz,sw2,qcdl
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,beam,
     .                breitmax,breitmin,breitwgt
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /parmom/ ppar(4,10)
      common /jetmom/ pjet(8,10),jp(10)
      common /invar/ sab,ya1,yb1,ya2,yb2,y12
      common /order/iorder
*
* calculating renormalization and factorization scale
*
      pt=0d0
      ptmax=0d0
      do i=1,jets
         j=jp(i)
         if (pjet(4,j).gt.0d0) then
            pti=pjet(5,j)
            pt=pt+pti
            if(pti.gt.ptmax)ptmax=pti
         endif
      enddo
      ptv=sqrt((ppar(1,3)+ppar(1,4))**2+(ppar(2,3)+ppar(2,4))**2)
      if (irenorm.eq.1) renscale=crenorm*sqrt(sab)
      if (irenorm.eq.2) renscale=crenorm*rm
      if (irenorm.eq.3) renscale=crenorm*rmv
      if (irenorm.eq.4) renscale=crenorm*sqrt(rm**2+ptv**2)
      if (irenorm.eq.5) renscale=crenorm*ptmax
      if (irenorm.eq.6) renscale=crenorm*ptv
      als=alfas(renscale,qcdl,iorder+1)
      if (ifact.eq.1) facscale=cfact*sqrt(sab)
      if (ifact.eq.2) facscale=cfact*rm
      if (ifact.eq.3) facscale=cfact*rmv
      if (ifact.eq.4) facscale=cfact*sqrt(rm**2+ptv**2)
      if (ifact.eq.5) facscale=cfact*ptmax
      if (ifact.eq.6) facscale=cfact*ptv
      return
      end
*
************************************************************************ 
*
      subroutine dy(npar,nloop,rint)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /structure/ f1(13),f2(13),fx1(13),fx2(13)
      common /partonid/ iu,id,is,ic,ib,it,iub,idb,isb,icb,ibb,itb,ig,
     .                  iq(6),ip(6),il(6)
      common /sigma/ sigma0(13,13),sigma1(13,13)
      common /fraction/ rm,x1,x2,facscale,renscale,als
      dimension s(13)
      data s / 6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,6d0,16d0 /
*
      do 5 i=1,13
         do 5 j=1,13
            sigma0(i,j)=0d0
            sigma1(i,j)=0d0
 5    continue
      call parden(nloop)
      rint=0d0
      call dymat(npar,nloop)
      if (nloop.eq.0) then
        do 10 i=1,13
          do 10 j=1,13
	    rint=rint+f1(i)*f2(j)*sigma0(i,j)/s(i)/s(j)
   10  	continue 
      else
	do 20 i=1,13
	  do 20 j=1,13
	    rint=rint+(f1(i)*f2(j)*sigma1(i,j)
     .	             +(f1(i)*fx2(j)+fx1(i)*f2(j))*sigma0(i,j))
     .		     /s(i)/s(j)
   20   continue
      endif
*
* multiply with overall coupling constants.
*
      rint=(6d0*pi*als)**npar*rint
* 
      end
*
      subroutine dymat(npar,nloop)
      implicit double precision (a-h,o-z)
      common /koppel/ nf,b0,cru,clu,crd,cld,crl,cll,
     .                      cru2,clu2,crd2,cld2,crl2,cll2
      common /sigma/ sigma0(13,13),sigma1(13,13)
      common /partonid/ iu,id,is,ic,ib,it,iub,idb,isb,icb,ibb,itb,ig,
     .                  iq(6),ip(6),il(6)
      common /inppar/ njets,mloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      dimension r0(3,0:1),r1(3,0:1),r2(3,0:1),r3(3,0:1)
      dimension s0(3,3,0:1,5),s1(3,3,0:1,5)
*
* q-qb initial state
*
      if (npar.ge.0) then
        call fivq2(2,1,npar,nloop,r0,r1)
        call fivq2(1,2,npar,nloop,r2,r3)
        stat=1d0
        if (npar.eq.2) stat=2d0
        do 10 iqv=1,6
          sigma0(iqv,ip(iqv))=sigma0(iqv,ip(iqv))
     .                       +r0(iq(iqv),il(iqv))/stat
          sigma0(ip(iqv),iqv)=sigma0(ip(iqv),iqv)
     .                       +r2(iq(iqv),il(iqv))/stat
    	  if (nloop.eq.1) then
	    sigma1(iqv,ip(iqv))=sigma1(iqv,ip(iqv))
     .                         +r1(iq(iqv),il(iqv))/stat
    	    sigma1(ip(iqv),iqv)=sigma1(ip(iqv),iqv)
     .                         +r3(iq(iqv),il(iqv))/stat
	  endif
   10   continue
      endif
      if (npar.ge.2) then
        call fivq4(2,1,npar,s0)
        call fivq4(1,2,npar,s1)
	do 12 iqv=1,6
           ibv=ip(iqv)
           iib=ibv-6
           do 12 iq1=1,nf
              ity=itype(iqv,iib,iq1)
              sigma0(iqv,ibv)=sigma0(iqv,ibv)
     .                       +s0(iq(iqv),iq(iq1),il(iqv),ity)
              sigma0(ibv,iqv)=sigma0(ibv,iqv)
     .                       +s1(iq(iqv),iq(iq1),il(iqv),ity)
 12     continue
	call fivq4(2,3,npar,s0)
	call fivq4(3,2,npar,s1)
        do 13 iqv=1,6
           iib=ip(iqv)-6
           do 13 iq1=1,6
              ib1=iq1+6
              if ((iqv.ne.iq1).and.(iib.ne.iq1)) then
                 ity=itype(iqv,iib,iq1)
                 sigma0(iqv,ib1)=sigma0(iqv,ib1)
     .                          +s0(iq(iqv),iq(iq1),il(iqv),ity)
                 sigma0(ib1,iqv)=sigma0(ib1,iqv)
     .                +s1(iq(iqv),iq(iq1),il(iqv),ity)
              endif
 13     continue
        if (ivec.le.2) then
	call fivq4(1,4,npar,s0)
	call fivq4(4,1,npar,s1)
	do 14 iqv=1,6
           ibv=ip(iqv)
           iib=ibv-6
           do 14 iq1=1,6
              if ((iqv.ne.iq1).and.(iib.ne.iq1)) then
                 ity=itype(iqv,iib,iq1)
                 sigma0(ibv,iq1)=sigma0(ibv,iq1)
     .                +s0(iq(iqv),iq(iq1),il(iqv),ity)
                 sigma0(iq1,ibv)=sigma0(iq1,ibv)
     .                +s1(iq(iqv),iq(iq1),il(iqv),ity)
              endif
   14   continue
        call fivq4(4,3,npar,s0)
        call fivq4(3,4,npar,s1)
        do 16 iqv=1,6
           iib=ip(iqv)-6
           do 16 iq1=1,6
              ib1=iq1+6
              ity=itype(iqv,iib,iq1)
              sigma0(iq1,ib1)=sigma0(iq1,ib1)
     .             +s0(iq(iqv),iq(iq1),il(iqv),ity)
              sigma0(ib1,iq1)=sigma0(ib1,iq1)
     .             +s1(iq(iqv),iq(iq1),il(iqv),ity)
 16     continue
        endif
      endif
*
* q-g initial state
*
      if (npar.ge.1) then
        call fivq2(2,3,npar,nloop,r0,r1)
        call fivq2(3,2,npar,nloop,r2,r3)
        do 20 iqv=1,6
          sigma0(iqv,ig)=sigma0(iqv,ig)-r0(iq(iqv),il(iqv))
          sigma0(ig,iqv)=sigma0(ig,iqv)-r2(iq(iqv),il(iqv))
    	  if (nloop.eq.1) then
	    sigma1(iqv,ig)=sigma1(iqv,ig)-r1(iq(iqv),il(iqv))
            sigma1(ig,iqv)=sigma1(ig,iqv)-r3(iq(iqv),il(iqv))
          endif
   20   continue
      endif
*
* qb-g initial state
*
      if (npar.ge.1) then
        call fivq2(1,3,npar,nloop,r0,r1)
        call fivq2(3,1,npar,nloop,r2,r3)
        do 30 iqv=1,6
          sigma0(ip(iqv),ig)=sigma0(ip(iqv),ig)-r0(iq(iqv),il(iqv))
          sigma0(ig,ip(iqv))=sigma0(ig,ip(iqv))-r2(iq(iqv),il(iqv))
    	  if (nloop.eq.1) then
            sigma1(ip(iqv),ig)=sigma1(ip(iqv),ig)-r1(iq(iqv),il(iqv))
            sigma1(ig,ip(iqv))=sigma1(ig,ip(iqv))-r3(iq(iqv),il(iqv))
          endif
   30   continue
      endif
*
* q-q initial state
*
      if (npar.ge.2) then
         call fivq4(2,4,npar,s0)
         if (ivec.le.2) then
            call fivq4(4,2,npar,s1)
         endif
         do 40 iqv=1,6
            iib=ip(iqv)-6
            do 40 iq1=1,6
              ity=itype(iqv,iib,iq1)
              if (iq1.eq.iib) then
                 fac=0.5d0
              else
                 fac=1.0d0
              endif
              sigma0(iqv,iq1)=sigma0(iqv,iq1)
     .                       +fac*s0(iq(iqv),iq(iq1),il(iqv),ity)
              if ((iqv.ne.iq1).and.(ivec.le.2)) then
                 sigma0(iq1,iqv)=sigma0(iq1,iqv)
     .                          +fac*s1(iq(iqv),iq(iq1),il(iqv),ity)
              endif
 40        continue
      endif
*
* qb-qb initial state
*
      if (npar.ge.2) then
        call fivq4(1,3,npar,s0)
        if (ivec.le.2) then
           call fivq4(3,1,npar,s1)
        endif
        do 50 iqv=1,6
           ibv=ip(iqv)
           iib=ibv-6
           do 50 iq1=1,6
              ib1=iq1+6
              ity=itype(iqv,iib,iq1)
              if (ib1.eq.iqv+6) then
                 fac=0.5d0
              else
                 fac=1.0d0
              endif
              sigma0(ibv,ib1)=sigma0(ibv,ib1)
     .                       +fac*s0(iq(iqv),iq(iq1),il(iqv),ity)
              if ((ibv.ne.ib1).and.(ivec.le.2)) then
                 sigma0(ib1,ibv)=sigma0(ib1,ibv)
     .                          +fac*s1(iq(iqv),iq(iq1),il(iqv),ity)
              endif
 50     continue
      endif
*
* g-g initial state
*
      if (npar.ge.2) then
         call fivq2(4,3,npar,nloop,r0,r1)
         do 60 iqv=1,6
            sigma0(ig,ig)=sigma0(ig,ig)+r0(iq(iqv),il(iqv))
 60      continue
      endif
*
      end
*
      function itype(iq1,ib1,iq2)
      implicit double precision (a-h,o-z)
*
      if (iq1.eq.ib1) then
* Z-case
         it=4
         if (iq1.eq.iq2) it=5
      else
* W-case
         it=1
         if (iq1.eq.iq2) it=2
         if (ib1.eq.iq2) it=3
      endif
      itype=it
*
      end
*
************************************************************************ 

*
      subroutine fivq2(i1,i2,npar,nloop,r0,r1)
      implicit double precision (a-h,o-z)
      common /koppel/ nf,b0,cru,clu,crd,cld,crl,cll,
     .                      cru2,clu2,crd2,cld2,crl2,cll2
      common /buildq2/ rb(2,2),rnlo(2,2)
      dimension r0(3,0:1),r1(3,0:1)
*
      call mq2(i1,i2,npar,nloop)
      rrr=rb(1,1)
      rrl=rb(1,2)
      rlr=rb(2,1)
      rll=rb(2,2)
      r0(1,0)=cru2*crl2*rrr+cru2*cll2*rrl
     .       +clu2*crl2*rlr+clu2*cll2*rll
      r0(2,0)=crd2*crl2*rrr+crd2*cll2*rrl
     .       +cld2*crl2*rlr+cld2*cll2*rll
      r0(3,0)=0d0
      rrr=rb(1,2)
      rrl=rb(1,1)
      rlr=rb(2,2)
      rll=rb(2,1)
      r0(1,1)=cru2*crl2*rrr+cru2*cll2*rrl
     .       +clu2*crl2*rlr+clu2*cll2*rll
      r0(2,1)=crd2*crl2*rrr+crd2*cll2*rrl
     .       +cld2*crl2*rlr+cld2*cll2*rll
      r0(3,1)=0d0
*
      if (nloop.eq.1) then
         rrr=rnlo(1,1)
         rrl=rnlo(1,2)
         rlr=rnlo(2,1)
         rll=rnlo(2,2)
         r1(1,0)=cru2*crl2*rrr+cru2*cll2*rrl
     .          +clu2*crl2*rlr+clu2*cll2*rll
         r1(2,0)=crd2*crl2*rrr+crd2*cll2*rrl
     .          +cld2*crl2*rlr+cld2*cll2*rll
         r1(3,0)=0d0
         rrr=rnlo(1,2)
         rrl=rnlo(1,1)
         rlr=rnlo(2,2)
         rll=rnlo(2,1)
         r1(1,1)=cru2*crl2*rrr+cru2*cll2*rrl
     .          +clu2*crl2*rlr+clu2*cll2*rll
         r1(2,1)=crd2*crl2*rrr+crd2*cll2*rrl
     .          +cld2*crl2*rlr+cld2*cll2*rll
         r1(3,1)=0d0
      endif
*
      end
*
************************************************************************ 
*
      subroutine mq2(j1,j2,npar,nloop)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      parameter(c0=-3.789868134d0)
      parameter(c1=-3.357514045d0)
      common /buildq2/ rb(2,2),rnlo(2,2)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /thcut/ smin,s0
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /invariant/ sab,siv(10,10)
      common /koppel/ nf,b0,cru,clu,crd,cld,crl,cll,
     .                      cru2,clu2,crd2,cld2,crl2,cll2
      dimension ip(10)
*
* ip(1) -> points to      quark momentum in plab
* ip(2) -> points to anti quark momentum in plab
* ip(3) -> points to gluon momentum #1 in plab
* ip(4) -> gluon 2
* etc
*
      ip(j1)=1
      ip(j2)=2
      j=4
      do 10 i=1,npar+2
         if ((i.ne.j1).and.(i.ne.j2)) then
            j=j+1
            ip(i)=j
         endif
 10   continue
      ia=ip(1)
      ib=ip(2)
*
      if (npar.eq.0) then
         rb(1,1)=q2g0(ia,ib,3,4)
         rb(1,2)=q2g0(ia,ib,4,3)
         if (nloop.eq.1) then
            dl=-log(smin/sab)
            rk=1d0+als/pi/0.75d0*(-dl**2+1.5d0*dl+c0+pi**2/2d0)
            rnlo(1,1)=rk*rb(1,1)
            rnlo(1,2)=rk*rb(1,2)
         endif
      endif
*
      if (npar.ge.1) then
         i1=ip(3)
         if (npar.eq.1) then
*
            rb(1,1)=q2g1(ia,i1,ib,3,4)
            rb(1,2)=q2g1(ia,i1,ib,4,3)
            if (nloop.eq.1) then
* (c1 = -2*pi**2/3 + 29/9, c0 = -pi**2/3 - 1/2 )
               ymin=smin/sab
               y=siv(ia,i1)
               d1a1=log(abs(y)/ymin)
               d2a1=d1a1**2
               tha1=dim(y,0d0)/y
               y=siv(ib,i1)
               d1b1=log(abs(y)/ymin)
               d2b1=d1b1**2
               thb1=dim(y,0d0)/y
               y=siv(ia,ib)
               d1ab=log(abs(y)/ymin)
               d2ab=d1ab**2
               thab=dim(y,0d0)/y
               rk=1.5d0*als/pi*(-d2a1-d2b1+0.75d0*(d1a1+d1b1)
     .                         +pi**2/2d0*(tha1+thb1)+c1-nf/5.4d0
     .                        -(-d2ab+1.5d0*d1ab+c0+pi**2/2*thab)/9d0)
               rk=rk+als*b0*log(renscale**2/smin)
               rnlo(1,1)=(1d0+rk)*rb(1,1)
     .                  +1.5d0*als/pi*fq2g1(ia,i1,ib,3,4)
               rnlo(1,2)=(1d0+rk)*rb(1,2)
     .                  +1.5d0*als/pi*fq2g1(ia,i1,ib,4,3)
            endif
         endif
      endif
*
      if (npar.ge.2) then
         i2=ip(4)
         if (npar.eq.2) then
            rb(1,1)=q2g2(ia,i1,i2,ib,3,4)+q2g2(ia,i2,i1,ib,3,4)
     .             -q2f2(ia,i1,i2,ib,3,4)/9d0
            rb(1,2)=q2g2(ia,i1,i2,ib,4,3)+q2g2(ia,i2,i1,ib,4,3)
     .             -q2f2(ia,i1,i2,ib,4,3)/9d0
         endif
      endif
*
      rb(2,1)=rb(1,2)
      rb(2,2)=rb(1,1)
      if (nloop.eq.1) then
         rnlo(2,1)=rnlo(1,2)
         rnlo(2,2)=rnlo(1,1)
      endif
*
      end
*
************************************************************************ 
*
      function q2g0(ia,ib,ip,im)
      implicit double precision (a-h,o-z)
      common /invariant/ sab,siv(10,10)
*
      yap=siv(ia,ip)
*
      q2g0=12d0*(yap*sab)**2
*
      end
*
      function q2g1(ia,i1,ib,ip,im)
      implicit double precision (a-h,o-z)
      common /invariant/ sab,siv(10,10)
*
      ypm=siv(ip,im)
      ya1=siv(ia,i1)
      yb1=siv(ib,i1)
      yap=siv(ia,ip)
      ybm=siv(ib,im)
*      
      q2g1=(64d0/3d0)*sab*ypm*(yap**2+ybm**2)/ya1/yb1
*
      end
*
      function q2g2(ia,i1,i2,ib,ip,im)
      implicit double precision (a-h,o-y)
      implicit complex*16 (z)
      common /invariant/ sab,siv(10,10)
      common /spinor/ zu(10,10),zd(10,10)
*
      ya1=siv(ia,i1)
      ya2=siv(ia,i2)
      yb1=siv(ib,i1)
      yb2=siv(ib,i2)
      y12=siv(i1,i2)
      yap=siv(ia,ip)
      ybm=siv(ib,im)
      ypm=siv(ip,im)
*
      ya12=ya1+ya2+y12
      y12b=y12+yb1+yb2
*
* equal helicities (sum over ++ & --)
*
      s1=ypm*(yap**2+ybm**2)/ya1/y12/yb2
*
* unequal helicities
*
      zua1=zu(ia,i1)
      zua2=zu(ia,i2)
      zub1=zu(ib,i1)
      zub2=zu(ib,i2)
      zu12=zu(i1,i2)
      zubm=zu(ib,im)
      zum1=zu(im,i1)
      zum2=zu(im,i2)
*
      zda1=zd(ia,i1)
      zda2=zd(ia,i2)
      zdb1=zd(ib,i1)
      zdb2=zd(ib,i2)
      zd12=zd(i1,i2)
      zdap=zd(ia,ip)
      zdp1=zd(ip,i1)
      zdp2=zd(ip,i2)
*
* +-
*
      za=zda1*(zdap*zua2-zdp1*zu12)
      zb=zub2*(zdb1*zubm+zd12*zum2)
*
      zm=-zda1*zua2*zubm*za/ya1/y12/ya12
     .   -zub2*zdb1*zdap*zb/yb2/y12/y12b
     .   +za*zb/ya1/y12/yb2
      s2=abs(zm)**2
*
* -+
*
      zm=-zda2**2*zua1*zubm*(zdap*zua1+zdp2*zu12)/ya1/y12/ya12
     .   -zub1**2*zdb2*zdap*(zdb2*zubm-zd12*zum1)/yb2/y12/y12b
     .   +zua1*zda2*zub1*zdb2*zubm*zdap/ya1/y12/yb2
      s3=abs(zm)**2
*
      if (s1.gt.0) then
         q2g2=128d0/3d0*(s1+s2+s3)
      else
         q2g2=128d0/3d0*(s1-s2-s3)
      endif
*
      end
*
************************************************************************ 

*
      function q2f2(ia,i1,i2,ib,ip,im)
      implicit double precision (a-h,o-y)
      implicit complex*16 (z)
      common /invariant/ sab,siv(10,10)
      common /spinor/ zu(10,10),zd(10,10)
*
      yab=siv(ia,ib)
      ya1=siv(ia,i1)
      ya2=siv(ia,i2)
      yb1=siv(ib,i1)
      yb2=siv(ib,i2)
      yap=siv(ia,ip)
      ybm=siv(ib,im)
      ypm=siv(ip,im)
      y12=siv(i1,i2)
*
      ya12=ya1+ya2+y12
      y12b=y12+yb1+yb2
*
* ++ ( & -- )
*
      s1=ypm*yab*(yap**2+ybm**2)/ya1/ya2/yb1/yb2
*
* unequal helicities 

*
      zuab=zu(ia,ib)
      zua1=zu(ia,i1)
      zua2=zu(ia,i2)
      zub1=zu(ib,i1)
      zub2=zu(ib,i2)
      zu12=zu(i1,i2)
      zubm=zu(ib,im)
      zum1=zu(im,i1)
      zum2=zu(im,i2)
*
      zdab=zd(ia,ib)
      zda1=zd(ia,i1)
      zda2=zd(ia,i2)
      zdb1=zd(ib,i1)
      zdb2=zd(ib,i2)
      zd12=zd(i1,i2)
      zdap=zd(ia,ip)
      zdp1=zd(ip,i1)
      zdp2=zd(ip,i2)
*
* +- 

*
      za=zda1*(zdap*zua2-zdp1*zu12)
      zb=zub2*(zdb1*zubm+zd12*zum2)
*
      zm=-zda1*zua2*zubm*za/ya1/ya2/ya12
     .   -zub2*zdb1*zdap*zb/yb1/yb2/y12b
     .   +zda1*zua2*zdb1*zub2*(zdap*zuab+zdp1*zub1)
     .                       *(zda2*zum2-zdab*zubm)/ya1/ya2/yb1/yb2
      s2=abs(zm)**2
*
* -+ 

*
      za=zda2*(zdap*zua1+zdp2*zu12)
      zb=zub1*(zdb2*zubm-zd12*zum1)
*
      zm=-zda2*zua1*zubm*za/ya1/ya2/ya12
     .   -zub1*zdb2*zdap*zb/yb1/yb2/y12b
     .   +zda2*zua1*zdb2*zub1*(zdap*zuab+zdp2*zub2)
     .                       *(zda1*zum1-zdab*zubm)/ya1/ya2/yb1/yb2
      s3=abs(zm)**2
*
      if (s1.ge.0) then
         q2f2=128d0/3d0*(s1+s2+s3)
      else
         q2f2=128d0/3d0*(s1-s2-s3)
      endif         
*
      end
*
************************************************************************ 

*
      function fq2g1(ia,i1,ib,ip,im)
      implicit double precision (a-h,o-y)
      implicit complex*16 (z)
      common /invariant/ sab,siv(10,10)
      common /spinor/ zu(10,10),zd(10,10)
      common /fraction/ rm,x1,x2,facscale,renscale,als
*
      zuab=zu(ia,ib)
      zua1=zu(ia,i1)
      zub1=zu(ib,i1)
      zuap=zu(ia,ip)
      zuam=zu(ia,im)
      zubm=zu(ib,im)
      zup1=zu(ip,i1)
      zum1=zu(im,i1)
      zupm=zu(ip,im)
*
      zdab=zd(ia,ib)
      zda1=zd(ia,i1)
      zdb1=zd(ib,i1)
      zdap=zd(ia,ip)
      zdbp=zd(ib,ip)
      zdam=zd(ia,im)
      zdbm=zd(ib,im)
      zdp1=zd(ip,i1)
      zdpm=zd(ip,im)
* (+,+,-) 
*
      call ert(ia,i1,ib,zal,zbe,zga)
*
      zlo=zdbm*zdbm*zupm/zda1/zdb1
      zf=-(zal*zdap*zuab+zbe*zdp1*zub1)*zubm/zua1/zub1
     .    +zga*zda1*zuam*zdp1/zua1/zdb1
      fppm=real(zlo*zf)
* (+,-,-) 
*
      call ert(ib,i1,ia,zal,zbe,zga)
*
      zal=conjg(zal)
      zbe=conjg(zbe)
      zga=conjg(zga)
      zlo=zuap*zuap*zdpm/zua1/zub1
      zf=(-zal*zdab*zubm+zbe*zda1*zum1)*zdap/zdb1/zda1
     .    -zga*zub1*zdbp*zum1/zdb1/zua1
*
      fpmm=real(zlo*zf)
*
      fq2g1=64d0/3d0*sab*(fppm+fpmm)
*
      end
*
c*********************************************************************** 
c subroutine ert(iq,ik,ip,al,be,de,ga) computes the functions multiplying
c  the different one-loop helicity structures
c*********************************************************************** 
*
      subroutine ert(iq,ik,ip,al,be,de)
      implicit double precision(a-h,o-y)
      implicit complex*16 (z)
      parameter(pi=3.141592653589793238d0)
      common /invariant/ sab,siv(10,10)
      common /fraction/ rm,x1,x2,facscale,renscale,als
      complex*16 al,al0,al2,be,be0,be2,de,de0,de2,ga,ga0,ga2,
     .           rqpkp,rqpqk,rqkkp,spence
c
      cov=sab/rm**2
      yqp=siv(iq,ip)*cov
      yqk=siv(iq,ik)*cov
      ykp=siv(ik,ip)*cov
      dqp=dlog(abs(yqp))
      tqp=-dim(-yqp,0d0)/yqp
      dqk=dlog(abs(yqk))
      tqk=-dim(-yqk,0d0)/yqk
      dkp=dlog(abs(ykp))
      tkp=-dim(-ykp,0d0)/ykp
      omyqp=1d0-yqp
      omyqk=1d0-yqk
      omykp=1d0-ykp
      omdqp=dlog(abs(omyqp))
      omtqp=-dim(-omyqp,0d0)/omyqp
      omdqk=dlog(abs(omyqk))
      omtqk=-dim(-omyqk,0d0)/omyqk
      omdkp=dlog(abs(omykp))
      omtkp=-dim(-omykp,0d0)/omykp
      zqp=dcmplx(dqp,tqp*pi*0d0)
      zqk=dcmplx(dqk,tqk*pi*0d0)
      zkp=dcmplx(dkp,tkp*pi*0d0)
      rqpkp=spence(dcmplx((1-yqp-ykp)/omyqp))
     .     +spence(dcmplx((1-yqp-ykp)/omykp))
     .     -spence(dcmplx((1-yqp-ykp)/omyqp/omykp))
     .     -omdqp*omdkp+dqp*dkp+pi**2*(omtqp*omtkp-tqp*tkp)
      rqpqk=spence(dcmplx((1-yqp-yqk)/omyqp))
     .     +spence(dcmplx((1-yqp-yqk)/omyqk))
     .     -spence(dcmplx((1-yqp-yqk)/omyqp/omyqk))
     .     -omdqp*omdqk+dqp*dqk+pi**2*(omtqp*omtqk-tqp*tqk)
      rqkkp=spence(dcmplx((1-yqk-ykp)/omyqk))
     .     +spence(dcmplx((1-yqk-ykp)/omykp))
     .     -spence(dcmplx((1-yqk-ykp)/omyqk/omykp))
     .     -omdqk*omdkp+dqk*dkp+pi**2*(omtqk*omtkp-tqk*tkp)
      rqpkp=rqpkp-pi*dcmplx(0d0,omdqp*omtkp+omdkp*omtqp-dqp*tkp-dkp*tqp)
      rqpqk=rqpqk-pi*dcmplx(0d0,omdqp*omtqk+omdqk*omtqp-dqp*tqk-dqk*tqp)
      rqkkp=rqkkp-pi*dcmplx(0d0,omdqk*omtkp+omdkp*omtqk-dqk*tkp-dkp*tqk)
      rqpkp=dcmplx(dreal(rqpkp),0d0)
      rqpqk=dcmplx(dreal(rqpqk),0d0)
      rqkkp=dcmplx(dreal(rqkkp),0d0)
c
      al0=-rqkkp-ykp*(4d0-3d0*ykp)/2d0/omykp**2*zkp-ykp/2d0/omykp
      be0=-rqkkp+(4d0-3d0*ykp)/2d0/omykp*zkp+1d0
      de0=ykp/2d0/omykp**2*zkp+ykp/2d0/omykp
      al0=al0-0.75d0*(zqk+zkp)
      be0=be0-0.75d0*(zqk+zkp)
      ga0=(yqp*ykp/2d0/omykp**2+yqp/omykp)*zkp+yqp/2d0/omykp
      al2=rqpqk+omyqp**2/yqk**2*rqpkp+ykp/yqk*zqp+ykp/2d0/omykp
     . +(ykp*(4d0-3d0*ykp)/2d0/omykp**2+ykp**2/yqk/omykp)*zkp
      be2=rqpqk-(yqp*omyqp/yqk**2+1d0/yqk)*rqpkp
     . -(yqk/omyqp**2+omykp/yqk)*zqp
     . -((4d0-3d0*ykp)/2d0/omykp+ykp/yqk)*zkp-yqk/omyqp
      de2=-ykp/yqk**2*rqpkp+(yqk/omyqp**2-1d0/yqk)*zqp
     . -(ykp/2d0/omykp**2+ykp/yqk/omykp)*zkp-ykp/omyqp-ykp/2d0/omykp
      ga2=-yqp*omyqp/yqk**2*rqpkp-yqp/yqk*zqp-yqp/2d0/omykp
     . -(yqp*ykp/2d0/omykp**2+yqp*omyqp/yqk/omykp)*zkp
      al2=al2+1.5d0*zqp
      be2=be2+1.5d0*zqp
      al=al0+al2/9d0
      be=be0+be2/9d0
      de=de0+de2/9d0
      ga=ga0+ga2/9d0
      end
*
************************************************************************ 
*
      complex*16 function spence(x)
c spence(x) calcs the complex spence-function, through mapping on
c the area where there is a quickly convergent series.
      real*8 pi
      complex*16 x,spenc
      pi=4d0*datan(1d0)
c map the x on the unit circle.
c but so that x is not in the neighbourhood of (1,0)
c abs(z)=-cdlog(1d0-x) is always smaller than 1.10
c but (1.10)^19/(19!)*bernoulli(19)=2.7d-15
      if ( abs(1d0-x).lt.1d-13) then
        spence=pi*pi/6d0
      else if (abs(1d0-x).lt.0.5d0) then
        spence=pi*pi/6d0-log(1d0-x)*log(x)-spenc(1d0-x)
      else if (abs(x).gt.1d0) then
        spence=-pi*pi/6d0-0.5d0*log(-x)*log(-x)-spenc(1d0/x)
      else
        spence = spenc(x)
      end if
      end
*
************************************************************************ 
*
      complex*16 function spenc(x)
      complex*16 x,sum,z,z2
      z=-log(1d0-x)
      z2=z*z
c horner's rule for the powers z^3 through z^19
      sum=43867d0/798d0
      sum=sum*z2/342d0-3617d0/510d0
      sum=sum*z2/272d0+7d0/6d0
      sum=sum*z2/210d0-691d0/2730d0
      sum=sum*z2/156d0+5d0/66d0
      sum=sum*z2/110d0-1d0/30d0
      sum=sum*z2/ 72d0+1d0/42d0
      sum=sum*z2/ 42d0-1d0/30d0
      sum=sum*z2/ 20d0+1d0/6d0
c the first three terms of the power serie
      sum=z2*z*sum/6d0-0.25d0*z2+z
      spenc=sum
      return
      end
*
************************************************************************ 
*
      subroutine fivq4(i1,i2,npar,s)
      implicit double precision (a-h,o-z)
      common /koppel/ nf,b0,cru,clu,crd,cld,crl,cll,
     .                      cru2,clu2,crd2,cld2,crl2,cll2
      common /buildq4/ rb(3,2),rbuu(2,2),rbud(2,2)
     .                        ,rbdu(2,2),rbdd(2,2)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      dimension s(3,3,0:1,5)
*
      if (ivec.le.2) then
         call mq4w(i1,i2,npar)
         do 10 i=1,3
            s(1,1,0,i)=clu2*cll2*rb(i,1)
            s(1,2,0,i)=s(1,1,0,i)
            s(1,3,0,i)=s(1,1,0,i)
            s(2,1,0,i)=s(1,1,0,i)
            s(2,2,0,i)=s(1,1,0,i)
            s(2,3,0,i)=s(1,1,0,i)
            s(1,1,1,i)=clu2*cll2*rb(i,2)
            s(1,2,1,i)=s(1,1,1,i)
            s(1,3,1,i)=s(1,1,1,i)
            s(2,1,1,i)=s(1,1,1,i)
            s(2,2,1,i)=s(1,1,1,i)
            s(2,3,1,i)=s(1,1,1,i)
 10      continue
      endif
*
      if (ivec.eq.3) then
         call mq4z(i1,i2,npar)
         do i=1,2
            s(1,1,0,i+3)=crl2*rbuu(i,1)+cll2*rbuu(i,2)
            s(1,2,0,i+3)=crl2*rbud(i,1)+cll2*rbud(i,2)
            s(2,1,0,i+3)=crl2*rbdu(i,1)+cll2*rbdu(i,2)
            s(2,2,0,i+3)=crl2*rbdd(i,1)+cll2*rbdd(i,2)
         enddo
*     
      endif
      end
*
************************************************************************ 

*
      subroutine mq4w(i1,i2,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /buildq4/ rb(3,2),rbuu(2,2),rbud(2,2)
     .                        ,rbdu(2,2),rbdd(2,2)
      dimension ip(10),q4(3)
*
* ip(1) -> points to vector boson      quark momentum in plab
* ip(2) -> points to vector boson anti quark momentum in plab
* ip(3) -> points to                   quark momentum in plab
* ip(4) -> points to              anti quark momentum in plab
* etc
*
      ip(i1)=1
      ip(i2)=2
      j=4
      do 10 i=1,npar+2
         if ((i.ne.i1).and.(i.ne.i2)) then
            j=j+1
            ip(i)=j
         endif
 10   continue
*
      iqv=ip(1)
      ibv=ip(2)
      iq1=ip(3)
      ib1=ip(4)
      call q4g0w(iqv,ibv,iq1,ib1,3,4,q4)
      do 20 i=1,3
         rb(i,1)=q4(i)
 20   continue
      call q4g0w(iqv,ibv,iq1,ib1,4,3,q4)
      do 30 i=1,3
         rb(i,2)=q4(i)
 30   continue
*
      end
*
************************************************************************ 
*
      subroutine mq4z(i1,i2,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /buildq4/ rb(3,2),rbuu(2,2),rbud(2,2)
     .                        ,rbdu(2,2),rbdd(2,2)
      dimension ip(10),q4(2,4)
*
* ip(1) -> points to vector boson      quark momentum in plab
* ip(2) -> points to vector boson anti quark momentum in plab
* ip(3) -> points to                   quark momentum in plab
* ip(4) -> points to              anti quark momentum in plab
* etc
*
      ip(i1)=1
      ip(i2)=2
      j=4
      do 10 i=1,npar+2
         if ((i.ne.i1).and.(i.ne.i2)) then
            j=j+1
            ip(i)=j
         endif
 10   continue
*
      iqv=ip(1)
      ibv=ip(2)
      iq1=ip(3)
      ib1=ip(4)
      call q4g0z(iqv,ibv,iq1,ib1,3,4,q4)
      do 20 i=1,2
         rbuu(i,1)=q4(i,1)
         rbud(i,1)=q4(i,2)
         rbdu(i,1)=q4(i,3)
         rbdd(i,1)=q4(i,4)
 20   continue
      call q4g0z(iqv,ibv,iq1,ib1,4,3,q4)
      do 30 i=1,2
         rbuu(i,2)=q4(i,1)
         rbud(i,2)=q4(i,2)
         rbdu(i,2)=q4(i,3)
         rbdd(i,2)=q4(i,4)
 30   continue
*
      end
*
************************************************************************ 
*
      subroutine q4g0w(i1,i2,i3,i4,ip,im,q4)
      implicit double precision (a-h,o-y)
      implicit complex*16 (z)
      dimension q4(3)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
*
      fac=128d0/9d0
      call zq4g0(i1,i2,i3,i4,ip,im,z1pm,z1mp)
      call zq4g0(i3,i2,i1,i4,ip,im,z2pm,z2mp)
      call zq4g0(i1,i4,i3,i2,ip,im,z3pm,z3mp)
      rm1=abs(z1pm)**2+abs(z1mp)**2
      rm2=abs(z2pm)**2+abs(z2mp)**2
      rm3=abs(z3pm)**2+abs(z3mp)**2
      q4(1)=fac*rm1
      q4(2)=fac*(rm1+rm3+2d0/3d0*real(z1pm*conjg(z3pm)))
      q4(3)=fac*(rm1+rm2+2d0/3d0*real(z1pm*conjg(z2pm)))
*
      end
*
************************************************************************ 
*
      subroutine q4g0z(i1,i2,i3,i4,ip,im,q4)
      implicit double precision (a-h,o-y)
      implicit complex*16 (z)
      dimension q4(2,4)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /koppel/ nf,b0,cru,clu,crd,cld,crl,cll,
     .                      cru2,clu2,crd2,cld2,crl2,cll2
*
      fac=128d0/9d0
      call zq4g0(i1,i2,i3,i4,ip,im,z1pm,z1mp)
      call zq4g0(i1,i2,i3,i4,im,ip,z1cpm,z1cmp)
      call zq4g0(i3,i2,i1,i4,ip,im,z2pm,z2mp)
      call zq4g0(i3,i2,i1,i4,im,ip,z2cpm,z2cmp)
      call zq4g0(i1,i4,i3,i2,ip,im,z3pm,z3mp)
      call zq4g0(i1,i4,i3,i2,im,ip,z3cpm,z3cmp)
      call zq4g0(i3,i4,i1,i2,ip,im,z4pm,z4mp)
      call zq4g0(i3,i4,i1,i2,im,ip,z4cpm,z4cmp)
*
      z1=cru*z1pm       +cru*z4pm
      z2=cru*z1mp       +clu*conjg(z4cmp)
      z3=clu*conjg(z1cmp)+cru*z4mp
      z4=clu*conjg(z1cpm)+clu*conjg(z4cpm)
      z5=cru*z3pm       +cru*z2pm
      z6=cru*z3mp       +clu*conjg(z2cmp)
      z7=clu*conjg(z3cmp)+cru*z2mp
      z8=clu*conjg(z3cpm)+clu*conjg(z2cpm)
      q4(1,1)=fac*(abs(z1)**2+abs(z2)**2+abs(z3)**2+abs(z4)**2)
      q4(2,1)=q4(1,1)
     .       +fac*(abs(z5)**2+abs(z6)**2+abs(z7)**2+abs(z8)**2
     .            +2d0/3d0*real(z1*conjg(z5)+z4*conjg(z8)))
*
      z1=cru*z1pm       +crd*z4pm
      z2=cru*z1mp       +cld*conjg(z4cmp)
      z3=clu*conjg(z1cmp)+crd*z4mp
      z4=clu*conjg(z1cpm)+cld*conjg(z4cpm)
      q4(1,2)=fac*(abs(z1)**2+abs(z2)**2+abs(z3)**2+abs(z4)**2)
      q4(2,2)=0d0
*
      z1=crd*z1pm       +cru*z4pm
      z2=crd*z1mp       +clu*conjg(z4cmp)
      z3=cld*conjg(z1cmp)+cru*z4mp
      z4=cld*conjg(z1cpm)+clu*conjg(z4cpm)
      q4(1,3)=fac*(abs(z1)**2+abs(z2)**2+abs(z3)**2+abs(z4)**2)
      q4(2,3)=0d0
*
      z1=crd*z1pm       +crd*z4pm
      z2=crd*z1mp       +cld*conjg(z4cmp)
      z3=cld*conjg(z1cmp)+crd*z4mp
      z4=cld*conjg(z1cpm)+cld*conjg(z4cpm)
      z5=crd*z3pm       +crd*z2pm
      z6=crd*z3mp       +cld*conjg(z2cmp)
      z7=cld*conjg(z3cmp)+crd*z2mp
      z8=cld*conjg(z3cpm)+cld*conjg(z2cpm)
      q4(1,4)=fac*(abs(z1)**2+abs(z2)**2+abs(z3)**2+abs(z4)**2)
      q4(2,4)=q4(1,4)
     .       +fac*(abs(z5)**2+abs(z6)**2+abs(z7)**2+abs(z8)**2
     .            +2d0/3d0*real(z1*conjg(z5)+z4*conjg(z8)))
*     
      end
*
************************************************************************ 
*
      subroutine zq4g0(i1,i2,i3,i4,ip,im,zpm,zmp)
      implicit double precision (a-h,o-y)
      implicit complex*16 (z)
      common /invariant/ sab,siv(10,10)
      common /spinor/ zu(10,10),zd(10,10)
*
      y13=siv(i1,i3)
      y14=siv(i1,i4)
      y23=siv(i2,i3)
      y24=siv(i2,i4)
      y34=siv(i3,i4)
*
      y134=y13+y14+y34
      y234=y23+y24+y34
*
      zu13=zu(i1,i3)
      zu14=zu(i1,i4)
      zu23=zu(i2,i3)
      zu24=zu(i2,i4)
      zu34=zu(i3,i4)
      zu2m=zu(i2,im)
      zu3m=zu(i3,im)
      zu4m=zu(i4,im)
*
      zd13=zd(i1,i3)
      zd14=zd(i1,i4)
      zd23=zd(i2,i3)
      zd24=zd(i2,i4)
      zd34=zd(i3,i4)
      zd1p=zd(i1,ip)
      zd3p=zd(i3,ip)
      zd4p=zd(i4,ip)
*
* +-;+- 
*
      zpm=-zd13*zu2m*(zd1p*zu14+zd3p*zu34)/y34/y134
     .     +zu24*zd1p*(zd23*zu2m-zd34*zu4m)/y34/y234
*
* +-;-+ 
*
      zmp=-zd14*zu2m*(zd1p*zu13-zd4p*zu34)/y34/y134
     .     +zu23*zd1p*(zd24*zu2m+zd34*zu3m)/y34/y234
*
      end
*
************************************************************************ 
*
      function rn(idummy)
      real*8 rn,ran
      save init
      data init /1/
      if (init.eq.1) then
        init=0
        call rmarin(1802,9373)
      end if
*
  10  call ranmar(ran)
      if (ran.lt.1d-16) goto 10
      rn=ran
*
      end
*
************************************************************************ 
*
      subroutine ranmar(rvec)
*     -----------------
* universal random number generator proposed by marsaglia and zaman
* in report fsu-scri-87-50
* in this version rvec is a double precision variable.
      implicit real*8(a-h,o-z)
      common/ raset1 / ranu(97),ranc,rancd,rancm
      common/ raset2 / iranmr,jranmr
      save /raset1/,/raset2/
      uni = ranu(iranmr) - ranu(jranmr)
      if(uni .lt. 0d0) uni = uni + 1d0
      ranu(iranmr) = uni
      iranmr = iranmr - 1
      jranmr = jranmr - 1
      if(iranmr .eq. 0) iranmr = 97
      if(jranmr .eq. 0) jranmr = 97
      ranc = ranc - rancd
      if(ranc .lt. 0d0) ranc = ranc + rancm
      uni = uni - ranc
      if(uni .lt. 0d0) uni = uni + 1d0
      rvec = uni
      end
* 
      subroutine rmarin(ij,kl)
*     -----------------
* initializing routine for ranmar, must be called before generating
* any pseudorandom numbers with ranmar. the input values should be in
* the ranges 0<=ij<=31328 ; 0<=kl<=30081
      implicit real*8(a-h,o-z)
      common/ raset1 / ranu(97),ranc,rancd,rancm
      common/ raset2 / iranmr,jranmr
      save /raset1/,/raset2/
* this shows correspondence between the simplified input seeds ij, kl
* and the original marsaglia-zaman seeds i,j,k,l.
* to get the standard values in the marsaglia-zaman paper (i=12,j=34
* k=56,l=78) put ij=1802, kl=9373
      i = mod( ij/177 , 177 ) + 2
      j = mod( ij     , 177 ) + 2
      k = mod( kl/169 , 178 ) + 1
      l = mod( kl     , 169 )
      do 300 ii = 1 , 97
        s =  0d0
        t = .5d0
        do 200 jj = 1 , 24
          m = mod( mod(i*j,179)*k , 179 )
          i = j
          j = k
          k = m
          l = mod( 53*l+1 , 169 )
          if(mod(l*m,64) .ge. 32) s = s + t
          t = .5d0*t
  200   continue
        ranu(ii) = s
  300 continue
      ranc  =   362436d0 / 16777216d0
      rancd =  7654321d0 / 16777216d0
      rancm = 16777213d0 / 16777216d0
      iranmr = 97
      jranmr = 33
      end
*
* The histo-handlers, makes easy interface between user routine 'bino'
* and general histogram manipulator 'ghiman'.
* 
* 'histoi' : sets up histogram 'idhis' with minimum bin value 'bmin',
*            maximum binvalue 'bmax' and number of bins 'nbin'
* 'histoa' : make specific entry in histogram 'idhis' for value 'val' and 
*            weight 'wgt'
* 'histoe' : calculate error request for histogram 'idhis', pipe through
* 'histow' : output request for histogram 'idhis', pipe through
* 'histows' : output request for histogram 'idhis', pipe through - sharp edge
*
      subroutine histoi(idhis,bmin,bmax,nbin)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* histogram initialization
*
      call ghiman(0,idhis,nbin,0d0,0d0)
      hmin(idhis)=bmin
      ibin(idhis)=nbin
      hwidth(idhis)=(bmax-bmin)/nbin
*
      return
      end
*
      subroutine histoa(idhis,val,wgt)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* histogram entry
*
      iloc=1+int((val-hmin(idhis))/hwidth(idhis))
      call ghiman(1,idhis,iloc,wgt,0d0)
*
      return
      end
*
      subroutine histoe(istat,idhis)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* event errors request, pipe through with correct 'ghiman' call
*
      call ghiman(istat,idhis,ibin(idhis),0d0,0d0)
*
      return
      end
*
      subroutine histow(idhis)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* output request, pipe through with correct 'ghiman' call
*
      call ghiman(4,idhis,ibin(idhis),hmin(idhis),hwidth(idhis))
*
      return
      end
*
      subroutine histows(idhis)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /hispar/hmin,hwidth,ibin 
      dimension hmin(nhisto),hwidth(nhisto),ibin(nhisto)
*
* output request, pipe through with correct 'ghiman' call
*
      call ghiman(5,idhis,ibin(idhis),hmin(idhis),hwidth(idhis))
*
      return
      end
*
* General histogram manipulator, has no knowledge about specific histograms.
*
* Maximum number of histograms is given by the nhisto parameter.
* Maximum number of bins       is given by the maxbin parameter.
*
* istat = 0 : set histogram 'idhis' to value 'entry1' from bin 1 to bin 'iloc'
*             and resets sweep counter 'itmx' to zero.
* istat = 1 : add in histogram 'idhis' weight 'entry1' in  bin location 'iloc'.
* istat = 2 : accumulate event errors in histogram 'idhis' from bin 1 to bin
*             'iloc' for this sweep and increases sweepcounter 'itmx' by 1.
* istat = 3 : calculate standard deviation of the 'itmx' sweeps per bin in 
*             histogram 'idhis' from bin 1 to bin 'iloc' as monte carlo error
*             estimate.
* istat = 4 : write final output to screen in histogram 'idhis' from bin 1 to 
*             bin 'iloc' with offset 'entry1' and binwidth 'entry2'
*            (format: 
*         from bin_number = 1 to 'iloc'  
*            write 'entry1'+'entry2'*(bin_number - 0.5), bin_value, bin_error
*         endfrom)
* istat = 5 : write final output to screen in histogram 'idhis' from bin 1 to 
*             bin 'iloc' with offset 'entry1' and binwidth 'entry2'
*            (format: 
*         from bin_number = 1 to 'iloc'  
*            write 'entry1'+'entry2'*(bin_number - 1), bin_value, bin_error
*         endfrom)
*
      subroutine ghiman(istat,idhis,iloc,entry1,entry2)
      implicit double precision (a-h,o-z)
      parameter(nhisto=100,maxbin=250)
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     .                ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      dimension bin(nhisto,4,maxbin)
*
      if ((idhis.lt.1).or.(idhis.gt.nhisto)) return
      if ((iloc .lt.1).or.(iloc .gt.maxbin)) return
*
*       init histograms
*
      if (istat.eq.0) then
          do i=1,iloc
           do j=1,4
               bin(idhis,j,i)=entry1
            enddo
         enddo
      endif
*
* write event into histogram
*
      if (istat.eq.1) then
         if ((iloc.ge.1).and.(iloc.le.maxbin)) then
            bin(idhis,1,iloc)=bin(idhis,1,iloc)+entry1
            bin(idhis,4,iloc)=bin(idhis,4,iloc)+1d0
         endif
      endif
*
* accumulate event errors
*
      if (istat.eq.2) then
         do i=1,iloc
            bin(idhis,2,i)=bin(idhis,2,i)+bin(idhis,1,i)**2
            bin(idhis,3,i)=bin(idhis,3,i)+bin(idhis,1,i)
            bin(idhis,1,i)=0d0
         enddo
      endif
*
* calculate event errors
*
      if (istat.eq.3) then
         do i=1,iloc
            bin(idhis,2,i)=
     .  sqrt((bin(idhis,2,i)/itmax2-(bin(idhis,3,i)/itmax2)**2)
     .        /float(itmax2-1))
         enddo
      endif
*
* output distributions
*
      if (istat.eq.4) then
         sum=0d0
         do i=1,iloc
            x=bin(idhis,3,i)/itmax2
	    sum=sum+x
            x2=bin(idhis,2,i)
            nn=bin(idhis,4,i)
            y=entry1+entry2*(dfloat(i)-.5d0)
            write(6,101) y,x/entry2,x2/entry2  

 101        format(3x,4g12.5,i9)
         enddo
      endif
*
* output distributions
*
      if (istat.eq.5) then
         sum=0d0
         do i=1,iloc
            x=bin(idhis,3,i)/itmax2
	    sum=sum+x
            x2=bin(idhis,2,i)
            nn=bin(idhis,4,i)
            y=entry1+entry2*(dfloat(i)-1d0)
            write(6,201) y,x/entry2,x2/entry2  

 201        format(3x,4g12.5,i9)
         enddo
      endif
*     
      return
      end
c*********************************************************************** 

c
c two loop strong coupling constant at scale rq
c
       real*8 function alfas(mu,lam,nloops)   
c      alfas in terms of lambda of four flavors for one or two loops.
c      matching acheived using renorm group eqn. approximately 

c      above mu=mb,mu=mt
       implicit none
       integer nloops
       real*8 mu,lam,mc,mb,mt
       real*8 b4,b4p,b5,b5p,b6,b6p,one,two
       real*8 atinv,abinv,atinv1,abinv1,asinv,xqc,xqb,xqt,xb,xt
       parameter(one=1.d0,two=2.d0)
       parameter(b4=1.326291192d0,b5=1.220187897d0,b6=1.114084601d0)    
       parameter(b4p=0.490197225d0,b5p=0.401347248d0,b6p=0.295573466d0)
c      b=(33.d0-2.d0*nf)/6.d0/pi       
c      bp=(153.d0-19.d0*nf)/(2.d0*pi*(33.d0-2.d0*nf))
       parameter(mc=1.5d0,mb=5.0d0,mt=175.d0)
*
       if (mu.lt.mc) then 
            write(6,*) 'unimplemented, mu too low'
            alfas=0.d0
            return
       endif
       xb=log(mb/lam)       
       abinv=b4*xb      
       if (mu.lt.mb) then
            xqc=log(mu/lam)  
            asinv=b4*xqc         
            alfas=one/asinv
       elseif (mu.lt.mt) then
            xqb=log(mu/mb)      
            asinv=abinv+b5*xqb
            alfas=one/asinv         
       else
            xqt=log(mu/mt)      
            xt=log(mt/mb)       
            atinv=abinv+b5*xt      
            asinv=atinv+b6*xqt
            alfas=one/asinv         
       endif
       if (nloops.eq.1) return         
       abinv1=abinv/(one-b4p*log(two*xb)/abinv)         
       if (mu.lt.mb) then
           asinv=asinv/(one-b4p*log(two*xqc)/asinv)
           alfas=one/asinv
       elseif (mu.lt.mt) then
           asinv=abinv1+b5*xqb+b5p*log((asinv+b5p)/(abinv+b5p))
           alfas=one/asinv
       else         
           atinv1=abinv1+b5*xt+b5p*log((b5p+atinv)/(b5p+abinv))
           asinv=atinv1+b6*xqt+b6p*log((b6p+asinv)/(atinv+b6p))
           alfas=one/asinv         
       endif    
       return
       end
*
************************************************************************ 
*
*
* structure function library
*
* computes parton densities and places them in f1,f2, fx1 and fx2
* according to partonid
*
      subroutine parden(nloop)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /phypar/ w,ipp1,ipp2,rmw,rgw,rmz,rgz,sw2,qcdl
      common /fraction/ rm,x1,x2,facscale,renscale,als
      common /inppar/ njets,mloop,istruc,irenorm,ifact,crenorm,cfact,
     .                ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /thcut/ smin,s0
      common /structure/ f1(13),f2(13),fx1(13),fx2(13)
      common /partonid/ iu,id,is,ic,ib,it,iub,idb,isb,icb,ibb,itb,ig,
     .                  iq(6),ip(6),il(6)
*
      call struct(x1,facscale,istruc,upv1,dnv1,ups1,dns1
     .                              ,str1,chm1,bot1,glu1)
      call struct(x2,facscale,istruc,upv2,dnv2,ups2,dns2
     .                              ,str2,chm2,bot2,glu2)
*
* x1-hadron content  (ipp1=0 -> proton, ipp1=1 -> antiproton)
*
      if (ipp1.eq.0) then
        f1(iu )=upv1+ups1
        f1(iub)=ups1
	f1(id )=dnv1+dns1
	f1(idb)=dns1
      else
        f1(iu )=ups1
        f1(iub)=upv1+ups1
	f1(id )=dns1
	f1(idb)=dnv1+dns1
      endif
      f1(is )=str1
      f1(isb)=str1
      f1(ic )=chm1
      f1(icb)=chm1
      f1(ib )=bot1
      f1(ibb)=bot1
      f1(it )=0d0
      f1(itb)=0d0
      f1(ig )=glu1
*
* x2-hadron content (ipp2=0 -> proton, ipp2=1 -> anti proton)
*
      if (ipp2.eq.0) then
        f2(iu )=upv2+ups2
        f2(iub)=ups2
	f2(id )=dnv2+dns2
	f2(idb)=dns2
      else
        f2(iu )=ups2
        f2(iub)=upv2+ups2
	f2(id )=dns2
	f2(idb)=dnv2+dns2
      endif
      f2(is )=str2
      f2(isb)=str2
      f2(ic )=chm2
      f2(icb)=chm2
      f2(ib )=bot2
      f2(ibb)=bot2
      f2(it )=0d0
      f2(itb)=0d0
      f2(ig )=glu2
*     
* fill the xing functions (only if nloop=1)
*
      if (nloop.eq.1) then
      call cstruct(x1,facscale,istruc
     .                        ,upa1,dna1,usa1,dsa1,sta1,cha1,boa1,gla1
     .                        ,upb1,dnb1,usb1,dsb1,stb1,chb1,bob1,glb1)
      call cstruct(x2,facscale,istruc
     .                        ,upa2,dna2,usa2,dsa2,sta2,cha2,boa2,gla2
     .                        ,upb2,dnb2,usb2,dsb2,stb2,chb2,bob2,glb2)
*
* x1-xing function
*
         rlg=log(smin/facscale**2)
         al=1.5d0*als/pi
         upv1=al*(rlg*upa1+upb1)
         dnv1=al*(rlg*dna1+dnb1)
         ups1=al*(rlg*usa1+usb1)
         dns1=al*(rlg*dsa1+dsb1)
         str1=al*(rlg*sta1+stb1)
         chm1=al*(rlg*cha1+chb1)
         bot1=al*(rlg*boa1+bob1)
         glu1=al*(rlg*gla1+glb1)
*
         upv2=al*(rlg*upa2+upb2)
         dnv2=al*(rlg*dna2+dnb2)
         ups2=al*(rlg*usa2+usb2)
         dns2=al*(rlg*dsa2+dsb2)
         str2=al*(rlg*sta2+stb2)
         chm2=al*(rlg*cha2+chb2)
         bot2=al*(rlg*boa2+bob2)
         glu2=al*(rlg*gla2+glb2)
*
      if (ipp1.eq.0) then
        fx1(iu )=upv1+ups1
        fx1(iub)=ups1
	fx1(id )=dnv1+dns1
	fx1(idb)=dns1
      else
        fx1(iu )=ups1
        fx1(iub)=upv1+ups1
	fx1(id )=dns1
	fx1(idb)=dnv1+dns1
      endif
      fx1(is )=str1
      fx1(isb)=str1
      fx1(ic )=chm1
      fx1(icb)=chm1
      fx1(ib )=bot1
      fx1(ibb)=bot1
      fx1(it )=0d0
      fx1(itb)=0d0
      fx1(ig )=glu1
*
* x2-hadron content (ipp2=0 -> proton, ipp2=1 -> anti proton)
*
      if (ipp2.eq.0) then
        fx2(iu )=upv2+ups2
        fx2(iub)=ups2
	fx2(id )=dnv2+dns2
	fx2(idb)=dns2
      else
        fx2(iu )=ups2
        fx2(iub)=upv2+ups2
	fx2(id )=dns2
	fx2(idb)=dnv2+dns2
      endif
      fx2(is )=str2
      fx2(isb)=str2
      fx2(ic )=chm2
      fx2(icb)=chm2
      fx2(ib )=bot2
      fx2(ibb)=bot2
      fx2(it )=0d0
      fx2(itb)=0d0
      fx2(ig )=glu2
      endif
*
      end
*
************************************************************************ 
*
      SUBROUTINE VEGASA(ISTAT,FXN,AVGI,SD,CHI2A)
C
C   SUBROUTINE PERFORMS N-DIMENSIONAL MONTE CARLO INTEG'N
C      - BY G.P. LEPAGE   SEPT 1976/(REV)APR 1978
C
      IMPLICIT REAL*8(A-H,O-Z)
      external FXN
      COMMON/BVEGA/NDIM,NCALL,NPRN
      DIMENSION XI(50,10),D(50,10),DI(50,10),XIN(50),R(50),DT(10),X(10)
     1   ,KG(10),IA(10)
      REAL*8 QRAN(10)
      DATA NDMX/50/,ALPH/1.5D0/,ONE/1D0/,MDS/1/
C
      if(istat.eq.1.or.istat.eq.2)then
c
c         initialize cumulative variables 
c
        IT=0
        SI  =0d0
        SI2 =0d0
        SWGT=0d0
        SCHI=0d0
        ND=NDMX
        NG=1
        IF(MDS.ne.0)then 

          NG=(NCALL/2.)**(1./NDIM)
          MDS=1
          IF((2*NG-NDMX).ge.0)then
            MDS=-1
            NPG=NG/NDMX+1
            ND=NG/NPG
            NG=NPG*ND
          endif
        endif
        K=NG**NDIM
        NPG=NCALL/K
        IF(NPG.LT.2) NPG=2
        CALLS=NPG*K
        DXG=ONE/NG
        DV2G=(CALLS*DXG**NDIM)**2/NPG/NPG/(NPG-ONE)
        XND=ND
        NDM=ND-1
        DXG=DXG*XND
        XJAC=ONE/CALLS
      IF(NPRN.NE.0) WRITE(6,200) NDIM,CALLS,IT,ITMX,MDS,ND
      endif
      if(istat.eq.1)then
C
C   construct uniform grid  
C
        RC=1d0/XND
        DO  J=1,NDIM
          xi(1,j)=1d0
          K=0
          XN=0d0
          DR=0d0
          I=0
4         K=K+1
          DR=DR+ONE
          XO=XN
          XN=XI(K,J)
5         IF(RC.GT.DR) GO TO 4
          I=I+1
          DR=DR-RC
          XIN(I)=XN-(XN-XO)*DR
          IF(I.LT.NDM) GO TO 5
          DO  I=1,NDM
            XI(I,J)=XIN(I)
          enddo
          XI(ND,J)=ONE
        enddo
        NDO=ND
        return
C
      elseif(istat.eq.2)then
C
C   rescale refined grid to new ND value - preserve bin density
C
        if(nd.ne.ndo)then
          RC=NDO/XND
          DO  J=1,NDIM
            K=0
            XN=0d0
            DR=0d0
            I=0
6           K=K+1
            DR=DR+ONE
            XO=XN
            XN=XI(K,J)
7           IF(RC.GT.DR) GO TO 6
            I=I+1
            DR=DR-RC
            XIN(I)=XN-(XN-XO)*DR
            IF(I.LT.NDM) GO TO 7
            DO  I=1,NDM
              XI(I,J)=XIN(I)
            enddo
            XI(ND,J)=ONE
          enddo
        endif
C
        return
C
      elseif(istat.eq.3.or.istat.eq.4)then
c
c    main integration loop
c         

        IT=IT+1
        TI =0d0
        TSI=0d0
        DO J=1,NDIM
          KG(J)=1
          DO I=1,ND
           D (I,J)=0d0
           DI(I,J)=0d0
          enddo
        enddo
C
11      FB=0d0
        F2B=0d0
        K=0
12      K=K+1
        do j=1,ndim
          qran(j)=rn(1.)
        enddo        
        WGT=XJAC
        DO J=1,NDIM
          XN=(KG(J)-QRAN(J))*DXG+ONE
          IA(J)=XN
          IF(IA(J).GT.1)then
            XO=XI(IA(J),J)-XI(IA(J)-1,J)
            RC=XI(IA(J)-1,J)+(XN-IA(J))*XO
          else
            XO=XI(IA(J),J)
            RC=(XN-IA(J))*XO
          endif
          X(J)=RC
          WGT=WGT*XO*XND
        enddo
C
        F=WGT
        F=F*FXN(X,WGT)
        F2=F*F
        FB=FB+F
        F2B=F2B+F2
        DO J=1,NDIM
          DI(IA(J),J)=DI(IA(J),J)+F
          IF(MDS.GE.0) D(IA(J),J)=D(IA(J),J)+F2
        enddo
        IF(K.LT.NPG) GO TO 12
C
        F2B=DSQRT(F2B*NPG)
        F2B=(F2B-FB)*(F2B+FB)
        TI=TI+FB
        TSI=TSI+F2B
        IF(MDS.lt.0) then
          DO J=1,NDIM
            D(IA(J),J)=D(IA(J),J)+F2B
          enddo
        endif
        K=NDIM
19      KG(K)=MOD(KG(K),NG)+1
        IF(KG(K).NE.1) GO TO 11
        K=K-1
        IF(K.GT.0) GO TO 19
C
C   FINAL RESULTS FOR THIS ITERATION
C
        TSI=TSI*DV2G
        TI2=TI*TI
        WGT=TI2/TSI
        SI=SI+TI*WGT
        SI2=SI2+TI2
        SWGT=SWGT+WGT
        SCHI=SCHI+TI2*WGT
        AVGI=SI/SWGT
        SD=SWGT*IT/SI2
        CHI2A=SD*(SCHI/SWGT-AVGI*AVGI)/(IT-.999)
        SD=DSQRT(ONE/SD)
C
        IF(NPRN.ne.0) then
          TSI=DSQRT(TSI)
          WRITE(6,201) IT,TI,TSI,AVGI,SD,CHI2A
        endif
        IF(NPRN.lt.0) then
          DO J=1,NDIM
            WRITE(6,202) J,(XI(I,J),DI(I,J),D(I,J),I=1,ND)
          enddo
        endif
      endif
C
C   REFINE GRID
C
      if(istat.eq.3)then
       DO J=1,NDIM
         XO=D(1,J)
         XN=D(2,J)
         D(1,J)=(XO+XN)/2.
         DT(J)=D(1,J)
         DO I=2,NDM
           D(I,J)=XO+XN
           XO=XN
           XN=D(I+1,J)
           D(I,J)=(D(I,J)+XN)/3.
           DT(J)=DT(J)+D(I,J)
        enddo
        D(ND,J)=(XN+XO)/2.
      DT(J)=DT(J)+D(ND,J)
      enddo
C
        DO 28 J=1,NDIM
        RC=0.
        DO 24 I=1,ND
        R(I)=0.
        IF(D(I,J).LE.0d0) GO TO 24
        XO=DT(J)/D(I,J)
        R(I)=((XO-ONE)/XO/DLOG(XO))**ALPH
24      RC=RC+R(I)
        RC=RC/XND
        K=0
        XN=0.
        DR=XN
        I=K
25      K=K+1
        DR=DR+R(K)
        XO=XN
        XN=XI(K,J)
26      IF(RC.GT.DR) GO TO 25
        I=I+1
        DR=DR-RC
        XIN(I)=XN-(XN-XO)*DR/R(K)
        IF(I.LT.NDM) GO TO 26
        DO 27 I=1,NDM
27      XI(I,J)=XIN(I)
28      XI(ND,J)=ONE
        return
      endif
C
200   FORMAT(///' INPUT PARAMETERS FOR VEGAS:  NDIM=',I3,'  NCALL=',
     1    F8.0/28X,'  IT=',I5,'  ITMX=',I5
     2    /28X,'  MDS=',I3,'   ND=',I4)
201   FORMAT(i4,4x,g15.7,g11.4,g15.7,g11.4,g11.4)
202   FORMAT(' DATA FOR AXIS',I2 / ' ',6X,'X',7X,'  DELT I  ',
     1    2X,' CONV''CE  ',11X,'X',7X,'  DELT I  ',2X,' CONV''CE  '
     2   ,11X,'X',7X,'  DELT I  ',2X,' CONV''CE  ' /
     2    (' ',3G12.4,5X,3G12.4,5X,3G12.4))
      RETURN
      END
      SUBROUTINE VEGASB(ISTAT,FXN,AVGI,SD,CHI2A)
C
C   SUBROUTINE PERFORMS N-DIMENSIONAL MONTE CARLO INTEG'N
C      - BY G.P. LEPAGE   SEPT 1976/(REV)APR 1978
C
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/BVEGB/NDIM,NCALL,NPRN
      DIMENSION XI(50,10),D(50,10),DI(50,10),XIN(50),R(50),DT(10),X(10)
     1   ,KG(10),IA(10)
      REAL*8 QRAN(10)
      external FXN
      DATA NDMX/50/,ALPH/1.5D0/,ONE/1D0/,MDS/1/
C
      if(istat.eq.1.or.istat.eq.2)then
c
c         initialize cumulative variables 
c
        IT=0
        SI  =0d0
        SI2 =0d0
        SWGT=0d0
        SCHI=0d0
        ND=NDMX
        NG=1
        IF(MDS.ne.0)then 
          NG=(NCALL/2.)**(1./NDIM)
          MDS=1
          IF((2*NG-NDMX).ge.0)then
            MDS=-1
            NPG=NG/NDMX+1
            ND=NG/NPG
            NG=NPG*ND
          endif
        endif
        K=NG**NDIM
        NPG=NCALL/K
        IF(NPG.LT.2) NPG=2
        CALLS=NPG*K
        DXG=ONE/NG
        DV2G=(CALLS*DXG**NDIM)**2/NPG/NPG/(NPG-ONE)
        XND=ND
        NDM=ND-1
        DXG=DXG*XND
        XJAC=ONE/CALLS
      IF(NPRN.NE.0) WRITE(6,200) NDIM,CALLS,IT,ITMX,MDS,ND
      endif
      if(istat.eq.1)then
C
C   construct uniform grid  
C
        RC=1d0/XND
        DO  J=1,NDIM
          xi(1,j)=1d0
          K=0
          XN=0d0
          DR=0d0
          I=0
4         K=K+1
          DR=DR+ONE
          XO=XN
          XN=XI(K,J)
5         IF(RC.GT.DR) GO TO 4
          I=I+1
          DR=DR-RC
          XIN(I)=XN-(XN-XO)*DR
          IF(I.LT.NDM) GO TO 5
          DO  I=1,NDM
            XI(I,J)=XIN(I)
          enddo
          XI(ND,J)=ONE
        enddo
        NDO=ND
        return
C
      elseif(istat.eq.2)then
C
C   rescale refined grid to new ND value - preserve bin density
C
        if(nd.ne.ndo)then
          RC=NDO/XND
          DO  J=1,NDIM
            K=0
            XN=0d0
            DR=0d0
            I=0
6           K=K+1
            DR=DR+ONE
            XO=XN
            XN=XI(K,J)
7           IF(RC.GT.DR) GO TO 6
            I=I+1
            DR=DR-RC
            XIN(I)=XN-(XN-XO)*DR
            IF(I.LT.NDM) GO TO 7
            DO  I=1,NDM
              XI(I,J)=XIN(I)
            enddo
            XI(ND,J)=ONE
          enddo
        endif
C
        return
C
      elseif(istat.eq.3.or.istat.eq.4)then
c
c    main integration loop
c         
        IT=IT+1
        TI =0d0
        TSI=0d0
        DO J=1,NDIM
          KG(J)=1
          DO I=1,ND
           D (I,J)=0d0
           DI(I,J)=0d0
          enddo
        enddo
C
11      FB=0d0
        F2B=0d0
        K=0
12      K=K+1
        do j=1,ndim
          qran(j)=rn(1.)
        enddo        
        WGT=XJAC
        DO J=1,NDIM
          XN=(KG(J)-QRAN(J))*DXG+ONE
          IA(J)=XN
          IF(IA(J).GT.1)then
            XO=XI(IA(J),J)-XI(IA(J)-1,J)
            RC=XI(IA(J)-1,J)+(XN-IA(J))*XO
          else
            XO=XI(IA(J),J)
            RC=(XN-IA(J))*XO
          endif
          X(J)=RC
          WGT=WGT*XO*XND
        enddo
C
        F=WGT
        F=F*FXN(X,WGT)
        F2=F*F
        FB=FB+F
        F2B=F2B+F2
        DO J=1,NDIM
          DI(IA(J),J)=DI(IA(J),J)+F
          IF(MDS.GE.0) D(IA(J),J)=D(IA(J),J)+F2
        enddo
        IF(K.LT.NPG) GO TO 12
C
        F2B=DSQRT(F2B*NPG)
        F2B=(F2B-FB)*(F2B+FB)
        TI=TI+FB
        TSI=TSI+F2B
        IF(MDS.lt.0) then
          DO J=1,NDIM
            D(IA(J),J)=D(IA(J),J)+F2B
          enddo
        endif
        K=NDIM
19      KG(K)=MOD(KG(K),NG)+1
        IF(KG(K).NE.1) GO TO 11
        K=K-1
        IF(K.GT.0) GO TO 19
C
C   FINAL RESULTS FOR THIS ITERATION
C
        TSI=TSI*DV2G
        TI2=TI*TI
        WGT=TI2/TSI
        SI=SI+TI*WGT
        SI2=SI2+TI2
        SWGT=SWGT+WGT
        SCHI=SCHI+TI2*WGT
        AVGI=SI/SWGT
        SD=SWGT*IT/SI2
        CHI2A=SD*(SCHI/SWGT-AVGI*AVGI)/(IT-.999)
        SD=DSQRT(ONE/SD)
C
        IF(NPRN.ne.0) then
          TSI=DSQRT(TSI)
          WRITE(6,201) IT,TI,TSI,AVGI,SD,CHI2A
        endif
        IF(NPRN.lt.0) then
          DO J=1,NDIM
            WRITE(6,202) J,(XI(I,J),DI(I,J),D(I,J),I=1,ND)
          enddo
        endif
      endif
C
C   REFINE GRID
C
      if(istat.eq.3)then
       DO J=1,NDIM
         XO=D(1,J)
         XN=D(2,J)
         D(1,J)=(XO+XN)/2.
         DT(J)=D(1,J)
         DO I=2,NDM
           D(I,J)=XO+XN
           XO=XN
           XN=D(I+1,J)
           D(I,J)=(D(I,J)+XN)/3.
           DT(J)=DT(J)+D(I,J)
        enddo
        D(ND,J)=(XN+XO)/2.
      DT(J)=DT(J)+D(ND,J)
      enddo
C
        DO 28 J=1,NDIM
        RC=0.
        DO 24 I=1,ND
        R(I)=0.
        IF(D(I,J).LE.0d0) GO TO 24
        XO=DT(J)/D(I,J)
        R(I)=((XO-ONE)/XO/DLOG(XO))**ALPH
24      RC=RC+R(I)
        RC=RC/XND
        K=0
        XN=0.
        DR=XN
        I=K
25      K=K+1
        DR=DR+R(K)
        XO=XN
        XN=XI(K,J)
26      IF(RC.GT.DR) GO TO 25
        I=I+1
        DR=DR-RC
        XIN(I)=XN-(XN-XO)*DR/R(K)
        IF(I.LT.NDM) GO TO 26
        DO 27 I=1,NDM
27      XI(I,J)=XIN(I)
28      XI(ND,J)=ONE
        return
      endif
C
200   FORMAT(///' INPUT PARAMETERS FOR VEGAS:  NDIM=',I3,'  NCALL=',
     1    F8.0/28X,'  IT=',I5,'  ITMX=',I5
     2    /28X,'  MDS=',I3,'   ND=',I4)
201   FORMAT(i4,4x,g15.7,g11.4,g15.7,g11.4,g11.4)
202   FORMAT(' DATA FOR AXIS',I2 / ' ',6X,'X',7X,'  DELT I  ',
     1    2X,' CONV''CE  ',11X,'X',7X,'  DELT I  ',2X,' CONV''CE  '
     2   ,11X,'X',7X,'  DELT I  ',2X,' CONV''CE  ' /
     2    (' ',3G12.4,5X,3G12.4,5X,3G12.4))
      RETURN
      END


