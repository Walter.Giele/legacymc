class StrongCoupling
{
public:
  StrongCoupling(){};
  StrongCoupling(const char* filename);
  double operator()(double Q);
  void print(const int N=10,const double Q0=1.0,const double Qmax=100.0);
private:
  double B(const double x);
  double DB(const double x);
  double rtnewt(const double x1,const double xc,const double x2,
		const double offs=0.0,const double xacc=1e-10,const int jmax=100);
  double mu2,mc2,mb2,m02,as,as_mc,as_mb,L,Order;
  double b0[3],b1[3],b2[3];
  int dnf;
};

StrongCoupling::StrongCoupling(const char* filename)
{
  using namespace mathsupport;
  Order=1;
  double Refalphas=0.1187;
  double RefScale=91.1876;
  double asm0=1.0;
  double asmc=1.3;
  double asmb=4.2;
  double Ca=3.0;
  double Cf=4.0/3.0;
  TiXmlDocument init(filename);init.LoadFile();
  
  TiXmlElement* parms=init.FirstChildElement("vircol_init_data")
                          ->FirstChildElement("physics_parameters")
                          ->FirstChildElement("alpha_S");
  if (parms!=0) {
    TiXmlElement* tag;
    tag=parms->FirstChildElement("order_evolution");
    if (tag!=0) tag->Attribute("OrderAlphas",&Order);
    tag=parms->FirstChildElement("alphaS_at_reference_scale");
    if (tag!=0) tag->Attribute("Refalphas",&Refalphas);
    if (tag!=0) tag->Attribute("RefScale",&RefScale);
    tag=parms->FirstChildElement("flavortresholds");
    if (tag!=0) tag->Attribute("M0",&asm0);
    if (tag!=0) tag->Attribute("Mc",&asmc);
    if (tag!=0) tag->Attribute("Mb",&asmb);
    tag=parms->FirstChildElement("casimirs");
    if (tag!=0) tag->Attribute("Ca",&Ca);
    if (tag!=0) {tag->Attribute("Cf_times_Ca",&Cf);Cf=Cf/Ca;}}
  int nf;
  as=Refalphas;
  mu2=RefScale*RefScale;
  mc2=asmc*asmc;mb2=asmb*asmb;m02=asm0*asm0;
  for (nf=3;nf<=5;nf++) 
    {b0[nf-3]=0.0;b1[nf-3]=0.0;b2[nf-3]=0.0;}
  if (Order>=1) 
    for (nf=3;nf<=5;nf++) 
      b0[nf-3]=(11.0*Ca-2.0*nf)/(12.0*pi);
  if (Order>=2) 
    for (nf=3;nf<=5;nf++) 
      b1[nf-3]=(17.0*Ca*Ca-(5.0*Ca+3.0*Cf)*nf)/(2.0*pi*(11.0*Ca-2.0*nf));
  if (Order>=3) 
    for (nf=3;nf<=5;nf++) 
      b2[nf-3]=(2857.0*Ca*Ca*Ca+(54.0*Cf*Cf-615.0*Cf*Ca-1415.0*Ca*Ca)*nf
	      +(66.0*Cf+79.0*Ca)*nf*nf)/(288.0*pi*pi*(11.0*Ca-2.0*nf));
  dnf=5;
  L=B(as)-log(mb2/mu2);
  as_mb=rtnewt(as,0.2,5.0,L);
  dnf=4;
  L=B(as_mb)-log(mc2/mb2);
  as_mc=rtnewt(as_mb,0.3,5.0,L);
};

double StrongCoupling::operator()(double Q)
{
  double Q2=Q*Q;
  Q2=max(m02,Q2);
  if (Q2<=mc2) 
    {
      dnf=3;
      L=log(Q2/mc2);
      double asQ=as_mc/(1.0+as_mc*b0[dnf-3]*L);
      L=B(as_mc)-L;
      return rtnewt(as_mc,asQ,5.0,L);
    }
  if (Q2<=mb2)
    {
      dnf=4;
      L=log(Q2/mb2);
      double asQ=as_mb/(1.0+as_mb*b0[dnf-3]*L);
      L=B(as_mb)-L;
      return rtnewt(as_mb,asQ,as_mc,L);
    }
  dnf=5;
  L=log(Q2/mu2);
  double asQ=as/(1.0+as*b0[dnf-3]*L);
  L=B(as)-L;
  return rtnewt(1e-5,asQ,as_mb,L);
}

double StrongCoupling::B(const double x)
{
  double beta=-1.0/(b0[dnf-3]*x);
  if (b1[dnf-3]!=0.0)
    beta+=b1[dnf-3]*log((1+b1[dnf-3]*x)/x)/b0[dnf-3];
  if (b2[dnf-3]!=0.0)
    {
      double s=sqrt(4.0*b2[dnf-3]-b1[dnf-3]*b1[dnf-3]);
      double p=(b1[dnf-3]*b1[dnf-3]-2.0*b2[dnf-3])/s;
      beta+=p*atan((b1[dnf-3]+2.0*b2[dnf-3]*x)/s)/b0[dnf-3];
      beta+=b1[dnf-3]/b0[dnf-3]*log(sqrt(1.0+b1[dnf-3]*x+b2[dnf-3]*x*x)/(1.0+b1[dnf-3]*x));
    }
  return beta;
}

double StrongCoupling::DB(const double x)
{
  return 1.0/(b0[dnf-3]*x*x*(1+b1[dnf-3]*x+b2[dnf-3]*x*x));
}

double StrongCoupling::rtnewt(const double x1,const double xc,const double x2,
				const double offs,const double xacc,const int jmax)
{
  int j;
  double rtn=xc;
  for (j=0;j<jmax;j++)
    {
      double f=B(rtn)-offs;
      double df=DB(rtn);
      double dx=f/df;
      rtn-=dx;
      if ((x1-rtn)*(rtn-x2)<0.0)
	cout<<"Jumped out of brackets in rtnewt: ["<<x1<<","<<rtn<<","<<x2<<"]"<<endl;
      if (fabs(dx)<xacc) return rtn;
    }
  cout<<"Maximum number of iterations exceeded in rtnewt"<<endl;
  return 0.0;
}
void StrongCoupling::print(const int N,const double Q0,const double Qmax)
{
  double delta=pow(Qmax/Q0,1.0/N);
  for (int i=0;i<=N;i++)
    {
      double Q=Q0*pow(delta,i);
      cout<<"      alpha_S("<<Q<<")= "<<operator()(Q)<<endl;
    }
}

