#include "header.cc"

int main()
{
  Event DipoleShower("VirCol.xml");
  double Qinit=500.0,Qhad=1.0;
  DipoleShower.Generate(Qinit,Qhad,"Sample2g.xml");
  cout<<endl<<endl;
  for (double Qresolution=10.0;Qresolution>=0.0;Qresolution-=1.0)
    DipoleShower.Print(Qresolution);
  cout<<"Final Event:"<<endl;
  DipoleShower.Print();
};
