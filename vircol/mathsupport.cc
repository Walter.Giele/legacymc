namespace mathsupport
{
  const double pi=3.14159265359;
  double Li2(const double x,const int kmax=100,const double xerr=1e-10)
  {
    if (x<=0.0) return 0.0;
    if (x<=0.5)
      {
	double sum  = x;
	double term = x;
	for(int k=2; k<kmax; k++) 
	  {
	    double rk = (k-1.0)/k;
	    term *= x;
	    term *= rk*rk;
	    sum += term;
	    if (fabs(term/sum) < xerr) return sum;
	  }
	cout<<"Maximum number of iterations exceeded in Li2"<<endl;
	return sum;
      }
    if (x<1.0) 
      return pi*pi/6.0-Li2(1.0-x)-log(x)*log(1.0-x);
    if (x==1.0) 
      return pi*pi/6.0;
    if (x<=1.01)
      {
	const double eps = x - 1.0;
	const double lne = log(eps);
	const double c0 = pi*pi/6.0;
	const double c1 =   1.0 - lne;
	const double c2 = -(1.0 - 2.0*lne)/4.0;
	const double c3 =  (1.0 - 3.0*lne)/9.0;
	const double c4 = -(1.0 - 4.0*lne)/16.0;
	const double c5 =  (1.0 - 5.0*lne)/25.0;
	const double c6 = -(1.0 - 6.0*lne)/36.0;
	const double c7 =  (1.0 - 7.0*lne)/49.0;
	const double c8 = -(1.0 - 8.0*lne)/64.0;
	return c0+eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*(c6+eps*(c7+eps*c8)))))));
      }
    double log_x=log(x);
    if (x<=2.0) 
      return pi*pi/6.0+Li2(1.0 - 1.0/x)-log_x*(log(1.0-1.0/x)+0.5*log_x);
    return pi*pi/3.0-Li2(1.0/x)-0.5*log_x*log_x;
  }

  // Adapted from numerical recipes:
  //  Solves f(x)=offs on interval x1<=x<=x2
  //  Functions fo be supplied in funcd are f(x) and its derivative

  double rtnewt(void funcd(const double,double &,double &),
		const double x1,const double xc,const double x2,
		const double offs=0.0,const double xacc=1e-15,const int jmax=20)
  {
    int j;
    double df,f;
    double rtn=xc;
    for (j=0;j<jmax;j++)
      {
	funcd(rtn,f,df);
	double dx=f/df;
	rtn-=dx;
	if ((x1-rtn)*(rtn-x2)<0.0)
	  cerr<<"Jumped out of brackets in rtnewt"<<endl;exit(1);
	if (fabs(dx)<xacc) return rtn;
      }
    cerr<<"Maximum number of iterations exceeded in rtnewt"<<endl;exit(1);
    return 0.0;
  }

double uniform() 
{
  double random,s,t;
  int m,seed1=1429,seed2=9343;
  int i=(seed1/177)%177+2,j=(seed1%177)+2,k=(seed2/169)%178+1,l=seed2%169;
  static bool init=false;
  static int iranmr=96,jranmr=32;
  static double ranc=362436.0/16777216.0,rancd=7654321.0/16777216.0,
                rancm=16777213.0/16777216.0,ranu[97];
  if (init==false) 
    {
      init=true;
      for (int il=0;il<97;++il)
	{
	  s=0;t=0.5;
	  for (int jl=0;jl<24;++jl)
	    {
	      m=(((i*j%179)*k)%179);i=j;j=k;k=m;l=(53*l+1)%169;
	      if (((l*m)%64)>=32) s+=t;
	      t*=0.5;
	    };
	  ranu[il]=s;
	};
    };
  random=ranu[iranmr]-ranu[jranmr];
  if (random<0.0) ++random;
  ranu[iranmr--]=random;
  jranmr--;
  if (iranmr<0) iranmr=96;
  if (jranmr<0) jranmr=96;
  ranc-=rancd;
  if (ranc<0.0) ranc+=rancm;
  random-=ranc;
  if (random<0.0) random++;
  return random;  
}

double uniform(double min,double max) {return min+(max-min)*uniform();}
double gaussian()
{
  static int iset=0;
  static double gset;
  double fac,rsq,v1,v2;

  if (iset==0)
    {
      do
	{
	  v1=uniform(-1.0,1.0);
	  v2=uniform(-1.0,1.0);
	  rsq=v1*v1+v2*v2;
	} while (rsq>=1.0||rsq==0.0);
      fac=sqrt(-2.0*log(rsq)/rsq);
      gset=v1*fac;
      iset=1;
      return v2*fac;
    }
  else
    {
      iset=0;
      return gset;
    }
};

double gaussian(double average,double width){return average+width*gaussian();}
double gaussian(double average,double width,double min,double max)
{
  double r=0.0;
  while (r==0.0)
    {
      r=gaussian(average,width);
      if (r<min||r>max) r=0.0;
    }
  return r;
};

}
