// Error functions
void ErrorSubscriptRange(string, int);
void ErrorDivideZero(string);  
void ErrorInitXml(string,string);  
void ErrorInitXml(string,string,int);  

// vec4 class. (Adapted from Pythia8::Vec4)
// This class implements four-vectors, in energy-momentum space.
// (But could equally well be used to hold space-time four-vectors.)

class vec4 {

public:
  // Constructors.
  vec4(double xIn = 0., double yIn = 0., double zIn = 0., double eIn = 0.)
    { p[1]=xIn; p[2]=yIn; p[3]=zIn; p[0]=eIn; }
  vec4(const vec4& v) 
    { p[1]=v.p[1]; p[2]=v.p[2]; p[3]=v.p[3]; p[0]=v.p[0]; }
  vec4& operator=(const vec4& v) {
    if (this != &v) {
      p[1] = v.p[1]; p[2] = v.p[2]; p[3] = v.p[3]; p[0] = v.p[0];}
    return *this; }

  // public members (4-vector components, both for input and output)
  double p[4];

  // Additional member functions for output.
  double m2() const {return p[0]*p[0] - p[1]*p[1] - p[2]*p[2] - p[3]*p[3];}
  double m() const {double temp = m2();
    return (temp >= 0) ? sqrt(temp) : -sqrt(-temp);}
  double pT2() const {return p[1]*p[1] + p[2]*p[2];}
  double pT() const {return sqrt(pT2());}
  double pAbs2() const {return p[1]*p[1] + p[2]*p[2] + p[3]*p[3];}
  double pAbs() const {return sqrt(pAbs2());}
  double theta() const {return atan2(sqrt(p[1]*p[1] + p[2]*p[2]), p[3]);}
  double phi() const {return atan2(p[2],p[1]);}

  // Member functions that perform operations.
  void flip(){p[1] = -p[1]; p[2] = -p[2]; p[3] = -p[3];}
  void rot(double, double); 
  void bst(double, double, double); 
  void bst(const vec4&); 

  // Operator overloading with member functions
  vec4& operator+=(const vec4& v) { 
    for (int i=0;i<=3;i++) {p[i] += v.p[i];} return *this;}
  vec4& operator-=(const vec4& v) {
    for (int i=0;i<=3;i++) {p[i] -= v.p[i];} return *this;}
  vec4& operator*=(double f) {
    for (int i=0;i<=3;i++) {p[i] *= f;} return *this;}
  vec4& operator/=(double f) {
    if (f == 0) { ErrorDivideZero("vec4& operator /="); }
    for (int i=0;i<=3;i++) {p[i] /= f;} return *this;}

  // Subscriting vec4[i] gives (E,p) vector.
  double& operator[](int i) { 
    if (i < 0 || i >= 4) ErrorSubscriptRange("vec4",i);   
    return p[i];
  }
};
