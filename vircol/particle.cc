class particle
{
public:
  // Constructors
  //  particle() : p(Pythia8::Vec4(0.,0.,0.,0.)), m(0.), color(0,0), 
  //  name('Noname'){}
  
  // public members (everything is public)
  vec4 p;
  double m0;
  int id,c,cb;  
  string name;
  bool status;
 
  // Member functions for additional input
  void setcolor(int cIn){c = cIn;}
  void setcolor(int c1In,int c2In){c = c1In;c = c2In;}
  void setanticolor(int cIn){cb = cIn;}
  void setmomentum(double p0,double p1,double p2,double p3)
    {p[0]=p0;p[1]=p1;p[2]=p2;p[3]=p3;} 

  // Member functions for additional output
  int color(){return c;}
  int anticolor(){return cb;}
  double virtuality() {return p.m2()-m0*m0;}
  double* momentum(){return &p[0];}
  double pT() const {return p.pT();}
  double pT2() const {return p.pT2();}
  double mT() const {return sqrt(p.m2() + p.pT2());}
  double mT2() const {return p.m2() + p.pT2();}
  double pAbs() const {return p.pAbs();}
  double pAbs2() const {return p.pAbs2();}
  double theta() const {return p.theta();}
  double phi() const {return p.phi();}
// Functions for rapidity and pseudorapidity.
  double y() {
    double temp = log( ( p[0] + abs(p[3]) ) / max( 1e-20, mT() ) ); 
    return (p[3] > 0) ? temp : -temp;
  }
  double eta() {
    double temp = log( ( p[0] + abs(p[3]) ) / max( 1e-20, pT() ) ); 
    return (p[3] > 0) ? temp : -temp;
  }


  // Allow indexing to get E,p components, ie particle[0] = energy, etc.
  double& operator[](int i) {
    const string name = "particle";
    if (i < 0 || i >= 4) ErrorSubscriptRange(name,i);
    return p[i];
  }

  // Print
  void print(){
    cout<<"Id      : "<<name<<"\n";
    cout<<"Momentum: ("<<p[0]<<","<<p[1]<<","<<p[2]<<","<<p[3]<<")"<<"\n";
    cout<<"Mass    : "<<m0<<"   (Virtuality: "<<virtuality()<<")"<<"\n";
    cout<<"Color   : ("<<color()<<","<<anticolor()<<")"<<"\n";
    return;}

};


// Invariant mass of a pair and its square.
// (Not part of class proper, but tightly linked.)

double m(particle& pp1, particle& pp2) {
  double m2 = pow2(pp1[0] + pp2[0]) - pow2(pp1[1] + pp2[1])
     - pow2(pp1[2] + pp2[2]) - pow2(pp1[3] + pp2[3]);
  return (m2 > 0. ? sqrt(m2) : 0.); 
}

double m2(particle& pp1, particle& pp2) {
  double m2 = pow2(pp1[0] + pp2[0]) - pow2(pp1[1] + pp2[1])
     - pow2(pp1[2] + pp2[2]) - pow2(pp1[3] + pp2[3]);
  return m2;
} 
