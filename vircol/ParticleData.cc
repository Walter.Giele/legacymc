// Function definitions (not found in the header) for 
// the ParticleDataTable class.

#include "ParticleData.h"

namespace Pythia8 {

//**************************************************************************

// ParticleDataEntry class.
// This class holds info on a single particle species.

//*********

// Function to give spin of particle as 2 * s + 1.
// Still does not cover ordinary hadrons??

int ParticleDataEntry::spinType() const {
  
  // Preliminaries and default value.
  const int SUSY = 1000000;
  int spin = 0;

  // Go through various known cases; could be expanded.
       if (idAbs > 0 && idAbs <= 18) spin = 2; 
  else if (idAbs > SUSY && idAbs <= SUSY+18) spin = 1;  
  else if (idAbs > 2*SUSY && idAbs <= 2*SUSY+18) spin = 1; 
  else if (idAbs == 21) spin = 3; 
  else if (idAbs >= 22 && idAbs <= 24) spin = 3; 
  else if (idAbs >= 32 && idAbs <= 34) spin = 3; 
  else if (idAbs == 25 || (idAbs >= 35 && idAbs <= 37) ) spin = 1;
  else if (idAbs == SUSY+21) spin = 2; 
  else if (idAbs >= SUSY+22 && idAbs <= SUSY+37) spin = 2;

  return spin;
}

//*********

// Functions to identify quark or lepton.

bool ParticleDataEntry::isQuark() const { 
  return (idAbs > 0 && idAbs < 9) ? true : false;
}

bool ParticleDataEntry::isLepton() const { 
  return (idAbs > 10 && idAbs < 19) ? true : false;
}

//**************************************************************************

// ParticleDataTable class.
// This class holds a map of all ParticleDataEntries,
// each entry containing info on a particle species.

//*********

// Definitions of static variables. 

map<int, ParticleDataEntry> ParticleDataTable::pdt;
bool ParticleDataTable::isInit = false;

//*********

// Read in database from specific file.

bool ParticleDataTable::init(string startFile) {

  // Don't initialize if it has already been done.
  if (isInit) return true;

  // List of files to be checked.
  vector<string> files;
  files.push_back(startFile);

  // Loop over files. Open them for read.
  for (int i = 0; i < int(files.size()); ++i) {
    const char* cstring = files[i].c_str();
    ifstream is(cstring);  

    // Check that instream is OK.
    if (!is) {cout << "Error: settings file " << files[i] 
       << "not found \n"; return false;}

    // Read in one line at a time.
    string line;
    while ( getline(is, line) ) {

      // Get first word of a line.
      istringstream getfirst(line);
      string word1;
      getfirst >> word1;
    
      // Check for occurence of a particle and add to particle data table.
      if (word1 == "<particle>") {
        istringstream particleData(line);
        string dummy;
        int id;
        string name, antiName;
        int charge3, col;
        double mass;
        particleData >> dummy >> id >> name >> antiName >> charge3 
                     >> col >> mass;   
        if (!particleData) cout << "Error: incomplete particle " << id 
          << "\n";
        else if (isParticle(id)) cout << "Error: particle " << id 
          << " is already defined\n";
        else addParticle( id, name, antiName, charge3, col, mass); 

      // Check for occurence of a file also to be read.
      } else if (word1 == "<file>") {
        istringstream fileData(line);
        string dummy, file;
        fileData >> dummy >> file;
        if (!fileData) cout << "Error: incomplete file " << file << "\n";
        else files.push_back(file);
      }

    // End of loop over lines in input file and loop over files.
    };
  };

  isInit = true;
  return true;
}

//*********

// Overwrite existing database by reading from specific file.

bool ParticleDataTable::reInit(string startFile) {

  // Reset map to empty and then let normal init do the rest.
  pdt.clear();
  isInit = false;
  return init(startFile);

} 

//*********

// Read in updates from user-defined file.

bool ParticleDataTable::read(string updateFile) {

  // Open file with updates.
  const char* cstring = updateFile.c_str();
  ifstream is(cstring);  
  if (!is) {cout << "Error: user update file " << updateFile 
     << "not found \n"; return false;}

  // Read in one line at a time.
  string line;
  while ( getline(is, line) ) {

    // Get first word of a line; check that matches particle.
    istringstream getWord(line);
    string word1;
    int id;
    getWord >> word1 >> id;
    if (getWord && tolower(word1) == "particle" 
      && isParticle(id) && id > 0) {

      // Find quantity and value, if = sign then skip for value
      string quantity, valueString, secondValue;
      while (getWord >> quantity >> valueString) {
        if (valueString == "=") getWord >> valueString;
        if (tolower(quantity) == "name") 
          pdt[id].setName(valueString);
        else if (tolower(quantity) == "antiname") 
          pdt[id].setAntiName(valueString);
        else if (tolower(quantity) == "names") { 
          getWord >> secondValue;
          pdt[id].setNames(valueString, secondValue); }
        else if (tolower(quantity.substr(0,6)) == "charge") {
          istringstream getInt(valueString); 
          int charge3;
          getInt >> charge3;
          pdt[id].setCharge3(charge3); }
        else if (tolower(quantity.substr(0,3)) == "col") {
          istringstream getInt(valueString); 
          int col;
          getInt >> col;
          pdt[id].setCol(col); }
        else if (tolower(quantity) == "mass") {
          istringstream getDouble(valueString); 
          double mass;
          getDouble >> mass;
          pdt[id].setMass(mass); 
	}
      };
    }

  // Reached end of input file.
  };
  return true;
}

//*********
 
// Print out table of database in numerical order.

void ParticleDataTable::list(ostream& os) {

  // Table header; output for bool as off/on.
  os << "\n --------  Pythia Particle Data Table  ---------------------"
     << "------------------- \n \n      id   name            "
     << "antiname    3*charge colour     mass \n";

  // Iterate through the particle data table.
  for (map<int, ParticleDataEntry>::const_iterator pdtEntry 
    = pdt.begin(); pdtEntry != pdt.end(); ++pdtEntry) {
    const ParticleDataEntry* particle = &pdtEntry->second;
    string antiName = (particle->name(-1) == "void") ?
      "                " : particle->name(-1) ;
    os << setw(8) << particle->id() << "   " << setw(16) << std::left 
       << particle->name() << setw(16) << antiName << std::right 
       << setw(4) << particle->charge3() << setw(4) 
       << particle->colType() << std::fixed << std::setprecision(4) 
       << setw(12) << particle->mass() << "\n";
  } ;

  // End of loop over database contents.
  os << "\n --------  End Particle Data Table  ------------------------"
     << "------------------- \n";

}

//**************************************************************************

} // end namespace Pythia8
