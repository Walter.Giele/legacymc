class Event
{
public:
  explicit Event(const char *filename,int v);
#include "particle.cc"
#include "branchelemental.cc"
  bool Initialize(double Qinit,const char* eventfile);
  void Generate(double QrInit,double Qhad,const char* eventfile);
  void Shower(double Qhad);
  double Evolve();
  double Evolve(int Nstep);
  double Evolve(double Qr);
  bool BranchEvent();
  int Resolution2Generation(double Qr2);
  void Hadronize(double Qh);
  int SelectGeneration();
  int SelectGeneration(int n);
  int SelectGeneration(double Qr);
  particle operator()(int k){return E[k];}
  int NParticle(){return Nparticle;}
  int NGeneration(){return Generation;}
  double Resolution(){return sqrt(Q2resolution);}
  void Print();
  void Print(double Qr);
  void list(ostream& = cout);
  int verbose;
private:
#include "branch.cc"
  bool nextdipole(bool newdipole);
  particle* E;
  branchelemental* S;
  StrongCoupling alfaS;
  ParticleDataTable ParticleData;
  int MaxParticle,MaxBranch,Generation,Nparticle,GenMax;
  double tolerance,Q2resolution,Q2hadronization;
  string delimiter;
};

Event::Event(const char *initfile,int v=0):verbose(v)
{
  if (verbose>=0) {
    cout<<endl<<endl<<endl;
    cout<<"    ************************************************************************"<<endl;
    cout<<"    *                                                                      *"<<endl;
    cout<<"    *                     VIRCOL V1.0 03/30/05                             *"<<endl;
    cout<<"    *                                                                      *"<<endl;
    cout<<"    *                                                                      *"<<endl;
    cout<<"    ************************************************************************"<<endl;
    cout<<endl<<endl<<endl;}    
  delimiter="+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+";
  MaxBranch=1000;
  MaxParticle=1002;
  tolerance=1e-10;
  Q2hadronization=0.0;
  TiXmlDocument init(initfile);init.LoadFile();
  //Use a handle to easier access elements etc.
  TiXmlHandle initH(&init);
  //Step past the (redundant) global tag.
  initH=initH.Child("FileNames",0);

  //First nontrivial tag
  TiXmlHandle dataH=initH.Child("InitShower",0);
  if (dataH.Element()) {
    if (dataH.Child(0).Text()) {
      const char* filename= dataH.Child(0).Text()->Value();
      TiXmlDocument Sinit(filename);Sinit.LoadFile();
      TiXmlHandle SinitH(&Sinit);
      SinitH=SinitH.Child("vircol_init_data",0);
      TiXmlHandle SdataH=SinitH.Child("event_mechanics",0);
      if (SdataH.Element()) {
	TiXmlElement* tag;
	tag=SdataH.Child("maximum_branchings",0).Element();
	if (tag) tag->Attribute("MaxBranch",&MaxBranch);
	tag=SdataH.Child("maximum_particles",0).Element();
	if (tag) tag->Attribute("MaxParticle",&MaxParticle);
	tag=SdataH.Child("numerical_tolerance",0).Element();
	if (tag) tag->Attribute("tolerance",&tolerance);
	if (verbose>0) {
	  cout<<delimiter<<endl;
	  cout<<"Event object initialized with size parameters:"<<endl;
	  cout<<"   Maximum number of branches:  "<<MaxBranch<<endl;
	  cout<<"   Maximum number of particles: "<<MaxParticle<<endl;}}
      else ErrorInitXml("event_mechanics",filename);}
    else {ErrorInitXml("InitShower",initfile);exit(1);}}
  else {ErrorInitXml("FileNames",initfile);exit(1);}

  dataH=initH.Child("ParticleData",0);
  if (dataH.Element()) {
    if (dataH.Child(0).Text()) {
      const char* filename = dataH.Child(0).Text()->Value();
      if (verbose>0) 
	{cout<<endl<<"Initializing particle data table from file: " 
	     <<filename << "..." << endl;}
      ParticleData.XmlInit(filename,verbose);}
    else {ErrorInitXml("ParticleData",initfile);exit(1);}}
  else {ErrorInitXml("FileName",initfile);exit(1);}

  dataH=initH.Child("Alpha_S",0);
  if (dataH.Element()) {
    if (dataH.Child(0).Text()) {
      const char* filename = dataH.Child(0).Text()->Value();
      StrongCoupling aS(filename);
      alfaS=aS;
      if (verbose>0) {
	cout<<endl;
	cout<<"alpha_S initialized:"<<endl;
	cout<<"   alpha_S(Mz=91.1876)="<<alfaS(91.1876)<<endl;}
      if (verbose>1) {
	cout<<"   Testing running:"<<endl;
	alfaS.print(7,1.0,128.0);
	cout<<delimiter<<endl<<endl;}      }
    else {ErrorInitXml("Alpha_S",initfile);exit(1);}}
  else {ErrorInitXml("FileName",initfile);exit(1);}
  //Make a new event record and branch history record.
  E=new particle[MaxParticle];
  S=new branchelemental[MaxBranch];
};

void Event::Generate(double QrInit,double Qhad,const char* eventfile)
{
  Initialize(QrInit,eventfile);
  Q2resolution=QrInit*QrInit;
  Shower(Qhad);
  return;
}

void Event::Shower(double Qhad)
{
  Evolve(Qhad);
  Hadronize(Qhad);
  return;
}

// Evolve and perform 1 branching.
double Event::Evolve()
{
  while(!BranchEvent()){};
  if (verbose>0) Print();
  return sqrt(Q2resolution);
};

// Evolve (or devolve) k branchings
double Event::Evolve(int k)
{
  //cout << "Evolve " << k << ": Generation+k =" << Generation+k << endl;
  if (k<0) {
    GenMax=max(Generation+k,0);
    //cout << "GenMax = " << GenMax << ": Selecting Generation..." << endl;
    SelectGeneration();
    if (verbose>0) Print();
  }
  else for (int i=1;i<k;i++) Evolve();
  return sqrt(Q2resolution);
}

// Evolve (or devolve) to scale Qr.
double Event::Evolve(double Qr)
{
  double Qr2=Qr*Qr;
  int k=Resolution2Generation(Qr2);
  if (k<GenMax) {
    GenMax=k;
    SelectGeneration();
    if (verbose>0) Print();
  }
  else {
    while (Q2resolution>=Qr2) Evolve();
    Evolve(-1);}
  return sqrt(Q2resolution);
}

bool Event::BranchEvent()
{
  return Branch();
}

void Event::Hadronize(double Qhad)
{
  Q2hadronization=Qhad*Qhad;
  GenMax=Generation;
  SelectGeneration();
  if (verbose>0){
    cout<<endl<<"Hadronization scale set at "<<Qhad<<endl<<endl;
    Print();}
  return;
}

int Event::SelectGeneration()
{
  //cout << "*** Called SelectGeneration()." << endl;
  //cout << "GenMax     = " << GenMax << endl;
  //cout << "Generation = " << Generation << endl;
  return SelectGeneration(GenMax);
};

int Event::SelectGeneration(double Qr)
{
  return SelectGeneration(Resolution2Generation(Qr));
}
  
int Event::SelectGeneration(int k)
{
  int pointera=0,pointerb=0;
  //  cout << "*** Called SelectGeneration(int k)." << endl;
  //cout << "GenMax     = " << GenMax << endl;
  //cout << "Generation = " << Generation << endl;
  k=min(GenMax,k);
  //cout << "k          = " << k << endl;
  while(k>Generation){
    //cout << "Attempting to step forward" << endl;
    pointera=S[Generation].pointer_Pa;
    pointerb=S[Generation].pointer_Pb;
    E[pointera]=S[Generation].PaP;
    E[Nparticle+1]=S[Generation].P1P;
    E[pointerb]=S[Generation].PbP;
    Q2resolution=S[Generation].Q2resolutionP;
    Nparticle++;
    Generation++;
  };
  while(k<Generation){
    //cout << "Attempting to step back..." << endl;
    Nparticle--;
    Generation--;
    pointera=S[Generation].pointer_Pa;
    pointerb=S[Generation].pointer_Pb;
    //cout << "pa, pb = " << pointera << " " << pointerb << endl;
    //cout << "Replacement Particles:" << endl;
    //S[Generation].Pa.Print();
    //S[Generation].Pb.Print();
    E[pointera]=S[Generation].Pa;
    E[pointerb]=S[Generation].Pb;
    E[pointera].setcolor(pointerb);
    E[pointerb].setanticolor(pointera);
    Q2resolution=S[Generation].Q2resolution;
  };
  return k;
};


bool Event::Initialize(double Qinit,const char* eventfile)
{
  static int iev=0;
  bool status=true;
  Nparticle=0;GenMax=0;Generation=0;Q2resolution=Qinit*Qinit;
  TiXmlDocument xmltuple;
  xmltuple.LoadFile(eventfile);xmltuple.Parse(eventfile);
  TiXmlHandle xmltupleH(&xmltuple);
  //Step past the (redundant) global tag.
  xmltupleH=xmltupleH.FirstChild("xmltuple");
  TiXmlHandle eventH=xmltupleH.Child("event",iev++);
  if (!eventH.Element()) {
    if (verbose>0) {cout<<"No more events in "<< eventfile <<endl;}  
    status=false;
    //What to do at EOF???
    exit(1);
    return status;
  }
  int id;
  TiXmlHandle particleH=eventH.Child("entry",0);
  while(particleH.Element())
    {
      TiXmlElement* tag=particleH.Element();
      tag->Attribute("id",&id);
      E[Nparticle+1].id=id;
      E[Nparticle+1].name=ParticleData.name(id);
      E[Nparticle+1].m0=ParticleData.mass(id);
      if (verbose>0) cout << "reading particle " << Nparticle+1 << " " << id <<endl;
      tag->Attribute("E",&E[Nparticle+1].p[0]);
      tag->Attribute("Px",&E[Nparticle+1].p[1]);
      tag->Attribute("Py",&E[Nparticle+1].p[2]);
      tag->Attribute("Pz",&E[Nparticle+1].p[3]);
      tag=particleH.Child("colorline",0).Element();
      if (tag) 
	{
	  // Here rewrite so cb -> ac ?
	  tag->Attribute("c",&E[Nparticle+1].c);
	  tag->Attribute("ac",&E[Nparticle+1].cb);
	}
      else
	{ 
	  // Here probably rewrite so no colour -> 0.
	  E[Nparticle+1].c=Nparticle+1;
	  E[Nparticle+1].cb=Nparticle+1;
	};
      particleH=eventH.Child("entry",++Nparticle);
    };
  double En=0.0,px=0.0,py=0.0,pz=0.0;
  for (int i=1;i<=Nparticle;i++){En+=E[i].p[0];px+=E[i].p[1];py+=E[i].p[2];pz+=E[i].p[3];}
  if (En>tolerance||px>tolerance||py>tolerance||pz>tolerance) 
    {status=false;cerr<<"Event::newevent -> Momentum conservation failed for event"<<endl;};
  if (verbose>0) {cout<<"Initialized event:"<<endl<<endl;Print();}  
  return status;
}
 
void Event::Print(double Qr)
{
  int currentGen=Generation;
  SelectGeneration(Qr);
  cout<<"Generation:           "<<Generation<<endl;
  cout<<"Size of Event Record: "<<Nparticle<<endl;
  if (Qr*Qr>=Q2hadronization)
    cout<<"Event Resolution:     "<<Qr<<endl<<endl;
  else
    cout<<"Event Resolution:  "<<Qr<<" ( < Hadronization scale ="<<sqrt(Q2hadronization)<<" )"<<endl; 
  list();
  cout<<endl<<endl;
  SelectGeneration(currentGen);
}
void Event::Print()
{
  cout<<"Generation:           "<<Generation<<endl;
  cout<<"Size of Event Record: "<<Nparticle<<endl;
  cout<<"Event Resolution:     "<<sqrt(Q2resolution)<<endl<<endl;
  //    for (int i=1;i<Nparticle;i++) {cout<<"Particle "<<i<<":"<<endl;E[i].Print();cout<<endl;}
  list();
  cout<<endl<<endl;
};

void Event::list(ostream& os) {
  os.setf(ios::fixed, ios::floatfield); os.precision(2); 
  os << "\n------------------------------ Vircol Event List "
     <<   "------------------------------\n"
     << "   i      id   cp  acp"
     << "      p_x      p_y      p_z        E        M" 
     << "   off-shlns\n";
  for (int i=1;i<=Nparticle;i++) {
    particle& pt = E[i];
    os << setw(4) << i << setw(8) << pt.name << setw(5) << pt.c
       << setw(5) << pt.cb << setw(9) << pt.p[1] << setw(9) << pt.p[2] 
       << setw(9) << pt.p[3] << setw(9) << pt.p[0] << setw(9) << pt.m0 
       << setw(12) << pt.virtuality() << "\n";
  }
  os << "------------------------------ End of event list "
     << "------------------------------ \n";
}

bool Event::nextdipole(bool newdipole)
{
  bool status=false;
  static int checker,dipolep1,dipolep2;
  if (newdipole)
    {
      checker=1;
      dipolep1=1;
      for (int i=1;i<=Nparticle;i++){E[i].status=true;}
      status=true;
      return status;
    };
  bool search=true;
  while(search)
    {
      dipolep2=dipolep1;
      dipolep1=E[dipolep1].color();
      if (E[dipolep1].status)
	{
	  if (dipolep1==dipolep2)
	    {
	      E[dipolep1].status=false;
	    }
	  else
	    {
	      search=false;
	      status=true;
	      E[dipolep1].status=false;
	      S[Generation].pointer_Pb=dipolep1;
	      S[Generation].Pb=E[dipolep1];
	      S[Generation].pointer_Pa=dipolep2;
	      S[Generation].Pa=E[dipolep2];
	    };
	}
      else
	{
	  do{checker++;}while((!E[checker].status)&&(checker<=Nparticle)); 
	  if (checker<=Nparticle){dipolep1=checker;}
	  else{status=false;search=false;};
	};
    };
  if (verbose>1) {
    cout<<endl;
    if (status) {cout<<"-> new dipole found: ("<<dipolep2<<","<<dipolep1<<")"<<endl;}
    else {cout<<"-> all dipoles considered"<<endl;}}
  return status;
};

int Event::Resolution2Generation(double Q)
{
  double Q2=Q*Q;
  int k=Generation;
  if (Q2<=Q2resolution) {while (Q2<S[k].Q2resolution) {k++;if (k==GenMax) break;}}
  else {while (Q2>S[k].Q2resolution) {k--;if (k==0) break;}}
  return k;
}
