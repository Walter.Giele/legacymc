// Header file for the ParticleDataEntry and ParticleDataTable classes.
// ParticleDataEntry contains info on a single particle species.
// ParticleDataTable  collects info on all particles as a map.

#include "Stdlib.h"

#ifndef Pythia8_ParticleData_H
#define Pythia8_ParticleData_H

namespace Pythia8 {

//**************************************************************************

// This class holds info on a single particle species.

class ParticleDataEntry {

public:

  // Constructors: antiparticle or not?
  ParticleDataEntry(int idIn = 0, string nameIn = " ", int charge3In = 0, 
    int colIn = 0, double massIn = 0.) : idAbs(abs(idIn)), 
    nameNow(nameIn), nameDefault(nameIn),  antiNameNow("void"), 
    antiNameDefault("void"), hasAntiNow(false), hasAntiDefault(false), 
    charge3Now(charge3In), charge3Default(charge3In), colNow(colIn), 
    colDefault(colIn), massNow(massIn), massDefault(massIn) { }   
  ParticleDataEntry(int idIn, string nameIn, string antiNameIn, 
    int charge3In = 0, int colIn = 0, double massIn = 0.) 
    : idAbs(abs(idIn)), nameNow(nameIn), nameDefault(nameIn), 
    antiNameNow(antiNameIn), antiNameDefault(antiNameIn), hasAntiNow(true), 
    hasAntiDefault(true), charge3Now(charge3In), charge3Default(charge3In), 
    colNow(colIn), colDefault(colIn), massNow(massIn), massDefault(massIn) 
    { if (tolower(antiNameIn) == "void") { hasAntiDefault = false; 
    hasAntiNow = false; } }

  // Change current values (or set if not set before).
  void setName(string nameIn) {nameNow = nameIn; }
  void setAntiName(string antiNameIn) {antiNameNow = antiNameIn; }
  void setNames(string nameIn, string antiNameIn) {nameNow = nameIn; 
    antiNameNow = antiNameIn; hasAntiNow = true; 
    if (tolower(antiNameIn) == "void") hasAntiNow = false;}
  void setCharge3(int charge3In) {charge3Now = charge3In;}
  void setCol(int colIn) {colNow = colIn;}
  void setMass(double massIn) {massNow = massIn;}

  // Give back current values. 
  int id() const { return idAbs; }
  bool hasAnti() const { return hasAntiNow; } 
  string name(int idIn = 1) const { 
    return (idIn > 0) ? nameNow : antiNameNow; } 
  int charge3(int idIn = 1) const { 
    return (idIn > 0) ? charge3Now : -charge3Now; } 
  double charge(int idIn = 1) const { 
    return (idIn > 0) ? charge3Now / 3. : -charge3Now / 3.; } 
  int colType(int idIn = 1) const { if (colNow == 2) return colNow;
    return (idIn > 0) ? colNow : -colNow; } 
  double mass() const { return massNow; } 
  int spinType() const;
  bool isQuark() const;
  bool isLepton() const;
     
  // Restore current values to default.
  void reset() { nameNow = nameDefault; antiNameNow = antiNameDefault;
    hasAntiNow = hasAntiDefault; charge3Now = charge3Default;
    colNow = colDefault; massNow = massDefault; } 

private:

    // Particle data: both current and default.
    int idAbs;
    string nameNow, nameDefault, antiNameNow, antiNameDefault;
    bool hasAntiNow, hasAntiDefault;
    int charge3Now, charge3Default, colNow, colDefault;
    double massNow, massDefault;

};

//**************************************************************************

// This class holds a map of all ParticleDataEntries.

class ParticleDataTable {

public:

  // Constructor.
  ParticleDataTable() {;}
 
  // Read in database from specific file.
  static bool init(string = "ParticleData.man") ;

  // Overwrite existing database by reading from specific file.
  static bool reInit(string) ;
 
  // Read in updates from user-defined file.
  static bool read(string) ;

  // Extra stuff for XML read-in (for vircol)
  // Read in database from specific file.
  static bool XmlInit(const char*,int verbose) ;

  // Print out table of database.
  static void list(ostream& = cout) ; 

  // Restore all values to their defaults.
  static void reset() {
    for (map<int, ParticleDataEntry>::iterator pdtEntry = pdt.begin();
      pdtEntry != pdt.end(); ++pdtEntry) pdtEntry->second.reset(); }

  // Query existence of an entry.
  static bool isParticle(int idIn) {
    if (pdt.find(abs(idIn)) == pdt.end()) return false;
    if (idIn > 0 || pdt[abs(idIn)].hasAnti()) return true;
    return false; }
 
  // Add new entry.
  static void addParticle(int idIn, string nameIn = " ", int charge3In = 0, 
    int colIn = 0, double massIn = 0.) { pdt[abs(idIn)] = 
    ParticleDataEntry(idIn, nameIn, charge3In, colIn, massIn); }  
  static void addParticle(int idIn, string nameIn, string antiNameIn, 
    int charge3In = 0, int colIn = 0, double massIn = 0.) {
    pdt[abs(idIn)] = ParticleDataEntry(idIn, nameIn, antiNameIn, charge3In, 
    colIn, massIn); }  
  
  // Return pointer to entry.
  static ParticleDataEntry* particleData(int idIn) {
    return (isParticle(idIn)) ? &pdt[abs(idIn)] : 0; }

  // Change current values (or set if not set before).
  static void setName(int idIn, string nameIn) {
    if (isParticle(idIn)) pdt[abs(idIn)].setName(nameIn); }
  static void setAntiName(int idIn, string antiNameIn) {
    if (isParticle(idIn)) pdt[abs(idIn)].setAntiName(antiNameIn); }
  static void setNames(int idIn, string nameIn, string antiNameIn) {
    if (isParticle(idIn)) pdt[abs(idIn)].setNames(nameIn, antiNameIn); }
  static void setCharge3(int idIn, int charge3In) {
    if (isParticle(idIn)) pdt[abs(idIn)].setCharge3(charge3In); }
  static void setCol(int idIn, int colIn) {
    if (isParticle(idIn)) pdt[abs(idIn)].setCol(colIn); }
  static void setMass(int idIn, int massIn) {
    if (isParticle(idIn)) pdt[abs(idIn)].setMass(massIn); }

  // Give back current values. 
  static string name(int idIn) {
    return (isParticle(abs(idIn))) ? pdt[abs(idIn)].name(idIn) : " "; }
  static bool hasAnti(int idIn) {
    return isParticle(idIn) ? pdt[abs(idIn)].hasAnti() : false ; } 
  static int charge3(int idIn) {
    return isParticle(idIn) ? pdt[abs(idIn)].charge3(idIn) : 0 ; } 
  static double charge(int idIn) {
    return isParticle(idIn) ? pdt[abs(idIn)].charge(idIn) : 0 ; } 
  static int colType(int idIn) {
    return isParticle(idIn) ? pdt[abs(idIn)].colType(idIn) : 0 ; } 
  static double mass(int idIn) {
    return isParticle(idIn) ? pdt[abs(idIn)].mass() : 0. ; } 
  static int spinType(int idIn) {
    return isParticle(idIn) ? pdt[abs(idIn)].spinType() : 0 ; } 
  static bool isQuark(int idIn) {
    return isParticle(idIn) ? pdt[abs(idIn)].isQuark() : false ; } 
  static bool isLepton(int idIn) {
    return isParticle(idIn) ? pdt[abs(idIn)].isLepton() : false ; } 

private:

  // All particle data stored in a map.
  static map<int, ParticleDataEntry> pdt;

  // Flag that initialization has been performed.
  static bool isInit;

};
 
//**************************************************************************

} // end namespace Pythia8

#endif // Pythia8_ParticleData_H
