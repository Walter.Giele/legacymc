void ErrorSubscriptRange(string place, int offender) {
  cout << "(" << place << "):" << "called with index " << offender << ": out of range" << endl;
  exit(1);
}

void ErrorDivideZero(string place) {
  cout << "(" << place << "):" << "attempted divide by 0 " << endl;
  exit(1);
}

void ErrorInitXml(string tag, string xmlfile) {
  cout << "<" << tag << ">:" << " not found or corrupt in " << xmlfile << endl;
  exit(1);
}

void ErrorInitXml(string tag, string xmlfile, int entry) {
  cout << "<" << tag << ">:" << " not found or corrupt in " << xmlfile << " - offending entry: " << entry << endl;
  exit(1);
}

// Implementation of vec4 member functions

// Simple rotation
void vec4::rot(double theta, double phi) {
  double cthe = cos(theta); double sthe = sin(theta);
  double cphi = cos(phi); double sphi = sin(phi);
  double tmx =  cthe * cphi * p[1] -    sphi * p[2] + sthe * cphi * p[3];
  double tmy =  cthe * sphi * p[1] +    cphi * p[2] + sthe * sphi * p[3];
  double tmz = -sthe *        p[1] +                cthe *        p[3]; 
  p[1] = tmx; p[2] = tmy; p[3] = tmz;
}

// Simple boost, by a dimensionless 3-vector.
void vec4::bst(double betaX, double betaY, double betaZ) {
  double beta2 = betaX*betaX + betaY*betaY + betaZ*betaZ;
  double gamma = 1./sqrt(1.-beta2);
  double prod1 = betaX * p[1] + betaY * p[2] + betaZ * p[3];
  double prod2 = gamma * (gamma * prod1 / (1. + gamma) + p[0]);
  p[1] += prod2 * betaX;
  p[2] += prod2 * betaY;
  p[3] += prod2 * betaZ;
  p[0] = gamma * (p[0] + prod1);
}
  
// Simple boost, by the 4-vector (beta*gamma,gamma) 
void vec4::bst(const vec4& vec) {
  double betaX = vec.p[1]/vec.p[0];
  double betaY = vec.p[2]/vec.p[0];
  double betaZ = vec.p[3]/vec.p[0];
  double beta2 = betaX*betaX + betaY*betaY + betaZ*betaZ;
  double gamma = 1./sqrt(1.-beta2);
  double prod1 = betaX * p[1] + betaY * p[2] + betaZ * p[3];
  double prod2 = gamma * (gamma * prod1 / (1. + gamma) + p[0]);
  p[1] += prod2 * betaX;
  p[2] += prod2 * betaY;
  p[3] += prod2 * betaZ;
  p[0] = gamma * (p[0] + prod1);
}

// Print a 4-vector: also operator overloading.

ostream& operator<<(ostream& os, const vec4& v) {
  os.setf(ios::fixed, ios::floatfield); os.precision(2); 
  os << setw(9) << v.p[1] << setw(9) << v.p[2] << setw(9)  
     << v.p[3] << setw(9) << v.p[0] << "\n";
  return os;
}

// Additonal stuff: operations pairs of 4-vectors

// invariant mass of a pair of 4-vectors
double m2(const vec4& v1, const vec4& v2) {
  double m2 = pow2(v1.p[0] + v2.p[0]) - pow2(v1.p[1] + v2.p[1])
     - pow2(v1.p[2] + v2.p[2]) - pow2(v1.p[3] + v2.p[3]);
  return m2; }

double m(const vec4& v1, const vec4& v2) {
  double m2c = m2(v1,v2);
  return (m2c > 0. ? sqrt(m2c) : 0.); }

// spatial dot and cross products
double dot3(const vec4& v1, const vec4& v2)
  {return v1.p[1]*v2.p[1] + v1.p[2]*v2.p[2] + v1.p[3]*v2.p[3];} 

vec4 cross3(const vec4& v1, const vec4& v2) {
  vec4 v; v.p[1] = v1.p[2] * v2.p[3] - v1.p[3] * v2.p[2];
  v.p[2] = v1.p[3] * v2.p[1] - v1.p[1] * v2.p[3];
  v.p[3] = v1.p[1] * v2.p[2] - v1.p[2] * v2.p[1]; return v; }

// theta angle in standard spherical coordinates.
double costheta(const vec4& v1, const vec4& v2) {
  double cthe = v1.p[1]*v2.p[1] + v1.p[2]*v2.p[2] + v1.p[3]*v2.p[3]
    / sqrt( (v1.p[1]*v1.p[1] + v1.p[2]*v1.p[2] + v1.p[3]*v1.p[3]) 
    * (v2.p[1]*v2.p[1] + v2.p[2]*v2.p[2] + v2.p[3]*v2.p[3]) );
  cthe = max(-1., min(1., cthe));
  return cthe; } 

// phi angle with respect to arbitrary "z" axis, n.
double cosphi(const vec4& v1, const vec4& v2, const vec4& n) {
  double nx = n.p[1]; double ny = n.p[2]; double nz = n.p[3];
  double norm = 1. / sqrt(nx*nx + ny*ny + nz*nz);
  nx *= norm; ny *=norm; nz *=norm; 
  double v1s = v1.p[1] * v1.p[1] + v1.p[2] * v1.p[2] + v1.p[3] * v1.p[3];
  double v2s = v2.p[1] * v2.p[1] + v2.p[2] * v2.p[2] + v2.p[3] * v2.p[3];
  double v1v2 = v1.p[1] * v2.p[1] + v1.p[2] * v2.p[2] + v1.p[3] * v2.p[3];
  double v1n = v1.p[1] * nx + v1.p[2] * ny + v1.p[3] * nz;
  double v2n = v2.p[1] * nx + v2.p[2] * ny + v2.p[3] * nz;
  double cphi = (v1v2 - v1n * v2n) / sqrt( max(1e-20, 
    (v1s - v1n*v1n) * (v2s - v2n*v2n) ));  
  cphi = max(-1., min(1., cphi));
  return cphi; }

//Extra stuff for particle database XML read-in
//*********

// Read in database from specific file.

bool Pythia8::ParticleDataTable::XmlInit(const char *xmlfile,int verbose=0) {

  // Don't initialize if it has already been done.
  if (isInit) return true;

  // Open requested XML file
  TiXmlDocument main(xmlfile);main.LoadFile();
   //Use a handle to easier access elements etc.
  TiXmlHandle mainH(&main);
  //Step past the (redundant) global tag.
  mainH=mainH.Child("particledata",0);
  //Loop over <particle>s
  if (mainH.Element()) {
    int id, cl_rep, em3q, spin_i;
    string name, antiName, longname;
    double mass, width;
    int ichild=0;
    TiXmlElement* idata;
    TiXmlElement* rdata;    
    //Start at first <particle> tag.    
    ichild=0;
    TiXmlHandle entryH=mainH.FirstChild("particle");
    TiXmlHandle nameH=entryH;
    while (entryH.Element()) {      
      entryH.Element()->Attribute("id",&id);
      longname=entryH.Child(0).Text()->Value();
      idata=entryH.Child("idata",0).Element();
      if (idata) {
	idata->Attribute("em3q",&em3q);      
	idata->Attribute("cl_rep",&cl_rep);      
	idata->Attribute("spin_i",&spin_i);      
      } else {
	ErrorInitXml("idata",xmlfile,id);    
      }
      rdata=entryH.Child("rdata",0).Element();
      if (rdata) {
	rdata->Attribute("mass",&mass);      
	rdata->Attribute("width",&width);      
      } else {
	ErrorInitXml("rdata",xmlfile,id);    
      }
      name=" ";
      antiName=" ";
      nameH=entryH.Child("name",0);
      if (nameH.Element()) name=nameH.Child(0).Text()->Value();
      nameH=entryH.Child("anti",0);
      if (nameH.Element()) antiName=nameH.Child(0).Text()->Value();      
      //Add particle to DataTable.
      if (verbose>0) cout << "   adding particle " << id << " " << name << endl;
      addParticle( id, name, antiName, em3q, cl_rep, mass); 
      //Go to next particle (or end)
      entryH=mainH.Child("particle",++ichild);
    }
  } else {
    ErrorInitXml("particledata",xmlfile);    
  }
  isInit = true;
  return true;
}

