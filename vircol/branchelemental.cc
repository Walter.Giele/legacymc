struct branchelemental
{
  int pointer_Pa,pointer_Pb;
  particle Pa,Pb;
  particle PaP,P1P,PbP;
  double Q2resolution,Q2resolutionP;
  double dipole(){return s(Pa,Pb);}
  double sa1(){return s(PaP,P1P);}
  double s1b(){return s(P1P,PbP);}
  double sab(){return s(PaP,PbP);}
  double ya1(){return sa1()/dipole();}
  double y1b(){return s1b()/dipole();}
  double yab(){return sab()/dipole();}
private:
  double s(particle p1,particle p2){
    return pow(p1[0]+p2[0],2)-pow(p1[1]+p2[1],2)-pow(p1[2]+p2[2],2)-pow(p1[3]+p2[3],2);}
};

