bool Event::Branch()
{
  bool pass=false;
  double Q2max=0.0;
  branchelemental first;
  if (GenMax==MaxBranch) {
    cerr<<"Maximum number of generations reached:"<<endl;
    cerr<<"   increase MaxBranch in Shower initialization file"<<endl;
    return true;}
  if (Nparticle==MaxParticle) {
    cerr<<"Maximum number of particles reached:"<<endl;
    cerr<<"   increase MaxParticle in Shower initialization file"<<endl;
    return true;}
  nextdipole(true);
  while(nextdipole(false))
    {
      double Q2=BranchDipole();
      if (Q2>Q2max)
	{
	  first=S[Generation];
	  Q2max=Q2;
	};
    };
  if (Q2max>Q2hadronization) {
    S[Generation]=first;
    if (EventResolution(first)>=Q2max-tolerance) {
      GenMax++;
      S[Generation].PaP.setcolor(Nparticle+1);
      S[Generation].PaP.setanticolor(S[Generation].Pa.anticolor());
      S[Generation].P1P.setcolor(S[Generation].Pa.color());
      S[Generation].P1P.setanticolor(S[Generation].Pb.anticolor());
      S[Generation].PbP.setcolor(S[Generation].Pb.color());
      S[Generation].PbP.setanticolor(Nparticle+1);
      S[Generation].PaP.m0=0.0;
      S[Generation].P1P.m0=0.0;
      S[Generation].PbP.m0=0.0;
      S[Generation].PaP.name=S[Generation].Pa.name;
      S[Generation].P1P.name=S[Generation].Pa.name;
      S[Generation].PbP.name=S[Generation].Pb.name;
      S[Generation].Q2resolution=Q2resolution;
      S[Generation].Q2resolutionP=Q2max;
      SelectGeneration();pass=true;}
    if (verbose>1)
      if (pass) {cout<<endl<<endl<<"Event accepted: "<<endl<<endl;}
      else {
	cout<<endl<<"Event has failed the event resolution test"<<endl<<endl;
	cout<<delimiter<<endl;}}
  else {pass=true;cerr<<"Event already hadronized, cannot evolve forward any further"<<endl;}
  return pass;
};

double Event::EventResolution(branchelemental &B)
{
  int neighbor;
  double Q1,Q2;
  double *pn;
  double *pa=B.PaP.momentum();
  double *p1=B.P1P.momentum();
  double *pb=B.PbP.momentum();
  int pointer1=B.pointer_Pa;
  int pointer2=B.pointer_Pb;
  neighbor=E[pointer1].anticolor();
  if (neighbor==pointer2) {pn=pb;}
  else {pn=E[neighbor].momentum();}
  Q1=Resolution(pn,pa,p1);
  neighbor=E[pointer2].color();
  if (neighbor==pointer1) {pn=pa;}
  else {pn=E[neighbor].momentum();}
  Q2=Resolution(p1,pb,pn);
  if (verbose>1) {
    cout<<"-> Event Resolution: min("<<sqrt(abs(Q1))<<","<<sqrt(abs(Q2))<<")"<<endl;
  };
  return min(Q1,Q2);
};

double BranchDipole()
{
  double sa1b=S[Generation].dipole();
  double Q2=min(sa1b,Q2resolution);
  double yR=PickResolution(Q2);
  double y1=picky1(yR);
  double y2=picky2(y1,yR);
  double phi=pickangle();
  reconstruct(y1*Q2,y2*Q2,phi,sa1b);
  if (verbose>1) {
    cout<<"--> dipole mass: "<<sqrt(sa1b)<<endl;
    cout<<"--> dipole resolved at: "<<sqrt(yR*Q2)<<endl;}
  return yR*Q2;
};

double Resolution(double *ka,double *k1,double *kb)
{
  double sa1=pow(ka[0]+k1[0],2)-pow(ka[1]+k1[1],2)-pow(ka[2]+k1[2],2)-pow(ka[3]+k1[3],2);
  double s1b=pow(kb[0]+k1[0],2)-pow(kb[1]+k1[1],2)-pow(kb[2]+k1[2],2)-pow(kb[3]+k1[3],2);
  return min(sa1,s1b);
};
double Resolution(vec4 ka,vec4 k1,vec4 kb)
{
  double sa1=pow(ka[0]+k1[0],2)-pow(ka[1]+k1[1],2)-pow(ka[2]+k1[2],2)-pow(ka[3]+k1[3],2);
  double s1b=pow(kb[0]+k1[0],2)-pow(kb[1]+k1[1],2)-pow(kb[2]+k1[2],2)-pow(kb[3]+k1[3],2);
  return min(sa1,s1b);
};
double ScaleFactor(double yR)
{
  return 1.0;
};

double Sudakov(double yR,double Q2)
{
  using namespace mathsupport;
  double log_yR=log(yR);
  double basesudakov=exp(-3/2/pi*(log_yR*log_yR+2.0*Li2(yR)-pi*pi/6));
  return pow(basesudakov,alfaS(sqrt(ScaleFactor(yR)*Q2)));
};
double PickResolution(double Q2)
{
  using namespace mathsupport;
  double r1=uniform();
  double xl=0.0;
  double xh=0.5;
  double fl=Sudakov(xl,Q2)-r1;
  double fh=Sudakov(xh,Q2)-r1;
  double yR=10.0;
  bool loop=true;
  int count=0;
  while(loop)
    {
      loop=false;
      count++;
      if (count<1000){
	double xm=0.5*(xl+xh);
	double fm=Sudakov(xm,Q2)-r1;
	double s=sqrt(fm*fm-fl*fh);
	if (s!=0.0) {
	  double xnew=xm+(xm-xl)*((fl>=fh?1.0:-1.0)*fm/s);
	  if (abs(xnew-yR)>tolerance) {
	    yR=xnew;
	    double fnew=Sudakov(yR,Q2)-r1;
	    if (fnew!=0.0) {
	      if (sign(fm,fnew)!=fm) {xl=xm;fl=fm;xh=yR;fh=fnew;}
	      else if (sign(fl,fnew)!=fl) {xh=yR;fh=fnew;}
	      else if (sign(fh,fnew)!=fh) {xl=yR;fl=fnew;}
	      else {cerr<<"Event: PickResolution -> Ridder Algorithm logic error"<<endl;}
	      if(abs(xh-xl)>tolerance){loop=true;}
	    }}}}
      else{cerr<<"Event: PickResolution -> Ridder Algorithm maximum iterations exeeded"<<endl;}
    }
  return yR;
}
double sign(double a,double b){return (b)>=0?abs(a):-abs(a);};
double picky1(double yR)
{
  return yR;
};
double picky2(double y1,double yR)
{
  using namespace mathsupport;
  double r1=uniform();
  return pow(1.0-yR,r1)*pow(yR,1.0-r1);
};
double pickangle()
{
  using namespace mathsupport;
  double r1=uniform();
  return 2*pi*r1;
};
void reconstruct(double sa1,double s1b,double phi,double sa1b)
{
  using namespace mathsupport;
  double *ka=S[Generation].Pa.momentum();
  double *kb=S[Generation].Pb.momentum();
  double *q=new double[5];
  double *Q=new double[5];
  double *Pa=new double[4];
  double *Ps=new double[4];
  double *Pb=new double[4];
  double ra=min(sa1,s1b);
  double rb=max(sa1,s1b);
  double K=sqrt(sa1b);
  double Ea=(sa1b-rb)/2/K;
  double Es=(ra+rb)/2/K;
  double Eb=(sa1b-ra)/2/K;
  double Tab=acos(1-(sa1b-ra-rb)/2/Ea/Eb);
  double Tsb=acos(1-rb/2/Es/Eb);
  Ps[0]=Es;
  Ps[1]=Es*sin(Tsb)*cos(phi);
  Ps[2]=Es*sin(Tsb)*sin(phi);
  Ps[3]=Es*cos(Tsb);
  Pa[0]=Ea;
  Pa[1]=Ea*sin(Tab)*cos(pi+phi);
  Pa[2]=Ea*sin(Tab)*sin(pi+phi);
  Pa[3]=Ea*cos(Tab);
  Pb[0]=Eb;
  Pb[1]=0;
  Pb[2]=0;
  Pb[3]=Eb;
  Q[0]=ka[0]+kb[0];
  Q[1]=ka[1]+kb[1];
  Q[2]=ka[2]+kb[2];
  Q[3]=ka[3]+kb[3];
  double kbDQ=Q[1]*kb[1]+Q[2]*kb[2]+Q[3]*kb[3];
  double f=(kbDQ/(Q[0]+K)-kb[0])/K;
  q[0]=(Q[0]*kb[0]-kbDQ)/K;
  q[1]=kb[1]+f*Q[1];
  q[2]=kb[2]+f*Q[2];
  q[3]=kb[3]+f*Q[3];
  double PbDq=Pb[0]*q[0]-Pb[1]*q[1]-Pb[2]*q[2]-Pb[3]*q[3];
  double st=1-PbDq/2/q[0]/Pb[0];
  double ct=sqrt(1-st*st);
  double ph=atan2(q[2],q[1]);
  q[0]=Pa[0];
  q[1]=cos(ph)*ct*Pa[1]-sin(ph)*Pa[2]+cos(ph)*st*Pa[3];
  q[2]=sin(ph)*ct*Pa[1]+cos(ph)*Pa[2]+sin(ph)*st*Pa[3];
  q[3]=-st*Pa[1]+ct*Pa[3];
  double qDQ=q[1]*Q[1]+q[2]*Q[2]+q[3]*Q[3];
  f=(qDQ/(Q[0]+K)+q[0])/K;
  Pa[0]=(Q[0]*q[0]+qDQ)/K;
  Pa[1]=q[1]+f*Q[1];
  Pa[2]=q[2]+f*Q[2];
  Pa[3]=q[3]+f*Q[3];

  q[0]=Ps[0];
  q[1]=cos(ph)*ct*Ps[1]-sin(ph)*Ps[2]+cos(ph)*st*Ps[3];
  q[2]=sin(ph)*ct*Ps[1]+cos(ph)*Ps[2]+sin(ph)*st*Ps[3];
  q[3]=-st*Ps[1]+ct*Ps[3];
  qDQ=q[1]*Q[1]+q[2]*Q[2]+q[3]*Q[3];
  f=(qDQ/(Q[0]+K)+q[0])/K;
  Ps[0]=(Q[0]*q[0]+qDQ)/K;
  Ps[1]=q[1]+f*Q[1];
  Ps[2]=q[2]+f*Q[2];
  Ps[3]=q[3]+f*Q[3];

  q[0]=Pb[0];
  q[1]=cos(ph)*ct*Pb[1]-sin(ph)*Pb[2]+cos(ph)*st*Pb[3];
  q[2]=sin(ph)*ct*Pb[1]+cos(ph)*Pb[2]+sin(ph)*st*Pb[3];
  q[3]=-st*Pb[1]+ct*Pb[3];
  qDQ=q[1]*Q[1]+q[2]*Q[2]+q[3]*Q[3];
  f=(qDQ/(Q[0]+K)+q[0])/K;
  Pb[0]=(Q[0]*q[0]+qDQ)/K;
  Pb[1]=q[1]+f*Q[1];
  Pb[2]=q[2]+f*Q[2];
  Pb[3]=q[3]+f*Q[3];

  // Can we do this by overloading an assignment operator?
  for (int i=0 ; i <=3 ; i++) { 
    S[Generation].P1P.p[i]=Ps[i];
  }
  
  if (uniform()>0.5)
    {
      for (int i=0 ; i <=3 ; i++) { 
	S[Generation].PaP.p[i]=Pa[i];
	S[Generation].PbP.p[i]=Pb[i];
      }
    }
  else
    {
      for (int i=0 ; i <=3 ; i++) { 
	S[Generation].PbP.p[i]=Pa[i];
	S[Generation].PaP.p[i]=Pb[i];
      }
    };
  return;
};
