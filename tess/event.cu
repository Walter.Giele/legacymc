#include "architecture.h"
#include "PDF.h"
#include "momentum.cu"
#include "random.h"
#include "/home/giele/lhapdf/include/LHAPDF/LHAPDF.h"

using namespace LHAPDF;

extern void *g_entropy,*g_weight,*g_cut;
extern void *g_observable0,*g_observable1,*g_observable2,*g_observable3,*g_observable4,*g_observable5,*g_observable6;

__constant__ bool *event_cut;
__constant__ uint2 *event_entropy;
__constant__ float *event_weight;
__constant__ float *event_observable0,*event_observable1,*event_observable2,*event_observable3,*event_observable4,*event_observable5,*event_observable6;
__constant__ float d_rlogqmin,d_rlogqmax,d_rlogxmin,d_rxmax;
__constant__ float d_Z;
texture<float,2,cudaReadModeElementType> gluonPDF;
texture<float,1,cudaReadModeElementType> alfaS;


void event_init(int N,int total_events)
{
  //
  // set-up device memory pointers
  //
  cudaMemcpyToSymbol("event_cut", &g_cut, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_entropy", &g_entropy, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_weight", &g_weight, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_observable0", &g_observable0, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_observable1", &g_observable1, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_observable2", &g_observable2, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_observable3", &g_observable3, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_observable4", &g_observable4, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_observable5", &g_observable5, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_observable6", &g_observable6, sizeof(void *), 0, cudaMemcpyHostToDevice);
  //
  // set-up random number seeds
  //
  uint2 buffer[total_events*(N-1)];
  for (int i=0;i<total_events*(N-1);i++) 
    {
      buffer[i].x=rand(); 
      buffer[i].y=rand(); 
    }
  cudaMemcpy(g_entropy,buffer,sizeof(uint2)*total_events*(N-1),cudaMemcpyHostToDevice);
  //
  // set-up phase space weight constant
  //
  int Gamma=1,Nc=3;
  float Cgev2pb=0.389379304e9;
  float pi=3.1415926536;
  for (int n=1;n<N-2;n++) Gamma*=n;
  float Z=Cgev2pb*pow(4.0*pi,3)*pow(Nc/8.0/pi,N-2)*(Nc^2-1)*(N-1)*(N-3)/4.0/Gamma/Gamma;
  Z*=4.0*pow(2.0,N)/pow(2.0*(Nc*Nc-1.0),2);
  cudaMemcpyToSymbol("d_Z",&Z,sizeof(float),0,cudaMemcpyHostToDevice);
  //
  // set-up alpha_S and PDF's
  //
  setVerbosity(SILENT);
  const int SUBSET=0;
  initPDFSet("CTEQ6L1", LHPDF,SUBSET);
  int xpoints=X_GRID_POINTS;
  int qpoints=Q_GRID_POINTS;
  int xlinlog=X_GRID_LIN_LOG;
  float rmaxlinlog=X_LIN_LOG;
  float rxmax=getXmax(SUBSET);
  float rlogqmin=log(sqrt(getQ2min(SUBSET)));
  float rlogqmax=log(sqrt(getQ2max(SUBSET))*1.1);
  float rlogxmin=log(getXmin(SUBSET));
  float rlogxmax=log(rxmax);
  float rlogxmaxlinlog=log(rmaxlinlog);

  // constructing alfa_S grid  
  float alphaS[qpoints];
  for (int iq=0;iq<qpoints;iq++){
    float rlogq=rlogqmin+(iq+0.5)/((float) (qpoints))*(rlogqmax-rlogqmin);
    float Q=exp(rlogq);
    alphaS[iq]=alphasPDF(Q);
  }
  
  //constructing gluon PDF grid
  float gluon[qpoints][xpoints];
  for (int iq=0;iq<qpoints;iq++){
    float rlogq=rlogqmin+(iq+0.5)/((float) (qpoints))*(rlogqmax-rlogqmin);
    float Q=exp(rlogq);
    for (int ix=0;ix<xpoints;ix++){
      float x;
      if (ix<xlinlog) {
	float rlogx=rlogxmin+(ix+0.5)/((float) (xlinlog-1))*(rlogxmaxlinlog-rlogxmin);
	x=exp(rlogx); }
      else {
	x=rmaxlinlog+(ix+0.5-(xpoints-xlinlog))/((float)(xpoints-xlinlog))*(rxmax-rmaxlinlog);
      }
      gluon[iq][ix]=(float) xfx(x,Q,0);
    }}

  cudaChannelFormatDesc channelDesc=cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
  cudaArray* cu_gluonPDF;
  cudaArray* cu_alfaS;
  cudaMallocArray(&cu_gluonPDF,&channelDesc,xpoints,qpoints);
  cudaMallocArray(&cu_alfaS,&channelDesc,qpoints);
  cudaMemcpyToArray(cu_gluonPDF,0,0,gluon,qpoints*xpoints*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpyToArray(cu_alfaS,0,0,alphaS,(qpoints)*sizeof(float),cudaMemcpyHostToDevice);
  gluonPDF.addressMode[0]=cudaAddressModeClamp;
  gluonPDF.addressMode[1]=cudaAddressModeClamp;
  gluonPDF.filterMode=cudaFilterModeLinear;
  gluonPDF.normalized=true;
  alfaS.addressMode[0]=cudaAddressModeClamp;
  alfaS.filterMode=cudaFilterModeLinear;
  alfaS.normalized=true;
  cudaBindTextureToArray(gluonPDF,cu_gluonPDF,channelDesc);
  cudaBindTextureToArray(alfaS,cu_alfaS,channelDesc);
  cudaMemcpyToSymbol("d_rlogqmin",&rlogqmin,sizeof(float),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("d_rlogqmax",&rlogqmax,sizeof(float),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("d_rlogxmin",&rlogxmin,sizeof(float),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("d_rxmax",&rxmax,sizeof(float),0,cudaMemcpyHostToDevice);
}

__device__ float xgluon(float X,float Q) 
{
  float x,rlogxmaxlinlog=__logf(X_LIN_LOG);
  if (X<X_LIN_LOG) x=__fdividef(X_GRID_LIN_LOG-1.0f,(float) X_GRID_POINTS)*__fdividef(__logf(X)-d_rlogxmin,rlogxmaxlinlog-d_rlogxmin);
  else x=__fdividef((__fdividef(X-X_LIN_LOG,d_rxmax-X_LIN_LOG)+1)*(X_GRID_POINTS-X_GRID_LIN_LOG),X_GRID_POINTS);
  float q=__fdividef(__logf(Q)-d_rlogqmin,d_rlogqmax-d_rlogqmin);
  return __fdividef(tex2D(gluonPDF,x,q),X);
}

__device__ float alfas(float Q) 
{
  float q=__fdividef(__logf(Q)-d_rlogqmin,d_rlogqmax-d_rlogqmin);
  return tex1D(alfaS,q);
}


__device__ void ramboo_parallel(int i,momentum<float> *K)
{
  float Eb=-(K[0].p[0]+K[1].p[0]);
  float pz=-(K[0].p[1]+K[1].p[1]);
  float f=K[i].p[0];
  K[i].p[0]=(Eb*f+pz*K[i].p[1]);
  f+=K[i].p[0];f*=__fdividef(pz,1.0+Eb);
  K[i].p[1]+=f;
}

__device__ float scale(int Nparticle,float Ecm,momentum<float> *K)
{
  /*
  float mu=0.0;
  for (int i=2;i<Nparticle;i++)
    {
      float px=K[i].p[2],py=K[i].p[3];
      mu+=sqrt(px*px+py*py);
    }
  return mu*Ecm;
  */
  return 91.188f;
}

__device__ float Ht(int Nparticle,float Ecm,momentum<float> *K)
{
  float Obs=0.0;
  for (int i=2;i<Nparticle;i++)
    {
      float px=K[i].p[2],py=K[i].p[3];
      Obs+=sqrt(px*px+py*py);
    }
  return Obs*Ecm;
}
__device__ float dR(int Nparticle,float Ecm,momentum<float> *K)
{
  float sumV=10.0;
  for (int i=2;i<Nparticle-1;i++)
  {
    float pe=K[i].p[0],px=K[i].p[2],py=K[i].p[3],pz=K[i].p[1];
    float eta=0.5*__logf(__fdividef(pe+pz,pe-pz));
    for (int j=i+1;j<Nparticle;j++)
    {
      float qe=K[j].p[0],qx=K[j].p[2],qy=K[j].p[3],qz=K[j].p[1];
      float Deta=eta-0.5*__logf(__fdividef(qe+qz,qe-qz));
      float d=__fdividef(px*qx+py*qy,sqrt(px*px+py*py)*sqrt(qx*qx+qy*qy));
      d=max(d,-1.0);d=min(d,1.0);
      float Dphi=acos(d);
      float R=Deta*Deta+Dphi*Dphi;
      sumV=min(sumV,sqrt(R));
    }
  }
    //return 2.0*sumV/((Nparticle-2)*(Nparticle-3));
    return sumV;
}

__device__ float PhiMin(int Nparticle,float Ecm,momentum<float> *K)
{
  float sumV=10.0;
  for (int i=2;i<Nparticle-1;i++)
  {
    float px=K[i].p[2],py=K[i].p[3];
     for (int j=i+1;j<Nparticle;j++)
    {
      float qx=K[j].p[2],qy=K[j].p[3];
      float d=__fdividef(px*qx+py*qy,sqrt(px*px+py*py)*sqrt(qx*qx+qy*qy));
      d=max(d,-1.0);d=min(d,1.0);
      float Dphi=acos(d);
      sumV=min(Dphi,sumV);
    }
  }
  //return 2.0*sumV/((Nparticle-2)*(Nparticle-3));
  return sumV;
}

__device__ float PhiMax(int Nparticle,float Ecm,momentum<float> *K)
{
  float sumV=0.0;
  for (int i=2;i<Nparticle-1;i++)
  {
    float px=K[i].p[2],py=K[i].p[3];
     for (int j=i+1;j<Nparticle;j++)
    {
      float qx=K[j].p[2],qy=K[j].p[3];
      float d=__fdividef(px*qx+py*qy,sqrt(px*px+py*py)*sqrt(qx*qx+qy*qy));
      d=max(d,-1.0);d=min(d,1.0);
      float Dphi=acos(d);
      sumV=max(Dphi,sumV);
    }
  }
  //return 2.0*sumV/((Nparticle-2)*(Nparticle-3));
  return sumV;
}

__device__ float Eta(int Nparticle,float Ecm,momentum<float> *K)
{
  float sumV=0.0;
  for (int i=2;i<Nparticle-1;i++)
  {
    float pe=K[i].p[0],pz=K[i].p[1];
    float eta=0.5*__logf(__fdividef(pe+pz,pe-pz));
    for (int j=i+1;j<Nparticle;j++)
    {
      float qe=K[j].p[0],qz=K[j].p[1];
      float Deta=eta-0.5*__logf(__fdividef(qe+qz,qe-qz));
      sumV+=abs(Deta);
    }
  }
  return 2.0*sumV/((Nparticle-2)*(Nparticle-3));
}



__device__ bool cutter(int Nparticle,float Ecm,momentum<float> *K,float Ptmin,float etamax,float Rmin) {
  bool pass=true;
  momentum<float> S=K[0]+K[1];
  for (int i=2;i<Nparticle;i++)
  {
    float pe=K[i].p[0],px=K[i].p[2],py=K[i].p[3],pz=K[i].p[1];
    float Pt=sqrt(px*px+py*py);
    float eta=0.5*__logf(abs(__fdividef(pe+pz,pe-pz)));
    if (Pt*Ecm<Ptmin) pass=false;
    if (abs(eta)>etamax) pass=false;
    S+=K[i];
  }
  if ((S.p[0]*S.p[0]+S.p[1]*S.p[1]+S.p[2]*S.p[2]+S.p[3]*S.p[3])>1e-6) pass=false;
  if (pass) {
    for (int i=2;i<Nparticle-1;i++)
      {
	for (int j=i+1;j<Nparticle;j++)
	  {
	    float pe=K[i].p[0],px=K[i].p[2],py=K[i].p[3],pz=K[i].p[1];
	    float qe=K[j].p[0],qx=K[j].p[2],qy=K[j].p[3],qz=K[j].p[1];
	    float Deta=0.5*__logf(abs(__fdividef((pe+pz)*(qe-qz),(pe-pz)*(qe+qz))));
	    float d=__fdividef(px*qx+py*qy,sqrt(px*px+py*py)*sqrt(qx*qx+qy*qy));
	    d=max(d,-1.0);d=min(d,1.0);
	    float Dphi=acos(d);
	    float R=sqrt(Deta*Deta+Dphi*Dphi);
	    if (R<Rmin) pass=false;
	  }
      }
  }
  return pass;
}

#define IDX(m1,m2) (N*(m1)+m2-(((m1)*(m1-1))>>1))

__device__ void orthogonal(uint2 &w, momentum<float> &J, const momentum<float> P) {
  momentum<float> J1;
  float pi=3.1415926536;
  float z=uniform(w,-1.0f,1.0f);
  float s=sqrt(abs(1.0f-z*z));
  float phi=uniform(w,0.0f,2.0*pi);
  float x,y;
  __sincosf(phi,&x,&y);
  x*=s;
  y*=s;
  J1.p[0]=0.0f;
  J1.p[1]=y*P.p[3]-z*P.p[2];
  J1.p[2]=z*P.p[1]-x*P.p[3];
  J1.p[3]=x*P.p[2]-y*P.p[1];
  float norm=sqrt(J1.p[1]*J1.p[1]+J1.p[2]*J1.p[2]+J1.p[3]*J1.p[3]);
  if (norm>0.0f) {
    J1.p[1]=__fdividef(J1.p[1],norm);
    J1.p[2]=__fdividef(J1.p[2],norm);
    J1.p[3]=__fdividef(J1.p[3],norm);
  }
  else J1=momentum_<float>(0.0,0.0,0.0,0.0);
  __syncthreads();
  J=J1;
}



__global__ void CUDA_event_parallel(int N,float Ecm,float Ptmin,float etamax,float Rmin,int number_events_per_MP) 
{
  unsigned int k=threadIdx.x%(N-1);
  unsigned int eventIdx=threadIdx.x/(N-1);
  unsigned int event=blockIdx.x*number_events_per_MP+eventIdx;
  extern __shared__ momentum<float> sharedMem[];
  int sizePerEvent=N*(N+1)/2;
  momentum<float> *K=sharedMem+sizePerEvent*eventIdx;
  uint2 w=event_entropy[event*(N-1)+k];
  float weight=0.0f;
  if (k<N-2){
    float Rmass;
    do {
      float c=uniform(w,-1.0f,1.0f);
      float s=sqrt(abs(1.0f-c*c));
      float pi=3.1415926536;
      float phi=uniform(w,0.0f,2.0*pi);
      float x,y;
      __sincosf(phi,&x,&y);
      K[k+2]=-__logf(uniform(w,0.0f,1.0f)*uniform(w,0.0f,1.0f))*momentum_<float>(1.0f, c, x*s, y*s);
      if (k==0) {
          momentum<float> R=momentum_<float>(0.0, 0.0, 0.0, 0.0);
	  for (int i=2;i<N;i++) R=R+K[i];
	  K[0]=R;
      }
      __syncthreads();
      Rmass=K[0]*K[0];
    } while ((Rmass<1e-6) || abs(1.0f-K[0].p[0])<1e-6);
    Rmass=sqrt(Rmass);
    if (k==0) {
      momentum<float> R=K[0];
      R=-R/Rmass;
      K[0]=R;
    }
    __syncthreads();
    momentum<float> R=K[k+2];
    float a=__fdividef(1.0f,1.0f-K[0].p[0]);
    float x=__fdividef(1.0f,Rmass);
    float bq=K[0].p[1]*R.p[1]+K[0].p[2]*R.p[2]+K[0].p[3]*R.p[3];
    float xq=R.p[0]+a*bq;
    R=x*momentum_<float>(-K[0].p[0]*R.p[0]+bq, R.p[1]+K[0].p[1]*xq, 
			      R.p[2]+K[0].p[2]*xq, R.p[3]+K[0].p[3]*xq);
    K[k+2]=R;
    float x1,x2,Epar;
    if (k==0) {
      float eps=__powf(__fdividef((N-2.0)*Ptmin,Ecm),2);
      float r1=uniform(w);
      float r2=uniform(w);
      x1=__powf(eps,r1*r2);
      x2=__fdividef(__powf(eps,r2),x1);
      weight=d_Z*r2*x1*x2*pow(log(eps),2);
      Epar=sqrt(x1*x2);
      K[0]=-(x1/Epar)*momentum_<float>(0.5f, 0.5f, 0.0, 0.0);
      K[1]=-(x2/Epar)*momentum_<float>(0.5f,-0.5f, 0.0, 0.0);
      Epar*=Ecm;
      weight=__fdividef(weight,Epar*Epar);
    }
    __syncthreads();
    ramboo_parallel(k+2,K);
    if (k==0) {
      event_cut[event]=cutter(N,Epar,K,Ptmin,etamax,Rmin);
      event_observable3[event]=Ht(N,Epar,K);
      event_observable4[event]=dR(N,Epar,K);
      event_observable5[event]=PhiMin(N,Epar,K);
      event_observable6[event]=PhiMax(N,Epar,K);
      float mu=scale(N,Epar,K);
      weight*=xgluon(x1,mu)*xgluon(x2,mu);
      float as=alfas(mu);
      weight*=__powf(as,N-2);
      int r=1+uniform(w)*(N-1);
      momentum<float> Kt=K[r];
      K[r]=K[1];
      K[1]=Kt;
    }
  } 
  //
  // recursion goes here
  //
  __syncthreads();
  N--;
  k++;
  momentum<float> *Jlk=K;
  orthogonal(w,Jlk[IDX(k,k)],K[k]);
  if(k==1) {
    momentum<float> P=K[0];
    for(int i=1; i<=N; i++) {
      P+=K[i];
      K[i]=P;
    }
  }
  for(int l=1; l<N; l++) {
    __syncthreads();
    if(k<=N-l) {
      momentum<float> J=momentum_<float>(0.0,0.0,0.0,0.0);
      for (int n=k; n<k+l; n++) {
        // 2 J1.K2 J2 - 2 J2.K1 J1 + J1.J2 (K1-K2)
        momentum<float> K1=K[n]-K[k-1];
        momentum<float> K2=K[k+l]-K[n];
        momentum<float> K12=K1-K2;
        momentum<float> J1=Jlk[IDX(k,n)], J2=Jlk[IDX(n+1, k+l)];
	float d1=2.0f*(J1*K2),d2=2.0f*(J2*K1),d3=J1*J2;
        J+=d1*J2-d2*J1+d3*K12;
      }
      for (int n=k; n<k+l-1; n++) {
	for (int m=n+1;m<k+l;m++) {
	  momentum<float> J1=Jlk[IDX(k,n)],J2=Jlk[IDX(n+1,m)],J3=Jlk[IDX(m+1,k+l)];
	  float d1=2.0f*(J1*J3),d2=J1*J2,d3=J2*J3;
	  J+=d1*J2-d2*J3-d3*J1;
	}
      }
      if (l<N-1) {
	momentum<float> P=K[k+l]-K[k-1];
	J/=P*P;
      }
      Jlk[IDX(k,k+l)]=J;
    }
  }
  __syncthreads();
  if(k==1) {
    momentum<float> J;
    orthogonal(w, J, K[0]);
    event_observable0[event]=weight;
    event_observable1[event]=abs(Jlk[IDX(1,N)]*J);
    event_observable2[event]=abs(Jlk[IDX(1,N)]*K[0]);
    weight*=pow(J*Jlk[IDX(1,N)],2);
    event_weight[event]=weight;
  }
  __syncthreads();
  event_entropy[event*N+k-1]=w;
}


void Event(int N,float Ecm,float Ptmin,float etamax,float Rmin,int events_per_MP)
{
  int sharedMem_size=events_per_MP*N*(N+1)/2*sizeof(momentum<float>);
  CUDA_event_parallel<<<NUMBER_MP,events_per_MP*(N-1),sharedMem_size>>>(N,Ecm,Ptmin,etamax,Rmin,events_per_MP);
}
