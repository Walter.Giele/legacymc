#include <cstdio>
#include <iostream>
#include <fstream>
#include "architecture.h"
#include "momentum.h"
#include "KahanAdder.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TPie.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TGraphErrors.h"

using namespace std;

extern void *g_weight,*g_cut;
extern void *g_observable0,*g_observable1,*g_observable2,*g_observable3,*g_observable4;
extern void init(int N,int N);
extern void Event(int N,float Ecm,float Ptmin,float etamax,float Rmin,int N);

int main(void)
{
  //
  // run set-up
  //
  int N=5;
  int Iterations=100000000;
  float Ecm=14000.0;
  float Ptmin=60.0;
  float etamax=2.0;
  float Rmin=0.4;
  //
  // set-up
  //
  int events_limit_register=REGISTER_MEMORY/(2*MAX_REGISTER);
  int max_events_per_MP=(SHARED_MEMORY-16)/(sizeof(momentum<float>)*N*(N+1)/2);
  int events_per_MP=min(events_limit_register,max_events_per_MP);
  int total_events_per_iteration=events_per_MP*NUMBER_MP;
  cout<<endl<<endl<<endl;
  cout<<"Running configuration: gg ---> "<<N-2<<" gluons"<<endl;
  cout<<"                       number of MP's used                 = "
      <<NUMBER_MP<<endl;
  cout<<"                       events/MP limit from used registers = "
      <<events_limit_register<<endl;
  cout<<"                       events/MP limit from shared memory  = "
      <<max_events_per_MP<<endl;
  cout<<"                       events/MP                           = "
      <<events_per_MP<<endl;
  cout<<"                       threads used/MP (out of 1024)       = "
      <<events_per_MP*(N-1)<<endl;
  cout<<"                       events per iteration                = "
      <<total_events_per_iteration<<endl;
  cout<<"                       number of iterations                = "
      <<Iterations<<endl;
  cout<<"                       total generated event in this run   = "
      <<(double) Iterations*(double) total_events_per_iteration<<endl;
  cout<<endl<<endl<<endl;
  //
  gROOT->Reset();
  TApplication myapp("myapp",0,0);
  TCanvas *screen = new TCanvas("screen","Output",0,0,800,400);
  gStyle->SetOptStat(kFALSE);
  gStyle->SetEndErrorSize(0);
  screen->Divide(2,1);
  //screen->cd(1);
  // 5 gluons
  //TProfile *profileRelGauge=new TProfile("Run Stats","Relative Gauge Invariance profile (9 gluons)",100,-2.0,11.0,-8.0,0.0);
  //profileRelGauge->SetMarkerColor(kBlue);
  //profileRelGauge->SetMarkerSize(0.25);
  //profileRelGauge->SetMarkerStyle(21);
  ifstream Ht_in("FinalState_HT_A.dat",ios::in);
  int nHt=1400;
  float HtComix[nHt],HtEComix[nHt],x[nHt],xe[nHt];;
  KahanAdder<double> HtTes[nHt];
  float Htsum=0.0;
  for (int n=0;n<nHt;n++) {
    float bin,Ht,HtE;
    Ht_in>>bin>>Ht>>HtE;
    x[n]=bin+5.0;
    xe[n]=5.0;
    HtComix[n]=Ht;
    HtEComix[n]=HtE;
    Htsum+=Ht;
  }
  for (int n=0;n<nHt;n++) {HtComix[n]/=Htsum;HtEComix[n]/=Htsum;}
  Ht_in.close();


  ifstream DRmin_in("FinalState_DRmin_A.dat",ios::in);
  int nRmin=400;
  float RComix[nRmin],REComix[nRmin],Rx[nRmin],Rxe[nRmin];;
  KahanAdder<double> RTes[nRmin];
  float Rsum=0.0;
  for (int n=0;n<nRmin;n++) {
    float bin,R,RE;
    DRmin_in>>bin>>R>>RE;
    Rx[n]=bin+0.005;
    Rxe[n]=0.005;
    RComix[n]=R;
    REComix[n]=RE;
    Rsum+=R;
  }
  for (int n=0;n<nRmin;n++) {RComix[n]/=Rsum;REComix[n]/=Rsum;}
  DRmin_in.close();

  //
  srand(53);
  cudaEvent_t start,stop,event;
  float init_time=0.0,event_time=0.0,histogram_time=0.0;
  float time;
  cudaEventCreate(&start);cudaEventCreate(&stop);cudaEventCreate(&event);
  //
  // Initialize run
  //
  cudaEventRecord(start, 0);
  init(N,total_events_per_iteration);
  cudaEventRecord(stop, 0);cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time, start, stop);
  init_time+=time;
  //
  // start calculating the events
  //
  int update=10000;
  KahanAdder<double> xsection,x2section;
  KahanAdder<long int> Naccepted;
  //float maxwgt=0.0;
  for (int eventsblocks=0;eventsblocks<Iterations;++eventsblocks)
    {
      if (eventsblocks!=0)
	{
	  float wgt[total_events_per_iteration],Ht[total_events_per_iteration],R[total_events_per_iteration];
	  float M[total_events_per_iteration],G[total_events_per_iteration];
	  bool cut[total_events_per_iteration];
	  cudaMemcpy(wgt,g_weight,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost);
	  cudaMemcpy(Ht,g_observable3,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost);
	  cudaMemcpy(R,g_observable4,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost);
	  cudaMemcpy(cut,g_cut,sizeof(bool)*total_events_per_iteration,cudaMemcpyDeviceToHost);
	  for (int i=0;i<total_events_per_iteration;i++) {
	      Naccepted+=1;
	      if (cut[i]==true) {
		xsection+=wgt[i];
		x2section+=wgt[i]*wgt[i];
		int n=(float) Ht[i]/10.0;
		HtTes[n]+=wgt[i];
		n=(float) R[i]/0.01;
		RTes[n]+=wgt[i];
	      }}
	  if (eventsblocks==update) 
	    {
	      screen->cd(1);
	      float y[nHt],ye[nHt];
	      for (int n=0;n<nHt;n++) {
		if (HtTes[n]()>0) {
		  y[n]=HtComix[n]*xsection()/HtTes[n]()-1.0;
		  ye[n]=HtEComix[n]*xsection()/HtTes[n]();}
		else {y[n]=0.0;ye[n]=0.0;}
	      }
	      TH2F *hpx=new TH2F("hpx","Comix comparison 5-gluon for Ht",140,0.0,14000.0,20,-1.0,1.0);
	      hpx->SetStats(kFALSE);
	      hpx->Draw();
	      TGraphErrors *RatioHt=new TGraphErrors(nHt,x,y,xe,ye);
	      RatioHt->SetMarkerColor(kBlue);
	      //RatioHt->SetMarkerStyle(21);
	      RatioHt->SetMarkerSize(0.10);

	      RatioHt->Draw("P");
	      screen->cd(2);
	      float yR[nHt],yeR[nHt];
	      for (int n=0;n<nRmin;n++) {
		if (RTes[n]()>0) {
		  yR[n]=RComix[n]*xsection()/RTes[n]()-1.0;
		  yeR[n]=REComix[n]*xsection()/RTes[n]();}
		else {yR[n]=0.0;yeR[n]=0.0;}
	      }
	      TH2F *hpxR=new TH2F("hpxR","Comix comparison 5-gluon for min(R(jet,jet))",400,0.0,4.0,20,-0.1 ,0.1);
	      hpxR->SetStats(kFALSE);
	      hpxR->Draw();
	      TGraphErrors *RatioR=new TGraphErrors(nRmin,Rx,yR,Rxe,yeR);
	      RatioR->SetMarkerColor(kBlue);
	      //RatioR->SetMarkerStyle(21);
	      RatioR->SetMarkerSize(0.10);
	      RatioR->Draw("P");

	      screen->Modified();
	      screen->Update();
	      cout<<"Accepted events:"<<Naccepted();
	      double avg=xsection()/(double) Naccepted();
	      double avg2=x2section()/(double) Naccepted();
	      double sigma_mean=sqrt((avg2-avg*avg)/(double) Naccepted());
	      cout<<"   cross section: ("<<avg<<" +/- "<<sigma_mean<<" pb)"<<endl;
	      update+=10000;
	    }
	  cudaEventRecord(stop, 0);
	  cudaEventSynchronize(stop);
	  cudaEventElapsedTime(&time,start,event);
	  event_time+=time;
	  cudaEventElapsedTime(&time,event,stop);
	  histogram_time+=time;
    }
      cudaEventRecord(start, 0);
      Event(N,Ecm,Ptmin,etamax,Rmin,events_per_MP);
      cudaEventRecord(event, 0);
}
  float wgt[total_events_per_iteration],Ht[total_events_per_iteration],R[total_events_per_iteration];
  float M[total_events_per_iteration],G[total_events_per_iteration];
  bool cut[total_events_per_iteration];
  cudaMemcpy(wgt,g_weight,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost);
  cudaMemcpy(Ht,g_observable3,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost);
  cudaMemcpy(R,g_observable4,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost);
  cudaMemcpy(cut,g_cut,sizeof(bool)*total_events_per_iteration,cudaMemcpyDeviceToHost);
  for (int i=0;i<total_events_per_iteration;i++) {
    if (cut[i]==true) {
      xsection+=wgt[i];
      x2section+=wgt[i]*wgt[i];
      int n=(float) Ht[i]/10.0;
      HtTes[n]+=wgt[i];
      n=(float) R[i]/0.01;
      RTes[n]+=wgt[i];
      Naccepted+=1;
    }}
  screen->cd(1);
  float y[nHt],ye[nHt];
  for (int n=0;n<nHt;n++) {
    if (HtTes[n]()>0) {
      y[n]=HtComix[n]*xsection()/HtTes[n]()-1.0;
      ye[n]=HtEComix[n]*xsection()/HtTes[n]();}
    else {y[n]=0.0;ye[n]=0.0;}
  }
  TH2F *hpx=new TH2F("hpx","Comix comparison 5-gluon for Ht",140,0.0,14000.0,20,-1.0,1.0);
  hpx->SetStats(kFALSE);
  hpx->Draw();
  TGraphErrors *RatioHt=new TGraphErrors(nHt,x,y,xe,ye);
  RatioHt->SetMarkerColor(kBlue);
  RatioHt->SetMarkerStyle(21);
  RatioHt->SetMarkerSize(0.25);
  RatioHt->Draw("P");
  screen->cd(2);
  float yR[nHt],yeR[nHt];
  for (int n=0;n<nRmin;n++) {
    if (RTes[n]()>0) {
      yR[n]=RComix[n]*xsection()/RTes[n]()-1.0;
      yeR[n]=REComix[n]*xsection()/RTes[n]();}
    else {yR[n]=0.0;yeR[n]=0.0;}
  }
  TH2F *hpxR=new TH2F("hpxR","Comix comparison 5-gluon for min(R(jet,jet))",400,0.0,4.0,20,-0.1 ,0.1);
  hpxR->SetStats(kFALSE);
  hpxR->Draw();
  TGraphErrors *RatioR=new TGraphErrors(nRmin,Rx,yR,Rxe,yeR);
  RatioR->SetMarkerColor(kBlue);
  RatioR->SetMarkerStyle(21);
  RatioR->SetMarkerSize(0.25);
  RatioR->Draw("P");

  screen->Modified();
  screen->Update();
  screen->SaveAs("ratio5.eps");
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,event);
  event_time+=time;
  cudaEventElapsedTime(&time,event,stop);
  histogram_time+=time;
  float total_time=init_time+event_time+histogram_time;
  cout<<"Accepted events:"<<Naccepted();
  double avg=xsection()/(double) Naccepted();
  double avg2=x2section()/(double) Naccepted();
  double sigma_mean=sqrt((avg2-avg*avg)/(double) Naccepted());
  cout<<"   cross section: ("<<avg<<" +/- "<<sigma_mean<<" pb)"<<endl;
  cout<<endl<<"Timing:"<<endl;
  cout<<"       Initialization:"<<init_time/1000.0<<" seconds ("
      <<100.0*init_time/total_time<<"%)"<<endl;
  cout<<"       Event         :"<<event_time/1000.0<<" seconds ("
      <<100.0*event_time/total_time<<"%)"<<endl;
  cout<<"       Histogramming :"<<histogram_time/1000.0<<" seconds ("
      <<100.0*histogram_time/total_time<<"%)"<<endl;
  cout<<endl;
  cout<<"      Total event time/events   :"
      <<event_time/1000.0/((double) Iterations*(double) events_per_MP*(double) NUMBER_MP)<<" seconds"<<endl;
  cout<<"       Total/event   :"
      <<total_time/1000.0/((double) Iterations*(double) events_per_MP*(double) NUMBER_MP)<<" seconds"<<endl;
  cout<<"       Total         :"
      <<total_time/1000.0<<" seconds"<<endl;

  TCanvas *pie=new TCanvas("pie","Timing",820,0,200,200);
  int nvals=3;
  float vals[3];
  const char *labels[3];
  vals[0]=init_time/1000.0;labels[0]="init";
  vals[1]=event_time/1000.0;labels[1]="event";
  vals[2]=histogram_time/1000.0;labels[2]="histogramming";
  int colors[]={1,2,3};
  TPie *piechart = new TPie("piechart","Timing",nvals,vals,colors,labels);
  piechart->SetRadius(.2);
  piechart->SetLabelsOffset(.01);
  piechart->SetLabelFormat("#splitline{%val (%perc)}{%txt}");
  piechart->Draw("nol <");
  piechart->Draw();
  pie->Update();
  cout << endl << "Enter q to terminate..." << endl;
  char tmp;cin>>tmp;
}
