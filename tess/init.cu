//
// initializing device memory
//

void *g_nan,*g_entropy,*g_weight,*g_cut;
void *g_observable0,*g_observable1,*g_observable2,*g_observable3,*g_observable4,*g_observable5,*g_observable6;
extern void event_init(int N,int N);

void init(int N,int total_events) 
{
  //
  // mapping device memory
  //
  cudaMalloc(&g_nan,total_events*sizeof(bool));
  cudaMalloc(&g_cut,total_events*sizeof(bool));
  cudaMalloc(&g_entropy,total_events*(N-1)*sizeof(uint2));
  cudaMalloc(&g_weight,total_events*sizeof(float));
  cudaMalloc(&g_observable0,total_events*sizeof(float));
  cudaMalloc(&g_observable1,total_events*sizeof(float));
  cudaMalloc(&g_observable2,total_events*sizeof(float));
  cudaMalloc(&g_observable3,total_events*sizeof(float));
  cudaMalloc(&g_observable4,total_events*sizeof(float));
  cudaMalloc(&g_observable5,total_events*sizeof(float));
  cudaMalloc(&g_observable6,total_events*sizeof(float));
  //
  // Kernel initializations
  //
  event_init(N,total_events);
}  
