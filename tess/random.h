
// random integer between 0 and 2^32-1 inclusive
__device__ unsigned int random(uint2 &w) {
  w.x = 36969 * (w.x & 65535) + (w.x >> 16);
  w.y = 18000 * (w.y & 65535) + (w.y >> 16);
  return (w.x << 16) + w.y;
}

// random float uniform in interval (0,1]
__device__ float uniform(uint2 &w) {
  //  return __int_as_float(__float_as_int(__uint2float_rn((w.x << 16) + w.y))-(32<<23));  /* 32-bit result */
  unsigned int tmp=random(w);
  return tmp ? __uint2float_rn(tmp)/4294967296.0f : 1.0f;  /* 32-bit result */
}

// random float uniform in interval (min, max]
__device__ float uniform(uint2 &w, float min, float max) {
  return min+(max-min)*uniform(w);
}

