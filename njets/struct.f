*
* structure functions
*
* istruc=0  -> test        x*all(x) = x*(1-x)
* istruc=1  -> mrse        (msbar), Lambda = 0.1  GeV
* istruc=2  -> mrsb        (msbar), Lambda = 0.2  GeV
* istruc=3  -> kmrshb      (msbar), Lambda = 0.19 GeV
* istruc=4  -> kmrsb0      (msbar), Lambda = 0.19 GeV
* istruc=5  -> kmrsb-      (msbar), Lambda = 0.19 GeV
* istruc=6  -> kmrsh- (5)  (msbar), Lambda = 0.19 GeV
* istruc=7  -> kmrsh- (2)  (msbar), Lambda = 0.19 GeV
* istruc=8  -> mts1        (msbar), Lambda = 0.2  GeV
* istruc=9  -> mte1        (msbar), Lambda = 0.2  GeV
* istruc=10 -> mtb1        (msbar), Lambda = 0.2  GeV
* istruc=11 -> mtb2        (msbar), Lambda = 0.2  GeV
* istruc=12 -> mtsn1       (msbar), Lambda = 0.2  GeV
* istruc=13 -> kmrss0      (msbar), Lambda = 0.215 GeV
* istruc=14 -> kmrsd0      (msbar), Lambda = 0.215 GeV
* istruc=15 -> kmrsd-      (msbar), Lambda = 0.215 GeV
* istruc=16 -> mrss0       (msbar), Lambda = 0.230 GeV
* istruc=17 -> mrsd0       (msbar), Lambda = 0.230 GeV
* istruc=18 -> mrsd-       (msbar), Lambda = 0.230 GeV
* istruc=19 -> cteq1l      (msbar), Lambda = 0.168 GeV
* istruc=20 -> cteq1m      (msbar), Lambda = 0.231 GeV
* istruc=21 -> cteq1ml     (msbar), Lambda = 0.322 GeV
* istruc=22 -> cteq1ms     (msbar), Lambda = 0.231 GeV
* istruc=23 -> grv         (msbar), Lambda = 0.200 GeV
* istruc=24 -> cteq2l      (msbar), Lambda = 0.190 GeV
* istruc=25 -> cteq2m      (msbar), Lambda = 0.213 GeV
* istruc=26 -> cteq2ml     (msbar), Lambda = 0.322 GeV
* istruc=27 -> cteq2ms     (msbar), Lambda = 0.208 GeV
* istruc=28 -> cteq2mf     (msbar), Lambda = 0.208 GeV
* istruc=29 -> mrsh        (msbar), Lambda = 0.230 GeV
* istruc=30 -> mrsa        (msbar), Lambda = 0.230 GeV
* istruc=31 -> mrsg        (msbar), Lambda = 0.255 GeV
* istruc=32 -> mrsap       (msbar), Lambda = 0.231 GeV
* istruc=33 -> grv94       (msbar), Lambda = 0.200 GeV
* istruc=34 -> cteq3l      (msbar), Lambda = 0.177 GeV
* istruc=35 -> cteq3m      (msbar), Lambda = 0.239 GeV
*
* extra structure functions with variable as/Lambda
*
* istruc=101 -> mrsap       (msbar), as(Mz) = 0.110 -> Lambda = 0.216
* istruc=102 -> mrsap       (msbar), as(Mz) = 0.115 -> Lambda = 0.284
* istruc=103 -> mrsap       (msbar), as(Mz) = 0.120 -> Lambda = 0.366
* istruc=104 -> mrsap       (msbar), as(Mz) = 0.125 -> Lambda = 0.458
* istruc=105 -> mrsap       (msbar), as(Mz) = 0.130 -> Lambda = 0.564
* istruc=111 -> grv94       (msbar), Lambda = 0.150 GeV
* istruc=112 -> grv94       (msbar), Lambda = 0.200 GeV = istruc33
* istruc=113 -> grv94       (msbar), Lambda = 0.250 GeV
* istruc=114 -> grv94       (msbar), Lambda = 0.300 GeV
* istruc=115 -> grv94       (msbar), Lambda = 0.350 GeV
* istruc=116 -> grv94       (msbar), Lambda = 0.400 GeV
*
      subroutine struct(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      implicit real*8(a-h,o-z)
      data init/0/
      if(istruc.eq.0)then
        call test(x,scale,upv,dnv,sea,str,chm,bot,glu)
	ups=sea
	dns=sea 
      elseif(istruc.ge.1.and.istruc.le.2)then
        call mrs(x,scale,istruc,upv,dnv,sea,str,chm,bot,glu) 
	ups=sea
	dns=sea 
      elseif(istruc.ge.3.and.istruc.le.7)then
        call kmrs(x,scale,istruc,upv,dnv,sea,str,chm,bot,glu) 
	ups=sea
	dns=sea 
      elseif(istruc.ge.8.and.istruc.le.12)then
*        call mt(x,scale,istruc,upv,dnv,sea,str,chm,bot,glu) 
*	ups=sea
*	dns=sea 
        call xmrs(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.ge.13.and.istruc.le.18)then
        call mrs92(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.ge.19.and.istruc.le.22)then
*        call cteq(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
        call xmrs(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.eq.23)then
*        call grv(x,scale,istruc,upv,dnv,sea,str,chm,bot,glu) 
*	ups=sea
*	dns=sea 
        call xmrs(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.ge.23.and.istruc.le.28)then
*        call cteq2(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
        call xmrs(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.eq.29)then
        call mrs92(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.ge.30.and.istruc.le.32)then
        call mrs95(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.eq.33)then
*        call grv94(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu) 
        call xmrs(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.ge.34.and.istruc.le.35)then
*        call cteq3(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
        call xmrs(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.ge.101.and.istruc.le.105)then
        call mrs95(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      elseif(istruc.ge.111.and.istruc.le.116)then
*        call grv94al(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
        call xmrs(x,scale,istruc,upv,dnv,ups,dns,str,chm,bot,glu)
      endif
      if(init.ne.0) goto 10
        init=1
        write(*,*)' structure functions as of 18/4/95 '
   10 continue	
      return
      end
*
      subroutine test(x,scale,upv,dnv,sea,str,chm,bot,glu)
      implicit real*8(a-h,o-z)
      upv=x*(1d0-x)
      dnv=x*(1d0-x)
      sea=x*(1d0-x)
      str=x*(1d0-x)
      chm=x*(1d0-x)
      bot=x*(1d0-x)
      glu=x*(1d0-x)
      return
      end
*
      subroutine mrs(x,scale,istruc,upv,dnv,sea,str,chm,bot,glu)
c***************************************************************c
c                                                               c
c                                                               c
c         !!!!!!! new versions january 1989  !!!!!!!!           c
c                                                               c
c                                                               c
c  istruc 1 corresponds to                                        c
c  martin, roberts, stirling (emc fit)    with lambda= 100 mev  c
c                                                               c
c  istruc 2  corresponds to                                       c
c  martin, roberts, stirling (bcdms fit)  with lambda= 200 mev  c
c                                                               c
c  (  soft glue :  x g(x,q0) = a (1-x)**5 )                     c
c                                                               c
c                         -*-                                   c
c                                                               c
c    (note that x times the parton distribution function        c
c    is returned i.e. g(x) = glu/x etc, and that "sea"          c
c    is the light quark sea i.e. ubar(x)=dbar(x)=  ...          c
c    = sea/x for a proton.  if in doubt, check the              c
c    momentum sum rule! note also that scale=q in gev)          c
c                                                               c
c                         -*-                                   c
c                                                               c
c     (the range of applicability is currently:                 c
c     10**-4 < x < 1  and  5 < q**2 < 1.31 * 10**6              c
c     higher q**2 values can be supplied on request             c
c     - problems, comments etc to srg$t3@gen                    c
c                                                               c
c                                                               c
c***************************************************************c
c :::::::::::: martin roberts stirling :::::soft glue:::: 91 mev:::
      implicit real*8(a-h,o-z)
      parameter(nx=42,ntenth=16,nq=20)
      dimension f(6,nx,nq),g(6),xx(nx),n0(6)
      data xx/1.d-4,2.d-4,4.d-4,6.d-4,8.d-4,
     .       1.d-3,2.d-3,4.d-3,6.d-3,8.d-3,
     .       1.d-2,2.d-2,4.d-2,6.d-2,8.d-2,
     .     .1d0,.125d0,.15d0,.175d0,.2d0,.225d0,.25d0,.275d0,
     .     .3d0,.325d0,.35d0,.375d0,.4d0,.425d0,.45d0,.475d0,
     .     .5d0,.525d0,.55d0,.575d0,.6d0,.65d0,.7d0,.75d0,
     .     .8d0,.9d0,1.d0/
      data xmin,xmax,qsqmin,qsqmax/1.d-4,1.d0,5.d0,1310720.d0/
      data n0/2,5,4,5,0,0/
      data init/0/
      save xx,xmin,xmax,qsqmin,qsqmax,n0,init
      if(init.ne.0) goto 10
      init=1
      if(istruc.eq.1)then
       open(unit=1,file='stfns/MRSEB1.DAT',status='old')
      elseif(istruc.eq.2)then
       open(unit=1,file='stfns/MRSEB2.DAT',status='old')
      endif
      do 20 n=1,nx-1
      do 20 m=1,nq-1
      read(1,50)f(1,n,m),f(2,n,m),f(3,n,m),f(4,n,m),f(5,n,m),f(6,n,m)
         do 25 i=1,6
  25     f(i,n,m)=f(i,n,m)/(1.d0-xx(n))**n0(i)
  20  continue
      close(1)
      do 30 j=1,ntenth-1
      xx(j)=log10(xx(j))+1.1d0
      do 30 i=1,5
      do 30 k=1,nq-1
  30  f(i,j,k)=log(f(i,j,k))*f(i,16,k)/log(f(i,16,k))
  50  format(6f10.5)
      do 40 i=1,6
      do 40 m=1,nq-1
  40  f(i,nx,m)=0.d0
  10  continue
      if(x.lt.xmin) then
        write(*,*)' x < xmin in structure functions '
	x=xmin
      endif
      if(x.gt.xmax) then
        write(*,*)' x > xmax in structure functions '
        x=xmax
      endif
      qsq=scale**2
      if(qsq.lt.qsqmin) then
        write(*,*)' q**2 < min q**2 in structure functions '
        qsq=qsqmin
      endif
      if(qsq.gt.qsqmax) then
*       write(*,*)' q**2 > max q**2 in structure functions '
        qsq=qsqmax
      endif
      xxx=x
      if(x.lt.1.d-1) xxx=log10(x)+1.1d0
      n=0
  70  n=n+1
      if(xxx.gt.xx(n+1)) goto 70
      a=(xxx-xx(n))/(xx(n+1)-xx(n))
      rm=log(qsq/qsqmin)/log(2.d0)
      b=rm-dint(rm)
      m=1+idint(rm)
      do 60 i=1,6
      g(i)= (1.d0-a)*(1.d0-b)*f(i,n,m)+(1.d0-a)*b*f(i,n,m+1)
     .    + a*(1.d0-b)*f(i,n+1,m)  + a*b*f(i,n+1,m+1)
      if(n.ge.ntenth) goto 65
      if(i.eq.6) goto 65
	 fac=(1.d0-b)*f(i,ntenth,m)+b*f(i,ntenth,m+1)
         g(i)=fac**(g(i)/fac)
  65  continue
      g(i)=g(i)*(1.d0-x)**n0(i)
  60  continue
      upv=g(1)
      dnv=g(2)
      sea=g(4)
      str=g(4)
      chm=g(5)
      glu=g(3)
      bot=g(6)
      bot=0d0
      return
      end
*
      subroutine kmrs(x,scale,istruc,upv,dnv,sea,str,chm,bot,glu)
c***************************************************************c
c                                                               c
c     -----  variable quarks and gluons at small x ----         c
c                                                               c
c     new versions !!!! july 1990                               c
c     "........................ " j. kwiecinski, a.d. martin,   c
c     r.g. roberts and w.j. stirling preprint dtp-90-46 )       c
c                                                               c
c  istruc 1 corresponds to  harriman,                             c
c  martin, roberts, stirling (bcdms fit)  with lambda= 190 mev  c
c  with small x behaviour determined from fit  "hb fit"         c
c                                                               c
c  istruc 2 corresponds to  kwiecinski,                           c
c  martin, roberts, stirling (bcdms fit)  with lambda= 190 mev  c
c  and xg,xq --> constant as x--> 0 at q0**2   "b0 fit"         c
c                                                               c
c  istruc 3 corresponds to  kwiecinski,                           c
c  martin, roberts, stirling (bcdms fit)  with lambda= 190 mev  c
c  and xg,xq --> x**-1/2 as x--> 0 at q0**2    "b- fit"         c
c                                                               c
c  istruc 4 corresponds to  kwiecinski,                           c
c  martin, roberts, stirling (bcdms fit)  with lambda= 190 mev  c
c  and xg,xq --> x**-1/2 as x--> 0 at q0**2    "b-(5) fit"      c
c  i.e. with weak (r=5 gev^-1) shadowing included               c
c                                                               c
c  istruc 5 corresponds to  kwiecinski,                           c
c  martin, roberts, stirling (bcdms fit)  with lambda= 190 mev  c
c  and xg,xq --> x**-1/2 as x--> 0 at q0**2    "b-(2) fit"      c
c  i.e. with strong  (r=2 gev^-1) shadowing included            c
c                                                               c
c                                                               c
c             >>>>>>>>  cross check  <<<<<<<<                   c
c                                                               c
c    the first number in the "hb" grid is  .03058               c
c    the first number in the "b0" grid is  .01727               c
c    the first number in the "b-" grid is  .01543               c
c    the first number in the "b-5"grid is  .01543               c
c    the first number in the "b-2"grid is  .01543               c
c                                                               c
c                                                               c
c                                                               c
c                         -*-                                   c
c                                                               c
c    (note that x times the parton distribution function        c
c    is returned i.e. g(x) = glu/x etc, and that "sea"          c
c    is the light quark sea i.e. ubar(x)=dbar(x)                c
c    = sea/x for a proton.  if in doubt, check the              c
c    momentum sum rule! note also that scale=q in gev)          c
c                                                               c
c                         -*-                                   c
c                                                               c
c     (the range of applicability is currently:                 c
c     10**-5 < x < 1  and  5 < q**2 < 1.31 * 10**6              c
c     higher q**2 values can be supplied on request             c
c     - problems, comments etc to wjs@uk.ac.dur.hep             c
c                                                               c
c                                                               c
c***************************************************************c
      implicit real*8(a-h,o-z)
      parameter(nx=47,ntenth=21,nq=20)
      dimension f(7,nx,nq),g(7),xx(nx),n0(7)
      data xx/1.d-5,2.d-5,4.d-5,6.d-5,8.d-5,
     .        1.d-4,2.d-4,4.d-4,6.d-4,8.d-4,
     .        1.d-3,2.d-3,4.d-3,6.d-3,8.d-3,
     .        1.d-2,2.d-2,4.d-2,6.d-2,8.d-2,
     .     .1d0,.125d0,.15d0,.175d0,.2d0,.225d0,.25d0,.275d0,
     .     .3d0,.325d0,.35d0,.375d0,.4d0,.425d0,.45d0,.475d0,
     .     .5d0,.525d0,.55d0,.575d0,.6d0,.65d0,.7d0,.75d0,
     .     .8d0,.9d0,1.d0/
      data xmin,xmax,qsqmin,qsqmax/1.d-5,1.d0,5.d0,1310720.d0/
      data n0/2,5,4,5,0,0,5/
      data init/0/
      save xx,xmin,xmax,qsqmin,qsqmax,n0,init
      xsave=x
      if(init.ne.0) goto 10
      if(istruc.eq.3)then
        open(unit=1,file='stfns/KMRSHB.DAT',status='old')
      elseif(istruc.eq.4)then
        open(unit=1,file='stfns/KMRSB0.DAT',status='old')
      elseif(istruc.eq.5)then
        open(unit=1,file='stfns/KMRSB-.DAT',status='old')
      elseif(istruc.eq.6)then
        open(unit=1,file='stfns/KMRSB-5.DAT',status='old')
      elseif(istruc.eq.7)then
        open(unit=1,file='stfns/KMRSB-2.DAT',status='old')
      endif
      do 20 n=1,nx-1
      do 20 m=1,nq-1
      read(1,50)f(1,n,m),f(2,n,m),f(3,n,m),f(4,n,m),f(5,n,m),f(7,n,m),
     .          f(6,n,m)
c 1=uv 2=dv 3=glue 4=(ubar+dbar)/2 5=cbar 7=bbar 6=sbar
         do 25 i=1,7
  25     f(i,n,m)=f(i,n,m)/(1.d0-xx(n))**n0(i)
  20  continue
      do 30 j=1,ntenth-1
      xx(j)=dlog10(xx(j))+1.1d0
      do 30 i=1,6
      do 30 k=1,nq-1
  30  f(i,j,k)=dlog(f(i,j,k))*f(i,ntenth,k)/dlog(f(i,ntenth,k))
  50  format(7f10.5)
      do 40 i=1,7
      do 40 m=1,nq-1
  40  f(i,nx,m)=0.d0
      close(1)
      init=1
  10  continue
      if(x.lt.xmin) then
        write(*,*)' x < xmin in structure functions '
	x=xmin
      endif
      if(x.gt.xmax) then
        write(*,*)' x > xmax in structure functions '
        x=xmax
      endif
      qsq=scale**2
      if(qsq.lt.qsqmin) then
        write(*,*)' q**2 < min q**2 in structure functions '
        qsq=qsqmin
      endif
      if(qsq.gt.qsqmax) then
*        write(*,*)' q**2 > max q**2 in structure functions '
        qsq=qsqmax
      endif
      xxx=x
      if(x.lt.1.d-1) xxx=dlog10(x)+1.1d0
      n=0
  70  n=n+1
      if(xxx.gt.xx(n+1)) goto 70
      a=(xxx-xx(n))/(xx(n+1)-xx(n))
      rm=dlog(qsq/qsqmin)/dlog(2.d0)
      b=rm-dint(rm)
      m=1+idint(rm)
      do 60 i=1,7
      g(i)= (1.d0-a)*(1.d0-b)*f(i,n,m)+(1.d0-a)*b*f(i,n,m+1)
     .    + a*(1.d0-b)*f(i,n+1,m)  + a*b*f(i,n+1,m+1)
      if(n.ge.ntenth) goto 65
      if(i.eq.7) goto 65
	  fac=(1.d0-b)*f(i,ntenth,m)+b*f(i,ntenth,m+1)
          g(i)=fac**(g(i)/fac)
  65  continue
      g(i)=g(i)*(1.d0-x)**n0(i)
  60  continue
      upv=g(1)
      dnv=g(2)
      sea=g(4) ! this sea is (ubar+dbar)/2
      str=g(6)
      chm=g(5)
      glu=g(3)
      bot=g(7)
      x=xsave
      return
      end
*
      subroutine mt(x,scale,istruc,upv,dnv,sea,str,chm,bot,glu)
      implicit real*8(a-h,o-z)
      dimension a(0:3,0:2,8),f(8)
      data init/0/
      if(init.ne.0) goto 10
      if(istruc.eq.8)then
        open(unit=1,file='stfns/MTS1.DAT',status='old')
      elseif(istruc.eq.9)then
        open(unit=1,file='stfns/MTE1.DAT',status='old')
      elseif(istruc.eq.10)then
        open(unit=1,file='stfns/MTB1.DAT',status='old')
      elseif(istruc.eq.11)then
        open(unit=1,file='stfns/MTB2.DAT',status='old')
      elseif(istruc.eq.12)then
        open(unit=1,file='stfns/MTSN1.DAT',status='old')
      endif
c 2=uv 1=dv 3=glue 4=(ubar+dbar)/2 5=sbar 6=cbar 7=bbar 8=ttbar
      read(1,49)q0,qcdl
*      write(6,49)q0,qcdl
  49  format(2f6.3)
      do 20 n=0,3
      do 20 m=0,2
      read(1,50)(a(n,m,j),j=1,8)
*      write(6,50)(a(n,m,j),j=1,8)
  50  format(8f6.2)
  20  continue
      close(1)
      init=1
  10  continue
      t=log(log(scale/qcdl)/log(q0/qcdl))
      t2=t*t
      omx=1d0-x
      dl=log(1d0+1d0/x)
      do 40 i=1,8
        a0=a(0,0,i)+a(0,1,i)*t+a(0,2,i)*t2
        a1=a(1,0,i)+a(1,1,i)*t+a(1,2,i)*t2
        a2=a(2,0,i)+a(2,1,i)*t+a(2,2,i)*t2
        a3=a(3,0,i)+a(3,1,i)*t+a(3,2,i)*t2
        f(i)=exp(a0)*x**a1*omx**a2*dl**a3
   40 continue
      upv=f(2)
      dnv=f(1)
      glu=f(3)
      sea=f(4)
      str=f(5)
      chm=f(6)
      bot=f(7)
      top=f(8)
      return
      end
      SUBROUTINE MRS92(X,SCALE,istruc,UPV,DNV,USEA,DSEA,STR,CHM,BOT,GLU) 
C***************************************************************C      
C                                                               C
C                                                               C
C     NEW VERSIONS:  APRIL  1992, istruc 1 IS THE 1990 KMRS(B0)   C
C     SET; istrucS 2-4 ARE NEW SETS FITTED TO THE RECENT NMC      C
C     AND CCFR PRELIMINARY STRUCTURE FUNCTION DATA.             C
C     THE THREE NEW SETS HAVE LAMBDA(MSbar,NF=4) = 215 MeV      C
C                                                               C
C     THE REFERENCE IS: A.D. Martin, R.G. Roberts and           C
C     W.J. Stirling, University of Durham preprint DTP/92/16    C
C                                                               C
C        istruc 1: KMRS(B0) (Lambda(4) = 190 MeV)                 C
C        istruc 2: MRS(S0) (updated B0, Lambda(4) = 215 MeV)      C
C        istruc 3: MRS(D0) (... but with ubar not= dbar)          C
C        istruc 4: MRS(D-) (updated B-, ubar not= dbar)           C
C        istruc 5: MRS(S0'/new) (final DIS data, ubar    = dbar)  C
C        istruc 6: MRS(D0'/new) (final DIS data, ubar not= dbar)  C
C        istruc 7: MRS(D-'/new) (final DIS data, ubar not= dbar)  C      
C                                                               C
C        For the new sets (5,6,7) Lambda(4) = 230 MeV           C
C                                                               C
C             >>>>>>>>  CROSS CHECK  <<<<<<<<                   C
C                                                               C
C    THE FIRST NUMBER IN THE "1" GRID IS 0.01727                C
C    THE FIRST NUMBER IN THE "2" GRID IS 0.01356                C
C    THE FIRST NUMBER IN THE "3" GRID IS 0.00527                C
C    THE FIRST NUMBER IN THE "4" GRID IS 0.00474                C
C    THE FIRST NUMBER IN THE "5" GRID IS 0.01617                C
C    THE FIRST NUMBER IN THE "6" GRID IS 0.00820                C
C    THE FIRST NUMBER IN THE "7" GRID IS 0.00678                C
C                                                               C
C    NOTE THE EXTRA ARGUMENT IN THIS SUBROUTINE MRSEB,          C
C    TO ALLOW FOR THE POSSIBILITY OF A *** DIFFERENT ***        C
C    UBAR AND DBAR SEA!                                         C
C                                                               C
C                         -*-                                   C
C                                                               C
C    (NOTE THAT X TIMES THE PARTON DISTRIBUTION FUNCTION        C
C    IS RETURNED I.E. G(X) = GLU/X ETC. IF IN DOUBT, CHECK THE  C
C    MOMENTUM SUM RULE! NOTE ALSO THAT SCALE=Q IN GEV)          C
C                                                               C
C                         -*-                                   C
C                                                               C
C     (THE RANGE OF APPLICABILITY IS CURRENTLY:                 C
C     10**-5 < X < 1  AND  5 < Q**2 < 1.31 * 10**6              C
C     HIGHER Q**2 VALUES CAN BE SUPPLIED ON REQUEST             C
C     - PROBLEMS, COMMENTS ETC TO WJS@UK.AC.DUR.HEP             C
C                                                               C
C                                                               C
C***************************************************************C
      IMPLICIT REAL*8(A-H,O-Z)
      parameter(nx=47,ntenth=21,nq=20)
      DIMENSION F(8,NX,nq),G(8),XX(NX),N0(8)
      DATA XX/1.d-5,2.d-5,4.d-5,6.d-5,8.d-5,
     .        1.D-4,2.D-4,4.D-4,6.D-4,8.D-4,
     .        1.D-3,2.D-3,4.D-3,6.D-3,8.D-3,
     .        1.D-2,2.D-2,4.D-2,6.D-2,8.D-2,
     .     .1D0,.125D0,.15D0,.175D0,.2D0,.225D0,.25D0,.275D0,
     .     .3D0,.325D0,.35D0,.375D0,.4D0,.425D0,.45D0,.475D0,
     .     .5D0,.525D0,.55D0,.575D0,.6D0,.65D0,.7D0,.75D0,
     .     .8D0,.9D0,1.D0/
      DATA XMIN,XMAX,QSQMIN,QSQMAX/1.D-5,1.D0,5.D0,1310720.D0/
      DATA N0/2,5,4,5,0,0,5,5/
      DATA INIT/0/
      save xx,xmin,xmax,qsqmin,qsqmax,n0,init
      xsave=x  ! don't let x be altered if it's out of range!!
      IF(INIT.NE.0) GOTO 10
      if(istruc.eq.13)then
        open(unit=1,file='stfns/KMRSS0.DAT',status='old')
      elseif(istruc.eq.14)then
        open(unit=1,file='stfns/KMRSD0.DAT',status='old')
      elseif(istruc.eq.15)then
        open(unit=1,file='stfns/KMRSD-.DAT',status='old')
      elseif(istruc.eq.16)then
        open(unit=1,file='stfns/MRSS0P.DAT',status='old')
      elseif(istruc.eq.17)then
        open(unit=1,file='stfns/MRSD0P.DAT',status='old')
      elseif(istruc.eq.18)then
        open(unit=1,file='stfns/MRSD-P.DAT',status='old')
      elseif(istruc.eq.29)then
        open(unit=1,file='stfns/MRSH.DAT',status='old')
      endif
      DO 20 N=1,nx-1
      DO 20 M=1,nq-1
      READ(1,50)F(1,N,M),F(2,N,M),F(3,N,M),F(4,N,M),F(5,N,M),F(7,N,M),
     .          F(6,N,M),F(8,N,M)
C 1=UV 2=DV 3=GLUE 4=UBAR 5=CBAR 7=BBAR 6=SBAR 8=DBAR
         DO 25 I=1,8
  25     F(I,N,M)=F(I,N,M)/(1.D0-XX(N))**N0(I)
  20  CONTINUE
      DO 31 J=1,NTENTH-1
      XX(J)=DLOG10(XX(J))+1.1D0
      DO 31 I=1,8
      IF(I.EQ.7) GO TO 31
      DO 30 K=1,nq-1
  30  F(I,J,K)=DLOG(F(I,J,K))*F(I,ntenth,K)/DLOG(F(I,ntenth,K))
  31  continue
  50  FORMAT(8F10.5)
      DO 40 I=1,8
      DO 40 M=1,nq-1
  40  F(I,nx,M)=0.D0
      close(1)
      INIT=1
  10  CONTINUE
      if(x.lt.xmin) then
        write(*,*)' x < xmin in structure functions '
	x=xmin
      endif
      if(x.gt.xmax) then
        write(*,*)' x > xmax in structure functions '
        x=xmax
      endif
      qsq=scale**2
      if(qsq.lt.qsqmin) then
        write(*,*)' q**2 < min q**2 in structure functions '
        qsq=qsqmin
      endif
      if(qsq.gt.qsqmax) then
*        write(*,*)' q**2 > max q**2 in structure functions '
        qsq=qsqmax
      endif
      XXX=X
      IF(X.LT.1.D-1) XXX=DLOG10(X)+1.1D0
      N=0
  70  N=N+1
      IF(XXX.GT.XX(N+1)) GOTO 70
      A=(XXX-XX(N))/(XX(N+1)-XX(N))
      RM=DLOG(QSQ/QSQMIN)/DLOG(2.D0)
      B=RM-DINT(RM)
      M=1+IDINT(RM)
      DO 60 I=1,8
      g(i)= (1.d0-a)*(1.d0-b)*f(i,n,m)+(1.d0-a)*b*f(i,n,m+1)
     .    + a*(1.d0-b)*f(i,n+1,m)  + a*b*f(i,n+1,m+1)
      IF(N.GE.ntenth) GOTO 65
      IF(I.EQ.7) GOTO 65
	  fac=(1.d0-b)*f(i,ntenth,m)+b*f(i,ntenth,m+1)
          G(I)=FAC**(G(I)/FAC)
  65  CONTINUE
      G(I)=G(I)*(1.D0-X)**N0(I)
  60  CONTINUE
      UPV=G(1)
      DNV=G(2)
      USEA=G(4)
      DSEA=G(8)
      STR=G(6)
      CHM=G(5)
      GLU=G(3)
      BOT=G(7)
      x=xsave  !restore x
      RETURN
      END
      SUBROUTINE MRS95(X,SCALE,istruc,UPV,DNV,USEA,DSEA,STR,CHM,BOT,GLU) 
C***************************************************************C
C								C
C     This is a package for the new MRS(A',G) parton            C
C     distributions. The minimum Q^2  value is 5 GeV^2 and the  C
C     x range is, as before 10^-5 < x < 1. MSbar factorization  C
C     is used. The package reads 2 grids, which are in separate C
C     files (A'=for030.dat/ftn30, G=for031.dat/ftn31).          C  
C     Note that x times the parton distribution is returned,    C
C     Q is the scale in GeV,                                    C
C     and Lambda(MSbar,nf=4) = 231/255 MeV for A'/G.            C
C								C
C	istruc=1 for MRS(A')                                      C
C	istruc=2 for MRS(G)                                       C
C								C
C         The reference is :                                    C
C         A.D. Martin, R.G. Roberts and W.J. Stirling,          C
C         RAL preprint  RAL-95-021 (1995)                       C
C                                                               C
C         Comments to : W.J.Stirling@durham.ac.uk               C
C                                                               C
C             >>>>>>>>  CROSS CHECK  <<<<<<<<                   C
C                                                               C
C         THE FIRST NUMBER IN THE 30 GRID IS 0.00341            C
C         THE FIRST NUMBER IN THE 31 GRID IS 0.00269            C
C								C
C***************************************************************C
      IMPLICIT REAL*8(A-H,O-Z)
      parameter(nx=47,ntenth=21,nq=20)
      DIMENSION F(8,NX,nq),G(8),XX(NX),N0(8)
      DATA XX/1.d-5,2.d-5,4.d-5,6.d-5,8.d-5,
     .        1.D-4,2.D-4,4.D-4,6.D-4,8.D-4,
     .        1.D-3,2.D-3,4.D-3,6.D-3,8.D-3,
     .        1.D-2,2.D-2,4.D-2,6.D-2,8.D-2,
     .     .1D0,.125D0,.15D0,.175D0,.2D0,.225D0,.25D0,.275D0,
     .     .3D0,.325D0,.35D0,.375D0,.4D0,.425D0,.45D0,.475D0,
     .     .5D0,.525D0,.55D0,.575D0,.6D0,.65D0,.7D0,.75D0,
     .     .8D0,.9D0,1.D0/
      DATA XMIN,XMAX,QSQMIN,QSQMAX/1.D-5,1.D0,5.D0,1310720.D0/
      DATA N0/2,5,5,9,0,0,9,9/
      DATA INIT/0/
      save xx,xmin,xmax,qsqmin,qsqmax,n0,init
      xsave=x  ! don't let x be altered if it's out of range!!
      IF(INIT.NE.0) GOTO 10
      if(istruc.eq.30)then
        open(unit=1,file='stfns/MRSA.DAT',status='old')
      elseif(istruc.eq.31)then
        open(unit=1,file='stfns/MRSG.DAT',status='old')
      elseif(istruc.eq.32)then
        open(unit=1,file='stfns/MRSAP.DAT',status='old')
      elseif(istruc.eq.101)then
        open(unit=1,file='stfns/MRSAP110.DAT',status='old')
      elseif(istruc.eq.102)then
        open(unit=1,file='stfns/MRSAP115.DAT',status='old')
      elseif(istruc.eq.103)then
        open(unit=1,file='stfns/MRSAP120.DAT',status='old')
      elseif(istruc.eq.104)then
        open(unit=1,file='stfns/MRSAP125.DAT',status='old')
      elseif(istruc.eq.105)then
        open(unit=1,file='stfns/MRSAP130.DAT',status='old')
      endif
      DO 20 N=1,nx-1
      DO 20 M=1,nq-1
      READ(1,50)F(1,N,M),F(2,N,M),F(3,N,M),F(4,N,M),F(5,N,M),F(7,N,M),
     .          F(6,N,M),F(8,N,M)
C 1=UV 2=DV 3=GLUE 4=UBAR 5=CBAR 7=BBAR 6=SBAR 8=DBAR
         DO 25 I=1,8
  25     F(I,N,M)=F(I,N,M)/(1.D0-XX(N))**N0(I)
  20  CONTINUE
      DO 31 J=1,NTENTH-1
      XX(J)=DLOG10(XX(J))+1.1D0
      DO 31 I=1,8
      IF(I.EQ.7) GO TO 31
      DO 30 K=1,nq-1
  30  F(I,J,K)=DLOG(F(I,J,K))*F(I,ntenth,K)/DLOG(F(I,ntenth,K))
  31  continue
  50  FORMAT(8F10.5)
      DO 40 I=1,8
      DO 40 M=1,nq-1
  40  F(I,nx,M)=0.D0
      close(1)
      INIT=1
  10  CONTINUE
      if(x.lt.xmin) then
        write(*,*)' x < xmin in structure functions '
	x=xmin
      endif
      if(x.gt.xmax) then
        write(*,*)' x > xmax in structure functions '
        x=xmax
      endif
      qsq=scale**2
      if(qsq.lt.qsqmin) then
        write(*,*)' q**2 < min q**2 in structure functions '
        qsq=qsqmin
      endif
      if(qsq.gt.qsqmax) then
*        write(*,*)' q**2 > max q**2 in structure functions '
        qsq=qsqmax
      endif
      XXX=X
      IF(X.LT.1.D-1) XXX=DLOG10(X)+1.1D0
      N=0
  70  N=N+1
      IF(XXX.GT.XX(N+1)) GOTO 70
      A=(XXX-XX(N))/(XX(N+1)-XX(N))
      RM=DLOG(QSQ/QSQMIN)/DLOG(2.D0)
      B=RM-DINT(RM)
      M=1+IDINT(RM)
      DO 60 I=1,8
      g(i)= (1.d0-a)*(1.d0-b)*f(i,n,m)+(1.d0-a)*b*f(i,n,m+1)
     .    + a*(1.d0-b)*f(i,n+1,m)  + a*b*f(i,n+1,m+1)
      IF(N.GE.ntenth) GOTO 65
      IF(I.EQ.7) GOTO 65
	  fac=(1.d0-b)*f(i,ntenth,m)+b*f(i,ntenth,m+1)
          G(I)=FAC**(G(I)/FAC)
  65  CONTINUE
      G(I)=G(I)*(1.D0-X)**N0(I)
  60  CONTINUE
      UPV=G(1)
      DNV=G(2)
      USEA=G(4)
      DSEA=G(8)
      STR=G(6)
      CHM=G(5)
      GLU=G(3)
      BOT=G(7)
      x=xsave  !restore x
      RETURN
      END
c
c     
c
      SUBROUTINE GRV(X,SCALE,istruc,UPV,DNV,SEA,STR,CHM,BTM,GLU)
C***************************************************************C
C                                                               C
C   GRV (HO,MSBAR) PARTON DISTRIBUTIONS WITH LAMBDA(4)=200 MEV  C
C                                                               C  
C   REFERENCE IS: M. Gluck, E. Reya and A. Vogt, DO-TH 91/07    C
C                                                               C
C   SCALE IS Q IN GEV, istruc IS IRRELEVANT HERE,                 C
C   ARGUMENT STRUCTURE COMPATIBLE WITH  HMRS..                  C
C                                                               C
C   THE RANGE OF APPLICABILITY IS:                              C
C   10**-5 < X < 1  AND  0.3 < Q**2 <  10**8                    C
C                                                               C
C***************************************************************C
      IMPLICIT REAL*8(A-H,O-Z)
      RTX=DSQRT(X)
      Y=1D0-X
      XLOG=DLOG(1D0/X)
      S=DLOG(DLOG(SCALE/0.248D0)/DLOG(DSQRT(0.3D0)/0.248D0))
      RTS=DSQRT(S)
        STR=0D0
        CHM=0D0
        BTM=0D0
C -- D VALENCE
      RN=0.459D0+0.315D0*RTS+0.515D0*S
      RA=0.624D0-0.031D0*S
      A=8.13D0-6.77D0*RTS+0.46D0*S
*      B=6.59D0-12.83D0*RTS+5.56D0*S
      B=6.59D0-12.83D0*RTS+5.65D0*S
      D=3.98D0+1.04D0*S-0.34D0*S**2
      DNV=RN*X**RA*(1D0+A*RTX+B*X)*Y**D
C -- U+D VALENCE
      RN=0.330D0+0.151D0*S-0.059D0*S**2+0.027*S**3
      RA=0.285D0
      A=-2.28D0+15.73D0*S-4.58D0*S**2
      B=56.7D0-53.6D0*S+11.21D0*S**2
      D=3.17D0+1.17D0*S-0.47D0*S**2+0.09D0*S**3
      UPVDNV=RN*X**RA*(1D0+A*RTX+B*X)*Y**D
      UPV=UPVDNV-DNV
C -- GLUON
      GA=1.128D0
      GB=1.575D0
      RA=0.323D0+1.653D0*S
      RB=0.811D0+2.044D0*S
      A=1.963D0*S-0.519D0*S**2
      B=0.078D0+6.24D0*S
      C=30.77D0-24.19D0*S
      D=3.188D0+0.720D0*S
      E=-0.881D0+2.687D0*S
      EP=2.466D0
      GLU=(X**RA*(A+B*X+C*X**2)*XLOG**RB
     .    +S**GA*DEXP(-E+DSQRT(EP*S**GB*XLOG)))*Y**D
C -- LIGHT SEA (U,D)
      GA=0.594D0
      GB=0.614D0
      RA=0.636D0-0.084D0*S
      RB=0D0
      A=1.121D0-0.193D0*S
      B=0.751D0-0.785D0*S
      C=8.57D0-1.763D0*S
      D=10.22D0+0.668D0*S
      E=3.784D0+1.280D0*S
      EP=1.808D0+0.980D0*S
      SEA=(X**RA*(A+B*X+C*X**2)*XLOG**RB
     .    +S**GA*DEXP(-E+DSQRT(EP*S**GB*XLOG)))*Y**D
C -- STRANGE SEA
      SSTR=0D0
      IF(S.LE.SSTR) RETURN
      GA=0.756D0
      GB=0.101D0
      RA=2.942D0-1.016D0*S
      RB=0D0
      A=-4.60D0+1.167D0*S
      B=9.31D0-1.324D0*S
      C=0D0
      D=11.49D0-1.198D0*S+0.053D0*S**2
      E=2.630D0+1.729D0*S
      EP=8.12D0
      STR=(S-SSTR)**GA*(1D0+A*RTX+B*X)/XLOG**RA
     .    *DEXP(-E+DSQRT(EP*S**GB*XLOG))*Y**D
C -- CHARM SEA
      SCHM=0.820D0
      IF(S.LE.SCHM) RETURN
      GA=0.980D0
      GB=0D0
      RA=-0.625D0-0.523D0*S
      RB=0D0
      A=0D0
      B=1.896D0+1.616D0*S
      C=0D0
      D=4.12D0+0.683D0*S
      E=4.36D0+1.328D0*S
      EP=0.677D0+0.679D0*S
      CHM=(S-SCHM)**GA*(1D0+A*RTX+B*X)/XLOG**RA
     .    *DEXP(-E+DSQRT(EP*S**GB*XLOG))*Y**D
C -- BOTTOM SEA
      SBTM=1.297D0
      IF(S.LE.SBTM) RETURN
      GA=0.990D0
      GB=0D0
      RA=-0.193D0*S
      RB=0D0
      A=0D0
      B=0D0
      C=0D0
      D=3.447D0+0.927D0*S
      E=4.68D0+1.259D0*S
      EP=1.892D0+2.199D0*S
      BTM=(S-SBTM)**GA*(1D0+A*RTX+B*X)/XLOG**RA
     .    *DEXP(-E+DSQRT(EP*S**GB*XLOG))*Y**D
      RETURN
      END
      SUBROUTINE XMRS(X,SCALE,istruc,UPV,DNV,USEA,DSEA,STR,CHM,BOT,GLU) 
C***************************************************************C      
C                                                               C
C    MRS style grids for CTEQ and GRV st. fns.                  C                                                   C                                                               C
C                                                               C
C    NOTE THE EXTRA ARGUMENT IN THIS SUBROUTINE MRSEB,          C
C    TO ALLOW FOR THE POSSIBILITY OF A *** DIFFERENT ***        C
C    UBAR AND DBAR SEA!                                         C
C                                                               C
C                         -*-                                   C
C                                                               C
C    (NOTE THAT X TIMES THE PARTON DISTRIBUTION FUNCTION        C
C    IS RETURNED I.E. G(X) = GLU/X ETC. IF IN DOUBT, CHECK THE  C
C    MOMENTUM SUM RULE! NOTE ALSO THAT SCALE=Q IN GEV)          C
C                                                               C
C                                                               C
C***************************************************************C
      IMPLICIT REAL*8(A-H,O-Z)
      parameter(nx=47,ntenth=21,nq=20)
      DIMENSION F(8,NX,nq),G(8),XX(NX),N0(8)
      DATA XX/1.d-5,2.d-5,4.d-5,6.d-5,8.d-5,
     .        1.D-4,2.D-4,4.D-4,6.D-4,8.D-4,
     .        1.D-3,2.D-3,4.D-3,6.D-3,8.D-3,
     .        1.D-2,2.D-2,4.D-2,6.D-2,8.D-2,
     .     .1D0,.125D0,.15D0,.175D0,.2D0,.225D0,.25D0,.275D0,
     .     .3D0,.325D0,.35D0,.375D0,.4D0,.425D0,.45D0,.475D0,
     .     .5D0,.525D0,.55D0,.575D0,.6D0,.65D0,.7D0,.75D0,
     .     .8D0,.9D0,1.D0/
      DATA XMIN,XMAX,QSQMIN,QSQMAX/1.D-5,1.D0,5.D0,1310720.D0/
      DATA N0/2,5,4,5,5,0,5,5/
*      DATA N0/3,4,4,10,0,8,0,10/
      DATA INIT/0/
      save xx,xmin,xmax,qsqmin,qsqmax,n0,init
      xsave=x  ! don't let x be altered if it's out of range!!
      IF(INIT.NE.0) GOTO 10
        if(istruc.eq.8)then
          open(unit=1,file='stfns/XMTS1.DAT',status='old')
	elseif(istruc.eq.9)then
          open(unit=1,file='stfns/XMTE1.DAT',status='old')
	elseif(istruc.eq.10)then
          open(unit=1,file='stfns/XMTB1.DAT',status='old')
	elseif(istruc.eq.11)then
          open(unit=1,file='stfns/XMTB2.DAT',status='old')
	elseif(istruc.eq.12)then
          open(unit=1,file='stfns/XMTSN1.DAT',status='old')
	elseif(istruc.eq.19)then
          open(unit=1,file='stfns/XTEQ1L.DAT',status='old')
	elseif(istruc.eq.20)then
          open(unit=1,file='stfns/XTEQ1M.DAT',status='old')
	elseif(istruc.eq.21)then
          open(unit=1,file='stfns/XTEQ1ML.DAT',status='old')
	elseif(istruc.eq.22)then
          open(unit=1,file='stfns/XTEQ1MS.DAT',status='old')
	elseif(istruc.eq.23)then
          open(unit=1,file='stfns/XGRV.DAT',status='old')
	elseif(istruc.eq.24)then
          open(unit=1,file='stfns/XTEQ2L.DAT',status='old')
	elseif(istruc.eq.25)then
          open(unit=1,file='stfns/XTEQ2M.DAT',status='old')
	elseif(istruc.eq.26)then
          open(unit=1,file='stfns/XTEQ2ML.DAT',status='old')
	elseif(istruc.eq.27)then
          open(unit=1,file='stfns/XTEQ2MS.DAT',status='old')
	elseif(istruc.eq.28)then
          open(unit=1,file='stfns/XTEQ2MF.DAT',status='old')
	elseif(istruc.eq.33)then
          open(unit=1,file='stfns/XGRV94.DAT',status='old')
	elseif(istruc.eq.34)then
          open(unit=1,file='stfns/XTEQ3L.DAT',status='old')
	elseif(istruc.eq.35)then
          open(unit=1,file='stfns/XTEQ3M.DAT',status='old')
	elseif(istruc.eq.111)then
          open(unit=1,file='stfns/XGRV94150.DAT',status='old')
	elseif(istruc.eq.112)then
          open(unit=1,file='stfns/XGRV94200.DAT',status='old')
	elseif(istruc.eq.113)then
          open(unit=1,file='stfns/XGRV94250.DAT',status='old')
	elseif(istruc.eq.114)then
          open(unit=1,file='stfns/XGRV94300.DAT',status='old')
	elseif(istruc.eq.115)then
          open(unit=1,file='stfns/XGRV94350.DAT',status='old')
	elseif(istruc.eq.116)then
          open(unit=1,file='stfns/XGRV94400.DAT',status='old')
	endif
      DO 20 N=1,nx-1
      DO 20 M=1,nq-1
      READ(1,50)F(1,N,M),F(2,N,M),F(3,N,M),F(4,N,M),F(5,N,M),F(7,N,M),
     .          F(6,N,M),F(8,N,M)
C 1=UV 2=DV 3=GLUE 4=UBAR 5=CBAR 7=BBAR 6=SBAR 8=DBAR
         DO 25 I=1,8
  25     F(I,N,M)=F(I,N,M)/(1.D0-XX(N))**N0(I)
  20  CONTINUE
      DO 31 J=1,NTENTH-1
      XX(J)=DLOG10(XX(J))+1.1D0
      DO 31 I=1,8
*
* skip over distributions that are zero - bot and chm for GRV
*
      IF(I.EQ.7) GOTO 31
      IF(I.EQ.5.and.istruc.eq.33) GOTO 31
      IF(I.EQ.5.and.istruc.eq.111) GOTO 31
      IF(I.EQ.5.and.istruc.eq.112) GOTO 31
      IF(I.EQ.5.and.istruc.eq.113) GOTO 31
      IF(I.EQ.5.and.istruc.eq.114) GOTO 31
      IF(I.EQ.5.and.istruc.eq.115) GOTO 31
      IF(I.EQ.5.and.istruc.eq.116) GOTO 31
      DO 30 K=1,nq-1
  30  F(I,J,K)=DLOG(F(I,J,K))*F(I,ntenth,K)/DLOG(F(I,ntenth,K))
  31  continue
  50  format(8e11.4)
      DO 40 I=1,8
      DO 40 M=1,nq-1
  40  F(I,nx,M)=0.D0
      close(1)
      INIT=1
  10  CONTINUE
      if(x.lt.xmin) then
        write(*,*)' x < xmin in structure functions '
	x=xmin
      endif
      if(x.gt.xmax) then
        write(*,*)' x > xmax in structure functions '
        x=xmax
      endif
      qsq=scale**2
      if(qsq.lt.qsqmin) then
        write(*,*)' q**2 < min q**2 in structure functions '
        qsq=qsqmin
      endif
      if(qsq.gt.qsqmax) then
*        write(*,*)' q**2 > max q**2 in structure functions '
        qsq=qsqmax
      endif
      XXX=X
      IF(X.LT.1.D-1) XXX=DLOG10(X)+1.1D0
      N=0
  70  N=N+1
      IF(XXX.GT.XX(N+1)) GOTO 70
      A=(XXX-XX(N))/(XX(N+1)-XX(N))
      RM=DLOG(QSQ/QSQMIN)/DLOG(2.D0)
      B=RM-DINT(RM)
      M=1+IDINT(RM)
      DO 60 I=1,8
      g(i)= (1.d0-a)*(1.d0-b)*f(i,n,m)+(1.d0-a)*b*f(i,n,m+1)
     .    + a*(1.d0-b)*f(i,n+1,m)  + a*b*f(i,n+1,m+1)
      IF(N.GE.ntenth) GOTO 65
*
* skip over distributions that are zero - bot and chm for GRV
*
      IF(I.EQ.7) GOTO 65
      IF(I.EQ.5.and.istruc.eq.33) GOTO 65
      IF(I.EQ.5.and.istruc.eq.111) GOTO 65
      IF(I.EQ.5.and.istruc.eq.112) GOTO 65
      IF(I.EQ.5.and.istruc.eq.113) GOTO 65
      IF(I.EQ.5.and.istruc.eq.114) GOTO 65
      IF(I.EQ.5.and.istruc.eq.115) GOTO 65
      IF(I.EQ.5.and.istruc.eq.116) GOTO 65
	  fac=(1.d0-b)*f(i,ntenth,m)+b*f(i,ntenth,m+1)
          G(I)=FAC**(G(I)/FAC)
  65  CONTINUE
      G(I)=G(I)*(1.D0-X)**N0(I)
  60  CONTINUE
      UPV=G(1)
      DNV=G(2)
      USEA=G(4)
      DSEA=G(8)
      STR=G(6)
      CHM=G(5)
      GLU=G(3)
      BOT=G(7)
      x=xsave  !restore x
      RETURN
      END
c
