************************************************************************
* MATLIB.FOR contains all the exact matrix elements for N partons.     *
* Version 1.4: 15 -10 - 1990.                                          *
* There are five external references:                                  *
* SUBROUTINE SPHEL(PLAB(4,*),NPART,IQ,IQB,IR,IRB,OUT(2),L)             *
* SUBROUTINE Q0S(PLAB(4,*),NPART,IHRN,OUT(2))                          *
* SUBROUTINE Q2S(PLAB(4,*),NPART,IQ,IQB,RMQ,IHRN,OUT(2))               *
* SUBROUTINE Q4S(PLAB(4,*),NPART,IQ,IQB,RMQ,IQP,IQBP,RMQP,IHRN,OUT(2)) *
* SUBROUTINE Q6S(PLAB(4,*),NPART,I1,IB1,I2,IB2,I3,IB3,IW,IHRN,OUT(5))  *
* FUNCTION RN(IDUMMY) -- RANDOM NUMBER GENERATOR.                      *
************************************************************************
*
************************************************************************
* SUBROUTINE SPHEL(PLAB(4,*),NPART,IQ,IQB,IR,IRB,OUT(2),L)             *
* SPHEL is an approximation of the exact M^2 based on special hel.     *
* amplitudes. L is the number of quark pairs present in the process.   *
* L=3 is approximated by 0.                                            *
************************************************************************
* Input: PLAB(4,*)  input momenta                                      *
*        NPART      total number of particles                          *
*        IQ,IQB     first quark pair, valid for L=1,2                  *
*        IR,IRB     second quark pair, valid for L=2                   *
*        OUT(2)     L=0,1 returns only OUT(1)                          *
*                   L=2 returns OUT(1)=diff. flav., OUT(2)=same flav.  *
*        L          number of quark pairs.                             *
* ==> In principle SPHEL works for NPART<=10, however in NJETS itself  *
*     the maximum is set to NPART=8, because RAMBO cannot handle       *
*     nine parton events very well (weight gets too big)               *
************************************************************************
      SUBROUTINE SPHEL(PLAB,NPART,IQ,IQB,IR,IRB,OUT,L)
      PARAMETER(A0=1.0,A1=1.0,A2=1.0)
      IMPLICIT REAL (A-H,O-Y)
      REAL*8 PLAB(4,*),OUT(2)
      IF (L.EQ.0) THEN
        CALL SPHEL0(PLAB,NPART,AR,SR,CR)
        OUT(1)=A0*AR*SR*CR
      END IF
      IF (L.EQ.1) THEN
        CALL SPHEL1(PLAB,NPART-2,IQ,IQB,AR,SR,CR)
        OUT(1)=A1*AR*SR*CR
      END IF
      IF (L.EQ.2) THEN
        CALL SPHEL2(PLAB,NPART-4,IQ,IQB,IR,IRB,AR,SR,CR)
        OUT(1)=A2*AR*SR*CR
        CALL SPHEL2(PLAB,NPART-4,IQ,IRB,IR,IQB,AR,SR,CR)
        OUT(2)=OUT(1)+A2*AR*SR*CR
      END IF
*
      END
*
************************************************************************
* SPHEL0 calculates the contribution to n gluon scattering of          *
* the helicity combinations where all but two gluons have the same     *
* helicity. Colour matrix is taken to be diagonal.                     *
*                                                                      *
* Conventions: K      number of gluons                                 *
*              PLAB   set of momenta all outgoing ( REAL*8 )           *
*              AR,SR,CR the output parameters                          *
* Limitation: K<=10 ( value of KUP )                                   *
************************************************************************
      SUBROUTINE SPHEL0(PLAB,K,AR,SR,CR)
      PARAMETER(KUP=10,NCOL=3)
      IMPLICIT REAL (A-H,O-Y)
      REAL*8 PLAB(4,*)
      DIMENSION PR(KUP,KUP),IP(KUP),ICOUNT(KUP),IPOS(KUP),IDIR(KUP)
*
      PR4=0.0
      DO 10 I=1,K-1
        DO 10 J=I+1,K
          PR(I,J)=REAL(+PLAB(4,I)*PLAB(4,J)-PLAB(1,I)*PLAB(1,J)
     .                 -PLAB(2,I)*PLAB(2,J)-PLAB(3,I)*PLAB(3,J))
          PR(J,I)=PR(I,J)
          PR4=PR4+PR(I,J)**4
   10 CONTINUE
*
      FSTART=PR4/PR(K,1)
      DO 20 I=1,K-1
        FSTART=FSTART/PR(I,I+1)
   20 CONTINUE
*
      DO 30 I=1,K
        IP(I)=I
        IPOS(I)=I
        ICOUNT(I)=I-2
        IDIR(I)=-1
   30 CONTINUE
      IP(K+1)=1
*
      AR=FSTART
  101 I=K
  102 IF (ICOUNT(I).GT.0) THEN
        IC=IPOS(I)
        IHELP=IP(IC+IDIR(I))
        IP(IC+IDIR(I))=IP(IC)
        IP(IC)=IHELP
        IPOS(I)=IC+IDIR(I)
        IPOS(IP(IC))=IC
        IAA=IP(IC-IDIR(I))
        IBA=IP(IC+IDIR(I))
        ICA=IP(IC)
        IDA=IP(IC+2*IDIR(I))
        FSTART=FSTART*PR(IAA,IBA)*PR(ICA,IDA)/PR(IAA,ICA)/PR(IDA,IBA)
        AR=AR+FSTART
        ICOUNT(I)=ICOUNT(I)-1
        GOTO 101
      ELSE
        ICOUNT(I)=I-2
        IDIR(I)=-IDIR(I)
        I=I-1
        IF (I.GT.3) GOTO 102
      END IF
*
      AR=4.0*AR
      SR=( 2.0**K - 2.0*(K+1) ) / ( K*(K-1.0) )
      CR=4.0*(NCOL/2.0)**(K-2)*(NCOL**2-1.0)
*
      END
*
************************************************************************
* SPHEL1 calculates the contribution to qqb n-2 gluon scattering of    *
* the helicity combinations where all but one gluon has the same       *
* helicity. Colour matrix is taken to be diagonal.                     *
*                                                                      *
* Conventions: K      number of gluons                                 *
*              PLAB   set of momenta all outgoing ( REAL*8 )           *
*              IQ,IQB positions of quarks in PLAB                      *
*              AR,SR,CR the output parameters                          *
* Limitation: K<=8 ( value of KUP-2 )                                  *
************************************************************************
      SUBROUTINE SPHEL1(PLAB,K,IQ,IQB,AR,SR,CR)
      PARAMETER(KUP=10,NCOL=3)
      IMPLICIT REAL (A-H,O-Y)
      REAL*8 PLAB(4,*)
      DIMENSION PR(KUP,KUP),IP(0:KUP),ICOUNT(KUP),IPOS(KUP),IDIR(KUP)
      DIMENSION IX(KUP)
*
* Put quarks on places 1 and K+2
*
      DO 15 I=1,K+2
        IX(I)=I
   15 CONTINUE
      ITEMP=IX(IQ)
      IX(IQ)=IX(1)
      IX(1)=ITEMP
      IF (IQB.EQ.1) THEN
        ITEMP=IX(IQ)
        IX(IQ)=IX(K+2)
        IX(K+2)=ITEMP
      ELSE
        ITEMP=IX(IQB)
        IX(IQB)=IX(K+2)
        IX(K+2)=ITEMP
      END IF
*
      DO 20 I=1,K+1
        DO 20 J=I+1,K+2
          PR(I,J)=REAL(+PLAB(4,IX(I))*PLAB(4,IX(J))
     .                 -PLAB(1,IX(I))*PLAB(1,IX(J))
     .                 -PLAB(2,IX(I))*PLAB(2,IX(J))
     .                 -PLAB(3,IX(I))*PLAB(3,IX(J)))
          PR(J,I)=PR(I,J)
   20 CONTINUE
*
      FSTART=0.0
      DO 30 I=2,K+1
        FSTART=FSTART+PR(1,I)**3*PR(K+2,I)+PR(K+2,I)**3*PR(1,I)
   30 CONTINUE
      FSTART=FSTART/PR(1,K+2)
      DO 35 I=1,K+1
        FSTART=FSTART/PR(I,I+1)
   35 CONTINUE
*
      DO 40 I=1,K
        IP(I)=I
        IPOS(I)=I
        ICOUNT(I)=I-1
        IDIR(I)=-1
   40 CONTINUE
      IP(0)=0
      IP(K+1)=K+1
*
      AR=FSTART
  101 I=K
  102 IF (ICOUNT(I).GT.0) THEN
        IC=IPOS(I)
        IHELP=IP(IC+IDIR(I))
        IP(IC+IDIR(I))=IP(IC)
        IP(IC)=IHELP
        IPOS(I)=IC+IDIR(I)
        IPOS(IP(IC))=IC
        IAA=IP(IC-IDIR(I))+1
        IBA=IP(IC+IDIR(I))+1
        ICA=IP(IC)+1
        IDA=IP(IC+2*IDIR(I))+1
        FSTART=FSTART*PR(IAA,IBA)*PR(ICA,IDA)/PR(IAA,ICA)/PR(IDA,IBA)
        AR=AR+FSTART
        ICOUNT(I)=ICOUNT(I)-1
        GOTO 101
      ELSE
        ICOUNT(I)=I-1
        IDIR(I)=-IDIR(I)
        I=I-1
        IF (I.GT.1) GOTO 102
      END IF
*
      AR=2.0*AR
      SR=( 2.0**K - 2.0 ) / ( 2.0*K )
      CR=2.0*(NCOL/2.0)**(K-1)*(NCOL**2-1.0)
*
      END
*
************************************************************************
* SPHEL2 calculates the contribution to qqb rrb n-4 gluon scattering   *
* of the helicity combinations where all gluons have the same          *
* helicity. Colour matrix is taken to be diagonal.                     *
*                                                                      *
* Conventions: K      number of gluons                                 *
*              PLAB   set of momenta all outgoing ( REAL*8 )           *
*              IQ,IQB positions of quarks in PLAB                      *
*              IR,IRB positions of quarks in PLAB                      *
*              AR,SR,CR the output parameters                          *
* Limitation: K<=6 ( value of KUP-4 )                                  *
************************************************************************
      SUBROUTINE SPHEL2(PLAB,K,IQ,IQB,IR,IRB,AR,SR,CR)
      PARAMETER(KUP=10,NCOL=3)
      IMPLICIT REAL (A-H,O-Y)
      REAL*8 PLAB(4,*)
      DIMENSION P(0:3,KUP)
      DIMENSION PR(KUP,KUP),IP(0:KUP),ICOUNT(KUP),IPOS(KUP),IDIR(KUP)
*
      CALL MAPEVE(4+K,PLAB,P,SCALE)
      CALL RESHQS(P,IQ,IQB,IR,IRB,4)
*
      DO 20 I=1,K+3
        DO 20 J=I+1,K+4
          PR(I,J)=REAL(+P(0,I)*P(0,J)-P(1,I)*P(1,J)
     .                 -P(2,I)*P(2,J)-P(3,I)*P(3,J))
          PR(J,I)=PR(I,J)
   20 CONTINUE
*
      AR=2.0*(PR(1,3)**2+PR(2,4)**2+PR(1,4)**2+PR(2,3)**2)/
     .                    (PR(1,2)*PR(3,4))
*
      IF (K.GT.0) THEN
        FACT=0.0
        DO 40 I=1,K
          IP(I)=I
          IPOS(I)=I
          ICOUNT(I)=I-1
          IDIR(I)=-1
   40   CONTINUE
*
  101   I=K
        SOM1=PR(1,4)*PR(2,3)/PR(1,4+IP(1))/PR(2,4+IP(K))
        DO 35 J=1,K-1
          SOM1=SOM1/PR(4+IP(J),4+IP(J+1))
   35   CONTINUE
        SOM2=0.0
        DO 36 J=1,K+1
          IF (J.EQ.1) THEN
            SOM2=SOM2+PR(1,4+IP(1))/PR(1,4)/PR(3,4+IP(1))
          ELSE IF (J.EQ.(K+1)) THEN
            SOM2=SOM2+PR(2,4+IP(K))/PR(3,2)/PR(4,4+IP(K))
          ELSE
            SOM2=SOM2+PR(4+IP(J-1),4+IP(J))
     .       /PR(4,4+IP(J-1))/PR(3,4+IP(J))
          END IF
   36   CONTINUE
        FACT=FACT+ABS(SOM1*SOM2)
*
  102   IF (ICOUNT(I).GT.0) THEN
          IC=IPOS(I)
          IHELP=IP(IC+IDIR(I))
          IP(IC+IDIR(I))=IP(IC)
          IP(IC)=IHELP
          IPOS(I)=IC+IDIR(I)
          IPOS(IP(IC))=IC
          ICOUNT(I)=ICOUNT(I)-1
          GOTO 101
        ELSE
          ICOUNT(I)=I-1
          IDIR(I)=-IDIR(I)
          I=I-1
          IF (I.GT.1) GOTO 102
        END IF
      ELSE
        FACT=1.0
      END IF
*
      AR=ABS(AR*FACT)/(SCALE**K)/(SCALE**K)
      SR=2.0**(K-1.0)
      CR=(NCOL/2.0)**K*(NCOL**2-1.0)
*
      END
*
************************************************************************
* Q0S contains all the exact matrix elements for 4,5,6 and 7 gluon     *
* scattering processes. The central calling routine Q0S determines     *
* which matrix element to call. The results are returned in            *
* OUT(2), with 1 the exact result and 2 the LO result.                 *
* If IHRN=1 then a MC over helicities is performed if it is faster     *
* than the full calculation (NPART>5)                                  *
************************************************************************
      SUBROUTINE Q0S(PLAB,NPART,IHRN,OUT)
      REAL*8 PLAB(4,*),OUT(2)
*
      IF (NPART.EQ.4) CALL Q0G4S(PLAB,OUT)
      IF (NPART.EQ.5) CALL Q0G5S(PLAB,OUT)
      IF (NPART.EQ.6) CALL Q0G6S(PLAB,IHRN,OUT)
      IF (NPART.EQ.7) CALL Q0G7S(PLAB,IHRN,OUT)
*
      END
*
************************************************************************
* SUBROUTINE Q0G4S(PLAB,OUT) calculates gg->gg.                        *
************************************************************************
* input:  Array with 4 momenta. The fourth component is the energy.    *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:4)             *
************************************************************************
* output: No spin and colour averaging, no bose factor.                *
*         Normally this would be 1/4 * 1/64 * 1/2                      *
*         OUT(1)/OUT(2) = EXACT/LO M^2 for gg->gg.                     *
************************************************************************
      SUBROUTINE Q0G4S(PLAB,OUT)
      IMPLICIT REAL (A-H,O-Y)
      REAL*8 PLAB(4,*),OUT(2)
*
* Calculate Mandelstam Variables
*
      S12=+REAL(PLAB(4,1)*PLAB(4,2)-PLAB(1,1)*PLAB(1,2)
     .         -PLAB(2,1)*PLAB(2,2)-PLAB(3,1)*PLAB(3,2))
      S13=-REAL(PLAB(4,1)*PLAB(4,3)-PLAB(1,1)*PLAB(1,3)
     .         -PLAB(2,1)*PLAB(2,3)-PLAB(3,1)*PLAB(3,3))
      S14=-S12-S13
      OUT(1)=288.0*(S12**4+S13**4+S14**4)*
     .  (+1.0/(S12**2*S13**2) +1.0/(S12**2*S14**2) +1.0/(S13**2*S14**2))
      OUT(2)=OUT(1)
*
      END
*
************************************************************************
* SUBROUTINE Q0G5S(PLAB,OUT) calculates gg->ggg.                       *
************************************************************************
* input:  Array with 5 momenta. The fourth component is the energy.    *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:5)             *
************************************************************************
* output: No spin and colour averaging, no bose factor.                *
*         Normally this would be 1/4 * 1/64 * 1/6                      *
*         OUT(1)/OUT(2) = EXACT/LO M^2 for gg->ggg.                    *
************************************************************************
      SUBROUTINE Q0G5S(PLAB,OUT)
      IMPLICIT REAL (A-H,O-Y)
      REAL*8 PLAB(4,*),OUT(2)
*
* Calculate Mandelstam Variables
*
      S12=+REAL(PLAB(4,1)*PLAB(4,2)-PLAB(1,1)*PLAB(1,2)
     .         -PLAB(2,1)*PLAB(2,2)-PLAB(3,1)*PLAB(3,2))
      S13=-REAL(PLAB(4,1)*PLAB(4,3)-PLAB(1,1)*PLAB(1,3)
     .         -PLAB(2,1)*PLAB(2,3)-PLAB(3,1)*PLAB(3,3))/S12
      S14=-REAL(PLAB(4,1)*PLAB(4,4)-PLAB(1,1)*PLAB(1,4)
     .         -PLAB(2,1)*PLAB(2,4)-PLAB(3,1)*PLAB(3,4))/S12
      S23=-REAL(PLAB(4,2)*PLAB(4,3)-PLAB(1,2)*PLAB(1,3)
     .         -PLAB(2,2)*PLAB(2,3)-PLAB(3,2)*PLAB(3,3))/S12
      S24=-REAL(PLAB(4,2)*PLAB(4,4)-PLAB(1,2)*PLAB(1,4)
     .         -PLAB(2,2)*PLAB(2,4)-PLAB(3,2)*PLAB(3,4))/S12
      FACTOR=S12
      S12=1.0
      S34=-S12-S13-S14-S23-S24
      S15=-S12-S13-S14
      S25=-S12-S23-S24
      S35=-S13-S23-S34
      S45=-S14-S24-S34
      OUT(1)=+432.0/FACTOR*
     .    (+S12**4+S13**4+S14**4+S15**4+S23**4
     .     +S24**4+S25**4+S34**4+S35**4+S45**4)
     . * ( +1.0/(S12*S23*S34*S45*S15) + 1.0/(S12*S23*S35*S45*S14)
     .     +1.0/(S12*S24*S45*S35*S13) + 1.0/(S12*S24*S34*S35*S15)
     .     +1.0/(S12*S25*S35*S34*S14) + 1.0/(S12*S25*S45*S34*S13)
     .     +1.0/(S13*S34*S24*S25*S15) + 1.0/(S13*S35*S25*S24*S14)
     .     +1.0/(S13*S23*S24*S45*S15) + 1.0/(S13*S23*S25*S45*S14)
     .     +1.0/(S14*S34*S23*S25*S15) + 1.0/(S14*S24*S23*S35*S15) )
      OUT(2)=OUT(1)
*
      END
*
************************************************************************
* SUBROUTINE Q0G6S(PLAB,IHRN,OUT) calculates gg->gggg.                 *
************************************************************************
* input:  Array with 6 momenta. The fourth component is the energy.    *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:6)             *
************************************************************************
* output: No spin and colour averaging, no bose factor.                *
*         Normally this would be 1/4 * 1/64 * 1/24                     *
*         OUT(1)/OUT(2) = EXACT/LO M^2 for gg->gggg.                   *
************************************************************************
* Literature:                                                          *
*   1) THE SIX-GLUON PROCESS AS AN EXAMPLE OF WEYL - VD WAERDEN        *
*      SPINOR CALCULUS, by BERENDS AND GIELE                           *
*   2) EXACT AND APPROXIMATE EXPRESSIONS FOR MULTI GLUON SCATTERING    *
*                       by BERENDS, GIELE and KUIJF                    *
************************************************************************
* Remark: If IHRN=1 only one helicity combination is evaluated.        *
************************************************************************
      SUBROUTINE Q0G6S(PLAB,IHRN,OUT)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION P(0:3,1:6)
      DIMENSION ZC(1:24,0:10)
      COMMON /CEES6/ ZC,PTNUM
      REAL*8 PLAB(4,*),OUT(2),RN
      SAVE COLLO,COLSUB,/CEES6/
      DATA COLLO, COLSUB / 2592.0, 6912.0/
*
* Transfrom Vectors, normalize vectors according to energies.
*
      CALL MAPEVE(6,PLAB,P,SCALE)
      FACTOR=1.0 / SCALE**4
*
      IF (IHRN.EQ.1) THEN
        IHELRN=1+INT(25*RN(1))
        IF (IHELRN.GT.10) IHELRN=11
      ELSE
        IHELRN=0
      END IF
*
      CALL CFILL6(P,IHELRN)
*
      OUT(1)=0D0
      OUT(2)=0D0
*
      IF ((IHRN.EQ.0).OR.(IHELRN.EQ.11)) THEN
        OUT(2)=COLLO * PTNUM * RLEAD6(ZC(1,0))
        OUT(1)=COLSUB* PTNUM * RSUB6(ZC(1,0))
      END IF
      DO 10 I=1,10
        IF ((IHRN.EQ.0).OR.(IHELRN.EQ.I)) THEN
          OUT(2)=OUT(2)+COLLO * RLEAD6(ZC(1,I))
          OUT(1)=OUT(1)+COLSUB* RSUB6(ZC(1,I))
        END IF
   10 CONTINUE
      OUT(1)=(OUT(1)+OUT(2)) * FACTOR
      OUT(2)=OUT(2) * FACTOR
*
      IF (IHRN.EQ.1) THEN
        IF (IHELRN.EQ.11) THEN
          OUT(1)=25.0/15.0*OUT(1)
          OUT(2)=25.0/15.0*OUT(2)
        ELSE
          OUT(1)=25.0*OUT(1)
          OUT(2)=25.0*OUT(2)
        END IF
      END IF
*
      END
*
************************************************************************
* FUNCTION RLEAD6(CBASE) returns the LO in colour contribution for     *
* a given set of invariant C-amplitudes.                               *
************************************************************************
      FUNCTION RLEAD6(CBASE)
      COMPLEX C(25:60),CBASE(1:24)
*
* First the C's directly expressed in the C(12xxxx)-base by subsyclicity
      C(25)=-( CBASE(24) + CBASE(14) + CBASE(17) + CBASE(18) )
      C(26)=-( CBASE(14) + CBASE(24) + CBASE(21) + CBASE(22) )
      C(29)=-( CBASE(10) + CBASE(21) + CBASE(24) + CBASE(23) )
      C(32)=-( CBASE(21) + CBASE(10) + CBASE( 7) + CBASE( 8) )
      C(35)=-( CBASE(17) + CBASE( 7) + CBASE( 9) + CBASE(10) )
      C(36)=-( CBASE( 7) + CBASE(17) + CBASE(14) + CBASE(13) )
      C(37)=-( CBASE( 1) + CBASE(11) + CBASE( 8) + CBASE( 7) )
      C(38)=-( CBASE( 2) + CBASE(12) + CBASE( 9) + CBASE(10) )
      C(39)=-( CBASE( 3) + CBASE(16) + CBASE(13) + CBASE(14) )
      C(40)=-( CBASE( 4) + CBASE(15) + CBASE(17) + CBASE(18) )
      C(41)=-( CBASE( 5) + CBASE(19) + CBASE(21) + CBASE(22) )
      C(42)=-( CBASE( 6) + CBASE(20) + CBASE(23) + CBASE(24) )
      C(44)=-( CBASE(20) + CBASE( 6) + CBASE( 3) + CBASE( 4) )
      C(46)=-( CBASE(16) + CBASE( 3) + CBASE( 5) + CBASE( 6) )
      C(47)=-( CBASE(11) + CBASE( 1) + CBASE( 3) + CBASE( 4) )
      C(48)=-( CBASE(12) + CBASE( 2) + CBASE( 5) + CBASE( 6) )
      C(49)=-( CBASE( 8) + CBASE(18) + CBASE(15) + CBASE(16) )
      C(50)=-( CBASE( 9) + CBASE(22) + CBASE(19) + CBASE(20) )
      C(51)=-( CBASE(23) + CBASE(13) + CBASE(15) + CBASE(16) )
      C(52)=-( CBASE(13) + CBASE(23) + CBASE(19) + CBASE(20) )
      C(55)=-( CBASE(15) + CBASE( 4) + CBASE( 1) + CBASE( 2) )
      C(56)=-( CBASE(18) + CBASE( 8) + CBASE(11) + CBASE(12) )
      C(57)=-( CBASE(22) + CBASE( 9) + CBASE(11) + CBASE(12) )
      C(60)=-( CBASE(19) + CBASE( 5) + CBASE( 2) + CBASE( 1) )
*
* The C's with a 2 in the middle
      C(27)=-( CBASE( 1) + C(37) + C(25) + CBASE(24) )
      C(28)=-( CBASE( 2) + C(38) + C(26) + CBASE(14) )
      C(30)=-( CBASE( 4) + C(40) + C(32) + CBASE(21) )
      C(31)=-( CBASE( 3) + C(39) + C(29) + CBASE(10) )
      C(33)=-( CBASE( 5) + C(41) + C(35) + CBASE(17) )
      C(34)=-( CBASE( 6) + C(42) + C(36) + CBASE( 7) )
      C(43)=-( CBASE( 8) + C(49) + C(44) + CBASE(20) )
      C(45)=-( CBASE( 9) + C(50) + C(46) + CBASE(16) )
      C(53)=-( CBASE(11) + C(47) + C(51) + CBASE(23) )
      C(54)=-( CBASE(12) + C(48) + C(52) + CBASE(13) )
      C(58)=-( CBASE(15) + C(55) + C(57) + CBASE(22) )
      C(59)=-( CBASE(18) + C(56) + C(60) + CBASE(19) )
*
* Square the 60 C's into RLEAD6
      RLEAD6=0.0
      DO 10 I=1,24
        RLEAD6=RLEAD6+CBASE(I)*CONJG(CBASE(I))
   10 CONTINUE
      DO 20 I=25,60
        RLEAD6=RLEAD6+C(I)*CONJG(C(I))
   20 CONTINUE
      END
*
************************************************************************
* FUNCTION RSUB6(CBASE) returns the NLO in colour contribution for     *
* a given set of invariant C-amplitudes.                               *
************************************************************************
      FUNCTION RSUB6(CBASE)
      COMPLEX CBASE(1:24),Z(24)
*
* First add the C's on each row. Only those above the diagonal
      Z( 1) = CBASE(13) + CBASE(14) + CBASE(21) + CBASE(23) + CBASE(24)
      Z( 2) = CBASE(13) + CBASE(14) + CBASE(17) + CBASE(23) + CBASE(24)
      Z( 3) = CBASE( 7) + CBASE( 9) + CBASE(10) + CBASE(21) + CBASE(22)
      Z( 4) = CBASE( 9) + CBASE(10) + CBASE(21) + CBASE(22) + CBASE(24)
      Z( 5) = CBASE( 7) + CBASE( 8) + CBASE(14) + CBASE(17) + CBASE(18)
      Z( 6) = CBASE( 7) + CBASE( 8) + CBASE(10) + CBASE(17) + CBASE(18)
      Z( 7) = CBASE(19) + CBASE(20)
      Z( 8) = CBASE(19) + CBASE(20) + CBASE(23)
      Z( 9) = CBASE(13) + CBASE(15) + CBASE(16)
      Z(10) = CBASE(15) + CBASE(16)
      Z(11) = CBASE(13) + CBASE(14) + CBASE(20) + CBASE(23) + CBASE(24)
      Z(12) = CBASE(13) + CBASE(14) + CBASE(16) + CBASE(23) + CBASE(24)
      Z(15) = CBASE(19) + CBASE(21) + CBASE(22)
      Z(16) = CBASE(21) + CBASE(22)
      Z(17) = CBASE(19) + CBASE(20)
      Z(18) = CBASE(19) + CBASE(20) + CBASE(22)
*
* Contract the Z's with the C-functions in the base.
      RSUB6=0.0
      DO 10 I=1,12
        RSUB6=RSUB6+CBASE(I)*CONJG(Z(I))
   10 CONTINUE
      DO 20 I=15,18
        RSUB6=RSUB6+CBASE(I)*CONJG(Z(I))
   20 CONTINUE
*
      END
*
************************************************************************
* CFILL6(P,IHELRN) calculates all the invariant C-amplitudes into      *
* ZC(1:60,0:10) in the COMMON /CEES6/                                  *
* Note: only 24 out of 60 are actually calculated. The others are      *
*       obtained by subcyclic relations.                               *
* If IHELRN<>0 then only that hel amplitude is considered.             *
************************************************************************
      SUBROUTINE CFILL6(P,IHELRN)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION P(0:3,1:6)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      DIMENSION ZC(1:24,0:10)
      COMMON /CEES6/ ZC,PTNUM
      DIMENSION M(24)
      COMMON /CPARMS/ M,P1,P2,P3,P4
      DIMENSION L(16,24)
      SAVE L,/DOTPR/,/PROPS/,/CEES6/,/CPARMS/
      DATA L
     ./1,2,3,4,5,6,1,2,3,4,5,6,7,8,9,10,1,2,3,4,6,5,1,2,4,3,5,7,6,9,8,
     . 10,1,2,3,5,6,4,1,3,4,2,6,7,5,10,8,9,1,2,3,5,4,6,1,3,2,4,6,5,7,
     . 8,10,9,1,2,3,6,4,5,1,4,2,3,7,5,6,9,10,8,1,2,3,6,5,4,1,4,3,2,7,
     . 6,5,10,9,8,1,2,4,5,6,3,2,3,4,1,8,9,5,10,6,7,1,2,4,5,3,6,2,3,1,
     . 4,8,5,9,6,10,7,1,2,4,6,3,5,2,4,1,3,9,5,8,7,10,6,1,2,4,6,5,3,2,
     . 4,3,1,9,8,5,10,7,6,1,2,4,3,5,6,2,1,3,4,5,8,9,6,7,10,1,2,4,3,6,
     . 5,2,1,4,3,5,9,8,7,6,10,1,2,5,6,3,4,3,4,1,2,10,6,8,7,9,5,1,2,5,
     . 6,4,3,3,4,2,1,10,8,6,9,7,5,1,2,5,3,4,6,3,1,2,4,6,8,10,5,7,9,1,
     . 2,5,3,6,4,3,1,4,2,6,10,8,7,5,9,1,2,5,4,6,3,3,2,4,1,8,10,6,9,5,
     . 7,1,2,5,4,3,6,3,2,1,4,8,6,10,5,9,7,1,2,6,3,4,5,4,1,2,3,7,9,10,
     . 5,6,8,1,2,6,3,5,4,4,1,3,2,7,10,9,6,5,8,1,2,6,4,5,3,4,2,3,1,9,
     . 10,7,8,5,6,1,2,6,4,3,5,4,2,1,3,9,7,10,5,8,6,1,2,6,5,3,4,4,3,1,
     . 2,10,7,9,6,8,5,1,2,6,5,4,3,4,3,2,1,10,9,7,8,6,5/
*
* Init spinors and propagators
*
      CALL SETGSP(P,6,0)
      CALL PRFILL(P,6)
*
* Put PT helicity denominators in ZC(i,0)
*
      IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.11)) THEN
        DO 10 I=1,24
          ZC(I,0)=1.0/(ZUD(L(1,I),L(2,I))*ZUD(L(2,I),L(3,I))*
     .                 ZUD(L(3,I),L(4,I))*ZUD(L(4,I),L(5,I))*
     .                 ZUD(L(5,I),L(6,I))*ZUD(L(6,I),L(1,I)))
   10   CONTINUE
*
* Put Sum of (PT-numerators)**2 in PTNUM
*
        PTNUM=0.0
        DO 20 I=1,5
          DO 20 J=I+1,6
            PTNUM=PTNUM+PRO2(I,J)**4
   20   CONTINUE
* premature exit when IHELRN=11
        IF (IHELRN.EQ.11) RETURN
      END IF
*
* Calculate all the C(3+;3-) functions in ZC(1:24,1:10)
*
      DO 40 I=1,24
        DO 30 J=1,6
          M(J)=L(J,I)
          M(6+J)=M(J)
          M(19-J)=M(J)
          M(25-J)=M(J)
   30   CONTINUE
        RA=PRO2(M(4),M(5))*PRO2(M(5),M(6))
        RB=PRO2(M(2),M(3))*PRO2(M(3),M(4))
        RC=PRO2(M(6),M(1))*PRO2(M(1),M(2))
        R1=RA*PRO23(M(1),M(2),M(3))
        R2=RB*PRO23(M(5),M(6),M(1))
        R3=RC*PRO23(M(3),M(4),M(5))
        R4=RA*RB*RC
        P1=R1
        P2=R2
        P3=R3
        P4=R4
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L( 7,I))) ZC(I,L( 7,I))=ZCPPP( 0)
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L( 9,I))) ZC(I,L( 9,I))=ZCPPM( 0)
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L(12,I))) ZC(I,L(12,I))=ZCPMP( 0)
        P3=R1
        P1=R3
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L( 8,I))) ZC(I,L( 8,I))=ZCPPM(16)
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L(14,I))) ZC(I,L(14,I))=ZCPPM(13)
        P2=R1
        P3=R2
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L(10,I))) ZC(I,L(10,I))=ZCPPP( 5)
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L(11,I))) ZC(I,L(11,I))=ZCPPM( 2)
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L(15,I))) ZC(I,L(15,I))=ZCPPM( 5)
        P1=R2
        P3=R3
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L(13,I))) ZC(I,L(13,I))=ZCPPM(17)
        P2=R3
        P3=R1
        IF((IHELRN.EQ.0).OR.(IHELRN.EQ.L(16,I))) ZC(I,L(16,I))=ZCPPP( 4)
   40 CONTINUE
*
      END
*
************************************************************************
* ZCPPP GIVES THE C(+++---) FUNCTION, OPTIMISED FOR NUMERIC USAGE.     *
* INPUT /COMMON/ CPARMS and ZFUNCT.                                    *
************************************************************************
      FUNCTION ZCPPP(I)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION M(24)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      COMMON /CPARMS/ M,P1,P2,P3,P4
      SAVE /DOTPR/,/PROPS/,/CPARMS/
*
      I1=M(1+I)
      I2=M(2+I)
      I3=M(3+I)
      I4=M(4+I)
      I5=M(5+I)
      I6=M(6+I)
      ZB=ZUD(I5,I6)*ZD(I2,I3)*
     .           (ZD(I1,I2)*ZUD(I4,I2)+ZD(I1,I3)*ZUD(I4,I3))
      ZC=ZUD(I4,I5)*ZD(I1,I2)*
     .           (ZD(I3,I1)*ZUD(I6,I1)+ZD(I3,I2)*ZUD(I6,I2))
      ZCPPP=+ ZB**2/P2 + ZC**2/P3 + ZB*ZC*PRO3(I1,I2,I3)/P4
      END
*
************************************************************************
* ZCPPM GIVES THE C(++-+--) FUNCTION, OPTIMISED FOR NUMERIC USAGE.     *
* INPUT /COMMON/ CPARMS and ZFUNCT.                                    *
************************************************************************
      FUNCTION ZCPPM(I)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION M(24)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      COMMON /CPARMS/ M,P1,P2,P3,P4
      SAVE /DOTPR/,/PROPS/,/CPARMS/
*
      I1=M(1+I)
      I2=M(2+I)
      I3=M(3+I)
      I4=M(4+I)
      I5=M(5+I)
      I6=M(6+I)
      ZA=ZUD(I4,I6)*ZD(I1,I2)*
     .           (ZD(I5,I1)*ZUD(I3,I1)+ZD(I5,I2)*ZUD(I3,I2))
      ZB=-ZUD(I3,I4)*ZD(I1,I5)*
     .           (ZD(I2,I1)*ZUD(I6,I1)+ZD(I2,I5)*ZUD(I6,I5))
      ZC=-ZUD(I3,I4)*ZD(I1,I2)*
     .           (ZD(I5,I1)*ZUD(I6,I1)+ZD(I5,I2)*ZUD(I6,I2))
      ZCPPM=+ ZA**2/P1 + ZB**2/P2 + ZC**2/P3
     .  + ( + ZA*ZB*PRO3(I3,I4,I5)
     .      + ZB*ZC*PRO3(I1,I2,I3)
     .      + ZC*ZA*PRO3(I2,I3,I4) )/P4
      END
*
************************************************************************
* ZCPPP GIVES THE C(+-+-+-) FUNCTION, OPTIMISED FOR NUMERIC USAGE.     *
* INPUT /COMMON/ CPARMS and ZFUNCT.                                    *
************************************************************************
      FUNCTION ZCPMP(I)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION M(24)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      COMMON /CPARMS/ M,P1,P2,P3,P4
      SAVE /DOTPR/,/PROPS/,/CPARMS/
*
      I1=M(1+I)
      I2=M(2+I)
      I3=M(3+I)
      I4=M(4+I)
      I5=M(5+I)
      I6=M(6+I)
      ZA=ZUD(I4,I6)*ZD(I1,I3)*
     .           (ZD(I5,I1)*ZUD(I2,I1)+ZD(I5,I3)*ZUD(I2,I3))
      ZB=ZUD(I2,I4)*ZD(I5,I1)*
     .           (ZD(I3,I5)*ZUD(I6,I5)+ZD(I3,I1)*ZUD(I6,I1))
      ZC=-ZUD(I2,I6)*ZD(I3,I5)*
     .           (ZD(I1,I3)*ZUD(I4,I3)+ZD(I1,I5)*ZUD(I4,I5))
      ZCPMP=+ ZA**2/P1 + ZB**2/P2 + ZC**2/P3
     .  + ( + ZA*ZB*PRO3(I3,I4,I5)
     .      + ZB*ZC*PRO3(I1,I2,I3)
     .      + ZC*ZA*PRO3(I2,I3,I4) )/P4
      END
*
************************************************************************
* SUBROUTINE Q0G7S(PLAB,IHRN,OUT) calculates gg->ggggg.                *
************************************************************************
* input:  Array with 7 momenta. The fourth component is the energy.    *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:7)             *
*         If IHRN=1 then only one helicity state is evaluated.         *
************************************************************************
* output: No spin and colour averaging, no bose factor.                *
*         Normally this would be 1/4 * 1/64 * 1/120                    *
*         OUT(1)/OUT(2) = Total EXACT/LO of gg->ggggg.                 *
************************************************************************
* Literature:                                                          *
*      EXACT AND APPROXIMATE EXPRESSIONS FOR MULTI GLUON SCATTERING    *
*                 by BERENDS, GIELE and KUIJF                          *
************************************************************************
      SUBROUTINE Q0G7S(PLAB,IHRN,OUT)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2),RN
      DIMENSION P(0:3,1:7)
      COMMON /CEES7/ ZC(1:360,0:35)
      SAVE /CEES7/,INIT
      DATA INIT/1/
*
* Initialize arrays.
*
      IF (INIT.EQ.1) THEN
        CALL MAKEIH
        CALL MAKMAT
        INIT=0
      END IF
*
* Transfrom Vectors, normalize vectors according to energies.
*
      CALL MAPEVE(7,PLAB,P,SCALE)
      FACTOR=1.0 / SCALE**6
*
      IF (IHRN.EQ.1) THEN
        IHELRN=1+INT(56*RN(1))
        IF (IHELRN.GT.35) IHELRN=36
      ELSE
        IHELRN=0
      END IF
*
      CALL CFILL7(P,IHELRN)
      DO 10 I=0,35
        IF ( (IHRN.EQ.0).OR.(MOD(IHELRN,36).EQ.I)) CALL EXPAND(ZC(1,I))
   10 CONTINUE
      CALL RLEAD7(PTLO,AMPLO,IHELRN)
      CALL RSUB7(PTNLO,AMPNLO,IHELRN)
      OUT(2)=PTLO * FACTOR
      OUT(1)=PTNLO * FACTOR
      OUT(2)=OUT(2)+AMPLO*FACTOR
      OUT(1)=OUT(1)+AMPNLO*FACTOR
      OUT(1)=OUT(1)+OUT(2)
*
      IF (IHRN.EQ.1) THEN
        IF (IHELRN.EQ.36) THEN
          OUT(1)=56.0/21.0*OUT(1)
          OUT(2)=56.0/21.0*OUT(2)
        ELSE
          OUT(1)=56.0*OUT(1)
          OUT(2)=56.0*OUT(2)
        END IF
      END IF
*
      END
*
************************************************************************
* SUBROUTINE RLEAD7(PT,REST,IHELRN) returns the LO in colour           *
* contribution for a given set of invariant C-amplitudes.              *
************************************************************************
      SUBROUTINE RLEAD7(PT,REST,IHELRN)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /CEES7/ ZC(1:360,0:35)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      SAVE /CEES7/,/PROPS/
*
      PTFACT=0.0
      REST=0.0
      PT=0.0
      IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.36)) THEN
        DO 10 I=1,6
          DO 10 J=I+1,7
   10       PTFACT=PTFACT+PRO2(I,J)**4
      END IF
*
      DO 20 I=0,35
        IF ((IHELRN.EQ.0).OR.(MOD(IHELRN,36).EQ.I)) THEN
          Z=(0.0,0.0)
          DO 30 J=1,360
   30       Z=Z+ZC(J,I)*CONJG(ZC(J,I))
          IF(I.EQ.0) THEN
            PT=PTFACT*REAL(Z)
          ELSE
            REST=REST+REAL(Z)
          END IF
        END IF
   20 CONTINUE
*
* colour factor and normalization: N^5*(N^2-1)*4
*
      PT=PT*243.0*32.0
      REST=REST*243.0*32.0
      END
*
************************************************************************
* SUBROUTINE RSUB7(PT,REST,IHELRN) returns the NLO colour contribution *
* for a given set of invariant C-amplitudes.                           *
************************************************************************
      SUBROUTINE RSUB7(PT,REST,IHELRN)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /CEES7/ ZC(1:360,0:35)
      INTEGER IMAT(2,16,360)
      COMMON /NLO/ IMAT
      DIMENSION Z(0:35)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      SAVE /NLO/,/CEES7/,/PROPS/
*
      PTFACT=0.0
      PT=0.0
      IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.36)) THEN
        DO 10 I=1,6
          DO 10 J=I+1,7
   10       PTFACT=PTFACT+PRO2(I,J)**4
      END IF
*
      DO 15 I=0,35
   15   Z(I)=(0.0,0.0)
      IHMOD=MOD(IHELRN,36)
      DO 20 I=1,360
        DO 22 K=0,35
          IF ((IHELRN.EQ.0).OR.(IHMOD.EQ.K)) THEN
            Z1=(0.0,0.0)
            DO 19 J=1,16
              IF(IMAT(1,J,I).GT.I) THEN
                Z1=Z1+IMAT(2,J,I)*ZC(IMAT(1,J,I),K)
              END IF
   19       CONTINUE
            Z(K)=Z(K)+ZC(I,K)*CONJG(Z1)
          END IF
   22   CONTINUE
   20 CONTINUE
*
      IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.36)) PT=REAL(Z(0))*PTFACT
      REST=0.0
      DO 30 I=1,35
        IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.I)) REST=REST+REAL(Z(I))
   30 CONTINUE
*
* colour factor and normalization: N^3*(N^2-1)*4
*
      PT=-PT*27.0*32.0*4.0
      REST=-REST*27.0*32.0*4.0
      END
*
************************************************************************
* SUBROUTINE CFILL7(P,IHELRN) calculates all the invariant C-functions *
* into ZC(1:360,0:35). In case IHELRN<>0 then just one (IHELRN MOD 36) *
* helicity amplitude is determined.                                    *
* Note: only 120 out of 360 are actually calculated.                   *
************************************************************************
      SUBROUTINE CFILL7(P,IHELRN)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION P(0:3,1:7)
      DIMENSION IPUT(3,35)
      COMMON /HELS/ IHTAB(35,120),IPERM(7,120)
      COMMON /CEES7/ ZC(1:360,0:35)
      COMMON /C7PARM/ M(28)
      SAVE IPUT,/C7PARM/,/CEES7/,/HELS/
      DATA IPUT/1,1,1,   1,2,1,   1,3,1, 20,2,-1, 7,1,1,   18,2,-1,
     .          1,4,1,   6,4,1,   7,2,1, 4,3,1,   4,4,1,   7,3,1,
     .          5,2,1,   21,2,-1, 6,1,1, 2,1,1,   2,2,1,   2,3,1,
     .          19,2,-1, 17,2,-1, 2,4,1, 18,4,-1, 5,3,1,   5,4,1,
     .          6,2,1,   3,1,1,   3,2,1, 3,3,1,   16,2,-1, 3,4,1,
     .          6,3,1,   4,1,1,   4,2,1, 15,2,-1, 5,1,1/
*
      CALL SETGSP(P,7,0)
*
      IF (IHELRN.NE.36) THEN
        CALL PRFILL(P,7)
        CALL ZZFILL(7)
      END IF
*
      DO 10 I=1,120
        DO 20 J=1,7
          M(J)=IPERM(J,I)
          M(J+7)=M(J)
          M(22-J)=M(J)
          M(29-J)=M(J)
   20   CONTINUE
        IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.36)) ZC(I,0)=ZPT(1)
        DO 30 J=1,35
          IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.J)) THEN
            IWHICH=IPUT(2,J)
            IF (IWHICH.EQ.1) THEN
              Z=ZCPPPM(IPUT(1,J))
            ELSE IF (IWHICH.EQ.2) THEN
              Z=ZCPPMP(IPUT(1,J))
            ELSE IF (IWHICH.EQ.3) THEN
              Z=ZCPPMM(IPUT(1,J))
            ELSE
              Z=ZCPMPM(IPUT(1,J))
            END IF
            ZC(I,IHTAB(J,I))=Z*IPUT(3,J)
          END IF
   30   CONTINUE
   10 CONTINUE
*
      END
*
************************************************************************
* ZPT evaluates C(++----), the ++ may be anywhere (no numerator!)      *
************************************************************************
      FUNCTION ZPT(INDEX)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /C7PARM/ M(28)
      SAVE /DOTPR/,/C7PARM/
*
      ZPT=1.0/(ZUD(M(INDEX  ),M(INDEX+1))*
     .         ZUD(M(INDEX+1),M(INDEX+2))*
     .         ZUD(M(INDEX+2),M(INDEX+3))*
     .         ZUD(M(INDEX+3),M(INDEX+4))*
     .         ZUD(M(INDEX+4),M(INDEX+5))*
     .         ZUD(M(INDEX+5),M(INDEX+6))*
     .         ZUD(M(INDEX+6),M(INDEX+7)) )
      END
*
************************************************************************
* ZCPPPM evaluates C(+++----).                                         *
************************************************************************
      FUNCTION ZCPPPM(INDEX)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /C7PARM/ M(28)
      SAVE /DOTPR/,/C7PARM/,/PROPS/,/ZFUNCT/
*
      I1=M(INDEX)
      I2=M(INDEX+1)
      I3=M(INDEX+2)
      I4=M(INDEX+3)
      I5=M(INDEX+4)
      I6=M(INDEX+5)
      I7=M(INDEX+6)
      Z12=ZUD(I4,I5)*ZUD(I5,I6)
      Z13=ZUD(I4,I5)*ZUD(I6,I7)
      Z23=ZUD(I5,I6)*ZUD(I6,I7)
      ZD12=ZD(I1,I2)
      P1=PRO2(I6,I7)*PRO2(I7,I1)*PRO2(I1,I2)
      P2=PRO2(I2,I3)*PRO2(I3,I4)*PRO2(I4,I5)*PRO2(I5,I6)
      P3L234=PRO23(I2,I3,I4)
      P3L345=PRO23(I3,I4,I5)
      P3L671=PRO23(I6,I7,I1)
      P3L712=PRO23(I7,I1,I2)
      ZA=ZD(I2,I3)*ZZ(I1,I2,I3,I4)
      ZB=ZD12*ZZ(I3,I1,I2,I7)
      ZT1=-Z23*ZUD(I7,I1)*ZZ(I1,I6,I7,I5)*ZA**2/
     . (P3L234*PRO2(I5,I6)*P3L671)
      ZT2=+ZD12*Z12*Z13*ZB*
     .  (ZD(I5,I6)*ZZ(I3,I1,I2,I6)+ZD(I5,I7)*ZZ(I3,I1,I2,I7))/
     . (P3L345*PRO2(I5,I6)*P1)
      ZT3=-ZD12*ZD(I2,I3)*Z13*Z13*
     .  (ZD(I3,I4)*ZZ(I1,I6,I7,I4)+ZD(I3,I5)*ZZ(I1,I6,I7,I5))/
     . (P3L345*P3L671)
      ZT4=-ZUD(I3,I4)*Z12*ZZ(I3,I4,I5,I6)*ZB**2/
     . (P3L345*PRO2(I5,I6)*P3L712)
      ZT5=-ZD(I2,I3)*Z13*Z23*ZA*
     .  (ZD(I6,I4)*ZZ(I1,I2,I3,I4)+ZD(I6,I5)*ZZ(I1,I2,I3,I5))/
     . (P2*P3L671)
      ZT6=+Z12*ZUD(I6,I7)*ZA*ZB*PRO3(I1,I2,I3)/(P1*P2)
      ZCPPPM=ZT1+ZT2+ZT3+ZT4+ZT5+ZT6
      END
*
************************************************************************
* ZCPPMP evaluates C(++-+---).                                         *
************************************************************************
      FUNCTION ZCPPMP(INDEX)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /C7PARM/ M(28)
      SAVE /DOTPR/,/C7PARM/,/PROPS/,/ZFUNCT/
*
      I1=M(INDEX)
      I2=M(INDEX+1)
      I3=M(INDEX+2)
      I4=M(INDEX+3)
      I5=M(INDEX+4)
      I6=M(INDEX+5)
      I7=M(INDEX+6)
      Z12=ZUD(I5,I6)*ZUD(I6,I7)
      P1=PRO2(I6,I7)*PRO2(I7,I1)*PRO2(I1,I2)
      P2=PRO2(I2,I3)*PRO2(I3,I4)*PRO2(I4,I5)*PRO2(I5,I6)
      P3L123=PRO23(I1,I2,I3)
      P3L234=PRO23(I2,I3,I4)
      P3L345=PRO23(I3,I4,I5)
      P3L456=PRO23(I4,I5,I6)
      P3L671=PRO23(I6,I7,I1)
      P3L712=PRO23(I7,I1,I2)
      ZA=ZD(I1,I2)*ZZ(I4,I1,I2,I3)
      ZB=ZD(I2,I4)*ZZ(I1,I2,I4,I3)
      ZC=ZD(I1,I2)*ZZ(I4,I1,I2,I7)
      ZH1=-ZD(I2,I4)*ZD(I6,I7)*ZUD(I2,I3)*ZC*
     . ZZ(I4,I5,I6,I7)+ZD(I3,I4)*ZUD(I1,I7)*ZA*(
     . ZZ(I4,I1,I2,I3)*ZD(I1,I6)+ZD(I1,I4)*ZD(I6,I7)*ZUD(I3,I7))
      ZH2=+ZD(I1,I6)*ZUD(I3,I5)*ZB*PRO2(I4,I5)+ZD(I4,I6)*
     . ZUD(I4,I5)*ZB*ZZ(I1,I6,I7,I3)-ZD(I1,I2)*
     . ZD(I5,I6)*ZUD(I2,I3)*ZUD(I3,I5)*ZZ(I1,I6,I7,I5)*ZD(I2,I4)
      ZH3=-ZUD(I3,I5)*ZA*ZC*PRO3(I2,I3,I4)-ZD(I1,I2)*ZB*ZUD(I4,I5)*
     . ZZ(I4,I1,I2,I7)*ZZ(I4,I1,I2,I3)-ZD(I1,I2)*ZB*ZUD(I3,I5)*
     . ZZ(I4,I5,I6,I7)*PRO2(I1,I2)
      ZT1=-ZUD(I4,I5)*Z12*ZZ(I4,I5,I6,I7)*ZA**2/
     . (P3L123*P3L456*PRO2(I6,I7))
      ZT2=-Z12*ZUD(I7,I1)*ZZ(I1,I6,I7,I5)*ZB**2/
     . (P3L234*PRO2(I5,I6)*P3L671)
      ZT3=+ZD(I1,I2)*ZUD(I3,I5)**2*Z12*ZC*
     .  (ZD(I5,I6)*ZZ(I4,I1,I2,I6)+ZD(I5,I7)*ZZ(I4,I1,I2,I7))/
     . (P3L345*PRO2(I5,I6)*P1)
      ZT4=-ZD(I1,I2)*ZD(I2,I4)*ZUD(I3,I5)**2*ZUD(I6,I7)**2*
     .  (ZD(I4,I3)*ZZ(I1,I6,I7,I3)+ZD(I4,I5)*ZZ(I1,I6,I7,I5))/
     . (P3L345*P3L671)
      ZT5=+ZUD(I3,I4)*ZUD(I3,I5)*ZUD(I5,I6)*ZZ(I4,I3,I5,I6)*ZC**2/
     . (P3L345*PRO2(I5,I6)*P3L712)
      ZT6=+ZD(I1,I2)*ZUD(I3,I4)*ZUD(I5,I6)*Z12*ZH1/
     . (PRO2(I2,I3)*PRO2(I3,I4)*P3L456*P1)
      ZT7=-ZUD(I3,I4)*ZUD(I5,I6)**2*ZZ(I4,I5,I6,I3)*ZC**2/
     . (PRO2(I3,I4)*P3L456*P3L712)
      ZT8=-ZD(I2,I4)*Z12*ZUD(I6,I7)*ZH2/
     . (P2*P3L671)
      ZT9=+Z12*ZH3/(P1*P2)
      ZCPPMP=ZT1+ZT2+ZT3+ZT4+ZT5+ZT6+ZT7+ZT8+ZT9
      END
*
************************************************************************
* ZCPPMM evaluates C(++--+--).                                         *
************************************************************************
      FUNCTION ZCPPMM(INDEX)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /C7PARM/ M(28)
      SAVE /DOTPR/,/C7PARM/,/PROPS/,/ZFUNCT/
*
      I1=M(INDEX)
      I2=M(INDEX+1)
      I3=M(INDEX+2)
      I4=M(INDEX+3)
      I5=M(INDEX+4)
      I6=M(INDEX+5)
      I7=M(INDEX+6)
      Z12=ZUD(I3,I4)*ZUD(I6,I7)
      P1=PRO2(I5,I6)*PRO2(I6,I7)*PRO2(I7,I1)
      P2=PRO2(I2,I3)*PRO2(I3,I4)*PRO2(I4,I5)
      P3L123=PRO23(I1,I2,I3)
      P3L234=PRO23(I2,I3,I4)
      P3L345=PRO23(I3,I4,I5)
      P3L456=PRO23(I4,I5,I6)
      P3L567=PRO23(I5,I6,I7)
      P3L671=PRO23(I6,I7,I1)
      P3L712=PRO23(I7,I1,I2)
      ZA=ZD(I1,I2)*ZZ(I5,I1,I2,I3)
      ZB=ZD(I1,I2)*ZZ(I5,I1,I2,I7)
      ZH1=ZD(I5,I7)*ZZ(I5,I1,I2,I7)*ZB+ZD(I5,I6)*ZB*ZZ(I5,I1,I2,I6)
     . -ZD(I1,I5)*ZD(I2,I5)*ZD(I5,I6)*ZUD(I6,I7)*PRO2(I1,I2)
      ZH2=-ZD(I3,I5)*ZZ(I5,I1,I2,I3)*ZA-ZD(I4,I5)*ZA*ZZ(I5,I1,I2,I4)
     . +ZD(I1,I5)*ZD(I2,I5)*ZD(I4,I5)*ZUD(I3,I4)*PRO2(I1,I2)
      ZH3=+ZD(I1,I5)*ZD(I2,I5)*Z12*PRO2(I1,I2)*
     .  (PRO2(I4,I5)+PRO2(I5,I6))
     . +ZD(I1,I5)*ZD(I2,I5)*ZUD(I4,I5)*ZUD(I5,I6)*
     .  ZZ(I5,I1,I2,I3)*ZZ(I5,I1,I2,I7)
     . +ZA*ZD(I1,I5)*ZUD(I1,I7)*ZUD(I4,I6)*PRO2(I3,I4)
     . +ZD(I1,I2)*ZD(I5,I6)*ZUD(I1,I7)*ZUD(I4,I6)*
     .  ZZ(I1,I2,I5,I3)*ZZ(I5,I1,I2,I6)
     . +ZB*(
     .  +ZD(I2,I5)*ZD(I5,I7)*ZUD(I2,I3)*ZUD(I4,I7)*ZUD(I5,I6)
     .  -ZD(I1,I5)*ZD(I5,I7)*ZUD(I1,I7)*ZUD(I3,I4)*ZUD(I5,I6)
     .  +ZD(I5,I7)*ZUD(I1,I7)*ZUD(I4,I6)*ZZ(I1,I2,I5,I3)
     .  +ZD(I2,I5)*ZUD(I2,I3)*ZUD(I4,I6)*(PRO2(I5,I6)+PRO2(I6,I7))
     .  -ZD(I1,I5)*ZUD(I1,I6)*ZUD(I3,I4)*(PRO2(I4,I5)+PRO2(I5,I6)) )
      ZT1=+ZUD(I4,I5)*ZUD(I4,I6)*ZUD(I6,I7)*ZZ(I5,I4,I6,I7)*ZA**2/
     . (P3L123*P3L456*PRO2(I6,I7))
      ZT2=-ZUD(I4,I5)*ZUD(I6,I7)**2*ZZ(I5,I6,I7,I4)*ZA**2/
     . (P3L123*PRO2(I4,I5)*P3L567)
      ZT3=-ZD(I1,I5)**2*ZUD(I3,I4)*Z12*(ZD(I1,I2)*ZUD(I1,I7)*
     . ZZ(I2,I3,I4,I6)-ZD(I2,I5)*ZUD(I5,I6)*ZZ(I2,I3,I4,I7))/
     . (P3L234*P1)
      ZT4=-ZD(I1,I2)*ZD(I1,I5)**2*Z12**2*
     . ZZ(I2,I3,I4,I1)/(P3L234*P3L567)
      ZT5=-ZD(I1,I5)*ZD(I2,I5)*Z12**2*
     . (ZD(I2,I3)*ZZ(I1,I6,I7,I3)+ZD(I2,I4)*ZZ(I1,I6,I7,I4))/
     . (P3L234*P3L671)
      ZT6=+ZD(I1,I2)*ZUD(I3,I4)*Z12*ZUD(I5,I6)*ZH1/
     .  (PRO2(I1,I2)*P3L345*P1)
      ZT7=-ZD(I1,I2)*ZD(I2,I5)**2*Z12**2*
     . ZZ(I1,I6,I7,I2)/(P3L345*P3L671)
      ZT8=-ZUD(I3,I4)**2*ZUD(I5,I6)*ZZ(I5,I3,I4,I6)*ZB**2/
     . (P3L345*PRO2(I5,I6)*P3L712)
      ZT9=-ZD(I1,I2)*ZUD(I4,I6)**2*ZUD(I1,I7)*ZUD(I6,I7)*ZA*
     . (ZD(I1,I7)*ZD(I5,I6)*ZUD(I3,I7)+ZD(I1,I6)*ZZ(I5,I4,I6,I3))/
     . (PRO2(I1,I2)*PRO2(I2,I3)*P3L456*PRO2(I6,I7)*PRO2(I7,I1))
      ZT10=-ZD(I1,I2)*ZUD(I2,I3)*ZUD(I3,I4)*ZUD(I4,I6)**2*ZB*
     . (ZD(I2,I3)*ZD(I4,I5)*ZUD(I3,I7)+ZD(I2,I4)*ZZ(I5,I4,I6,I7))/
     . (PRO2(I1,I2)*PRO2(I2,I3)*PRO2(I3,I4)*P3L456*PRO2(I7,I1))
      ZT11=+ZUD(I3,I4)*ZUD(I4,I6)*ZUD(I5,I6)*ZZ(I5,I4,I6,I3)*ZB**2/
     . (PRO2(I3,I4)*P3L456*P3L712)
      ZT12=-ZD(I1,I2)*Z12*ZUD(I4,I5)*ZUD(I6,I7)*ZH2/
     . (PRO2(I1,I2)*P2*P3L567)
      ZT13=+ZD(I2,I5)**2*Z12*ZUD(I6,I7)*(ZD(I1,I2)*ZUD(I2,I3)*
     . ZZ(I1,I6,I7,I4)-ZD(I1,I5)*ZUD(I4,I5)*ZZ(I1,I6,I7,I3))/(P2*P3L671)
      ZT14=-ZD(I1,I2)*Z12*ZH3/
     . (PRO2(I1,I2)*P2*P1)
      ZCPPMM=ZT1+ZT2+ZT3+ZT4+ZT5+ZT6+ZT7+
     .       ZT8+ZT9+ZT10+ZT11+ZT12+ZT13+ZT14
      END
*
************************************************************************
* ZCPMPM GIVES THE C(+-+-+--) FUNCTION, OPTIMISED FOR NUMERIC USAGE.   *
************************************************************************
      FUNCTION ZCPMPM(INDEX)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /C7PARM/ M(28)
      SAVE /DOTPR/,/C7PARM/,/PROPS/,/ZFUNCT/
*
      I1=M(INDEX)
      I2=M(INDEX+1)
      I3=M(INDEX+2)
      I4=M(INDEX+3)
      I5=M(INDEX+4)
      I6=M(INDEX+5)
      I7=M(INDEX+6)
      P1=PRO2(I5,I6)*PRO2(I6,I7)*PRO2(I7,I1)
      P2=PRO2(I1,I2)*PRO2(I2,I3)*PRO2(I3,I4)*PRO2(I4,I5)
      P3L123=PRO23(I1,I2,I3)
      P3L234=PRO23(I2,I3,I4)
      P3L345=PRO23(I3,I4,I5)
      P3L456=PRO23(I4,I5,I6)
      P3L567=PRO23(I5,I6,I7)
      P3L671=PRO23(I6,I7,I1)
      P3L712=PRO23(I7,I1,I2)
      ZA=ZD(I1,I3)*ZZ(I5,I1,I3,I2)
      ZB=ZD(I3,I5)*ZZ(I1,I3,I5,I4)
      ZH1=+ZD(I3,I5)*ZD(I6,I7)*ZUD(I2,I7)*ZUD(I2,I3)*ZZ(I5,I4,I6,I7)
     .    -ZD(I1,I5)*ZD(I6,I7)*ZUD(I1,I7)*ZUD(I2,I7)*ZZ(I5,I1,I3,I2)
     .    +ZD(I6,I1)*ZUD(I1,I7)*ZZ(I5,I1,I3,I2)**2
      ZH2=-ZD(I1,I5)**2*ZD(I2,I3)*ZD(I4,I5)*ZUD(I1,I2)*ZUD(I2,I4)**2
     .    -ZD(I1,I3)*ZD(I1,I5)*ZD(I4,I5)*ZUD(I1,I2)*ZUD(I2,I4)*
     .       ZZ(I5,I1,I3,I4)
     .    +ZD(I1,I3)*ZD(I3,I5)*ZUD(I3,I4)*ZZ(I5,I1,I3,I2)*
     .       ZZ(I5,I6,I7,I2)
      ZH3=+ZD(I1,I3)*ZUD(I1,I2)*ZUD(I2,I3)*ZZ(I1,I3,I5,I4)*
     .       ZZ(I1,I6,I7,I4)
     .    +ZD(I1,I5)*ZUD(I2,I4)*ZUD(I4,I5)*ZZ(I1,I6,I7,I2)*PRO2(I1,I2)
      ZH4=-ZD(I1,I7)*ZUD(I4,I6)*ZZ(I1,I3,I5,I4)*PRO2(I5,I6)
     .    -ZD(I5,I7)*ZUD(I5,I6)*ZZ(I1,I3,I5,I4)*ZZ(I1,I2,I7,I4)
     .    +ZD(I1,I3)*ZD(I6,I7)*ZUD(I3,I4)*ZUD(I4,I6)*ZZ(I1,I2,I7,I6)
      ZH5=
     .    +ZD(I1,I3)**2*ZD(I3,I5)**2*ZUD(I2,I3)*ZUD(I3,I4)*ZUD(I4,I6)*
     .       ZUD(I2,I7)*PRO2(I6,I7)
     .    +ZD(I1,I3)*ZD(I3,I5)*ZD(I1,I7)*ZD(I1,I5)*ZUD(I1,I7)*
     .       ZUD(I6,I7)*ZUD(I4,I5)*ZUD(I2,I4)*ZZ(I5,I1,I3,I2)
     .    -ZD(I1,I3)*ZD(I1,I5)**2*ZD(I3,I5)*ZUD(I1,I7)*ZUD(I4,I5)*
     .       ZUD(I2,I6)*ZUD(I2,I4)*PRO2(I1,I2)
     .    +ZD(I1,I3)*ZD(I1,I2)*ZD(I1,I5)*ZD(I3,I5)**2*ZUD(I1,I7)*
     .       ZUD(I2,I3)*ZUD(I4,I5)*ZUD(I2,I6)*ZUD(I2,I4)
     .    -ZD(I1,I3)*ZD(I3,I5)**2*ZD(I1,I5)*ZUD(I1,I7)*ZUD(I2,I3)*
     .       ZUD(I4,I6)*ZUD(I2,I4)*ZZ(I1,I3,I4,I5)
      ZH5=ZH5
     .    -ZD(I1,I3)**2*ZD(I1,I5)*ZD(I3,I5)*ZUD(I1,I7)*ZUD(I2,I3)*
     .       ZUD(I4,I6)*ZUD(I1,I2)*ZZ(I1,I3,I5,I4)
     .    +ZD(I1,I3)**2*ZD(I3,I5)**2*ZUD(I1,I7)*ZUD(I2,I3)*ZUD(I4,I6)*
     .       ZUD(I3,I4)*ZZ(I1,I6,I7,I2)
     .    -ZZ(I1,I3,I5,I4)*ZD(I1,I3)*ZD(I1,I5)**2*ZD(I3,I4)*
     .       ZUD(I1,I7)*ZUD(I4,I6)*ZUD(I2,I4)*ZUD(I1,I2)
     .    +PRO3(I1,I2,I3)*ZUD(I2,I4)*ZUD(I5,I6)*ZUD(I2,I7)*
     .       ZD(I3,I5)**2*ZD(I1,I5)**2*ZUD(I4,I5)
     .    -ZD(I3,I5)**2*ZUD(I5,I6)*ZUD(I2,I7)*ZD(I1,I3)*
     .        ZD(I1,I5)*ZUD(I4,I5)*ZUD(I3,I4)*ZZ(I5,I1,I3,I2)
     .    +ZD(I3,I5)**2*ZD(I1,I3)**2*ZUD(I5,I6)*ZUD(I2,I7)*
     .        ZUD(I2,I3)*ZUD(I3,I4)*ZZ(I5,I6,I7,I4)
      ZT1=+ZUD(I4,I5)*ZUD(I4,I6)*ZUD(I6,I7)*
     .  ZZ(I5,I4,I6,I7)*ZA**2/(P3L123*P3L456*PRO2(I6,I7))
      ZT2=-ZUD(I4,I5)*ZUD(I6,I7)**2*ZZ(I5,I6,I7,I4)*ZA**2/
     .   (P3L123*PRO2(I4,I5)*P3L567)
      ZT3=-ZD(I1,I5)**2*ZUD(I2,I4)**2*ZUD(I6,I7)*(ZD(I1,I3)*ZUD(I1,I7)*
     .  ZZ(I3,I2,I4,I6)-ZD(I3,I5)*ZUD(I5,I6)*ZZ(I3,I2,I4,I7))/
     .  (P3L234*P1)
      ZT4=-ZD(I1,I3)*ZD(I1,I5)**2*ZUD(I2,I4)**2*ZUD(I6,I7)**2*
     .  ZZ(I3,I2,I4,I1)/(P3L234*P3L567)
      ZT5=-ZD(I1,I5)*ZD(I3,I5)*ZUD(I2,I4)**2*ZUD(I6,I7)**2*(ZD(I3,I4)*
     .  ZZ(I1,I6,I7,I4)+ZD(I3,I2)*ZZ(I1,I6,I7,I2))/(P3L234*P3L671)
      ZT6=-ZUD(I1,I2)*ZUD(I6,I7)**2*ZZ(I1,I6,I7,I2)*ZB**2/
     .  (PRO2(I1,I2)*P3L345*P3L671)
      ZT7=-ZUD(I6,I7)*ZUD(I1,I2)*ZUD(I2,I7)*ZZ(I1,I2,I7,I6)*ZB**2/
     .  (P3L345*PRO2(I6,I7)*P3L712)
      ZT8=-ZD(I1,I3)*ZD(I3,I5)*ZUD(I4,I6)**2*ZUD(I2,I7)**2*(ZD(I5,I4)*
     .  ZZ(I1,I2,I7,I4)+ZD(I5,I6)*ZZ(I1,I2,I7,I6))/(P3L456*P3L712)
      ZT9=-ZD(I1,I3)**2*ZUD(I6,I7)*ZUD(I4,I6)**2*ZH1/(PRO2(I1,I2)*
     .  PRO2(I2,I3)*P3L456*PRO2(I6,I7)*PRO2(I7,I1))
      ZT10=-ZD(I1,I3)*ZUD(I4,I5)*ZUD(I6,I7)**2*ZH2/(P2*P3L567)
      ZT11=-ZD(I3,I5)**2*ZUD(I6,I7)**2*ZH3/(P2*P3L671)
      ZT12=+ZD(I3,I5)**2*ZUD(I2,I7)**2*ZUD(I6,I7)*ZH4/(PRO2(I3,I4)*
     .  PRO2(I4,I5)*PRO2(I5,I6)*PRO2(I6,I7)*P3L712)
      ZT13=-ZUD(I6,I7)*ZH5/(P1*P2)
      ZCPMPM=ZT1+ZT2+ZT3+ZT4+ZT5+ZT6+ZT7+ZT8+ZT9+ZT10+ZT11+ZT12+ZT13
      END
*
************************************************************************
* SUBROUTINE EXPAND(C) determines the 240 missing C-functions from     *
* the 120 which where calculated in CFILL7.                            *
* No tabels are used (just for the sake of speed)                      *
************************************************************************
      SUBROUTINE EXPAND(C)
      COMPLEX C(360)
*
      C(133)=C(43)+C(29)+C(26)+C(25)+C(71)+C(68)+C(67)+C(54)+C(53)+C(50)
      C(121)= +C( 96) +C( 95) +C( 92) +C( 78) +C(120)
      C(122)= +C(114) +C(113) +C(110) +C(120) +C( 78)
      C(125)= +C(118) +C(117) +C(120) +C(110) +C( 60)
      C(128)= +C( 54) +C( 53) +C( 50) +C( 60) +C(110)
      C(131)= +C( 58) +C( 57) +C( 60) +C( 50) +C( 92)
      C(132)= +C( 76) +C( 75) +C( 78) +C( 92) +C( 50)
      C(139)= +C(119) +C(120) +C(117) +C(106) +C( 42)
      C(142)= +C( 36) +C( 35) +C( 32) +C( 42) +C(106)
      C(145)= +C( 40) +C( 39) +C( 42) +C( 32) +C( 88)
      C(146)= +C( 77) +C( 78) +C( 75) +C( 88) +C( 32)
      C(153)= +C( 86) +C( 85) +C( 88) +C( 75) +C(117)
      C(154)= +C(104) +C(103) +C(106) +C(117) +C( 75)
      C(159)= +C( 41) +C( 42) +C( 39) +C( 28) +C( 70)
      C(160)= +C( 59) +C( 60) +C( 57) +C( 70) +C( 28)
      C(167)= +C( 68) +C( 67) +C( 70) +C( 57) +C(113)
      C(168)= +C(105) +C(106) +C(103) +C(113) +C( 57)
      C(171)= +C(109) +C(110) +C(113) +C(103) +C( 39)
      C(174)= +C( 26) +C( 25) +C( 28) +C( 39) +C(103)
      C(181)= +C( 69) +C( 70) +C( 67) +C( 53) +C( 95)
      C(182)= +C( 87) +C( 88) +C( 85) +C( 95) +C( 53)
      C(185)= +C( 91) +C( 92) +C( 95) +C( 85) +C( 35)
      C(188)= +C( 27) +C( 28) +C( 25) +C( 35) +C( 85)
      C(191)= +C( 31) +C( 32) +C( 35) +C( 25) +C( 67)
      C(192)= +C( 49) +C( 50) +C( 53) +C( 67) +C( 25)
      C(193)= -C(  1) -C( 43) -C( 29) -C( 26) -C( 25)
      C(194)= -C(  2) -C( 44) -C( 30) -C( 27) -C( 28)
      C(195)= -C(  3) -C( 45) -C( 34) -C( 31) -C( 32)
      C(196)= -C(  4) -C( 46) -C( 33) -C( 36) -C( 35)
      C(197)= -C(  5) -C( 47) -C( 37) -C( 40) -C( 39)
      C(198)= -C(  6) -C( 48) -C( 38) -C( 41) -C( 42)
      C(199)= -C(  7) -C( 63) -C( 52) -C( 49) -C( 50)
      C(200)= -C(  8) -C( 64) -C( 51) -C( 54) -C( 53)
      C(201)= -C(  9) -C( 65) -C( 55) -C( 58) -C( 57)
      C(202)= -C( 10) -C( 66) -C( 56) -C( 59) -C( 60)
      C(203)= -C( 11) -C( 61) -C( 71) -C( 68) -C( 67)
      C(204)= -C( 12) -C( 62) -C( 72) -C( 69) -C( 70)
      C(205)= -C( 13) -C( 83) -C( 73) -C( 76) -C( 75)
      C(206)= -C( 14) -C( 84) -C( 74) -C( 77) -C( 78)
      C(207)= -C( 15) -C( 79) -C( 89) -C( 86) -C( 85)
      C(208)= -C( 16) -C( 80) -C( 90) -C( 87) -C( 88)
      C(209)= -C( 17) -C( 81) -C( 94) -C( 91) -C( 92)
      C(210)= -C( 18) -C( 82) -C( 93) -C( 96) -C( 95)
      C(211)= -C( 19) -C( 97) -C(107) -C(104) -C(103)
      C(212)= -C( 20) -C( 98) -C(108) -C(105) -C(106)
      C(213)= -C( 21) -C( 99) -C(112) -C(109) -C(110)
      C(214)= -C( 22) -C(100) -C(111) -C(114) -C(113)
      C(215)= -C( 23) -C(101) -C(115) -C(118) -C(117)
      C(216)= -C( 24) -C(102) -C(116) -C(119) -C(120)
      C(218)= +C( 18) +C( 17) +C( 14) +C( 24) +C(102)
      C(220)= +C( 22) +C( 21) +C( 24) +C( 14) +C( 84)
      C(225)= +C( 82) +C( 81) +C( 84) +C( 74) +C(116)
      C(226)= +C(100) +C( 99) +C(102) +C(116) +C( 74)
      C(230)= +C( 23) +C( 24) +C( 21) +C( 10) +C( 66)
      C(235)= +C( 64) +C( 63) +C( 66) +C( 56) +C(112)
      C(236)= +C(101) +C(102) +C( 99) +C(112) +C( 56)
      C(240)= +C(  8) +C(  7) +C( 10) +C( 21) +C( 99)
      C(245)= +C( 65) +C( 66) +C( 63) +C( 52) +C( 94)
      C(246)= +C( 83) +C( 84) +C( 81) +C( 94) +C( 52)
      C(250)= +C(  9) +C( 10) +C(  7) +C( 17) +C( 81)
      C(252)= +C( 13) +C( 14) +C( 17) +C(  7) +C( 63)
      C(253)= -C( 43) -C(  1) -C( 11) -C(  8) -C(  7)
      C(254)= -C( 44) -C(  2) -C( 12) -C(  9) -C( 10)
      C(255)= -C( 45) -C(  3) -C( 16) -C( 13) -C( 14)
      C(256)= -C( 46) -C(  4) -C( 15) -C( 18) -C( 17)
      C(257)= -C( 47) -C(  5) -C( 19) -C( 22) -C( 21)
      C(258)= -C( 48) -C(  6) -C( 20) -C( 23) -C( 24)
      C(259)= -C( 26) -C( 68) -C( 54) -C( 51) -C( 52)
      C(260)= -C( 27) -C( 69) -C( 58) -C( 55) -C( 56)
      C(261)= -C( 29) -C( 71) -C( 61) -C( 64) -C( 63)
      C(262)= -C( 30) -C( 72) -C( 62) -C( 65) -C( 66)
      C(263)= -C( 31) -C( 87) -C( 76) -C( 73) -C( 74)
      C(264)= -C( 33) -C( 89) -C( 79) -C( 82) -C( 81)
      C(265)= -C( 34) -C( 90) -C( 80) -C( 83) -C( 84)
      C(266)= -C( 36) -C( 86) -C( 96) -C( 93) -C( 94)
      C(267)= -C( 37) -C(107) -C( 97) -C(100) -C( 99)
      C(268)= -C( 38) -C(108) -C( 98) -C(101) -C(102)
      C(269)= -C( 40) -C(104) -C(114) -C(111) -C(112)
      C(270)= -C( 41) -C(105) -C(118) -C(115) -C(116)
      C(271)= +C( 93) +C( 94) +C( 91) +C( 77) +C(119)
      C(272)= +C(111) +C(112) +C(109) +C(119) +C( 77)
      C(275)= +C(115) +C(116) +C(119) +C(109) +C( 59)
      C(278)= +C( 51) +C( 52) +C( 49) +C( 59) +C(109)
      C(281)= +C( 55) +C( 56) +C( 59) +C( 49) +C( 91)
      C(282)= +C( 73) +C( 74) +C( 77) +C( 91) +C( 49)
      C(283)= -C(133) -C(193) -C(203) -C(200) -C(199)
      C(291)= +C( 46) +C( 45) +C( 48) +C( 38) +C(108)
      C(294)= +C(  4) +C(  3) +C(  6) +C( 20) +C( 98)
      C(297)= +C( 47) +C( 48) +C( 45) +C( 34) +C( 90)
      C(300)= +C(  5) +C(  6) +C(  3) +C( 16) +C( 80)
      C(301)= -C( 61) -C( 11) -C(  1) -C(  4) -C(  3)
      C(302)= -C( 62) -C( 12) -C(  2) -C(  5) -C(  6)
      C(303)= -C( 64) -C(  8) -C( 18) -C( 15) -C( 16)
      C(304)= -C( 65) -C(  9) -C( 22) -C( 19) -C( 20)
      C(305)= -C( 68) -C( 26) -C( 36) -C( 33) -C( 34)
      C(306)= -C( 69) -C( 27) -C( 40) -C( 37) -C( 38)
      C(307)= -C( 71) -C( 29) -C( 43) -C( 46) -C( 45)
      C(308)= -C( 72) -C( 30) -C( 44) -C( 47) -C( 48)
      C(309)= -C( 51) -C( 93) -C( 82) -C( 79) -C( 80)
      C(310)= -C( 54) -C( 96) -C( 86) -C( 89) -C( 90)
      C(311)= -C( 55) -C(111) -C(100) -C( 97) -C( 98)
      C(312)= -C( 58) -C(114) -C(104) -C(107) -C(108)
      C(313)= +C( 89) +C( 90) +C( 87) +C( 76) +C(118)
      C(314)= +C(107) +C(108) +C(105) +C(118) +C( 76)
      C(318)= +C( 33) +C( 34) +C( 31) +C( 41) +C(105)
      C(320)= +C( 37) +C( 38) +C( 41) +C( 31) +C( 87)
      C(326)= +C( 15) +C( 16) +C( 13) +C( 23) +C(101)
      C(328)= +C( 19) +C( 20) +C( 23) +C( 13) +C( 83)
      C(333)= +C( 79) +C( 80) +C( 83) +C( 73) +C(115)
      C(334)= +C( 97) +C( 98) +C(101) +C(115) +C( 73)
      C(337)= -C( 79) -C( 15) -C(  4) -C(  1) -C(  2)
      C(338)= -C( 82) -C( 18) -C(  8) -C( 11) -C( 12)
      C(339)= -C( 86) -C( 36) -C( 26) -C( 29) -C( 30)
      C(340)= -C( 89) -C( 33) -C( 46) -C( 43) -C( 44)
      C(341)= -C( 93) -C( 51) -C( 64) -C( 61) -C( 62)
      C(342)= -C( 96) -C( 54) -C( 68) -C( 71) -C( 72)
      C(343)= +C( 71) +C( 72) +C( 69) +C( 58) +C(114)
      C(346)= +C( 29) +C( 30) +C( 27) +C( 40) +C(104)
      C(350)= +C( 11) +C( 12) +C(  9) +C( 22) +C(100)
      C(353)= +C( 61) +C( 62) +C( 65) +C( 55) +C(111)
      C(355)= -C(302) -C(  6) +C( 50) +C(199) +C(283)
      C(356)= -C(308) -C( 48) +C( 49) -C(192) +C(133)
      C(357)= +C( 43) +C( 44) +C( 47) +C( 37) +C(107)
      C(358)= -C(357) +C(107) -C( 93) -C(341) -C(355)
      C(360)= +C(  1) +C(  2) +C(  5) +C( 19) +C( 97)
      C(123)= -C(121) +C(120) -C(  1) -C(193) -C(133)
      C(124)= -C(123) +C(342) -C(181) -C(131) -C(122)
      C(134)= -C(124) -C(122) +C( 78) -C(  2) -C(194)
      C(158)= +C(124) +C(343) -C( 71) +C(115) -C(275)
      C(166)= -C(158) -C(160) +C( 28) -C( 14) -C(206)
      C(190)= +C(123) -C(342) -C( 72) +C( 73) -C(282)
      C(219)= +C(166) -C(122) -C(272) -C(226) -C(220)
      C(224)= -C(219) -C(220) +C( 84) -C( 27) -C(260)
      C(238)= +C(219) -C(214) -C(113) +C( 15) -C(326)
      C(241)= +C(358) +C(357) +C(360) +C(350) +C(240)
      C(247)= -C(245) +C( 94) -C( 37) -C(267) -C(241)
      C(248)= -C(247) +C(304) -C(230) -C(252) -C(246)
      C(273)= -C(271) +C(119) -C( 43) -C(253) -C(283)
      C(274)= -C(273) +C(341) -C(245) -C(281) -C(272)
      C(277)= -C(166) +C(122) -C(114) +C( 79) -C(333)
      C(284)= -C(134) -C(194) -C(204) -C(201) -C(202)
      C(285)= -C(277) -C(275) +C( 59) -C( 45) -C(255)
      C(289)= -C(355) -C(341) -C(338) -C(337) +C(300)
      C(290)= -C(356) -C(342) -C(339) -C(340) +C(297)
      C(292)= -C(291) +C(108) -C( 51) -C(309) -C(289)
      C(293)= -C(294) +C( 98) -C( 54) -C(310) -C(290)
      C(298)= +C(293) -C(196) -C( 35) +C( 21) +C(257)
      C(299)= +C(292) -C(256) -C( 17) +C( 39) +C(197)
      C(322)= -C(198) -C( 42) +C(  7) +C(253) -C(358)
      C(324)= -C(212) -C(106) +C(  8) -C(240) +C(241)
      C(332)= -C(224) -C(260) -C(269) -C(267) -C(268)
      C(335)= -C(285) -C(206) -C( 78) +C(  2) +C(337)
      C(336)= -C(335) +C(337) -C(300) -C(328) -C(334)
      C(344)= -C(134) +C(160) -C( 59) +C( 45) +C(307)
      C(347)= -C(344) -C(343) +C(114) -C( 79) -C(337)
      C(354)= -C(284) -C(202) -C( 60) +C(  3) +C(301)
      C(359)= -C(360) +C( 97) -C( 96) -C(342) -C(356)
      C(135)= -C(285) -C(255) -C(265) -C(263) +C(146)
      C(136)= +C(185) -C( 91) +C( 47) -C(297) +C(290)
      C(148)= -C(322) -C(302) -C(308) -C(306) +C(159)
      C(149)= +C(132) -C( 76) +C( 62) +C(341) -C(273)
      C(151)= -C(324) -C(304) -C(311) -C(312) +C(168)
      C(152)= +C(125) -C(118) +C( 61) -C(353) -C(274)
      C(156)= -C(148) -C(151) -C(152) -C(274) -C(124)
      C(157)= +C(156) +C(346) -C( 29) +C(116) +C(270)
      C(161)= -C(347) -C(337) -C(340) -C(339) +C(188)
      C(162)= +C(299) +C(300) +C(297) +C(320) +C(145)
      C(165)= -C(157) -C(159) +C( 70) -C( 13) -C(205)
      C(169)= -C(167) +C(113) -C( 15) -C(207) -C(161)
      C(170)= -C(168) +C( 57) -C( 16) -C(208) -C(162)
      C(175)= +C(174) -C( 26) +C(102) +C(268) +C(332)
      C(176)= +C(293) +C(294) +C(291) +C(318) +C(142)
      C(178)= +C(167) -C( 68) +C(101) -C(236) +C(224)
      C(180)= -C(190) -C(192) +C( 25) -C( 24) -C(216)
      C(183)= -C(181) +C( 95) -C( 19) -C(211) -C(175)
      C(184)= -C(182) +C( 53) -C( 20) -C(212) -C(176)
      C(186)= +C(169) -C(305) -C( 34) +C( 56) +C(260)
      C(187)= -C(186) +C(260) -C(160) -C(192) -C(185)
      C(189)= -C(190) -C(282) -C(132) -C(182) -C(191)
      C(217)= +C(248) +C(328) -C( 19) +C( 95) +C(210)
      C(223)= -C(217) -C(218) +C(102) -C( 26) -C(259)
      C(229)= +C(152) -C(125) -C(275) -C(236) -C(230)
      C(232)= +C(298) +C(297) +C(300) +C(328) +C(220)
      C(234)= -C(229) -C(230) +C( 66) -C( 31) -C(263)
      C(237)= -C(238) -C(326) -C(218) -C(240) -C(235)
      C(239)= +C(247) -C(304) -C( 20) +C( 53) +C(200)
      C(242)= +C(292) +C(291) +C(294) +C(326) +C(218)
      C(243)= +C(235) -C( 64) +C(105) -C(168) +C(151)
      C(244)= +C(225) -C( 82) +C(104) -C(154) +C(165)
      C(249)= +C(237) -C(303) -C( 16) +C( 57) +C(201)
      C(251)= -C(244) -C(242) -C(241) +C(324) -C(229)
      C(276)= -C(277) -C(333) -C(225) -C(271) -C(278)
      C(280)= -C(180) +C(121) -C( 96) +C( 97) -C(334)
      C(286)= -C(209) -C( 92) +C(  5) -C(300) +C(289)
      C(288)= -C(216) -C(120) +C(  1) -C(360) -C(336)
      C(295)= +C(354) +C(353) +C(350) +C(360) +C(294)
      C(296)= +C(344) +C(343) +C(346) +C(357) +C(291)
      C(315)= -C(135) +C(146) -C( 77) +C( 44) +C(340)
      C(316)= -C(314) +C( 76) -C( 62) -C(302) -C(322)
      C(317)= -C(162) +C(145) -C( 40) +C( 81) +C(264)
      C(319)= -C(176) +C(142) -C( 36) +C( 99) +C(267)
      C(321)= -C(315) -C(313) +C(118) -C( 61) -C(301)
      C(323)= -C(317) -C(318) +C(105) -C( 64) -C(303)
      C(325)= -C(232) +C(220) -C( 22) +C( 85) +C(207)
      C(327)= -C(242) +C(218) -C( 18) +C(103) +C(211)
      C(329)= -C(255) -C( 14) +C( 28) +C(194) +C(347)
      C(330)= -C(258) -C( 24) +C( 25) +C(193) -C(359)
      C(331)= -C(265) -C( 84) +C( 27) -C(188) +C(161)
      C(345)= -C(148) +C(159) -C( 41) +C( 63) +C(261)
      C(348)= -C(345) -C(346) +C(104) -C( 82) -C(338)
      C(349)= -C(359) -C(356) -C(355) +C(247) -C(239)
      C(351)= -C(254) -C( 10) +C( 32) +C(195) +C(321)
      C(352)= -C(349) -C(350) +C(100) -C( 86) -C(339)
      C(126)= +C(187) +C(281) -C( 55) +C( 90) +C(310)
      C(127)= -C(170) -C(162) -C(165) -C(166) -C(277)
      C(137)= +C(171) -C(109) +C( 46) -C(291) +C(296)
      C(138)= +C(139) -C(119) +C( 43) -C(357) -C(316)
      C(140)= +C(184) +C(320) -C( 37) +C( 94) +C(266)
      C(141)= -C(139) +C( 42) -C(  7) -C(199) -C(149)
      C(143)= +C(170) +C(318) -C( 33) +C(112) +C(269)
      C(144)= +C(127) +C(313) -C( 89) +C(111) -C(272)
      C(147)= +C(191) -C( 31) +C( 66) +C(262) +C(352)
      C(150)= -C(323) -C(303) -C(309) -C(310) +C(182)
      C(155)= +C(189) -C(263) -C( 74) +C( 30) +C(339)
      C(163)= +C(131) -C( 58) +C( 80) +C(309) -C(276)
      C(164)= +C(181) -C( 69) +C( 83) -C(246) +C(223)
      C(172)= +C(183) -C(306) -C( 38) +C( 52) +C(259)
      C(173)= -C(141) -C(149) -C(152) -C(151) +C(243)
      C(177)= +C(239) +C(240) +C(235) +C(278) +C(128)
      C(179)= +C(153) -C( 86) +C(100) -C(226) +C(234)
      C(221)= +C(252) -C( 13) +C( 70) +C(204) +C(348)
      C(222)= +C(230) -C( 23) +C( 67) +C(203) -C(349)
      C(227)= +C(251) -C(205) -C( 75) +C( 12) +C(338)
      C(228)= +C(229) -C(215) -C(117) +C( 11) -C(350)
      C(231)= +C(250) -C(  9) +C( 88) +C(208) +C(323)
      C(233)= +C(245) -C( 65) +C( 87) -C(182) +C(150)
      C(279)= -C(177) +C(128) -C( 54) +C( 98) +C(311)
      C(287)= -C(213) -C(110) +C(  4) -C(294) +C(295)
      C(129)= +C(173) +C(278) -C( 51) +C(108) +C(312)
      C(130)= +C(141) +C(271) -C( 93) +C(107) -C(314)
*
      END
*
************************************************************************
* SUBROUTINE MAKMAT returns the NLO in colour matrix in the form of a  *
* index matrix IMAT in the common NLO.                                 *
************************************************************************
      SUBROUTINE MAKMAT
      PARAMETER(NUP=7)
      INTEGER IC(720)
      INTEGER P(1:NUP),Q(1:NUP),R(1:NUP)
      INTEGER IMAT(2,16,360),INHOUD(16),INDEX(16)
      COMMON /NLO/ IMAT
      SAVE INHOUD,INDEX,/NLO/
      DATA INHOUD/3,3,3,3,3,3,1,3,3,-3,-3,-1,3,3,-3,-3/
      DATA INDEX /9,16,33,64,123,145,151,157,193,
     .            198,215,297,323,385,402,477/
      N=7
      ICOUNT=1
      DO 10 I=1,720
        CALL INVID(I,P,N)
        IF(P(2).LT.P(N))THEN
          IC(I)=ICOUNT
          ICOUNT=ICOUNT+1
        ELSE
          IC(I)=0
        END IF
   10 CONTINUE
      DO 30 I=1,720
        CALL INVID(I,P,N)
        IF (P(2).LT.P(7)) THEN
          DO 40 J=1,16
            CALL INVID(INDEX(J),Q,N)
            DO 50 K=1,7
   50         Q(K)=P(Q(K))
            ITEK=1
            IF (Q(2).GT.Q(7)) THEN
              ITEK=-1
              R(1)=1
              DO 60 K=2,7
   60           R(K)=Q(9-K)
              IDNR=ID(R,7)
            ELSE
              IDNR=ID(Q,7)
            END IF
            IMAT(1,J,IC(I))=IC(IDNR)
            IMAT(2,J,IC(I))=ITEK*INHOUD(J)
   40     CONTINUE
        END IF
   30 CONTINUE
      END
*
************************************************************************
* SUBROUTINE MAKEIH generates the table for calling the C-functions.   *
* In total there are 35 combinations with 3+ and 4- helicities. The    *
* calling sequences are put in IHTAB(35,120).                          *
* The permutations of (12xxxxx) are stored in IPERM(7,120)             *
************************************************************************
      SUBROUTINE MAKEIH
      COMMON /HELS/ IHTAB(35,120),IPERM(7,120)
      DIMENSION IHEL(3,35),IP(7),IFOUND(35)
      SAVE IHEL,/HELS/
      DATA IHEL/1,2,3,1,2,4,1,2,5,1,2,6,1,2,7,1,3,4,
     .          1,3,5,1,3,6,1,3,7,1,4,5,1,4,6,1,4,7,
     .          1,5,6,1,5,7,1,6,7,2,3,4,2,3,5,2,3,6,
     .          2,3,7,2,4,5,2,4,6,2,4,7,2,5,6,2,5,7,
     .          2,6,7,3,4,5,3,4,6,3,4,7,3,5,6,3,5,7,
     .          3,6,7,4,5,6,4,5,7,4,6,7,5,6,7/
*
      DO 10 I=1,120
        CALL INVID(I,IP,7)
        DO 20 J=1,35
          I1=IP(IHEL(1,J))
          I2=IP(IHEL(2,J))
          I3=IP(IHEL(3,J))
* make sure that I1<I2<I3
          IF(I1.GT.I2) CALL SWAP(I1,I2)
          IF(I1.GT.I3) CALL SWAP(I1,I3)
          IF(I2.GT.I3) CALL SWAP(I2,I3)
* Find I1,I2,I3 in IHEL and put index
          DO 30 K=1,35
            IF( (I1.EQ.IHEL(1,K)) .AND. (I2.EQ.IHEL(2,K))
     .          .AND. (I3.EQ.IHEL(3,K)) ) IFOUND(J)=K
   30     CONTINUE
   20   CONTINUE
        DO 40 K=1,7
   40     IPERM(K,I)=IP(K)
        DO 50 K=1,35
   50     IHTAB(K,I)=IFOUND(K)
   10 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE SWAP(I1,I2) transforms I1 into I2 and I2 into I1.         *
************************************************************************
      SUBROUTINE SWAP(I1,I2)
      I0=I1
      I1=I2
      I2=I0
      END
 
**********************************************************************
* Q2S contains all the matrix elements with two quarks               *
* and N (2,3,4 or 5) gluons. For N<5 specialised matrix elements     *
* preferred above the recursive techniques. This when mq=0           *
* The general procedure to call a matrix is through:                 *
*                                                                    *
* SUBROUTINE Q2S(PLAB,NPART,IQ,IQB,RMQ,IHRN,OUT)                     *
*    with  PLAB  = Momenta of partons                                *
*          NPART = Number of partons = 2 + # gluons                  *
*          IQ    = Position of outgoing quark                        *
*          IQB   = Position of outgoing anti-quark                   *
*          RMQ   = Mass of quarks                                    *
*          IHRN  = Boolean to indicate whether to MC over helicities *
*          OUT(2)= EXACT/LO result                                   *
**********************************************************************
      SUBROUTINE Q2S(PLAB,NPART,IQ,IQB,RMQ,IHRN,OUT)
      REAL*8 PLAB(4,*),OUT(2),RMQ
*
      IF (NPART.EQ.4) THEN
        CALL Q2G2S(PLAB,IQ,IQB,REAL(RMQ),OUT)
      ELSE IF (RMQ.LT.1D-10) THEN
        IF (NPART.EQ.5) CALL Q2G3S(PLAB,IQ,IQB,OUT)
        IF (NPART.EQ.6) CALL Q2G4S(PLAB,IQ,IQB,IHRN,OUT)
        IF (NPART.EQ.7)
     .           CALL Q2MGNS(NPART-2,PLAB,IQ,IQB,0.0,IHRN,OUT)
      ELSE
        CALL Q2MGNS(NPART-2,PLAB,IQ,IQB,REAL(RMQ),IHRN,OUT)
      END IF
*
      END
*
**********************************************************************
* SUBROUTINE Q2G2S(PLAB,IQ,IQB,RMQ,OUT) calculates q qb gg           *
**********************************************************************
* input:  Array with 4 momenta. The fourth component is the energy.  *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:4)           *
*         IQ1 is the quark, IQ2 the anti-quark.                      *
**********************************************************************
* output: OUT(1)/OUT(2)     = EXACT/LO of q qb 2g.                   *
* NOTE:   Number of colours set to 3                                 *
**********************************************************************
      SUBROUTINE Q2G2S(PLAB,IQ,IQB,RMQ,OUT)
      PARAMETER(NCOL=3,C0=NCOL*(NCOL**2-1.0),CK=(NCOL**2-1.0)/NCOL)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2)
*
* Matrix element from Heavy flavour production paper
* by Beenakker et al.
*
      IMASS=0
      IF (RMQ.GT.0.0) IMASS=1
*
* find gluon indices
      IG1=1
      IF ((IG1.EQ.IQ).OR.(IG1.EQ.IQB)) IG1=IG1+1
      IF ((IG1.EQ.IQ).OR.(IG1.EQ.IQB)) IG1=IG1+1
      IG2=IG1+1
      IF ((IG2.EQ.IQ).OR.(IG2.EQ.IQB)) IG2=IG2+1
      IF ((IG2.EQ.IQ).OR.(IG2.EQ.IQB)) IG2=IG2+1
      S=2.0*REAL(PLAB(4,IG1)*PLAB(4,IG2)-PLAB(3,IG1)*PLAB(3,IG2)
     .          -PLAB(2,IG1)*PLAB(2,IG2)-PLAB(1,IG1)*PLAB(1,IG2))
      T1=2.0*REAL(PLAB(4,IG1)*PLAB(4,IQ)-PLAB(3,IG1)*PLAB(3,IQ)
     .           -PLAB(2,IG1)*PLAB(2,IQ)-PLAB(1,IG1)*PLAB(1,IQ))
      IF (IG1.LT.3) THEN
        S=-S
        T1=-T1
      END IF
      IF (IG2.LT.3) S=-S
      IF (IQ.LT.3) T1=-T1
      U1=-S-T1
*
      BQED=T1/U1+U1/T1
      IF (IMASS.EQ.1) BQED=BQED+4.0*RMQ**2*S/T1/U1*(1.0-RMQ**2*S/T1/U1)
      B0=(1.0-2.0*T1*U1/S/S)*BQED
      BK=-BQED
      OUT(1)=2.0*ABS((C0*B0+CK*BK))
      OUT(2)=2.0*ABS(C0*B0)
*
      END
*
**********************************************************************
* SUBROUTINE Q2G3S(PLAB,IQ,IQB,OUT) calculates q qb ggg              *
**********************************************************************
* input:  Array with 5 momenta. The fourth component is the energy.  *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:5)           *
*         IQ1 is the quark, IQ2 the anti-quark.                      *
**********************************************************************
* output: No spin and colour averaging, no bose factor.              *
*         Normally this would be                                     *
*         1/4 * 1/9 * 1/6  for q qb -> 3g                            *
*         1/4 * 1/24 * 1/2 for q g -> q 2g                           *
*         1/4 * 1/81 * 1   for g g -> q qb g                         *
*         OUT(1)/OUT(2)     = EXACT/LO of q qb 3g.                   *
* NOTE:   Extra factor 2 for helicities in the colour factors        *
**********************************************************************
      SUBROUTINE Q2G3S(PLAB,IQ1,IQ2,OUT)
      PARAMETER(NC=3,C1=-2.*NC**2+1.0,C2=-NC**2+1.0,C3=1.0,C4=NC**2+1.0)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2)
      DIMENSION P(0:3,1:5)
      DIMENSION ZDF(1:6),SUBMAT(6,6)
      SAVE COLLO,COLSUB,SUBMAT
      DATA COLLO, COLSUB / 144.0, 1.77777777778/
      DATA SUBMAT /C1,C2,C3,C2,C3,C4,
     .             C2,C1,C4,C3,C2,C3,
     .             C3,C4,C1,C2,C3,C2,
     .             C2,C3,C2,C1,C4,C3,
     .             C3,C2,C3,C4,C1,C2,
     .             C4,C3,C2,C3,C2,C1/
*
* Transfrom Vectors, normalize vectors according to energies.
*
      CALL MAPEVE(5,PLAB,P,SCALE)
*
* Shuffle momenta such that first is IQ, second is IQB ...
*
      CALL RESHQS(P,IQ1,IQ2,0,0,2)
*
      FACTOR=1.0 / SCALE**2
*
      CALL DFILL5(P,ZDF,RMPNUM)
      OUT(2)=COLLO * RMPNUM * FACTOR *
     .   ( + ZDF(1)*CONJG(ZDF(1)) + ZDF(2)*CONJG(ZDF(2))
     .     + ZDF(3)*CONJG(ZDF(3)) + ZDF(4)*CONJG(ZDF(4))
     .     + ZDF(5)*CONJG(ZDF(5)) + ZDF(6)*CONJG(ZDF(6))  )
      OUT(1)=COLSUB* RMPNUM * FACTOR * SQUARE(SUBMAT,6,ZDF,6) + OUT(2)
*
      END
*
**********************************************************************
* DFILL5 calculates all the invariant D-amplitudes into ZDF(1:6)     *
* Note: only 1 helicities is needed for this qqb proces.             *
**********************************************************************
      SUBROUTINE DFILL5(P,ZDF,RMPNUM)
      PARAMETER(NGUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION P(0:3,*),PR2(NGUP,NGUP),ZDF(6)
      COMMON /DOTPR/ ZUD(NGUP,NGUP),ZD(NGUP,NGUP)
      DIMENSION L(3,6)
      SAVE L,/DOTPR/
      DATA L / 3,4,5, 3,5,4, 4,5,3, 4,3,5, 5,3,4, 5,4,3/
*
* Init spinors and propagators
*
      CALL SETGSP(P,5,0)
      DO 5 I=1,4
        DO 5 J=I+1,5
          PR2(I,J)=2.0*(+P(0,I)*P(0,J)-P(1,I)*P(1,J)
     .                  -P(2,I)*P(2,J)-P(3,I)*P(3,J))
          PR2(J,I)=PR2(I,J)
    5 CONTINUE
*
* Put MP helicity denominators in ZDF(i)
*
      DO 10 I=1,6
        ZDF(I)=1.0/(ZUD(1,L(1,I))*ZUD(L(1,I),L(2,I))*
     .        ZUD(L(2,I),L(3,I))*ZUD(L(3,I),2))
   10 CONTINUE
*
* Put Sum of (MP-numerators)**2 in RMPNUM
*
      RMPNUM=0.0
      DO 20 I=3,5
        RMPNUM=RMPNUM+PR2(1,I)**3*PR2(2,I)+PR2(2,I)**3*PR2(1,I)
   20 CONTINUE
      RMPNUM=RMPNUM/PR2(1,2)
      END
*
**********************************************************************
* SUBROUTINE Q2G4S(PLAB,IQ,IQB,IHRN,OUT) calculates q qb gggg        *
**********************************************************************
* input:  Array with 6 momenta. The fourth component is the energy.  *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:6)           *
*         IQ1 is the quark, IQ2 the anti-quark.                      *
**********************************************************************
* output: No spin and colour averaging, no bose factor.              *
*         Normally this would be                                     *
*         1/4 * 1/9 * 1/24 for q qb -> 4g                            *
*         1/4 * 1/24 * 1/6 for q g -> q 3g                           *
*         1/4 * 1/81 * 1/2 for g g -> q qb 2g                        *
*         OUT(1)/OUT(2) = EXACT / LO                                 *
* NOTE:   Extra factor 2 for helicities in the colour factors        *
**********************************************************************
* Literature:                                                        *
*   1) Matrix elements from Mangano+Parke, Nucl. Phys. B299 (673)    *
**********************************************************************
      SUBROUTINE Q2G4S(PLAB,IQ1,IQ2,IHRN,OUT)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2),RN
      DIMENSION P(0:3,1:6)
      DIMENSION ZDF(1:24,0:6)
      SAVE COLLO,COLSUB
      DATA COLLO, COLSUB / 432.0, 0.592592592/
*
* Transfrom Vectors, normalize vectors according to energies.
*
      CALL MAPEVE(6,PLAB,P,SCALE)
*
* Shuffle momenta such that first is IQ, second is IQB ...
*
      CALL RESHQS(P,IQ1,IQ2,0,0,2)
*
      FACTOR=1.0 / SCALE**4
*
      IF (IHRN.EQ.1) THEN
        IHELRN=1+INT(10.0*RN(1))
        IF (IHELRN.GT.6) IHELRN=7
      ELSE
        IHELRN=0
      END IF
*
      CALL DFILL6(P,ZDF,RMPNUM,IHELRN)
*
      OUT(1)=0D0
      OUT(2)=0D0
*
      IF ((IHRN.EQ.0).OR.(IHELRN.EQ.7)) THEN
        OUT(2)=COLLO * RMPNUM * RLO6Q2(ZDF(1,0))
        OUT(1)=COLSUB* RMPNUM * RSU6Q2(ZDF(1,0))
      END IF
      DO 10 I=1,6
        IF ((IHRN.EQ.0).OR.(IHELRN.EQ.I)) THEN
          OUT(2)=OUT(2)+COLLO * RLO6Q2(ZDF(1,I))
          OUT(1)=OUT(1)+COLSUB* RSU6Q2(ZDF(1,I))
        END IF
   10 CONTINUE
      OUT(2)=OUT(2) * FACTOR
      OUT(1)=OUT(1) * FACTOR
      IF (IHRN.EQ.1) THEN
        IF (IHELRN.EQ.7) THEN
          OUT(1)=10.0/4.0*OUT(1)
          OUT(2)=10.0/4.0*OUT(2)
        ELSE
          OUT(1)=10.0*OUT(1)
          OUT(2)=10.0*OUT(2)
        END IF
      END IF
*
      OUT(1)= OUT(1) + OUT(2)
*
      END
*
************************************************************************
* FUNCTION RLO6Q2 calculates the LO contribution to q2 g4.             *
************************************************************************
      FUNCTION RLO6Q2(ZDF)
      COMPLEX ZDF(1:24)
*
      RLO6Q2=0.0
      DO 10 I=1,24
        RLO6Q2=RLO6Q2+REAL(ZDF(I)*CONJG(ZDF(I)))
   10 CONTINUE
*
      END
*
************************************************************************
* FUNCTION RSU6Q2 calculates the NLO contribution to q2 g4.            *
************************************************************************
      FUNCTION RSU6Q2(ZDF)
      COMPLEX ZDF(24)
      DIMENSION COLMAT(24,24),IORD1(24,6),IORD2(24,6),IORD3(24,6)
      SAVE COLMAT,IORD1,IORD2,IORD3,INIT,IORDER,NCOL
      DATA IORD1
     . /-3,-1,0,-1,0,1,0,0,0,0,-1,0,1,1,0,0,0,1,0,0,1,0,1,0,
     .  -1,-3,1,0,-1,0,0,0,0,0,0,-1,1,0,0,0,1,0,0,0,0,1,1,1,
     .  0,1,-3,-1,0,-1,1,0,1,0,0,0,0,0,0,-1,0,0,0,0,1,1,1,0,
     .  -1,0,-1,-3,1,0,0,1,1,1,0,0,0,0,-1,0,0,0,0,0,0,1,0,1,
     .  0,-1,0,1,-3,-1,1,1,1,0,0,0,0,1,0,0,0,1,-1,0,0,0,0,0,
     .  1,0,-1,0,-1,-3,0,1,0,1,0,0,1,0,0,0,1,1,0,-1,0,0,0,0/
      DATA IORD2
     . /3,2,1,2,1,0,0,1,0,-1,2,1,-1,-2,1,0,-1,0,0,-1,-2,-1,-2,-3,
     .  2,3,0,1,2,1,-1,0,1,0,1,2,-2,-3,0,-1,-2,-1,1,0,-1,0,-1,-2,
     .  1,0,3,2,1,2,-2,-1,-2,-3,0,-1,1,0,1,2,-1,0,0,1,-2,-1,0,-1,
     .  2,1,2,3,0,1,-1,0,-1,-2,1,0,0,-1,2,1,0,1,-1,0,-3,-2,-1,-2,
     .  1,2,1,0,3,2,-2,-1,0,-1,0,1,-1,-2,-1,0,-3,-2,2,1,0,1,0,-1,
     .  0,1,2,1,2,3,-3,-2,-1,-2,-1,0,0,-1,0,1,-2,-1,1,2,-1,0,1,0/
      DATA IORD3 / 144*-1 /
      DATA INIT, IORDER , NCOL / 1,3,3 /
      IF (INIT.EQ.1) THEN
        INIT=0
        DO 5 I=1,6
          DO 5 J=1,24
            COLMAT(J,I)=0.0
            IF (IORDER.GE.1) COLMAT(J,I)=COLMAT(J,I)+NCOL**4*IORD1(J,I)
            IF (IORDER.GE.2) COLMAT(J,I)=COLMAT(J,I)+NCOL**2*IORD2(J,I)
            IF (IORDER.GE.3) COLMAT(J,I)=COLMAT(J,I)+IORD3(J,I)
    5   CONTINUE
        DO 7 IROT=1,3
          DO 7 I=1,6
            DO 7 J=1,24
              COLMAT(J,I+6*IROT)=COLMAT(1+MOD(J+23-6*IROT,24),I)
    7   CONTINUE
      END IF
      RSU6Q2=SQUARE(COLMAT,24,ZDF,24)
*
      END
*
************************************************************************
* SUBROUTINE DFILL6 calculates all the invariant D-amplitudes into     *
* ZDF(1:24,0:6)                                                        *
* Note: There are SIX ( 2+;2- ) helicities for the qqb proces.         *
* If IHELRN<>0 then just one hel. combination needs to be evaluated    *
************************************************************************
      SUBROUTINE DFILL6(P,ZDF,RMPNUM,IHELRN)
      PARAMETER(NGUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION P(0:3,*)
      COMMON /DOTPR/ ZUD(NGUP,NGUP),ZD(NGUP,NGUP)
      DIMENSION PROP2(NGUP,NGUP),PROP3(NGUP,NGUP,NGUP),
     .          PROP23(NGUP,NGUP,NGUP)
      COMMON /PROPS/ PROP2,PROP3,PROP23
      DIMENSION ZDF(1:24,0:6),L(10,24)
      COMMON /DPARMS/ P1,P2,P3,P4
      SAVE L,/PROPS/,/DPARMS/,/DOTPR/
      DATA L / 3,4,5,6,1,2,3,4,5,6, 3,4,6,5,1,3,2,5,4,6,
     .         3,5,6,4,2,3,1,6,4,5, 3,5,4,6,2,1,3,4,6,5,
     .         3,6,4,5,3,1,2,5,6,4, 3,6,5,4,3,2,1,6,5,4,
     .         4,5,6,3,4,5,1,6,2,3, 4,5,3,6,4,1,5,2,6,3,
     .         4,6,3,5,5,1,4,3,6,2, 4,6,5,3,5,4,1,6,3,2,
     .         4,3,5,6,1,4,5,2,3,6, 4,3,6,5,1,5,4,3,2,6,
     .         5,6,3,4,6,2,4,3,5,1, 5,6,4,3,6,4,2,5,3,1,
     .         5,3,4,6,2,4,6,1,3,5, 5,3,6,4,2,6,4,3,1,5,
     .         5,4,6,3,4,6,2,5,1,3, 5,4,3,6,4,2,6,1,5,3,
     .         6,3,4,5,3,5,6,1,2,4, 6,3,5,4,3,6,5,2,1,4,
     .         6,4,5,3,5,6,3,4,1,2, 6,4,3,5,5,3,6,1,4,2,
     .         6,5,3,4,6,3,5,2,4,1, 6,5,4,3,6,5,3,4,2,1 /
*
* Init spinors and propagators
*
      CALL SETGSP(P,6,0)
      CALL PRFILL(P,6)
*
* Put MP helicity denominators in ZDF(i,0)
*
      IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.7)) THEN
        DO 10 I=1,24
          ZDF(I,0)=1.0/(ZUD(1,L(1,I))*ZUD(L(1,I),L(2,I))*
     .            ZUD(L(2,I),L(3,I))*ZUD(L(3,I),L(4,I))*ZUD(L(4,I),2))
   10   CONTINUE
*
* Put Sum of (MP-numerators)**2 in RMPNUM
*
        RMPNUM=0.0
        DO 20 I=3,6
         RMPNUM=RMPNUM+PROP2(1,I)**3*PROP2(2,I)+PROP2(2,I)**3*PROP2(1,I)
   20   CONTINUE
        RMPNUM=RMPNUM/PROP2(1,2)
* Dirty exit when monte carlo goes over this special helicity
        IF (IHELRN.EQ.7) RETURN
      END IF
*
* Calculate dynamic spinor structures
*
      CALL ZZFILL(6)
*
* Calculate all the D(+-;2+2-) functions in ZDF(1:24,1:6)
*
      DO 40 I=1,24
        I3=L(1,I)
        I4=L(2,I)
        I5=L(3,I)
        I6=L(4,I)
        P1=PROP23(1,2,I6)*PROP2(I4,I5)*PROP2(I3,I4)
        P2=PROP23(2,I6,I5)*PROP2(I3,I4)*PROP2(1,I3)
        P3=PROP23(I4,I5,I6)*PROP2(1,I3)*PROP2(1,2)
        P4=PROP2(1,2)*PROP2(2,I6)*PROP2(I6,I5)*
     .     PROP2(I5,I4)*PROP2(I4,I3)*PROP2(I3,1)
        IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.L(5,I)))
     .            ZDF(I,L(5,I))=ZDPPMM(I3,I4,I5,I6)
        IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.L(6,I)))
     .           ZDF(I,L(6,I))=ZDPMPM(I3,I4,I5,I6)
        IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.L(7,I)))
     .           ZDF(I,L(7,I))=ZDPMMP(I3,I4,I5,I6)
        IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.L(8,I)))
     .           ZDF(I,L(8,I))=ZDMPPM(I3,I4,I5,I6)
        IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.L(9,I)))
     .           ZDF(I,L(9,I))=ZDMPMP(I3,I4,I5,I6)
        IF ((IHELRN.EQ.0).OR.(IHELRN.EQ.L(10,I)))
     .           ZDF(I,L(10,I))=ZDMMPP(I3,I4,I5,I6)
   40 CONTINUE
*
      END
*
************************************************************************
* COMPLEX FUNCTION ZDPPMM evaluates D(+-;++--).                        *
************************************************************************
      FUNCTION ZDPPMM(I3,I4,I5,I6)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION PROP2(NUP,NUP),PROP3(NUP,NUP,NUP),PROP23(NUP,NUP,NUP)
      COMMON /PROPS/ PROP2,PROP3,PROP23
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DPARMS/ P1,P2,P3,P4
      SAVE /DPARMS/,/DOTPR/,/PROPS/,/ZFUNCT/
*
      Z1=ZD(I4,I3)**2*ZUD(1,I6)*ZUD(2,I6)*ZZ(1,2,I6,I5)**2
      Z2=(0.0,0.0)
      Z3=-ZD(1,I3)*ZD(2,I3)*ZUD(I6,I5)**2*ZZ(I4,I5,I6,2)**2
      Z4=ZUD(2,I6)*ZUD(I6,I5)*ZD(I4,I3)*ZD(I3,1)*
     . (+ZZ(1,2,I6,I5)*ZZ(2,I5,I6,1)*ZZ(I4,I5,I6,2)
     .  +PROP2(I3,I4)*ZD(2,I6)*ZUD(I6,I5)*ZZ(I4,I5,I6,2)
     .  +PROP2(I5,I6)*ZD(I4,I3)*ZUD(I3,1)*ZZ(1,2,I6,I5))
      ZDPPMM=+ Z1/P1 + Z2/P2 + Z3/P3 + Z4/P4
*
      END
*
************************************************************************
* COMPLEX FUNCTION ZDPMPM evaluates D(+-;+-+-).                        *
************************************************************************
      FUNCTION ZDPMPM(I3,I4,I5,I6)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION PROP2(NUP,NUP),PROP3(NUP,NUP,NUP),PROP23(NUP,NUP,NUP)
      COMMON /PROPS/ PROP2,PROP3,PROP23
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DPARMS/ P1,P2,P3,P4
      SAVE /DPARMS/,/DOTPR/,/PROPS/,/ZFUNCT/
*
      Z1=+ZD(I5,I3)**2*ZUD(1,I6)*ZUD(2,I6)*ZZ(1,2,I6,I4)**2
      Z2=-ZD(1,I3)*ZUD(2,I6)*ZZ(I3,2,I5,I6)*ZZ(I5,2,I6,I4)**2
      Z3=-ZD(1,I3)*ZD(2,I3)*ZUD(I6,I4)**2*ZZ(I5,I6,I4,2)**2
      Z4=+PROP3(1,2,I6)*ZD(1,I3)*ZUD(I6,I4)*ZZ(I3,2,I5,I6)*
     .    ZZ(I5,2,I6,I4)*ZZ(I5,I4,I6,2)
     .   +PROP3(2,I5,I6)*ZD(I5,I3)*ZUD(I6,I4)*ZZ(I3,2,I5,I6)*
     .    ZZ(1,2,I6,I4)*ZZ(I5,I4,I6,2)
     .   -PROP3(I4,I5,I6)*ZD(I5,I3)*ZUD(2,I6)*ZZ(I3,2,I5,I6)*
     .    ZZ(1,2,I6,I4)*ZZ(I5,2,I6,I4)
     .   -ZD(I5,I3)*ZD(I4,I3)*ZUD(I6,I5)*ZUD(I6,I4)*ZZ(1,2,I6,I4)*
     .    ZZ(I5,2,I6,I4)*ZZ(I5,I4,I6,2)
      ZDPMPM=+ Z1/P1 + Z2/P2 + Z3/P3 + Z4/P4
*
      END
*
************************************************************************
* COMPLEX FUNCTION ZDPMMP evaluates D(+-;+--+).                        *
************************************************************************
      FUNCTION ZDPMMP(I3,I4,I5,I6)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION PROP2(NUP,NUP),PROP3(NUP,NUP,NUP),PROP23(NUP,NUP,NUP)
      COMMON /PROPS/ PROP2,PROP3,PROP23
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DPARMS/ P1,P2,P3,P4
      SAVE /DPARMS/,/DOTPR/,/PROPS/,/ZFUNCT/
*
      Z1=-ZUD(I4,I5)**2*ZD(1,I6)*ZD(2,I6)*ZZ(I3,1,I6,2)**2
      Z2=-ZD(1,I3)*ZUD(2,I5)*ZZ(I3,2,I6,I5)*ZZ(I6,2,I5,I4)**2
      Z3=-ZUD(I4,I5)**2*ZD(1,I3)*ZD(2,I3)*ZZ(I6,I4,I5,2)**2
      Z4=+PROP3(1,2,I6)*ZD(1,I3)*ZD(2,I3)*ZUD(2,I5)*ZUD(I5,I4)*
     .    ZZ(I6,2,I5,I4)*ZZ(I6,I4,I5,2)
     .   +PROP3(2,I5,I6)*ZD(1,I6)*ZD(2,I3)*ZUD(I4,I5)**2*
     .    ZZ(I3,1,I6,2)*ZZ(I6,I4,I5,2)
     .   -PROP3(I4,I5,I6)*ZD(1,I6)*ZD(2,I3)*ZUD(2,I5)*ZUD(I5,I4)*
     .    ZZ(I3,1,I6,2)*ZZ(I6,2,I5,I4)
     .   +ZD(1,2)*ZD(I6,I3)*ZUD(I6,I5)*ZUD(I5,I4)*ZZ(I3,1,I6,2)*
     .    ZZ(I6,2,I5,I4)*ZZ(I6,I4,I5,2)
      ZDPMMP=+ Z1/P1 + Z2/P2 + Z3/P3 + Z4/P4
*
      END
*
************************************************************************
* COMPLEX FUNCTION ZDMPPM evaluates D(+-;-++-).                        *
************************************************************************
      FUNCTION ZDMPPM(I3,I4,I5,I6)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION PROP2(NUP,NUP),PROP3(NUP,NUP,NUP),PROP23(NUP,NUP,NUP)
      COMMON /PROPS/ PROP2,PROP3,PROP23
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DPARMS/ P1,P2,P3,P4
      SAVE /DPARMS/,/DOTPR/,/PROPS/,/ZFUNCT/
*
      Z1=+ZD(I5,I4)**2*ZUD(1,I6)*ZUD(2,I6)*ZZ(1,2,I6,I3)**2
      Z2=-ZD(1,I4)*ZUD(2,I6)*ZZ(I4,2,I5,I6)*ZZ(I5,2,I6,I3)**2
      Z3=+ZD(I4,I5)**2*ZUD(1,I3)*ZUD(2,I3)*ZZ(1,I4,I5,I6)**2
      Z4=-PROP3(1,2,I6)*ZD(1,I4)*ZD(I5,I4)*ZUD(1,I6)*ZUD(2,I3)*
     .    ZZ(I5,2,I6,I3)*ZZ(1,I4,I5,I6)
     .   -PROP3(2,I5,I6)*ZD(I5,I4)**2*ZUD(1,I6)*ZUD(2,I3)*
     .    ZZ(1,2,I6,I3)*ZZ(1,I4,I5,I6)
     .   +PROP3(I4,I5,I6)*ZD(1,I4)*ZD(I5,I4)*ZUD(1,I6)*ZUD(2,I6)*
     .    ZZ(1,2,I6,I3)*ZZ(I5,2,I6,I3)
     .   +ZD(I5,I4)*ZD(I4,I3)*ZUD(1,2)*ZUD(I6,I3)*ZZ(1,2,I6,I3)*
     .    ZZ(I5,2,I6,I3)*ZZ(1,I4,I5,I6)
      ZDMPPM=+ Z1/P1 + Z2/P2 + Z3/P3 + Z4/P4
*
      END
*
************************************************************************
* COMPLEX FUNCTION ZDMPMP evaluates D(+-;-+-+).                        *
************************************************************************
      FUNCTION ZDMPMP(I3,I4,I5,I6)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION PROP2(NUP,NUP),PROP3(NUP,NUP,NUP),PROP23(NUP,NUP,NUP)
      COMMON /PROPS/ PROP2,PROP3,PROP23
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DPARMS/ P1,P2,P3,P4
      SAVE /DPARMS/,/DOTPR/,/PROPS/,/ZFUNCT/
*
      Z1=-ZD(1,I6)*ZD(2,I6)*ZUD(I5,I3)**2*ZZ(I4,1,I6,2)**2
      Z2=-ZD(1,I4)*ZUD(2,I5)*ZZ(I4,2,I6,I5)*ZZ(I6,2,I5,I3)**2
      Z3=ZD(I6,I4)**2*ZUD(1,I3)*ZUD(2,I3)*ZZ(1,I6,I4,I5)**2
      Z4=+ZUD(1,I5)*ZUD(2,I3)*ZUD(2,I5)*ZUD(I3,I4)*ZD(1,2)*ZD(2,I6)*
     .    ZD(1,I4)*ZD(I4,I6)*ZZ(I4,1,I6,2)
     .   -ZUD(1,2)*ZUD(1,I3)*ZUD(2,I5)*ZUD(I3,I5)*ZD(1,I4)*ZD(1,I6)*
     .    ZD(2,I4)*ZD(I5,I6)*ZZ(1,I4,I6,I5)
     .   +PROP2(1,I3)*PROP2(2,I6)*ZUD(I3,I5)*ZD(I4,I6)*(ZUD(2,I3)*
     .    ZD(I4,I6)*ZZ(1,I4,I6,I5)-ZUD(I3,I5)*ZD(I4,I6)*
     .    ZZ(1,I3,I5,2)+ZUD(I3,I5)*ZD(1,I6)*ZZ(I4,I3,I5,2))
     .   +PROP2(1,2)*ZUD(I3,I5)*ZUD(2,I5)*ZD(I4,I6)*ZD(1,I4)*(
     .    (PROP2(1,I3)-PROP2(I4,I5)+PROP2(2,I6))*ZZ(I6,2,I5,I3)
     .    +PROP2(2,I5)*ZUD(1,I3)*ZD(1,I6) - PROP2(1,I4)*ZUD(2,I3)
     .     *ZD(2,I6) + ZUD(1,I3)*ZD(I6,2)*ZZ(1,I3,I5,2))
      ZDMPMP=+ Z1/P1 + Z2/P2 + Z3/P3 +  Z4/P4
*
      END
*
************************************************************************
* COMPLEX FUNCTION ZDMMPP evaluates D(+-;--++).                        *
************************************************************************
      FUNCTION ZDMMPP(I3,I4,I5,I6)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DPARMS/ P1,P2,P3,P4
      SAVE /DPARMS/,/DOTPR/,/ZFUNCT/
*
      Z1=-ZD(1,I6)*ZD(2,I6)*ZUD(I4,I3)**2*ZZ(I5,1,I6,2)**2
      Z2=(0.0,0.0)
      Z3=ZD(I6,I5)**2*ZUD(1,I3)*ZUD(2,I3)*ZZ(1,I5,I6,I4)**2
      Z4=ZD(2,I6)*ZD(I6,I5)*ZUD(I4,I3)*ZUD(I3,1)*ZZ(I5,1,I6,2)*
     .   ZZ(1,I5,I6,2)*ZZ(1,I5,I6,I4)
      ZDMMPP=+ Z1/P1 + Z2/P2 + Z3/P3 + Z4/P4
      END
*
**********************************************************************
* SUBROUTINE Q2MGNS(N,PLAB,IQ,IQB,RMQ,IHRN,OUT)                      *
*    calculates q qb Ng, where the quarks can be massive.            *
**********************************************************************
* input:  Array with 2+N momenta. The fourth component is the energy.*
*         All energies>0. Declaration REAL*8 PLAB(1:4,*)             *
*         IQ1 is the quark, IQ2 the anti-quark.                      *
**********************************************************************
* OUTPUT: No spin and colour averaging, no bose factor.              *
*         Normally this would be                                     *
*         1/4 * 1/9 * 1/N!      for q qb -> NG                       *
*         1/4 * 1/24 * 1/(N-1)! for q g  -> q (N-1)g                 *
*         1/4 * 1/81 * 1/(N-2)! for g g  -> q qb (N-2)g              *
*         OUT(1)/OUT(2)     = Total EXACT/LO of q qb Ng.             *
**********************************************************************
      SUBROUTINE Q2MGNS(N,PLAB,IQ1,IQ2,RMQ,IHRN,OUT)
      PARAMETER(NGUP=10,NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2)
      DIMENSION COLMAT(NUPFAC,NUPFAC)
      DIMENSION P(0:3,1:NGUP)
      SAVE INIT,NCOL,IORDER,COLMAT
      DATA INIT,NCOL,IORDER /1,3,99/
*
* General initialization
*
      IF (INIT.EQ.1) THEN
        CALL QQBCOL(COLMAT,N,NCOL,IORDER)
        CALL INIMAP(N)
        INIT=0
      END IF
*
* Transfrom Vectors, normalize vectors according to energies.
*
      CALL MAPEVE(N+2,PLAB,P,SCALE)
      RMQS=RMQ/SCALE
*
* Shuffle momenta such that first is IQ, second is IQB ...
*
      CALL RESHQS(P,IQ1,IQ2,0,0,2)
*
      FACTOR=1.0 / SCALE**(2*N-4)
*
* Init spinors and propagators
*
      CALL INITSP(P(0,3),N,P(0,1),2)
*
      CALL MQQBM(N,SQUARE,SQUALO,COLMAT,NCOL,RMQS,IHRN)
*
      OUT(1)=SQUARE*FACTOR
      OUT(2)=SQUALO*FACTOR
*
      END
*
************************************************************************
* SUBROUTINE MQQBM(N,SQUARE,SQUALO,COLMAT,NCOL,RMQ,IHRN) determines    *
* all N! D-functions 2**(N-1) times and squares them with              *
* the colour matrix COLMAT into SQUARE. Mass of quarks in RMQ.         *
*                                                                      *
* conventions: ZJ(2,2,2,0:NUP)                                         *
*       first  index = spinor index                                    *
*       second index = contravariant undotted, covariant dotted index  *
*       third  index = + hel, - hel index                              *
*       fourth index = length of current.                              *
************************************************************************
      SUBROUTINE MQQBM(N,SQUARE,SQUALO,COLMAT,NCOL,RMQ,IHRN)
      PARAMETER(NUP=5,NGUP=10,NUPM2=20,NUPM3=60,
     .          NUPM4=120,NUPM5=120,NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /SPEED/ IMASS
      REAL*8 RN
*
* Subroutine parameters
*
      DIMENSION COLMAT(NUPFAC,NUPFAC)
*
* Global variables
*
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /SPING/  ZKO(2,NGUP),ZKOD(2,NGUP),ZHVEC(2,2,2,NGUP)
*
* Local variables
*
      DIMENSION ZVP(2,2,2),ZQ(2,2,2,NUPM5,0:NUP),IHELI(NUP)
      DIMENSION ZD(NUPFAC,2,2),ZH(2,2,NUP)
      SAVE /SPING/,/GLUOPR/,/SPEED/
*
      SQUALO=0.0
      SQUARE=0.0
      IMASS=0
      IF (RMQ.GT.0.0) IMASS=1
*
* Init V(P) anti-quark spinor.
*
      CALL QBCUR(2,RMQ,0,0,ZVP,ZQ(1,1,1,1,1),ZQ(1,1,1,1,2),
     .   ZQ(1,1,1,1,3),ZQ(1,1,1,1,4),ZQ(1,1,1,1,5))
*
      IF (IHRN.EQ.1) THEN
        IHELRN=1+INT(2**(N-1)*RN(1))
      ELSE
        IHELRN=0
      END IF
*
* Sum over half the helicity combinations
*
      DO 10 I=1,2**(N-1)
*
* Check whether this hel combination needs to be evaluated.
*
        IF ((IHRN.EQ.1).AND.(I.NE.IHELRN)) GOTO 10
*
* Init helicity vectors before call to CURS
*
        IHEL=I-1
        DO 15 J=1,N
          IHELI(J)=MOD(IHEL,2)+1
          IHEL=IHEL/2
   15   CONTINUE
        DO 20 J=1,N
          CALL COPYTS(ZH(1,1,J),ZHVEC(1,1,IHELI(J),J),1)
   20   CONTINUE
*
* Init all gluonic currents up to length N, helicities in ZH
*
        CALL INITPR(ZP1,N)
        CALL CURS(N,1,ZH,0)
*
* Init all quark currents.
*
        CALL QCUR(1,RMQ,N,0,ZQ(1,1,1,1,0),ZQ(1,1,1,1,1),
     .      ZQ(1,1,1,1,2),ZQ(1,1,1,1,3),ZQ(1,1,1,1,4),ZQ(1,1,1,1,5))
*
* Contract with V(P)-spinor to give four helicity D-functions
*
        IF (IMASS.EQ.1) THEN
          DO 70 J=1,IFAC(N)
            DO 80 IH=1,2
              ZD(J,IH,1)= +ZQ(1,1,IH,J,N)*ZVP(1,1,1)
     .                    +ZQ(1,2,IH,J,N)*ZVP(1,2,1)
     .                    +ZQ(2,1,IH,J,N)*ZVP(2,1,1)
     .                    +ZQ(2,2,IH,J,N)*ZVP(2,2,1)
              ZD(J,IH,2)= +ZQ(1,1,IH,J,N)*ZVP(1,1,2)
     .                    +ZQ(1,2,IH,J,N)*ZVP(1,2,2)
     .                    +ZQ(2,1,IH,J,N)*ZVP(2,1,2)
     .                    +ZQ(2,2,IH,J,N)*ZVP(2,2,2)
   80       CONTINUE
   70     CONTINUE
        ELSE
          DO 71 J=1,IFAC(N)
              ZD(J,1,1)= +ZQ(1,1,1,J,N)*ZVP(1,1,1)
     .                   +ZQ(2,1,1,J,N)*ZVP(2,1,1)
              ZD(J,1,2)= +ZQ(1,2,1,J,N)*ZVP(1,2,1)
     .                   +ZQ(2,2,1,J,N)*ZVP(2,2,1)
   71     CONTINUE
        END IF
*
* Square all D-functions with the colour matrix.
*
        CALL SQUAQ2(N,ZD(1,1,1),COLMAT,SQLO11,SQRE11)
        CALL SQUAQ2(N,ZD(1,1,2),COLMAT,SQLO12,SQRE12)
        SQUALO=SQUALO+SQLO11+SQLO12
        SQUARE=SQUARE+SQRE11+SQRE12
        IF (IMASS.EQ.1) THEN
          CALL SQUAQ2(N,ZD(1,2,1),COLMAT,SQLO21,SQRE21)
          CALL SQUAQ2(N,ZD(1,2,2),COLMAT,SQLO22,SQRE22)
          SQUALO=SQUALO+SQLO21+SQLO22
          SQUARE=SQUARE+SQRE21+SQRE22
        END IF
*
   10 CONTINUE
*
      SQUARE=2.0*SQUARE*(NCOL**2-1.0)/(2.0**N*NCOL**(N-1))
      SQUALO=2.0*SQUALO*(NCOL**2-1.0)*NCOL**(N-1)/(2.0**N)
*
      IF (IHRN.EQ.1) THEN
        SQUARE=SQUARE*2**(N-1)
        SQUALO=SQUALO*2**(N-1)
      END IF
*
      END
*
      SUBROUTINE SQUAQ2(N,ZD,COLMAT,SQUALO,SQUARE)
      PARAMETER(NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
      DIMENSION ZD(NUPFAC),COLMAT(NUPFAC,NUPFAC)
*
      SQUALO=0.0
      SQUARE=0.0
*
      DO 10 I=1,IFAC(N)
        R=REAL(ZD(I)*CONJG(ZD(I)))
        SQUALO=SQUALO+R
        SQUARE=SQUARE+COLMAT(I,I)*R
        Z=(0.0,0.0)
        DO 20 J=I+1,IFAC(N)
          Z=Z+COLMAT(I,J)*ZD(J)
   20   CONTINUE
        SQUARE=SQUARE+2.0*REAL(ZD(I)*CONJG(Z))
   10 CONTINUE
*
      END
*
************************************************************************
* QQBCOL calculates the colour matrix for q qb --> N gluons            *
*                                                                      *
* input: COLMAT(NUP! , NUP!)  real*4 colour matrix.                    *
*        N                    number of gluons.                        *
*        NCOL                 number of SU(N) colours.                 *
*        IORDER               numbers of terms in colour polynomial.   *
*                             to take into account.                    *
*                                                                      *
* colour matrix left out factor: NCOL * (NCOL**2 - 1)/ (2*NCOL)**N     *
************************************************************************
      SUBROUTINE QQBCOL(COLMAT,N,NCOL,IORDER)
      PARAMETER (NUP=5,NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      DIMENSION COLMAT(1:NUPFAC,1:NUPFAC)
      INTEGER POL(0:2*NUP),P(1:NUP),Q(1:NUP),PERS(NUP,NUPFAC/NUP)
*
* calculate first row
*
      CALL INVID(1,P,N)
      DO 10 J=1,IFAC(N)
        CALL INVID(J,Q,N)
        CALL TRQQB(P,Q,N,POL)
        COLMAT(1,J)=0.0
        DO 20 K=N-1,0,-1
          COLMAT(1,J)=COLMAT(1,J)*NCOL*NCOL
          IF(IORDER.GE.(N-K)) COLMAT(1,J)=COLMAT(1,J)+POL(2*K)
   20   CONTINUE
   10 CONTINUE
*
* initialize other (n-1)! - 1 rows of colour matrix
*
      DO 30 I=1,IFAC(N-1)
        CALL INVID(I,P,N)
        DO 35 K=1,N
   35     PERS(P(K),I)=K
   30 CONTINUE
      DO 70 I=1,IFAC(N)
        CALL INVID(I,P,N)
        DO 60 J=1,IFAC(N-1)
          DO 50 K=1,N
   50       Q(K)=PERS(P(K),J)
          IDEE=ID(Q,N)
          COLMAT(J,I)=COLMAT(1,IDEE)
   60   CONTINUE
   70 CONTINUE
*
* fill rest of matrix by using the first (n-1)! rows
*
      DO 100 IROT=1,N-1
        IR=IFAC(N-1)*IROT
        IS=IFAC(N)-IR
        DO 90 I=1,IFAC(N-1)
          DO 19 K=1,IR
            COLMAT(IR+I,K)=COLMAT(I,K+IS)
   19     CONTINUE
          DO 21 K=1+IR,IFAC(N)
            COLMAT(IR+I,K)=COLMAT(I,K-IR)
   21     CONTINUE
   90   CONTINUE
  100 CONTINUE
*
      END
*
************************************************************************
* TRQQB calculates the colour trace occuring in q qb --> N gluons      *
*                                                                      *
* input: P,Q                  permutaion of D-functions                *
*        N                    number of gluons.                        *
*        POL                  output colour polynomial                 *
*                                                                      *
* method: eliminate fundamental T matrices with                        *
*   T(a,i,j)*T(a,k,l) = 1/2 * ( D(i,l)*D(j,k)- 1/NCOL * D(i,j)*D(k,l)) *
* the delta functions arithmetic is done in the IT-array               *
*                                                                      *
* polynomial left out factor: NCOL * (NCOL**2-1) / (2*NCOL)**N         *
************************************************************************
      SUBROUTINE TRQQB(P,Q,N,POL)
      PARAMETER(NUP=6)
      INTEGER P(1:*),Q(1:*),N,POL(0:*)
      INTEGER PI(1:NUP),QI(1:NUP),IT(1:2*NUP)
 
      DO 10 I=0,2*N
   10   POL(I)=0
 
      DO 20 I=1,N
        PI(P(I))=I
        QI(Q(N+1-I))=I
   20 CONTINUE
      DO 80 I=1,2**N
        IBIT=I-1
        NPOW=N-1
        NSIGN=1
        DO 30 J=1,2*N
   30     IT(J)=J
        DO 60 J=1,N
          K1=PI(J)
          K3=N+QI(J)
          IF (MOD(IBIT,2).EQ.0) THEN
            K2=1+MOD(K1,2*N)
            K4=1+MOD(K3,2*N)
            NPOW=NPOW-1
            NSIGN=-NSIGN
          ELSE
            K2=1+MOD(K3,2*N)
            K4=1+MOD(K1,2*N)
          END IF
   40     L=K2
          K2=IT(L)
          IF (K2.NE.L) GOTO 40
          IT(K1)=K2
   50     L=K4
          K4=IT(L)
          IF (K4.NE.L) GOTO 50
          IT(K3)=K4
          IBIT=IBIT/2
   60   CONTINUE
        DO 70 J=1,2*N
   70     IF (IT(J).EQ.J) NPOW=NPOW+1
        POL(NPOW)=POL(NPOW)+NSIGN
   80 CONTINUE
*
* divide out a factor (NCOL**2-1)
*
      POL(0)=-POL(0)
      POL(1)=-POL(1)
      DO 90 I=2,2*N
   90   POL(I)=POL(I-2)-POL(I)
*
      END
*
************************************************************************
* Subroutine Q4S calls the matrix elements with four quarks            *
* and N (0,1,2 or 3) gluons. For N<3 specialised matrix elements       *
* preferred above the recursive techniques. This when mq=0             *
* The general procedure to call a matrix is through:                   *
*                                                                      *
* SUBROUTINE Q4S(PLAB,NPART,IQ,IQB,RMQ,IQP,IQBP,RMQP,IHRN,OUT)         *
*    with  PLAB  = Momenta of partons                                  *
*          NPART = Number of partons = 2 + # gluons                    *
*          IQ    = Position of outgoing quark                          *
*          IQB   = Position of outgoing anti-quark                     *
*          RMQ   = Mass of quarks                                      *
*          IQP, IQBP and RMQP same for second quark pair.              *
*          IHRN  = Boolean indicating whether to do a MC over hels.    *
*          OUT   = result for different/same flavours.                 *
************************************************************************
      SUBROUTINE Q4S(PLAB,NPART,IQ,IQB,RMQ,IQP,IQBP,RMQP,IHRN,OUT)
      IMPLICIT REAL*8 (A-H,O-Y)
      DIMENSION PLAB(4,*),OUT(2)
*
      IF (NPART.EQ.4) THEN
        CALL Q4G0S(PLAB,IQ,IQB,REAL(RMQ),IQP,IQBP,REAL(RMQP),OUT)
      ELSE IF (NPART.EQ.5) THEN
        IF (RMQ.GT.0D0) THEN
          CALL Q4MG1S(PLAB,IQP,IQBP,IQ,IQB,REAL(RMQ),OUT)
        ELSE IF (RMQP.GT.0D0) THEN
          CALL Q4MG1S(PLAB,IQ,IQB,IQP,IQBP,REAL(RMQP),OUT)
        ELSE
          CALL Q4G1S(PLAB,IQ,IQB,IQP,IQBP,OUT)
        END IF
      ELSE IF (NPART.EQ.6) THEN
        IF (RMQ.GT.0D0) THEN
          CALL Q4MGNS(NPART-4,PLAB,IQ,IQB,REAL(RMQ),
     .                             IQP,IQBP,REAL(RMQP),IHRN,OUT)
        ELSE IF (RMQP.GT.0D0) THEN
          CALL Q4MGNS(NPART-4,PLAB,IQ,IQB,REAL(RMQ),
     .                             IQP,IQBP,REAL(RMQP),IHRN,OUT)
        ELSE
          CALL Q4G2S(PLAB,IQ,IQB,IQP,IQBP,IHRN,OUT)
        END IF
      ELSE
        CALL Q4MGNS(NPART-4,PLAB,IQ,IQB,REAL(RMQ),
     .                           IQP,IQBP,REAL(RMQP),IHRN,OUT)
      END IF
*
      END
*
**********************************************************************
* SUBROUTINE Q4G0S(PLAB,IQ,IQB,RMQ,IQP,IQBP,RMQP,OUT)                *
**********************************************************************
* Output: No spin and colour averaging, no bose factor.              *
*         OUT(1) = M^2 (DIFFERENT FLAVOURS)                          *
*         OUT(2) = M^2 (SAME FLAVOURS)                               *
* Implicit parameters: NCOL=3                                        *
**********************************************************************
* Checked with Kunszt. Timing (sum over processes)                   *
*         Kunszt= 1.10 per 2000             Kuijf= 1.26 per 2000     *
**********************************************************************
      SUBROUTINE Q4G0S(PLAB,IQ,IQB,RMQ,IQP,IQBP,RMQP,OUT)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2)
      DIMENSION PP(NUP,NUP)
*
* Mandelstam variables.
*
      DO 10 I1=1,3
        DO 10 I2=I1+1,4
          PP(I1,I2)=REAL(PLAB(4,I1)*PLAB(4,I2)-PLAB(3,I1)*PLAB(3,I2)
     .                  -PLAB(2,I1)*PLAB(2,I2)-PLAB(1,I1)*PLAB(1,I2))
          IF(I1.LT.3) PP(I1,I2)=-PP(I1,I2)
          IF(I2.LT.3) PP(I1,I2)=-PP(I1,I2)
          PP(I2,I1)=PP(I1,I2)
   10 CONTINUE
*
      IMASS=0
      IF ((RMQ.GT.0.0).OR.(RMQP.GT.0.0)) IMASS=1
*
* Different flavours piece.
      R1=PP(IQ,IQP)*PP(IQB,IQBP)+PP(IQ,IQBP)*PP(IQB,IQP)
      IF (IMASS.EQ.1) THEN
        R1=R1+RMQ**2*PP(IQP,IQBP)
     .       +RMQP**2*(PP(IQ,IQB)+2.0*RMQ**2)
      END IF
      R1=R1*16.0/((PP(IQ,IQB)+RMQ**2)**2)
*
* Same flavour piece, masses should be equal.
      R2=PP(IQ,IQP)*PP(IQBP,IQB)+PP(IQ,IQB)*PP(IQBP,IQP)
      IF (IMASS.EQ.1) THEN
        R2=R2+RMQ**2*PP(IQP,IQB)
     .       +RMQP**2*(PP(IQ,IQBP)+2.0*RMQ**2)
      END IF
      R2=R2*16.0/((PP(IQ,IQBP)+RMQ**2)**2)
*
* Interference, masses should be equal.
      R3=2.0*PP(IQ,IQP)*PP(IQB,IQBP)
      IF (IMASS.EQ.1) THEN
        R3=R3+2.0*RMQ**2*RMQP**2+RMQP**2*PP(IQ,IQB)
     .       -RMQ*RMQP*PP(IQ,IQP)+RMQ*RMQP*PP(IQ,IQBP)
     .       +RMQ*RMQP*PP(IQB,IQP)-RMQ*RMQP*PP(IQB,IQBP)
     .       +RMQ**2*PP(IQP,IQBP)
      END IF
      R3=-16.0/3.0*R3/(PP(IQ,IQB)+RMQ**2)/(PP(IQ,IQBP)+RMQP**2)
*
      OUT(1)=R1
      OUT(2)=R1+R2+R3
*
      END
*
**********************************************************************
* SUBROUTINE Q4G1S(PLAB,IQ,IQB,IQP,IQBP,OUT)                         *
**********************************************************************
* Output: No spin and colour averaging, no bose factor.              *
*         OUT(1) = M^2 (DIFFERENT FLAVOURS)                          *
*         OUT(2) = M^2 (SAME FLAVOURS)                               *
* Implicit parameters: NCOL=3                                        *
**********************************************************************
      SUBROUTINE Q4G1S(PLAB,IQ,IQB,IQP,IQBP,OUT)
      PARAMETER(NGUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2)
      DIMENSION P(0:3,NGUP)
      COMMON /DOTPR/ ZUD(NGUP,NGUP),ZD(NGUP,NGUP)
      DIMENSION COLMAT(4,4),ZDIFF(4),ZSAME(4)
      SAVE /DOTPR/,COLMAT
      DATA COLMAT / 12.0, 0.0, 4.0, 4.0, 0.0,12.0, 4.0, 4.0,
     .               4.0, 4.0,12.0, 0.0, 4.0, 4.0, 0.0,12.0/
*
      CALL MAPEVE(5,PLAB,P,SCALE)
      CALL RESHQS(P,IQ,IQB,IQP,IQBP,4)
      FACTOR=2.0/SCALE**2
      CALL SETGSP(P,5,0)
*
      Z1=ZUD(1,3)**2*ZD(1,3)**2+ZUD(2,4)**2*ZD(2,4)**2
      Z2=ZUD(1,4)**2*ZD(1,4)**2+ZUD(2,3)**2*ZD(2,3)**2
      Z3=ZUD(1,2)**2*ZD(1,2)**2+ZUD(3,4)**2*ZD(3,4)**2
*
      ZDIFF(1)=ZUD(1,2)/ZUD(1,5)/ZUD(5,2)
      ZDIFF(2)=ZUD(3,4)/ZUD(3,5)/ZUD(5,4)
      ZDIFF(3)=ZUD(1,4)/ZUD(1,5)/ZUD(5,4)
      ZDIFF(4)=ZUD(3,2)/ZUD(3,5)/ZUD(5,2)
*
      DO 10 I=1,2
        ZSAME(I)=ZDIFF(I)/(-ZUD(1,4)*ZUD(3,2))
        ZSAME(I+2)=ZDIFF(I+2)/(3.0*ZUD(1,4)*ZUD(3,2))
        ZDIFF(I)=ZDIFF(I)/(-3.0*ZUD(1,2)*ZUD(3,4))
        ZDIFF(I+2)=ZDIFF(I+2)/(ZUD(1,2)*ZUD(3,4))
   10 CONTINUE
*
      R1=SQUARE(COLMAT,4,ZDIFF,4)
      R2=SQUARE(COLMAT,4,ZSAME,4)
*
      DO 20 I=1,4
        ZSAME(I)=ZSAME(I)+ZDIFF(I)
   20 CONTINUE
      R3=SQUARE(COLMAT,4,ZSAME,4)
*
      OUT(1)=+R1*REAL(Z1+Z2)*FACTOR*2.0
      OUT(2)=(+R1*REAL(Z2)+R3*REAL(Z1)+R2*REAL(Z3))*FACTOR*2.0
*
      END
*
**********************************************************************
* SUBROUTINE Q4MG1S(PLAB,IQ,IQB,IQP,IQBP,RMQP,OUT)                   *
**********************************************************************
* Output: No spin and colour averaging, no bose factor.              *
*         OUT(1) = M^2 (DIFFERENT FLAVOURS)                          *
*         OUT(2) = M^2 (SAME FLAVOURS)  == 0 (different masses       *
* Implicit parameters: NCOL=3                                        *
* Optimised for speed, general massless method in Q4G1S              *
* Calculation done with FORM - symbolic manipulation program.        *
**********************************************************************
      SUBROUTINE Q4MG1S(PLAB,IQ,IQB,IQP,IQBP,RMQP,OUT)
      PARAMETER(NUP=10,NCOL=3)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2)
      DIMENSION PP(NUP,NUP)
*
      A1=0.5*(NCOL**2-1.0)*(NCOL-1.0/NCOL)
      A2=0.5*NCOL*(NCOL**2-1.0)
      A3=-1.0/NCOL*(NCOL**2-1.0)
*
      DO 10 I=1,4
        DO 10 J=I+1,5
          I1=1
          IF (I.LT.3) I1=-1
          J1=1
          IF (J.LT.3) J1=-1
          PP(I,J)=REAL(  (I1*PLAB(4,I)+J1*PLAB(4,J))**2
     .                  -(I1*PLAB(1,I)+J1*PLAB(1,J))**2
     .                  -(I1*PLAB(2,I)+J1*PLAB(2,J))**2
     .                  -(I1*PLAB(3,I)+J1*PLAB(3,J))**2)
          PP(J,I)=PP(I,J)
   10 CONTINUE
*
* Find gluon index
*
      DO 20 I=1,5
       IF ((I.NE.IQ).AND.(I.NE.IQB).AND.(I.NE.IQP).AND.(I.NE.IQBP)) IG=I
   20 CONTINUE
*
      PIJ=PP(IQ,IQB)
      PIK=PP(IQ,IQP)
      PIL=PP(IQ,IQBP)
      PIG=PP(IQ,IG)
      PJK=PP(IQB,IQP)
      PJL=PP(IQB,IQBP)
      PJG=PP(IQB,IG)
      PKL=PP(IQP,IQBP)
      PKG=PP(IQP,IG)
      PLG=PP(IQBP,IG)
*
      F1=0.5/PIG/PKL
      F2=0.5/PJG/PKL
      F3=1.0/PIJ/PKL
      F4=0.5/PIJ/(PKG-RMQP**2)
      F5=0.5/PIJ/(PLG-RMQP**2)
*
C Expression for matrix element qq QQ g
      T=0.0
      IF (RMQP.EQ.T) GOTO 999
*
      T=T
     +  + RMQP**6* (  - 128*F1*F3*A2 + 128*F1*F4*A3 - 128*F1*F5*A2 - 128
     +    *F1*F5*A3 - 128*F2*F3*A2 - 128*F2*F4*A2 - 128*F2*F4*A3 + 128*
     +    F2*F5*A3 - 64*F3**2*A2 - 32*F3*F4*A2 - 32*F3*F5*A2 - 64*F4**2
     +    *A1 - 64*F4*F5*A3 - 64*F5**2*A1 )
      T=T
     +  + RMQP**4 * ( 32*F1**2*A1*PIG + 64*F1*F2*A3*PIJ - 112*F1*F3*A2*
     +    PIJ + 96*F1*F3*A2*PIK + 96*F1*F3*A2*PIL + 64*F1*F3*A2*PJK +
     +    64*F1*F3*A2*PJL + 80*F1*F3*A2*PIG - 16*F1*F3*A2*PJG + 32*F1*
     +    F3*A2*PKG + 32*F1*F3*A2*PLG + 112*F1*F4*A3*PIJ - 144*F1*F4*A3
     +    *PIK - 48*F1*F4*A3*PIL - 64*F1*F4*A3*PJK - 64*F1*F4*A3*PJL -
     +    112*F1*F4*A3*PIG + 16*F1*F4*A3*PJG - 48*F1*F4*A3*PKG - 16*F1*
     +    F4*A3*PLG - 112*F1*F5*A2*PIJ + 48*F1*F5*A2*PIK + 144*F1*F5*A2
     +    *PIL + 64*F1*F5*A2*PJK + 64*F1*F5*A2*PJL + 112*F1*F5*A2*PIG
     +     - 16*F1*F5*A2*PJG + 16*F1*F5*A2*PKG + 48*F1*F5*A2*PLG - 112*
     +    F1*F5*A3*PIJ + 48*F1*F5*A3*PIK + 144*F1*F5*A3*PIL + 64*F1*F5*
     +    A3*PJK + 64*F1*F5*A3*PJL + 112*F1*F5*A3*PIG - 16*F1*F5*A3*PJG
     +     + 16*F1*F5*A3*PKG + 48*F1*F5*A3*PLG + 32*F2**2*A1*PJG - 112*
     +    F2*F3*A2*PIJ + 64*F2*F3*A2*PIK + 64*F2*F3*A2*PIL + 96*F2*F3*
     +    A2*PJK + 96*F2*F3*A2*PJL - 16*F2*F3*A2*PIG + 80*F2*F3*A2*PJG
     +     + 32*F2*F3*A2*PKG + 32*F2*F3*A2*PLG - 112*F2*F4*A2*PIJ + 64*
     +    F2*F4*A2*PIK + 64*F2*F4*A2*PIL + 144*F2*F4*A2*PJK + 48*F2*F4*
     +    A2*PJL )
      T=T
     +  + RMQP**4* (  - 16*F2*F4*A2*PIG + 112*F2*F4*A2*PJG + 48*F2*F4*A2
     +    *PKG + 16*F2*F4*A2*PLG - 112*F2*F4*A3*PIJ + 64*F2*F4*A3*PIK
     +     + 64*F2*F4*A3*PIL + 144*F2*F4*A3*PJK + 48*F2*F4*A3*PJL - 16*
     +    F2*F4*A3*PIG + 112*F2*F4*A3*PJG + 48*F2*F4*A3*PKG + 16*F2*F4*
     +    A3*PLG + 112*F2*F5*A3*PIJ - 64*F2*F5*A3*PIK - 64*F2*F5*A3*PIL
     +     - 48*F2*F5*A3*PJK - 144*F2*F5*A3*PJL + 16*F2*F5*A3*PIG - 112
     +    *F2*F5*A3*PJG - 16*F2*F5*A3*PKG - 48*F2*F5*A3*PLG + 32*F3**2*
     +    A2*PIK + 32*F3**2*A2*PIL + 32*F3**2*A2*PJK + 32*F3**2*A2*PJL
     +     - 32*F3**2*A2*PKL + 16*F3**2*A2*PIG + 16*F3**2*A2*PJG + 32*
     +    F3**2*A2*PKG + 32*F3**2*A2*PLG + 32*F3*F4*A2*PIJ + 16*F3*F4*
     +    A2*PIK + 16*F3*F4*A2*PIL + 16*F3*F4*A2*PJK + 16*F3*F4*A2*PJL
     +     - 32*F3*F4*A2*PKL + 16*F3*F4*A2*PIG + 16*F3*F4*A2*PJG + 32*
     +    F3*F4*A2*PKG + 32*F3*F5*A2*PIJ + 16*F3*F5*A2*PIK + 16*F3*F5*
     +    A2*PIL + 16*F3*F5*A2*PJK + 16*F3*F5*A2*PJL - 32*F3*F5*A2*PKL
     +     + 16*F3*F5*A2*PIG + 16*F3*F5*A2*PJG + 32*F3*F5*A2*PLG - 32*
     +    F4**2*A1*PIJ + 32*F4**2*A1*PIK + 32*F4**2*A1*PIL + 32*F4**2*
     +    A1*PJK )
      T=T
     +  + RMQP**4* ( 32*F4**2*A1*PJL + 48*F4**2*A1*PIG + 48*F4**2*A1*PJG
     +     - 96*F4*F5*A3*PIJ + 32*F4*F5*A3*PIK + 32*F4*F5*A3*PIL + 32*
     +    F4*F5*A3*PJK + 32*F4*F5*A3*PJL + 32*F4*F5*A3*PKL + 32*F4*F5*
     +    A3*PIG + 32*F4*F5*A3*PJG - 32*F5**2*A1*PIJ + 32*F5**2*A1*PIK
     +     + 32*F5**2*A1*PIL + 32*F5**2*A1*PJK + 32*F5**2*A1*PJL + 48*
     +    F5**2*A1*PIG + 48*F5**2*A1*PJG )
      T=T
     +  + RMQP**2 *(  - 16*F1**2*A1*PJK*PIG - 16*F1**2*A1*PJL*PIG + 32*
     +    F1**2*A1*PIG*PJG - 16*F1**2*A1*PIG*PKG - 16*F1**2*A1*PIG*PLG
     +     + 32*F1*F2*A3*PIJ**2 - 24*F1*F2*A3*PIJ*PIK - 24*F1*F2*A3*PIJ
     +    *PIL - 24*F1*F2*A3*PIJ*PJK - 24*F1*F2*A3*PIJ*PJL + 32*F1*F2*
     +    A3*PIJ*PIG + 32*F1*F2*A3*PIJ*PJG - 16*F1*F2*A3*PIJ*PKG - 16*
     +    F1*F2*A3*PIJ*PLG - 8*F1*F2*A3*PIK*PIG + 8*F1*F2*A3*PIK*PJG -
     +    8*F1*F2*A3*PIL*PIG + 8*F1*F2*A3*PIL*PJG + 8*F1*F2*A3*PJK*PIG
     +     - 8*F1*F2*A3*PJK*PJG + 8*F1*F2*A3*PJL*PIG - 8*F1*F2*A3*PJL*
     +    PJG + 40*F1*F3*A2*PIJ*PIK + 40*F1*F3*A2*PIJ*PIL + 24*F1*F3*A2
     +    *PIJ*PKG + 24*F1*F3*A2*PIJ*PLG - 16*F1*F3*A2*PIK**2 - 32*F1*
     +    F3*A2*PIK*PIL - 24*F1*F3*A2*PIK*PJK - 72*F1*F3*A2*PIK*PJL - 8
     +    *F1*F3*A2*PIK*PIG - 16*F1*F3*A2*PIK*PKG - 16*F1*F3*A2*PIK*PLG
     +     - 16*F1*F3*A2*PIL**2 - 72*F1*F3*A2*PIL*PJK - 24*F1*F3*A2*PIL
     +    *PJL - 8*F1*F3*A2*PIL*PIG - 16*F1*F3*A2*PIL*PKG - 16*F1*F3*A2
     +    *PIL*PLG - 40*F1*F3*A2*PJK*PIG - 8*F1*F3*A2*PJK*PKG - 24*F1*
     +    F3*A2*PJK*PLG - 40*F1*F3*A2*PJL*PIG - 24*F1*F3*A2*PJL*PKG - 8
     +    *F1*F3*A2*PJL*PLG )
      T=T
     +  + RMQP**2 * ( 16*F1*F3*A2*PKL*PIG + 32*F1*F3*A2*PIG*PJG - 16*F1*
     +    F3*A2*PIG*PKG - 16*F1*F3*A2*PIG*PLG - 64*F1*F4*A3*PIJ*PIK -
     +    16*F1*F4*A3*PIJ*PIL - 32*F1*F4*A3*PIJ*PIG - 48*F1*F4*A3*PIJ*
     +    PKG + 32*F1*F4*A3*PIK**2 + 32*F1*F4*A3*PIK*PIL + 48*F1*F4*A3*
     +    PIK*PJK + 96*F1*F4*A3*PIK*PJL + 32*F1*F4*A3*PIK*PIG - 16*F1*
     +    F4*A3*PIK*PJG + 32*F1*F4*A3*PIK*PKG + 16*F1*F4*A3*PIK*PLG +
     +    48*F1*F4*A3*PIL*PJK + 16*F1*F4*A3*PIL*PIG + 16*F1*F4*A3*PIL*
     +    PJG + 16*F1*F4*A3*PIL*PKG + 16*F1*F4*A3*PJK*PIG + 16*F1*F4*A3
     +    *PJK*PKG + 16*F1*F4*A3*PJK*PLG + 96*F1*F4*A3*PJL*PIG + 32*F1*
     +    F4*A3*PJL*PKG - 16*F1*F4*A3*PKL*PIG - 32*F1*F4*A3*PIG*PJG +
     +    32*F1*F4*A3*PIG*PKG + 16*F1*F5*A2*PIJ*PIK + 64*F1*F5*A2*PIJ*
     +    PIL + 32*F1*F5*A2*PIJ*PIG + 48*F1*F5*A2*PIJ*PLG - 32*F1*F5*A2
     +    *PIK*PIL - 48*F1*F5*A2*PIK*PJL - 16*F1*F5*A2*PIK*PIG - 16*F1*
     +    F5*A2*PIK*PJG - 16*F1*F5*A2*PIK*PLG - 32*F1*F5*A2*PIL**2 - 96
     +    *F1*F5*A2*PIL*PJK - 48*F1*F5*A2*PIL*PJL - 32*F1*F5*A2*PIL*PIG
     +     + 16*F1*F5*A2*PIL*PJG - 16*F1*F5*A2*PIL*PKG - 32*F1*F5*A2*
     +    PIL*PLG )
      T=T
     +  + RMQP**2 *(  - 96*F1*F5*A2*PJK*PIG - 32*F1*F5*A2*PJK*PLG - 16*
     +    F1*F5*A2*PJL*PIG - 16*F1*F5*A2*PJL*PKG - 16*F1*F5*A2*PJL*PLG
     +     + 16*F1*F5*A2*PKL*PIG + 32*F1*F5*A2*PIG*PJG - 32*F1*F5*A2*
     +    PIG*PLG + 16*F1*F5*A3*PIJ*PIK + 64*F1*F5*A3*PIJ*PIL + 32*F1*
     +    F5*A3*PIJ*PIG + 48*F1*F5*A3*PIJ*PLG - 32*F1*F5*A3*PIK*PIL -
     +    48*F1*F5*A3*PIK*PJL - 16*F1*F5*A3*PIK*PIG - 16*F1*F5*A3*PIK*
     +    PJG - 16*F1*F5*A3*PIK*PLG - 32*F1*F5*A3*PIL**2 - 96*F1*F5*A3*
     +    PIL*PJK - 48*F1*F5*A3*PIL*PJL - 32*F1*F5*A3*PIL*PIG + 16*F1*
     +    F5*A3*PIL*PJG - 16*F1*F5*A3*PIL*PKG - 32*F1*F5*A3*PIL*PLG -
     +    96*F1*F5*A3*PJK*PIG - 32*F1*F5*A3*PJK*PLG - 16*F1*F5*A3*PJL*
     +    PIG - 16*F1*F5*A3*PJL*PKG - 16*F1*F5*A3*PJL*PLG + 16*F1*F5*A3
     +    *PKL*PIG + 32*F1*F5*A3*PIG*PJG - 32*F1*F5*A3*PIG*PLG - 16*
     +    F2**2*A1*PIK*PJG - 16*F2**2*A1*PIL*PJG + 32*F2**2*A1*PIG*PJG
     +     - 16*F2**2*A1*PJG*PKG - 16*F2**2*A1*PJG*PLG + 40*F2*F3*A2*
     +    PIJ*PJK + 40*F2*F3*A2*PIJ*PJL + 24*F2*F3*A2*PIJ*PKG + 24*F2*
     +    F3*A2*PIJ*PLG - 24*F2*F3*A2*PIK*PJK - 72*F2*F3*A2*PIK*PJL -
     +    40*F2*F3*A2*PIK*PJG )
      T=T
     +  + RMQP**2 *(  - 8*F2*F3*A2*PIK*PKG - 24*F2*F3*A2*PIK*PLG - 72*F2
     +    *F3*A2*PIL*PJK - 24*F2*F3*A2*PIL*PJL - 40*F2*F3*A2*PIL*PJG -
     +    24*F2*F3*A2*PIL*PKG - 8*F2*F3*A2*PIL*PLG - 16*F2*F3*A2*PJK**2
     +     - 32*F2*F3*A2*PJK*PJL - 8*F2*F3*A2*PJK*PJG - 16*F2*F3*A2*PJK
     +    *PKG - 16*F2*F3*A2*PJK*PLG - 16*F2*F3*A2*PJL**2 - 8*F2*F3*A2*
     +    PJL*PJG - 16*F2*F3*A2*PJL*PKG - 16*F2*F3*A2*PJL*PLG + 16*F2*
     +    F3*A2*PKL*PJG + 32*F2*F3*A2*PIG*PJG - 16*F2*F3*A2*PJG*PKG -
     +    16*F2*F3*A2*PJG*PLG + 64*F2*F4*A2*PIJ*PJK + 16*F2*F4*A2*PIJ*
     +    PJL + 32*F2*F4*A2*PIJ*PJG + 48*F2*F4*A2*PIJ*PKG - 48*F2*F4*A2
     +    *PIK*PJK - 48*F2*F4*A2*PIK*PJL - 16*F2*F4*A2*PIK*PJG - 16*F2*
     +    F4*A2*PIK*PKG - 16*F2*F4*A2*PIK*PLG - 96*F2*F4*A2*PIL*PJK -
     +    96*F2*F4*A2*PIL*PJG - 32*F2*F4*A2*PIL*PKG - 32*F2*F4*A2*
     +    PJK**2 - 32*F2*F4*A2*PJK*PJL + 16*F2*F4*A2*PJK*PIG - 32*F2*F4
     +    *A2*PJK*PJG - 32*F2*F4*A2*PJK*PKG - 16*F2*F4*A2*PJK*PLG - 16*
     +    F2*F4*A2*PJL*PIG - 16*F2*F4*A2*PJL*PJG - 16*F2*F4*A2*PJL*PKG
     +     + 16*F2*F4*A2*PKL*PJG + 32*F2*F4*A2*PIG*PJG - 32*F2*F4*A2*
     +    PJG*PKG )
      T=T
     +  + RMQP**2* ( 64*F2*F4*A3*PIJ*PJK + 16*F2*F4*A3*PIJ*PJL + 32*F2*
     +    F4*A3*PIJ*PJG + 48*F2*F4*A3*PIJ*PKG - 48*F2*F4*A3*PIK*PJK -
     +    48*F2*F4*A3*PIK*PJL - 16*F2*F4*A3*PIK*PJG - 16*F2*F4*A3*PIK*
     +    PKG - 16*F2*F4*A3*PIK*PLG - 96*F2*F4*A3*PIL*PJK - 96*F2*F4*A3
     +    *PIL*PJG - 32*F2*F4*A3*PIL*PKG - 32*F2*F4*A3*PJK**2 - 32*F2*
     +    F4*A3*PJK*PJL + 16*F2*F4*A3*PJK*PIG - 32*F2*F4*A3*PJK*PJG -
     +    32*F2*F4*A3*PJK*PKG - 16*F2*F4*A3*PJK*PLG - 16*F2*F4*A3*PJL*
     +    PIG - 16*F2*F4*A3*PJL*PJG - 16*F2*F4*A3*PJL*PKG + 16*F2*F4*A3
     +    *PKL*PJG + 32*F2*F4*A3*PIG*PJG - 32*F2*F4*A3*PJG*PKG - 16*F2*
     +    F5*A3*PIJ*PJK - 64*F2*F5*A3*PIJ*PJL - 32*F2*F5*A3*PIJ*PJG -
     +    48*F2*F5*A3*PIJ*PLG + 96*F2*F5*A3*PIK*PJL + 96*F2*F5*A3*PIK*
     +    PJG + 32*F2*F5*A3*PIK*PLG + 48*F2*F5*A3*PIL*PJK + 48*F2*F5*A3
     +    *PIL*PJL + 16*F2*F5*A3*PIL*PJG + 16*F2*F5*A3*PIL*PKG + 16*F2*
     +    F5*A3*PIL*PLG + 32*F2*F5*A3*PJK*PJL + 16*F2*F5*A3*PJK*PIG +
     +    16*F2*F5*A3*PJK*PJG + 16*F2*F5*A3*PJK*PLG + 32*F2*F5*A3*
     +    PJL**2 - 16*F2*F5*A3*PJL*PIG + 32*F2*F5*A3*PJL*PJG + 16*F2*F5
     +    *A3*PJL*PKG )
      T=T
     +  + RMQP**2* ( 32*F2*F5*A3*PJL*PLG - 16*F2*F5*A3*PKL*PJG - 32*F2*
     +    F5*A3*PIG*PJG + 32*F2*F5*A3*PJG*PLG - 32*F3**2*A2*PIJ*PKL -
     +    16*F3**2*A2*PIK*PJK - 16*F3**2*A2*PIK*PJL + 16*F3**2*A2*PIK*
     +    PKL - 8*F3**2*A2*PIK*PJG - 8*F3**2*A2*PIK*PKG - 24*F3**2*A2*
     +    PIK*PLG - 16*F3**2*A2*PIL*PJK - 16*F3**2*A2*PIL*PJL + 16*
     +    F3**2*A2*PIL*PKL - 8*F3**2*A2*PIL*PJG - 24*F3**2*A2*PIL*PKG
     +     - 8*F3**2*A2*PIL*PLG + 16*F3**2*A2*PJK*PKL - 8*F3**2*A2*PJK*
     +    PIG - 8*F3**2*A2*PJK*PKG - 24*F3**2*A2*PJK*PLG + 16*F3**2*A2*
     +    PJL*PKL - 8*F3**2*A2*PJL*PIG - 24*F3**2*A2*PJL*PKG - 8*F3**2*
     +    A2*PJL*PLG + 16*F3**2*A2*PKL*PIG + 16*F3**2*A2*PKL*PJG + 32*
     +    F3**2*A2*PIG*PJG - 8*F3**2*A2*PIG*PKG - 8*F3**2*A2*PIG*PLG -
     +    8*F3**2*A2*PJG*PKG - 8*F3**2*A2*PJG*PLG - 32*F3*F4*A2*PIJ*PKL
     +     - 16*F3*F4*A2*PIJ*PKG - 16*F3*F4*A2*PIJ*PLG - 16*F3*F4*A2*
     +    PIK*PJK + 16*F3*F4*A2*PIK*PKL + 16*F3*F4*A2*PIK*PJG - 8*F3*F4
     +    *A2*PIK*PKG - 8*F3*F4*A2*PIK*PLG - 16*F3*F4*A2*PIL*PJL + 16*
     +    F3*F4*A2*PIL*PKL - 32*F3*F4*A2*PIL*PJG - 24*F3*F4*A2*PIL*PKG
     +     + 8*F3*F4*A2*PIL*PLG )
      T=T
     +  + RMQP**2* ( 16*F3*F4*A2*PJK*PKL + 16*F3*F4*A2*PJK*PIG - 8*F3*F4
     +    *A2*PJK*PKG - 8*F3*F4*A2*PJK*PLG + 16*F3*F4*A2*PJL*PKL - 32*
     +    F3*F4*A2*PJL*PIG - 24*F3*F4*A2*PJL*PKG + 8*F3*F4*A2*PJL*PLG
     +     + 16*F3*F4*A2*PKL*PIG + 16*F3*F4*A2*PKL*PJG + 32*F3*F4*A2*
     +    PIG*PJG - 16*F3*F4*A2*PIG*PKG - 16*F3*F4*A2*PJG*PKG - 32*F3*
     +    F5*A2*PIJ*PKL - 16*F3*F5*A2*PIJ*PKG - 16*F3*F5*A2*PIJ*PLG -
     +    16*F3*F5*A2*PIK*PJK + 16*F3*F5*A2*PIK*PKL - 32*F3*F5*A2*PIK*
     +    PJG + 8*F3*F5*A2*PIK*PKG - 24*F3*F5*A2*PIK*PLG - 16*F3*F5*A2*
     +    PIL*PJL + 16*F3*F5*A2*PIL*PKL + 16*F3*F5*A2*PIL*PJG - 8*F3*F5
     +    *A2*PIL*PKG - 8*F3*F5*A2*PIL*PLG + 16*F3*F5*A2*PJK*PKL - 32*
     +    F3*F5*A2*PJK*PIG + 8*F3*F5*A2*PJK*PKG - 24*F3*F5*A2*PJK*PLG
     +     + 16*F3*F5*A2*PJL*PKL + 16*F3*F5*A2*PJL*PIG - 8*F3*F5*A2*PJL
     +    *PKG - 8*F3*F5*A2*PJL*PLG + 16*F3*F5*A2*PKL*PIG + 16*F3*F5*A2
     +    *PKL*PJG + 32*F3*F5*A2*PIG*PJG - 16*F3*F5*A2*PIG*PLG - 16*F3*
     +    F5*A2*PJG*PLG - 32*F4**2*A1*PIJ*PKG - 32*F4**2*A1*PIK*PJL -
     +    32*F4**2*A1*PIL*PJK - 48*F4**2*A1*PIL*PJG - 48*F4**2*A1*PJL*
     +    PIG )
      T=T
     +  + RMQP**2* (  - 16*F4**2*A1*PIG*PKG - 16*F4**2*A1*PJG*PKG + 32*
     +    F4*F5*A3*PIJ*PKL + 16*F4*F5*A3*PIJ*PKG + 16*F4*F5*A3*PIJ*PLG
     +     + 16*F4*F5*A3*PIK*PJK - 48*F4*F5*A3*PIK*PJL - 16*F4*F5*A3*
     +    PIK*PKL - 16*F4*F5*A3*PIK*PJG - 8*F4*F5*A3*PIK*PKG + 8*F4*F5*
     +    A3*PIK*PLG - 48*F4*F5*A3*PIL*PJK + 16*F4*F5*A3*PIL*PJL - 16*
     +    F4*F5*A3*PIL*PKL - 16*F4*F5*A3*PIL*PJG + 8*F4*F5*A3*PIL*PKG
     +     - 8*F4*F5*A3*PIL*PLG - 16*F4*F5*A3*PJK*PKL - 16*F4*F5*A3*PJK
     +    *PIG - 8*F4*F5*A3*PJK*PKG + 8*F4*F5*A3*PJK*PLG - 16*F4*F5*A3*
     +    PJL*PKL - 16*F4*F5*A3*PJL*PIG + 8*F4*F5*A3*PJL*PKG - 8*F4*F5*
     +    A3*PJL*PLG - 16*F4*F5*A3*PKL*PIG - 16*F4*F5*A3*PKL*PJG - 32*
     +    F4*F5*A3*PIG*PJG - 32*F5**2*A1*PIJ*PLG - 32*F5**2*A1*PIK*PJL
     +     - 48*F5**2*A1*PIK*PJG - 32*F5**2*A1*PIL*PJK - 48*F5**2*A1*
     +    PJK*PIG - 16*F5**2*A1*PIG*PLG - 16*F5**2*A1*PJG*PLG )
*
  999 CONTINUE
*
      T = T
     +  + 16*F1**2*A1*PJK*PIG*PLG + 16*F1**2*A1*PJL*PIG*PKG + 16*F1*F2*
     +    A3*PIJ*PIK*PJL + 8*F1*F2*A3*PIJ*PIK*PLG + 16*F1*F2*A3*PIJ*PIL
     +    *PJK + 8*F1*F2*A3*PIJ*PIL*PKG + 8*F1*F2*A3*PIJ*PJK*PLG + 8*F1
     +    *F2*A3*PIJ*PJL*PKG - 16*F1*F2*A3*PIK*PIL*PJG + 8*F1*F2*A3*PIK
     +    *PJL*PIG + 8*F1*F2*A3*PIK*PJL*PJG + 8*F1*F2*A3*PIL*PJK*PIG +
     +    8*F1*F2*A3*PIL*PJK*PJG - 16*F1*F2*A3*PJK*PJL*PIG - 8*F1*F3*A2
     +    *PIJ*PIK*PLG - 8*F1*F3*A2*PIJ*PIL*PKG + 16*F1*F3*A2*PIK**2*
     +    PJL + 16*F1*F3*A2*PIK*PIL*PJK + 16*F1*F3*A2*PIK*PIL*PJL + 16*
     +    F1*F3*A2*PIK*PIL*PJG + 8*F1*F3*A2*PIK*PJK*PLG + 8*F1*F3*A2*
     +    PIK*PJL*PIG + 16*F1*F3*A2*PIK*PJL*PKG + 8*F1*F3*A2*PIK*PJL*
     +    PLG + 16*F1*F3*A2*PIL**2*PJK + 8*F1*F3*A2*PIL*PJK*PIG + 8*F1*
     +    F3*A2*PIL*PJK*PKG + 16*F1*F3*A2*PIL*PJK*PLG + 8*F1*F3*A2*PIL*
     +    PJL*PKG - 8*F1*F3*A2*PJK*PKL*PIG + 16*F1*F3*A2*PJK*PIG*PLG -
     +    8*F1*F3*A2*PJL*PKL*PIG + 16*F1*F3*A2*PJL*PIG*PKG + 16*F1*F4*
     +    A3*PIJ*PIL*PKG - 32*F1*F4*A3*PIK**2*PJL - 32*F1*F4*A3*PIK*PIL
     +    *PJK - 16*F1*F4*A3*PIK*PIL*PJG - 16*F1*F4*A3*PIK*PJK*PLG - 32
     +    *F1*F4*A3*PIK*PJL*PIG
      T=T
     +  - 32*F1*F4*A3*PIK*PJL*PKG - 16*F1*F4*A3*PIL*PJK*PIG - 16*F1*F4*
     +    A3*PIL*PJK*PKG + 16*F1*F4*A3*PJK*PKL*PIG - 32*F1*F4*A3*PJL*
     +    PIG*PKG - 16*F1*F5*A2*PIJ*PIK*PLG + 32*F1*F5*A2*PIK*PIL*PJL
     +     + 16*F1*F5*A2*PIK*PIL*PJG + 16*F1*F5*A2*PIK*PJL*PIG + 16*F1*
     +    F5*A2*PIK*PJL*PLG + 32*F1*F5*A2*PIL**2*PJK + 32*F1*F5*A2*PIL*
     +    PJK*PIG + 32*F1*F5*A2*PIL*PJK*PLG + 16*F1*F5*A2*PIL*PJL*PKG
     +     + 32*F1*F5*A2*PJK*PIG*PLG - 16*F1*F5*A2*PJL*PKL*PIG - 16*F1*
     +    F5*A3*PIJ*PIK*PLG + 32*F1*F5*A3*PIK*PIL*PJL + 16*F1*F5*A3*PIK
     +    *PIL*PJG + 16*F1*F5*A3*PIK*PJL*PIG + 16*F1*F5*A3*PIK*PJL*PLG
     +     + 32*F1*F5*A3*PIL**2*PJK + 32*F1*F5*A3*PIL*PJK*PIG + 32*F1*
     +    F5*A3*PIL*PJK*PLG + 16*F1*F5*A3*PIL*PJL*PKG + 32*F1*F5*A3*PJK
     +    *PIG*PLG - 16*F1*F5*A3*PJL*PKL*PIG + 16*F2**2*A1*PIK*PJG*PLG
     +     + 16*F2**2*A1*PIL*PJG*PKG - 8*F2*F3*A2*PIJ*PJK*PLG - 8*F2*F3
     +    *A2*PIJ*PJL*PKG + 16*F2*F3*A2*PIK*PJK*PJL + 8*F2*F3*A2*PIK*
     +    PJK*PLG + 16*F2*F3*A2*PIK*PJL**2 + 8*F2*F3*A2*PIK*PJL*PJG + 8
     +    *F2*F3*A2*PIK*PJL*PKG + 16*F2*F3*A2*PIK*PJL*PLG - 8*F2*F3*A2*
     +    PIK*PKL*PJG
      T=T
     +  + 16*F2*F3*A2*PIK*PJG*PLG + 16*F2*F3*A2*PIL*PJK**2 + 16*F2*F3*
     +    A2*PIL*PJK*PJL + 8*F2*F3*A2*PIL*PJK*PJG + 16*F2*F3*A2*PIL*PJK
     +    *PKG + 8*F2*F3*A2*PIL*PJK*PLG + 8*F2*F3*A2*PIL*PJL*PKG - 8*F2
     +    *F3*A2*PIL*PKL*PJG + 16*F2*F3*A2*PIL*PJG*PKG + 16*F2*F3*A2*
     +    PJK*PJL*PIG - 16*F2*F4*A2*PIJ*PJL*PKG + 32*F2*F4*A2*PIK*PJK*
     +    PJL + 16*F2*F4*A2*PIK*PJK*PLG + 16*F2*F4*A2*PIK*PJL*PJG + 16*
     +    F2*F4*A2*PIK*PJL*PKG - 16*F2*F4*A2*PIK*PKL*PJG + 32*F2*F4*A2*
     +    PIL*PJK**2 + 32*F2*F4*A2*PIL*PJK*PJG + 32*F2*F4*A2*PIL*PJK*
     +    PKG + 32*F2*F4*A2*PIL*PJG*PKG + 16*F2*F4*A2*PJK*PJL*PIG - 16*
     +    F2*F4*A3*PIJ*PJL*PKG + 32*F2*F4*A3*PIK*PJK*PJL + 16*F2*F4*A3*
     +    PIK*PJK*PLG + 16*F2*F4*A3*PIK*PJL*PJG + 16*F2*F4*A3*PIK*PJL*
     +    PKG - 16*F2*F4*A3*PIK*PKL*PJG + 32*F2*F4*A3*PIL*PJK**2 + 32*
     +    F2*F4*A3*PIL*PJK*PJG + 32*F2*F4*A3*PIL*PJK*PKG + 32*F2*F4*A3*
     +    PIL*PJG*PKG + 16*F2*F4*A3*PJK*PJL*PIG + 16*F2*F5*A3*PIJ*PJK*
     +    PLG - 32*F2*F5*A3*PIK*PJL**2 - 32*F2*F5*A3*PIK*PJL*PJG - 32*
     +    F2*F5*A3*PIK*PJL*PLG - 32*F2*F5*A3*PIK*PJG*PLG - 32*F2*F5*A3*
     +    PIL*PJK*PJL
      T=T
     +  - 16*F2*F5*A3*PIL*PJK*PJG - 16*F2*F5*A3*PIL*PJK*PLG - 16*F2*F5*
     +    A3*PIL*PJL*PKG + 16*F2*F5*A3*PIL*PKL*PJG - 16*F2*F5*A3*PJK*
     +    PJL*PIG + 16*F3**2*A2*PIK*PJK*PLG - 16*F3**2*A2*PIK*PJL*PKL
     +     + 8*F3**2*A2*PIK*PJL*PKG + 8*F3**2*A2*PIK*PJL*PLG - 8*F3**2*
     +    A2*PIK*PKL*PJG + 8*F3**2*A2*PIK*PJG*PLG - 16*F3**2*A2*PIL*PJK
     +    *PKL + 8*F3**2*A2*PIL*PJK*PKG + 8*F3**2*A2*PIL*PJK*PLG + 16*
     +    F3**2*A2*PIL*PJL*PKG - 8*F3**2*A2*PIL*PKL*PJG + 8*F3**2*A2*
     +    PIL*PJG*PKG - 8*F3**2*A2*PJK*PKL*PIG + 8*F3**2*A2*PJK*PIG*PLG
     +     - 8*F3**2*A2*PJL*PKL*PIG + 8*F3**2*A2*PJL*PIG*PKG + 16*F3*F4
     +    *A2*PIK*PJK*PLG - 16*F3*F4*A2*PIK*PJL*PKL + 8*F3*F4*A2*PIK*
     +    PJL*PKG - 8*F3*F4*A2*PIK*PJL*PLG - 8*F3*F4*A2*PIK*PKL*PJG -
     +    16*F3*F4*A2*PIL*PJK*PKL + 8*F3*F4*A2*PIL*PJK*PKG - 8*F3*F4*A2
     +    *PIL*PJK*PLG + 16*F3*F4*A2*PIL*PJL*PKG - 8*F3*F4*A2*PIL*PKL*
     +    PJG + 16*F3*F4*A2*PIL*PJG*PKG - 8*F3*F4*A2*PJK*PKL*PIG - 8*F3
     +    *F4*A2*PJL*PKL*PIG + 16*F3*F4*A2*PJL*PIG*PKG + 16*F3*F5*A2*
     +    PIK*PJK*PLG - 16*F3*F5*A2*PIK*PJL*PKL - 8*F3*F5*A2*PIK*PJL*
     +    PKG
      T=T
     +  + 8*F3*F5*A2*PIK*PJL*PLG - 8*F3*F5*A2*PIK*PKL*PJG + 16*F3*F5*A2
     +    *PIK*PJG*PLG - 16*F3*F5*A2*PIL*PJK*PKL - 8*F3*F5*A2*PIL*PJK*
     +    PKG + 8*F3*F5*A2*PIL*PJK*PLG + 16*F3*F5*A2*PIL*PJL*PKG - 8*F3
     +    *F5*A2*PIL*PKL*PJG - 8*F3*F5*A2*PJK*PKL*PIG + 16*F3*F5*A2*PJK
     +    *PIG*PLG - 8*F3*F5*A2*PJL*PKL*PIG + 16*F4**2*A1*PIL*PJG*PKG
     +     + 16*F4**2*A1*PJL*PIG*PKG - 16*F4*F5*A3*PIK*PJK*PLG + 16*F4*
     +    F5*A3*PIK*PJL*PKL + 8*F4*F5*A3*PIK*PJL*PKG + 8*F4*F5*A3*PIK*
     +    PJL*PLG + 8*F4*F5*A3*PIK*PKL*PJG + 16*F4*F5*A3*PIL*PJK*PKL +
     +    8*F4*F5*A3*PIL*PJK*PKG + 8*F4*F5*A3*PIL*PJK*PLG - 16*F4*F5*A3
     +    *PIL*PJL*PKG + 8*F4*F5*A3*PIL*PKL*PJG + 8*F4*F5*A3*PJK*PKL*
     +    PIG + 8*F4*F5*A3*PJL*PKL*PIG + 16*F5**2*A1*PIK*PJG*PLG + 16*
     +    F5**2*A1*PJK*PIG*PLG
*
      OUT(1)=ABS(T)
      OUT(2)=0D0
*
      END
*
************************************************************************
* SUBROUTINE Q4G2S(PLAB,IQ,IQB,IQP,IQBP,IHRN,OUT)                      *
*                         calculates q qb q' qb' gg                    *
************************************************************************
* input:  Array with 6 momenta. The fourth component is the energy.    *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:6)             *
*         IQ1 is the quark, IQ2 the anti-quark.                        *
************************************************************************
* output: No spin and colour averaging, no bose factor.                *
*         Normally this would be                                       *
*         1/4 * 1/9 * 1/24 for q qb -> 4g                              *
*         1/4 * 1/24 * 1/6 for q g -> q 3g                             *
*         1/4 * 1/81 * 1/2 for g g -> q qb 2g                          *
*         Additional information: REAL*8 /OUTQ4/ OUT(2)                *
*         OUT(1)/OUT(2) = DIFFERENT / SAME FLAVOURS                    *
* NOTE:   Extra factor 2 for helicities in the colour factors          *
************************************************************************
* Organisation of colour structures:                                   *
*  1: ()_12 (a1 a2)_34    2: (a1)_12 (a2)_34    3: (a1 a2)_12 ()_34    *
*  4: ()_12 (a2 a1)_34    5: (a2)_12 (a1)_34    6: (a2 a1)_12 ()_34    *
*  7: ()_14 (a1 a2)_32    8: (a1)_14 (a2)_32    9: (a1 a2)_14 ()_32    *
* 10: ()_14 (a2 a1)_32   11: (a2)_14 (a1)_32   12: (a2 a1)_14 ()_32    *
************************************************************************
* Analytical calculation by H. Kuijf & B. Tausk  (april 1990)          *
************************************************************************
      SUBROUTINE Q4G2S(PLAB,IQ,IQB,IQP,IQBP,IHRN,OUT)
      PARAMETER(NC=3,NC2=(NC**2-1),
     .          C1=NC2**2/4.0,C2=0.0,            C3=NC2/4.0,
     .          C4=-NC2/4.0,  C5=NC2**2/(4.0*NC),C6=-NC2/(4.0*NC))
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2),RN
      DIMENSION P(0:3,1:6),Z12TAB(8,12),Z14TAB(8,12),
     .          COLMAT(12,12),ITAB(8,4),ZS(12,8),ZD(12,8)
      SAVE COLMAT,ITAB
      DATA COLMAT/C1,C2,C3,C4,C2,C3,C5,C6,C5,C6,C5,C6,
     .            C2,C1,C2,C2,C3,C2,C6,C5,C5,C5,C5,C6,
     .            C3,C2,C1,C3,C2,C4,C5,C5,C5,C6,C6,C6,
     .            C4,C2,C3,C1,C2,C3,C6,C5,C6,C5,C6,C5,
     .            C2,C3,C2,C2,C1,C2,C5,C5,C6,C6,C5,C5,
     .            C3,C2,C4,C3,C2,C1,C6,C6,C6,C5,C5,C5,
     .            C5,C6,C5,C6,C5,C6,C1,C2,C3,C4,C2,C3,
     .            C6,C5,C5,C5,C5,C6,C2,C1,C2,C2,C3,C2,
     .            C5,C5,C5,C6,C6,C6,C3,C2,C1,C3,C2,C4,
     .            C6,C5,C6,C5,C6,C5,C4,C2,C3,C1,C2,C3,
     .            C5,C5,C6,C6,C5,C5,C2,C3,C2,C2,C1,C2,
     .            C6,C6,C6,C5,C5,C5,C3,C2,C4,C3,C2,C1/
      DATA ITAB/1,2,3,4,5,6,7,8,
     .          1,2,3,4,8,7,6,5,
     .          1,3,2,4,5,7,6,8,
     .          1,3,2,4,8,6,7,5/
*
* Transfrom Vectors, normalize vectors according to energies.
*
      CALL MAPEVE(6,PLAB,P,SCALE)
*
* Shuffle momenta such that first is IQ, second is IQB ...
*
      CALL RESHQS(P,IQ,IQB,IQP,IQBP,4)
*
* Odd # of quarks with E<0?
*
      NEGATE=1
      DO 10 I=1,4
        IF (P(0,I).LT.0.0) NEGATE=-NEGATE
   10 CONTINUE
*
* Conversion factor is equal to 1.0 for the four quark process.
*
      FACTOR=1.0/SCALE**4
      CALL SETGSP(P,6,0)
      CALL ZZFILL(6)
      CALL PRFILL(P,6)
*
* MC over helicities?
*
      IF (IHRN.EQ.1) THEN
        IHELR=1+INT(8.0*RN(1))
      ELSE
        IHELR=0
      END IF
*
      CALL D20(3,4,1,2,5,6,ITAB(1,3),Z12TAB(1,1),-1.0/NC,0,IHELR)
      CALL D11(1,2,3,4,5,6,ITAB(1,1),Z12TAB(1,2),-1.0/NC,0,IHELR)
      CALL D20(1,2,3,4,5,6,ITAB(1,1),Z12TAB(1,3),-1.0/NC,0,IHELR)
*
      CALL D20(3,4,1,2,6,5,ITAB(1,4),Z12TAB(1,4),-1.0/NC,NEGATE,IHELR)
      CALL D11(1,2,3,4,6,5,ITAB(1,2),Z12TAB(1,5),-1.0/NC,NEGATE,IHELR)
      CALL D20(1,2,3,4,6,5,ITAB(1,2),Z12TAB(1,6),-1.0/NC,NEGATE,IHELR)
*
      CALL C20(3,4,1,2,5,6,ITAB(1,3),Z12TAB(1,7),1.0,0,IHELR)
      CALL C11(1,2,3,4,5,6,ITAB(1,1),Z12TAB(1,8),1.0,0,IHELR)
      CALL C20(1,2,3,4,5,6,ITAB(1,1),Z12TAB(1,9),1.0,0,IHELR)
*
      CALL C20(3,4,1,2,6,5,ITAB(1,4),Z12TAB(1,10),1.0,NEGATE,IHELR)
      CALL C11(1,2,3,4,6,5,ITAB(1,2),Z12TAB(1,11),1.0,NEGATE,IHELR)
      CALL C20(1,2,3,4,6,5,ITAB(1,2),Z12TAB(1,12),1.0,NEGATE,IHELR)
*
* Same calls with quarks interchanged.
*
      CALL D20(3,2,1,4,5,6,ITAB(1,3),Z14TAB(1,7),-1.0/NC,0,IHELR)
      CALL D11(1,4,3,2,5,6,ITAB(1,1),Z14TAB(1,8),-1.0/NC,0,IHELR)
      CALL D20(1,4,3,2,5,6,ITAB(1,1),Z14TAB(1,9),-1.0/NC,0,IHELR)
*
      CALL D20(3,2,1,4,6,5,ITAB(1,4),Z14TAB(1,10),-1.0/NC,NEGATE,IHELR)
      CALL D11(1,4,3,2,6,5,ITAB(1,2),Z14TAB(1,11),-1.0/NC,NEGATE,IHELR)
      CALL D20(1,4,3,2,6,5,ITAB(1,2),Z14TAB(1,12),-1.0/NC,NEGATE,IHELR)
*
      CALL C20(3,2,1,4,5,6,ITAB(1,3),Z14TAB(1,1),1.0,0,IHELR)
      CALL C11(1,4,3,2,5,6,ITAB(1,1),Z14TAB(1,2),1.0,0,IHELR)
      CALL C20(1,4,3,2,5,6,ITAB(1,1),Z14TAB(1,3),1.0,0,IHELR)
*
      CALL C20(3,2,1,4,6,5,ITAB(1,4),Z14TAB(1,4),1.0,NEGATE,IHELR)
      CALL C11(1,4,3,2,6,5,ITAB(1,2),Z14TAB(1,5),1.0,NEGATE,IHELR)
      CALL C20(1,4,3,2,6,5,ITAB(1,2),Z14TAB(1,6),1.0,NEGATE,IHELR)
*
* simplify data structure before squaring.
      DO 30 I=1,12
        ZD(I,1)=Z12TAB(1,I)
        ZD(I,2)=Z12TAB(2,I)
        ZD(I,3)=Z12TAB(3,I)
        ZD(I,4)=Z12TAB(4,I)
        ZD(I,5)=Z12TAB(5,I)
        ZD(I,6)=Z12TAB(6,I)
        ZD(I,7)=Z12TAB(7,I)
        ZD(I,8)=Z12TAB(8,I)
        ZS(I,1)=Z12TAB(1,I)-Z14TAB(1,I)
        ZS(I,2)=Z14TAB(2,I)
        ZS(I,3)=Z14TAB(3,I)
        ZS(I,4)=Z12TAB(4,I)-Z14TAB(4,I)
        ZS(I,5)=Z12TAB(5,I)-Z14TAB(5,I)
        ZS(I,6)=Z14TAB(6,I)
        ZS(I,7)=Z14TAB(7,I)
        ZS(I,8)=Z12TAB(8,I)-Z14TAB(8,I)
   30 CONTINUE
      IF (IHRN.EQ.1) THEN
        OUT(1)=SQUARE(COLMAT,12,ZD(1,IHELR),12)
        OUT(2)=SQUARE(COLMAT,12,ZS(1,IHELR),12)
        IF ((IHELR.EQ.2).OR.(IHELR.EQ.3).OR.
     .      (IHELR.EQ.6).OR.(IHELR.EQ.7)) THEN
          OUT(2)=6.0*(OUT(2)+OUT(1))
        ELSE
          OUT(2)=12.0*OUT(2)
        END IF
        OUT(1)=8.0*OUT(1)
      ELSE
        OUT(1)=SQUARE(COLMAT,12,ZD(1,2),12)
     .        +SQUARE(COLMAT,12,ZD(1,3),12)
     .        +SQUARE(COLMAT,12,ZD(1,6),12)
     .        +SQUARE(COLMAT,12,ZD(1,7),12)
        OUT(2)=OUT(1)
     .        +SQUARE(COLMAT,12,ZS(1,1),12)
     .        +SQUARE(COLMAT,12,ZS(1,2),12)
     .        +SQUARE(COLMAT,12,ZS(1,3),12)
     .        +SQUARE(COLMAT,12,ZS(1,4),12)
     .        +SQUARE(COLMAT,12,ZS(1,5),12)
     .        +SQUARE(COLMAT,12,ZS(1,6),12)
     .        +SQUARE(COLMAT,12,ZS(1,7),12)
     .        +SQUARE(COLMAT,12,ZS(1,8),12)
        OUT(1)=OUT(1)
     .        +SQUARE(COLMAT,12,ZD(1,1),12)
     .        +SQUARE(COLMAT,12,ZD(1,4),12)
     .        +SQUARE(COLMAT,12,ZD(1,5),12)
     .        +SQUARE(COLMAT,12,ZD(1,8),12)
      END IF
*
      OUT(1)=2.0*OUT(1)*FACTOR
      OUT(2)=2.0*OUT(2)*FACTOR
*
      END
*
************************************************************************
* SUBROUTINE C20 returns 8 helicity combinations in ZTAB with indexing *
* given by ITAB. Colour structure (a1 a2)_c1c4 ()_c3c2                 *
* Q1,Q2,Q3,Q4,G1,G2  :  1 = + - + - ; + +     2 = + - - + ; + +        *
*                       3 = - + + - ; + +     4 = - + - + ; + +        *
*                       5 = + - + - ; + -     6 = + - - + ; + -        *
*                       7 = - + + - ; + -     8 = - + - + ; + -        *
* ICC determines whether to complex conjugate the last four amplitudes *
* because the call is with G2,G1 instead of G1,G2. If ICC<>0 it        *
* is +1 or -1 depending on the number of quarks with E<0.              *
************************************************************************
      SUBROUTINE C20(IQ1,IQ2,IQ3,IQ4,IG1,IG2,ITAB,ZTAB,FACT,ICC,IHELR)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ITAB(*),ZTAB(*)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      SAVE /ZFUNCT/,/DOTPR/,/PROPS/
*
      IH1=ITAB(1)
      IH2=ITAB(2)
      IH3=ITAB(3)
      IH4=ITAB(4)
      IH5=ITAB(5)
      IH6=ITAB(6)
      IH7=ITAB(7)
      IH8=ITAB(8)
      IF (IHELR.GT.0) THEN
        IF (IH5.NE.IHELR) IH5=0
        IF (IH6.NE.IHELR) IH6=0
        IF (IH7.NE.IHELR) IH7=0
        IF (IH8.NE.IHELR) IH8=0
      END IF
      PRAL1=PRO23(IQ1,IG1,IG2)*PRO2(IQ4,IQ3)
      PRAL2=PRO2(IQ2,IQ1)*PRO23(IG1,IG2,IQ4)
      PRAL3=PRO23(IQ2,IQ1,IG1)*PRO2(IG2,IQ4)*PRO2(IQ4,IQ3)
      PRAL4=PRO2(IQ2,IQ1)*PRO2(IQ1,IG1)*PRO2(IG1,IG2)*
     .      PRO2(IG2,IQ4)*PRO2(IQ4,IQ3)
* + + helicity
      ZFAC4=(0.0,-2.0)*FACT*ZD(IQ1,IG1)*ZD(IG1,IG2)*ZD(IG2,IQ4)
     .                     *ZUD(IQ1,IQ4)/PRAL4
      ZTAB(IH1)=ZFAC4*ZUD(IQ2,IQ4)**2*ZD(IQ1,IQ2)*ZD(IQ3,IQ4)
      ZTAB(IH2)=ZFAC4*ZUD(IQ2,IQ3)**2*ZD(IQ1,IQ2)*ZD(IQ4,IQ3)
      ZTAB(IH3)=ZFAC4*ZUD(IQ1,IQ4)**2*ZD(IQ2,IQ1)*ZD(IQ3,IQ4)
      ZTAB(IH4)=ZFAC4*ZUD(IQ1,IQ3)**2*ZD(IQ2,IQ1)*ZD(IQ4,IQ3)
* + - helicity
      ZFAC1=(0.0,2.0)*FACT/PRAL1
      ZFAC2=(0.0,2.0)*FACT/PRAL2
      ZFAC3=(0.0,2.0)*FACT/PRAL3
      ZFAC4=(0.0,2.0)*FACT/PRAL4
      IF (IH5.NE.0) THEN
        ZTAB(IH5)=ZFAC1*ZUD(IG2,IQ1)*ZUD(IQ2,IQ4)*ZD(IQ1,IG1)**2
     .                 *ZZ(IQ3,IQ1,IG1,IG2)
     .           +ZFAC2*ZUD(IQ4,IG2)**2*ZD(IQ3,IQ1)*ZD(IG1,IQ4)
     .                 *ZZ(IG1,IQ4,IG2,IQ2)
     .           +ZFAC3*ZUD(IQ4,IG2)**2*ZD(IQ1,IG1)**2
     .                 *ZZ(IQ3,IQ2,IG1,IQ1)*ZZ(IQ4,IQ3,IG2,IQ2)
     .           +ZFAC4*ZUD(IQ4,IG2)**2*ZUD(IG2,IQ1)*ZD(IQ1,IG1)
     .                 *ZD(IG2,IG1)*ZD(IQ3,IQ1)*ZZ(IQ4,IQ3,IG2,IQ2)
     .           +ZFAC4*ZUD(IQ4,IG2)*ZUD(IG1,IG2)*ZUD(IQ2,IQ4)
     .                 *ZD(IQ1,IG1)**2*ZD(IG1,IQ4)*ZZ(IQ3,IQ2,IG1,IQ1)
     .           -ZFAC4*PRO3(IQ2,IQ1,IG1)*ZUD(IQ4,IG2)*ZUD(IG2,IQ1)
     .                 *ZUD(IQ2,IQ4)*ZD(IQ1,IG1)*ZD(IQ3,IQ1)*ZD(IG1,IQ4)
        IF (ICC.NE.0) ZTAB(IH5)=-CONJG(ZTAB(IH5))*ICC
      END IF
      IF (IH6.NE.0) THEN
        ZTAB(IH6)=ZFAC1*ZUD(IQ3,IQ2)*ZUD(IQ1,IG2)*ZD(IQ1,IG1)**2
     .                 *ZZ(IQ4,IQ1,IG1,IG2)
     .           +ZFAC2*ZUD(IQ2,IQ3)*ZUD(IQ4,IG2)*ZD(IG1,IQ4)**2
     .                 *ZZ(IQ1,IQ4,IG1,IG2)
     .           +ZFAC3*ZUD(IG2,IQ3)*ZUD(IG2,IQ4)*ZD(IQ1,IG1)**2
     .                 *ZZ(IQ4,IQ2,IG1,IQ1)*ZZ(IQ4,IQ3,IG2,IQ2)
     .           +ZFAC4*ZUD(IG2,IQ3)*ZUD(IG2,IQ4)*ZUD(IG2,IQ1)
     .                 *ZD(IG2,IG1)*ZD(IQ1,IG1)*ZD(IQ4,IQ1)
     .                 *ZZ(IQ4,IQ3,IG2,IQ2)
     .           +ZFAC4*ZUD(IG2,IQ4)*ZUD(IQ2,IQ3)*ZUD(IG2,IG1)
     .                 *ZD(IQ1,IG1)**2*ZD(IG1,IQ4)*ZZ(IQ4,IQ2,IG1,IQ1)
     .           +ZFAC4*PRO3(IQ2,IQ1,IG1)*ZUD(IG2,IQ1)*ZUD(IG2,IQ4)
     .                 *ZUD(IQ2,IQ3)*ZD(IQ1,IG1)*ZD(IQ4,IQ1)*ZD(IG1,IQ4)
        IF (ICC.NE.0) ZTAB(IH6)=-CONJG(ZTAB(IH6))*ICC
      END IF
      IF (IH7.NE.0) THEN
        ZTAB(IH7)=ZFAC1*ZUD(IG2,IQ1)**2*ZD(IQ1,IG1)*ZD(IQ3,IQ2)
     .                 *ZZ(IG1,IQ1,IG2,IQ4)
     .           +ZFAC2*ZUD(IQ4,IG2)**2*ZD(IQ2,IQ3)*ZD(IQ4,IG1)
     .                 *ZZ(IG1,IQ4,IG2,IQ1)
     .           +ZFAC3*ZUD(IQ4,IG2)**2*ZD(IG1,IQ1)*ZD(IG1,IQ2)
     .                 *ZZ(IQ4,IQ3,IG2,IQ1)*ZZ(IQ3,IQ2,IG1,IQ1)
     .           +ZFAC4*ZUD(IG2,IQ1)*ZUD(IQ4,IG2)**2*ZD(IG1,IG2)
     .                 *ZD(IQ3,IQ2)*ZD(IG1,IQ1)*ZZ(IQ4,IQ3,IG2,IQ1)
     .           +ZFAC4*ZUD(IQ1,IQ4)*ZUD(IQ4,IG2)*ZUD(IG1,IG2)
     .                 *ZD(IG1,IQ1)*ZD(IG1,IQ4)*ZD(IG1,IQ2)
     .                 *ZZ(IQ3,IQ2,IG1,IQ1)
     .           +ZFAC4*PRO3(IQ2,IQ1,IG1)*ZUD(IQ4,IG2)*ZUD(IQ1,IQ4)
     .                 *ZUD(IG2,IQ1)*ZD(IG1,IQ1)*ZD(IG1,IQ4)*ZD(IQ3,IQ2)
        IF (ICC.NE.0) ZTAB(IH7)=-CONJG(ZTAB(IH7))*ICC
      END IF
      IF (IH8.NE.0) THEN
        ZTAB(IH8)=ZFAC1*ZUD(IG2,IQ1)**2*ZD(IQ4,IQ2)*ZD(IQ1,IG1)
     .                 *ZZ(IG1,IQ1,IG2,IQ3)
     .           +ZFAC2*ZUD(IQ1,IQ3)*ZUD(IQ4,IG2)*ZD(IQ4,IG1)**2
     .                 *ZZ(IQ2,IQ4,IG1,IG2)
     .           +ZFAC3*ZUD(IQ4,IG2)*ZUD(IQ3,IG2)*ZD(IQ2,IG1)
     .                 *ZD(IQ1,IG1)*ZZ(IQ4,IQ3,IG2,IQ1)
     .                 *ZZ(IQ4,IQ2,IG1,IQ1)
     .           +ZFAC4*ZUD(IQ4,IG2)*ZUD(IQ3,IG2)*ZUD(IQ1,IG2)
     .                 *ZD(IG1,IG2)*ZD(IQ4,IQ2)*ZD(IQ1,IG1)
     .                 *ZZ(IQ4,IQ3,IG2,IQ1)
     .           +ZFAC4*ZUD(IQ4,IG2)*ZUD(IQ3,IQ1)*ZUD(IG1,IG2)
     .                 *ZD(IQ1,IG1)*ZD(IQ4,IG1)*ZD(IQ2,IG1)
     .                 *ZZ(IQ4,IQ2,IG1,IQ1)
     .           +ZFAC4*PRO3(IQ2,IQ1,IG1)*ZUD(IQ1,IG2)*ZUD(IQ3,IQ1)
     .                *ZUD(IQ4,IG2)*ZD(IQ1,IG1)*ZD(IQ4,IG1)*ZD(IQ4,IQ2)
        IF (ICC.NE.0) ZTAB(IH8)=-CONJG(ZTAB(IH8))*ICC
      END IF
*
      END
*
************************************************************************
* SUBROUTINE C11 returns 8 helicity combinations in ZTAB with indexing *
* given by ITAB. Colour structure (a1)_c1c4 (a2)_c3c2                  *
* Q1,Q2,Q3,Q4,G1,G2  :  1 = + - + - ; + +     2 = + - - + ; + +        *
*                       3 = - + + - ; + +     4 = - + - + ; + +        *
*                       5 = + - + - ; + -     6 = + - - + ; + -        *
*                       7 = - + + - ; + -     8 = - + - + ; + -        *
* ICC determines whether to complex conjugate the last four amplitudes *
* because the call is with G2,G1 instead of G1,G2. If ICC<>0 it        *
* is +1 or -1 depending on the number of quarks with E<0.              *
************************************************************************
      SUBROUTINE C11(IQ1,IQ2,IQ3,IQ4,IG1,IG2,ITAB,ZTAB,FACT,ICC,IHELR)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ITAB(*),ZTAB(*)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      SAVE /ZFUNCT/,/DOTPR/,/PROPS/
*
      IH1=ITAB(1)
      IH2=ITAB(2)
      IH3=ITAB(3)
      IH4=ITAB(4)
      IH5=ITAB(5)
      IH6=ITAB(6)
      IH7=ITAB(7)
      IH8=ITAB(8)
      IF (IHELR.GT.0) THEN
        IF (IH5.NE.IHELR) IH5=0
        IF (IH6.NE.IHELR) IH6=0
        IF (IH7.NE.IHELR) IH7=0
        IF (IH8.NE.IHELR) IH8=0
      END IF
*
      PRAL1=PRO23(IQ2,IQ1,IG1)*PRO2(IQ4,IQ3)*PRO2(IQ3,IG2)
      PRAL2=PRO2(IQ2,IQ1)*PRO23(IG1,IQ4,IQ3)*PRO2(IG2,IQ2)
      PRAL3=PRO2(IQ2,IQ1)*PRO2(IQ1,IG1)*PRO2(IG1,IQ4)*
     .      PRO2(IQ4,IQ3)*PRO2(IQ3,IG2)*PRO2(IG2,IQ2)
* + + helicity
      ZFAC3=(0.0,-2.0)*FACT*ZD(IG1,IQ1)*ZUD(IQ1,IQ4)*ZD(IQ4,IG1)
     .               *ZD(IG2,IQ3)*ZUD(IQ3,IQ2)*ZD(IQ2,IG2)/PRAL3
      ZTAB(IH1)=ZFAC3*ZUD(IQ2,IQ4)**2*ZD(IQ1,IQ2)*ZD(IQ3,IQ4)
      ZTAB(IH2)=ZFAC3*ZUD(IQ2,IQ3)**2*ZD(IQ1,IQ2)*ZD(IQ4,IQ3)
      ZTAB(IH3)=ZFAC3*ZUD(IQ1,IQ4)**2*ZD(IQ2,IQ1)*ZD(IQ3,IQ4)
      ZTAB(IH4)=ZFAC3*ZUD(IQ1,IQ3)**2*ZD(IQ2,IQ1)*ZD(IQ4,IQ3)
* + - helicity
      ZFAC1=(0.0,2.0)*FACT/PRAL1
      ZFAC2=(0.0,2.0)*FACT/PRAL2
      ZFAC3=(0.0,2.0)*FACT*ZD(IG1,IQ1)*ZUD(IQ1,IQ4)*ZD(IQ4,IG1)
     .               *ZUD(IG2,IQ3)*ZD(IQ3,IQ2)*ZUD(IQ2,IG2)/PRAL3
      IF (IH5.NE.0) THEN
        ZTAB(IH5)=ZFAC1*ZUD(IQ4,IG2)*ZUD(IQ3,IG2)*ZD(IG1,IQ1)**2
     .                 *ZZ(IQ3,IQ2,IG1,IQ1)*ZZ(IQ3,IQ1,IG1,IQ2)
     .           +ZFAC2*ZUD(IQ2,IG2)**2*ZD(IG1,IQ3)*ZD(IG1,IQ4)
     .                 *ZZ(IQ2,IQ1,IG2,IQ4)*ZZ(IQ1,IQ2,IG2,IQ4)
     .           +ZFAC3*ZZ(IQ3,IQ1,IG1,IQ2)*ZZ(IQ1,IQ3,IG1,IQ4)
        IF (ICC.NE.0) ZTAB(IH5)=-CONJG(ZTAB(IH5))*ICC
      END IF
      IF (IH6.NE.0) THEN
        ZTAB(IH6)=ZFAC1*ZUD(IG2,IQ3)**2*ZD(IQ1,IG1)**2
     .                 *ZZ(IQ3,IQ2,IG1,IQ1)*ZZ(IQ4,IQ1,IG1,IQ2)
     .           +ZFAC2*ZUD(IQ2,IG2)**2*ZD(IG1,IQ4)**2
     .                 *ZZ(IQ1,IQ2,IG2,IQ3)*ZZ(IQ2,IQ1,IG2,IQ4)
     .           +ZFAC3*ZZ(IQ1,IQ4,IG1,IQ3)*ZZ(IQ4,IQ1,IG1,IQ2)
        IF (ICC.NE.0) ZTAB(IH6)=-CONJG(ZTAB(IH6))*ICC
      END IF
      IF (IH7.NE.0) THEN
        ZTAB(IH7)=ZFAC1*ZUD(IQ4,IG2)*ZUD(IG2,IQ3)*ZD(IG1,IQ2)
     .                 *ZD(IQ1,IG1)*ZZ(IQ3,IQ2,IG1,IQ1)**2
     .           +ZFAC2*ZUD(IG2,IQ1)*ZUD(IG2,IQ2)*ZD(IG1,IQ4)
     .                 *ZD(IG1,IQ3)*ZZ(IQ2,IQ1,IG2,IQ4)**2
     .           +ZFAC3*ZZ(IQ3,IQ2,IG1,IQ1)*ZZ(IQ2,IQ3,IG1,IQ4)
        IF (ICC.NE.0) ZTAB(IH7)=-CONJG(ZTAB(IH7))*ICC
      END IF
      IF (IH8.NE.0) THEN
        ZTAB(IH8)=ZFAC1*ZUD(IQ3,IG2)**2*ZD(IG1,IQ1)*ZD(IG1,IQ2)
     .                 *ZZ(IQ3,IQ2,IG1,IQ1)*ZZ(IQ4,IQ2,IG1,IQ1)
     .           +ZFAC2*ZUD(IG2,IQ2)*ZUD(IG2,IQ1)*ZD(IG1,IQ4)**2
     .                 *ZZ(IQ2,IQ1,IG2,IQ3)*ZZ(IQ2,IQ1,IG2,IQ4)
     .           +ZFAC3*ZZ(IQ4,IQ3,IG2,IQ1)*ZZ(IQ2,IQ1,IG2,IQ3)
        IF (ICC.NE.0) ZTAB(IH8)=-CONJG(ZTAB(IH8))*ICC
      END IF
*
      END
*
************************************************************************
* SUBROUTINE D20 returns 8 helicity combinations in ZTAB with indexing *
* given by ITAB. Colour structure (a1 a2)_c1c2 (a2)_c3c4               *
* Q1,Q2,Q3,Q4,G1,G2  :  1 = + - + - ; + +     2 = + - - + ; + +        *
*                       3 = - + + - ; + +     4 = - + - + ; + +        *
*                       5 = + - + - ; + -     6 = + - - + ; + -        *
*                       7 = - + + - ; + -     8 = - + - + ; + -        *
* ICC determines whether to complex conjugate the last four amplitudes *
* because the call is with G2,G1 instead of G1,G2. If ICC<>0 it        *
* is +1 or -1 depending on the number of quarks with E<0.              *
************************************************************************
      SUBROUTINE D20(IQ1,IQ2,IQ3,IQ4,IG1,IG2,ITAB,ZTAB,FACT,ICC,IHELR)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ITAB(*),ZTAB(*)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      SAVE /ZFUNCT/,/DOTPR/,/PROPS/
*
      IH1=ITAB(1)
      IH2=ITAB(2)
      IH3=ITAB(3)
      IH4=ITAB(4)
      IH5=ITAB(5)
      IH6=ITAB(6)
      IH7=ITAB(7)
      IH8=ITAB(8)
      IF (IHELR.GT.0) THEN
        IF (IH5.NE.IHELR) IH5=0
        IF (IH6.NE.IHELR) IH6=0
        IF (IH7.NE.IHELR) IH7=0
        IF (IH8.NE.IHELR) IH8=0
      END IF
* + + helicity
      ZFAC=(0.0,-2.0)*FACT/ZUD(IQ1,IG1)/ZUD(IG1,IG2)
     .                    /ZUD(IG2,IQ2)/ZUD(IQ3,IQ4)
      ZTAB(IH1)=ZFAC*ZUD(IQ2,IQ4)**2
      ZTAB(IH2)=-ZFAC*ZUD(IQ2,IQ3)**2
      ZTAB(IH3)=-ZFAC*ZUD(IQ1,IQ4)**2
      ZTAB(IH4)=ZFAC*ZUD(IQ1,IQ3)**2
* + - helicity
      PRAL1=PRO23(IQ2,IG2,IG1)*PRO2(IQ4,IQ3)
      PRAL2=PRO23(IG2,IG1,IQ1)*PRO2(IQ4,IQ3)
      PRAL3=PRO2(IQ2,IG2)*PRO2(IG2,IG1)*PRO2(IG1,IQ1)*PRO2(IQ4,IQ3)
      ZFAC1=(0.0,2.0)*FACT*ZUD(IQ2,IG2)/PRAL1
      ZFAC2=(0.0,2.0)*FACT*ZD(IQ1,IG1)/PRAL2
      ZFAC3=(0.0,2.0)*FACT*ZUD(IQ2,IG2)*ZD(IQ1,IG1)/PRAL3
      IF (IH5.NE.0) THEN
        ZTAB(IH5)=ZFAC1*ZUD(IQ2,IG2)*ZD(IG1,IQ2)*ZD(IQ1,IQ3)
     .                 *ZZ(IG1,IG2,IQ2,IQ4)
     .           +ZFAC2*ZUD(IQ1,IG2)*ZUD(IQ2,IQ4)*ZD(IG1,IQ1)
     .                 *ZZ(IQ3,IQ1,IG1,IG2)
     .           +ZFAC3*ZZ(IQ3,IQ1,IG1,IG2)*ZZ(IG1,IQ2,IG2,IQ4)
        IF (ICC.NE.0) ZTAB(IH5)=-CONJG(ZTAB(IH5))*ICC
      END IF
      IF (IH6.NE.0) THEN
        ZTAB(IH6)=ZFAC1*ZUD(IQ2,IG2)*ZD(IG1,IQ2)*ZD(IQ1,IQ4)
     .                 *ZZ(IG1,IG2,IQ2,IQ3)
     .           +ZFAC2*ZUD(IQ1,IG2)*ZUD(IQ2,IQ3)*ZD(IG1,IQ1)
     .                 *ZZ(IQ4,IQ1,IG1,IG2)
     .           +ZFAC3*ZZ(IQ4,IQ1,IG1,IG2)*ZZ(IG1,IQ2,IG2,IQ3)
        IF (ICC.NE.0) ZTAB(IH6)=-CONJG(ZTAB(IH6))*ICC
      END IF
      IF (IH7.NE.0) THEN
        ZTAB(IH7)=ZFAC1*ZUD(IQ4,IQ1)*ZD(IG1,IQ2)**2
     .                 *ZZ(IQ3,IG1,IQ2,IG2)
     .           +ZFAC2*ZUD(IQ1,IG2)**2*ZD(IQ3,IQ2)
     .                 *ZZ(IG1,IQ1,IG2,IQ4)
     .           +ZFAC3*ZUD(IQ1,IQ4)*ZUD(IQ1,IG2)
     .                 *ZD(IQ2,IQ3)*ZD(IQ2,IG1)
        IF (ICC.NE.0) ZTAB(IH7)=-CONJG(ZTAB(IH7))*ICC
      END IF
      IF (IH8.NE.0) THEN
        ZTAB(IH8)=ZFAC1*ZUD(IQ3,IQ1)*ZD(IG1,IQ2)**2
     .                 *ZZ(IQ4,IG1,IQ2,IG2)
     .           +ZFAC2*ZUD(IQ1,IG2)**2*ZD(IQ4,IQ2)
     .                 *ZZ(IG1,IQ1,IG2,IQ3)
     .           +ZFAC3*ZUD(IQ1,IQ3)*ZUD(IQ1,IG2)
     .                 *ZD(IQ2,IQ4)*ZD(IQ2,IG1)
        IF (ICC.NE.0) ZTAB(IH8)=-CONJG(ZTAB(IH8))*ICC
      END IF
*
      END
*
************************************************************************
* SUBROUTINE D11 returns 8 helicity combinations in ZTAB with indexing *
* given by ITAB. Colour structure (a1)_c1c2 (a2)_c3c4                  *
* Q1,Q2,Q3,Q4,G1,G2  :  1 = + - + - ; + +     2 = + - - + ; + +        *
*                       3 = - + + - ; + +     4 = - + - + ; + +        *
*                       5 = + - + - ; + -     6 = + - - + ; + -        *
*                       7 = - + + - ; + -     8 = - + - + ; + -        *
* ICC determines whether to complex conjugate the last four amplitudes *
* because the call is with G2,G1 instead of G1,G2. If ICC<>0 it        *
* is +1 or -1 depending on the number of quarks with E<0.              *
************************************************************************
      SUBROUTINE D11(IQ1,IQ2,IQ3,IQ4,IG1,IG2,ITAB,ZTAB,FACT,ICC,IHELR)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ITAB(*),ZTAB(*)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      SAVE /ZFUNCT/,/DOTPR/,/PROPS/
*
* + + helicity
      ZFAC=(0.0,-2.0)*FACT/ZUD(IQ1,IG1)/ZUD(IG1,IQ2)
     .                    /ZUD(IQ3,IG2)/ZUD(IG2,IQ4)
      ZTAB(ITAB(1))=ZFAC*ZUD(IQ2,IQ4)**2
      ZTAB(ITAB(2))=-ZFAC*ZUD(IQ2,IQ3)**2
      ZTAB(ITAB(3))=-ZFAC*ZUD(IQ1,IQ4)**2
      ZTAB(ITAB(4))=ZFAC*ZUD(IQ1,IQ3)**2
* + - helicity
      ZFAC=(0.0,2.0)*FACT/ZUD(IQ1,IG1)/ZUD(IG1,IQ2)
     .                   /ZD(IQ3,IG2)/ZD(IG2,IQ4)/PRO3(IQ1,IQ2,IG1)
      ZTAB(ITAB(5))=ZFAC*ZZ(IQ3,IQ1,IG1,IQ2)*ZZ(IQ3,IG2,IQ4,IQ2)
      ZTAB(ITAB(6))=-ZFAC*ZZ(IQ4,IQ1,IG1,IQ2)*ZZ(IQ4,IG2,IQ3,IQ2)
      ZTAB(ITAB(7))=-ZFAC*ZZ(IQ3,IQ2,IG1,IQ1)*ZZ(IQ3,IG2,IQ4,IQ1)
      ZTAB(ITAB(8))=ZFAC*ZZ(IQ4,IQ2,IG1,IQ1)*ZZ(IQ4,IG2,IQ3,IQ1)
*
      IF (ICC.NE.0) THEN
        ZTAB(ITAB(5))=-CONJG(ZTAB(ITAB(5)))*ICC
        ZTAB(ITAB(6))=-CONJG(ZTAB(ITAB(6)))*ICC
        ZTAB(ITAB(7))=-CONJG(ZTAB(ITAB(7)))*ICC
        ZTAB(ITAB(8))=-CONJG(ZTAB(ITAB(8)))*ICC
      END IF
*
      END
*
************************************************************************
* SUBROUTINE Q4MGNS(N,PLAB,IQ,IQB,RMQ,IQP,IQBP,RMQP,IHRN,OUT)          *
* determines the matrix element for 4 quark + N gluons.                *
* The quarks have masses RMQ and RMQP.                                 *
* If (RMQ<>RMQP) then the quarks are not interchanged.                 *
* Input/Output is described in the calling routine Q4S.                *
* Q4MNGS serves as general initialisation of the MQ4M routine          *
************************************************************************
      SUBROUTINE Q4MGNS(N,PLAB,IQ,IQB,RMQ,IQP,IQBP,RMQP,IHRN,OUT)
      PARAMETER(NUP2=10,NCOL=3,NMAT=48)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),OUT(2)
      DIMENSION P(0:3,NUP2),COLMAT(NMAT,NMAT)
      SAVE COLMAT,INITQ4
      DATA INITQ4 /1/
*
* General initialisation
*
      IF (INITQ4.EQ.1) THEN
        CALL Q4CMAT(COLMAT,N,NCOL)
        CALL INIMAP(N+1)
        CALL Q4TABS(N)
        INITQ4=0
      END IF
*
* Transfrom Vectors, normalize vectors.
*
      CALL MAPEVE(4+N,PLAB,P,SCALE)
*
* Shuffle momenta such that first is IQ, second is IQB ...
*
      CALL RESHQS(P,IQ,IQB,IQP,IQBP,4)
*
* Conversion factor is equal to 1.0 for the four quark process.
*
      FACTOR=1.0/SCALE**(2*N)
      RMQL=RMQ/SCALE
      RMQPL=RMQP/SCALE
*
      CALL INITSP(P(0,5),N,P(0,1),4)
*
      CALL MQ4M(N,SQUADI,SQUASA,COLMAT,NCOL,RMQL,RMQPL,IHRN)
*
      OUT(1)=SQUADI*FACTOR
      OUT(2)=SQUASA*FACTOR
*
      END
*
************************************************************************
* SUBROUTINE MQ4M(N,SQUADI,SQUASA,COLMAT,NCOL,RMQ,RMQP,IHRN)           *
* determines the matrix element for 4 quark + N gluons.                *
* The quarks are on places 1,2,3,4: gluons run from 5 to 4+N           *
* The quarks have masses RMQ and RMQP.                                 *
* If (RMQ<>RMQP) then the quarks are not interchanged.                 *
************************************************************************
      SUBROUTINE MQ4M(N,SQUADI,SQUASA,COLMAT,NCOL,RMQ,RMQP,IHRN)
      PARAMETER(NUP=5,NGUP=10,NMAT=48,IQUP=4,IFAC3=6,IFAC4=24,
     .          NUPM2=20,NUPM3=60,NUPM4=120)
      IMPLICIT COMPLEX (Z)
      IMPLICIT REAL (A-H,O-Y)
      REAL*8 RN
      COMMON /FERMPR/ ZQP0(2,2,IQUP),ZQP1(2,2,IQUP,NUP),
     .                ZQP2(2,2,IQUP,NUPM2),ZQP3(2,2,IQUP,NUPM3),
     .                ZQP4(2,2,IQUP,NUPM4),PQP1(IQUP,NUP),
     .                PQP2(IQUP,NUPM2),PQP3(IQUP,NUPM3),PQP4(IQUP,NUPM4)
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /Q4TAB/ IQ4TAB(21,193),IQ4COL(4,336),NRCOMB,NRCOQ4
      COMMON /QQBCUR/ ZQ1(2,2,2,IFAC3,0:NUP),ZQB1(2,2,2,IFAC3,0:NUP),
     .                ZQ2(2,2,2,IFAC3,0:NUP),ZQB2(2,2,2,IFAC3,0:NUP)
      COMMON /SPING/  ZKO(2,NGUP),ZKOD(2,NGUP),ZHVEC(2,2,2,NGUP)
      COMMON /SPEED/  IMASS
      DIMENSION COLMAT(NMAT,NMAT),ZE(NMAT+1,16),ZP(2,2,NUP),ZH(2,2,NUP)
      DIMENSION IHELI(NUP),ZMOMR(2,2),ZQPQBP(2,2),ZHEL(2,2,NUP)
      DIMENSION ZAMPS(IFAC4,16),ZMOM(2,2,NUP)
      SAVE /FERMPR/,/GLUOPR/,/Q4TAB/,/QQBCUR/,/SPING/,/SPEED/
*
      IOFFS=NRCOQ4/2
      SQUADI=0.
      SQUASA=0.
*
* Init speedup parameter and number of quark helicity combinations.
      IMASS=0
      IHELC=4
      IF ( (RMQ.GT.0.0) .OR. (RMQP.GT.0.0) ) THEN
        IMASS=1
        IHELC=16
      END IF
*
* Save gluon momenta in ZP structure
      CALL COPYTS(ZP,ZP1,N)
*
* Sum over half the gluon helicities. If N=0 then execute loop once.
*
      NRLOOP=2**(N-1)
      IF (N.EQ.0) NRLOOP=1
* IHRN=1 then a monte carlo over helicities.
      IF (IHRN.EQ.1) THEN
        IHELR=1+INT(NRLOOP*RN(1))
      ELSE
        IHELR=0
      END IF
*
      DO 3 IHS=1,NRLOOP
*
        IF ((IHS.NE.IHELR).AND.(IHRN.EQ.1)) GOTO 3
*
* Init helicity vectors before call to CURS
*
        IHEL=IHS-1
        DO 5 J=1,N
          IHELI(J)=MOD(IHEL,2)+1
          IHEL=IHEL/2
    5   CONTINUE
        DO 6 J=1,N
          CALL COPYTS(ZH(1,1,J),ZHVEC(1,1,IHELI(J),J),1)
    6   CONTINUE
*
* Init all quark currents.
*
        CALL GETQQB(N,ZH,ZP,RMQ,RMQP)
*
* Clear ZE
        DO 8 I=1,NRCOQ4
          DO 8 J=1,IHELC
            ZE(I,J)=(0.0,0.0)
    8   CONTINUE
*
* Q QB piece, OFF-SHELL MOMENTUM always contains QP and QBP
        CALL ADDTS(ZQP0(1,1,3),ZQP0(1,1,4),ZQPQBP)
        DO 10 I=1,NRCOMB
          CALL COPYTS(ZMOMR,ZQPQBP,1)
* Construct off-shell momentum
          DO 15 J=1,IQ4TAB(15,I)
            CALL ADDTS(ZMOMR,ZP(1,1,IQ4TAB(15+J,I)),ZMOMR)
   15     CONTINUE
* Copy gluon helicity tensors and momenta to local structures
          DO 16 J=1,IQ4TAB(9,I)
            CALL COPYTS(ZHEL(1,1,J),ZH(1,1,IQ4TAB(9+J,I)),1)
            CALL COPYTS(ZMOM(1,1,J),ZP(1,1,IQ4TAB(9+J,I)),1)
   16     CONTINUE
* Get all Hel amplitudes
          IQQB=1
          CALL GETAMP(ZQ1(1,1,1,IQ4TAB(2,I),IQ4TAB(1,I)),
     .                ZQB1(1,1,1,IQ4TAB(4,I),IQ4TAB(3,I)),
     .                ZQ2(1,1,1,IQ4TAB(6,I),IQ4TAB(5,I)),
     .                ZQB2(1,1,1,IQ4TAB(8,I),IQ4TAB(7,I)),
     .                IQ4TAB(9,I),ZHEL,ZMOM,IQQB,ZMOMR,ZAMPS)
* Add 8 different hel amps to main structure.
          DO 17 J=IQ4TAB(21,I),IQ4TAB(21,I)-1+IFAC(IQ4TAB(9,I)+1)
            JS=J-IQ4TAB(21,I)+1
            IND1=IQ4COL(1,J)
            IND2=IQ4COL(2,J)
            IND3=IQ4COL(3,J)
            IND4=IQ4COL(4,J)
            DO 18 J1=1,IHELC
              ZE(IND1,J1)=ZE(IND1,J1)+ZAMPS(JS,J1)
              ZE(IND2,J1)=ZE(IND2,J1)-ZAMPS(JS,J1)/NCOL
              ZE(IND3,J1)=ZE(IND3,J1)-ZAMPS(JS,J1)/NCOL
              ZE(IND4,J1)=ZE(IND4,J1)+ZAMPS(JS,J1)/NCOL
   18       CONTINUE
   17     CONTINUE
   10   CONTINUE
*
* Square of different flavours:
        DO 20 I=1,IHELC
          SHELP=+SQUARE(COLMAT,NMAT,ZE(1,I),NRCOQ4)
          SQUADI=SQUADI+SHELP
          IF ( (IMASS.EQ.0).AND.( (I.EQ.2).OR.(I.EQ.3) ) )
     .                         SQUASA=SQUASA+SHELP
   20   CONTINUE
*
* Q QPB piece, OFF-SHELL MOMENTUM always contains QP and QB
* If IMASS=0 then forbidden helicities (2 and 3) must be cleared.
        IF (IMASS.EQ.0) THEN
          DO 9 I=1,NRCOQ4
            ZE(I,2)=(0.0,0.0)
            ZE(I,3)=(0.0,0.0)
    9     CONTINUE
        END IF
        CALL ADDTS(ZQP0(1,1,3),ZQP0(1,1,2),ZQPQBP)
        DO 40 I=1,NRCOMB
          CALL COPYTS(ZMOMR,ZQPQBP,1)
* Construct off-shell momentum
          DO 45 J=1,IQ4TAB(15,I)
            CALL ADDTS(ZMOMR,ZP(1,1,IQ4TAB(15+J,I)),ZMOMR)
   45     CONTINUE
* Copy gluon helicity tensors and momenta to local structures
          DO 46 J=1,IQ4TAB(9,I)
            CALL COPYTS(ZHEL(1,1,J),ZH(1,1,IQ4TAB(9+J,I)),1)
            CALL COPYTS(ZMOM(1,1,J),ZP(1,1,IQ4TAB(9+J,I)),1)
   46     CONTINUE
* Get all Hel amplitudes
          IQQB=0
          CALL GETAMP(ZQ1(1,1,1,IQ4TAB(2,I),IQ4TAB(1,I)),
     .                ZQB2(1,1,1,IQ4TAB(4,I),IQ4TAB(3,I)),
     .                ZQ2(1,1,1,IQ4TAB(6,I),IQ4TAB(5,I)),
     .                ZQB1(1,1,1,IQ4TAB(8,I),IQ4TAB(7,I)),
     .                IQ4TAB(9,I),ZHEL,ZMOM,IQQB,ZMOMR,ZAMPS)
* Add 8 different hel amps to main structure.
          DO 47 J=IQ4TAB(21,I),IQ4TAB(21,I)-1+IFAC(IQ4TAB(9,I)+1)
            JS=J-IQ4TAB(21,I)+1
            IND1=IQ4COL(1,J)-IOFFS
            IND2=IQ4COL(2,J)+IOFFS
            IND3=IQ4COL(3,J)+IOFFS
            IND4=IQ4COL(4,J)
            IF (IND4.LT.NRCOQ4) IND4=IND4+IOFFS
            DO 48 J1=1,IHELC
              ZE(IND1,J1)=ZE(IND1,J1)-ZAMPS(JS,J1)
              ZE(IND2,J1)=ZE(IND2,J1)+ZAMPS(JS,J1)/NCOL
              ZE(IND3,J1)=ZE(IND3,J1)+ZAMPS(JS,J1)/NCOL
              ZE(IND4,J1)=ZE(IND4,J1)-ZAMPS(JS,J1)/NCOL
   48       CONTINUE
   47     CONTINUE
   40   CONTINUE
*
* Square of same flavours:
        DO 50 I=1,IHELC
          SQUASA=SQUASA+SQUARE(COLMAT,NMAT,ZE(1,I),NRCOQ4)
   50   CONTINUE
*
    3 CONTINUE
*
      IF (N.GT.0) THEN
        SQUADI=2.*SQUADI
        SQUASA=2.*SQUASA
      END IF
*
      IF (IHRN.EQ.1) THEN
        SQUADI=NRLOOP*SQUADI
        SQUASA=NRLOOP*SQUASA
      END IF
*
      END
*
************************************************************************
* SUBROUTINE GETQQB calculates all QQB currents.                       *
* In:  N, number of gluons,                                            *
*      ZH, helicity vectors of gluons and ZP the corresponding momenta.*
*      RMQ1 and RMQ2 masses of quarks.                                 *
* Out: All four Qx QBy currents                                        *
*                                                                      *
* Initsp must have been called. Currently N<=3, this is expressed      *
* by IFAC3=6 and MAX3=49                                               *
************************************************************************
      SUBROUTINE GETQQB(N,ZH,ZP,RMQ1,RMQ2)
      PARAMETER(NUP=5,IFAC3=6)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters
*
      DIMENSION ZH(2,2,NUP),ZP(2,2,NUP)
*
* Global variables
*
      COMMON /QQBCUR/ ZQ1(2,2,2,IFAC3,0:NUP),ZQB1(2,2,2,IFAC3,0:NUP),
     .                ZQ2(2,2,2,IFAC3,0:NUP),ZQB2(2,2,2,IFAC3,0:NUP)
      SAVE /QQBCUR/
*
* Init gluon currents with NO off-shell gluons.
*
      CALL INITPR(ZP,N)
      CALL CURS(N,1,ZH,0)
*
* Init quark spinors with last propagator included.
*
      CALL QCUR(1,RMQ1,N,1,ZQ1(1,1,1,1,0),ZQ1(1,1,1,1,1),
     .  ZQ1(1,1,1,1,2),ZQ1(1,1,1,1,3),ZQ1(1,1,1,1,4),ZQ1(1,1,1,1,5))
      CALL QBCUR(2,RMQ1,N,1,ZQB1(1,1,1,1,0),ZQB1(1,1,1,1,1),
     .  ZQB1(1,1,1,1,2),ZQB1(1,1,1,1,3),ZQB1(1,1,1,1,4),ZQB1(1,1,1,1,5))
      CALL QCUR(3,RMQ2,N,1,ZQ2(1,1,1,1,0),ZQ2(1,1,1,1,1),
     .  ZQ2(1,1,1,1,2),ZQ2(1,1,1,1,3),ZQ2(1,1,1,1,4),ZQ2(1,1,1,1,5))
      CALL QBCUR(4,RMQ2,N,1,ZQB2(1,1,1,1,0),ZQB2(1,1,1,1,1),
     .  ZQB2(1,1,1,1,2),ZQB2(1,1,1,1,3),ZQB2(1,1,1,1,4),ZQB2(1,1,1,1,5))
*
      END
*
**********************************************************************
* GETAMP returns all the helicity amplitudes for a given gluon       *
* helicity configuration and configuration of gluons on the QP QBP   *
* side of the diagram. N is the number of real gluons.               *
* Parameter IQQB indicates how to handle the quark helicity configs. *
**********************************************************************
      SUBROUTINE GETAMP(ZQ,ZQB,ZQP,ZQBP,N,ZHEL,ZMOM,IQQB,ZMOMR,ZAMPS)
      PARAMETER(NUP=5,IFAC4=24,NUPM2=20,NUPM3=60,NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /SPEED/  IMASS
      DIMENSION ZJO(2,2,IFAC4,2,2),ZAMPS(IFAC4,16),ZEUNIT(2,2,2,2)
      DIMENSION ZH(2,2,NUP),ZP(2,2,NUP),ZHEL(2,2,NUP),ZMOM(2,2,NUP)
      DIMENSION ZQ(2,2,2),ZQB(2,2,2),ZQP(2,2,2),ZQBP(2,2,2),ZMOMR(2,2)
      DIMENSION ZL1(2,2),ZL2(2,2),ZR1(2,2),ZR2(2,2)
      DIMENSION IHAND(16,2),IHAND0(4,2)
      SAVE /GLUCUR/,/SPEED/,IHAND,IHAND0,ZEUNIT
      DATA IHAND /1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
     .            1,5,3,7,2,6,4,8,9,13,11,15,10,14,12,16/
      DATA IHAND0/1,2,3,4,  1,3,2,4/
      DATA ZEUNIT
     .      /( 0.0,0.0), ( 0.0,0.0), ( 0.0,0.0), ( 1.0,0.0),
     .       ( 0.0,0.0), ( 0.0,0.0), (-1.0,0.0), ( 0.0,0.0),
     .       ( 0.0,0.0), (-1.0,0.0), ( 0.0,0.0), ( 0.0,0.0),
     .       ( 1.0,0.0), ( 0.0,0.0), ( 0.0,0.0), ( 0.0,0.0)/
*
* Copy helicity vectors and momenta to calling arrays.
*
      CALL COPYTS(ZH,ZHEL,N)
      CALL COPYTS(ZP,ZMOM,N)
      CALL COPYTS(ZP(1,1,N+1),ZMOMR,1)
*
      CALL INITPR(ZP,N+1)
      DO 10 ICD=1,2
        DO 10 ID=1,2
          CALL COPYTS(ZH(1,1,N+1),ZEUNIT(1,1,ICD,ID),1)
          CALL CURS(N+1,1,ZH,1)
          IF (N.EQ.0) CALL COPYTS(ZJO(1,1,1,ICD,ID),ZJ1, 1)
          IF (N.EQ.1) CALL COPYTS(ZJO(1,1,1,ICD,ID),ZJ2, 2)
          IF (N.EQ.2) CALL COPYTS(ZJO(1,1,1,ICD,ID),ZJ3, 6)
          IF (N.EQ.3) CALL COPYTS(ZJO(1,1,1,ICD,ID),ZJ4,24)
   10 CONTINUE
*
      ZPROP=-(0.0,-1.0)/(0.5*ZMUL(ZMOMR,ZMOMR))
      INDEX=1
      IF (IMASS.EQ.1) THEN
        DO 20 IHQ1=1,2
         DO 20 IHQB1=1,2
          DO 20 IHQ2=1,2
           DO 20 IHQB2=1,2
            CALL ZHELP1(ZQ(1,1,IHQ1),ZQB(1,1,IHQB1),
     .                  ZQP(1,1,IHQ2),ZQBP(1,1,IHQB2),
     .                  ZJO,ZAMPS(1,IHAND(INDEX,1+IQQB)),N,ZPROP)
            INDEX=INDEX+1
   20   CONTINUE
      ELSE
* QQB Tensor on left side (contra-variant.)
        ZL1(1,1)=ZQ(2,2,1)*ZQB(2,1,1)
        ZL1(1,2)=-ZQ(2,2,1)*ZQB(1,1,1)
        ZL1(2,1)=-ZQ(1,2,1)*ZQB(2,1,1)
        ZL1(2,2)=ZQ(1,2,1)*ZQB(1,1,1)
* QQB Tensor on left side (contra-variant.)
        ZL2(1,1)=ZQB(1,2,1)*ZQ(1,1,1)
        ZL2(1,2)=ZQB(1,2,1)*ZQ(2,1,1)
        ZL2(2,1)=ZQB(2,2,1)*ZQ(1,1,1)
        ZL2(2,2)=ZQB(2,2,1)*ZQ(2,1,1)
* QQBP Tensor on right side (contra-variant.)
        ZR1(1,1)=ZQP(2,2,1)*ZQBP(2,1,1)
        ZR1(1,2)=-ZQP(2,2,1)*ZQBP(1,1,1)
        ZR1(2,1)=-ZQP(1,2,1)*ZQBP(2,1,1)
        ZR1(2,2)=ZQP(1,2,1)*ZQBP(1,1,1)
* QQBP Tensor on right side (contra-variant.)
        ZR2(1,1)=ZQBP(1,2,1)*ZQP(1,1,1)
        ZR2(1,2)=ZQBP(1,2,1)*ZQP(2,1,1)
        ZR2(2,1)=ZQBP(2,2,1)*ZQP(1,1,1)
        ZR2(2,2)=ZQBP(2,2,1)*ZQP(2,1,1)
*
        DO 30 I=1,IFAC(N+1)
          Z1=(0.0,0.0)
          Z2=(0.0,0.0)
          Z3=(0.0,0.0)
          Z4=(0.0,0.0)
          DO 40 ICD=1,2
            DO 40 ID=1,2
              ZA=+ZL1(1,1)*ZJO(1,1,I,ICD,ID)
     .           +ZL1(1,2)*ZJO(1,2,I,ICD,ID)
     .           +ZL1(2,1)*ZJO(2,1,I,ICD,ID)
     .           +ZL1(2,2)*ZJO(2,2,I,ICD,ID)
              ZB=+ZL2(1,1)*ZJO(1,1,I,ICD,ID)
     .           +ZL2(1,2)*ZJO(1,2,I,ICD,ID)
     .           +ZL2(2,1)*ZJO(2,1,I,ICD,ID)
     .           +ZL2(2,2)*ZJO(2,2,I,ICD,ID)
              Z1=Z1+ZA*ZR1(ICD,ID)
              Z2=Z2+ZA*ZR2(ICD,ID)
              Z3=Z3+ZB*ZR1(ICD,ID)
              Z4=Z4+ZB*ZR2(ICD,ID)
   40     CONTINUE
          ZAMPS(I,IHAND0(1,IQQB+1))=Z1*ZPROP
          ZAMPS(I,IHAND0(2,IQQB+1))=Z2*ZPROP
          ZAMPS(I,IHAND0(3,IQQB+1))=Z3*ZPROP
          ZAMPS(I,IHAND0(4,IQQB+1))=Z4*ZPROP
   30   CONTINUE
      END IF
*
      END
*
**********************************************************************
* ZHELP1 contracts four quark currents with an off-shell gluon       *
* current for all (N+1)! different currents.                         *
* Input: ZQ, ZQB, ZQP and ZQBP are the quark currents for given      *
*        helicities. ZJO are the (N+1)! off-shell gluon currents.    *
*        N is number of real gluons in off-shell gluon current.      *
*        ZPROP is propagator term.                                   *
* Out:   (N+1)! amplitudes in ZAMPS.                                 *
**********************************************************************
      SUBROUTINE ZHELP1(ZQ,ZQB,ZQP,ZQBP,ZJO,ZAMPS,N,ZPROP)
      PARAMETER(IFAC4=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters
*
      DIMENSION ZQ(2,2),ZQB(2,2),ZQP(2,2),ZQBP(2,2)
      DIMENSION ZJO(2,2,IFAC4,2,2),ZAMPS(IFAC4),ZL(2,2),ZR(2,2)
*
* QQB Tensor on left side (contra-variant.)
      ZL(1,1)=+ZQ(2,2)*ZQB(2,1)-ZQ(1,1)*ZQB(1,2)
      ZL(1,2)=-ZQ(2,2)*ZQB(1,1)-ZQ(2,1)*ZQB(1,2)
      ZL(2,1)=-ZQ(1,2)*ZQB(2,1)-ZQ(1,1)*ZQB(2,2)
      ZL(2,2)=+ZQ(1,2)*ZQB(1,1)-ZQ(2,1)*ZQB(2,2)
* QQBP Tensor on right side (contra-variant.)
      ZR(1,1)=+ZQP(2,2)*ZQBP(2,1)-ZQP(1,1)*ZQBP(1,2)
      ZR(1,2)=-ZQP(2,2)*ZQBP(1,1)-ZQP(2,1)*ZQBP(1,2)
      ZR(2,1)=-ZQP(1,2)*ZQBP(2,1)-ZQP(1,1)*ZQBP(2,2)
      ZR(2,2)=+ZQP(1,2)*ZQBP(1,1)-ZQP(2,1)*ZQBP(2,2)
*
      DO 10 I=1,IFAC(N+1)
        Z=(0.0,0.0)
        DO 20 ICD=1,2
          DO 20 ID=1,2
            ZA=+ZL(1,1)*ZJO(1,1,I,ICD,ID)
     .         +ZL(2,1)*ZJO(2,1,I,ICD,ID)
     .         +ZL(1,2)*ZJO(1,2,I,ICD,ID)
     .         +ZL(2,2)*ZJO(2,2,I,ICD,ID)
            Z=Z+ZA*ZR(ICD,ID)
   20   CONTINUE
        ZAMPS(I)=Z*ZPROP
   10 CONTINUE
*
      END
*
************************************************************************
* ADDTS adds spinor tensors ZT1 and ZT2 into ZANS                      *
************************************************************************
      SUBROUTINE ADDTS(ZT1,ZT2,ZANS)
      COMPLEX ZT1(2,2),ZT2(2,2),ZANS(2,2)
*
      ZANS(1,1)=ZT1(1,1)+ZT2(1,1)
      ZANS(1,2)=ZT1(1,2)+ZT2(1,2)
      ZANS(2,1)=ZT1(2,1)+ZT2(2,1)
      ZANS(2,2)=ZT1(2,2)+ZT2(2,2)
*
      END
*
************************************************************************
* SUBROUTINE Q4TABS(N) inits data structure to evaluate q4 amplitudes. *
*                      N is the number of gluons (N<4)                 *
*   Basicly there are two structures:                                  *
*          The first IQ4TAB serves to construct                        *
*          amplitudes from the currents and it contains an index to    *
*          IQ4COL where to put the amplitudes.                         *
*   Details of IQ4TAB(*,21)                                            *
*           1,2 : howmany gluons in which Q current.                   *
*           3,4 : howmany gluons in which QB current.                  *
*           5,6 : howmany gluons in which Q' current.                  *
*           7,8 : howmany gluons in which QB' current.                 *
*           9   : howmany gluons on the propagator.                    *
*          10-14: which gluons (ordered)                               *
*          15   : howmany gluons on Q'QB' current                      *
*          16-20: which gluons (ordered)                               *
*          21   : index to IQ4COL structure.                           *
* Number of different objects as a function of N                       *
*            N=0;   NRCOMB=1;   COLCOMB=1                              *
*            N=1;   NRCOMB=5;   COLCOMB=6                              *
*            N=2;   NRCOMB=29;  COLCOMB=42                             *
*            N=3;   NRCOMB=193; COLCOMB=336                            *
* Details of IQ4COL(*,4)                                               *
*    Mapping to colour datastructure for QQB on left and Q'QB' on      *
*    right. Use trick with IOFFS to interchange of QB and QB'          *
************************************************************************
      SUBROUTINE Q4TABS(N)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
*
      COMMON /Q4TAB/ IQ4TAB(21,193),IQ4COL(4,336),NRCOMB,NRCOQ4
*
      DIMENSION ITEMP(21),IP(NUP),IPC(NUP),IPD(NUP),ICD(NUP)
      SAVE /MAPPIN/,/Q4TAB/
*
      NRCOQ4=2*IFAC(N+1)
* All possible distributions of the five sub currents.
      NRCOMB=0
      INDOUD=1
      DO 10 I=1,IFAC(N)
        CALL INVID(I,IP,N)
        I1S=0
        DO 20 I1=0,N-I1S
         ITEMP(1)=I1
         IF (I1.EQ.0) ITEMP(2)=1
         IF (I1.EQ.1) ITEMP(2)=M1H(IP(I1S+1))
         IF (I1.EQ.2) ITEMP(2)=M2H(IP(I1S+1),IP(I1S+2))
         IF (I1.EQ.3) ITEMP(2)=M3H(IP(I1S+1),IP(I1S+2),IP(I1S+3))
         I2S=I1
         DO 20 I2=0,N-I2S
          ITEMP(3)=I2
          IF (I2.EQ.0) ITEMP(4)=1
          IF (I2.EQ.1) ITEMP(4)=M1H(IP(I2S+1))
          IF (I2.EQ.2) ITEMP(4)=M2H(IP(I2S+1),IP(I2S+2))
          IF (I2.EQ.3) ITEMP(4)=M3H(IP(I2S+1),IP(I2S+2),IP(I2S+3))
          I3S=I2+I1
          DO 20 I3=0,N-I3S
           ITEMP(5)=I3
           IF (I3.EQ.0) ITEMP(6)=1
           IF (I3.EQ.1) ITEMP(6)=M1H(IP(I3S+1))
           IF (I3.EQ.2) ITEMP(6)=M2H(IP(I3S+1),IP(I3S+2))
           IF (I3.EQ.3) ITEMP(6)=M3H(IP(I3S+1),IP(I3S+2),IP(I3S+3))
           I4S=I3+I2+I1
           DO 20 I4=0,N-I4S
            ITEMP(7)=I4
            IF (I4.EQ.0) ITEMP(8)=1
            IF (I4.EQ.1) ITEMP(8)=M1H(IP(I4S+1))
            IF (I4.EQ.2) ITEMP(8)=M2H(IP(I4S+1),IP(I4S+2))
            IF (I4.EQ.3) ITEMP(8)=M3H(IP(I4S+1),IP(I4S+2),IP(I4S+3))
* zero ITEMP for comparison
            DO 19 J=9,21
              ITEMP(J)=0
   19       CONTINUE
* gluons on propagator
            ITEMP(9)=N-I1-I2-I3-I4
            IFIRST=10
            DO 21 J=I1+I2+I3+I4+1,N
              ITEMP(IFIRST)=IP(J)
              IFIRST=IFIRST+1
   21       CONTINUE
* sort propagator gluons.
            DO 22 J1=10,8+ITEMP(9)
              DO 22 J2=J1+1,9+ITEMP(9)
                IF (ITEMP(J1).GT.ITEMP(J2)) THEN
                  IHELP=ITEMP(J1)
                  ITEMP(J1)=ITEMP(J2)
                  ITEMP(J2)=IHELP
                END IF
   22       CONTINUE
* gluons on right current.
            ITEMP(15)=+I3+I4
            IFIRST=16
            DO 23 J=I3S+1,I3S+I3+I4
              ITEMP(IFIRST)=IP(J)
              IFIRST=IFIRST+1
   23       CONTINUE
* sort gluons on right current (not necessary!) .
            DO 24 J1=16,14+ITEMP(15)
              DO 24 J2=J1+1,15+ITEMP(15)
                IF (ITEMP(J1).GT.ITEMP(J2)) THEN
                  IHELP=ITEMP(J1)
                  ITEMP(J1)=ITEMP(J2)
                  ITEMP(J2)=IHELP
                END IF
   24       CONTINUE
* Is this permutation already present?
            IOK=1
            J=1
   25       IF (J.GT.NRCOMB) GOTO 27
            IDIFF=0
            DO 26 J1=1,20
              IF (IQ4TAB(J1,J).NE.ITEMP(J1)) IDIFF=1
   26       CONTINUE
            IF (IDIFF.EQ.0) IOK=0
            J=J+1
            GOTO 25
*
   27       CONTINUE
            IF (IOK.EQ.1) THEN
              NRCOMB=NRCOMB+1
* updata index pointer
              ITEMP(21)=INDOUD
              DO 28 J1=1,21
                IQ4TAB(J1,NRCOMB)=ITEMP(J1)
   28         CONTINUE
* construct colour mappings, sum over all permutations gluons on prop
              NP1=ITEMP(9)+1
              DO 31 J1=1,IFAC(NP1)
                INDEX=INDOUD+J1-1
                CALL INVID(J1,ICD,NP1)
* fill IPC and IPD
                NC=0
                NT=0
   32           NT=NT+1
                IF (ICD(NT).NE.NP1) THEN
                  NC=NC+1
                  IPC(NC)=ITEMP(9+ICD(NT))
                  GOTO 32
                END IF
                ND=0
   33           NT=NT+1
                IF (NT.LE.NP1) THEN
                  ND=ND+1
                  IPD(ND)=ITEMP(9+ICD(NT))
                  GOTO 33
                END IF
* construct (ACF)il (EDB)kj term
                IQ4COL(1,INDEX)=ICOL(IP(I1S+1),I1,IPC,NC,IP(I4S+1),I4,
     .           IP(I3S+1),I3,IPD,ND,IP(I2S+1),I2,I1+NC+I4,N)+IFAC(N+1)
* construct (ACDB)ij (EF)kl term
                IQ4COL(2,INDEX)=ICOL(IP(I1S+1),I1,IPC,NC,IPD,ND,
     .           IP(I2S+1),I2,IP(I3S+1),I3,IP(I4S+1),I4,I1+NC+ND+I2,N)
* construct (AB)ij (EDCF)kl term
                IQ4COL(3,INDEX)=ICOL(IP(I1S+1),I1,IP(I2S+1),I2,
     .            IP(I3S+1),I3,IPD,ND,IPC,NC,IP(I4S+1),I4,I1+I2,N)
* construct (AB)ij (CD) (EF)kl term
                IF ((NC+ND).EQ.0) THEN
                  IQ4COL(4,INDEX)=ICOL(IP(I1S+1),I1,IP(I2S+1),I2,IPC,NC,
     .             IPD,ND,IP(I3S+1),I3,IP(I4S+1),I4,I1+I2,N)
                ELSE
                  IQ4COL(4,INDEX)=2*IFAC(NP1)+1
                END IF
   31         CONTINUE
              INDOUD=INDOUD+IFAC(ITEMP(9)+1)
*
            END IF
   20   CONTINUE
   10 CONTINUE
*
      END
*
************************************************************************
* FUNCTION ICOL maps the general colour structure of the four quark    *
* process to the basic structures used when squaring the invariants.   *
************************************************************************
      FUNCTION ICOL(IPA,NA,IPB,NB,IPC,NC,IPD,ND,IPE,NE,IPF,NF,NBREAK,N)
      PARAMETER(NUP=5)
*
      DIMENSION IP(NUP,6),IT(6),IPERM(NUP),
     .          IPA(*),IPB(*),IPC(*),IPD(*),IPE(*),IPF(*)
*
      IT(1)=NA
      IT(2)=NB
      IT(3)=NC
      IT(4)=ND
      IT(5)=NE
      IT(6)=NF
      DO 5 I=1,NUP
        IP(I,1)=IPA(I)
        IP(I,2)=IPB(I)
        IP(I,3)=IPC(I)
        IP(I,4)=IPD(I)
        IP(I,5)=IPE(I)
        IP(I,6)=IPF(I)
    5 CONTINUE
*
      NTEL=0
      DO 10 I=1,6
        DO 20 J=1,IT(I)
          NTEL=NTEL+1
          IPERM(NTEL)=IP(J,I)
   20   CONTINUE
   10 CONTINUE
      INDEX=ID(IPERM,N)
      ICOL=(N+1)*(INDEX-1)+1+NBREAK
*
      END
*
************************************************************************
* Q4CMAT determines the colour matrix for Q4 + N gluons.               *
*                                                                      *
* Input: COLMAT with size (NUP+1)!*2  with NUP currently 3             *
* Colour structures are organised as follows:                          *
*    index=1 -- (N+1)*N! : (a_1 ... a_k)_ij  (a_k+1 ... a_n)_kl        *
*    with (N+1) places to break the permutation and                    *
*                         the first is (1)_ij (a_1 .. a_n)_kl          *
*    with N! permutations of a_1..a_n according to INVID               *
*    index=1+(N+1)*N! -- 2*(N+1)*N!  (j<-->l) interchange              *
*                                                                      *
* Restriction: N<=3 in this version.                                   *
************************************************************************
      SUBROUTINE Q4CMAT(COLMAT,N,NCOL)
      PARAMETER(NUP=3,NUP2=6,NMAT=(NUP+1)*6*2)
      IMPLICIT REAL (A-H,O-Z)
      DIMENSION COLMAT(NMAT,NMAT)
      DIMENSION IP1(NUP2),IP2(NUP2),IP3(NUP2),IP4(NUP2),
     .          IP(NUP2),IQ(NUP2)
*
* Loop over all permutations of gluons (normal)
      DO 10 IX1=1,IFAC(N)
        CALL INVID(IX1,IP,N)
* Loop over possible ways to split the gluons in two colour structures
        DO 20 IX2=0,1
          IF (IX2.EQ.0) THEN
            IP1(1)=1
            IP1(2)=2
            IP2(1)=3
            IP2(2)=4
          ELSE
            IP1(1)=1
            IP1(2)=4
            IP2(1)=3
            IP2(2)=2
          END IF
          DO 30 IX3=0,N
            DO 40 IX4=1,IX3
              IP1(2+IX4)=IP(IX4)
   40       CONTINUE
            IP1(3+IX3)=0
            DO 50 IX4=IX3+1,N
              IP2(2+IX4-IX3)=IP(IX4)
   50       CONTINUE
            IP2(3+N-IX3)=0
* index
            IX=(N+1)*(IX1-1+IFAC(N)*IX2)+1+IX3
* Loop over all permutations of gluons (complex conjugate)
            DO 60 IY1=1,IFAC(N)
              CALL INVID(IY1,IQ,N)
* Loop over possible ways to split the gluons in two colour structures
              DO 70 IY2=0,1
                IF (IY2.EQ.0) THEN
                  IP3(1)=1
                  IP3(2)=2
                  IP4(1)=3
                  IP4(2)=4
                ELSE
                  IP3(1)=1
                  IP3(2)=4
                  IP4(1)=3
                  IP4(2)=2
                END IF
                DO 80 IY3=0,N
                  DO 90 IY4=1,IY3
                    IP3(2+IY4)=IQ(IY4)
   90             CONTINUE
                  IP3(3+IY3)=0
                  DO 100 IY4=IY3+1,N
                    IP4(2+IY4-IY3)=IQ(IY4)
  100             CONTINUE
                  IP4(3+N-IY3)=0
* index
                  IY=(N+1)*(IY1-1+IFAC(N)*IY2)+1+IY3
                  COLMAT(IX,IY)=TRTRQ4(N,IP1,IP2,IP3,IP4,NCOL)
   80           CONTINUE
   70         CONTINUE
   60       CONTINUE
   30     CONTINUE
   20   CONTINUE
   10 CONTINUE
*
      END
*
************************************************************************
* TRTRQ4 calculates the colour trace products in Q QB Q QB + N G       *
*                                                                      *
* input: IP1,  first colour trace with IP1=(i,j,a1,a2,...,0)           *
*        IP2,  second colour trace with IP2=(k,l,a1,a2,...,0)          *
*        IP1 and IP2 together contain N gluons.                        *
*        IP3 and IP4 with the same convention.                         *
*        NCOL     number of colours to use.                            *
*                                                                      *
* method: eliminate fundamental T matrices with                        *
*   T(a,i,j)*T(a,k,l) = 1/2 * ( D(i,l)*D(j,k)- 1/NCOL * D(i,j)*D(k,l)) *
* the delta functions arithmetic is done in the IT-array               *
*                                                                      *
* Restriction: N<=3 in this version.                                   *
************************************************************************
      FUNCTION TRTRQ4(N,IP1,IP2,IP3,IP4,NCOL)
      PARAMETER (NUP=3,NUP2=6,NUP3=10)
      IMPLICIT REAL (A-H,O-Z)
      DIMENSION IP1(*),IP2(*),IP3(*),IP4(*)
      DIMENSION IL1(NUP2),IL2(NUP2),IL3(NUP2),IL4(NUP2)
      DIMENSION IT1(2,NUP),IT2(2,NUP),IW(2,NUP3),IDEL(2,4)
*
* Convert the four colour pieces to a product of fundamental
* matrices by straightforward testing methods.
*
      DO 10 I=1,NUP2
        IL1(I)=IP1(I)
        IL2(I)=IP2(I)
        IL3(I)=IP3(I)
        IL4(I)=IP4(I)
   10 CONTINUE
*
      INTEL=5
      INDN=1
*
* Put IL1 in the IT1 array as product of T-matrices
*
      IF (IL1(3).EQ.0) THEN
        IDEL(1,INDN)=IL1(1)
        IDEL(2,INDN)=IL1(2)
        INDN=INDN+1
      ELSE
        INDEX=3
   21   IF (IL1(INDEX+1).EQ.0) THEN
          IT1(1,IL1(INDEX))=IL1(1)
          IT1(2,IL1(INDEX))=IL1(2)
        ELSE
          IT1(1,IL1(INDEX))=IL1(1)
          IT1(2,IL1(INDEX))=INTEL
          IL1(1)=INTEL
          INTEL=INTEL+1
          INDEX=INDEX+1
          GOTO 21
        END IF
      END IF
*
* Put IL2 in the IT1 array as product of T-matrices
*
      IF (IL2(3).EQ.0) THEN
        IDEL(1,INDN)=IL2(1)
        IDEL(2,INDN)=IL2(2)
        INDN=INDN+1
      ELSE
        INDEX=3
   22   IF (IL2(INDEX+1).EQ.0) THEN
          IT1(1,IL2(INDEX))=IL2(1)
          IT1(2,IL2(INDEX))=IL2(2)
        ELSE
          IT1(1,IL2(INDEX))=IL2(1)
          IT1(2,IL2(INDEX))=INTEL
          IL2(1)=INTEL
          INTEL=INTEL+1
          INDEX=INDEX+1
          GOTO 22
        END IF
      END IF
*
* Put IL3 in the IT2 array as product of T-matrices
*
      IF (IL3(3).EQ.0) THEN
        IDEL(2,INDN)=IL3(1)
        IDEL(1,INDN)=IL3(2)
        INDN=INDN+1
      ELSE
        INDEX=3
   23   IF (IL3(INDEX+1).EQ.0) THEN
          IT2(2,IL3(INDEX))=IL3(1)
          IT2(1,IL3(INDEX))=IL3(2)
        ELSE
          IT2(2,IL3(INDEX))=IL3(1)
          IT2(1,IL3(INDEX))=INTEL
          IL3(1)=INTEL
          INTEL=INTEL+1
          INDEX=INDEX+1
          GOTO 23
        END IF
      END IF
*
* Put IL4 in the IT2 array as product of T-matrices
*
      IF (IL4(3).EQ.0) THEN
        IDEL(2,INDN)=IL4(1)
        IDEL(1,INDN)=IL4(2)
        INDN=INDN+1
      ELSE
        INDEX=3
   24   IF (IL4(INDEX+1).EQ.0) THEN
          IT2(2,IL4(INDEX))=IL4(1)
          IT2(1,IL4(INDEX))=IL4(2)
        ELSE
          IT2(2,IL4(INDEX))=IL4(1)
          IT2(1,IL4(INDEX))=INTEL
          IL4(1)=INTEL
          INTEL=INTEL+1
          INDEX=INDEX+1
          GOTO 24
        END IF
      END IF
*
* Clear final result
*
      TRTRQ4=0.0
*
      DO 50 I=0,2**N-1
        IHULP=I
        FACTOR=1.0
        NR=0
        DO 52 IJ=1,INDN-1
          IW(1,IJ+2*N)=IDEL(1,IJ)
          IW(2,IJ+2*N)=IDEL(2,IJ)
   52   CONTINUE
        DO 40 J=1,N
          IF (IHULP.GE.2**(N-J)) THEN
            IHULP=IHULP-2**(N-J)
            IW(1,J)=IT1(1,J)
            IW(2,J)=IT2(2,J)
            IW(1,J+N)=IT2(1,J)
            IW(2,J+N)=IT1(2,J)
          ELSE
            IW(1,J)=IT1(1,J)
            IW(2,J)=IT1(2,J)
            IW(1,J+N)=IT2(1,J)
            IW(2,J+N)=IT2(2,J)
            FACTOR=-FACTOR/NCOL
          END IF
   40   CONTINUE
        NR=0
        DO 45 J=1,2*N+INDN-1
          IF (IW(1,J).EQ.IW(2,J)) THEN
            NR=NR+1
          ELSE
            DO 44 K1=J+1,2*N+INDN-1
              DO 44 K2=1,2
                IF (IW(K2,K1).EQ.IW(1,J)) THEN
                  IW(K2,K1)=IW(2,J)
                  GOTO 45
                END IF
   44       CONTINUE
          END IF
   45   CONTINUE
        FACTOR=FACTOR*NCOL**NR
        TRTRQ4=TRTRQ4+FACTOR
   50 CONTINUE
*
      TRTRQ4=TRTRQ4*(0.5)**N
*
      END
*
      SUBROUTINE QCUR(IQ,RMQ,N,ILAST,Z0,Z1,Z2,Z3,Z4,Z5)
      PARAMETER(NUP=5,IQUP=4,NUPM1=5,NUPM2=20,NUPM3=60,
     .          NUPM4=120,NUPM5=120,NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters
*
      DIMENSION Z0(2,2,2),Z1(2,2,2,*),Z2(2,2,2,*),
     .          Z3(2,2,2,*),Z4(2,2,2,*),Z5(2,2,2,*)
*
* Global variables
*
      COMMON /SPEED/ IMASS
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /SPINQ/  ZQKO(2,IQUP),ZQKOD(2,IQUP),
     .                ZQMO(2,IQUP),ZQMOD(2,IQUP)
      COMMON /FERMPR/ ZQP0(2,2,IQUP),ZQP1(2,2,IQUP,NUP),
     .                ZQP2(2,2,IQUP,NUPM2),ZQP3(2,2,IQUP,NUPM3),
     .                ZQP4(2,2,IQUP,NUPM4),PQP1(IQUP,NUP),
     .                PQP2(IQUP,NUPM2),PQP3(IQUP,NUPM3),PQP4(IQUP,NUPM4)
*
* Local variables
*
      DIMENSION ZH1(2,2,2),ZH2(2,2,2),ZH3(2,2,2),ZH4(2,2,2),ZH5(2,2,2)
      DIMENSION IPERM(NUP,NUPFAC)
      SAVE INIT,IPERM,/FERMPR/,/SPINQ/,/GLUCUR/,/MAPPIN/,/SPEED/
      DATA INIT /1/
*
      IF (INIT.EQ.1) THEN
        INIT=0
        DO 5 I=1,IFAC(N)
          CALL INVID(I,IPERM(1,I),N)
    5   CONTINUE
      END IF
*
* Init the currents of length 0, UB(+) and UB(-). If IMASS=0 we only use
* the (.,.,1) index.
*
      Z0(1,2,1)=-(0.0,1.0)*ZQKOD(1,IQ)
      Z0(2,2,1)=-(0.0,1.0)*ZQKOD(2,IQ)
      Z0(1,1,1+IMASS)=-(0.0,1.0)*ZQKO(2,IQ)
      Z0(2,1,1+IMASS)=+(0.0,1.0)*ZQKO(1,IQ)
      IF (IMASS.EQ.1) THEN
        Z0(1,1,1)=+(0.0,1.0)*ZQMO(2,IQ)
        Z0(2,1,1)=-(0.0,1.0)*ZQMO(1,IQ)
        Z0(1,2,2)=-(0.0,1.0)*ZQMOD(1,IQ)
        Z0(2,2,2)=-(0.0,1.0)*ZQMOD(2,IQ)
      END IF
*
      IF (N.EQ.0) GOTO 999
*
* If N>0 then calculate all Z1-spinors.
*
      DO 10 J=1,N
        CALL QCURG(Z0(1,1,1),ZJ1(1,1,J),Z1(1,1,1,J))
        IF ((N.NE.1).OR.(ILAST.EQ.1))  CALL QCURP(Z1(1,1,1,J),
     .              ZQP1(1,1,IQ,J),RMQ,PQP1(IQ,J),Z1(1,1,1,J))
   10 CONTINUE
*
      IF (N.EQ.1) GOTO 999
*
* If N>1 then calculate all Z2-spinors.
*
      DO 20 J1=1,N-1
        DO 20 J2=J1+1,N
          INDEX=M2H(J1,J2)
          CALL QCURG(Z0(1,1,1),ZJ2(1,1,INDEX),ZH1(1,1,1))
          CALL QCURG(Z1(1,1,1,J1),ZJ1(1,1,J2),Z2(1,1,1,INDEX))
          CALL ADDZHS(Z2(1,1,1,INDEX),ZH1)
          IF ((N.NE.2).OR.(ILAST.EQ.1)) CALL QCURP(Z2(1,1,1,INDEX),
     .        ZQP2(1,1,IQ,INDEX),RMQ,PQP2(IQ,INDEX),Z2(1,1,1,INDEX))
          INDEX=M2H(J2,J1)
          CALL QCURG(Z1(1,1,1,J2),ZJ1(1,1,J1),Z2(1,1,1,INDEX))
          CALL SUBZHS(Z2(1,1,1,INDEX),ZH1)
          IF ((N.NE.2).OR.(ILAST.EQ.1)) CALL QCURP(Z2(1,1,1,INDEX),
     .        ZQP2(1,1,IQ,INDEX),RMQ,PQP2(IQ,INDEX),Z2(1,1,1,INDEX))
   20 CONTINUE
*
      IF (N.EQ.2) GOTO 999
*
* If N>2 then obtain all Z3-spinors.
*
      DO 30 J1=1,N-2
       DO 30 J2=J1+1,N-1
        DO 30 J3=J2+1,N
* 1, 2, 3
          INDEX=M3H(J1,J2,J3)
          CALL QCURG(Z0(1,1,1),ZJ3(1,1,INDEX),ZH1(1,1,1))
          CALL QCURG(Z1(1,1,1,J1),ZJ2(1,1,M2H(J2,J3)),ZH2(1,1,1))
          CALL QCURG(Z2(1,1,1,M2H(J1,J2)),ZJ1(1,1,J3),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH2)
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH1)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP(Z3(1,1,1,INDEX),
     .        ZQP3(1,1,IQ,INDEX),RMQ,PQP3(IQ,INDEX),Z3(1,1,1,INDEX))
* 1, 3, 2
          INDEX=M3H(J1,J3,J2)
          CALL QCURG(Z0(1,1,1),ZJ3(1,1,INDEX),ZH4(1,1,1))
          CALL QCURG(Z2(1,1,1,M2H(J1,J3)),ZJ1(1,1,J2),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH4)
          CALL SUBZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),RMQ,PQP3(IQ,INDEX),Z3(1,1,1,INDEX))
* 2, 1, 3
          INDEX=M3H(J2,J1,J3)
          CALL QCURG(Z0(1,1,1),ZJ3(1,1,INDEX),ZH5(1,1,1))
          CALL QCURG(Z1(1,1,1,J2),ZJ2(1,1,M2H(J1,J3)),ZH2(1,1,1))
          CALL QCURG(Z2(1,1,1,M2H(J2,J1)),ZJ1(1,1,J3),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH5)
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),RMQ,PQP3(IQ,INDEX),Z3(1,1,1,INDEX))
* 2, 3, 1
          INDEX=M3H(J2,J3,J1)
          CALL QCURG(Z2(1,1,1,M2H(J2,J3)),ZJ1(1,1,J1),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH4)
          CALL SUBZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),RMQ,PQP3(IQ,INDEX),Z3(1,1,1,INDEX))
* 3, 1, 2
          INDEX=M3H(J3,J1,J2)
          CALL QCURG(Z1(1,1,1,J3),ZJ2(1,1,M2H(J1,J2)),ZH2(1,1,1))
          CALL QCURG(Z2(1,1,1,M2H(J3,J1)),ZJ1(1,1,J2),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH5)
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),RMQ,PQP3(IQ,INDEX),Z3(1,1,1,INDEX))
* 3, 2, 1
          INDEX=M3H(J3,J2,J1)
          CALL QCURG(Z2(1,1,1,M2H(J3,J2)),ZJ1(1,1,J1),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH1)
          CALL SUBZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQ,INDEX),RMQ,PQP3(IQ,INDEX),Z3(1,1,1,INDEX))
   30 CONTINUE
*
      IF (N.EQ.3) GOTO 999
*
* If N>3 then obtain all Z4-spinors.
*
      DO 40 J=1,IFAC(N)
        J1=IPERM(1,J)
        J2=IPERM(2,J)
        J3=IPERM(3,J)
        J4=IPERM(4,J)
        INDEX=M4H(J1,J2,J3,J4)
        CALL QCURG(Z0(1,1,1),ZJ4(1,1,INDEX),ZH1(1,1,1))
        CALL QCURG(Z1(1,1,1,J1),ZJ3(1,1,M3H(J2,J3,J4)),ZH2(1,1,1))
        CALL QCURG(Z2(1,1,1,M2H(J1,J2)),ZJ2(1,1,M2H(J3,J4)),ZH3(1,1,1))
        CALL QCURG(Z3(1,1,1,M3H(J1,J2,J3)),ZJ1(1,1,J4),Z4(1,1,1,INDEX))
        CALL ADDZHS(Z4(1,1,1,INDEX),ZH1)
        CALL ADDZHS(Z4(1,1,1,INDEX),ZH2)
        CALL ADDZHS(Z4(1,1,1,INDEX),ZH3)
        IF ((N.NE.4).OR.(ILAST.EQ.1)) CALL QCURP(Z4(1,1,1,INDEX),
     .     ZQP4(1,1,IQ,INDEX),RMQ,PQP4(IQ,INDEX),Z4(1,1,1,INDEX))
   40 CONTINUE
*
      IF (N.EQ.4) GOTO 999
*
* If N>4 then obtain all Z5-spinors. NO propagator for the N=5 case.
*
      DO 50 J=1,IFAC(N)
        J1=IPERM(1,J)
        J2=IPERM(2,J)
        J3=IPERM(3,J)
        J4=IPERM(4,J)
        J5=IPERM(5,J)
        INDEX=M5H(J1,J2,J3,J4,J5)
        CALL QCURG(Z0(1,1,1),ZJ5(1,1,INDEX),Z5(1,1,1,INDEX))
        CALL QCURG(Z1(1,1,1,J1),ZJ4(1,1,M4H(J2,J3,J4,J5)),ZH1(1,1,1))
        CALL QCURG(Z2(1,1,1,M2H(J1,J2)),ZJ3(1,1,M3H(J3,J4,J5)),
     .                                                      ZH2(1,1,1))
        CALL QCURG(Z3(1,1,1,M3H(J1,J2,J3)),ZJ2(1,1,M2H(J4,J5)),
     .                                                      ZH3(1,1,1))
        CALL QCURG(Z4(1,1,1,M4H(J1,J2,J3,J4)),ZJ1(1,1,J5),ZH4(1,1,1))
        CALL ADDZHS(Z5(1,1,1,INDEX),ZH1)
        CALL ADDZHS(Z5(1,1,1,INDEX),ZH2)
        CALL ADDZHS(Z5(1,1,1,INDEX),ZH3)
        CALL ADDZHS(Z5(1,1,1,INDEX),ZH4)
   50 CONTINUE
*
  999 CONTINUE
*
      END
*
************************************************************************
* QCURG calculates product of a quark spinor and a gluon current.      *
************************************************************************
      SUBROUTINE QCURG(ZJ,ZG,ZANS)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters
*
      DIMENSION ZJ(2,2,2),ZG(2,2),ZANS(2,2,2)
      COMMON /SPEED/ IMASS
      SAVE /SPEED/
*
* spinor current contracted with contra-variant helicity spinor tensor
*
      DO 10 IH=1,1+IMASS
        ZANS(1,1,IH)=-ZJ(1,2,IH)*ZG(2,2)+ZJ(2,2,IH)*ZG(1,2)
        ZANS(2,1,IH)=+ZJ(1,2,IH)*ZG(2,1)-ZJ(2,2,IH)*ZG(1,1)
        ZANS(1,2,IH)=+ZJ(1,1,IH)*ZG(1,1)+ZJ(2,1,IH)*ZG(1,2)
        ZANS(2,2,IH)=+ZJ(1,1,IH)*ZG(2,1)+ZJ(2,1,IH)*ZG(2,2)
   10 CONTINUE
*
      END
*
************************************************************************
* QCURP calculates product of quark spinor with the propagator.        *
************************************************************************
      SUBROUTINE QCURP(ZJ,ZP,RMQ,PROP,ZANS)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters,
*
      DIMENSION ZJ(2,2,2),ZP(2,2),ZANS(2,2,2)
      COMMON /SPEED/ IMASS
      SAVE /SPEED/
*
* Spinor current contracted with propagator spinor tensor,
* include (0.0,1.0) for propagator.
* Note: ZJ and ZANS can be the same objects!
*
      DO 10 IH=1,1+IMASS
        Z1=( - ZJ(1,2,IH)*ZP(2,2)
     .       + ZJ(2,2,IH)*ZP(1,2) )/PROP
        Z2=( + ZJ(1,2,IH)*ZP(2,1)
     .       - ZJ(2,2,IH)*ZP(1,1) )/PROP
        Z3=( + ZJ(1,1,IH)*ZP(1,1)
     .       + ZJ(2,1,IH)*ZP(1,2) )/PROP
        Z4=( + ZJ(1,1,IH)*ZP(2,1)
     .       + ZJ(2,1,IH)*ZP(2,2) )/PROP
        IF (IMASS.EQ.1) THEN
          Z1=Z1+ (0.0,1.0)*RMQ*ZJ(1,1,IH)/PROP
          Z2=Z2+ (0.0,1.0)*RMQ*ZJ(2,1,IH)/PROP
          Z3=Z3+ (0.0,1.0)*RMQ*ZJ(1,2,IH)/PROP
          Z4=Z4+ (0.0,1.0)*RMQ*ZJ(2,2,IH)/PROP
        END IF
*
        ZANS(1,1,IH)=Z1
        ZANS(2,1,IH)=Z2
        ZANS(1,2,IH)=Z3
        ZANS(2,2,IH)=Z4
   10 CONTINUE
*
      END
*
* Order in Zi's of gluons is such that far right is gluon close to
* the anti-quark
*
      SUBROUTINE QBCUR(IQB,RMQ,N,ILAST,Z0,Z1,Z2,Z3,Z4,Z5)
      PARAMETER(NUP=5,IQUP=4,NUPM1=5,NUPM2=20,NUPM3=60,
     .          NUPM4=120,NUPM5=120,NUPFAC=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters
*
      DIMENSION Z0(2,2,2),Z1(2,2,2,*),Z2(2,2,2,*),
     .          Z3(2,2,2,*),Z4(2,2,2,*),Z5(2,2,2,*)
*
* Global variables
*
      COMMON /SPEED/ IMASS
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /SPINQ/  ZQKO(2,IQUP),ZQKOD(2,IQUP),
     .                ZQMO(2,IQUP),ZQMOD(2,IQUP)
      COMMON /FERMPR/ ZQP0(2,2,IQUP),ZQP1(2,2,IQUP,NUP),
     .                ZQP2(2,2,IQUP,NUPM2),ZQP3(2,2,IQUP,NUPM3),
     .                ZQP4(2,2,IQUP,NUPM4),PQP1(IQUP,NUP),
     .                PQP2(IQUP,NUPM2),PQP3(IQUP,NUPM3),PQP4(IQUP,NUPM4)
*
* Local variables
*
      DIMENSION ZH1(2,2,2),ZH2(2,2,2),ZH3(2,2,2),ZH4(2,2,2),ZH5(2,2,2)
      DIMENSION IPERM(NUP,NUPFAC)
      SAVE INIT,IPERM,/FERMPR/,/SPINQ/,/GLUCUR/,/MAPPIN/,/SPEED/
      DATA INIT /1/
*
      IF (INIT.EQ.1) THEN
        INIT=0
        DO 5 I=1,IFAC(N)
          CALL INVID(I,IPERM(1,I),N)
    5   CONTINUE
      END IF
*
* Init the currents of length 0, V(+) and V(-)
*
      Z0(1,2,1)=-ZQKOD(2,IQB)
      Z0(2,2,1)=+ZQKOD(1,IQB)
      Z0(1,1,1+IMASS)=+ZQKO(1,IQB)
      Z0(2,1,1+IMASS)=+ZQKO(2,IQB)
      IF (IMASS.EQ.1) THEN
        Z0(1,1,1)=-ZQMO(1,IQB)
        Z0(2,1,1)=-ZQMO(2,IQB)
        Z0(1,2,2)=-ZQMOD(2,IQB)
        Z0(2,2,2)=+ZQMOD(1,IQB)
      END IF
*
      IF (N.EQ.0) GOTO 999
*
* If N>0 then calculate all Z1-spinors.
*
      DO 10 J=1,N
        CALL QBCURG(Z0(1,1,1),ZJ1(1,1,J),Z1(1,1,1,J))
        IF ((N.NE.1).OR.(ILAST.EQ.1))  CALL QBCURP(Z1(1,1,1,J),
     .              ZQP1(1,1,IQB,J),RMQ,PQP1(IQB,J),Z1(1,1,1,J))
   10 CONTINUE
*
      IF (N.EQ.1) GOTO 999
*
* If N>1 then calculate all Z2-spinors.
*
      DO 20 J1=1,N-1
        DO 20 J2=J1+1,N
          INDEX=M2H(J2,J1)
          CALL QBCURG(Z0(1,1,1),ZJ2(1,1,INDEX),ZH1(1,1,1))
          CALL QBCURG(Z1(1,1,1,J1),ZJ1(1,1,J2),Z2(1,1,1,INDEX))
          CALL ADDZHS(Z2(1,1,1,INDEX),ZH1)
          IF ((N.NE.2).OR.(ILAST.EQ.1)) CALL QBCURP(Z2(1,1,1,INDEX),
     .       ZQP2(1,1,IQB,INDEX),RMQ,PQP2(IQB,INDEX),Z2(1,1,1,INDEX))
          INDEX=M2H(J1,J2)
          CALL QBCURG(Z1(1,1,1,J2),ZJ1(1,1,J1),Z2(1,1,1,INDEX))
          CALL SUBZHS(Z2(1,1,1,INDEX),ZH1)
          IF ((N.NE.2).OR.(ILAST.EQ.1)) CALL QBCURP(Z2(1,1,1,INDEX),
     .       ZQP2(1,1,IQB,INDEX),RMQ,PQP2(IQB,INDEX),Z2(1,1,1,INDEX))
   20 CONTINUE
*
      IF (N.EQ.2) GOTO 999
*
* If N>2 then obtain all Z3-spinors.
*
      DO 30 J1=1,N-2
       DO 30 J2=J1+1,N-1
        DO 30 J3=J2+1,N
* 3, 2, 1
          INDEX=M3H(J3,J2,J1)
          CALL QBCURG(Z0(1,1,1),ZJ3(1,1,INDEX),ZH1(1,1,1))
          CALL QBCURG(Z1(1,1,1,J1),ZJ2(1,1,M2H(J3,J2)),ZH2(1,1,1))
          CALL QBCURG(Z2(1,1,1,M2H(J2,J1)),ZJ1(1,1,J3),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH2)
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH1)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),RMQ,PQP3(IQB,INDEX),Z3(1,1,1,INDEX))
* 2, 3, 1
          INDEX=M3H(J2,J3,J1)
          CALL QBCURG(Z0(1,1,1),ZJ3(1,1,INDEX),ZH4(1,1,1))
          CALL QBCURG(Z2(1,1,1,M2H(J3,J1)),ZJ1(1,1,J2),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH4)
          CALL SUBZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),RMQ,PQP3(IQB,INDEX),Z3(1,1,1,INDEX))
* 3, 1, 2
          INDEX=M3H(J3,J1,J2)
          CALL QBCURG(Z0(1,1,1),ZJ3(1,1,INDEX),ZH5(1,1,1))
          CALL QBCURG(Z1(1,1,1,J2),ZJ2(1,1,M2H(J3,J1)),ZH2(1,1,1))
          CALL QBCURG(Z2(1,1,1,M2H(J1,J2)),ZJ1(1,1,J3),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH5)
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),RMQ,PQP3(IQB,INDEX),Z3(1,1,1,INDEX))
* 1, 3, 2
          INDEX=M3H(J1,J3,J2)
          CALL QBCURG(Z2(1,1,1,M2H(J3,J2)),ZJ1(1,1,J1),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH4)
          CALL SUBZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),RMQ,PQP3(IQB,INDEX),Z3(1,1,1,INDEX))
* 2, 1, 3
          INDEX=M3H(J2,J1,J3)
          CALL QBCURG(Z1(1,1,1,J3),ZJ2(1,1,M2H(J2,J1)),ZH2(1,1,1))
          CALL QBCURG(Z2(1,1,1,M2H(J1,J3)),ZJ1(1,1,J2),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH5)
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),RMQ,PQP3(IQB,INDEX),Z3(1,1,1,INDEX))
* 1, 2, 3
          INDEX=M3H(J1,J2,J3)
          CALL QBCURG(Z2(1,1,1,M2H(J2,J3)),ZJ1(1,1,J1),Z3(1,1,1,INDEX))
          CALL ADDZHS(Z3(1,1,1,INDEX),ZH1)
          CALL SUBZHS(Z3(1,1,1,INDEX),ZH2)
          IF ((N.NE.3).OR.(ILAST.EQ.1)) CALL QBCURP(Z3(1,1,1,INDEX),
     .       ZQP3(1,1,IQB,INDEX),RMQ,PQP3(IQB,INDEX),Z3(1,1,1,INDEX))
   30 CONTINUE
*
      IF (N.EQ.3) GOTO 999
*
* If N>3 then obtain all Z4-spinors.
*
      DO 40 J=1,IFAC(N)
        J1=IPERM(1,J)
        J2=IPERM(2,J)
        J3=IPERM(3,J)
        J4=IPERM(4,J)
        INDEX=M4H(J4,J3,J2,J1)
        CALL QBCURG(Z0(1,1,1),ZJ4(1,1,INDEX),ZH1(1,1,1))
        CALL QBCURG(Z1(1,1,1,J1),ZJ3(1,1,M3H(J4,J3,J2)),ZH2(1,1,1))
        CALL QBCURG(Z2(1,1,1,M2H(J2,J1)),ZJ2(1,1,M2H(J4,J3)),ZH3(1,1,1))
        CALL QBCURG(Z3(1,1,1,M3H(J3,J2,J1)),ZJ1(1,1,J4),Z4(1,1,1,INDEX))
        CALL ADDZHS(Z4(1,1,1,INDEX),ZH1)
        CALL ADDZHS(Z4(1,1,1,INDEX),ZH2)
        CALL ADDZHS(Z4(1,1,1,INDEX),ZH3)
        IF ((N.NE.4).OR.(ILAST.EQ.1)) CALL QBCURP(Z4(1,1,1,INDEX),
     .     ZQP4(1,1,IQB,INDEX),RMQ,PQP4(IQB,INDEX),Z4(1,1,1,INDEX))
   40 CONTINUE
*
      IF (N.EQ.4) GOTO 999
*
* If N>4 then obtain all Z5-spinors. NO propagator for the N=5 case.
*
      DO 50 J=1,IFAC(N)
        J1=IPERM(1,J)
        J2=IPERM(2,J)
        J3=IPERM(3,J)
        J4=IPERM(4,J)
        J5=IPERM(5,J)
        INDEX=M5H(J5,J4,J3,J2,J1)
        CALL QBCURG(Z0(1,1,1),ZJ5(1,1,INDEX),Z5(1,1,1,INDEX))
        CALL QBCURG(Z1(1,1,1,J1),ZJ4(1,1,M4H(J5,J4,J3,J2)),ZH2(1,1,1))
        CALL QBCURG(Z2(1,1,1,M2H(J2,J1)),ZJ3(1,1,M3H(J5,J4,J3)),
     .                                                     ZH3(1,1,1))
        CALL QBCURG(Z3(1,1,1,M3H(J3,J2,J1)),ZJ2(1,1,M2H(J5,J4)),
     .                                                     ZH4(1,1,1))
        CALL QBCURG(Z4(1,1,1,M4H(J4,J3,J2,J1)),ZJ1(1,1,J5),ZH5(1,1,1))
        CALL ADDZHS(Z5(1,1,1,INDEX),ZH2)
        CALL ADDZHS(Z5(1,1,1,INDEX),ZH3)
        CALL ADDZHS(Z5(1,1,1,INDEX),ZH4)
        CALL ADDZHS(Z5(1,1,1,INDEX),ZH5)
   50 CONTINUE
*
  999 CONTINUE
      END
*
************************************************************************
* QBCURG calculates product of a antiquark spinor and a gluon current. *
************************************************************************
      SUBROUTINE QBCURG(ZJ,ZG,ZANS)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters
*
      DIMENSION ZJ(2,2,2),ZG(2,2),ZANS(2,2,2)
      COMMON /SPEED/ IMASS
      SAVE /SPEED/
*
* spinor current contracted with contra-variant helicity spinor tensor
*
      DO 10 IH=1,1+IMASS
        ZANS(1,1,IH)=+ZJ(1,2,IH)*ZG(1,1)+ZJ(2,2,IH)*ZG(2,1)
        ZANS(2,1,IH)=+ZJ(1,2,IH)*ZG(1,2)+ZJ(2,2,IH)*ZG(2,2)
        ZANS(1,2,IH)=-ZJ(1,1,IH)*ZG(2,2)+ZJ(2,1,IH)*ZG(2,1)
        ZANS(2,2,IH)=+ZJ(1,1,IH)*ZG(1,2)-ZJ(2,1,IH)*ZG(1,1)
   10 CONTINUE
*
      END
*
************************************************************************
* QBCURP calculates product of antiquark spinor with the propagator.   *
************************************************************************
      SUBROUTINE QBCURP(ZJ,ZP,RMQ,PROP,ZANS)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters,
*
      DIMENSION ZJ(2,2,2),ZP(2,2),ZANS(2,2,2)
      COMMON /SPEED/ IMASS
      SAVE /SPEED/
*
* Spinor current contracted with propagator spinor tensor,
* include (0.0,-1.0) for propagator, massterm extra minus in numerator.
* Note: ZJ and ZANS can be the same objects!
*
      DO 10 IH=1,1+IMASS
        Z1=( - ZJ(1,2,IH)*ZP(1,1)
     .       - ZJ(2,2,IH)*ZP(2,1) )/PROP
        Z2=( - ZJ(1,2,IH)*ZP(1,2)
     .       - ZJ(2,2,IH)*ZP(2,2) )/PROP
        Z3=( + ZJ(1,1,IH)*ZP(2,2)
     .       - ZJ(2,1,IH)*ZP(2,1) )/PROP
        Z4=( - ZJ(1,1,IH)*ZP(1,2)
     .       + ZJ(2,1,IH)*ZP(1,1) )/PROP
        IF (IMASS.EQ.1) THEN
          Z1=Z1 + (0.0,1.0)*RMQ*ZJ(1,1,IH)/PROP
          Z2=Z2 + (0.0,1.0)*RMQ*ZJ(2,1,IH)/PROP
          Z3=Z3 + (0.0,1.0)*RMQ*ZJ(1,2,IH)/PROP
          Z4=Z4 + (0.0,1.0)*RMQ*ZJ(2,2,IH)/PROP
        END IF
*
        ZANS(1,1,IH)=Z1
        ZANS(2,1,IH)=Z2
        ZANS(1,2,IH)=Z3
        ZANS(2,2,IH)=Z4
   10 CONTINUE
*
      END
*
************************************************************************
* ADDZHS adds two quark spinors. (ZH1=ZH1+ZH2)                         *
************************************************************************
      SUBROUTINE ADDZHS(ZH1,ZH2)
      COMPLEX ZH1(2,2,2),ZH2(2,2,2)
      COMMON /SPEED/ IMASS
      SAVE /SPEED/
      DO 10 K=1,1+IMASS
        DO 10 I=1,2
          DO 10 J=1,2
            ZH1(I,J,K)=ZH1(I,J,K)+ZH2(I,J,K)
   10 CONTINUE
      END
*
************************************************************************
* SUBZHS subtracts two quark spinors. (ZH1=ZH1-ZH2)                    *
************************************************************************
      SUBROUTINE SUBZHS(ZH1,ZH2)
      COMPLEX ZH1(2,2,2),ZH2(2,2,2)
      COMMON /SPEED/ IMASS
      SAVE /SPEED/
      DO 10 K=1,1+IMASS
        DO 10 I=1,2
          DO 10 J=1,2
            ZH1(I,J,K)=ZH1(I,J,K)-ZH2(I,J,K)
   10 CONTINUE
      END
*
************************************************************************
* CURS calculates gluonic currents with possibly off shell particles   *
*                                                                      *
* In:   Set of N helicity vectors, helicities are implicit             *
*       ILAST=1 means to include last propagator                       *
*       Limitation N<=5                                                *
* Out:  All gluonic currents in spinor language                        *
************************************************************************
      SUBROUTINE CURS(N,ILAST,ZH,IOFFSH)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,
     .          NUPM5=120,NUPMFA=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters
*
      DIMENSION ZH(2,2,*)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
*
* local variables CURINT = common with internal variables
*
      COMMON /CURINT/ ZJ1J1(NUP,NUP),ZJ1J2(NUP,NUP,NUP),
     .   ZJ2J2(NUP,NUP,NUP,NUP),IPERM(NUP,NUPMFA)
      SAVE /MAPPIN/,/GLUCUR/,/GLUOPR/,/CURINT/,INIT5
      DATA INIT5 /1/
*
      IF (N.EQ.0) GOTO 999
*
* Calculate the 1-currents, copy vectors.
*
      CALL COPYTS(ZJ1,ZH,N)
*
      IF (N.EQ.1) GOTO 999
*
* Calculate the 2-currents and all JiDJj products
*
      DO 20 I1=1,N-1
       DO 20 I2=I1+1,N
         Z1=0.5*ZMUL(ZJ1(1,1,I1),ZJ1(1,1,I2))
         ZJ1J1(I1,I2)=Z1
         ZJ1J1(I2,I1)=Z1
         Z2=+ZMUL(ZP1(1,1,I2),ZJ1(1,1,I1))
         Z3=-ZMUL(ZP1(1,1,I1),ZJ1(1,1,I2))
         IF (IOFFSH.EQ.1) THEN
           Z2=Z2+0.5*ZMUL(ZP1(1,1,I1),ZJ1(1,1,I1))
           Z3=Z3-0.5*ZMUL(ZP1(1,1,I2),ZJ1(1,1,I2))
         END IF
         PROP=1.0
         IF( (N.NE.2).OR.(ILAST.EQ.1) ) PROP=PP2(M2H(I1,I2))
         DO 30 IAD=1,2
          DO 30 IB=1,2
            ZZ1=(+Z2*ZJ1(IAD,IB,I2)+Z3*ZJ1(IAD,IB,I1)
     .           +Z1*(ZP1(IAD,IB,I1)-ZP1(IAD,IB,I2)))/PROP
            ZJ2(IAD,IB,M2H(I1,I2))=ZZ1
            ZJ2(IAD,IB,M2H(I2,I1))=-ZZ1
   30    CONTINUE
   20 CONTINUE
*
      IF (N.EQ.2) GOTO 999
*
* Calculation of all J1DJ2 products
*
      DO 40 I1=1,N
       DO 40 I2=1,N-1
        DO 40 I3=I2+1,N
          ZZ1=0.5*ZMUL(ZJ1(1,1,I1),ZJ2(1,1,M2H(I2,I3)))
          ZJ1J2(I1,I2,I3)=ZZ1
          ZJ1J2(I1,I3,I2)=-ZZ1
   40 CONTINUE
*
* Calculation of the 3-currents
*
      DO 50 I1=1,N-2
       DO 50 I2=I1+1,N-1
        DO 50 I3=I2+1,N
          Z1=ZJ1J2(I1,I2,I3)
          Z2=ZMUL(ZP2(1,1,M2H(I2,I3)),ZJ1(1,1,I1))
          Z3=-ZMUL(ZP1(1,1,I1),ZJ2(1,1,M2H(I2,I3)))
          Z4=ZJ1J2(I3,I1,I2)
          Z5=+ZMUL(ZP1(1,1,I3),ZJ2(1,1,M2H(I1,I2)))
          Z6=-ZMUL(ZP2(1,1,M2H(I1,I2)),ZJ1(1,1,I3))
          Z7=ZJ1J2(I2,I1,I3)
          Z8=+ZMUL(ZP1(1,1,I2),ZJ2(1,1,M2H(I1,I3)))
          Z9=-ZMUL(ZP2(1,1,M2H(I1,I3)),ZJ1(1,1,I2))
          IF (IOFFSH.EQ.1) THEN
            Z2=Z2+0.5*ZMUL(ZP1(1,1,I1),ZJ1(1,1,I1))
            Z3=Z3-0.5*ZMUL(ZP2(1,1,M2H(I2,I3)),ZJ2(1,1,M2H(I2,I3)))
            Z5=Z5+0.5*ZMUL(ZP2(1,1,M2H(I1,I2)),ZJ2(1,1,M2H(I1,I2)))
            Z6=Z6-0.5*ZMUL(ZP1(1,1,I3),ZJ1(1,1,I3))
            Z8=Z8+0.5*ZMUL(ZP2(1,1,M2H(I1,I3)),ZJ2(1,1,M2H(I1,I3)))
            Z9=Z9-0.5*ZMUL(ZP1(1,1,I2),ZJ1(1,1,I2))
          END IF
          PROP=1.0
          IF( (N.NE.3).OR.(ILAST.EQ.1) ) PROP=PP3(M3H(I1,I2,I3))
          DO 60 IAD=1,2
           DO 60 IB=1,2
             ZHELP= +Z2*ZJ2(IAD,IB,M2H(I2,I3))+Z3*ZJ1(IAD,IB,I1)
     .              +(ZP1(IAD,IB,I1)-ZP2(IAD,IB,M2H(I2,I3)))*Z1
             ZZ1=(  +ZHELP
     .              +Z5*ZJ1(IAD,IB,I3)+Z6*ZJ2(IAD,IB,M2H(I1,I2))
     .              +(ZP2(IAD,IB,M2H(I1,I2))-ZP1(IAD,IB,I3))*Z4
     .              +2.0*ZJ1J1(I1,I3)*ZJ1(IAD,IB,I2)
     .              -ZJ1J1(I1,I2)*ZJ1(IAD,IB,I3)
     .              -ZJ1J1(I2,I3)*ZJ1(IAD,IB,I1)    )/PROP
             ZZ2=(  -ZHELP
     .              +Z8*ZJ1(IAD,IB,I2)+Z9*ZJ2(IAD,IB,M2H(I1,I3))
     .              +(ZP2(IAD,IB,M2H(I1,I3))-ZP1(IAD,IB,I2))*Z7
     .              +2.0*ZJ1J1(I1,I2)*ZJ1(IAD,IB,I3)
     .              -ZJ1J1(I1,I3)*ZJ1(IAD,IB,I2)
     .              -ZJ1J1(I3,I2)*ZJ1(IAD,IB,I1)    )/PROP
            ZJ3(IAD,IB,M3H(I1,I2,I3))=ZZ1
            ZJ3(IAD,IB,M3H(I3,I2,I1))=ZZ1
            ZJ3(IAD,IB,M3H(I1,I3,I2))=ZZ2
            ZJ3(IAD,IB,M3H(I2,I3,I1))=ZZ2
            ZJ3(IAD,IB,M3H(I2,I1,I3))=-ZZ1-ZZ2
            ZJ3(IAD,IB,M3H(I3,I1,I2))=-ZZ1-ZZ2
   60     CONTINUE
   50 CONTINUE
*
      IF (N.EQ.3) GOTO 999
*
* Calculation of 4-currents
*
      DO 70 I1=1,N-3
       DO 70 I2=I1+1,N-2
        DO 70 I3=I2+1,N-1
         DO 70 I4=I3+1,N
           PROP=0.0
           IF( (N.NE.4).OR.(ILAST.EQ.1)) PROP=PP4(M4H(I1,I2,I3,I4))
           CALL CUR4(I1,I2,I3,I4,PROP,IOFFSH)
           CALL CUR4(I1,I2,I4,I3,PROP,IOFFSH)
           CALL CUR4(I1,I3,I4,I2,PROP,IOFFSH)
           CALL CUR4(I1,I3,I2,I4,PROP,IOFFSH)
           CALL CUR4(I1,I4,I2,I3,PROP,IOFFSH)
           CALL CUR4(I1,I4,I3,I2,PROP,IOFFSH)
           CALL FULLZ4(I1,I2,I3,I4)
   70 CONTINUE
*
      IF (N.EQ.4) GOTO 999
*
* Store permutations of 2..NUP in IPERM for two reasons:
* 1. calling CUR5         2. unwrapping base of ZJ5's to full set
*
      IF (INIT5.EQ.1) THEN
        INIT5=0
        DO 80 I=1,IFAC(N-1)
          CALL INVID(I,IPERM(1,I),N)
   80   CONTINUE
      END IF
*
* Calculate all J2DJ2 products.
*
      DO 90 I1=1,N
       DO 90 I2=I1+1,N
        DO 90 I3=1,N
         DO 90 I4=I3+1,N
           IF ( (I1.NE.I3).AND.(I1.NE.I4).AND.
     .          (I2.NE.I3).AND.(I2.NE.I4) ) THEN
             ZZ2=0.5*ZMUL(ZJ2(1,1,M2H(I1,I2)),ZJ2(1,1,M2H(I3,I4)))
             ZJ2J2(I1,I2,I3,I4)=ZZ2
             ZJ2J2(I1,I2,I4,I3)=-ZZ2
             ZJ2J2(I2,I1,I3,I4)=-ZZ2
             ZJ2J2(I2,I1,I4,I3)=ZZ2
             ZJ2J2(I3,I4,I1,I2)=ZZ2
             ZJ2J2(I4,I3,I1,I2)=-ZZ2
             ZJ2J2(I3,I4,I2,I1)=-ZZ2
             ZJ2J2(I4,I3,I2,I1)=ZZ2
           END IF
   90 CONTINUE
*
* Calculation of 5-currents
*
      PROP=1.0
      IF( ILAST.EQ.1 )
     .  PROP=REAL( +ZMUL(ZP1(1,1,1),ZP4(1,1,M4H(2,3,4,5)))
     .         +0.5*ZMUL(ZP1(1,1,1),ZP1(1,1,1))
     .         +0.5*ZMUL(ZP4(1,1,M4H(2,3,4,5)),ZP4(1,1,M4H(2,3,4,5)))  )
      DO 100 I=1,IFAC(N-1)
        CALL CUR5(IPERM(1,I),IPERM(2,I),IPERM(3,I),
     .            IPERM(4,I),IPERM(5,I),PROP,IOFFSH)
  100 CONTINUE
      CALL FULLZ5(1,2,3,4,5)
*
  999 CONTINUE
      END
*
************************************************************************
* CUR4(I1,I2,I3,I4,PROP) returns the sum over the (curly) brackets     *
* terms which appear in the gluonic recursion relation for 4 gluons.   *
*                                                                      *
*   In:  I1,I2,I3,I4 gluon identifiers, PROP propagator of current.    *
*   Out: ZJ4(2,2,I1,I2,I3,I4) current.                                 *
************************************************************************
      SUBROUTINE CUR4(I1,I2,I3,I4,PROP,IOFFSH)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,
     .          NUPM5=120,NUPMFA=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /CURINT/ ZJ1J1(NUP,NUP),ZJ1J2(NUP,NUP,NUP),
     .   ZJ2J2(NUP,NUP,NUP,NUP),IPERM(NUP,NUPMFA)
*
* local variables
*
      DIMENSION ZT1(2,2),ZT2(2,2),ZT3(2,2),ZT4(2,2)
      SAVE /MAPPIN/,/GLUCUR/,/GLUOPR/,/CURINT/
*
* Square bracket terms
*
      CALL BRAC(ZJ1(1,1,I1),ZP1(1,1,I1),ZJ3(1,1,M3H(I2,I3,I4)),
     .          ZP3(1,1,M3H(I2,I3,I4)),ZT1,IOFFSH)
      CALL BRAC(ZJ2(1,1,M2H(I1,I2)),ZP2(1,1,M2H(I1,I2)),
     .          ZJ2(1,1,M2H(I3,I4)),ZP2(1,1,M2H(I3,I4)),ZT2,IOFFSH)
      CALL BRAC(ZJ3(1,1,M3H(I1,I2,I3)),ZP3(1,1,M3H(I1,I2,I3)),
     .          ZJ1(1,1,I4),ZP1(1,1,I4),ZT3,IOFFSH)
*
* Contribution of curly bracket terms
*
      Z1=+2.0*ZJ1J2(I1,I3,I4)
      Z2=-ZJ1J1(I1,I2)
      Z3=-(ZJ1J2(I2,I3,I4)+ZJ1J2(I4,I2,I3))
      Z4=+2.0*ZJ1J1(I1,I4)
      Z5=-(ZJ1J2(I1,I2,I3)+ZJ1J2(I3,I1,I2))
      Z6=+2.0*ZJ1J2(I4,I1,I2)
      Z7=-ZJ1J1(I3,I4)
      DO 10 IAD=1,2
       DO 10 IB=1,2
         ZT4(IAD,IB)=+Z1*ZJ1(IAD,IB,I2)   +Z2*ZJ2(IAD,IB,M2H(I3,I4))
     .               +Z3*ZJ1(IAD,IB,I1)   +Z4*ZJ2(IAD,IB,M2H(I2,I3))
     .               +Z5*ZJ1(IAD,IB,I4)   +Z6*ZJ1(IAD,IB,I3)
     .               +Z7*ZJ2(IAD,IB,M2H(I1,I2))
         ZJ4(IAD,IB,M4H(I1,I2,I3,I4))=(ZT1(IAD,IB)+ZT2(IAD,IB)
     .               +ZT3(IAD,IB)+ZT4(IAD,IB))/PROP
         ZJ4(IAD,IB,M4H(I4,I3,I2,I1))=-ZJ4(IAD,IB,M4H(I1,I2,I3,I4))
   10 CONTINUE
*
      END
*
************************************************************************
* FULLZ4(I1,I2,I3,I4) build up all 4-currents from a reduced set       *
*                     where (I1<I2,I3,I4).                             *
*                                                                      *
*   In:  I1,I2,I3,I4 gluon identifiers.                                *
*   Out: All ZJ4(2,2,P(I1,I2,I3,I4)) currents.                         *
************************************************************************
      SUBROUTINE FULLZ4(I1,I2,I3,I4)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      SAVE /MAPPIN/,/GLUCUR/
*
      DO 10 IAD=1,2
       DO 10 IB=1,2
         Z1234=ZJ4(IAD,IB,M4H(I1,I2,I3,I4))
         Z1243=ZJ4(IAD,IB,M4H(I1,I2,I4,I3))
         Z1324=ZJ4(IAD,IB,M4H(I1,I3,I2,I4))
         Z1342=ZJ4(IAD,IB,M4H(I1,I3,I4,I2))
         Z1423=ZJ4(IAD,IB,M4H(I1,I4,I2,I3))
         Z1432=ZJ4(IAD,IB,M4H(I1,I4,I3,I2))
         Z2134=-Z1234-Z1324-Z1342
         Z2143=-Z1243-Z1423-Z1432
         Z3124=-Z1324-Z1234-Z1243
         Z3142=-Z1342-Z1432-Z1423
         Z4123=-Z1423-Z1243-Z1234
         Z4132=-Z1432-Z1342-Z1324
         ZJ4(IAD,IB,M4H(I2,I1,I3,I4))=Z2134
         ZJ4(IAD,IB,M4H(I2,I1,I4,I3))=Z2143
         ZJ4(IAD,IB,M4H(I3,I1,I2,I4))=Z3124
         ZJ4(IAD,IB,M4H(I3,I1,I4,I2))=Z3142
         ZJ4(IAD,IB,M4H(I4,I1,I2,I3))=Z4123
         ZJ4(IAD,IB,M4H(I4,I1,I3,I2))=Z4132
         ZJ4(IAD,IB,M4H(I4,I3,I1,I2))=-Z2134
         ZJ4(IAD,IB,M4H(I3,I4,I1,I2))=-Z2143
         ZJ4(IAD,IB,M4H(I4,I2,I1,I3))=-Z3124
         ZJ4(IAD,IB,M4H(I2,I4,I1,I3))=-Z3142
         ZJ4(IAD,IB,M4H(I3,I2,I1,I4))=-Z4123
         ZJ4(IAD,IB,M4H(I2,I3,I1,I4))=-Z4132
   10 CONTINUE
*
      END
*
************************************************************************
* CUR5(I1,I2,I3,I4,I5,PROP) returns the sum over the (curly) brackets  *
* terms which appear in the gluonic recursion relation for 5 gluons    *
*                                                                      *
*   In:  I1,I2,I3,I4,I5 gluon identifiers, PROP= propagator            *
*   Out: ZJ5(2,2,I1,I2,I3,I4) current.                                 *
************************************************************************
      SUBROUTINE CUR5(I1,I2,I3,I4,I5,PROP,IOFFSH)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,
     .          NUPM5=120,NUPMFA=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /CURINT/ ZJ1J1(NUP,NUP),ZJ1J2(NUP,NUP,NUP),
     .   ZJ2J2(NUP,NUP,NUP,NUP),IPERM(NUP,NUPMFA)
*
* local variables
*
      DIMENSION ZT1(2,2),ZT2(2,2),ZT3(2,2),ZT4(2,2),ZT5(2,2)
      SAVE /MAPPIN/,/GLUCUR/,/GLUOPR/,/CURINT/
*
* Square bracket terms
*
      CALL BRAC(ZJ1(1,1,I1),ZP1(1,1,I1),ZJ4(1,1,M4H(I2,I3,I4,I5)),
     .        ZP4(1,1,M4H(I2,I3,I4,I5)),ZT1,IOFFSH)
      CALL BRAC(ZJ2(1,1,M2H(I1,I2)),ZP2(1,1,M2H(I1,I2)),
     .        ZJ3(1,1,M3H(I3,I4,I5)),ZP3(1,1,M3H(I3,I4,I5)),ZT2,IOFFSH)
      CALL BRAC(ZJ3(1,1,M3H(I1,I2,I3)),ZP3(1,1,M3H(I1,I2,I3)),
     .        ZJ2(1,1,M2H(I4,I5)),ZP2(1,1,M2H(I4,I5)),ZT3,IOFFSH)
      CALL BRAC(ZJ4(1,1,M4H(I1,I2,I3,I4)),ZP4(1,1,M4H(I1,I2,I3,I4)),
     .        ZJ1(1,1,I5),ZP1(1,1,I5),ZT4,IOFFSH)
*
* Contribution of curly bracket terms
*
      Z2D345=0.5*ZMUL(ZJ1(1,1,I2),ZJ3(1,1,M3H(I3,I4,I5)))
      Z5D234=0.5*ZMUL(ZJ1(1,1,I5),ZJ3(1,1,M3H(I2,I3,I4)))
      Z1D345=0.5*ZMUL(ZJ1(1,1,I1),ZJ3(1,1,M3H(I3,I4,I5)))
      Z5D123=0.5*ZMUL(ZJ1(1,1,I5),ZJ3(1,1,M3H(I1,I2,I3)))
      Z1D234=0.5*ZMUL(ZJ1(1,1,I1),ZJ3(1,1,M3H(I2,I3,I4)))
      Z4D123=0.5*ZMUL(ZJ1(1,1,I4),ZJ3(1,1,M3H(I1,I2,I3)))
*
      Z1=-(Z2D345+ZJ2J2(I2,I3,I4,I5)+Z5D234)
      Z2=-(ZJ1J2(I3,I4,I5)+ZJ1J2(I5,I3,I4))
      Z3=-ZJ1J1(I4,I5)
      Z4=2.0*Z1D345
      Z5=2.0*ZJ1J2(I1,I4,I5)
      Z6=2.0*ZJ1J1(I1,I5)
      Z7=2.0*ZJ2J2(I1,I2,I4,I5)
      Z8=2.0*ZJ1J2(I5,I1,I2)
      Z9=-ZJ1J1(I1,I2)
      ZA=2.0*Z5D123
      ZB=-(ZJ1J2(I1,I2,I3)+ZJ1J2(I3,I1,I2))
      ZC=-(Z1D234+ZJ2J2(I1,I2,I3,I4)+Z4D123)
      DO 10 IAD=1,2
       DO 10 IB=1,2
         ZT5(IAD,IB)=+Z1*ZJ1(IAD,IB,I1)
     .               +Z2*ZJ2(IAD,IB,M2H(I1,I2))
     .               +Z3*ZJ3(IAD,IB,M3H(I1,I2,I3))
     .               +Z4*ZJ1(IAD,IB,I2)
     .               +Z5*ZJ2(IAD,IB,M2H(I2,I3))
     .               +Z6*ZJ3(IAD,IB,M3H(I2,I3,I4))
     .               +Z7*ZJ1(IAD,IB,I3)
     .               +Z8*ZJ2(IAD,IB,M2H(I3,I4))
     .               +Z9*ZJ3(IAD,IB,M3H(I3,I4,I5))
     .               +ZA*ZJ1(IAD,IB,I4)
     .               +ZB*ZJ2(IAD,IB,M2H(I4,I5))
     .               +ZC*ZJ1(IAD,IB,I5)
   10 CONTINUE
*
* Answer in ZJ5(1,1,I1,I2,I3,I4,5) & ZJ5(1,1,I5,I4,I3,I2,I1)
*
      DO 20 IAD=1,2
       DO 20 IB=1,2
        ZJ5(IAD,IB,M5H(I1,I2,I3,I4,I5))=(ZT1(IAD,IB)+ZT2(IAD,IB)
     .              +ZT3(IAD,IB)+ZT4(IAD,IB)+ZT5(IAD,IB))/PROP
        ZJ5(IAD,IB,M5H(I5,I4,I3,I2,I1))=ZJ5(IAD,IB,M5H(I1,I2,I3,I4,I5))
   20 CONTINUE
*
      END
*
************************************************************************
* FULLZ5(I1,I2,I3,I4,I5) build up all 4-currents from a reduced set    *
*                     where (I1<I2,I3,I4,I5).                          *
*                                                                      *
*   In:  I1,I2,I3,I4,I5 gluon identifiers.                             *
*   Out: All ZJ5(2,2,P(I1,I2,I3,I4,I5)) currents.                      *
************************************************************************
      SUBROUTINE FULLZ5(I1,I2,I3,I4,I5)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,
     .          NUPM5=120,NUPMFA=24)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUCUR/ ZJ1(2,2,NUP),ZJ2(2,2,NUPM2),ZJ3(2,2,NUPM3),
     .                ZJ4(2,2,NUPM4),ZJ5(2,2,NUPM5)
      COMMON /CURINT/ ZJ1J1(NUP,NUP),ZJ1J2(NUP,NUP,NUP),
     .   ZJ2J2(NUP,NUP,NUP,NUP),IPERM(NUP,NUPMFA)
      DIMENSION IP(NUP)
      SAVE /MAPPIN/,/GLUCUR/,/CURINT/
*
      IP(1)=I1
      IP(2)=I2
      IP(3)=I3
      IP(4)=I4
      IP(5)=I5
      DO 20 IAD=1,2
       DO 20 IB=1,2
         DO 30 I=1,24
           J1=IP(IPERM(1,I))
           J2=IP(IPERM(2,I))
           J3=IP(IPERM(3,I))
           J4=IP(IPERM(4,I))
           J5=IP(IPERM(5,I))
           ZJ5(IAD,IB,M5H(J5,J1,J2,J3,J4))=
     .              -ZJ5(IAD,IB,M5H(J1,J2,J3,J4,J5))
     .              -ZJ5(IAD,IB,M5H(J1,J2,J3,J5,J4))
     .              -ZJ5(IAD,IB,M5H(J1,J2,J5,J3,J4))
     .              -ZJ5(IAD,IB,M5H(J1,J5,J2,J3,J4))
           ZJ5(IAD,IB,M5H(J4,J3,J2,J1,J5))=
     .              +ZJ5(IAD,IB,M5H(J5,J1,J2,J3,J4))
   30    CONTINUE
         DO 40 I=1,24
           J1=IP(IPERM(1,I))
           J2=IP(IPERM(2,I))
           J3=IP(IPERM(3,I))
           J4=IP(IPERM(4,I))
           J5=IP(IPERM(5,I))
           ZJ5(IAD,IB,M5H(J2,J3,J1,J4,J5))=
     .              -ZJ5(IAD,IB,M5H(J1,J2,J3,J4,J5))
     .              -ZJ5(IAD,IB,M5H(J2,J1,J3,J4,J5))
     .              -ZJ5(IAD,IB,M5H(J2,J3,J4,J1,J5))
     .              -ZJ5(IAD,IB,M5H(J2,J3,J4,J5,J1))
   40    CONTINUE
   20 CONTINUE
*
      END
*
************************************************************************
* BRAC(Z1,ZP1,Z2,ZP2,Z3,IOFF) is the bracket term                      *
*           which appears in the recursion relation.                   *
*                                                                      *
* In:  Z1 & ZP1, attributes ( hel,mom ) of current 1                   *
*      Z2 & ZP2, same for current 2                                    *
*      Currents need not be conserved ( eg Z1.ZP1 != 0 ) when IOFFSH=1 *
* Out: Z3(2,2)                                                         *
************************************************************************
      SUBROUTINE BRAC(Z1,ZP1,Z2,ZP2,Z3,IOFFSH)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameters,
*
      DIMENSION Z1(2,2),ZP1(2,2),Z2(2,2),ZP2(2,2),Z3(2,2)
*
      ZA=ZMUL(ZP2,Z1)
      ZB=ZMUL(ZP1,Z2)
      IF (IOFFSH.EQ.1) THEN
        ZA=ZA+0.5*ZMUL(ZP1,Z1)
        ZB=ZB+0.5*ZMUL(ZP2,Z2)
      END IF
      ZC=0.5*ZMUL(Z1,Z2)
      DO 10 IAD=1,2
        DO 10 IB=1,2
          Z3(IAD,IB)=+ZA*Z2(IAD,IB)-ZB*Z1(IAD,IB)
     .               +ZC*(ZP1(IAD,IB)-ZP2(IAD,IB))
   10 CONTINUE
*
      END
*
************************************************************************
* Subroutine Q6S calls the matrix elements for six quarks + 0,1 gluons.*
* The general procedure to call a matrix element is:                   *
*                                                                      *
* SUBROUTINE Q6S(PLAB,NPART,IQ1,IQB1,IQ2,IQB2,IQ3,IQB3,IW,IHRN,OUT)    *
*    with  PLAB  = Momenta of partons                                  *
*          NPART = Number of partons = 2 + # gluons                    *
*          IQi   = Positions of outgoing quarks                        *
*          IQBi  = Positions of outgoing anti-quarks                   *
*          IW    = Which flavour combinations?                         *
*          IHRN  = if .eq.1 then MC over helicities                    *
*          OUT(5)= results for different flavour combinations.         *
*                                                                      *
* The subroutine Q6S has internally single precision.                  *
************************************************************************
      SUBROUTINE Q6S(PLAB,NPART,IQ1,IQB1,IQ2,IQB2,IQ3,IQB3,IW,IHRN,OUT)
      IMPLICIT REAL*8 (A-H,O-Y)
      DIMENSION PLAB(4,*),OUT(5)
*
      IF (NPART.EQ.6) THEN
        CALL Q6G0S(PLAB,IQ1,IQB1,IQ2,IQB2,IQ3,IQB3,IW,IHRN,OUT)
      ELSE
        CALL Q6G1S(PLAB,IQ1,IQB1,IQ2,IQB2,IQ3,IQB3,IW,IHRN,OUT)
      END IF
*
      END
*
************************************************************************
* SUBROUTINE Q6G0S(PLAB,IQ1,IQB1,IQ2,IQB2,IQ3,IQB3,IW,IHRN,OUT(5))     *
*                  calculates the process qqb qqb qqb                  *
************************************************************************
* Input:  Array with 6 momenta. The fourth component is the energy.    *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:6)             *
*         IQ is the quark, IQB the anti-quark.                         *
*         IW is a logical OR which flavour combinations to evaluate    *
*         bit 0 = 1:  OUT(1)                                           *
*         bit 1 = 1:  OUT(2) etc                                       *
*         IHRN=1 indicates MC over helicity amplitudes.                *
************************************************************************
* Output: No spin and colour averaging, no bose factor.                *
*         Output in REAL*8 OUT(5)                                      *
*         1:  Three different flavours                                 *
*         2:  Q1 and Q2 are the same flavour                           *
*         3:  Q1 and Q3 are the same flavour                           *
*         4:  Q2 and Q3 are the same flavour                           *
*         5:  All flavours are the same                                *
************************************************************************
      SUBROUTINE Q6G0S(PLAB,IQ1,IQB1,IQ2,IQB2,IQ3,IQB3,IW,IHRN,OUT)
      PARAMETER(NC=3,COL3=NC**3/16.0,COL2=NC**2/16.0,COL=NC/16.0)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),RN
      DIMENSION P(0:3,1:6)
      REAL*8 OUT(5)
      COMMON /WHICH/ IDO(5)
      DIMENSION ITABQ6(15,36),COLMAT(6,6),IDIAG(36)
      DIMENSION IQ6(5,36),ZQ6(10,36),MAPCOL(4,36),ZQ(6)
      DIMENSION ITRANS(6)
      SAVE INIT,COLMAT,/WHICH/,ITABQ6,IQ6,MAPCOL
      DATA COLMAT/ COL3, COL2, COL2, COL2, COL , COL ,
     .             COL2, COL3, COL , COL , COL2, COL2,
     .             COL2, COL , COL3, COL , COL2, COL2,
     .             COL2, COL , COL , COL3, COL2, COL2,
     .             COL , COL2, COL2, COL2, COL3, COL ,
     .             COL , COL2, COL2, COL2, COL , COL3/
      DATA INIT/1/
*
      IF (INIT.EQ.1) THEN
        CALL Q6TABS(ITABQ6)
        CALL Q6COL(ITABQ6,IQ6,MAPCOL)
        DO 1 I=1,10
          DO 1 J=1,36
            ZQ6(I,J)=(0.0,0.0)
    1   CONTINUE
        INIT=0
      END IF
*
* Transfrom Vectors, normalize vectors according to energies.
*
      ITRANS(1)=IQ1
      ITRANS(2)=IQB1
      ITRANS(3)=IQ2
      ITRANS(4)=IQB2
      ITRANS(5)=IQ3
      ITRANS(6)=IQB3
      TOTEN=0.0
      DO 5 I=1,6
        TOTEN=TOTEN+REAL(PLAB(4,ITRANS(I)))
    5 CONTINUE
      TOTEN=TOTEN/6.0
      DO 6 I=1,6
        DO 6 NU=1,4
          MU=NU
          IF(MU.EQ.4) MU=0
          P(MU,I)=REAL(PLAB(NU,ITRANS(I)))/TOTEN
          IF (ITRANS(I).LT.3) P(MU,I)=-P(MU,I)
    6 CONTINUE
      FACTOR=1.0/TOTEN**4
*
* Which processes have to be calculated
*
      IDOIT=IW
      DO 7 I=1,5
        IDO(I)=0
        IF (IDOIT/2*2.NE.IDOIT) THEN
          IDO(I)=1
          IDOIT=IDOIT-1
        END IF
        IDOIT=IDOIT/2
    7 CONTINUE
*
* Don't need to calculate all diagrams when IDO(5)=0
*
      IF (IDO(5).EQ.0) THEN
        DO 8 I=1,36
          IDIAG(I)=0
          DO 8 J=1,5
            IDIAG(I)=IDIAG(I)+IDO(J)*IQ6(J,I)
    8   CONTINUE
      END IF
*
* Init spinors and calculate the kinematics
*
      CALL SETGSP(P,6,0)
      CALL PRFILL(P,6)
*
* Monte Carlo over helicities?
*
      IF (IHRN.EQ.1) THEN
        IHELR=1+INT(10.0*RN(1))
      ELSE
        IHELR=0
      END IF
*
      DO 30 I=1,36
        IF( (IDIAG(I).NE.0).OR.(IDO(5).EQ.1) )
     .              CALL Q6DIAG(ZQ6(1,I),ITABQ6(1,I),IHELR)
   30 CONTINUE
*
* Transform colour basis to 6x6 base of delta functions
*
      DO 40 I=1,5
        OUT(I)=0D0
        IF (IDO(I).EQ.1) THEN
          DO 142 K=1,10
            IF ((IHELR.EQ.K).OR.(IHRN.EQ.0)) THEN
              DO 42 L=1,6
                ZQ(L)=(0.0,0.0)
   42         CONTINUE
              DO 43 J=1,36
                IF (IQ6(I,J).EQ.1) THEN
                  ZQ(MAPCOL(1,J))=ZQ(MAPCOL(1,J))+ZQ6(K,J)
                  ZQ(MAPCOL(2,J))=ZQ(MAPCOL(2,J))-ZQ6(K,J)/NC
                  ZQ(MAPCOL(3,J))=ZQ(MAPCOL(3,J))-ZQ6(K,J)/NC
                  ZQ(MAPCOL(4,J))=ZQ(MAPCOL(4,J))+ZQ6(K,J)/NC/NC
                END IF
   43         CONTINUE
              OUT(I)=OUT(I)+SQUARE(COLMAT,6,ZQ,6)
            END IF
  142     CONTINUE
        END IF
*
* Remember to add factor 2 from helicity conjugates
* and factor 10 in case of monte carlo over helicities.
*
        OUT(I)=OUT(I)*FACTOR*2.0
        IF (IHRN.EQ.1) OUT(I)=OUT(I)*10.0
   40 CONTINUE
*
      END
*
************************************************************************
* Q6DIAG calculates the two diagrams for permutation I of the quarks.  *
* INPUT: ITAB:   1:6  permutation of quarks and anti-quarks            *
*                7:14 where to store the results in ZQ6(10)            *
*                15   sign of permutation                              *
************************************************************************
      SUBROUTINE Q6DIAG(ZQ6,ITABQ6,IHELR)
      PARAMETER(NUP=10)
      IMPLICIT REAL(A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZQ6(20),ITABQ6(15)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      SAVE /DOTPR/,/PROPS/
*
      ZPM1(I1,I2,I3,I4,I5,I6)=
     . +Z1*ZD(I1,I3)*(ZD(I5,I1)*ZUD(I2,I1)+ZD(I5,I3)*ZUD(I2,I3))
     .                        *ZUD(I6,I4)
      ZMP1(I1,I2,I3,I4,I5,I6)=
     . +Z1*ZUD(I2,I3)*(ZD(I1,I2)*ZUD(I6,I2)+ZD(I1,I3)*ZUD(I6,I3))
     .                        *ZD(I5,I4)
      ZDRIE(I1,I2,I3,I4,I5,I6)=
     . +Z2*(  +ZD(I1,I5)*ZD(I1,I3)*ZUD(I1,I2)*ZUD(I6,I4)
     .        +ZD(I1,I5)*ZD(I3,I5)*ZUD(I2,I4)*ZUD(I6,I5)
     .        +ZD(I1,I3)*ZD(I3,I5)*ZUD(I2,I6)*ZUD(I4,I3)   )
*
      I1=ITABQ6(1)
      I2=ITABQ6(2)
      I3=ITABQ6(3)
      I4=ITABQ6(4)
      I5=ITABQ6(5)
      I6=ITABQ6(6)
      IH1=ITABQ6(7)
      IH2=ITABQ6(8)
      IH3=ITABQ6(9)
      IH4=ITABQ6(10)
      IH5=ITABQ6(11)
      IH6=ITABQ6(12)
      IH7=ITABQ6(13)
      IH8=ITABQ6(14)
      IF ( ((IH1.NE.IHELR).AND.(IHELR.GT.0)) .OR.(IH1.GT.10)) IH1=0
      IF ( ((IH2.NE.IHELR).AND.(IHELR.GT.0)) .OR.(IH2.GT.10)) IH2=0
      IF ( ((IH3.NE.IHELR).AND.(IHELR.GT.0)) .OR.(IH3.GT.10)) IH3=0
      IF ( ((IH4.NE.IHELR).AND.(IHELR.GT.0)) .OR.(IH4.GT.10)) IH4=0
      IF ( ((IH5.NE.IHELR).AND.(IHELR.GT.0)) .OR.(IH5.GT.10)) IH5=0
      IF ( ((IH6.NE.IHELR).AND.(IHELR.GT.0)) .OR.(IH6.GT.10)) IH6=0
      IF ( ((IH7.NE.IHELR).AND.(IHELR.GT.0)) .OR.(IH7.GT.10)) IH7=0
      IF ( ((IH8.NE.IHELR).AND.(IHELR.GT.0)) .OR.(IH8.GT.10)) IH8=0
      IF ( (IH1+IH2+IH3+IH4+IH5+IH6+IH7+IH8).EQ.0) RETURN
      SIGN=FLOAT(ITABQ6(15))
      Z1=(0.0,-4.0)*SIGN/PRO3(I1,I2,I3)/PRO2(I1,I2)/PRO2(I5,I6)
      IF (IH1.NE.0) ZQ6(IH1)=ZPM1(I1,I2,I3,I4,I5,I6)
      IF (IH2.NE.0) ZQ6(IH2)=ZPM1(I1,I2,I3,I4,I6,I5)
      IF (IH3.NE.0) ZQ6(IH3)=ZMP1(I1,I2,I3,I4,I5,I6)
      IF (IH4.NE.0) ZQ6(IH4)=ZMP1(I1,I2,I3,I4,I6,I5)
      IF (IH5.NE.0) ZQ6(IH5)=ZPM1(I2,I1,I3,I4,I5,I6)
      IF (IH6.NE.0) ZQ6(IH6)=ZPM1(I2,I1,I3,I4,I6,I5)
      IF (IH7.NE.0) ZQ6(IH7)=ZMP1(I2,I1,I3,I4,I5,I6)
      IF (IH8.NE.0) ZQ6(IH8)=ZMP1(I2,I1,I3,I4,I6,I5)
      IF (I3.EQ.3) THEN
        Z2=(0.0,-4.0)*SIGN/PRO2(I3,I4)/PRO2(I1,I2)/PRO2(I5,I6)
        IF (IH1.NE.0) ZQ6(IH1)=ZQ6(IH1)+ZDRIE(I1,I2,I3,I4,I5,I6)
        IF (IH2.NE.0) ZQ6(IH2)=ZQ6(IH2)+ZDRIE(I1,I2,I3,I4,I6,I5)
        IF (IH3.NE.0) ZQ6(IH3)=ZQ6(IH3)+ZDRIE(I1,I2,I4,I3,I5,I6)
        IF (IH4.NE.0) ZQ6(IH4)=ZQ6(IH4)+ZDRIE(I1,I2,I4,I3,I6,I5)
        IF (IH5.NE.0) ZQ6(IH5)=ZQ6(IH5)+ZDRIE(I2,I1,I3,I4,I5,I6)
        IF (IH6.NE.0) ZQ6(IH6)=ZQ6(IH6)+ZDRIE(I2,I1,I3,I4,I6,I5)
        IF (IH7.NE.0) ZQ6(IH7)=ZQ6(IH7)+ZDRIE(I2,I1,I4,I3,I5,I6)
        IF (IH8.NE.0) ZQ6(IH8)=ZQ6(IH8)+ZDRIE(I2,I1,I4,I3,I6,I5)
      END IF
*
      END
*
************************************************************************
* SUBROUTINE Q6TABS(ITABQ6) constructs table for the six quark process.*
* The table is used to call the different permutation/hels. of quarks  *
*                                                                      *
* ITABQ6( 1: 6,*)  =  permutation of quarks.                           *
* ITABQ6( 7:14,*)  =  assignment of helicity configurations.           *
* ITABQ6(15   ,*)  =  sign of quark permutation.                       *
************************************************************************
      SUBROUTINE Q6TABS(ITABQ6)
      DIMENSION ITABQ6(15,36)
      DIMENSION IHELS(6,20),IPERM1(4,6),IPERM2(4,6),IH(6,8),IHEL(6)
      SAVE IHELS,IPERM1,IPERM2,IH
      DATA IHELS/ 0,0,0,1,1,1,  0,0,1,0,1,1,  0,0,1,1,0,1,
     .            0,0,1,1,1,0,  0,1,0,0,1,1,  0,1,0,1,0,1,
     .            0,1,0,1,1,0,  0,1,1,0,0,1,  0,1,1,0,1,0,
     .            0,1,1,1,0,0,  1,0,0,0,1,1,  1,0,0,1,0,1,
     .            1,0,0,1,1,0,  1,0,1,0,0,1,  1,0,1,0,1,0,
     .            1,0,1,1,0,0,  1,1,0,0,0,1,  1,1,0,0,1,0,
     .            1,1,0,1,0,0,  1,1,1,0,0,0/
      DATA IPERM1 / 1,3,5,1, 1,5,3,-1, 3,1,5,-1,
     .              3,5,1,1, 5,3,1,-1, 5,1,3,1/
      DATA IPERM2 / 2,4,6,1, 2,6,4,-1, 4,2,6,-1,
     .              4,6,2,1, 6,4,2,-1, 6,2,4,1/
      DATA IH / 0,1,0,1,0,1, 0,1,0,1,1,0, 0,1,1,0,0,1, 0,1,1,0,1,0,
     .          1,0,0,1,0,1, 1,0,0,1,1,0, 1,0,1,0,0,1, 1,0,1,0,1,0/
*
      ITEL=1
      DO 10 IP1=1,6
        DO 10 IP2=1,6
          ITABQ6(15,ITEL)=IPERM1(4,IP1)*IPERM2(4,IP2)
          ITABQ6(1,ITEL)=IPERM1(1,IP1)
          ITABQ6(3,ITEL)=IPERM1(2,IP1)
          ITABQ6(5,ITEL)=IPERM1(3,IP1)
          ITABQ6(2,ITEL)=IPERM2(1,IP2)
          ITABQ6(4,ITEL)=IPERM2(2,IP2)
          ITABQ6(6,ITEL)=IPERM2(3,IP2)
          DO 20 I=1,8
            DO 30 J=1,6
              IHEL(ITABQ6(J,ITEL))=IH(J,I)
   30       CONTINUE
            J=0
   40       J=J+1
            IOK=0
            DO 50 K=1,6
              IF (IHELS(K,J).NE.IHEL(K)) IOK=1
   50       CONTINUE
            IF (IOK.EQ.0) ITABQ6(6+I,ITEL)=J
            IF (IOK.EQ.1) GOTO 40
   20     CONTINUE
          ITEL=ITEL+1
   10 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE Q6COL(ITABQ6,IQ6,MAPCOL) initialises the colourmapping    *
* for the 6 quark process. The calculated diagrams are mapped to a     *
* colour base consisting off:                                          *
*               1: d(i1,i2)*d(i3,i4)*d(i5,i6)                          *
*               2: d(i1,i4)*d(i3,i2)*d(i5,i6)                          *
*               3: d(i1,i6)*d(i3,i4)*d(i6,i2)                          *
*               4: d(i1,i2)*d(i3,i6)*d(i5,i4)                          *
*               5: d(i1,i4)*d(i3,i6)*d(i5,i2)                          *
*               6: d(i1,i6)*d(i3,i2)*d(i5,i4)                          *
*                                                                      *
* IQ6(5,36) contains a boolean list of quark permutations              *
* for squaring each of the 5 possible flavour combinations.            *
************************************************************************
      SUBROUTINE Q6COL(ITABQ6,IQ6,MAPCOL)
      IMPLICIT REAL(A-H,O-Y)
      DIMENSION ITABQ6(15,36),IQ6(5,36),MAPCOL(4,36),ITAB(12,6)
      DIMENSION IOK(3)
      SAVE ITAB
      DATA ITAB/1,2,3,4,1,2,5,6,3,4,5,6,
     .          1,4,3,2,1,4,5,6,3,2,5,6,
     .          1,6,3,4,1,6,5,2,3,4,5,2,
     .          1,2,3,6,1,2,5,4,3,6,5,4,
     .          1,4,3,6,1,4,5,2,3,6,5,2,
     .          1,6,3,2,1,6,5,4,3,2,5,4/
*
* Construct mapping between calculated colour structures
* and the ones used to determine M^2.
*
      DO 10 I=1,36
        DO 20 J=1,6
          DO 25 K=1,11,2
            K1=ITAB(K,J)
            K2=ITAB(K+1,J)
            K3=ITAB(1+MOD(K+1,12),J)
            K4=ITAB(1+MOD(K+2,12),J)
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(6,I)).AND.
     .         (K3.EQ.ITABQ6(3,I)).AND.(K4.EQ.ITABQ6(2,I)))
     .        MAPCOL(1,I)=J
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(2,I)).AND.
     .         (K3.EQ.ITABQ6(3,I)).AND.(K4.EQ.ITABQ6(6,I)))
     .        MAPCOL(2,I)=J
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(4,I)).AND.
     .         (K3.EQ.ITABQ6(3,I)).AND.(K4.EQ.ITABQ6(2,I)))
     .        MAPCOL(3,I)=J
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(2,I)).AND.
     .         (K3.EQ.ITABQ6(3,I)).AND.(K4.EQ.ITABQ6(4,I)))
     .        MAPCOL(4,I)=J
   25     CONTINUE
   20   CONTINUE
   10 CONTINUE
*
* Determine which permutation contribute to which flavour combination.
*
      DO 30 I=1,36
        IOK( (ITABQ6(1,I)+1)/2 ) =  ITABQ6(2,I)-1-ITABQ6(1,I)
        IOK( (ITABQ6(3,I)+1)/2 ) =  ITABQ6(4,I)-1-ITABQ6(3,I)
        IOK( (ITABQ6(5,I)+1)/2 ) =  ITABQ6(6,I)-1-ITABQ6(5,I)
        IF ((IOK(1).EQ.0) .AND. (IOK(2).EQ.0) .AND. (IOK(3).EQ.0)) THEN
          IQ6(1,I)=1
        ELSE
          IQ6(1,I)=0
        END IF
        IF (IOK(3).EQ.0) THEN
          IQ6(2,I)=1
        ELSE
          IQ6(2,I)=0
        END IF
        IF (IOK(2).EQ.0) THEN
          IQ6(3,I)=1
        ELSE
          IQ6(3,I)=0
        END IF
        IF (IOK(1).EQ.0) THEN
          IQ6(4,I)=1
        ELSE
          IQ6(4,I)=0
        END IF
        IQ6(5,I)=1
   30 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE Q6G1S(PLAB,IQ1,IQB1,IQ2,IQB2,IQ3,IQB3,IW,IHRN,OUT)        *
*                  calculates the process qqb qqb qqb g                *
************************************************************************
* input:  Array with 7 momenta. The fourth component is the energy.    *
*         All energies>0. Declaration REAL*8 PLAB(1:4,1:7)             *
*         IQ is the quark, IQB the anti-quark.                         *
*         IW is a logical OR which flavour combinations to evaluate.   *
*         bit 0 = 1:  OUT(1)                                           *
*         bit 1 = 1:  OUT(2) etc                                       *
************************************************************************
* output: No spin and colour averaging, no bose factor.                *
*         Output in common REAL*8 /OUTQ6/ OUT(5)                       *
*         1:  Three different flavours                                 *
*         2:  Q1 and Q2 are the same flavour                           *
*         3:  Q1 and Q3 are the same flavour                           *
*         4:  Q2 and Q3 are the same flavour                           *
*         5:  All flavours are the same                                *
************************************************************************
      SUBROUTINE Q6G1S(PLAB,IQ1,IQB1,IQ2,IQB2,IQ3,IQB3,IW,IHRN,OUT)
      PARAMETER(NC=3)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      REAL*8 PLAB(4,*),RN
      DIMENSION P(0:3,1:8)
      REAL*8 OUT(5)
      COMMON /WHICH/ IDO(5)
      DIMENSION ITABQ6(15,36),COLMAT(18,18),IDIAG(36)
      DIMENSION IQ6(5,36),ZQ6G(7,20,36),MAPCOL(4,36,7),ZQ(18)
      DIMENSION ITRANS(7)
      SAVE INIT,ITABQ6,IQ6,MAPCOL,COLMAT,/WHICH/
      DATA INIT/1/
*
      IF (INIT.EQ.1) THEN
        CALL Q6GTAB(ITABQ6)
        CALL Q6GCOL(ITABQ6,IQ6,MAPCOL,COLMAT)
        DO 1 I=1,20
          DO 1 J=1,36
            DO 1 K=1,7
            ZQ6G(K,I,J)=(0.0,0.0)
    1   CONTINUE
        INIT=0
      END IF
*
* Transfrom Vectors, normalize vectors according to energies.
*
      ITRANS(1)=IQ1
      ITRANS(2)=IQB1
      ITRANS(3)=IQ2
      ITRANS(4)=IQB2
      ITRANS(5)=IQ3
      ITRANS(6)=IQB3
* find gluon
      IG=0
    4 IG=IG+1
      IF ((IG.EQ.IQ1).OR.(IG.EQ.IQB1).OR.(IG.EQ.IQ2).OR.
     .    (IG.EQ.IQB2).OR.(IG.EQ.IQ3).OR.(IG.EQ.IQB3)) GOTO 4
*
      ITRANS(7)=IG
      TOTEN=0.0
      DO 5 I=1,7
        TOTEN=TOTEN+REAL(PLAB(4,ITRANS(I)))
    5 CONTINUE
      TOTEN=TOTEN/7.0
      DO 6 I=1,7
        DO 6 NU=1,4
          MU=NU
          IF(MU.EQ.4) MU=0
          P(MU,I)=REAL(PLAB(NU,ITRANS(I)))/TOTEN
          IF (ITRANS(I).LT.3) P(MU,I)=-P(MU,I)
    6 CONTINUE
      FACTOR=1.0/TOTEN**6
*
* Which processes have to be calculated
*
      IDOIT=IW
      DO 7 I=1,5
        IDO(I)=0
        IF (IDOIT/2*2.NE.IDOIT) THEN
          IDO(I)=1
          IDOIT=IDOIT-1
        END IF
        IDOIT=IDOIT/2
    7 CONTINUE
*
* Don't need to calculate all diagrams when IDO(5)=0
*
      IF (IDO(5).EQ.0) THEN
        DO 8 I=1,36
          IDIAG(I)=0
          DO 8 J=1,5
            IDIAG(I)=IDIAG(I)+IDO(J)*IQ6(J,I)
    8   CONTINUE
      END IF
*
* Gauge freedom in vector 8
*
      P(1,8)=REAL(RN(1))
      P(2,8)=REAL(RN(2))
      P(3,8)=REAL(RN(3))
      P(0,8)=SQRT(P(1,8)**2+P(2,8)**2+P(3,8)**2)
*
* MC over helicities
*
      IF (IHRN.EQ.1) THEN
        IHELR=1+INT(20.0*RN(1))
      ELSE
        IHELR=0
      END IF
*
* Init spinors and calculate the kinematics
*
      CALL PRFILL(P,7)
      CALL SETGSP(P,8,0)
      CALL Q6GDIA(ZQ6G(1,1,1),ITABQ6(1,1),IHELR,1)
*
      DO 30 I=1,36
        IF( (IDIAG(I).NE.0).OR.(IDO(5).EQ.1) )
     .         CALL Q6GDIA(ZQ6G(1,1,I),ITABQ6(1,I),IHELR,0)
   30 CONTINUE
*
* Transform colour basis to 18x18 base of delta functions
*
      DO 40 I=1,5
        OUT(I)=0D0
        IF (IDO(I).EQ.1) THEN
          DO 42 K=1,20
            IF ( (IHELR.EQ.K).OR.(IHRN.EQ.0) ) THEN
              DO 52 L=1,18
                ZQ(L)=(0.0,0.0)
   52         CONTINUE
              DO 43 J=1,36
                IF (IQ6(I,J).EQ.1) THEN
                 DO 46 L=1,7
                   ZQ(MAPCOL(1,J,L))=ZQ(MAPCOL(1,J,L))+ZQ6G(L,K,J)
                   ZQ(MAPCOL(2,J,L))=ZQ(MAPCOL(2,J,L))-ZQ6G(L,K,J)/NC
                   ZQ(MAPCOL(3,J,L))=ZQ(MAPCOL(3,J,L))-ZQ6G(L,K,J)/NC
                   ZQ(MAPCOL(4,J,L))=ZQ(MAPCOL(4,J,L))+ZQ6G(L,K,J)/NC/NC
   46            CONTINUE
                END IF
   43         CONTINUE
              OUT(I)=OUT(I)+SQUARE(COLMAT,18,ZQ,18)
            END IF
   42     CONTINUE
*
* Remember to add factor 2 from helicity conjugates of gluon
* If MC over helicities add factor 20
*
          OUT(I)=OUT(I)*FACTOR*2.0
          IF (IHRN.EQ.1) OUT(I)=OUT(I)*20.0
        END IF
   40 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE Q6GDIA calculates diagrams for permutation I of the quarks*
*                                                                      *
* INPUT: ITAB: 1:6  permutation of quarks and anti-quarks              *
*              7:14 where to store the results in ZQ6G(20)             *
*              15   sign of permutation                                *
*                                                                      *
* Return results in ZQ6G structure                                     *
*    First index is the colour structure                               *
*        1: (A1 A) (A B) (B)                                           *
*        2: (A A1) (A B) (B)                                           *
*        3: (A) (A1 A B) (B)                                           *
*        4: (A) (A A1 B) (B)                                           *
*        5: (A) (A B A1) (B)                                           *
*        6: (A) (A B) (A1 B)                                           *
*        7: (A) (A B) (B A1)                                           *
*    Second index is helicity structure (+-+-+-) ... (-+-+-+)          *
*                                                                      *
* If INIT=1 initialises all the gluon-quark-antiquark verteces in      *
* ZDRIE(IAD,IB,xx,II,IJ) to gain some speed, with                      *
*    xx=1  helicity = +-, colour = (a1 a)                              *
*    xx=2  helicity = +-, colour = (a a1)                              *
*    xx=3  helicity = -+, colour = (a1 a)                              *
*    xx=4  helicity = -+, colour = (a a1)                              *
************************************************************************
      SUBROUTINE Q6GDIA(ZQ6G,ITABQ6,IHELR,INIT)
      PARAMETER(NGUP=10)
      IMPLICIT REAL(A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION ZQ6G(7,20),ITABQ6(15)
      DIMENSION ZHPMPM(2,2),ZHPMMP(2,2),ZHMPPM(2,2),ZHMPMP(2,2)
      DIMENSION ZDRIE(2,2,4,6,6)
      COMMON /SPING/ ZKO(2,NGUP),ZKOD(2,NGUP),ZHVEC(2,2,2,NGUP)
      COMMON /DOTPR/ ZUD(NGUP,NGUP),ZD(NGUP,NGUP)
      COMMON /PROPS/ PRO2(NGUP,NGUP),PRO3(NGUP,NGUP,NGUP),
     .               PRO23(NGUP,NGUP,NGUP)
      SAVE /SPING/,/DOTPR/,/PROPS/
*
      IF (INIT.EQ.1) THEN
        DO 5 II=1,5,2
         DO 5 IJ=2,6,2
           ZFAC1=1.0/PRO2(II,7)/PRO3(II,IJ,7)
           ZFAC2=1.0/PRO2(IJ,7)/PRO3(II,IJ,7)
           ZFAC3=1.0/PRO2(II,IJ)/PRO3(II,IJ,7)
           DO 6 IAD=1,2
            DO 6 IB=1,2
              Z1=-ZFAC1*ZD(II,7)*(ZUD(II,8)*ZKOD(IAD,II)
     .           +ZUD(7,8)*ZKOD(IAD,7))*ZKO(IB,IJ)
              Z2=ZFAC2*ZD(IJ,7)*ZUD(IJ,8)*ZKOD(IAD,II)*ZKO(IB,IJ)
              Z3=ZFAC3*(+0.5*ZD(II,7)*ZUD(IJ,8)*
     .           (ZKOD(IAD,II)*ZKO(IB,II)+ZKOD(IAD,IJ)*ZKO(IB,IJ)
     .            -ZKOD(IAD,7)*ZKO(IB,7))
     .            +ZD(II,7)*ZUD(IJ,7)*ZKOD(IAD,7)*ZKO(IB,8)
     .            -(ZD(II,7)*ZUD(II,8)+ZD(IJ,7)*ZUD(IJ,8))
     .            *ZKOD(IAD,II)*ZKO(IB,IJ))
              ZDRIE(IAD,IB,1,II,IJ)=Z1+Z3
              ZDRIE(IAD,IB,2,II,IJ)=Z2-Z3
              Z1=-ZFAC1*ZD(II,7)*ZUD(II,8)*ZKO(IB,II)*ZKOD(IAD,IJ)
              Z2=ZFAC2*ZD(IJ,7)*(ZUD(IJ,8)*ZKOD(IAD,IJ)
     .           +ZUD(7,8)*ZKOD(IAD,7))*ZKO(IB,II)
              Z3=ZFAC3*(0.5*ZD(IJ,7)*ZUD(II,8)*
     .           (ZKOD(IAD,IJ)*ZKO(IB,IJ)+ZKOD(IAD,II)*ZKO(IB,II)
     .           -ZKOD(IAD,7)*ZKO(IB,7))
     .           +ZD(IJ,7)*ZUD(II,7)*ZKOD(IAD,7)*ZKO(IB,8)
     .           -(ZD(IJ,7)*ZUD(IJ,8)+ZD(II,7)*ZUD(II,8))
     .           *ZKOD(IAD,IJ)*ZKO(IB,II))
              ZDRIE(IAD,IB,3,II,IJ)=Z1+Z3
              ZDRIE(IAD,IB,4,II,IJ)=Z2-Z3
    6      CONTINUE
    5   CONTINUE
        RETURN
      END IF
*
      II=ITABQ6(1)
      IJ=ITABQ6(2)
      IK=ITABQ6(3)
      IL=ITABQ6(4)
      IM=ITABQ6(5)
      IN=ITABQ6(6)
      IH1=ITABQ6(7)
      IH2=ITABQ6(8)
      IH3=ITABQ6(9)
      IH4=ITABQ6(10)
      IH5=ITABQ6(11)
      IH6=ITABQ6(12)
      IH7=ITABQ6(13)
      IH8=ITABQ6(14)
*
      IF (IHELR.GT.0) THEN
        IF (IH1.NE.IHELR) IH1=0
        IF (IH2.NE.IHELR) IH2=0
        IF (IH3.NE.IHELR) IH3=0
        IF (IH4.NE.IHELR) IH4=0
        IF (IH5.NE.IHELR) IH5=0
        IF (IH6.NE.IHELR) IH6=0
        IF (IH7.NE.IHELR) IH7=0
        IF (IH8.NE.IHELR) IH8=0
        IF ((IH1+IH2+IH3+IH4+IH5+IH6+IH7+IH8).EQ.0) RETURN
      END IF
      ZSIGN=FLOAT(ITABQ6(15))*SQRT(2.0)/ZUD(7,8)
*
* Diagrams A1-A3, Colour structures 1:(A1 A)(A B)(B), 2:(A A1) (A B) (B)
*
      ZFAC=ZSIGN*(0.0,2.0)/PRO2(IM,IN)/PRO3(IL,IM,IN)
      DO 10 IAD=1,2
        DO 10 IB=1,2
          ZHPMPM(IAD,IB)=ZFAC*ZKOD(IAD,IK)*ZUD(IL,IN)*
     .          (ZD(IL,IM)*ZKO(IB,IL)+ZD(IN,IM)*ZKO(IB,IN))
          ZHPMMP(IAD,IB)=ZFAC*ZKOD(IAD,IK)*ZUD(IL,IM)*
     .          (ZD(IL,IN)*ZKO(IB,IL)+ZD(IM,IN)*ZKO(IB,IM))
          ZHMPPM(IAD,IB)=ZFAC*ZKO(IB,IK)*ZD(IL,IM)*
     .          (ZUD(IL,IN)*ZKOD(IAD,IL)+ZUD(IM,IN)*ZKOD(IAD,IM))
          ZHMPMP(IAD,IB)=ZFAC*ZKO(IB,IK)*ZD(IL,IN)*
     .          (ZUD(IL,IM)*ZKOD(IAD,IL)+ZUD(IN,IM)*ZKOD(IAD,IN))
   10 CONTINUE
      DO 30 I=1,2
        IF (IH1.NE.0)
     .     ZQ6G(I,IH1)=2.0*ZMUL(ZDRIE(1,1,I,II,IJ),ZHPMPM(1,1))
        IF (IH2.NE.0)
     .     ZQ6G(I,IH2)=2.0*ZMUL(ZDRIE(1,1,I,II,IJ),ZHPMMP(1,1))
        IF (IH3.NE.0)
     .     ZQ6G(I,IH3)=2.0*ZMUL(ZDRIE(1,1,I,II,IJ),ZHMPPM(1,1))
        IF (IH4.NE.0)
     .     ZQ6G(I,IH4)=2.0*ZMUL(ZDRIE(1,1,I,II,IJ),ZHMPMP(1,1))
        IF (IH5.NE.0)
     .     ZQ6G(I,IH5)=2.0*ZMUL(ZDRIE(1,1,I+2,II,IJ),ZHPMPM(1,1))
        IF (IH6.NE.0)
     .     ZQ6G(I,IH6)=2.0*ZMUL(ZDRIE(1,1,I+2,II,IJ),ZHPMMP(1,1))
        IF (IH7.NE.0)
     .     ZQ6G(I,IH7)=2.0*ZMUL(ZDRIE(1,1,I+2,II,IJ),ZHMPPM(1,1))
        IF (IH8.NE.0)
     .     ZQ6G(I,IH8)=2.0*ZMUL(ZDRIE(1,1,I+2,II,IJ),ZHMPMP(1,1))
   30 CONTINUE
*
* Diagram A4, Colour structure 3 = (A)(A1 A B)(B)
*
      ZFAC=ZSIGN*(0.0,-4.0)/PRO2(II,IJ)/PRO2(IK,7)
     .     /PRO3(IL,IM,IN)/PRO2(IM,IN)
      IF (IH1.NE.0) ZQ6G(3,IH1)=
     .     ZFAC*ZD(IK,7)*(ZUD(IK,8)*ZD(IK,II)+ZUD(7,8)*ZD(7,II))*
     .     (ZD(IL,IM)*ZUD(IL,IJ)+ZD(IN,IM)*ZUD(IN,IJ))*ZUD(IL,IN)
      IF (IH2.NE.0) ZQ6G(3,IH2)=
     .     ZFAC*ZD(IK,7)*(ZUD(IK,8)*ZD(IK,II)+ZUD(7,8)*ZD(7,II))*
     .     (ZD(IL,IN)*ZUD(IL,IJ)+ZD(IM,IN)*ZUD(IM,IJ))*ZUD(IL,IM)
      IF (IH3.NE.0) ZQ6G(3,IH3)=
     .     ZFAC*ZUD(IK,8)*ZD(IK,7)*ZUD(IK,IJ)*
     .     (ZD(IL,II)*ZUD(IL,IN)+ZD(IM,II)*ZUD(IM,IN))*ZD(IL,IM)
      IF (IH4.NE.0) ZQ6G(3,IH4)=
     .     ZFAC*ZUD(IK,8)*ZD(IK,7)*ZUD(IK,IJ)*
     .     (ZD(IL,II)*ZUD(IL,IM)+ZD(IN,II)*ZUD(IN,IM))*ZD(IL,IN)
      IF (IH5.NE.0) ZQ6G(3,IH5)=
     .     ZFAC*ZD(IK,7)*(ZUD(IK,8)*ZD(IK,IJ)+ZUD(7,8)*ZD(7,IJ))*
     .     (ZD(IL,IM)*ZUD(IL,II)+ZD(IN,IM)*ZUD(IN,II))*ZUD(IL,IN)
      IF (IH6.NE.0) ZQ6G(3,IH6)=
     .     ZFAC*ZD(IK,7)*(ZUD(IK,8)*ZD(IK,IJ)+ZUD(7,8)*ZD(7,IJ))*
     .     (ZD(IL,IN)*ZUD(IL,II)+ZD(IM,IN)*ZUD(IM,II))*ZUD(IL,IM)
      IF (IH7.NE.0) ZQ6G(3,IH7)=
     .     ZFAC*ZUD(IK,8)*ZD(IK,7)*ZUD(IK,II)*
     .     (ZD(IL,IJ)*ZUD(IL,IN)+ZD(IM,IJ)*ZUD(IM,IN))*ZD(IL,IM)
      IF (IH8.NE.0) ZQ6G(3,IH8)=
     .     ZFAC*ZUD(IK,8)*ZD(IK,7)*ZUD(IK,II)*
     .     (ZD(IL,IJ)*ZUD(IL,IM)+ZD(IN,IJ)*ZUD(IN,IM))*ZD(IL,IN)
*
* Diagram A5, Colour structure 4 = (A)(A A1 B)(B)
*
      ZFAC=ZSIGN*(0.0,-4.0)/PRO2(II,IJ)/PRO3(II,IJ,IK)
     .     /PRO3(IL,IM,IN)/PRO2(IM,IN)
      IF (IH1.NE.0) ZQ6G(4,IH1)=
     .     ZD(IK,II)*(ZD(IK,7)*ZUD(IK,IJ)+ZD(II,7)*ZUD(II,IJ))*
     .     ZFAC*(ZUD(IL,8)*ZD(IL,IM)+ZUD(IN,8)*ZD(IN,IM))*ZUD(IL,IN)
      IF (IH2.NE.0) ZQ6G(4,IH2)=
     .     ZD(IK,II)*(ZD(IK,7)*ZUD(IK,IJ)+ZD(II,7)*ZUD(II,IJ))*
     .     ZFAC*(ZUD(IL,8)*ZD(IL,IN)+ZUD(IM,8)*ZD(IM,IN))*ZUD(IL,IM)
      IF (IH3.NE.0) ZQ6G(4,IH3)=
     .     ZUD(IK,IJ)*(ZUD(IK,8)*ZD(IK,II)+ZUD(IJ,8)*ZD(IJ,II))*
     .     ZFAC*(ZD(IL,7)*ZUD(IL,IN)+ZD(IM,7)*ZUD(IM,IN))*ZD(IL,IM)
      IF (IH4.NE.0) ZQ6G(4,IH4)=
     .     ZUD(IK,IJ)*(ZUD(IK,8)*ZD(IK,II)+ZUD(IJ,8)*ZD(IJ,II))*
     .     ZFAC*(ZD(IL,7)*ZUD(IL,IM)+ZD(IN,7)*ZUD(IN,IM))*ZD(IL,IN)
      IF (IH5.NE.0) ZQ6G(4,IH5)=
     .     ZD(IK,IJ)*(ZD(IK,7)*ZUD(IK,II)+ZD(IJ,7)*ZUD(IJ,II))*
     .     ZFAC*(ZUD(IL,8)*ZD(IL,IM)+ZUD(IN,8)*ZD(IN,IM))*ZUD(IL,IN)
      IF (IH6.NE.0) ZQ6G(4,IH6)=
     .     ZD(IK,IJ)*(ZD(IK,7)*ZUD(IK,II)+ZD(IJ,7)*ZUD(IJ,II))*
     .     ZFAC*(ZUD(IL,8)*ZD(IL,IN)+ZUD(IM,8)*ZD(IM,IN))*ZUD(IL,IM)
      IF (IH7.NE.0) ZQ6G(4,IH7)=
     .     ZUD(IK,II)*(ZUD(IK,8)*ZD(IK,IJ)+ZUD(II,8)*ZD(II,IJ))*
     .     ZFAC*(ZD(IL,7)*ZUD(IL,IN)+ZD(IM,7)*ZUD(IM,IN))*ZD(IL,IM)
      IF (IH8.NE.0) ZQ6G(4,IH8)=
     .     ZUD(IK,II)*(ZUD(IK,8)*ZD(IK,IJ)+ZUD(II,8)*ZD(II,IJ))*
     .     ZFAC*(ZD(IL,7)*ZUD(IL,IM)+ZD(IN,7)*ZUD(IN,IM))*ZD(IL,IN)
*
* Diagram A6, Colour structure 5 = (A)(A B A1)(B)
*
      ZFAC=ZSIGN*(0.0,-4.0)/PRO2(II,IJ)/PRO3(II,IJ,IK)
     .     /PRO2(IL,7)/PRO2(IM,IN)
      IF (IH1.NE.0) ZQ6G(5,IH1)=
     .     ZD(IK,II)*(ZD(IK,IM)*ZUD(IK,IJ)+ZD(II,IM)*ZUD(II,IJ))*
     .     ZFAC*ZD(IL,7)*ZUD(IL,IN)*ZUD(IL,8)
      IF (IH2.NE.0) ZQ6G(5,IH2)=
     .     ZD(IK,II)*(ZD(IK,IN)*ZUD(IK,IJ)+ZD(II,IN)*ZUD(II,IJ))*
     .     ZFAC*ZD(IL,7)*ZUD(IL,IM)*ZUD(IL,8)
      IF (IH3.NE.0) ZQ6G(5,IH3)=
     .     ZUD(IK,IJ)*(ZD(IK,II)*ZUD(IK,IN)+ZD(IJ,II)*ZUD(IJ,IN))
     .     *ZFAC*(ZD(IL,IM)*ZUD(IL,8)+ZD(7,IM)*ZUD(7,8))*ZD(IL,7)
      IF (IH4.NE.0) ZQ6G(5,IH4)=
     .     ZUD(IK,IJ)*(ZD(IK,II)*ZUD(IK,IM)+ZD(IJ,II)*ZUD(IJ,IM))
     .     *ZFAC*(ZD(IL,IN)*ZUD(IL,8)+ZD(7,IN)*ZUD(7,8))*ZD(IL,7)
      IF (IH5.NE.0) ZQ6G(5,IH5)=
     .     ZD(IK,IJ)*(ZD(IK,IM)*ZUD(IK,II)+ZD(IJ,IM)*ZUD(IJ,II))*
     .     ZFAC*ZD(IL,7)*ZUD(IL,IN)*ZUD(IL,8)
      IF (IH6.NE.0) ZQ6G(5,IH6)=
     .     ZD(IK,IJ)*(ZD(IK,IN)*ZUD(IK,II)+ZD(IJ,IN)*ZUD(IJ,II))*
     .     ZFAC*ZD(IL,7)*ZUD(IL,IM)*ZUD(IL,8)
      IF (IH7.NE.0) ZQ6G(5,IH7)=
     .     ZUD(IK,II)*(ZD(IK,IJ)*ZUD(IK,IN)+ZD(II,IJ)*ZUD(II,IN))
     .     *ZFAC*(ZD(IL,IM)*ZUD(IL,8)+ZD(7,IM)*ZUD(7,8))*ZD(IL,7)
      IF (IH8.NE.0) ZQ6G(5,IH8)=
     .     ZUD(IK,II)*(ZD(IK,IJ)*ZUD(IK,IM)+ZD(II,IJ)*ZUD(II,IM))
     .     *ZFAC*(ZD(IL,IN)*ZUD(IL,8)+ZD(7,IN)*ZUD(7,8))*ZD(IL,7)
*
* Diagrams A7-A9, Colour structures 6:(A)(A B)(A1 B), 7:(A) (A B) (B A1)
*
      ZFAC=ZSIGN*(0.0,-2.0)/PRO2(II,IJ)/PRO3(II,IJ,IK)
      DO 40 IAD=1,2
        DO 40 IB=1,2
          ZHPMPM(IAD,IB)=ZFAC*ZKO(IB,IL)*ZD(IK,II)*
     .          (ZUD(IK,IJ)*ZKOD(IAD,IK)+ZUD(II,IJ)*ZKOD(IAD,II))
          ZHPMMP(IAD,IB)=ZFAC*ZKOD(IAD,IL)*ZUD(IK,IJ)*
     .          (ZD(IK,II)*ZKO(IB,IK)+ZD(IJ,II)*ZKO(IB,IJ))
          ZHMPPM(IAD,IB)=ZFAC*ZKO(IB,IL)*ZD(IK,IJ)*
     .          (ZUD(IK,II)*ZKOD(IAD,IK)+ZUD(IJ,II)*ZKOD(IAD,IJ))
          ZHMPMP(IAD,IB)=ZFAC*ZKOD(IAD,IL)*ZUD(IK,II)*
     .          (ZD(IK,IJ)*ZKO(IB,IK)+ZD(II,IJ)*ZKO(IB,II))
   40 CONTINUE
      DO 60 I=1,2
        IF (IH1.NE.0)
     .    ZQ6G(I+5,IH1)=2.0*ZMUL(ZHPMPM(1,1),ZDRIE(1,1,I,IM,IN))
        IF (IH2.NE.0)
     .    ZQ6G(I+5,IH2)=2.0*ZMUL(ZHPMPM(1,1),ZDRIE(1,1,I+2,IM,IN))
        IF (IH3.NE.0)
     .    ZQ6G(I+5,IH3)=2.0*ZMUL(ZHPMMP(1,1),ZDRIE(1,1,I,IM,IN))
        IF (IH4.NE.0)
     .    ZQ6G(I+5,IH4)=2.0*ZMUL(ZHPMMP(1,1),ZDRIE(1,1,I+2,IM,IN))
        IF (IH5.NE.0)
     .    ZQ6G(I+5,IH5)=2.0*ZMUL(ZHMPPM(1,1),ZDRIE(1,1,I,IM,IN))
        IF (IH6.NE.0)
     .    ZQ6G(I+5,IH6)=2.0*ZMUL(ZHMPPM(1,1),ZDRIE(1,1,I+2,IM,IN))
        IF (IH7.NE.0)
     .    ZQ6G(I+5,IH7)=2.0*ZMUL(ZHMPMP(1,1),ZDRIE(1,1,I,IM,IN))
        IF (IH8.NE.0)
     .    ZQ6G(I+5,IH8)=2.0*ZMUL(ZHMPMP(1,1),ZDRIE(1,1,I+2,IM,IN))
   60 CONTINUE
*
* Diagrams B1-B9, Colour structures 3:(A)(A1 A B)(B), 5:(A)(A B A1)(B)
*
      ZFAC=(0.0,1.0)*ZSIGN/PRO2(II,IJ)/PRO2(IM,IN)
      ZFAC1=ZD(II,IM)*ZUD(IJ,IM)+ZD(II,IN)*ZUD(IJ,IN)
      ZFAC2=ZD(IJ,IM)*ZUD(II,IM)+ZD(IJ,IN)*ZUD(II,IN)
      ZFAC3=ZD(IM,II)*ZUD(IN,II)+ZD(IM,IJ)*ZUD(IN,IJ)
      ZFAC4=ZD(IN,II)*ZUD(IM,II)+ZD(IN,IJ)*ZUD(IM,IJ)
      DO 70 IAD=1,2
        DO 70 IB=1,2
          ZHELP=ZKOD(IAD,II)*ZKO(IB,II)+ZKOD(IAD,IJ)*ZKO(IB,IJ)
     .         -ZKOD(IAD,IM)*ZKO(IB,IM)-ZKOD(IAD,IN)*ZKO(IB,IN)
          ZHPMPM(IAD,IB)=ZFAC*(ZHELP*ZD(II,IM)*ZUD(IJ,IN)
     .                        +2.0*ZFAC1*ZKOD(IAD,IM)*ZKO(IB,IN)
     .                        -2.0*ZFAC3*ZKOD(IAD,II)*ZKO(IB,IJ))
          ZHPMMP(IAD,IB)=ZFAC*(ZHELP*ZD(II,IN)*ZUD(IJ,IM)
     .                        +2.0*ZFAC1*ZKOD(IAD,IN)*ZKO(IB,IM)
     .                        -2.0*ZFAC4*ZKOD(IAD,II)*ZKO(IB,IJ))
          ZHMPPM(IAD,IB)=ZFAC*(ZHELP*ZD(IJ,IM)*ZUD(II,IN)
     .                        +2.0*ZFAC2*ZKOD(IAD,IM)*ZKO(IB,IN)
     .                        -2.0*ZFAC3*ZKOD(IAD,IJ)*ZKO(IB,II))
          ZHMPMP(IAD,IB)=ZFAC*(ZHELP*ZD(IJ,IN)*ZUD(II,IM)
     .                        +2.0*ZFAC2*ZKOD(IAD,IN)*ZKO(IB,IM)
     .                        -2.0*ZFAC4*ZKOD(IAD,IJ)*ZKO(IB,II))
   70 CONTINUE
      DO 90 I=1,2
        IH=2*I+1
        IF (IH1.NE.0) ZQ6G(IH,IH1)=ZQ6G(IH,IH1)
     .              +2.0*ZMUL(ZHPMPM(1,1),ZDRIE(1,1,I,IK,IL))
        IF (IH2.NE.0) ZQ6G(IH,IH2)=ZQ6G(IH,IH2)
     .              +2.0*ZMUL(ZHPMMP(1,1),ZDRIE(1,1,I,IK,IL))
        IF (IH3.NE.0) ZQ6G(IH,IH3)=ZQ6G(IH,IH3)
     .              +2.0*ZMUL(ZHPMPM(1,1),ZDRIE(1,1,I+2,IK,IL))
        IF (IH4.NE.0) ZQ6G(IH,IH4)=ZQ6G(IH,IH4)
     .              +2.0*ZMUL(ZHPMMP(1,1),ZDRIE(1,1,I+2,IK,IL))
        IF (IH5.NE.0) ZQ6G(IH,IH5)=ZQ6G(IH,IH5)
     .              +2.0*ZMUL(ZHMPPM(1,1),ZDRIE(1,1,I,IK,IL))
        IF (IH6.NE.0) ZQ6G(IH,IH6)=ZQ6G(IH,IH6)
     .              +2.0*ZMUL(ZHMPMP(1,1),ZDRIE(1,1,I,IK,IL))
        IF (IH7.NE.0) ZQ6G(IH,IH7)=ZQ6G(IH,IH7)
     .              +2.0*ZMUL(ZHMPPM(1,1),ZDRIE(1,1,I+2,IK,IL))
        IF (IH8.NE.0) ZQ6G(IH,IH8)=ZQ6G(IH,IH8)
     .              +2.0*ZMUL(ZHMPMP(1,1),ZDRIE(1,1,I+2,IK,IL))
   90 CONTINUE
*
* Diagram B10,Colour structures 3-5: (A) (xxx) (B)
*
      ZFAC=(0.0,-2.0)/3.0*ZSIGN/PRO2(II,IJ)/PRO2(IK,IL)/PRO2(IM,IN)
      IF (IH1.NE.0) THEN
        Z1A=ZFAC*ZD(II,IK)*ZUD(IJ,IL)*ZD(IM,7)*ZUD(IN,8)
        Z1B=ZFAC*ZD(II,IM)*ZUD(IJ,IN)*ZD(IK,7)*ZUD(IL,8)
        Z1C=ZFAC*ZD(IM,IK)*ZUD(IN,IL)*ZD(II,7)*ZUD(IJ,8)
        ZQ6G(3,IH1)=ZQ6G(3,IH1)-2.0*Z1A+    Z1B+    Z1C
        ZQ6G(4,IH1)=ZQ6G(4,IH1)+    Z1A-2.0*Z1B+    Z1C
        ZQ6G(5,IH1)=ZQ6G(5,IH1)+    Z1A+    Z1B-2.0*Z1C
      END IF
      IF (IH2.NE.0) THEN
        Z1A=ZFAC*ZD(II,IK)*ZUD(IJ,IL)*ZD(IN,7)*ZUD(IM,8)
        Z1B=ZFAC*ZD(II,IN)*ZUD(IJ,IM)*ZD(IK,7)*ZUD(IL,8)
        Z1C=ZFAC*ZD(IN,IK)*ZUD(IM,IL)*ZD(II,7)*ZUD(IJ,8)
        ZQ6G(3,IH2)=ZQ6G(3,IH2)-2.0*Z1A+    Z1B+    Z1C
        ZQ6G(4,IH2)=ZQ6G(4,IH2)+    Z1A-2.0*Z1B+    Z1C
        ZQ6G(5,IH2)=ZQ6G(5,IH2)+    Z1A+    Z1B-2.0*Z1C
      END IF
      IF (IH3.NE.0) THEN
        Z1A=ZFAC*ZD(II,IL)*ZUD(IJ,IK)*ZD(IM,7)*ZUD(IN,8)
        Z1B=ZFAC*ZD(II,IM)*ZUD(IJ,IN)*ZD(IL,7)*ZUD(IK,8)
        Z1C=ZFAC*ZD(IM,IL)*ZUD(IN,IK)*ZD(II,7)*ZUD(IJ,8)
        ZQ6G(3,IH3)=ZQ6G(3,IH3)-2.0*Z1A+    Z1B+    Z1C
        ZQ6G(4,IH3)=ZQ6G(4,IH3)+    Z1A-2.0*Z1B+    Z1C
        ZQ6G(5,IH3)=ZQ6G(5,IH3)+    Z1A+    Z1B-2.0*Z1C
      END IF
      IF (IH4.NE.0) THEN
        Z1A=ZFAC*ZD(II,IL)*ZUD(IJ,IK)*ZD(IN,7)*ZUD(IM,8)
        Z1B=ZFAC*ZD(II,IN)*ZUD(IJ,IM)*ZD(IL,7)*ZUD(IK,8)
        Z1C=ZFAC*ZD(IN,IL)*ZUD(IM,IK)*ZD(II,7)*ZUD(IJ,8)
        ZQ6G(3,IH4)=ZQ6G(3,IH4)-2.0*Z1A+    Z1B+    Z1C
        ZQ6G(4,IH4)=ZQ6G(4,IH4)+    Z1A-2.0*Z1B+    Z1C
        ZQ6G(5,IH4)=ZQ6G(5,IH4)+    Z1A+    Z1B-2.0*Z1C
      END IF
      IF (IH5.NE.0) THEN
        Z1A=ZFAC*ZD(IJ,IK)*ZUD(II,IL)*ZD(IM,7)*ZUD(IN,8)
        Z1B=ZFAC*ZD(IJ,IM)*ZUD(II,IN)*ZD(IK,7)*ZUD(IL,8)
        Z1C=ZFAC*ZD(IM,IK)*ZUD(IN,IL)*ZD(IJ,7)*ZUD(II,8)
        ZQ6G(3,IH5)=ZQ6G(3,IH5)-2.0*Z1A+    Z1B+    Z1C
        ZQ6G(4,IH5)=ZQ6G(4,IH5)+    Z1A-2.0*Z1B+    Z1C
        ZQ6G(5,IH5)=ZQ6G(5,IH5)+    Z1A+    Z1B-2.0*Z1C
      END IF
      IF (IH6.NE.0) THEN
        Z1A=ZFAC*ZD(IJ,IK)*ZUD(II,IL)*ZD(IN,7)*ZUD(IM,8)
        Z1B=ZFAC*ZD(IJ,IN)*ZUD(II,IM)*ZD(IK,7)*ZUD(IL,8)
        Z1C=ZFAC*ZD(IN,IK)*ZUD(IM,IL)*ZD(IJ,7)*ZUD(II,8)
        ZQ6G(3,IH6)=ZQ6G(3,IH6)-2.0*Z1A+    Z1B+    Z1C
        ZQ6G(4,IH6)=ZQ6G(4,IH6)+    Z1A-2.0*Z1B+    Z1C
        ZQ6G(5,IH6)=ZQ6G(5,IH6)+    Z1A+    Z1B-2.0*Z1C
      END IF
      IF (IH7.NE.0) THEN
        Z1A=ZFAC*ZD(IJ,IL)*ZUD(II,IK)*ZD(IM,7)*ZUD(IN,8)
        Z1B=ZFAC*ZD(IJ,IM)*ZUD(II,IN)*ZD(IL,7)*ZUD(IK,8)
        Z1C=ZFAC*ZD(IM,IL)*ZUD(IN,IK)*ZD(IJ,7)*ZUD(II,8)
        ZQ6G(3,IH7)=ZQ6G(3,IH7)-2.0*Z1A+    Z1B+    Z1C
        ZQ6G(4,IH7)=ZQ6G(4,IH7)+    Z1A-2.0*Z1B+    Z1C
        ZQ6G(5,IH7)=ZQ6G(5,IH7)+    Z1A+    Z1B-2.0*Z1C
      END IF
      IF (IH8.NE.0) THEN
        Z1A=ZFAC*ZD(IJ,IL)*ZUD(II,IK)*ZD(IN,7)*ZUD(IM,8)
        Z1B=ZFAC*ZD(IJ,IN)*ZUD(II,IM)*ZD(IL,7)*ZUD(IK,8)
        Z1C=ZFAC*ZD(IN,IL)*ZUD(IM,IK)*ZD(IJ,7)*ZUD(II,8)
        ZQ6G(3,IH8)=ZQ6G(3,IH8)-2.0*Z1A+    Z1B+    Z1C
        ZQ6G(4,IH8)=ZQ6G(4,IH8)+    Z1A-2.0*Z1B+    Z1C
        ZQ6G(5,IH8)=ZQ6G(5,IH8)+    Z1A+    Z1B-2.0*Z1C
      END IF
*
      END
*
************************************************************************
* SUBROUTINE Q6GTAB constructs table for 6q + 1g process.              *
*                                                                      *
* ITABQ6( 1: 6,*)  =  permutation of quarks.                           *
* ITABQ6( 7:14,*)  =  assignment of helicity configurations.           *
* ITABQ6(15   ,*)  =  sign of quark permutation.                       *
************************************************************************
      SUBROUTINE Q6GTAB(ITABQ6)
      DIMENSION ITABQ6(15,36)
      DIMENSION IHELS(6,20),IPERM1(4,6),IPERM2(4,6),IH(6,8),IHEL(6)
      SAVE IHELS,IPERM1,IPERM2,IH
      DATA IHELS/ 0,0,0,1,1,1,  0,0,1,0,1,1,  0,0,1,1,0,1,
     .            0,0,1,1,1,0,  0,1,0,0,1,1,  0,1,0,1,0,1,
     .            0,1,0,1,1,0,  0,1,1,0,0,1,  0,1,1,0,1,0,
     .            0,1,1,1,0,0,  1,0,0,0,1,1,  1,0,0,1,0,1,
     .            1,0,0,1,1,0,  1,0,1,0,0,1,  1,0,1,0,1,0,
     .            1,0,1,1,0,0,  1,1,0,0,0,1,  1,1,0,0,1,0,
     .            1,1,0,1,0,0,  1,1,1,0,0,0/
      DATA IPERM1 / 1,3,5,1, 1,5,3,-1, 3,1,5,-1,
     .              3,5,1,1, 5,3,1,-1, 5,1,3,1/
      DATA IPERM2 / 2,4,6,1, 2,6,4,-1, 4,2,6,-1,
     .              4,6,2,1, 6,4,2,-1, 6,2,4,1/
      DATA IH / 0,1,0,1,0,1, 0,1,0,1,1,0, 0,1,1,0,0,1, 0,1,1,0,1,0,
     .          1,0,0,1,0,1, 1,0,0,1,1,0, 1,0,1,0,0,1, 1,0,1,0,1,0/
*
      ITEL=1
      DO 10 IP1=1,6
        DO 10 IP2=1,6
          ITABQ6(15,ITEL)=IPERM1(4,IP1)*IPERM2(4,IP2)
          ITABQ6(1,ITEL)=IPERM1(1,IP1)
          ITABQ6(3,ITEL)=IPERM1(2,IP1)
          ITABQ6(5,ITEL)=IPERM1(3,IP1)
          ITABQ6(2,ITEL)=IPERM2(1,IP2)
          ITABQ6(4,ITEL)=IPERM2(2,IP2)
          ITABQ6(6,ITEL)=IPERM2(3,IP2)
          DO 20 I=1,8
            DO 30 J=1,6
              IHEL(ITABQ6(J,ITEL))=IH(J,I)
   30       CONTINUE
            J=0
   40       J=J+1
            IOK=0
            DO 50 K=1,6
              IF (IHELS(K,J).NE.IHEL(K)) IOK=1
   50       CONTINUE
            IF (IOK.EQ.0) ITABQ6(6+I,ITEL)=J
            IF (IOK.EQ.1) GOTO 40
   20     CONTINUE
          ITEL=ITEL+1
   10 CONTINUE
*
      END
*
************************************************************************
* SUBROUTINE Q6GCOL initialises the colour mapping for the 6 q + 1 g.  *
* In the colour base there are 18 colour structures:                   *
*                       d(i,j)*d(k,l)*T(a1,m,n)                        *
*                                                                      *
* IQ6(5,36) contains a boolean list of quark permutations              *
* for squaring each of the 5 possible flavour combinations.            *
************************************************************************
      SUBROUTINE Q6GCOL(ITABQ6,IQ6,MAPCOL,COLMAT)
      PARAMETER(NC=3,C=0.0,C0=(NC**2-1.0),
     .               C1=NC*(NC**2-1.0),C2=NC**2*(NC**2-1.0))
      IMPLICIT REAL(A-H,O-Y)
      DIMENSION ITABQ6(15,36),IQ6(5,36),MAPCOL(4,36,7),ITAB(10,18)
      DIMENSION IOK(3),COLMAT(18,18),ID1(6),FROW(18),IR(6)
      SAVE ITAB,FROW
      DATA ITAB/
     .  1,2,3,4,3,4,1,2,5,6,  1,2,5,6,5,6,1,2,3,4, 3,4,5,6,5,6,3,4,1,2,
     .  1,4,3,2,3,2,1,4,5,6,  1,4,5,6,5,6,1,4,3,2, 3,2,5,6,5,6,3,2,1,4,
     .  1,6,3,4,3,4,1,6,5,2,  1,6,5,2,5,2,1,6,3,4, 3,4,5,2,5,2,3,4,1,6,
     .  1,2,3,6,3,6,1,2,5,4,  1,2,5,4,5,4,1,2,3,6, 3,6,5,4,5,4,3,6,1,2,
     .  1,4,3,6,3,6,1,4,5,2,  1,4,5,2,5,2,1,4,3,6, 3,6,5,2,5,2,3,6,1,4,
     .  1,6,3,2,3,2,1,6,5,4,  1,6,5,4,5,4,1,6,3,2, 3,2,5,4,5,4,3,2,1,6/
      DATA FROW/ C2,C ,C ,C1,C ,C ,C1,C ,C1,
     .           C1,C1,C ,C0,C0,C0,C0,C0,C0/
*
* Construct mapping between calculated colour structures
* and the ones used to determine M^2. (Ta) (TaTb) (Tb) + gluon Ta1
*
      DO 10 I=1,36
        DO 20 J=1,18
          DO 25 K=1,5,4
            K1=ITAB(K,J)
            K2=ITAB(K+1,J)
            K3=ITAB(K+2,J)
            K4=ITAB(K+3,J)
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(6,I)).AND.
     .         (K3.EQ.ITABQ6(3,I)).AND.(K4.EQ.ITABQ6(2,I))) THEN
              MAPCOL(1,I,5)=J
              MAPCOL(1,I,6)=J
            END IF
            IF((K1.EQ.ITABQ6(3,I)).AND.(K2.EQ.ITABQ6(2,I)).AND.
     .         (K3.EQ.ITABQ6(5,I)).AND.(K4.EQ.ITABQ6(4,I))) THEN
              MAPCOL(1,I,1)=J
              MAPCOL(1,I,4)=J
              MAPCOL(1,I,7)=J
            END IF
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(6,I)).AND.
     .         (K3.EQ.ITABQ6(5,I)).AND.(K4.EQ.ITABQ6(4,I))) THEN
              MAPCOL(1,I,2)=J
              MAPCOL(1,I,3)=J
            END IF
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(4,I)).AND.
     .         (K3.EQ.ITABQ6(5,I)).AND.(K4.EQ.ITABQ6(6,I))) THEN
              MAPCOL(2,I,2)=J
              MAPCOL(2,I,3)=J
            END IF
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(4,I)).AND.
     .         (K3.EQ.ITABQ6(3,I)).AND.(K4.EQ.ITABQ6(2,I))) THEN
              MAPCOL(2,I,6)=J
              MAPCOL(2,I,7)=J
            END IF
            IF((K1.EQ.ITABQ6(3,I)).AND.(K2.EQ.ITABQ6(2,I)).AND.
     .         (K3.EQ.ITABQ6(5,I)).AND.(K4.EQ.ITABQ6(6,I))) THEN
              MAPCOL(2,I,1)=J
              MAPCOL(2,I,4)=J
              MAPCOL(2,I,5)=J
            END IF
            IF((K1.EQ.ITABQ6(3,I)).AND.(K2.EQ.ITABQ6(6,I)).AND.
     .         (K3.EQ.ITABQ6(5,I)).AND.(K4.EQ.ITABQ6(4,I))) THEN
              MAPCOL(3,I,1)=J
              MAPCOL(3,I,2)=J
            END IF
            IF((K1.EQ.ITABQ6(3,I)).AND.(K2.EQ.ITABQ6(6,I)).AND.
     .         (K3.EQ.ITABQ6(1,I)).AND.(K4.EQ.ITABQ6(2,I))) THEN
              MAPCOL(3,I,5)=J
              MAPCOL(3,I,6)=J
            END IF
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(2,I)).AND.
     .         (K3.EQ.ITABQ6(5,I)).AND.(K4.EQ.ITABQ6(4,I))) THEN
              MAPCOL(3,I,3)=J
              MAPCOL(3,I,4)=J
              MAPCOL(3,I,7)=J
            END IF
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(2,I)).AND.
     .         (K3.EQ.ITABQ6(3,I)).AND.(K4.EQ.ITABQ6(4,I))) THEN
              MAPCOL(4,I,6)=J
              MAPCOL(4,I,7)=J
            END IF
            IF((K1.EQ.ITABQ6(1,I)).AND.(K2.EQ.ITABQ6(2,I)).AND.
     .         (K3.EQ.ITABQ6(5,I)).AND.(K4.EQ.ITABQ6(6,I))) THEN
              MAPCOL(4,I,3)=J
              MAPCOL(4,I,4)=J
              MAPCOL(4,I,5)=J
            END IF
            IF((K1.EQ.ITABQ6(3,I)).AND.(K2.EQ.ITABQ6(4,I)).AND.
     .         (K3.EQ.ITABQ6(5,I)).AND.(K4.EQ.ITABQ6(6,I))) THEN
              MAPCOL(4,I,1)=J
              MAPCOL(4,I,2)=J
            END IF
   25     CONTINUE
   20   CONTINUE
   10 CONTINUE
*
* Calculate the colourmatrix by renumbering the products
* of the fundamental colour structures to the first row (defined above).
* Extra factor 1/4*1/4 because of transformation to new colour base.
*
      DO 27 J=1,18
        COLMAT(1,J)=FROW(J)/16.0
   27 CONTINUE
      DO 28 I=2,18
        IR(ITAB(1,I))=1
        IR(ITAB(2,I))=2
        IR(ITAB(3,I))=3
        IR(ITAB(4,I))=4
        IR(ITAB(9,I))=5
        IR(ITAB(10,I))=6
        DO 35 J=1,18
          DO 31 K=1,4
            ID1(K)=IR(ITAB(K,J))
   31     CONTINUE
          DO 32 K=1,18
            IF ( (ID1(1).EQ.ITAB(1,K)).AND.(ID1(2).EQ.ITAB(2,K)).AND.
     .           (ID1(3).EQ.ITAB(3,K)).AND.(ID1(4).EQ.ITAB(4,K)) )
     .      COLMAT(I,J)=COLMAT(1,K)
            IF ( (ID1(1).EQ.ITAB(5,K)).AND.(ID1(2).EQ.ITAB(6,K)).AND.
     .           (ID1(3).EQ.ITAB(7,K)).AND.(ID1(4).EQ.ITAB(8,K)) )
     .      COLMAT(I,J)=COLMAT(1,K)
   32     CONTINUE
   35  CONTINUE
   28 CONTINUE
*
* Determine which permutations contribute to which flavour combination.
*
      DO 30 I=1,36
        IOK( (ITABQ6(1,I)+1)/2 ) =  ITABQ6(2,I)-1-ITABQ6(1,I)
        IOK( (ITABQ6(3,I)+1)/2 ) =  ITABQ6(4,I)-1-ITABQ6(3,I)
        IOK( (ITABQ6(5,I)+1)/2 ) =  ITABQ6(6,I)-1-ITABQ6(5,I)
        IF ((IOK(1).EQ.0) .AND. (IOK(2).EQ.0) .AND. (IOK(3).EQ.0)) THEN
          IQ6(1,I)=1
        ELSE
          IQ6(1,I)=0
        END IF
        IF (IOK(3).EQ.0) THEN
          IQ6(2,I)=1
        ELSE
          IQ6(2,I)=0
        END IF
        IF (IOK(2).EQ.0) THEN
          IQ6(3,I)=1
        ELSE
          IQ6(3,I)=0
        END IF
        IF (IOK(1).EQ.0) THEN
          IQ6(4,I)=1
        ELSE
          IQ6(4,I)=0
        END IF
        IQ6(5,I)=1
   30 CONTINUE
*
      END
*
************************************************************************
* INITSP initializes all the spinors and propagators with quarks       *
*                                                                      *
* In:  P(1:NG) gluon momenta & Q(1:NQ) quark/antiquark momenta         *
* Out: Gluon spinors in /SPING/ and massive quark spinors in /SPINQ/   *
*      inverse gluon propagators in PPi(0,...)                         *
*      inverse quark+gluons propagators in PQPi(1,...)                 *
*      sum of gluon momenta in spinor language in ZPi(2,2,...)         *
*      and ZQ1(2,2,2) in common /KINEMA/                               *
*      Mass of quarks is implicit, i.e. deduced from momenta           *
*                                                                      *
* Designed to be general but applied only to work with quark currents! *
* ==> NQ=2 or NQ=4 (and not NQ=0)   (NUP=5 is the only possible value) *
************************************************************************
      SUBROUTINE INITSP(P,NG,Q,NQ)
      PARAMETER(NGUP=10,NUP=5,IQUP=4,NUPM1=5,NUPM2=20,NUPM3=60,
     .          NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters
*
      DIMENSION P(0:3,*),Q(0:3,*)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      COMMON /SPING/  ZKO(2,NGUP),ZKOD(2,NGUP),ZHVEC(2,2,2,NGUP)
      COMMON /SPINQ/  ZQKO(2,IQUP),ZQKOD(2,IQUP),
     .                ZQMO(2,IQUP),ZQMOD(2,IQUP)
      COMMON /FERMPR/ ZQP0(2,2,IQUP),ZQP1(2,2,IQUP,NUP),
     .                ZQP2(2,2,IQUP,NUPM2),ZQP3(2,2,IQUP,NUPM3),
     .                ZQP4(2,2,IQUP,NUPM4),PQP1(IQUP,NUP),
     .                PQP2(IQUP,NUPM2),PQP3(IQUP,NUPM3),PQP4(IQUP,NUPM4)
      SAVE /MAPPIN/,/GLUOPR/,/SPING/,/SPINQ/,/FERMPR/
*
* init gluon spinors and spinor momenta
*
      CALL SETGSP(P,NG,1)
      DO 9 I1=1,NG
       DO 9 IAD=1,2
        DO 9 IB=1,2
          ZP1(IAD,IB,I1)=+ZKOD(IAD,I1)*ZKO(IB,I1)
    9 CONTINUE
      CALL INITPR(ZP1,NG)
*
* init massive quark spinors and spinor momenta
*
      CALL SETQSP(Q,NQ)
*
* init sums of spinor momenta and inverse propagators, WITH quarks.
*
      DO 10 IQ=1,NQ
       RMQ=SQRT(ABS(Q(0,IQ)**2-Q(1,IQ)**2-Q(2,IQ)**2-Q(3,IQ)**2))
       DO 15 IAD=1,2
        DO 15 IB=1,2
         ZQP0(IAD,IB,IQ)=+ZQKOD(IAD,IQ)*ZQKO(IB,IQ)
     .                   +ZQMOD(IAD,IQ)*ZQMO(IB,IQ)
   15  CONTINUE
*
       IF (NG.EQ.0) GOTO 999
       DO 30 I1=1,NG
         DO 20 IAD=1,2
          DO 20 IB=1,2
            ZQP1(IAD,IB,IQ,I1)=+ZQP0(IAD,IB,IQ)+ZP1(IAD,IB,I1)
   20    CONTINUE
         PQP1(IQ,I1)=
     .    0.5*REAL(ZMUL(ZQP1(1,1,IQ,I1),ZQP1(1,1,IQ,I1)))-RMQ**2
   30  CONTINUE
*
       IF (NG.EQ.1) GOTO 999
*
       DO 40 I1=1,NG
        DO 40 I2=1,NG
         IF (M2H(I1,I2).NE.0) THEN
           DO 45 IAD=1,2
            DO 45 IB=1,2
              ZQP2(IAD,IB,IQ,M2H(I1,I2))=ZQP0(IAD,IB,IQ)
     .                                  +ZP2(IAD,IB,M2H(I1,I2))
   45      CONTINUE
           PQP2(IQ,M2H(I1,I2))=0.5*REAL(ZMUL(ZQP2(1,1,IQ,M2H(I1,I2)),
     .                         ZQP2(1,1,IQ,M2H(I1,I2))))-RMQ**2
         END IF
   40  CONTINUE
*
       IF (NG.EQ.2) GOTO 999
*
       DO 50 I1=1,NG
        DO 50 I2=1,NG
         DO 50 I3=1,NG
          IF (M3H(I1,I2,I3).NE.0) THEN
            DO 55 IAD=1,2
             DO 55 IB=1,2
               ZQP3(IAD,IB,IQ,M3H(I1,I2,I3))=ZQP0(IAD,IB,IQ)
     .                                      +ZP3(IAD,IB,M3H(I1,I2,I3))
   55       CONTINUE
            PQP3(IQ,M3H(I1,I2,I3))=
     .       0.5*REAL(ZMUL(ZQP3(1,1,IQ,M3H(I1,I2,I3)),
     .                     ZQP3(1,1,IQ,M3H(I1,I2,I3))))-RMQ**2
          END IF
   50  CONTINUE
*
       IF (NG.EQ.3) GOTO 999
*
       DO 60 I1=1,NG
        DO 60 I2=1,NG
         DO 60 I3=1,NG
          DO 60 I4=1,NG
           IF (M4H(I1,I2,I3,I4).NE.0) THEN
             DO 65 IAD=1,2
              DO 65 IB=1,2
               ZQP4(IAD,IB,IQ,M4H(I1,I2,I3,I4))=ZQP0(IAD,IB,IQ)
     .                                     +ZP4(IAD,IB,M4H(I1,I2,I3,I4))
   65        CONTINUE
             PQP4(IQ,M4H(I1,I2,I3,I4))=
     .        0.5*REAL(ZMUL(ZQP4(1,1,IQ,M4H(I1,I2,I3,I4)),
     .                  ZQP4(1,1,IQ,M4H(I1,I2,I3,I4))))-RMQ**2
          END IF
   60  CONTINUE
*
  999 CONTINUE
*
   10 CONTINUE
*
      END
*
************************************************************************
* INITPR(ZP,NG) initializes sums off gluon momenta and                 *
*    the corresponding gluonic propagators in COMMON /GLUOPR/          *
*                                                                      *
* In:  Array of gluon momenta (need not be on-shell)                   *
* Out: COMMON /GLUOPR/ see below, mapping used for compact storage     *
************************************************************************
      SUBROUTINE INITPR(ZP,NG)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* subroutine parameters
*
      DIMENSION ZP(2,2,*)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      COMMON /GLUOPR/ ZP1(2,2,NUP),ZP2(2,2,NUPM2),
     .                ZP3(2,2,NUPM3),ZP4(2,2,NUPM4),
     .                PP2(NUPM2),PP3(NUPM3),PP4(NUPM4)
      SAVE /MAPPIN/,/GLUOPR/
*
      CALL COPYTS(ZP1,ZP,NG)
*
      IF (NG.EQ.1) GOTO 999
*
      DO 40 I1=1,NG
       DO 40 I2=1,NG
        IF (M2H(I1,I2).NE.0) THEN
          DO 45 IAD=1,2
           DO 45 IB=1,2
             ZP2(IAD,IB,M2H(I1,I2))=ZP1(IAD,IB,I1)+ZP1(IAD,IB,I2)
   45     CONTINUE
          PP2(M2H(I1,I2))=
     .      0.5*REAL(ZMUL(ZP2(1,1,M2H(I1,I2)),ZP2(1,1,M2H(I1,I2))))
        END IF
   40 CONTINUE
*
      IF (NG.EQ.2) GOTO 999
*
      DO 50 I1=1,NG
       DO 50 I2=1,NG
        DO 50 I3=1,NG
         IF (M3H(I1,I2,I3).NE.0) THEN
           DO 55 IAD=1,2
            DO 55 IB=1,2
              ZP3(IAD,IB,M3H(I1,I2,I3))=ZP2(IAD,IB,M2H(I1,I2))
     .                                 +ZP1(IAD,IB,I3)
   55      CONTINUE
           PP3(M3H(I1,I2,I3))=0.5*REAL(ZMUL(ZP3(1,1,M3H(I1,I2,I3)),
     .                        ZP3(1,1,M3H(I1,I2,I3))))
         END IF
   50 CONTINUE
*
      IF (NG.EQ.3) GOTO 999
*
      DO 60 I1=1,NG
       DO 60 I2=1,NG
        DO 60 I3=1,NG
         DO 60 I4=1,NG
          IF (M4H(I1,I2,I3,I4).NE.0) THEN
            DO 65 IAD=1,2
             DO 65 IB=1,2
              ZP4(IAD,IB,M4H(I1,I2,I3,I4))=ZP3(IAD,IB,M3H(I1,I2,I3))
     .                                    +ZP1(IAD,IB,I4)
   65       CONTINUE
            PP4(M4H(I1,I2,I3,I4))=
     .       0.5*REAL(ZMUL(ZP4(1,1,M4H(I1,I2,I3,I4)),
     .                 ZP4(1,1,M4H(I1,I2,I3,I4))))
         END IF
   60 CONTINUE
*
  999 CONTINUE
*
      END
*
************************************************************************
* INIMAP initializes the mapping tables, used for easy reference and   *
* compact storage of all calculated currents.                          *
*                                                                      *
* Important feature of mapping is that the first I! of MIH are the     *
* permutations of 1..I (do not included higher gluon numbers)          *
*                                                                      *
* In:  NG number of gluons which can occur                             *
* Out: COMMON /MAPPIN/                                                 *
************************************************************************
* VERY IMPROTANT: INIMAP can be called more than once, however if NG   *
* gets smaller /MAPPIN/ should not be reinitialised!!                  *
************************************************************************
      SUBROUTINE INIMAP(NG)
      PARAMETER(NUP=5,NUPM1=5,NUPM2=20,NUPM3=60,NUPM4=120,NUPM5=120)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* global variables, covariant tensors.
*
      COMMON /MAPPIN/ M1H(NUP),M1T(NUPM1,1),M1UP,
     .                M2H(NUP,NUP),M2T(NUPM2,2),M2UP,
     .                M3H(NUP,NUP,NUP),M3T(NUPM3,3),M3UP,
     .                M4H(NUP,NUP,NUP,NUP),M4T(NUPM4,4),M4UP,
     .                M5H(NUP,NUP,NUP,NUP,NUP),M5T(NUPM5,5),M5UP
      DIMENSION IP(NUP)
      SAVE NGOLD,/MAPPIN/
      DATA NGOLD /0/
*
      IF (NG.LE.NGOLD) RETURN
      NGOLD=NG
*
      DO 10 I=1,IFAC(NUP)
        CALL INVID(I,IP,NUP)
        M1H(IP(1))=0
        M2H(IP(1),IP(2))=0
        M3H(IP(1),IP(2),IP(3))=0
        M4H(IP(1),IP(2),IP(3),IP(4))=0
        M5H(IP(1),IP(2),IP(3),IP(4),IP(5))=0
   10 CONTINUE
      M1UP=0
      M2UP=0
      M3UP=0
      M4UP=0
      M5UP=0
      DO 20 NTEL=1,NG
        DO 30 I=1,IFAC(NTEL)
          CALL INVID(I,IP,NTEL)
          I1=IP(1)
          I2=IP(2)
          I3=IP(3)
          I4=IP(4)
          I5=IP(5)
          IF (M1H(I1).EQ.0) THEN
            M1UP=M1UP+1
            M1H(I1)=M1UP
            M1T(M1UP,1)=I1
          END IF
          IF ((M2H(I1,I2).EQ.0).AND.(NTEL.GT.1)) THEN
            M2UP=M2UP+1
            M2H(I1,I2)=M2UP
            M2T(M2UP,1)=I1
            M2T(M2UP,2)=I2
          END IF
          IF ((M3H(I1,I2,I3).EQ.0).AND.(NTEL.GT.2)) THEN
            M3UP=M3UP+1
            M3H(I1,I2,I3)=M3UP
            M3T(M3UP,1)=I1
            M3T(M3UP,2)=I2
            M3T(M3UP,3)=I3
          END IF
          IF ((M4H(I1,I2,I3,I4).EQ.0).AND.(NTEL.GT.3)) THEN
            M4UP=M4UP+1
            M4H(I1,I2,I3,I4)=M4UP
            M4T(M4UP,1)=I1
            M4T(M4UP,2)=I2
            M4T(M4UP,3)=I3
            M4T(M4UP,4)=I4
          END IF
          IF ((M5H(I1,I2,I3,I4,I5).EQ.0).AND.(NTEL.GT.4)) THEN
            M5UP=M5UP+1
            M5H(I1,I2,I3,I4,I5)=M5UP
            M5T(M5UP,1)=I1
            M5T(M5UP,2)=I2
            M5T(M5UP,3)=I3
            M5T(M5UP,4)=I4
            M5T(M5UP,5)=I5
          END IF
   30   CONTINUE
   20 CONTINUE
*
      END
*
************************************************************************
* SETQSP(PQUARK,NQUARK) inits the quark spinors (can be massive)       *
*   Transformation to Weyl-vd Waerden spinor, (P1,P2,P3)-> (P2,P3,P1)  *
*                                                                      *
*   In:  Set of contra-variant momenta, NQUARK is number of quarks     *
*   Out: COMMON /SPINQ/ spinors, both k and m types.                   *
*        In CASE MQ=0 only the K spinors are inited to gain speed.     *
************************************************************************
      SUBROUTINE SETQSP(PQUARK,NQUARK)
      PARAMETER(IQUP=4)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      DIMENSION PQUARK(0:3,*)
      COMMON /SPINQ/ ZQKO(2,IQUP),ZQKOD(2,IQUP),
     .               ZQMO(2,IQUP),ZQMOD(2,IQUP)
      SAVE /SPINQ/
*
      IF (IQUP.LT.NQUARK) STOP
*
      DO 10 I=1,NQUARK
        QLEN=SQRT( PQUARK(1,I)**2 + PQUARK(2,I)**2 + PQUARK(3,I)**2 )
        IF ((PQUARK(0,I)**2-QLEN**2).GT.1D-10) THEN
          IF (PQUARK(0,I).LT.0.0) THEN
            ZFACT=(0.0,-1.0)
          ELSE
            ZFACT=(1.0,0.0)
          END IF
          ZQSRT=ZFACT/CSQRT( CMPLX( 2.0 * QLEN * (QLEN - PQUARK(1,I)) ))
          ZQKO(1,I)=ZQSRT* (PQUARK(2,I)-(0.0,1.0)*PQUARK(3,I))
     .                  * CSQRT(CMPLX(PQUARK(0,I)+QLEN))
          ZQKO(2,I)=ZQSRT* (QLEN - PQUARK(1,I))
     .                  * CSQRT(CMPLX(PQUARK(0,I)+QLEN))
          ZQMO(1,I)=(0.0,-1.0)*ZQSRT* ( PQUARK(1,I) - QLEN )
     .                  * CSQRT(CMPLX(PQUARK(0,I)-QLEN))
          ZQMO(2,I)=(0.0,-1.0)*ZQSRT*(PQUARK(2,I)+(0.0,1.0)*PQUARK(3,I))
     .                  * CSQRT(CMPLX(PQUARK(0,I)-QLEN))
        ELSE
          ZQKO(1,I)=(PQUARK(2,I)-(0.0,1.0)*PQUARK(3,I))
     .               /CSQRT(CMPLX(PQUARK(0,I)-PQUARK(1,I)))
          ZQKO(2,I)=CSQRT(CMPLX(PQUARK(0,I)-PQUARK(1,I)))
          ZQMO(1,I)=(0.0,0.0)
          ZQMO(2,I)=(0.0,0.0)
        END IF
        IS=1
        IF (PQUARK(0,I).LT.0.0) IS=-1
        ZQKOD(1,I)=IS*CONJG(ZQKO(1,I))
        ZQKOD(2,I)=IS*CONJG(ZQKO(2,I))
        ZQMOD(1,I)=IS*CONJG(ZQMO(1,I))
        ZQMOD(2,I)=IS*CONJG(ZQMO(2,I))
   10 CONTINUE
*
      END
*
************************************************************************
* SETGSP(PGLUON,NGLUON) inits the gluon spinors and helicity vectors.  *
*   Transformation to Weyl-vd Waerden spinor, (P1,P2,P3)-> (P2,P3,P1)  *
*                                                                      *
*   In:  Set of contra-variant momenta, NGLUON is number of gluons     *
*   Out: COMMON /SPING/ spinors and helicity vectors with random       *
*        spinor B, if flag IHVEC=1                                     *
************************************************************************
      SUBROUTINE SETGSP(PGLUON,NGLUON,IHVEC)
      PARAMETER(NGUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
* Subroutine parameter: gluon momenta
*
      DIMENSION PGLUON(0:3,*)
*
* Global variables
*
      COMMON /SPING/ ZKO(2,NGUP),ZKOD(2,NGUP),ZHVEC(2,2,2,NGUP)
      COMMON /DOTPR/ ZUD(NGUP,NGUP),ZD(NGUP,NGUP)
*
* Local variables
*
      DIMENSION ZB(2)
      REAL*8 RN
      SAVE /SPING/,/DOTPR/
*
* Transform contravariant momentum into covariant spinors, add extra
* minus sign when the energy<0 to preserve momentum conservation.
* Singularity along x-axis, if it occurs put it along the y-axis
*
      IX=1
      IY=2
    5 CONTINUE
      DO 10 I=1,NGLUON
        IF (ABS(1.0-PGLUON(IX,I)/PGLUON(0,I)).LT.1D-6) THEN
          IX=2
          IY=1
          GOTO 5
        END IF
        ZKO(1,I)=(PGLUON(IY,I)-(0.0,1.0)*PGLUON(3,I))
     .             /CSQRT(CMPLX(PGLUON(0,I)-PGLUON(IX,I)))
        ZKO(2,I)=CSQRT(CMPLX(PGLUON(0,I)-PGLUON(IX,I)))
        ZKOD(1,I)=CONJG(ZKO(1,I))
        ZKOD(2,I)=CONJG(ZKO(2,I))
        IF (PGLUON(0,I).LT.0.0) THEN
          ZKOD(1,I)=-ZKOD(1,I)
          ZKOD(2,I)=-ZKOD(2,I)
        END IF
   10 CONTINUE
*
* Helicity vectors
*
      IF (IHVEC.EQ.1) THEN
        SQR2=SQRT(2.0)
        DO 20 I=1,NGLUON
          ZB(1)=REAL(RN(1))+(0.0,1.0)*REAL(RN(2))
          ZB(2)=REAL(RN(3))+(0.0,1.0)*REAL(RN(4))
          ZN0=(ZKO(2,I)*ZB(1)-ZKO(1,I)*ZB(2))
          ZHVEC(1,1,1,I)=SQR2*ZKOD(1,I)*ZB(1)/ZN0
          ZHVEC(1,2,1,I)=SQR2*ZKOD(1,I)*ZB(2)/ZN0
          ZHVEC(2,1,1,I)=SQR2*ZKOD(2,I)*ZB(1)/ZN0
          ZHVEC(2,2,1,I)=SQR2*ZKOD(2,I)*ZB(2)/ZN0
          ZN1=(ZKOD(2,I)*CONJG(ZB(1))-ZKOD(1,I)*CONJG(ZB(2)))
          ZHVEC(1,1,2,I)=SQR2*CONJG(ZB(1))*ZKO(1,I)/ZN1
          ZHVEC(1,2,2,I)=SQR2*CONJG(ZB(1))*ZKO(2,I)/ZN1
          ZHVEC(2,1,2,I)=SQR2*CONJG(ZB(2))*ZKO(1,I)/ZN1
          ZHVEC(2,2,2,I)=SQR2*CONJG(ZB(2))*ZKO(2,I)/ZN1
   20   CONTINUE
      END IF
*
* Spinor innerproducts
*
      DO 30 I=1,NGLUON-1
        DO 30 J=I,NGLUON
          ZUD(I,J)=-ZKO(1,I)*ZKO(2,J)+ZKO(2,I)*ZKO(1,J)
          ZUD(J,I)=-ZUD(I,J)
          ZD(I,J)=-ZKOD(1,I)*ZKOD(2,J)+ZKOD(2,I)*ZKOD(1,J)
          ZD(J,I)=-ZD(I,J)
   30 CONTINUE
*
      END
*
***********************************************************************
* FUNCTION ZMUL(Z1,Z2) calculates a SMU*SNU inproduct in              *
*   spinor tensor language.                                           *
*                                                                     *
*   In:  Z1(2,2),Z2(2,2) both co(ntra) variant spinor tensors         *
*   Out: Innerproduct  (2x normal minkowski innerprodcut              *
***********************************************************************
      FUNCTION ZMUL(Z1,Z2)
      COMPLEX ZMUL,Z1(2,2),Z2(2,2)
      ZMUL=Z1(1,1)*Z2(2,2)+Z1(2,2)*Z2(1,1)
     .    -Z1(1,2)*Z2(2,1)-Z1(2,1)*Z2(1,2)
*
      END
*
***********************************************************************
* PRFILL(P) calculates all the 2 and 3 propagators.                   *
* input: P = Array of covariant vectors (+,-,-,-) metric              *
* Output:                                                             *
*   COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP) *
*      with PRO23 = PRO2(i,j)*PRO2(j,k)*PRO3(i,j,k)                   *
***********************************************************************
      SUBROUTINE PRFILL(P,N)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      DIMENSION P(0:3,*)
      COMMON /PROPS/ PRO2(NUP,NUP),PRO3(NUP,NUP,NUP),PRO23(NUP,NUP,NUP)
      SAVE /PROPS/
*
      DO 10 I=1,N
        DO 10 J=1,N
          PRO2(I,J)=2.0*(P(0,I)*P(0,J)-P(1,I)*P(1,J)
     .                  -P(2,I)*P(2,J)-P(3,I)*P(3,J))
          PRO2(J,I)=PRO2(I,J)
   10 CONTINUE
*
      DO 20 I=1,N
        DO 20 J=1,N
          DO 20 K=1,N
            PRO3(I,J,K)=PRO2(I,J)+PRO2(I,K)+PRO2(J,K)
            PRO23(I,J,K)=PRO2(I,J)*PRO2(J,K)*PRO3(I,J,K)
   20 CONTINUE
*
      END
*
***********************************************************************
* SUBROUTINE ZZFILL(N) calculates all possible <a|b+c|d> terms for N  *
* particles and puts them in COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)      *
***********************************************************************
      SUBROUTINE ZZFILL(N)
      PARAMETER(NUP=10)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
      COMMON /DOTPR/ ZUD(NUP,NUP),ZD(NUP,NUP)
      COMMON /ZFUNCT/ ZZ(NUP,NUP,NUP,NUP)
      SAVE /DOTPR/,/ZFUNCT/
*
      DO 10 I1=1,N
       DO 10 I2=1,N
        DO 10 I3=1,N
         DO 10 I4=1,N
           IF ( (I1.LE.I4).AND.(I2.LE.I3) ) THEN
             Z=ZD(I1,I2)*ZUD(I4,I2)+ZD(I1,I3)*ZUD(I4,I3)
             ZZ(I1,I2,I3,I4)=Z
             ZZ(I1,I3,I2,I4)=Z
             Z=ZD(I4,I2)*ZUD(I1,I2)+ZD(I4,I3)*ZUD(I1,I3)
             ZZ(I4,I2,I3,I1)=Z
             ZZ(I4,I3,I2,I1)=Z
           END IF
   10 CONTINUE
*
      END
*
************************************************************************
* FUNCTION ID(LPERM,NGLUON) maps permutation of (1...N) onto integers. *
*   Also works for NGLUON=0 (returns 1).  Inverse routine is INVID.    *
*   Limitation: NGLUON <= NGUP!                                        *
*                                                                      *
*   In:  LPERM = permutation of 1..NGLUON                              *
*   Out: Mapping according to subsyclic rotations                      *
************************************************************************
      FUNCTION ID(LPERM,NGLUON)
      PARAMETER(NGUP=10)
      DIMENSION LPERM(1:*),IW(1:NGUP)
*
      IF (NGUP.LT.NGLUON) STOP
*
      ID=1
      DO 10 I=1,NGLUON
        IW(I)=I-1
   10 CONTINUE
      DO 30 I=1,NGLUON-1
        K1=IW(LPERM(I))
        ID=ID+K1*IFAC(NGLUON-I)
        K2=K1+1
        K3=NGLUON-I-K1
        DO 20 J=1,NGLUON
          IF (IW(J).LT.K2) THEN
            IW(J)=IW(J)+K3
          ELSE
            IW(J)=IW(J)-K2
          END IF
   20   CONTINUE
   30 CONTINUE
      END
*
************************************************************************
*                                                                      *
* SUBROUTINE INVID(IDNUM,LPERM,NGLUON) creates a permutation           *
*   of (1...NGLUON) from IDNUM according to subcyclic numbering.       *
*   Result in LPERM. Also works for N=0 (returns nothing!).            *
*   Inverse routine of ID-function. Limitation: NGLUON <= NGUP         *
*                                                                      *
*   In:  IDNUM corresponding with a permutation of (1..NGLUON)         *
*   Out: LPERM permutation of (1..NGLUON)                              *
************************************************************************
      SUBROUTINE INVID(IDNUM,LPERM,NGLUON)
      PARAMETER(NGUP=10)
      DIMENSION LPERM(1:*),LHELP(1:NGUP*NGUP)
*
      IDNR=IDNUM-1
      DO 10 I=1,NGLUON
        LHELP(I)=I
   10 CONTINUE
      IB=1
*
      DO 40 I=1,NGLUON-1
        K1=IFAC(NGLUON-I)
        K2=IDNR/K1
        LPERM(I)=LHELP(IB+K2)
        K3=NGLUON-I+1
        DO 20 J=IB,IB+K2-1
          LHELP(J+K3)=LHELP(J)
   20   CONTINUE
        IB=IB+K2+1
        IDNR=MOD(IDNR,K1)
   40 CONTINUE
      IF (NGLUON.GT.0) LPERM(NGLUON)=LHELP(IB)
      END
*
***********************************************************************
* INTEGER FUNCTION IFAC(N) returns N! (N<11)                          *
***********************************************************************
      FUNCTION IFAC(N)
      INTEGER IFAK(0:10)
      SAVE IFAK
      DATA IFAK/1,1,2,6,24,120,720,5040,40320,362880,3628800/
      IFAC=IFAK(N)
      END
*
**********************************************************************
* FUNCTION SQUARE(COLMAT,NMAT,Z,N) determines the square of a column *
* of N complex*8 numbers Z with an NMAT*NMAT colour matrix.          *
* The matrix is supposed to be symmetric.                            *
**********************************************************************
      FUNCTION SQUARE(COLMAT,NMAT,Z,N)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
      DIMENSION COLMAT(NMAT,NMAT),Z(*)
*
      SQUARE=0.0
      DO 10 I=1,N
        Z1=0.5*COLMAT(I,I)*Z(I)
        DO 20 J=I+1,N
          Z1=Z1+COLMAT(I,J)*Z(J)
   20   CONTINUE
        SQUARE=SQUARE+2.0*REAL(Z1*CONJG(Z(I)))
   10 CONTINUE
*
      END
*
************************************************************************
* Subroutine MAPEVE(N,PLAB,P,SCALE) rescales an event to have an       *
* average energy of 1 GeV per particle.                                *
* In:  N,PLAB(4,*) double precision momenta.                           *
* Out: P(0:3,*) and SCALE, single precision datatypes.                 *
*      The first two particles in PLAB of spacetime reversed.          *
************************************************************************
      SUBROUTINE MAPEVE(N,PLAB,P,SCALE)
      IMPLICIT REAL*4 (A-H,O-Y)
      REAL*8 PLAB(4,*)
      DIMENSION P(0:3,*)
*
      TOTEN=0.0
      DO 10 I=1,N
        TOTEN=TOTEN+REAL(PLAB(4,I))
   10 CONTINUE
      SCALE=TOTEN/(N)
      DO 20 I=1,N
        DO 20 NU=1,4
          MU=NU
          IF(MU.EQ.4) MU=0
          P(MU,I)=REAL(PLAB(NU,I))/SCALE
          IF (I.LT.3) P(MU,I)=-P(MU,I)
   20 CONTINUE
*
      END
*
************************************************************************
* Subroutine RESHQS(P,IQ1,IQB1,IQ2,IQB2,NQ) reorders the momenta       *
* such that the first is a Q, the second is the corresponding QB, etc. *
* In:  P(0:3,*) single precision momenta.                              *
* Out: Reshuffled array P(0:3,*)                                       *
* When NQ=2 only the first two indices are used.                       *
************************************************************************
      SUBROUTINE RESHQS(P,IQ1,IQB1,IQ2,IQB2,NQ)
      IMPLICIT REAL*4 (A-H,O-Y)
      DIMENSION P(0:3,*),INDEX(4)
*
* Make sure the swapping does not swap one momentum twice.
*
      INDEX(1)=IQ1
      INDEX(2)=IQB1
      IF (INDEX(2).EQ.1) INDEX(2)=INDEX(1)
      IF (NQ.EQ.4) THEN
        INDEX(3)=IQ2
        IF (INDEX(3).EQ.1) INDEX(3)=INDEX(1)
        IF (INDEX(3).EQ.2) INDEX(3)=INDEX(2)
        INDEX(4)=IQB2
        IF (INDEX(4).EQ.1) INDEX(4)=INDEX(1)
        IF (INDEX(4).EQ.2) INDEX(4)=INDEX(2)
        IF (INDEX(4).EQ.3) INDEX(4)=INDEX(3)
      END IF
*
      DO 10 I=1,NQ
        DO 20 MU=0,3
          TEMP=P(MU,I)
          P(MU,I)=P(MU,INDEX(I))
          P(MU,INDEX(I))=TEMP
   20   CONTINUE
   10 CONTINUE
*
      END
*
************************************************************************
* COPYTS(ZTARG,ZSOUR,N) copies N spinor tensors from ZSOUR to ZTARG.   *
*                                                                      *
* In:  ZSOUR(2,2,N)                                                    *
* Out: ZTARG(2,2,N) = ZSOUR(2,2,N)                                     *
************************************************************************
      SUBROUTINE COPYTS(ZTARG,ZSOUR,N)
      IMPLICIT REAL (A-H,O-Y)
      IMPLICIT COMPLEX (Z)
*
      DIMENSION ZTARG(2,2,*),ZSOUR(2,2,*)
*
      DO 10 I=1,N
        ZTARG(1,1,I)=ZSOUR(1,1,I)
        ZTARG(1,2,I)=ZSOUR(1,2,I)
        ZTARG(2,1,I)=ZSOUR(2,1,I)
        ZTARG(2,2,I)=ZSOUR(2,2,I)
   10 CONTINUE
*
      END
