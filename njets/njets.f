************************************************************************
* NJETS    determines cross sections for 2 --> 2,3,4 or 5 jets.        *
*          by means of a simple Monte Carlo over the phase space.      *
************************************************************************
* NJETS is written between 08-02-90 and 15-10-90,  VERSION 1.4         *
* E-mail address of author: KUIJF@HLERUL59                             *
************************************************************************
      PROGRAM NJETS
      IMPLICIT REAL*8 (A-H,O-Z)
      COMMON /BVEG1/  XL(10),XU(10),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON /EFFIC/  NIN1,NIN2,NOUT
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /INTER/  ITMX1,NCALL1,IMANN1,ITMX2,NCALL2,IMANN2
      COMMON /PROCES/ ISUB(4),ISTAT(4),IINIT(3),IHISTO(40)
      COMMON /CUTVAL/ PTMIN1,ETAMA1,COSTM1,DELTR1,ETSUM,
     .                PTMIN2,ETAMA2,COSTM2,DELTR2
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /HISSCA/ CMMAX,PTMAX,RM2MAX,POUMAX,ICM,IPT,IM2,IPOUT,
     .                PETSUM,PPTMAX,PSPHER,PANGLE,PWEIGH,
     .                IETSUM,IPTMAX,ISPHE,IANGLE,IWEIGH
      COMMON /HISSC2/ PTMAX2,IPT2
      COMMON /MCRES/  AVGI1,SD1,CHI2A1,AVGI2,SD2,CHI2A2
      DIMENSION COLLID(40,4)
      EXTERNAL FXN
*
*     CUTS :      PTMIN1<  PTMIN <PTMIN2
*          :      ETAMA2<  ETA   <ETAMA1
*          :      SEPAN1<  SEPAN <SEPAN2  (DELTR1=-1)
*          :      DELTR1<  DELTR <DELTR2
*     DATA   COLLID  ( SPS, TEVATRON, LHC, SSC )
*    . / PPBAR ,ECM   ,PTMIN1,PTMIN2,ETAMA1,ETAMA2,DELTR1,DELTR2,
*    .   SEPAN1,SEPAN2,ETSUM ,......,CMMAX ,ICM   ,PTMAX ,IPT   ,
*    .   RM2MAX,IM2   ,POUMAX,IPOUT ,PETSUM,IETSUM,PPTMAX,IPTMAX,
*    .   PSPHER,ISPHE ,PANGLE,IANGLE,PWEIGH,IWEIGH,PTMAX2,IPT2  ,
*    .   ......,......,......,......,......,......,......,......./
*
      DATA   COLLID / 1D0,630D0,18D0,70d2,2.5D0 , 0D0, -1D0,100D0 ,
     .   30D0  ,180D0 ,0D0  ,0D0   ,500D0 ,25D0  ,200D0 ,20D0  ,
     .   125D0 ,25D0  ,100D0 ,20D0  ,500D0 ,50D0  ,100D0 ,50D0  ,
     .   1D0   ,50D0  ,  1D0 ,20D0  ,10D0  ,20D0  ,100D0 ,20D0  ,
     .   0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0,
*
     .   1D0   ,1800D0,20D0  ,1000D0,4D0 ,0.0D0 ,0.7D0 ,100D0 ,
     .   00D0  ,180D0 ,0D0   ,0D0   ,700D0 ,35D0  ,200D0 ,20D0  ,
     .   200D0 ,20D0  ,100D0 ,20D0  ,500D0 ,50D0  ,100D0 ,50D0  ,
     .   1D0   ,50D0  ,1D0   ,20D0  ,10D0  ,20D0  ,200D0 ,20D0,
     .   0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0,
*
     .   0D0   ,16D3  ,50D0  ,300d2 ,3.0D0 ,0D0   ,-1D0  ,100D0 ,
     .   30D0  ,180D0 ,0.0D0 ,0D0   ,2000D0,20D0  ,400D0 ,20D0  ,
     .   500D0 ,25D0  ,200D0 ,20D0  ,500D0 ,50D0  ,100D0 ,50D0  ,
     .   1D0   ,50D0  , 1D0  ,20D0  ,10D0  ,20D0  ,400D0, 20D0   ,
     .   0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0,
*
     .   0D0   ,40D3  ,100D0 ,1000D0,3.0D0 ,0D0   , -1D0 ,100D0 ,
     .   30D0  ,180D0 ,00D0  ,0D0   ,3000D0,30D0  ,600D0 ,30D0  ,
     .   1000D0,20D0  ,400D0 ,20D0  ,500D0 ,50D0  ,100D0 ,50D0  ,
     .   1D0   ,50D0  ,1D0   ,20D0  ,10D0  ,20D0  ,600D0, 20D0,
     .   0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0   ,0D0/
*
* Input parameters
* Number of final state jets (<=8)
      NJET=2
* Use effective structure functions
* 0 = Don't use it.
* 1 = Use Gluonic matrix element
* 2 = Use Q QB' in intial state matrix elements.
      IEFF=0
* Type of structure function to use in VEGAS integration:
* istruc=0  -> test        x*all(x) = x*(1-x)
* istruc=1  -> mrse        (msbar), Lambda = 0.1  GeV
* istruc=2  -> mrsb        (msbar), Lambda = 0.2  GeV
* istruc=3  -> kmrshb      (msbar), Lambda = 0.19 GeV
* istruc=4  -> kmrsb0      (msbar), Lambda = 0.19 GeV
* istruc=5  -> kmrsb-      (msbar), Lambda = 0.19 GeV
* istruc=6  -> kmrsh- (5)  (msbar), Lambda = 0.19 GeV
* istruc=7  -> kmrsh- (2)  (msbar), Lambda = 0.19 GeV
* istruc=8  -> mts1        (msbar), Lambda = 0.2  GeV
* istruc=9  -> mte1        (msbar), Lambda = 0.2  GeV
* istruc=10 -> mtb1        (msbar), Lambda = 0.2  GeV
* istruc=11 -> mtb2        (msbar), Lambda = 0.2  GeV
* istruc=12 -> mtsn1       (msbar), Lambda = 0.2  GeV
* istruc=13 -> kmrss0      (msbar), Lambda = 0.215 GeV
* istruc=14 -> kmrsd0      (msbar), Lambda = 0.215 GeV
* istruc=15 -> kmrsd-      (msbar), Lambda = 0.215 GeV
* istruc=16 -> mrss0       (msbar), Lambda = 0.230 GeV
* istruc=17 -> mrsd0       (msbar), Lambda = 0.230 GeV
* istruc=18 -> mrsd-       (msbar), Lambda = 0.230 GeV
* istruc=19 -> cteq1l      (msbar), Lambda = 0.168 GeV
* istruc=20 -> cteq1m      (msbar), Lambda = 0.231 GeV
* istruc=21 -> cteq1ml     (msbar), Lambda = 0.322 GeV
* istruc=22 -> cteq1ms     (msbar), Lambda = 0.231 GeV
* istruc=23 -> grv         (msbar), Lambda = 0.200 GeV
* istruc=24 -> cteq2l      (msbar), Lambda = 0.190 GeV
* istruc=25 -> cteq2m      (msbar), Lambda = 0.213 GeV
* istruc=26 -> cteq2ml     (msbar), Lambda = 0.322 GeV
* istruc=27 -> cteq2ms     (msbar), Lambda = 0.208 GeV
* istruc=28 -> cteq2mf     (msbar), Lambda = 0.208 GeV
* istruc=29 -> mrsh        (msbar), Lambda = 0.230 GeV
* istruc=30 -> mrsa        (msbar), Lambda = 0.230 GeV
* istruc=31 -> mrsg        (msbar), Lambda = 0.255 GeV
* istruc=32 -> mrsap       (msbar), Lambda = 0.231 GeV
* istruc=33 -> grv94       (msbar), Lambda = 0.200 GeV
* istruc=34 -> cteq3l      (msbar), Lambda = 0.177 GeV
* istruc=35 -> cteq3m      (msbar), Lambda = 0.239 GeV
* istruc=36 -> mrsj        (msbar), Lambda = 0.366 GeV
* istruc=37 -> mrsjp       (msbar), Lambda = 0.542 GeV
* istruc=38 -> cteqjet     (msbar), Lambda = 0.286 GeV
* istruc=39 -> mrsr1       (msbar), Lambda = 0.241 GeV
* istruc=40 -> mrsr2       (msbar), Lambda = 0.344 GeV
* istruc=41 -> mrsr3       (msbar), Lambda = 0.241 GeV 
* istruc=42 -> mrsr4       (msbar), Lambda = 0.344 GeV 
* istruc=43 -> cteq4l      (msbar), Lambda = 0.300 GeV
* istruc=44 -> cteq4m      (msbar), Lambda = 0.300 GeV
* istruc=45 -> cteq4hj     (msbar), Lambda = 0.300 GeV
* istruc=46 -> cteq4lq     (msbar), Lambda = 0.300 GeV
*
*
* extra structure functions with variable as/Lambda
*
* istruc=101 -> mrsap       (msbar), as(Mz) = 0.110 -> Lambda = 0.216
* istruc=102 -> mrsap       (msbar), as(Mz) = 0.115 -> Lambda = 0.284
* istruc=103 -> mrsap       (msbar), as(Mz) = 0.120 -> Lambda = 0.366
* istruc=104 -> mrsap       (msbar), as(Mz) = 0.125 -> Lambda = 0.458
* istruc=105 -> mrsap       (msbar), as(Mz) = 0.130 -> Lambda = 0.564
* istruc=111 -> grv94       (msbar), Lambda = 0.150 GeV
* istruc=112 -> grv94       (msbar), Lambda = 0.200 GeV = istruc33
* istruc=113 -> grv94       (msbar), Lambda = 0.250 GeV
* istruc=114 -> grv94       (msbar), Lambda = 0.300 GeV
* istruc=115 -> grv94       (msbar), Lambda = 0.350 GeV
* istruc=116 -> grv94       (msbar), Lambda = 0.400 GeV
* istruc=121 -> cteq3m      (msbar), Lambda = 0.100 GeV
* istruc=122 -> cteq3m      (msbar), Lambda = 0.120 GeV
* istruc=123 -> cteq3m      (msbar), Lambda = 0.140 GeV
* istruc=124 -> cteq3m      (msbar), Lambda = 0.180 GeV
* istruc=125 -> cteq3m      (msbar), Lambda = 0.200 GeV
* istruc=126 -> cteq3m      (msbar), Lambda = 0.220 GeV
* istruc=127 -> cteq3m      (msbar), Lambda = 0.260 GeV
* istruc=128 -> cteq3m      (msbar), Lambda = 0.300 GeV
* istruc=129 -> cteq3m      (msbar), Lambda = 0.340 GeV
* istruc=130 -> cteq4m      (msbar), Lambda = 0.215 GeV
* istruc=131 -> cteq4m      (msbar), Lambda = 0.255 GeV
* istruc=132 -> cteq4m      (msbar), Lambda = 0.300 GeV = istruc44
* istruc=133 -> cteq4m      (msbar), Lambda = 0.348 GeV
* istruc=134 -> cteq4m      (msbar), Lambda = 0.401 GeV
*
* select up to 6 PDF's in iuse1,...,iuse6. If set to 0 no PDF is choosen.
* set the variable iuse between 1 and 6 depending which PDF you want to
* use in the integration (iuse=# means use iuse# for integration).
      IUSE1=30
      IUSE2=101
      IUSE3=102
      IUSE4=103
      IUSE5=104
      IUSE6=105
      IUSE=1
* Collider, 1= CERN, 2= Fermilab, 3= LHC, 4= SSC
      ICOLLI=2
* MC over helicities, effects: Q0,Q2,Q4,Q6
      IHELRN=0
* Set up initializing and final integration parameters
* Matrix element to use: 1: Exact,   2: LO,    3: SPHEL,
*                        5: M^2=1,   6:M^2=1/SHAT**(NJET-2)
      ITMX1=2
      NCALL1=1000
      IMANN1=1
      ITMX2=2
      NCALL2=10000
      IMANN2=1
* Which subprocesses to take along, 0, 2, 4 or 6 quarks.
      ISUB(1)=1
      ISUB(2)=1
      ISUB(3)=1
      ISUB(4)=0
* Generate extensive histograms for subprocesses?
      ISTAT(1)=0
      ISTAT(2)=0
      ISTAT(3)=0
      ISTAT(4)=0
* Generate extensive histograms for initial states, gg, gq and qq?
      IINIT(1)=0
      IINIT(2)=0
      IINIT(3)=0
* Plot histograms in NJETS.OUT?
      DO 12 I=1,40
        IHISTO(I)=1
   12 CONTINUE
* Mass of quarks in final state.
      RMQ=0D0
* To have a massless B Bbar pair in the final state set IMQ=1
      IMQ=0
      IF (RMQ.GT.0D0) IMQ=1
* Dynamical scale: 1. PT-average  2: Total invariant mass
*                  3. Average invariant mass of two outgoing particles
*                  4. Mass of quarks  5: Ptmax in the event
      IQCDDS=1
*
* Error trapping
*
      IF (NJET.GT.8) THEN
         WRITE(*,*) ' You must be kidding!! '
         STOP
      END IF
      IF ((IEFF.EQ.1).AND.( (ISUB(1).EQ.0).OR.(ISUB(2).EQ.1).OR.
     .                      (ISUB(3).EQ.1).OR.(ISUB(4).EQ.1) ) ) THEN
        WRITE(*,*) ' Illegal use of effective gluon structure function.'
        STOP
      END IF
      IF ((IEFF.EQ.2).AND.( (ISUB(1).EQ.1).OR.(ISUB(2).EQ.1) ) ) THEN
        WRITE(*,*) ' Illegal use of effective quark structure function.'
        STOP
      END IF
      IF ((IQCDDS.EQ.4).AND.(RMQ.EQ.0D0)) THEN
        WRITE(*,*) '  Input inconsistent : MQ=0 in QCD scale! '
        STOP
      END IF
      IF ((RMQ.GT.0D0).AND.(ISUB(4).EQ.1)) THEN
        WRITE(*,*) '  Input inconsistent : MQ>0 with 6 quarks.'
        STOP
      END IF
      IF ((RMQ.GT.0D0).AND.((IMANN1.EQ.3).OR.(IMANN2.EQ.3))) THEN
        WRITE(*,*) '  Input inconsistent : MQ>0 with special hels.'
        STOP
      END IF
      IF ((NJET.LT.4).AND.(ISUB(4).NE.0)) THEN
        WRITE(*,*) '  Input inconsistent : NJET<4 & SIX QUARKS ? '
        STOP
      END IF
      IF ( (ISUB(4).EQ.1).AND.(IMANN2.GT.1).AND.(IMANN2.LT.5) ) THEN
        WRITE(*,*) ' Input inconsistent: Final integration. '
        STOP
      END IF
      IF ( ((IMANN1.EQ.2).OR.(IMANN2.EQ.2)).AND.
     .     ((ISUB(3).EQ.1).OR.(ISUB(4).EQ.1))  ) THEN
        WRITE(*,*) ' Input inconsistent: No LO for 4-q or 6q '
        STOP
      END IF
* Number of final state flavours (also used in alphas)
      NF=5
* Which structure function should be examined besides the one used
* for the Monte Carlo integration (set itry(2),...,itry(6)) to requested PDF
      ITRY(1)=IUSE1
      ITRY(2)=IUSE2
      ITRY(3)=IUSE3
      ITRY(4)=IUSE4
      ITRY(5)=IUSE5
      ITRY(6)=IUSE6
* Pick up Monte Carlo parameters.
      IPPBAR=INT(COLLID(1,ICOLLI))
      W=COLLID(2,ICOLLI)
      PTMIN1=COLLID(3,ICOLLI)
      PTMIN2=COLLID(4,ICOLLI)
      ETAMA1=COLLID(5,ICOLLI)
      ETAMA2=COLLID(6,ICOLLI)
      DELTR1=COLLID(7,ICOLLI)
      DELTR2=COLLID(8,ICOLLI)
* Mimimum separation angle of jets, valid when DELTR1 < 0D0
      SEPAN1=COLLID(9,ICOLLI)
      COSTM1=DCOS(SEPAN1*4D0*DATAN(1D0)/180D0)
      SEPAN2=COLLID(10,ICOLLI)
      COSTM2=DCOS(SEPAN2*4D0*DATAN(1D0)/180D0)
      ETSUM=COLLID(11,ICOLLI)
* Histo information
      CMMAX=COLLID(13,ICOLLI)
      ICM=INT(COLLID(14,ICOLLI))
      PTMAX=COLLID(15,ICOLLI)
      IPT=INT(COLLID(16,ICOLLI))
      RM2MAX=COLLID(17,ICOLLI)
      IM2=INT(COLLID(18,ICOLLI))
      POUMAX=COLLID(19,ICOLLI)
      IPOUT=INT(COLLID(20,ICOLLI))
      PETSUM=COLLID(21,ICOLLI)
      IETSUM=INT(COLLID(22,ICOLLI))
      PPTMAX=COLLID(23,ICOLLI)
      IPTMAX=INT(COLLID(24,ICOLLI))
      PSPHER=COLLID(25,ICOLLI)
      ISPHE=INT(COLLID(26,ICOLLI))
      PANGLE=COLLID(27,ICOLLI)
      IANGLE=INT(COLLID(28,ICOLLI))
      PWEIGH=COLLID(29,ICOLLI)
      IWEIGH=INT(COLLID(30,ICOLLI))
* individual PT plots of jets.
      PTMAX2=COLLID(31,ICOLLI)
      IPT2=INT(COLLID(32,ICOLLI))
*
* Issue message about start of program
      WRITE(*,*)
     .' NJETS is a Monte Carlo for QCD backgrounds written by H. Kuijf.'
      WRITE(*,*)
     .  ' Current Version of NJETS: 1.4, dated 26-10-90. '
      WRITE(*,*)
     .  ' E-MAIL address for support:  giele@fnal.gov '
      WRITE(*,*)
     .  ' Output is redirected to NJETS.OUT.'
      WRITE(*,*)
*
* Vegas global parameters
*
      NDIM=2
      DO 16 I=1,2
        XL(I)=0.D0
        XU(I)=1.D0
   16 CONTINUE
      ACC=-1.D0
      NPRN=1
*
* If ITMX1=0 then no initialisation
*
      IF (ITMX1.GT.0) THEN
* Init van der marcks histo routine
        CALL HISTO(1,0,0D0,0D0,0D0,0D0,0,' ',6,0)
        CALL CLRSTS
*
* Initialize grid
        ITMX=ITMX1
        NCALL=NCALL1
        IMANNR=IMANN1
        CALL VEGAS(FXN,AVGI1,SD1,CHI2A1)
      END IF
*
* Final itegrations
* Start counting statistics
      NIN1=0
      NIN2=0
      NOUT=0
* Init van der marcks histo routine
      CALL HISTO(1,0,0D0,0D0,0D0,0D0,0,' ',6,0)
      ITMX=ITMX2
      NCALL=NCALL2
      IMANNR=IMANN2
*
* Clear statistics arrays
      CALL CLRSTS
*
      IF (ITMX1.GT.0) THEN
        CALL VEGAS1(FXN,AVGI2,SD2,CHI2A2)
      ELSE
        CALL VEGAS(FXN,AVGI2,SD2,CHI2A2)
      END IF
*
* Plot extensive output files and standard normal distributions
*
      CALL STAT(ISTAT,ISUB)
*
* Output histograms
*
      CALL HISOUT(1,1)
      IF (ISTAT(1).EQ.1) CALL HISOUT(41,2)
      IF (ISTAT(2).EQ.1) CALL HISOUT(81,3)
      IF (ISTAT(3).EQ.1) CALL HISOUT(121,4)
      IF (ISTAT(4).EQ.1) CALL HISOUT(161,5)
      IF (IINIT(1).EQ.1) CALL HISOUT(201,6)
      IF (IINIT(2).EQ.1) CALL HISOUT(241,7)
      IF (IINIT(3).EQ.1) CALL HISOUT(281,8)
*
      END
