************************************************************************
* NJETSLIB.FOR contains all the subroutines which are needed for       *
* the monte carlo integration, counting, statistics, ..                *
* Version 1.2: 11 - 6 - 1990.                                          *
* In case no statistics are wanted and only the matrix elements are    *
* needed, the main entrance is the subroutine EVENT.                   *
* All the routines in this file above *** stat start  *** line and     *
* the matrix elements in MATLIB.FOR are needed in that case.           *
* So below this line are the integration routines like vegas and the   *
* general routines needed to get the NJETS.FOR shell going.            *
************************************************************************
*
      SUBROUTINE RAMBO(N,ET,XM,P,WT)
***********************************************************************
*                       RAMBO                                         *
*    RA(NDOM)  M(OMENTA)  B(EAUTIFULLY)  O(RGANIZED)                  *
*                                                                     *
*    A DEMOCRATIC MULTI-PARTICLE PHASE SPACE GENERATOR                *
*    AUTHORS:  S.D. ELLIS,  R. KLEISS,  W.J. STIRLING                 *
*    THIS IS VERSION 1.0 -  WRITTEN BY R. KLEISS                      *
*    -- ADJUSTED BY HANS KUIJF, WEIGHTS ARE LOGARITHMIC (20-08-90)    *
*                                                                     *
*    N  = NUMBER OF PARTICLES (>1, IN THIS VERSION <11)               *
*    ET = TOTAL CENTRE-OF-MASS ENERGY                                 *
*    XM = PARTICLE MASSES ( DIM=10 )                                  *
*    P  = PARTICLE MOMENTA ( DIM=(4,10) )                             *
*    WT = WEIGHT OF THE EVENT                                         *
***********************************************************************
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION XM(10),P(4,10),Q(4,10),Z(10),R(4),
     .   B(3),P2(10),XM2(10),E(10),V(10),IWARN(5)
      SAVE ACC,ITMAX,IBEGIN,IWARN
      DATA ACC/1.D-14/,ITMAX/6/,IBEGIN/0/,IWARN/5*0/
*
* INITIALIZATION STEP: FACTORIALS FOR THE PHASE SPACE WEIGHT
      IF(IBEGIN.NE.0) GOTO 103
      IBEGIN=1
      TWOPI=8.*DATAN(1.D0)
      PO2LOG=LOG(TWOPI/4.)
      Z(2)=PO2LOG
      DO 101 K=3,10
  101 Z(K)=Z(K-1)+PO2LOG-2.*LOG(DFLOAT(K-2))
      DO 102 K=3,10
  102 Z(K)=(Z(K)-LOG(DFLOAT(K-1)))
*
* CHECK ON THE NUMBER OF PARTICLES
  103 IF(N.GT.1.AND.N.LT.101) GOTO 104
      PRINT 1001,N
      STOP
*
* CHECK WHETHER TOTAL ENERGY IS SUFFICIENT; COUNT NONZERO MASSES
  104 XMT=0.
      NM=0
      DO 105 I=1,N
      IF(XM(I).NE.0.D0) NM=NM+1
  105 XMT=XMT+ABS(XM(I))
      IF(XMT.LE.ET) GOTO 201
      PRINT 1002,XMT,ET
      STOP
*
* THE PARAMETER VALUES ARE NOW ACCEPTED
*
* GENERATE N MASSLESS MOMENTA IN INFINITE PHASE SPACE
  201 DO 202 I=1,N
         r1=rn(1)
      C=2.*r1-1.
      S=SQRT(1.-C*C)
      F=TWOPI*RN(2)
      r1=rn(3)
      r2=rn(4)
      Q(4,I)=-LOG(r1*r2)
      Q(3,I)=Q(4,I)*C
      Q(2,I)=Q(4,I)*S*COS(F)
  202 Q(1,I)=Q(4,I)*S*SIN(F)
*
* CALCULATE THE PARAMETERS OF THE CONFORMAL TRANSFORMATION
      DO 203 I=1,4
  203 R(I)=0.
      DO 204 I=1,N
      DO 204 K=1,4
  204 R(K)=R(K)+Q(K,I)
      RMAS=SQRT(R(4)**2-R(3)**2-R(2)**2-R(1)**2)
      DO 205 K=1,3
  205 B(K)=-R(K)/RMAS
      G=R(4)/RMAS
      A=1./(1.+G)
      X=ET/RMAS
*
* TRANSFORM THE Q'S CONFORMALLY INTO THE P'S
      DO 207 I=1,N
      BQ=B(1)*Q(1,I)+B(2)*Q(2,I)+B(3)*Q(3,I)
      DO 206 K=1,3
  206 P(K,I)=X*(Q(K,I)+B(K)*(Q(4,I)+A*BQ))
  207 P(4,I)=X*(G*Q(4,I)+BQ)
*
* CALCULATE WEIGHT AND POSSIBLE WARNINGS
      WT=PO2LOG
      IF(N.NE.2) WT=(2.*N-4.)*LOG(ET)+Z(N)
      IF(WT.GE.-180.D0) GOTO 208
      IF(IWARN(1).LE.5) PRINT 1004,WT
      IWARN(1)=IWARN(1)+1
  208 IF(WT.LE. 174.D0) GOTO 209
      IF(IWARN(2).LE.5) PRINT 1005,WT
      IWARN(2)=IWARN(2)+1
*
* RETURN FOR WEIGHTED MASSLESS MOMENTA
  209 IF(NM.NE.0) GOTO 210
* RETURN LOG OF WEIGHT
      WT=WT
      RETURN
*
* MASSIVE PARTICLES: RESCALE THE MOMENTA BY A FACTOR X
  210 XMAX=SQRT(1.-(XMT/ET)**2)
      DO 301 I=1,N
      XM2(I)=XM(I)**2
  301 P2(I)=P(4,I)**2
      ITER=0
      X=XMAX
      ACCU=ET*ACC
  302 F0=-ET
      G0=0.
      X2=X*X
      DO 303 I=1,N
      E(I)=SQRT(XM2(I)+X2*P2(I))
      F0=F0+E(I)
  303 G0=G0+P2(I)/E(I)
      IF(ABS(F0).LE.ACCU) GOTO 305
      ITER=ITER+1
      IF(ITER.LE.ITMAX) GOTO 304
      PRINT 1006,ITMAX
      GOTO 305
  304 X=X-F0/(X*G0)
      GOTO 302
  305 DO 307 I=1,N
      V(I)=X*P(4,I)
      DO 306 K=1,3
  306 P(K,I)=X*P(K,I)
  307 P(4,I)=E(I)
*
* CALCULATE THE MASS-EFFECT WEIGHT FACTOR
      WT2=1.
      WT3=0.
      DO 308 I=1,N
      WT2=WT2*V(I)/E(I)
  308 WT3=WT3+V(I)**2/E(I)
      WTM=(2.*N-3.)*LOG(X)+LOG(WT2/WT3*ET)
*
* RETURN FOR  WEIGHTED MASSIVE MOMENTA
      WT=WT+WTM
      IF(WT.GE.-180.D0) GOTO 309
      IF(IWARN(3).LE.5) PRINT 1004,WT
      IWARN(3)=IWARN(3)+1
  309 IF(WT.LE. 174.D0) GOTO 310
      IF(IWARN(4).LE.5) PRINT 1005,WT
      IWARN(4)=IWARN(4)+1
* RETURN LOG OF WEIGHT
  310 WT=WT
      RETURN
*
 1001 FORMAT(' RAMBO FAILS: # OF PARTICLES =',I5,' IS NOT ALLOWED')
 1002 FORMAT(' RAMBO FAILS: TOTAL MASS =',D15.6,' IS NOT',
     . ' SMALLER THAN TOTAL ENERGY =',D15.6)
 1004 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY UNDERFLOW')
 1005 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY  OVERFLOW')
 1006 FORMAT(' RAMBO WARNS:',I3,' ITERATIONS DID NOT GIVE THE',
     . ' DESIRED ACCURACY =',D15.6)
      END
*
************************************************************************
* SUBROUTINE EVENT(Y,WEIGHT,WGT,IPASS) is the central routine in the   *
* Monte Carlo over the phase space. It can also be used separately.    *
* However, a number of variables must be initialised before a call can *
* be made. They are explained below.                                   *
************************************************************************
* General: EVENT generates an event using RAMBO and two input random   *
* number for parton momentum fractions. It then checks for the cuts    *
* and calls the proper matrix elements in the proper approximation.    *
************************************************************************
* Parameters: Y(10), vegas N-dimensional random point                  *
*             WGT, vegas weight                                        *
*             WEIGHT, mote carlo result for phase space point          *
*             IPASS, is the event acceptable, ie passed the cuts       *
*                   = 0 event rejected by cut-routine                  *
*                   = 1 event accepted by falls outside secondary cuts *
*                   = 2 event passed cuts and WEIGHT is determined.    *
************************************************************************
* Input settings:                                                      *
*   NJET    = Number of final state jets                               *
*   NF      = Number of flavours to use in the calculation (NF=5)      *
*   IUSE    = Which of the possible 6 PD to use for the WEIGHT,        *
*             other can be used to keep statistics, just for comparison*
*   IEFF    = 0 , no structure function approximation                  *
*             1 , effective gluon sf (use gluon matrix element)        *
*             2 , effective quark sf (use quark matrix element)        *
*   IHELRN  = 0/1/ , no/do a/ monte carlo over helicities              *
*   ISUB(4) = 0/1/ take/dont take/ the 0, 2, 4, and 6 quark processes  *
*             when determining the WEIGHT, eg ISUB(1)=1, ISUB(2)=1     *
*             ISUB(3)=0, ISUB(4)=0 takes only the purely gluon and the *
*             subprocesses with two quarks.                            *
*   RMQ     = Mass of quarks.                                          *
*   IMQ     = 0 no massive quarks in final state                       *
*           = 1 massive quarks or a massless bottom pair in final state*
*   IQCDDS  = Dynamic QCD scale.                                       *
*             1= PT average, 2= What, 3= Average invariant two particle*
*             mass. 4= RMQ, 5= Pt maximum                              *
*   IPPBAR  = 0 proton-proton collider, 1 p pbar                       *
*   W       = collider CM energy                                       *
*   PTMIN1  = Minimum PT per jet                                       *
*   PTMIN2  = Maximum PT per jet                                       *
*   ETAMA1  = Maximum rapidity (absolute) valid when DELTR1=-1.0       *
*   ETAMA2  = Minimum rapidity (absolute) valid when DELTR1=-1.0       *
*   DELTR1  = Minimum DELTA R                                          *
*   DELTR2  = Maximum DELTA R                                          *
*   COSTM1  = Minimum opening angle between jets.                      *
*   COSTM2  = Maximum opening angle between jets.                      *
*   ETSUM   = Minimum total transverse energy.                         *
*   IMANNR  = Method of calculation of the matrix elements.            *
*             1 = Exact, 2= LO in colour (gluonic only)                *
*             3 = Special helicity                                     *
*             5 = M^2 =1, 6 = M^2 = 1/SHAT**(NJET-2)                   *
************************************************************************
* In case statistics over subprocesses are wanted:                     *
*    Init statistic array with: CALL CLRSTS                            *
*    an end the MC run with a : CALL STAT(ISTAT,ISUB)                  *
************************************************************************
      SUBROUTINE EVENT(Y,WEIGHT,WGT,IPASS)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION Y(10),M(10)
      REAL*8 AM(100),PJ(4,100),PBOO(4),PL(4),PCM(4)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /CUTVAL/ PTMIN1,ETAMA1,COSTM1,DELTR1,ETSUM,
     .                PTMIN2,ETAMA2,COSTM2,DELTR2
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /BINVAR/ PT(10),ETA(10),DR(10,10),DRR(10,10),ETOT,POUT
      COMMON /BINVA2/ SUMET,PTNORM,SPH,ANGLE(10,10),Y1,Y2,PTSORT(10)
      SAVE /BINVAR/,/BINVA2/,/MOM/,TWOPI,GEVNB
      DATA TWOPI,GEVNB/6.2831853D0,389385.73D0/
      WEIGHT=0.D0
*
* Mapping of X1 and X2 for efficient integration
*
      EPS=(PTMIN1*NJET/W)**2+4D0*RMQ**2/W**2
      Y1=Y(1)
      Y2=Y(2)
      CALL PARTON(Y(1),Y(2),EPS,X1,X2,WTPAR)
      WHAT=W*SQRT(X1*X2)
*
* Generate event
      PLAB(1,1)=0.D0
      PLAB(2,1)=0.D0
      PLAB(3,1)=.5*W*X1
      PLAB(4,1)=.5*W*X1
      PLAB(1,2)=0.D0
      PLAB(2,2)=0.D0
      PLAB(3,2)=-.5*W*X2
      PLAB(4,2)=.5*W*X2
      AM(1)=RMQ
      AM(2)=RMQ
      AM(3)=0D0
      AM(4)=0D0
      AM(5)=0D0
      CALL RAMBO(NJET,WHAT,AM,PJ,WTPS)
      WTPS=WTPS+LOG(TWOPI**(4.D0-3.D0*DFLOAT(NJET)))
* Boost jet momenta back to the lab frame
      DO 100 I=1,4
  100   PBOO(I)=PLAB(I,1)+PLAB(I,2)
        DO 200 J=1,NJET
          DO 300 I=1,4
            PCM(I)=PJ(I,J)
  300     CONTINUE
          CALL BOOST(WHAT,PBOO,PCM,PL)
          DO 400 I=1,4
  400       PLAB(I,J+2)=PL(I)
  200   CONTINUE
*
* Check whether event passes the cuts
      CALL CUTS(IPASS)
      IF(IPASS.LT.2) RETURN
* DETERMINE ANGLES BETWEEN JETS IN CM ORDERED TO PT IN LABFRAME
      DO 4 I=1,NJET
       M(I)=I
    4 CONTINUE
      DO 5 I=1,NJET-1
       DO 5 J=I+1,NJET
         PTI=SQRT(PLAB(1,M(I)+2)**2+PLAB(2,M(I)+2)**2)
         PTJ=SQRT(PLAB(1,M(J)+2)**2+PLAB(2,M(J)+2)**2)
         IF (PTJ.GT.PTI) THEN
           ITEMP=M(I)
           M(I)=M(J)
           M(J)=ITEMP
         END IF
    5 CONTINUE
      DO 6 I=1,NJET-1
        DO 6 J=I+1,NJET
          ANGLE(I,J)= (+PJ(1,M(I))*PJ(1,M(J))
     .                 +PJ(2,M(I))*PJ(2,M(J))
     .                 +PJ(3,M(I))*PJ(3,M(J)))
     .                   /(PJ(4,M(I))*PJ(4,M(J)))
    6 CONTINUE
* Sphericity
*      CALL SPHNEW(PJ,NJET,SPH)
      SPH=0.5D0
* Calculate the dynamical scale
      ETOT=0.D0
      ETMAX=0.D0
      DO 60 J=1,NJET
        PTSORT(J)=PT(J)
        IF(PT(J).GT.ETMAX) ETMAX = PT(J)
        ETOT=ETOT+PT(J)
   60 CONTINUE
*
      DO 62 I=1,NJET-1
        DO 62 J=I+1,NJET
          IF (PTSORT(I).LT.PTSORT(J)) THEN
            TEMP=PTSORT(I)
            PTSORT(I)=PTSORT(J)
            PTSORT(J)=TEMP
          END IF
   62 CONTINUE
*
      IF (IQCDDS.EQ.1) Q=ETOT/DFLOAT(NJET)
      IF (IQCDDS.EQ.2) Q=WHAT
      IF (IQCDDS.EQ.3) Q=AVEMIJ(PLAB,NJET)
      IF (IQCDDS.EQ.4) Q=RMQ
      IF (IQCDDS.EQ.5) Q=ETMAX
* Flux factor
      FLUX=.5D0/(X1*X2*W*W)
*
* Calculate structure functions, matrix elements, process statistics.
      WTMC=LOG(WTPAR*FLUX*GEVNB*WGT)
      WTMC=EXP(WTMC+WTPS)
      WEIGHT=SFME(WTMC)
      RETURN
      END
*
      SUBROUTINE PARTON(R1,R2,EPS,X1,X2,WT)
      IMPLICIT REAL*8(A-H,O-Z)
      X1=EPS**(R2*R1)
      X2=EPS**(R2*(1.D0-R1))
      WT=R2*EPS**R2*LOG(EPS)**2
      RETURN
      END
*
      FUNCTION AVEMIJ(PLAB,NJET)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION PLAB(4,*)
*
      ICOUNT=0
      RMTOT=0D0
      DO 10 I=3,2+NJET
        DO 10 J=I+1,2+NJET
          RMIJ=(PLAB(4,I)+PLAB(4,J))**2
     .        -(PLAB(3,I)+PLAB(3,J))**2
     .        -(PLAB(2,I)+PLAB(2,J))**2
     .        -(PLAB(1,I)+PLAB(1,J))**2
          RMTOT=RMTOT+SQRT(RMIJ)
          ICOUNT=ICOUNT+1
   10 CONTINUE
*
      AVEMIJ=RMTOT/ICOUNT
*
      END
C
* CUTS implements some detector cuts.
* For each jet:
* Test: a) Transverse Momenta. > PTMIN1    < PTMIN2
*       b) Rapidity. < ETAMA1    > ETAMA2
*       c) delta R > DELTR1 or COS(Angle with other jets). < COSTX1
*          or R < DELTR2      COS > COSTX2
*       d) minimal total transverse energy
*
* In addition calculate some kinmatical properties of the event.
*       a) POUT
*
      SUBROUTINE CUTS(ICUT)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /CUTVAL/ PTMIN1,ETAMA1,COSTM1,DELTR1,ETSUM,
     .                PTMIN2,ETAMA2,COSTM2,DELTR2
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /BINVAR/ PT(10),ETA(10),DR(10,10),DRR(10,10),ETOT,POUT
      COMMON /BINVA2/ SUMET,PTNORM,SPH,ANGLE(10,10),Y1,Y2,PTSORT(10)
      DIMENSION PN(3)
      SAVE /MOM/,/BINVAR/,/BINVA2/
*
      ICUT=0
      ITEL=0
      PTSUM=0D0
      DO 10 I=1,NJET
        PT(I)=SQRT(PLAB(1,I+2)**2+PLAB(2,I+2)**2)
        PTSUM=PTSUM+PT(I)
        IF (PT(I).LT.PTMIN1) RETURN
        IF (PT(I).GT.PTMIN2) ITEL=ITEL+1
*
        ETA(I)=0.5D0*LOG((PLAB(4,I+2)+PLAB(3,I+2))
     .                   /(PLAB(4,I+2)-PLAB(3,I+2)))
        IF (ABS(ETA(I)).GT.ETAMA1) RETURN
        IF (ABS(ETA(I)).LT.ETAMA2) ITEL=ITEL+1
        DO 20 J=I+1,NJET
          DR(I,J)=(PLAB(1,I+2)*PLAB(1,J+2)+PLAB(2,I+2)*PLAB(2,J+2)+
     .             PLAB(3,I+2)*PLAB(3,J+2))/(PLAB(4,I+2)*PLAB(4,J+2))
          DR(J,I)=DR(I,J)
          DRR(I,J)=R(I+2,J+2)
          DRR(J,I)=DRR(I,J)
          IF (DELTR1.GE.0D0) THEN
            IF (DRR(I,J).LT.DELTR1) RETURN
            IF (DRR(I,J).GT.DELTR2) ITEL=ITEL+1
          ELSE
            IF (DR(I,J).GT.COSTM1) RETURN
            IF (DR(I,J).LT.COSTM2) ITEL=ITEL+1
          END IF
  20    CONTINUE
  10  CONTINUE
      IF (PTSUM.LT.ETSUM) RETURN
* Event Passes Cuts, now check for integration tricks
      ICUT=1
      IF (ITEL.GT.0) RETURN
* Test inbetween cut, does the event pass it?
      ICUT=2
* Calculate POUT
      IMAX=0
      PTMAX=0D0
      DO 30 I=1,NJET
        IF (PT(I).GT.PTMAX) THEN
          IMAX=I
          PTMAX=PT(I)
        END IF
   30 CONTINUE
* Beam is in the z-direction
      SNORM=SQRT(PLAB(2,IMAX+2)**2+PLAB(1,IMAX+2)**2)
      PN(1)=PLAB(2,IMAX+2)/SNORM
      PN(2)=-PLAB(1,IMAX+2)/SNORM
      PN(3)=0D0
      POUT=0D0
      DO 40 I=1,NJET
        IF (I.NE.IMAX) THEN
          POUT=POUT+0.5D0*ABS(+PN(1)*PLAB(1,I+2)
     .                         +PN(2)*PLAB(2,I+2)
     .                         +PN(3)*PLAB(3,I+2))
        END IF
   40 CONTINUE
* SUM ET, PTNORM
      PTX=0.0
      PTY=0.0
      SUMET=0.0
      DO 50 I=1,NJET
        PTX=PTX+PLAB(1,I+2)
        PTY=PTY+PLAB(2,I+2)
        SUMET=SUMET+SQRT(PLAB(1,I+2)**2+PLAB(2,I+2)**2)
   50 CONTINUE
      PTNORM=SQRT(PTX**2+PTY**2)
      RETURN
      END
*
      FUNCTION R(I,J)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /MOM/ PLAB(4,10),WHAT
      SAVE /MOM/
      R1= (PLAB(4,I)+PLAB(3,I))*(PLAB(4,J)-PLAB(3,J))/
     .   ((PLAB(4,J)+PLAB(3,J))*(PLAB(4,I)-PLAB(3,I)))
      DELY=.5*LOG(R1)
      R2= (PLAB(1,I)*PLAB(1,J)+PLAB(2,I)*PLAB(2,J))
     .   /SQRT((PLAB(1,I)**2+PLAB(2,I)**2)*(PLAB(1,J)**2+PLAB(2,J)**2))
      IF(R2.LT.-0.999999999D0) R2=-1D0
      DELFI=ACOS(R2)
      R=SQRT(DELY**2+DELFI**2)
      RETURN
      END
*
      SUBROUTINE BOOST(Q,PBOO,PCM,PLB)
      REAL*8 PBOO(4),PCM(4),PLB(4),Q,FACT
      PLB(4)=(PBOO(4)*PCM(4)+PBOO(3)*PCM(3)
     &       +PBOO(2)*PCM(2)+PBOO(1)*PCM(1))/Q
      FACT=(PLB(4)+PCM(4))/(Q+PBOO(4))
      DO 10 J=1,3
        PLB(J)=PCM(J)+FACT*PBOO(J)
   10 CONTINUE
      END
*
************************************************************************
* REAL*8 FUNCTION SFME(WTMC) calls the various matrix elements for     *
* the event in PLAB. It returns the common /SUBPRO/ and the Monte      *
* Carlo weigth as the function value.                                  *
*                                                                      *
* Remarks:                                                             *
* Q6-process: IMANNR=1,2,3,4 are treated the same way, so when           *
* initializing the grid with 3, the Q6-process should be switched off. *
*                                                                      *
************************************************************************
      FUNCTION SFME(WTMC)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /BVEG1/  XL(10),XU(10),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON /PROCES/ ISUB(4),ISTAT(4),IINIT(3),IHISTO(40)
      COMMON /SUBPRO/ SFQ0,SFQ2,SFQ4,SFQ6,SINIT(5)
      COMMON /Q0STAT/ TQ0(1,6) ,TFLQ0(5,6)
      COMMON /Q2STAT/ TQ2(4,6) ,TFLQ2(5,6)
      COMMON /Q4STAT/ TQ4(11,6),TFLQ4(5,6)
      COMMON /Q6STAT/ TQ6(19,6),TFLQ6(5,6)
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /STRUCF/ SF(7,6),SFEFF(6),ALPHAS(6)
      DIMENSION TQ0L(1,6),IFLTQ0(1),TQ2L(4,6),IFLTQ2(4)
      DIMENSION TQ4L(11,6),IFLTQ4(11),TQ6L(19,6),IFLTQ6(19)
      SAVE /BVEG1/,/SUBPRO/,/Q0STAT/,/Q2STAT/,/Q4STAT/,/Q6STAT/
     .    ,/STRUCF/,IFLTQ0,IFLTQ2,IFLTQ4,IFLTQ6,FORPI,INIT
      DATA IFLTQ0/1/
      DATA IFLTQ2/5,3,2,1/
      DATA IFLTQ4/5,5,4,5,4,3,3,2,2,1,1/
      DATA IFLTQ6/5,5,5,5,4,4,4,4,5,5,5,2,2,2,2,3,3,3,3/
      DATA FORPI,INIT /1.2566371D1,1/
*
      IF (INIT.EQ.1) THEN
* Structure functions scales:
        DO I=1,6
           ISTRUC=ITRY(I)
           if (istruc.eq.0) qcdl=0D0
           if (istruc.eq.1) qcdl=0.1d0
           if (istruc.eq.2) qcdl=0.2d0
           if (istruc.eq.3) qcdl=0.19d0
           if (istruc.eq.4) qcdl=0.19d0
           if (istruc.eq.5) qcdl=0.19d0
           if (istruc.eq.6) qcdl=0.19d0
           if (istruc.eq.7) qcdl=0.19d0
           if (istruc.eq.8) qcdl=0.144d0
           if (istruc.eq.9) qcdl=0.155d0
           if (istruc.eq.10) qcdl=0.194d0
           if (istruc.eq.11) qcdl=0.191d0
           if (istruc.eq.12) qcdl=0.237d0
           if (istruc.eq.13) qcdl=0.215d0
           if (istruc.eq.14) qcdl=0.215d0
           if (istruc.eq.15) qcdl=0.215d0
           if (istruc.eq.16) qcdl=0.230d0
           if (istruc.eq.17) qcdl=0.230d0
           if (istruc.eq.18) qcdl=0.230d0
           if (istruc.eq.19) qcdl=0.168d0
           if (istruc.eq.20) qcdl=0.231d0
           if (istruc.eq.21) qcdl=0.322d0
           if (istruc.eq.22) qcdl=0.231d0
           if (istruc.eq.23) qcdl=0.200d0
           if (istruc.eq.24) qcdl=0.190d0
           if (istruc.eq.25) qcdl=0.213d0
           if (istruc.eq.26) qcdl=0.322d0
           if (istruc.eq.27) qcdl=0.208d0
           if (istruc.eq.28) qcdl=0.208d0
           if (istruc.eq.29) qcdl=0.230d0
           if (istruc.eq.30) qcdl=0.230d0
           if (istruc.eq.31) qcdl=0.255d0
           if (istruc.eq.32) qcdl=0.231d0
           if (istruc.eq.33) qcdl=0.200d0
           if (istruc.eq.34) qcdl=0.177d0
           if (istruc.eq.35) qcdl=0.239d0
           if (istruc.eq.36) qcdl=0.366d0
           if (istruc.eq.37) qcdl=0.542d0
           if (istruc.eq.38) qcdl=0.286d0
           if (istruc.eq.39) qcdl=0.241d0
           if (istruc.eq.40) qcdl=0.344d0
           if (istruc.eq.41) qcdl=0.241d0
           if (istruc.eq.42) qcdl=0.344d0
           if (istruc.eq.43) qcdl=0.300d0
           if (istruc.eq.44) qcdl=0.300d0
           if (istruc.eq.45) qcdl=0.300d0
           if (istruc.eq.46) qcdl=0.300d0
           if (istruc.eq.101) qcdl=0.216d0
           if (istruc.eq.102) qcdl=0.284d0
           if (istruc.eq.103) qcdl=0.366d0
           if (istruc.eq.104) qcdl=0.458d0
           if (istruc.eq.105) qcdl=0.564d0
           if (istruc.eq.111) qcdl=0.150d0
           if (istruc.eq.112) qcdl=0.200d0
           if (istruc.eq.113) qcdl=0.250d0
           if (istruc.eq.114) qcdl=0.300d0
           if (istruc.eq.115) qcdl=0.350d0
           if (istruc.eq.116) qcdl=0.400d0
           if (istruc.eq.121) qcdl=0.100d0
           if (istruc.eq.122) qcdl=0.120d0
           if (istruc.eq.123) qcdl=0.140d0
           if (istruc.eq.124) qcdl=0.180d0
           if (istruc.eq.125) qcdl=0.200d0
           if (istruc.eq.126) qcdl=0.220d0
           if (istruc.eq.127) qcdl=0.260d0
           if (istruc.eq.128) qcdl=0.300d0
           if (istruc.eq.129) qcdl=0.340d0
           if (istruc.eq.130) qcdl=0.215d0
           if (istruc.eq.131) qcdl=0.255d0
           if (istruc.eq.132) qcdl=0.300d0
           if (istruc.eq.133) qcdl=0.348d0
           if (istruc.eq.134) qcdl=0.401d0
           QCDSCA(I)=QCDL
        ENDDO
        INIT=0
      END IF
      CALL PARDEN(IPPBAR)
      DO 5 I=1,6
        ALPHA=1.5*FORPI/((33.-2.*NF)*LOG(Q/QCDSCA(I)))
        ALPHAS(I)=(FORPI*ALPHA)**NJET/X1/X2/4D0
        IF ((IMANNR.EQ.5).OR.(IMANNR.EQ.6)) ALPHAS(I)=1D0/X1/X2
   5  CONTINUE
*
      SFQ0=0D0
      SFQ2=0D0
      SFQ4=0D0
      SFQ6=0D0
      DO 7 I=1,5
        SINIT(I)=0D0
    7 CONTINUE
*
      IF (ISUB(1).EQ.1) THEN
        CALL MEQ0(TOT,TQ0L)
        DO 10 I=1,6
          TQ0(1,I)=TQ0(1,I)+WTMC*TQ0L(1,I)/ITMX
          TFLQ0(IFLTQ0(1),I)=TFLQ0(IFLTQ0(1),I)+WTMC*TQ0L(1,I)/ITMX
          IF (I.EQ.IUSE)
     .      SINIT(IFLTQ0(1))=SINIT(IFLTQ0(1))+WTMC*TQ0L(1,I)/ITMX
   10   CONTINUE
        SFQ0=WTMC*TOT
      END IF
*
      IF (ISUB(2).EQ.1) THEN
        CALL MEQ2(TOT,TQ2L)
        DO 20 I=1,4
          DO 20 J=1,6
            TQ2(I,J)=TQ2(I,J)+WTMC*TQ2L(I,J)/ITMX
            TFLQ2(IFLTQ2(I),J)=TFLQ2(IFLTQ2(I),J)+WTMC*TQ2L(I,J)/ITMX
            IF (J.EQ.IUSE)
     .        SINIT(IFLTQ2(I))=SINIT(IFLTQ2(I))+WTMC*TQ2L(I,J)/ITMX
   20   CONTINUE
        SFQ2=WTMC*TOT
      END IF
*
      IF (ISUB(3).EQ.1) THEN
        CALL MEQ4(TOT,TQ4L)
        INDEX=5
        IF (NJET.EQ.3) INDEX=9
        IF (NJET.GT.3) INDEX=11
        DO 30 I=1,INDEX
          DO 30 J=1,6
            TQ4(I,J)=TQ4(I,J)+WTMC*TQ4L(I,J)/ITMX
            TFLQ4(IFLTQ4(I),J)=TFLQ4(IFLTQ4(I),J)+WTMC*TQ4L(I,J)/ITMX
            IF (J.EQ.IUSE)
     .        SINIT(IFLTQ4(I))=SINIT(IFLTQ4(I))+WTMC*TQ4L(I,J)/ITMX
   30   CONTINUE
        SFQ4=WTMC*TOT
      END IF
*
      IF (ISUB(4).EQ.1) THEN
        CALL MEQ6(TOT,TQ6L)
        INDEX=8*NJET-21
        DO 40 J=1,6
          IF (ITRY(J).NE.0) THEN
           DO 42 I=1,INDEX
             TQ6(I,J)=TQ6(I,J)+WTMC*TQ6L(I,J)/ITMX
             TFLQ6(IFLTQ6(I),J)=TFLQ6(IFLTQ6(I),J)+WTMC*TQ6L(I,J)/ITMX
             IF (J.EQ.IUSE)
     .         SINIT(IFLTQ6(I))=SINIT(IFLTQ6(I))+WTMC*TQ6L(I,J)/ITMX
   42      CONTINUE
          END IF
   40   CONTINUE
        SFQ6=WTMC*TOT
      END IF
*
      SFME=SFQ0+SFQ2+SFQ4+SFQ6
*
      END
*
************************************************************************
* MEQ0 calls the n-gluon matrix element with approximation IMANNR      *
*                                                                      *
* Table of subprocesses TQ0(1,6):                                      *
*  IEFF=0 : gluonic subproces                                          *
*  IEFF=1 : effective structure function approach                      *
*                                                                      *
************************************************************************
      SUBROUTINE MEQ0(TOT,TQ0)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /STRUCF/ SF(7,6),SFEFF(6),ALPHAS(6)
      COMMON /MOM/    PLAB(4,10),WHAT
      DIMENSION TQ0(1,6),OUT(2)
      SAVE /MOM/,/STRUCF/
*
* Colour averaging and bose factor
*
      WT=0D0
      FACT=64D0*IFAC(NJET)
*
* Exact Matrix element
*
      IF ((IMANNR.EQ.1).AND.(IMQ.NE.1)) THEN
        CALL Q0S(PLAB,NJET+2,IHELRN,OUT)
        WT=OUT(1)
      END IF
*
* Matrix element in leading order in colour approximation
*
      IF ((IMANNR.EQ.2).AND.(IMQ.NE.1)) THEN
        CALL Q0S(PLAB,NJET+2,IHELRN,OUT)
        WT=OUT(2)
      END IF
*
* Matrix element according to SPHEL approximation
*
      IF ((IMANNR.EQ.3).AND.(IMQ.NE.1)) THEN
        CALL SPHEL(PLAB,NJET+2,0,0,0,0,OUT,0)
        WT=OUT(1)
      END IF
*
* Matrix element is a constant
*
      IF ((IMANNR.EQ.5).AND.(IMQ.NE.1)) THEN
        WT=1D0
      END IF
*
      IF ((IMANNR.EQ.6).AND.(IMQ.NE.1)) THEN
        WT=1D0/WHAT**(NJET-2)
      END IF
*
* Combine matrix element with Spin and Colour Average
* and Symmetry factors and structure functions.
*
      DO 10 I=1,6
        IF (IEFF.EQ.0) TQ0(1,I)=WT*ALPHAS(I)*SF(7,I)/FACT
        IF (IEFF.EQ.1) TQ0(1,I)=WT*ALPHAS(I)*SFEFF(I)/FACT
   10 CONTINUE
*
      TOT=TQ0(1,IUSE)
*
      END
*
************************************************************************
* MEQ2 calls the various r rb + gluons subprocesses.                   *
* Convention for Massive quarks: particles 3 and 4 are massive quarks. *
*                                                                      *
* Table of subprocesses TQ2(4,6):                                      *
*                      :NJET=2+ N GLUON:                               *
*                1 = QQB -> G G    + N GLUON                           *
*                2 = Q G -> Q G    + N GLUON                           *
*                3 = G Q -> Q G    + N GLUON                           *
*                4 = G G -> Q QB   + N GLUON                           *
************************************************************************
      SUBROUTINE MEQ2(TOT,TQ2)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /STRUCF/ SF(7,6),SFEFF(6),ALPHAS(6)
      COMMON /MOM/    PLAB(4,10),WHAT
      DIMENSION TQ2(4,6),OUT(2)
      SAVE /MOM/,/STRUCF/
*
* Common factor in all subprocesses
*
      IF (IMANNR.LT.3) THEN
        IF (IMQ.EQ.0) THEN
          CALL Q2S(PLAB,NJET+2,2,1,0D0,IHELRN,OUT)
          DO 11 I=1,6
            TQ2(1,I)=SF(3,I)*ALPHAS(I)*OUT(IMANNR)/9D0/IFAC(NJET)
   11     CONTINUE
          CALL Q2S(PLAB,NJET+2,1,3,0D0,IHELRN,OUT)
          DO 12 I=1,6
            TQ2(2,I)=SF(6,I)*ALPHAS(I)*OUT(IMANNR)/24D0/IFAC(NJET-1)
   12     CONTINUE
          CALL Q2S(PLAB,NJET+2,2,3,0D0,IHELRN,OUT)
          DO 13 I=1,6
            TQ2(3,I)=SF(5,I)*ALPHAS(I)*OUT(IMANNR)/24D0/IFAC(NJET-1)
   13     CONTINUE
        END IF
        CALL Q2S(PLAB,NJET+2,3,4,RMQ,IHELRN,OUT)
        DO 14 I=1,6
          TQ2(4,I)=SF(7,I)*ALPHAS(I)*OUT(IMANNR)/64D0/IFAC(NJET-2)
          IF (IMQ.EQ.0) TQ2(4,I)=TQ2(4,I)*NF
   14   CONTINUE
      END IF
*
      IF ((IMANNR.EQ.3).OR.(IMANNR.GT.4)) THEN
        OUT(1)=1D0
        OUT(2)=1D0
        IF (IMANNR.EQ.6) THEN
          OUT(1)=1D0/WHAT**(NJET-2)
          OUT(2)=1D0/WHAT**(NJET-2)
        END IF
        IF (IMQ.EQ.0) THEN
          IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,2,1,0,0,OUT,1)
          DO 21 I=1,6
            TQ2(1,I)=SF(3,I)*ALPHAS(I)*OUT(1)/9.0/IFAC(NJET)
   21     CONTINUE
          IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,1,3,0,0,OUT,1)
          DO 22 I=1,6
            TQ2(2,I)=SF(6,I)*ALPHAS(I)*OUT(1)/24.0/IFAC(NJET-1)
   22     CONTINUE
          IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,2,3,0,0,OUT,1)
          DO 23 I=1,6
            TQ2(3,I)=SF(5,I)*ALPHAS(I)*OUT(1)/24.0/IFAC(NJET-1)
   23     CONTINUE
        END IF
        IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,3,4,0,0,OUT,1)
        DO 24 I=1,6
          TQ2(4,I)=SF(7,I)*ALPHAS(I)*OUT(1)/64.0/IFAC(NJET-2)
          IF (IMQ.EQ.0) TQ2(4,I)=TQ2(4,I)*NF
   24   CONTINUE
      END IF
*
      TOT=0.0
      DO 40 I=1,4
        TOT=TOT+TQ2(I,IUSE)
   40 CONTINUE
*
      END
*
************************************************************************
* MEQ4 calls the various q qb r rb (+ gluons) subprocesses.            *
* Convention for Massive quarks: particles 3 and 4 are massive quarks. *
*                                                                      *
* Table of subprocesses TQ4(11,6):                                     *
*        :NJET=2:            :NJET=3:             :NJET=4 (5):         *
*   1 = QQB -> QQB      1 = QQB -> QQB G     1 = QQB -> QQB G G (G)    *
*   2 = QQB -> RRB      2 = QQB -> RRB G     2 = QQB -> RRB G G (G)    *
*   3 = Q Q -> Q Q      3 = Q Q -> Q Q G     3 = Q Q -> Q Q G G (G)    *
*   4 = QRB -> QRB      4 = QRB -> QRB G     4 = QRB -> QRB G G (G)    *
*   5 = Q R -> Q R      5 = Q R -> Q R G     5 = Q R -> Q R G G (G)    *
*                       6 = Q G -> QQB Q     6 = Q G -> QQB Q G (G)    *
*                       7 = Q G -> RRB Q     7 = Q G -> RRB Q G (G)    *
*                       8 = G Q -> QQB Q     8 = G Q -> QQB Q G (G)    *
*                       9 = G Q -> RRB Q     9 = G Q -> RRB Q G (G)    *
*                                           10 = G G -> QQB QQB (G)    *
*                                           11 = G G -> QQB RRB (G)    *
************************************************************************
      SUBROUTINE MEQ4(TOT,TQ4)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /STRUCF/ SF(7,6),SFEFF(6),ALPHAS(6)
      DIMENSION OUT(2)
      DIMENSION TQ4(11,6)
      SAVE /MOM/,/STRUCF/
*
      IF (IEFF.EQ.2) THEN
       IF (IMANNR.EQ.1) CALL Q4S(PLAB,NJET+2,3,1,0D0,2,4,0D0,IHELRN,OUT)
       IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,3,1,2,4,OUT,2)
       DO 3 I=1,6
         TQ4(4,I)=SFEFF(I)*ALPHAS(I)*OUT(1)/9D0/IFAC(NJET-2)
    3  CONTINUE
       TOT=TQ4(4,IUSE)
       RETURN
      END IF
*
* init for IMANNR=5,6
      OUT(1)=1D0
      OUT(2)=1D0
      IF (IMANNR.EQ.6) THEN
        OUT(1)=1D0/WHAT**(NJET-2)
        OUT(2)=1D0/WHAT**(NJET-2)
      END IF
*
      IF (IMANNR.EQ.1) CALL Q4S(PLAB,NJET+2,2,1,0D0,3,4,RMQ,IHELRN,OUT)
      IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,2,1,3,4,OUT,2)
      DO 11 I=1,6
       IF (IMQ.EQ.0) TQ4(1,I)=SF(3,I)*ALPHAS(I)*OUT(2)/9D0/IFAC(NJET-2)
       TQ4(2,I)=SF(3,I)*ALPHAS(I)*OUT(1)/9D0*(NF-1.0)/IFAC(NJET-2)
       IF (IMQ.EQ.1) TQ4(2,I)=TQ4(2,I)/(NF-1.0)
   11 CONTINUE
      IF (IMQ.EQ.0) THEN
       IF (IMANNR.EQ.1) CALL Q4S(PLAB,NJET+2,1,3,0D0,2,4,0D0,IHELRN,OUT)
       IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,1,3,2,4,OUT,2)
       DO 12 I=1,6
         TQ4(3,I)=SF(1,I)*ALPHAS(I)*OUT(2)/2D0/9D0/IFAC(NJET-2)
         TQ4(5,I)=SF(2,I)*ALPHAS(I)*OUT(1)/9D0/IFAC(NJET-2)
   12  CONTINUE
       IF (IMANNR.EQ.1) CALL Q4S(PLAB,NJET+2,3,1,0D0,2,4,0D0,IHELRN,OUT)
       IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,3,1,2,4,OUT,2)
       DO 13 I=1,6
         TQ4(4,I)=SF(4,I)*ALPHAS(I)*OUT(1)/9D0/IFAC(NJET-2)
   13  CONTINUE
      END IF
*
      IF (NJET.GT.2) THEN
       IF (IMANNR.EQ.1) CALL Q4S(PLAB,NJET+2,5,1,0D0,3,4,RMQ,IHELRN,OUT)
       IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,5,1,3,4,OUT,2)
       DO 14 I=1,6
         IF (IMQ.EQ.0)
     .      TQ4(6,I)=SF(6,I)*ALPHAS(I)*OUT(2)/2D0/24D0/IFAC(NJET-3)
         TQ4(7,I)=SF(6,I)*ALPHAS(I)*OUT(1)/24D0*(NF-1.0)/IFAC(NJET-3)
         IF (IMQ.EQ.1) TQ4(7,I)=TQ4(7,I)/(NF-1.0)
   14  CONTINUE
*
       IF (IMANNR.EQ.1) CALL Q4S(PLAB,NJET+2,5,2,0D0,3,4,RMQ,IHELRN,OUT)
       IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,5,2,3,4,OUT,2)
       DO 15 I=1,6
         IF (IMQ.EQ.0)
     .     TQ4(8,I)=SF(5,I)*ALPHAS(I)*OUT(2)/2D0/24D0/IFAC(NJET-3)
         TQ4(9,I)=SF(5,I)*ALPHAS(I)*OUT(1)/24D0*(NF-1.0)/IFAC(NJET-3)
         IF (IMQ.EQ.1) TQ4(9,I)=TQ4(9,I)/(NF-1.0)
   15  CONTINUE
      END IF
*
      IF (NJET.GT.3) THEN
       IF (IMANNR.EQ.1) CALL Q4S(PLAB,NJET+2,3,4,RMQ,5,6,0D0,IHELRN,OUT)
       IF (IMANNR.EQ.3) CALL SPHEL(PLAB,NJET+2,3,4,5,6,OUT,2)
       DO 16 I=1,6
         IF (IMQ.EQ.0) TQ4(10,I)=SF(7,I)*ALPHAS(I)*OUT(2)/4D0/81D0*NF
         TQ4(11,I)=SF(7,I)*ALPHAS(I)*OUT(1)/81D0*NF*(NF-1.0)/2.0
         IF (IMQ.EQ.1) TQ4(11,I)=TQ4(11,I)/NF*2.0
   16  CONTINUE
      END IF
*
      IF (NJET.EQ.2) INDEX=5
      IF (NJET.EQ.3) INDEX=9
      IF (NJET.GT.3) INDEX=11
*
      TOT=0D0
      DO 17 I=1,INDEX
        TOT=TOT+TQ4(I,IUSE)
   17 CONTINUE
*
      END
*
************************************************************************
* MEQ6 calls the various q qb r rb s sb (+ gluon) subprocesses.        *
*                                                                      *
* Table of subprocesses TQ6(19,6):                                     *
*           :NJET=4:                         :NJET=5:                  *
*       1 = QQB -> QQB QQB             1 = QQB -> QQB QQB G            *
*       2 = QQB -> QQB RRB             2 = QQB -> QQB RRB G            *
*       3 = QQB -> RRB RRB             3 = QQB -> RRB RRB G            *
*       4 = QQB -> RRB SSB             4 = QQB -> RRB SSB G            *
*       5 = Q Q -> Q Q QQB             5 = Q Q -> Q Q QQB G            *
*       6 = Q Q -> Q Q RRB             6 = Q Q -> Q Q RRB G            *
*       7 = Q R -> Q R QQB             7 = Q R -> Q R QQB G            *
*       8 = Q R -> Q R SSB             8 = Q R -> Q R SSB G            *
*       9 = QRB -> QRB QQB             9 = QRB -> QRB QQB G            *
*      10 = QRB -> QRB RRB            10 = QRB -> QRB RRB G            *
*      11 = QRB -> QRB SSB            11 = QRB -> QRB SSB G            *
*                                     12 = G Q -> QQB QQB Q            *
*                                     13 = G Q -> QQB RRB Q            *
*                                     14 = G Q -> RRB RRB Q            *
*                                     15 = G Q -> RRB SSB Q            *
*                                     16 = Q G -> QQB QQB Q            *
*                                     17 = Q G -> QQB RRB Q            *
*                                     18 = Q G -> RRB RRB Q            *
*                                     19 = Q G -> RRB SSB Q            *
************************************************************************
      SUBROUTINE MEQ6(TOT,TQ6)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /STRUCF/ SF(7,6),SFEFF(6),ALPHAS(6)
      COMMON /MOM/    PLAB(4,10),WHAT
      DIMENSION TQ6(19,6),OUT(5,5)
      SAVE /MOM/,/STRUCF/
*
      IF (IEFF.EQ.2) THEN
        CALL Q6S(PLAB,NJET+2,3,1,2,4,5,6,13,IHELRN,OUT(1,3))
        DO 5 I=1,6
          IF (ITRY(I).NE.0) THEN
            TQ6( 9,I)=SFEFF(I)*ALPHAS(I)*OUT(3,3)/2.0/9.D0
            TQ6(10,I)=SFEFF(I)*ALPHAS(I)*OUT(4,3)/2.0/9.0
            TQ6(11,I)=SFEFF(I)*ALPHAS(I)*OUT(1,3)/9.0    *(NF-2)
          END IF
    5   CONTINUE
        TOT=0.0
        DO 6 I=9,11
          TOT=TOT+TQ6(I,IUSE)
    6   CONTINUE
        RETURN
      END IF
*
* init for IMANNR=5
*
      DO 7 I=1,5
        DO 7 J=1,5
          OUT(I,J)=1D0
          IF (IMANNR.EQ.6) OUT(I,J)=1D0/WHAT**(NJET-2)
    7 CONTINUE
*
      IF (IMANNR.NE.5) THEN
        CALL Q6S(PLAB,NJET+2,2,1,3,4,5,6,27,IHELRN,OUT(1,1))
        CALL Q6S(PLAB,NJET+2,3,1,4,2,5,6,23,IHELRN,OUT(1,2))
        CALL Q6S(PLAB,NJET+2,3,1,2,4,5,6,13,IHELRN,OUT(1,3))
        IF (NJET.EQ.5) THEN
          CALL Q6S(PLAB,NJET+2,3,2,4,5,6,7,27,IHELRN,OUT(1,4))
          CALL Q6S(PLAB,NJET+2,3,1,4,5,6,7,27,IHELRN,OUT(1,5))
        END IF
      END IF
*
      DO 10 I=1,6
        IF (ITRY(I).NE.0) THEN
          TQ6( 1,I)=SF(3,I)*ALPHAS(I)*OUT(5,1)/4.0/9.0
          IF (IMQ.EQ.1) TQ6( 1,I)=0.0
          TQ6( 2,I)=SF(3,I)*ALPHAS(I)*OUT(2,1)/9.0    *(NF-1)
          IF (IMQ.EQ.1) TQ6( 2,I)=0.0
          TQ6( 3,I)=SF(3,I)*ALPHAS(I)*OUT(4,1)/4.0/9.0*(NF-1)
          IF (IMQ.EQ.1) TQ6( 3,I)=0.0
          TQ6( 4,I)=SF(3,I)*ALPHAS(I)*OUT(1,1)/9.0    *(NF-1)*(NF-2)/2.0
          IF (IMQ.EQ.1) TQ6( 4,I)=TQ6( 4,I)/(NF-1)*2.0
          TQ6( 5,I)=SF(1,I)*ALPHAS(I)*OUT(5,2)/6.0/9.0
          IF (IMQ.EQ.1) TQ6( 5,I)=0.0
          TQ6( 6,I)=SF(1,I)*ALPHAS(I)*OUT(2,2)/2.0/9.0*(NF-1)
          IF (IMQ.EQ.1) TQ6( 6,I)=TQ6( 6,I)/(NF-1)
          TQ6( 7,I)=SF(2,I)*ALPHAS(I)*OUT(3,2)/2.0/9.0
          IF (IMQ.EQ.1) TQ6( 7,I)=0.0
          TQ6( 8,I)=SF(2,I)*ALPHAS(I)*OUT(1,2)/9.0    *(NF-2)
          IF (IMQ.EQ.1) TQ6( 8,I)=TQ6( 8,I)/(NF-2)
          TQ6( 9,I)=SF(4,I)*ALPHAS(I)*OUT(3,3)/2.0/9.D0
          IF (IMQ.EQ.1) TQ6( 9,I)=0.0
          TQ6(10,I)=SF(4,I)*ALPHAS(I)*OUT(4,3)/2.0/9.0
          IF (IMQ.EQ.1) TQ6(10,I)=0.0
          TQ6(11,I)=SF(4,I)*ALPHAS(I)*OUT(1,3)/9.0    *(NF-2)
          IF (IMQ.EQ.1) TQ6(11,I)=TQ6(11,I)/(NF-2)
          IF (NJET.EQ.5) THEN
            TQ6(12,I)=SF(5,I)*ALPHAS(I)*OUT(5,4)/12.0/24.0
            IF (IMQ.EQ.1) TQ6(12,I)=0.0
            TQ6(13,I)=SF(5,I)*ALPHAS(I)*OUT(2,4)/2.0/24.0*(NF-1)
            IF (IMQ.EQ.1) TQ6(13,I)=TQ6(13,I)/(NF-1)
            TQ6(14,I)=SF(5,I)*ALPHAS(I)*OUT(4,4)/4.0/24.0*(NF-1)
            IF (IMQ.EQ.1) TQ6(14,I)=0.0
            TQ6(15,I)=SF(5,I)*ALPHAS(I)*OUT(1,4)/24.0*(NF-1)*(NF-2)/2.0
            IF (IMQ.EQ.1) TQ6(15,I)=TQ6(15,I)/(NF-1)*2.0
            TQ6(16,I)=SF(6,I)*ALPHAS(I)*OUT(5,5)/12.0/24.0
            IF (IMQ.EQ.1) TQ6(16,I)=0.0
            TQ6(17,I)=SF(6,I)*ALPHAS(I)*OUT(2,5)/2.0/24.0*(NF-1)
            IF (IMQ.EQ.1) TQ6(17,I)=TQ6(17,I)/(NF-1)
            TQ6(18,I)=SF(6,I)*ALPHAS(I)*OUT(4,5)/4.0/24.0*(NF-1)
            IF (IMQ.EQ.1) TQ6(18,I)=0.0
            TQ6(19,I)=SF(6,I)*ALPHAS(I)*OUT(1,5)/24.0*(NF-1)*(NF-2)/2.0
            IF (IMQ.EQ.1) TQ6(19,I)=TQ6(19,I)/(NF-1.0)*2.0
          END IF
        END IF
   10 CONTINUE
*
      INDEX=8*NJET-21
      TOT=0.0
      DO 16 I=1,INDEX
        TOT=TOT+TQ6(I,IUSE)
   16 CONTINUE
*
      END
*
************************************************************************
* STAT(ISTAT) reports Monte Carlo output in separate file NJETS.OUT    *
*                                                                      *
* This includes:                                                       *
*  - Input parameters for integration, results of integration.         *
*  - Table of the relevant subprocesses for 6 parton densities.        *
*  - Distributions.                                                    *
*  - ISTAT determines whether to print histograms for 0,2,4,6 quarks.  *
*  - The relative contribution of each process.                        *
*                                                                      *
************************************************************************
      SUBROUTINE STAT(ISTAT,ISUB)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION ISTAT(4),ISUB(4)
      CHARACTER*20 SQ0(1,7)
      CHARACTER*20 SQ2(4,7)
      CHARACTER*20 SQ4(11,4),SQ4A(11,3)
      CHARACTER*20 SQ6(19,2),MANNR(6),QCDSC(5)
      CHARACTER*20 SSTRU
      CHARACTER*60 FL(5)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /CUTVAL/ PTMIN1,ETAMA1,COSTM1,DELTR1,ETSUM,
     .                PTMIN2,ETAMA2,COSTM2,DELTR2
      COMMON /EFFIC/  NIN1,NIN2,NOUT
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /INTER/  ITMX1,NCALL1,IMANN1,ITMX2,NCALL2,IMANN2
      COMMON /Q0STAT/ TQ0(1,6) ,TFLQ0(5,6)
      COMMON /Q2STAT/ TQ2(4,6) ,TFLQ2(5,6)
      COMMON /Q4STAT/ TQ4(11,6),TFLQ4(5,6)
      COMMON /Q6STAT/ TQ6(19,6),TFLQ6(5,6)
      COMMON /MCRES/  AVGI1,SD1,CHI2A1,AVGI2,SD2,CHI2A2
      LOGICAL EXISTS
      SAVE /Q0STAT/,/Q2STAT/,/Q4STAT/,/Q6STAT/
     .    ,SQ0,SQ2,SQ4,SQ4A,SQ6,FL,MANNR,QCDSC
      DATA SQ0/' G G -> G G ',' G G -> G G G ',
     .         ' G G -> G G G G ',' G G -> G G G G G ',
     .         ' G G -> GGGGGG ',' G G -> GGGGGGG ',' G G -> GGGGGGGG '/
      DATA SQ2/' QQB -> G G ',' Q G -> Q G ',
     .         ' G Q -> Q G ',' G G -> QQB ',
     .         ' QQB -> G G G ',' Q G -> Q G G ',
     .         ' G Q -> Q G G ',' G G -> QQB G ',
     .         ' QQB -> G G G G ',' Q G -> Q G G G ',
     .         ' G Q -> Q G G G ',' G G -> QQB G G ',
     .         ' QQB -> G G G G G ',' Q G -> Q G G G G ',
     .         ' G Q -> Q G G G G ',' G G -> QQB G G G ',
     .         ' QQB -> GGGGGG ',' Q G -> Q GGGGG ',
     .         ' G Q -> Q GGGGG ',' G G -> QQB GGGG ',
     .         ' QQB -> GGGGGGG ',' Q G -> Q GGGGGG ',
     .         ' G Q -> Q GGGGGG ',' G G -> QQB GGGGG ',
     .         ' QQB -> GGGGGGGG ',' Q G -> Q GGGGGGG ',
     .         ' G Q -> Q GGGGGGG ',' G G -> QQB GGGGGG '/
      DATA SQ4/
     . ' QQB -> QQB ',' QQB -> RRB',' Q Q -> Q Q',
     . ' QRB -> QRB ',' Q R -> Q R',6*' ',
     . ' QQB -> QQB G',' QQB -> RRB G',' Q Q -> Q Q G',
     . ' QRB -> QRB G',' Q R -> Q R G',' Q G -> Q QB Q',
     . ' Q G -> R RB Q',' G Q -> Q QB Q',' G Q -> R RB Q',2*' ',
     . ' QQB -> QQB G G',' QQB -> RRB G G',' Q Q -> Q Q G G',
     . ' QRB -> QRB G G',' Q R -> Q R G G',' Q G -> Q QB Q G',
     . ' Q G -> R RB Q G',' G Q -> Q QB Q G',' G Q -> R RB Q G',
     . ' G G -> Q QB Q QB',' G G -> Q QB R RB',
     . ' QQB -> QQB G G G',' QQB -> RRB G G G',' Q Q -> Q Q G G G',
     . ' QRB -> QRB G G G',' Q R -> Q R G G G',' Q G -> Q QB Q G G',
     . ' Q G -> R RB Q G G',' G Q -> Q QB Q G G',' G Q -> R RB Q G G',
     . ' G G -> Q QB Q QB G',' G G -> Q QB R RB G'/
      DATA SQ4A/
     . ' QQB -> QQB GGGG',' QQB -> RRB GGGG',' Q Q -> Q Q GGGG',
     . ' QRB -> QRB GGGG',' Q R -> Q R GGGG',' Q G -> Q QB Q GGG',
     . ' Q G -> R RB Q GGG',' G Q -> Q QB Q GGG',' G Q -> R RB Q GGG',
     . ' G G -> Q QB Q QB GG',' G G -> Q QB R RB GG',
     . ' QQB -> QQBGGGGG',' QQB -> RRBGGGGG',' Q Q -> QQGGGGG',
     . ' QRB -> QRBGGGGG',' Q R -> Q RGGGGG',' Q G -> QQBQGGGG',
     . ' Q G -> RRBQGGGG',' G Q -> QQBQGGGG',' G Q -> RRBQGGGG',
     . ' G G -> QQBQQBGGG',' G G -> QQBRRBGGG',
     . ' QQB -> QQBGGGGGG',' QQB -> RRBGGGGGG',' Q Q -> QQGGGGGG',
     . ' QRB -> QRBGGGGGG',' Q R -> Q RGGGGGG',' Q G -> QQBQGGGGG',
     . ' Q G -> RRBQGGGGG',' G Q -> Q QBQGGGGG',' G Q -> RRBQGGGGG',
     . ' G G -> QQBQQBGGGG',' G G -> QQBRRBGGGG'/
      DATA SQ6/
     . ' QQB -> QQB QQB',' QQB -> QQB RRB',' QQB -> RRB RRB',
     . ' QQB -> RRB SSB',' Q Q -> Q Q QQB',' Q Q -> Q Q RRB',
     . ' Q R -> Q R QQB',' Q R -> Q R SSB',' QRB -> QRB QQB',
     . ' QRB -> QRB RRB',' QRB -> QRB SSB',8*' ',
     . ' QQB -> QQB QQB G',' QQB -> QQB RRB G',' QQB -> RRB RRB G',
     . ' QQB -> RRB SSB G',' Q Q -> Q Q QQB G',' Q Q -> Q Q RRB G',
     . ' Q R -> Q R QQB G',' Q R -> Q R SSB G',' QRB -> QRB QQB G',
     . ' QRB -> QRB RRB G',' QRB -> QRB SSB G',' G Q -> QQB QQB Q',
     . ' G Q -> QQB RRB Q',' G Q -> RRB RRB Q',' G Q -> RRB SSB Q',
     . ' Q G -> QQB QQB Q',' Q G -> QQB RRB Q',' Q G -> RRB RRB Q',
     . ' Q G -> RRB SSB Q'/
      DATA FL/
     . ' TOTAL GG        ',' TOTAL GQ + GQB  ',' TOTAL QG + QBG  ',
     . ' TOTAL QQ + QBQB ',' TOTAL QQB       '/
      DATA MANNR/ ' EXACT ',' LEADING ORDER ',' SPECIAL HELICITIES',
     .            ' ????? ',' UNITY (M^2=1) ',' UNITY (M^2=f(s) '/
      DATA QCDSC/ ' PT-AVERAGE ',' CM-ENERGY ',
     .            ' Mij-AVERAGE ',' RMQ ',' PT-max '/
*
      INQUIRE(FILE='NJETS.OUT',EXIST=EXISTS)
      IF (.NOT.EXISTS) THEN
        OPEN(4,FILE='NJETS.OUT',STATUS='NEW')
        WRITE(4,*) ' NJETS.OUT '
        CLOSE(4)
      END IF
      OPEN(UNIT=4,FILE='NJETS.OUT',STATUS='OLD',ACCESS='APPEND')
*
* Heading: Author + addresses
*
  311 FORMAT(A)
      WRITE(4,311)
     .' ==============================================================='
      WRITE(4,311)
     .' NJETS is a Monte Carlo for QCD backgrounds written by H. Kuijf.'
      WRITE(4,311)
     .  ' Current Version of NJETS: 1.3, dated 15-09-90. '
      WRITE(4,311)
     .  ' E-MAIL address of author: KUIJF@HLERUL59 '
      WRITE(4,311)
     .' ==============================================================='
      WRITE(4,*)
*
* General information
*
      WRITE(4,311) ' General Monte Carlo parameters.'
      WRITE(4,311) ' ==============================='
      WRITE(4,91) INT(W)
   91 FORMAT(' Hadronic CM energy:          ',1I5,' GeV.')
      WRITE(4,92) NF
   92 FORMAT(' # flavours in Alpha:         ',1I4)
      WRITE(4,93) NJET
   93 FORMAT(' # final state partons:       ',1I4)
      WRITE(4,94) PTMIN1,PTMIN2
   94 FORMAT(' Transverse momentum window: [',1D11.4,',',1D11.4,']')
      WRITE(4,95) ETAMA1,ETAMA2
   95 FORMAT(' |Rapidity| window:          [',1D11.4,',',1D11.4,']')
      IF (DELTR1.GE.0D0) THEN
       WRITE(4,96) DELTR1,DELTR2
   96  FORMAT(' Delta R window:             [',1D11.4,',',1D11.4,']')
      ELSE
       WRITE(4,97) ACOS(COSTM1)*180D0/(4D0*DATAN(1D0)),
     .             ACOS(COSTM2)*180D0/(4D0*DATAN(1D0))
   97  FORMAT(' Separation angle window:    [',1D11.4,',',1D11.4,']')
      ENDIF
      WRITE(4,98) ETSUM
   98 FORMAT(' Min. total transverse energy:',1D11.4)
      IF (IMQ.EQ.1) WRITE(4,99) RMQ
   99 FORMAT(' Tagging for quark pair in final state with mass:',1D11.4)
      WRITE(4,81) QCDSC(IQCDDS)
   81 FORMAT(' Dynamical QCD scale:     ',A15)
      IF(IHELRN.EQ.1) WRITE(4,82)
   82 FORMAT(' ==> Integration is a Monte Carlo over hel. configs.!')
      IF(IEFF.EQ.1) WRITE(4,83)
   83 FORMAT(' ==> Effective gluon structure function was used.')
      IF(IEFF.EQ.2) WRITE(4,84)
   84 FORMAT(' ==> Effective quark structure function was used.')
      WRITE(4,*)
      WRITE(4,311) ' Input parameters of integration.'
      WRITE(4,311) ' ================================'
      IF (ITMX1.GT.0) WRITE(4,101) ITMX1,NCALL1,MANNR(IMANN1)
  101 FORMAT(' Initializing iterations: ',1I3,' of ',1I7,' points ',
     . 'with method ',1A20)
      WRITE(4,102) ITMX2,NCALL2,MANNR(IMANN2)
  102 FORMAT(' Integrating  iterations: ',1I3,' of ',1I7,' points ',
     . 'with method ',1A20)
*
      WRITE(4,*)
      WRITE(4,311) ' Results of Monte Carlo run.'
      WRITE(4,311) ' ==========================='
      WRITE(4,105) NJET,AVGI1,SD1,CHI2A1
  105 FORMAT('      Init: SIGMA(',I2,') =',1D11.4,' +/- ',1D11.4,
     .   ' nb.   ( CHI2/DOF=',1F7.3,') ')
      WRITE(4,106) NJET,AVGI2,SD2,CHI2A2
  106 FORMAT(' ==> Final: SIGMA(',I2,') =',1D11.4,' +/- ',1D11.4,
     .   ' nb.   ( CHI2/DOF=',1F7.3,')')
      EFF=1.0D0*NIN2/(NIN1+NIN2+NOUT)
      WRITE(4,115) NIN1+NIN2
  115 FORMAT(' # accepted events:   ',1I7)
      WRITE(4,116) NIN2
  116 FORMAT(' # calculated events: ',1I7)
      WRITE(4,117) NOUT
  117 FORMAT(' # rejected events:   ',1I7)
      WRITE(4,118) EFF
  118 FORMAT(' Efficiency of event generation: ',1D11.4)
      WRITE(4,*)
*
* Statistics for various subprocesses and parton densities
*
      WRITE(4,311) ' Subprocess          col 1    col 2    col 3    col 4
     .     col 5    col 6  CONT(%)'
      IF (ISUB(1).EQ.1) THEN
        WRITE(4,111) SQ0(1,NJET-1),(TQ0(1,J),J=1,6),
     .                       TQ0(1,IUSE)*100.0/AVGI2
      END IF
      IF (ISUB(2).EQ.1) THEN
        DO 201 I=1,4
          WRITE(4,111) SQ2(I,NJET-1),(TQ2(I,J),J=1,6),
     .                       TQ2(I,IUSE)*100.0/AVGI2
  201   CONTINUE
      END IF
      IF (ISUB(3).EQ.1) THEN
        IF (NJET.EQ.2) INDEX=5
        IF (NJET.EQ.3) INDEX=9
        IF (NJET.GT.3) INDEX=11
        DO 202 I=1,INDEX
          IF (NJET.LT.6) THEN
            WRITE(4,111) SQ4(I,NJET-1),(TQ4(I,J),J=1,6),
     .                         TQ4(I,IUSE)*100.0/AVGI2
          ELSE
            WRITE(4,111) SQ4A(I,NJET-5),(TQ4(I,J),J=1,6),
     .                       TQ4(I,IUSE)*100.0/AVGI2
          END IF
  202   CONTINUE
      END IF
      IF (ISUB(4).EQ.1) THEN
        INDEX=8*NJET-21
        DO 203 I=1,INDEX
          WRITE(4,111) SQ6(I,NJET-3),(TQ6(I,J),J=1,6),
     .                       TQ6(I,IUSE)*100.0/AVGI2
  203   CONTINUE
      END IF
      WRITE(4,*)
      DO 204 I=1,5
        WRITE(4,111) FL(I),((TFLQ0(I,J)+TFLQ2(I,J)+
     .  TFLQ4(I,J)+TFLQ6(I,J)),J=1,6),(TFLQ0(I,IUSE)+TFLQ2(I,IUSE)+
     .  TFLQ4(I,IUSE)+TFLQ6(I,IUSE))*100.0/AVGI2
  204 CONTINUE
  111 FORMAT(1A18,6D9.2,1F7.2)
      WRITE(4,*)
      DO I=1,6
         IF (ITRY(I).NE.0) THEN
            ISTRUC=ITRY(I)
            sstru='non existing ?'
            if (istruc.eq.1)  sstru='mrse    (msbar)'
            if (istruc.eq.2)  sstru='mrsb    (msbar)'
            if (istruc.eq.3)  sstru='kmrshb  (msbar)'
            if (istruc.eq.4)  sstru='kmrsb0  (msbar)'
            if (istruc.eq.5)  sstru='kmrsb-  (msbar)'
            if (istruc.eq.6)  sstru='kmrsb-5 (msbar)'
            if (istruc.eq.7)  sstru='kmrsb-2 (msbar)'
            if (istruc.eq.8)  sstru='mts1    (msbar)'
            if (istruc.eq.9)  sstru='mte1    (msbar)'
            if (istruc.eq.10) sstru='mtb1    (msbar)'
            if (istruc.eq.11) sstru='mtb2    (msbar)'
            if (istruc.eq.12) sstru='mtsn1   (msbar)'
            if (istruc.eq.13) sstru='kmrss0  (msbar)'
            if (istruc.eq.14) sstru='kmrsd0  (msbar)'
            if (istruc.eq.15) sstru='kmrsd-  (msbar)'
            if (istruc.eq.16) sstru='mrss0   (msbar)'
            if (istruc.eq.17) sstru='mrsd0   (msbar)'
            if (istruc.eq.18) sstru='mrsd-   (msbar)'
            if (istruc.eq.19) sstru='cteq1l  (msbar)'
            if (istruc.eq.20) sstru='cteq1m  (msbar)'
            if (istruc.eq.21) sstru='cteq1ml (msbar)'
            if (istruc.eq.22) sstru='cteq1ms (msbar)'
            if (istruc.eq.23) sstru='grv     (msbar)'
            if (istruc.eq.24) sstru='cteq2l  (msbar)'
            if (istruc.eq.25) sstru='cteq2m  (msbar)'
            if (istruc.eq.26) sstru='cteq2ml (msbar)'
            if (istruc.eq.27) sstru='cteq2ms (msbar)'
            if (istruc.eq.28) sstru='cteq2mf (msbar)'
            if (istruc.eq.29) sstru='mrsh    (msbar)'
            if (istruc.eq.30) sstru='mrsa    (msbar)'
            if (istruc.eq.31) sstru='mrsg    (msbar)'
            if (istruc.eq.32) sstru='mrsap   (msbar)'
            if (istruc.eq.33) sstru='grv94   (msbar)'
            if (istruc.eq.34) sstru='cteq3l  (msbar)'
            if (istruc.eq.35) sstru='cteq3m  (msbar)'
            if (istruc.eq.36) sstru='mrsj    (msbar)'
            if (istruc.eq.37) sstru='mrsjp   (msbar)'
            if (istruc.eq.38) sstru='cteqjet (msbar)'
            if (istruc.eq.39) sstru='mrsr1   (msbar)'
            if (istruc.eq.40) sstru='mrsr2   (msbar)'
            if (istruc.eq.41) sstru='mrsr3   (msbar)'
            if (istruc.eq.42) sstru='mrsr4   (msbar)'
            if (istruc.eq.43) sstru='cteq4l  (msbar)'
            if (istruc.eq.44) sstru='cteq4m  (msbar)'
            if (istruc.eq.45) sstru='cteq4lq (msbar)'
            if (istruc.eq.46) sstru='cteq4lq (msbar)'
            if (istruc.eq.101) sstru='mrsap (Lambda=0.216)'
            if (istruc.eq.102) sstru='mrsap (Lambda=0.284)'
            if (istruc.eq.103) sstru='mrsap (Lambda=0.366)'
            if (istruc.eq.104) sstru='mrsap (Lambda=0.458)'
            if (istruc.eq.105) sstru='mrsap (Lambda=0.564)'
            if (istruc.eq.111) sstru='grv94 (Lambda=0.150)'
            if (istruc.eq.112) sstru='grv94 (Lambda=0.200)'
            if (istruc.eq.113) sstru='grv94 (Lambda=0.250)'
            if (istruc.eq.114) sstru='grv94 (Lambda=0.300)'
            if (istruc.eq.115) sstru='grv94 (Lambda=0.350)'
            if (istruc.eq.116) sstru='grv94 (Lambda=0.400)'
            if (istruc.eq.121) sstru='cteq3m (msbar)'
            if (istruc.eq.122) sstru='cteq3m (msbar)'
            if (istruc.eq.123) sstru='cteq3m (msbar)'
            if (istruc.eq.124) sstru='cteq3m (msbar)'
            if (istruc.eq.125) sstru='cteq3m (msbar)'
            if (istruc.eq.126) sstru='cteq3m (msbar)'
            if (istruc.eq.127) sstru='cteq3m (msbar)'
            if (istruc.eq.128) sstru='cteq3m (msbar)'
            if (istruc.eq.129) sstru='cteq3m (msbar)'
            if (istruc.eq.130) sstru='cteq4m (msbar)'
            if (istruc.eq.131) sstru='cteq4m (msbar)'
            if (istruc.eq.132) sstru='cteq4m (msbar)'
            if (istruc.eq.133) sstru='cteq4m (msbar)'
            if (istruc.eq.134) sstru='cteq4m (msbar)'
            if (i.eq.iuse) then
               write(4,*) 'column',i,' uses ',sstru,
     .                    '(-> used during integration)'
            else
               write(4,*) 'column',i,' uses ',sstru
            endif
         endif
      enddo
      WRITE(4,*)
*
      CLOSE(4)
*
      END
*
************************************************************************
* PARDEN calculates the parton density functions.                      *
*                                                                      *
* Combinations:  SF(1,i) = Q Q and QB QB                               *
*                SF(2,i) = Q R and QB RB                               *
*                SF(3,i) = Q QB                                        *
*                SF(4,i) = Q RB and R QB                               *
*                SF(5,i) = G Q and G QB                                *
*                SF(6,i) = Q G and QB G                                *
*                SF(7,i) = G G                                         *
*                SFEFF(i)= Effective structure functions (GG-process)  *
* IPPB=1, collider is proton-antiproton type                           *
************************************************************************
      SUBROUTINE PARDEN(IPPB)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /STRUCF/ SF(7,6),SFEFF(6),ALPHAS(6)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      SAVE /STRUCF/
*
      DO 10 I=1,6
        IF (ITRY(I).NE.0) THEN
          I1=ITRY(I)
          CALL STRUCT(X1,Q,I1,UV1,DV1,US1,DS1,STR1,CHM1,BOT1,G1)
          CALL STRUCT(X2,Q,I1,UV2,DV2,US2,DS2,STR2,CHM2,BOT2,G2)
          SF(1,I)=UV1*US2+US1*UV2+DV1*DS2+DS1*DV2+2D0*(US1*US2+DS1*DS2)
     .           +2D0*(STR1*STR2+CHM1*CHM2+BOT1*BOT2)
          SF(2,I)=(UV1+US1+DV1+DS1+STR1+CHM1+BOT1)
     .           *(US2+DS2+STR2+CHM2+BOT2)
     .           +(US1+DS1+STR1+CHM1+BOT1)
     .           *(UV2+US2+DV2+DS2+STR2+CHM2+BOT2)
     .           -SF(1,I)
          SF(3,I)=(UV1+US1)*(UV2+US2)+US1*US2
     .           +(DV1+DS1)*(DV2+DS2)+DS1*DS2
     .            +2D0*(STR1*STR2+CHM1*CHM2+BOT1*BOT2)
          SF(4,I)=(UV1+US1+DV1+DS1+STR1+CHM1+BOT1)
     .           *(UV2+US2+DV2+DS2+STR2+CHM2+BOT2)
     .           +(US1+DS1+STR1+CHM1+BOT1)
     .           *(US2+DS2+STR2+CHM2+BOT2)
     .           -SF(3,I)
          SF(5,I)=G1*(U2+D2+4D0*SEA2+2D0*STR2+2D0*CHM2)
          SF(6,I)=(U1+D1+4D0*SEA1+2D0*STR1+2D0*CHM1)*G2
          SF(7,I)=G1*G2
          IF (IPPB.EQ.0) THEN
            S=SF(1,I)
            SF(1,I)=SF(3,I)
            SF(3,I)=S
            S=SF(2,I)
            SF(2,I)=SF(4,I)
            SF(4,I)=S
          END IF
          IF (IEFF.EQ.1) THEN
            SFEFF(I)=( (SF(1,I)+SF(2,I)+SF(3,I)+SF(4,I))*16D0/81D0
     .              +(SF(5,I)+SF(6,I))*4D0/9D0  +  SF(7,I) )
          ELSE
            SFEFF(I)=( (SF(1,I)+SF(2,I)+SF(3,I)+SF(4,I))
     .              +(SF(5,I)+SF(6,I))*9D0/4D0  +  81D0/16D0*SF(7,I) )
          END IF
        END IF
   10 CONTINUE
*
      END
*
*
      FUNCTION RN(IDUMMY)
      REAL*8 RN,RAN
      SAVE INIT
      DATA INIT /1/
      IF (INIT.EQ.1) THEN
        INIT=0
        CALL RMARIN(1802,9373)
      END IF
*
  10  CALL RANMAR(RAN)
      IF (RAN.LT.1D-16) GOTO 10
      RN=RAN
*
      END
*
      SUBROUTINE RANMAR(RVEC)
*     -----------------
* Universal random number generator proposed by Marsaglia and Zaman
* in report FSU-SCRI-87-50
* In this version RVEC is a double precision variable.
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/ RASET1 / RANU(97),RANC,RANCD,RANCM
      COMMON/ RASET2 / IRANMR,JRANMR
      SAVE /RASET1/,/RASET2/
      UNI = RANU(IRANMR) - RANU(JRANMR)
      IF(UNI .LT. 0D0) UNI = UNI + 1D0
      RANU(IRANMR) = UNI
      IRANMR = IRANMR - 1
      JRANMR = JRANMR - 1
      IF(IRANMR .EQ. 0) IRANMR = 97
      IF(JRANMR .EQ. 0) JRANMR = 97
      RANC = RANC - RANCD
      IF(RANC .LT. 0D0) RANC = RANC + RANCM
      UNI = UNI - RANC
      IF(UNI .LT. 0D0) UNI = UNI + 1D0
      RVEC = UNI
      END
 
      SUBROUTINE RMARIN(IJ,KL)
*     -----------------
* Initializing routine for RANMAR, must be called before generating
* any pseudorandom numbers with RANMAR. The input values should be in
* the ranges 0<=ij<=31328 ; 0<=kl<=30081
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/ RASET1 / RANU(97),RANC,RANCD,RANCM
      COMMON/ RASET2 / IRANMR,JRANMR
      SAVE /RASET1/,/RASET2/
* This shows correspondence between the simplified input seeds IJ, KL
* and the original Marsaglia-Zaman seeds I,J,K,L.
* To get the standard values in the Marsaglia-Zaman paper (i=12,j=34
* k=56,l=78) put ij=1802, kl=9373
      I = MOD( IJ/177 , 177 ) + 2
      J = MOD( IJ     , 177 ) + 2
      K = MOD( KL/169 , 178 ) + 1
      L = MOD( KL     , 169 )
      DO 300 II = 1 , 97
        S =  0D0
        T = .5D0
        DO 200 JJ = 1 , 24
          M = MOD( MOD(I*J,179)*K , 179 )
          I = J
          J = K
          K = M
          L = MOD( 53*L+1 , 169 )
          IF(MOD(L*M,64) .GE. 32) S = S + T
          T = .5D0*T
  200   CONTINUE
        RANU(II) = S
  300 CONTINUE
      RANC  =   362436D0 / 16777216D0
      RANCD =  7654321D0 / 16777216D0
      RANCM = 16777213D0 / 16777216D0
      IRANMR = 97
      JRANMR = 33
      END
*
*** STAT START ***
*
************************************************************************
* Library routines to complete the NJET monte carlo                    *
*                                                                      *
* This includes                                                        *
*    VEGAS   - Integration routine by Lepage                           *
*    STRUCT  - Parton densities from Stirling group                    *
*              Uses files : MRSEB1.DAT and MRSEB2.DAT                  *
*    HISTO   - Steven van der Marcks HISTOGRAM routine                 *
*                                                                      *
************************************************************************
      SUBROUTINE VEGAS(FXN,AVGI,SD,CHI2A)
C
C   SUBROUTINE PERFORMS N-DIMENSIONAL MONTE CARLO INTEG'N
C      - BY G.P. LEPAGE   SEPT 1976/(REV)APR 1978
C
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/BVEG1/XL(10),XU(10),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON/BVEG2/XI(50,10),SI,SI2,SWGT,SCHI,NDO,IT
      DIMENSION D(50,10),DI(50,10),XIN(50),R(50),DX(10),DT(10),X(10)
     1   ,KG(10),IA(10)
      REAL*8 QRAN(10)
      EXTERNAL FXN
      SAVE /BVEG1/,/BVEG2/,NDMX,ALPH,ONE,MDS
      DATA NDMX/50/,ALPH/1.5D0/,ONE/1D0/,MDS/1/
C
      NDO=1
      DO 1 J=1,NDIM
1     XI(1,J)=ONE
C
      ENTRY VEGAS1(FXN,AVGI,SD,CHI2A)
C         - INITIALIZES CUMMULATIVE VARIABLES, BUT NOT GRID
      IT=0
      SI=0.
      SI2=SI
      SWGT=SI
      SCHI=SI
C
      ENTRY VEGAS2(FXN,AVGI,SD,CHI2A)
C         - NO INITIALIZATION
      ND=NDMX
      NG=1
      IF(MDS.EQ.0) GO TO 2
      NG=(NCALL/2.)**(1./NDIM)
      MDS=1
      IF((2*NG-NDMX).LT.0) GO TO 2
      MDS=-1
      NPG=NG/NDMX+1
      ND=NG/NPG
      NG=NPG*ND
2     K=NG**NDIM
      NPG=NCALL/K
      IF(NPG.LT.2) NPG=2
      CALLS=NPG*K
      DXG=ONE/NG
      DV2G=(CALLS*DXG**NDIM)**2/NPG/NPG/(NPG-ONE)
      XND=ND
      NDM=ND-1
      DXG=DXG*XND
      XJAC=ONE/CALLS
      DO 3 J=1,NDIM
      DX(J)=XU(J)-XL(J)
3     XJAC=XJAC*DX(J)
C
C   REBIN PRESERVING BIN DENSITY
C
      IF(ND.EQ.NDO) GO TO 8
      RC=NDO/XND
      DO 7 J=1,NDIM
      K=0
      XN=0.
      DR=XN
      I=K
4     K=K+1
      DR=DR+ONE
      XO=XN
      XN=XI(K,J)
5     IF(RC.GT.DR) GO TO 4
      I=I+1
      DR=DR-RC
      XIN(I)=XN-(XN-XO)*DR
      IF(I.LT.NDM) GO TO 5
      DO 6 I=1,NDM
6     XI(I,J)=XIN(I)
7     XI(ND,J)=ONE
      NDO=ND
C
8     IF(NPRN.NE.0) WRITE(6,200) NDIM,CALLS,IT,ITMX,ACC,MDS,ND
     1                           ,(XL(J),XU(J),J=1,NDIM)
C
      ENTRY VEGAS3(FXN,AVGI,SD,CHI2A)
C         - MAIN INTEGRATION LOOP
9     IT=IT+1
      TI=0.
      TSI=TI
      DO 10 J=1,NDIM
      KG(J)=1
      DO 10 I=1,ND
      D(I,J)=TI
10    DI(I,J)=TI
C
11    FB=0.
      F2B=FB
      K=0
12    K=K+1
      CALL ARAN9(QRAN,NDIM)
      WGT=XJAC
      DO 15 J=1,NDIM
      XN=(KG(J)-QRAN(J))*DXG+ONE
      IA(J)=XN
      IF(IA(J).GT.1) GO TO 13
      XO=XI(IA(J),J)
      RC=(XN-IA(J))*XO
      GO TO 14
13    XO=XI(IA(J),J)-XI(IA(J)-1,J)
      RC=XI(IA(J)-1,J)+(XN-IA(J))*XO
14    X(J)=XL(J)+RC*DX(J)
15    WGT=WGT*XO*XND
C
      F=WGT
      F=F*FXN(X,WGT)
      F2=F*F
      FB=FB+F
      F2B=F2B+F2
      DO 16 J=1,NDIM
      DI(IA(J),J)=DI(IA(J),J)+F
16    IF(MDS.GE.0) D(IA(J),J)=D(IA(J),J)+F2
      IF(K.LT.NPG) GO TO 12
C
      F2B=SQRT(F2B*NPG)
      F2B=(F2B-FB)*(F2B+FB)
      TI=TI+FB
      TSI=TSI+F2B
      IF(MDS.GE.0) GO TO 18
      DO 17 J=1,NDIM
17    D(IA(J),J)=D(IA(J),J)+F2B
18    K=NDIM
19    KG(K)=MOD(KG(K),NG)+1
      IF(KG(K).NE.1) GO TO 11
      K=K-1
      IF(K.GT.0) GO TO 19
C
C   FINAL RESULTS FOR THIS ITERATION
C
      TSI=TSI*DV2G
      TI2=TI*TI
      WGT=TI2/TSI
      SI=SI+TI*WGT
      SI2=SI2+TI2
      SWGT=SWGT+WGT
      SCHI=SCHI+TI2*WGT
      AVGI=SI/SWGT
      SD=SWGT*IT/SI2
      CHI2A=SD*(SCHI/SWGT-AVGI*AVGI)/(IT-.999)
      SD=SQRT(ONE/SD)
C
      IF(NPRN.EQ.0) GO TO 21
      TSI=SQRT(TSI)
      WRITE(6,201) IT,TI,TSI,AVGI,SD,CHI2A
      IF(NPRN.GE.0) GO TO 21
      DO 20 J=1,NDIM
20    WRITE(6,202) J,(XI(I,J),DI(I,J),D(I,J),I=1,ND)
C
C   REFINE GRID
C
21    DO 23 J=1,NDIM
      XO=D(1,J)
      XN=D(2,J)
      D(1,J)=(XO+XN)/2.
      DT(J)=D(1,J)
      DO 22 I=2,NDM
      D(I,J)=XO+XN
      XO=XN
      XN=D(I+1,J)
      D(I,J)=(D(I,J)+XN)/3.
22    DT(J)=DT(J)+D(I,J)
      D(ND,J)=(XN+XO)/2.
23    DT(J)=DT(J)+D(ND,J)
C
      DO 28 J=1,NDIM
      RC=0.
      DO 24 I=1,ND
      R(I)=0.
      IF(D(I,J).LE.0.) GO TO 24
      XO=DT(J)/D(I,J)
      R(I)=((XO-ONE)/XO/LOG(XO))**ALPH
24    RC=RC+R(I)
      RC=RC/XND
      K=0
      XN=0.
      DR=XN
      I=K
25    K=K+1
      DR=DR+R(K)
      XO=XN
      XN=XI(K,J)
26    IF(RC.GT.DR) GO TO 25
      I=I+1
      DR=DR-RC
      XIN(I)=XN-(XN-XO)*DR/R(K)
      IF(I.LT.NDM) GO TO 26
      DO 27 I=1,NDM
27    XI(I,J)=XIN(I)
28    XI(ND,J)=ONE
C
      IF(IT.LT.ITMX.AND.ACC*ABS(AVGI).LT.SD) GO TO 9
200   FORMAT('0INPUT PARAMETERS FOR VEGAS:  NDIM=',I3,'  NCALL=',F8.0
     1    /28X,'  IT=',I5,'  ITMX=',I5/28X,'  ACC=',G9.3
     2    /28X,'  MDS=',I3,'   ND=',I4/28X,'  (XL,XU)=',
     3    (T40,'( ',G12.6,' , ',G12.6,' )'))
201   FORMAT(///' INTEGRATION BY VEGAS' / '0ITERATION NO.',I3,
     1    ':   INTEGRAL =',G14.8/21X,'STD DEV  =',G10.4 /
     2    ' ACCUMULATED RESULTS:   INTEGRAL =',G14.8 /
     3    24X,'STD DEV  =',G10.4 / 24X,'CHI**2 PER IT''N =',G10.4)
202   FORMAT('0DATA FOR AXIS',I2 / ' ',6X,'X',7X,'  DELT I  ',
     1    2X,' CONV''CE  ',11X,'X',7X,'  DELT I  ',2X,' CONV''CE  '
     2   ,11X,'X',7X,'  DELT I  ',2X,' CONV''CE  ' /
     2    (' ',3G12.4,5X,3G12.4,5X,3G12.4))
      RETURN
      END
      SUBROUTINE SAVE(NDIM)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/BVEG2/XI(50,10),SI,SI2,SWGT,SCHI,NDO,IT
      SAVE /BVEG2/
C
C   STORES VEGAS DATA (UNIT 7) FOR LATER RE-INITIALIZATION
C
      WRITE(7,200) NDO,IT,SI,SI2,SWGT,SCHI,
     1             ((XI(I,J),I=1,NDO),J=1,NDIM)
      RETURN
      ENTRY RESTR(NDIM)
C
C   ENTERS INITIALIZATION DATA FOR VEGAS
C
      READ(7,200) NDO,IT,SI,SI2,SWGT,SCHI,
     1            ((XI(I,J),I=1,NDO),J=1,NDIM)
200   FORMAT(2I8,4Z16/(5Z16))
      RETURN
      END
 
      SUBROUTINE ARAN9(QRAN,NDIM)
      REAL*8 QRAN(10),RN
      DO 1 I=1,NDIM
    1   QRAN(I)=RN(I)
      RETURN
      END
 
      FUNCTION GAMFUN(Y)
*
* Gamma function : See Abramowitz, page 257, form. 6.4.40
*
      REAL*8 Y,GAMFUN
*
      REAL*8 R,S,T,AFSPL,X
      INTEGER I,N
*
      REAL*8 COEF(10),PI
*
      DATA COEF/8.3333333333333334E-02,-2.7777777777777778E-03,
     .          7.9365079365079365E-04,-5.9523809523809524E-04,
     .          8.4175084175084175E-04,-1.9175269175269175E-03,
     .          6.4102564102564103E-03,-2.9550653594771242E-02,
     .          0.1796443723688306    ,-0.6962161084529506    /
      DATA PI/  3.141592653589793/
*
      X=Y
      AFSPL=1.0D0
      N=INT(10.0D0-Y)
      DO 10 I=0,N
        AFSPL=AFSPL*X
        X=X+1.0D0
10    CONTINUE
      R=(X-0.5D0)*LOG(X)-X+0.5D0*LOG(2.0D0*PI)
      S=X
      T=0.0D0
      DO 20 I=1,10
        T=T+COEF(I)/S
        S=S*X**2
20    CONTINUE
      GAMFUN=EXP(R+T)/AFSPL
      END
*
************************************************************************
* HISOUT puts the histograms in the file NJETS.OUT.                    *
* convention: IHIST=1  total process.                                  *
*             IHIST=41 gluonic stuff.                                  *
*             IHIST=81 2 q stuff.                                      *
*             IHIST=121 4 q stuff.                                     *
*             IHIST=161 6 q stuff.                                     *
*             IHIST=201 GG stuff.                                      *
*             IHIST=241 QG stuff.                                      *
*             IHIST=281 QQ stuff.                                      *
************************************************************************
      SUBROUTINE HISOUT(IHIST,IS)
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*40 PROC(8)
      COMMON /PROCES/ ISUB(4),ISTAT(4),IINIT(3),IHISTO(40)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      SAVE PROC
      DATA PROC / '  ==> All processes together ',
     .  ' ==> Gluonic process only. ',' ==> Two quark processes only. ',
     .  ' ==> Four quark processes only. ',
     .  ' ==> Six quark processes only. ',
     .  ' ==> GG initial state only. ',
     .  ' ==> GQ initial state only. ',
     .  ' ==> QQ initial state only. '/
*
      OPEN(UNIT=4,FILE='NJETS.OUT',STATUS='OLD',ACCESS='APPEND')
*
      ICOUNT=0
*
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  CM-ENERGY ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,2,' ',4,1)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  PT ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,2,' ',4,2)
      END IF
      ICOUNT=ICOUNT+1
      DO 5 I=1,NJET
        IF (IHISTO(ICOUNT+1).EQ.1) THEN
          WRITE(4,*)
          WRITE(4,*) ' ============================================= '
          WRITE(4,*) '  PT ORDERED JET ',I,PROC(IS)
          CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,2,' ',4,2)
        END IF
        ICOUNT=ICOUNT+1
    5 CONTINUE
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  INVARIANT MASS ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  POUT ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  ETSUM ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      END IF
      ICOUNT=ICOUNT+1
      DO 10 I=1,NJET-1
        DO 10 J=I+1,NJET
          IF (IHISTO(ICOUNT+1).EQ.1) THEN
            WRITE(4,*)
            WRITE(4,*) ' ============================================= '
            WRITE(4,*) '  ANGLE BETWEEN JETS ',I,' AND ',J,PROC(IS)
            CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
          END IF
          ICOUNT=ICOUNT+1
   10 CONTINUE
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  OCCURENCES OF CERTAIN WEIGHT ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  MONTE CARLO X1-DISTRIBUTION ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  MONTE CARLO X2-DISTRIBUTION ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        WRITE(4,*)
        WRITE(4,*) ' ============================================= '
        WRITE(4,*) '  MONTE CARLO X1*X2-DISTRIBUTION ',PROC(IS)
        CALL HISTO(IHIST+ICOUNT,2,0D0,0D0,0D0,0D0,1,' ',4,1)
      END IF
      ICOUNT=ICOUNT+1
*
      CLOSE(4)
*
      END
*
      SUBROUTINE HISTO(I,ISTAT,X,X0,X1,WEIGHT,LINLOG,TITLE,IUNIT,NX)
*     ----------------
* Steven van der Marck, February 22nd, 1990.
* I      = number of this particular histogram
* ISTAT  = 0    : clear the arrays
*          1    : fill  the arrays
*          2    : print a histogram
*          3    : output the data to HISTO.GRA - to be used for a
*                 'home made' graphics program.
*          4    : same as 3, but the whole histogram is divided by
*                 the number of points.
*          5    : save all relevant information to a file HISTO.DAT
*          6    : read all relevant information from file HISTO.DAT
*          ELSE : rescale all histo's by a factor X
* X      = x-value to be placed in a bin of the histogram
*          If ISTAT=2 and LINLOG<>1, x = the number of decades (def=3).
* X0     = the minimum for x in the histogram
* X1     = the maximum for x in the histogram
* WEIGHT = If ISTAT=1: the weight assigned to this value of x.
*          If ISTAT=2: the number of divisions on the y-axis (def=20),
*                     should not be <5 or >79 (because of screenwidth).
* LINLOG = 1    : linear      histogram
*          ELSE : logarithmic histogram
*          If a linear histo has only one peak that is too sharp,
*          this routine will automatically switch to a log. histo.
* TITLE  = title of this particular histogram ( character*(*) )
* IUNIT  = unit number to write this histogram to
* NX     = the number of divisions on the x-axis for this histogram
*          NX should not be greater than NXMAX (=50 in this version).
*
* When ISTAT = 0,5,6  : no other variables are used.
*            = 1      : I, X, X0, X1, WEIGHT, and NX are used.
*            = 2      : I, X, WEIGHT, LINLOG, TITLE and IUNIT are used.
*            = 3,4    : I, LINLOG and TITLE are used. The user should
*         not be using unit nr 11 when invoking HISTO with this option!
*            = ELSE   : only X is used.
      IMPLICIT LOGICAL(A-Z)
      INTEGER N,NX,NXMAX,I,ISTAT,LINLOG,IUNIT
* N = the maximum number of histo's allowed; can be changed on its own.
* NXMAX = the maximum number (NX) of divisions allowed on the x-axis.
      PARAMETER( N = 320, NXMAX = 50 )
      INTEGER J,J1,J2,IX,IY,JUNIT,NYDIV
      REAL*8 X,X0,X1,WEIGHT, Z,WEISUM,WEISU2,FACTOR
      REAL*8 Y(N,NXMAX), YMAX(N), BOUND0(N), BOUND1(N),
     +                 XMIN(N), XMAX(N), YSQUAR(N,NXMAX), YOUT(N)
      INTEGER IUNDER(N), IIN(N), IOVER(N), NRBINS(N), IBIN(N,NXMAX)
      CHARACTER TITLE*(*),LINE(132),BLANK,STAR,TEXT*13
      CHARACTER FORM1*80,F*80,STRP*1,FH*30,F2(1:3)*12
      LOGICAL EX
      SAVE
      DATA STRP/'-'/F2/'(1X,4I10)','(1X,10I10)','(1X,10D12.5)'/
      DATA BLANK/' '/STAR/'*'/FH/'(A),''I'',3X,G11.4,2X,G11.4,I12)'/
      IF(ISTAT .EQ. 0) THEN
        DO 20 J1 = 1 , N
          BOUND0(J1) = 0D0
          BOUND1(J1) = 0D0
          XMIN  (J1) = 0D0
          XMAX  (J1) = 0D0
          YMAX  (J1) = 0D0
          YOUT  (J1) = 0D0
          IUNDER(J1) = 0
          IIN   (J1) = 0
          IOVER (J1) = 0
          NRBINS(J1) = 0
          DO 10 J2 = 1 , NXMAX
            Y     (J1,J2) = 0D0
            YSQUAR(J1,J2) = 0D0
            IBIN  (J1,J2) = 0
   10     CONTINUE
   20   CONTINUE
        INQUIRE(IUNIT,EXIST=EX)
        IF(EX) THEN
          JUNIT = IUNIT
        ELSE
          JUNIT = 6
        ENDIF
        WRITE(JUNIT,'(1X,A,I3,A)')'HISTO initialization is done.',N,
     +    ' histograms are available.'
      ELSEIF(ISTAT.EQ.1) THEN
        BOUND0(I) = X0
        BOUND1(I) = X1
        IF(NRBINS(I) .EQ. 0) THEN
          IF(NX.GT.0 .AND. NX.LE.NXMAX) THEN
            NRBINS(I) = NX
          ELSE
            NRBINS(I) = NXMAX
          ENDIF
        ENDIF
        IF(X.LT.X0)THEN
          IUNDER(I) = IUNDER(I) + 1
          YOUT  (I) = YOUT  (I) + WEIGHT
          IF(X.LT.XMIN(I).OR.IUNDER(I).EQ.1) XMIN(I) = X
        ELSEIF(X.GT.X1)THEN
          IOVER(I) = IOVER(I) + 1
          YOUT (I) = YOUT (I) + WEIGHT
          IF(X.GT.XMAX(I).OR. IOVER(I).EQ.1) XMAX(I) = X
        ELSE
          IIN(I) = IIN(I) + 1
          IX     = INT((X-X0)/(X1-X0)*NRBINS(I)) + 1
          IF(IX.EQ.NRBINS(I)+1) IX = NRBINS(I)
          IBIN  (I,IX) = IBIN  (I,IX) + 1
          Y     (I,IX) = Y     (I,IX) + WEIGHT
          YSQUAR(I,IX) = YSQUAR(I,IX) + WEIGHT**2
          IF(Y(I,IX).GT.YMAX(I)) YMAX(I) = Y(I,IX)
        ENDIF
      ELSEIF(ISTAT .EQ. 2) THEN
        WRITE(IUNIT,'(//,A,I3,A,I10,A,I8,A,I8)')' Histogram nr.',I,
     +  '  Points in:',IIN(I),'  under:',IUNDER(I),'  over:',IOVER(I)
        WRITE(IUNIT,*)' ',TITLE
        IF(YMAX(I).LE.0D0) RETURN
        NYDIV = INT(WEIGHT)
        IF(NYDIV .LT. 5 .OR. NYDIV .GT. 79) NYDIV = 20
        IF(LINLOG .EQ. 1) THEN
          IX = 0
          DO 30 J1 = 1 , NRBINS(I)
            IF(Y(I,J1)/YMAX(I)*NYDIV .GT. 1D0) IX = IX + 1
   30     CONTINUE
        ENDIF
        IF(IX .LE. 2 .OR. LINLOG .NE. 1) THEN
          IX = 2
          FACTOR = 1D3/YMAX(I)
          IF(X.GE.1D0 .AND. X.LE.10D0) FACTOR = 10D0**X/YMAX(I)
        ELSE
          IX = 1
        ENDIF
        WRITE(FORM1,'(A,I3,A)') '(''  '',G11.4,1X,',NYDIV,'(A),A)'
        WRITE(F,'(A,I3,A)') '('' '',A13,',NYDIV,FH
        WRITE(IUNIT,FORM1) BOUND0(I),(STRP,J1=1,NYDIV),
     +    '   bin boundary   bin ''area''    # points'
        WEISUM = 0D0
        WEISU2 = 0D0
        DO 50 J1 = 1 , NRBINS(I)
          IF(IX.EQ.1) THEN
            IY=INT(Y(I,J1)/YMAX(I)*FLOAT(NYDIV))+1
          ELSE
            IF(FACTOR*Y(I,J1).LE.1D0) THEN
              IY=1
            ELSE
              IY=INT(LOG(FACTOR*Y(I,J1))/LOG(FACTOR*YMAX(I))*
     +           FLOAT(NYDIV))+1
            ENDIF
          ENDIF
          IF(IY .EQ. NYDIV+1) IY = NYDIV
          DO 40 J2 = 1 , NYDIV
            LINE(J2)=BLANK
            IF(J2.EQ.IY) LINE(J2)=STAR
   40     CONTINUE
          Z = BOUND0(I) + J1/FLOAT(NRBINS(I))*(BOUND1(I)-BOUND0(I))
          WEISUM = WEISUM + Y(I,J1)
          WEISU2 = WEISU2 + YSQUAR(I,J1)
          IF(J1.EQ.INT(FLOAT(NRBINS(I))/2.D0))THEN
            TEXT = 'logarithmic I'
            IF(IX.EQ.1) TEXT = '  linear    I'
          ELSE
            TEXT = '            I'
          ENDIF
          WRITE(IUNIT,F)TEXT,(LINE(J2),J2=1,NYDIV),Z,Y(I,J1),IBIN(I,J1)
   50   CONTINUE
        WRITE(IUNIT,FORM1) BOUND1(I),(STRP,J1=1,NYDIV),' '
        Z=SQRT(ABS(WEISU2-WEISUM**2/FLOAT(IIN(I))))/FLOAT(IIN(I))
        WRITE(IUNIT,'(13X,''The average of the entries amounts to '',
     +    G11.4,'' +- '',G11.4,/,13X,
     +    ''The fraction inside the histo bounds: '',G11.4)')WEISUM/
     +    FLOAT(IIN(I)+IUNDER(I)+IOVER(I)),Z,WEISUM/(WEISUM+YOUT(I))
        IF(IUNDER(I).GE.1) WRITE(IUNIT,60)'minimum',XMIN(I)
        IF( IOVER(I).GE.1) WRITE(IUNIT,60)'maximum',XMAX(I)
   60   FORMAT(12X,' The ',A,' value that occurred was   ',G11.4)
      ELSEIF(ISTAT .EQ. 3 .OR. ISTAT .EQ. 4) THEN
        IF(YMAX(I) .LE. 0D0) RETURN
        JUNIT = 11
        OPEN(UNIT=JUNIT,FILE='HISTO.GRA',STATUS='NEW')
        FACTOR = NRBINS(I)/(BOUND1(I)-BOUND0(I))
        IF(ISTAT .EQ. 4) FACTOR=FACTOR/FLOAT(IIN(I)+IUNDER(I)+IOVER(I))
        IF(LINLOG .EQ. 1) THEN
          WRITE(JUNIT,110) BOUND0(I),BOUND1(I),1.1D0*YMAX(I)*FACTOR
  110     FORMAT('*B',/,'VH 3.0',/,'LX 14.0',/,'LY 14.0',/,
     +       'XM ',D12.4,2X,D12.4,' 10',/,'YM 0. ',D12.4,' 10',/,'//')
        ELSE
          Z = YMAX(I)*FACTOR
          DO 120 J1 = 2 , NRBINS(I)
            IF(FACTOR*Y(I,J1).LT.Z.AND.Y(I,J1).GT.0D0) Z=Y(I,J1)*FACTOR
  120     CONTINUE
          WEISUM = .8D0*Z
          WRITE(JUNIT,130) BOUND0(I),BOUND1(I)
  130     FORMAT('*B',/,'VH 3.0',/,'LX 14.0',/,'LY 14.0',/,'XM ',D12.4,
     +      2X,D12.4,' 10',/,'YL ',/,'//')
        ENDIF
        WRITE(JUNIT,*)' ',TITLE
        WRITE(JUNIT,'(///,''//'',/,''*P'',/,''SN -1'',/,''CL'')')
        IF(Y(I,1).GT.0D0 .OR. LINLOG.EQ.1)
     +    WRITE(JUNIT,150) BOUND0(I),Y(I,1)*FACTOR
        DO 140 J1 = 1 , NRBINS(I)-1
          Z = BOUND0(I) + J1/FLOAT(NRBINS(I))*(BOUND1(I)-BOUND0(I))
          IF((Y(I,J1).GT.0D0.AND.Y(I,J1+1).GT.0D0).OR.LINLOG.EQ.1)THEN
            WRITE(JUNIT,150) Z,Y(I,J1  )*FACTOR
            WRITE(JUNIT,150) Z,Y(I,J1+1)*FACTOR
          ELSEIF(Y(I,J1).GT.0D0) THEN
            WRITE(JUNIT,150) Z,Y(I,J1)*FACTOR
            WRITE(JUNIT,150) Z, WEISUM
            WRITE(JUNIT,'(''/'')')
          ELSEIF(Y(I,J1+1).GT.0D0) THEN
            WRITE(JUNIT,150) Z, WEISUM
            WRITE(JUNIT,150) Z,Y(I,J1+1)*FACTOR
          ENDIF
  140   CONTINUE
        J1 = NRBINS(I)
        IF(Y(I,J1).GT.0D0 .OR. LINLOG.EQ.1)
     +    WRITE(JUNIT,150) BOUND1(I),Y(I,J1)*FACTOR
  150   FORMAT(' ',2G12.4)
        WRITE(JUNIT,'(''//'',/,''*E'')')
        CLOSE(UNIT=JUNIT)
      ELSEIF(ISTAT .EQ. 5) THEN
        JUNIT = 11
        OPEN(UNIT=JUNIT,FILE='HISTO.DAT',STATUS='NEW')
        WRITE(JUNIT,F2(1))(IUNDER(J),IIN(J),IOVER(J),NRBINS(J),J=1,N)
        WRITE(JUNIT,F2(2)) ((IBIN(J1,J2),J2=1,NXMAX),J1=1,N)
        WRITE(JUNIT,F2(3)) (YMAX(J1),BOUND0(J1),BOUND1(J1),
     +                     XMIN(J1),XMAX  (J1),YOUT  (J1),J1=1,N)
        WRITE(JUNIT,F2(3))((Y(J1,J2),YSQUAR(J1,J2),J2=1,NXMAX),J1=1,N)
        CLOSE(UNIT=JUNIT)
      ELSEIF(ISTAT .EQ. 6) THEN
        JUNIT = 11
        OPEN(UNIT=JUNIT,FILE='HISTO.DAT',STATUS='OLD')
        READ(JUNIT,F2(1))(IUNDER(J),IIN(J),IOVER(J),NRBINS(J),J=1,N)
        READ(JUNIT,F2(2)) ((IBIN(J1,J2),J2=1,NXMAX),J1=1,N)
        READ(JUNIT,F2(3)) (YMAX(J1),BOUND0(J1),BOUND1(J1),
     +                    XMIN(J1),XMAX  (J1),YOUT  (J1),J1=1,N)
        READ(JUNIT,F2(3))((Y(J1,J2),YSQUAR(J1,J2),J2=1,NXMAX),J1=1,N)
        CLOSE(UNIT=JUNIT)
      ELSE
        DO 200 J1 = 1 , N
          YMAX(J1) = X * YMAX(J1)
          DO 190 J2 = 1 , NXMAX
            Y     (J1,J2) = X    * Y     (J1,J2)
            YSQUAR(J1,J2) = X**2 * YSQUAR(J1,J2)
  190     CONTINUE
  200   CONTINUE
      ENDIF
      END
*
      SUBROUTINE CLRSTS
      IMPLICIT REAL*8 (A-H,O-Z)
      COMMON /Q0STAT/ TQ0(1,6) ,TFLQ0(5,6)
      COMMON /Q2STAT/ TQ2(4,6) ,TFLQ2(5,6)
      COMMON /Q4STAT/ TQ4(11,6),TFLQ4(5,6)
      COMMON /Q6STAT/ TQ6(19,6),TFLQ6(5,6)
      SAVE /Q0STAT/,/Q2STAT/,/Q4STAT/,/Q6STAT/
*
      DO 10 I=1,6
        DO 15 J=1,5
          TFLQ0(J,I)=0D0
          TFLQ2(J,I)=0D0
          TFLQ4(J,I)=0D0
          TFLQ6(J,I)=0D0
   15   CONTINUE
        DO 20 J=1,19
          IF (J.LT.2)  TQ0(J,I)=0D0
          IF (J.LT.5)  TQ2(J,I)=0D0
          IF (J.LT.12) TQ4(J,I)=0D0
          IF (J.LT.20) TQ6(J,I)=0D0
   20   CONTINUE
   10 CONTINUE
*
      END
*
      FUNCTION FXN(X,WGT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION X(10)
      COMMON /BVEG1/  XL(10),XU(10),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON /EFFIC/  NIN1,NIN2,NOUT
      COMMON /SUBPRO/ SFQ0,SFQ2,SFQ4,SFQ6,SINIT(5)
      COMMON /PROCES/ ISUB(4),ISTAT(4),IINIT(3),IHISTO(40)
      SAVE /BVEG1/,/SUBPRO/
*
      CALL EVENT(X,WT,WGT,IDUMP)
      IF(IDUMP.EQ.0) THEN
         FXN=0.D0
         NOUT=NOUT+1
         RETURN
      ENDIF
      IF(IDUMP.EQ.1) THEN
         FXN=0.D0
         NIN1=NIN1+1
         RETURN
      END IF
*
      FXN=WT/WGT
*
      NIN2=NIN2+1
      CALL BIN(WT/ITMX,1)
      IF (ISTAT(1).EQ.1) CALL BIN(SFQ0/ITMX,41)
      IF (ISTAT(2).EQ.1) CALL BIN(SFQ2/ITMX,81)
      IF (ISTAT(3).EQ.1) CALL BIN(SFQ4/ITMX,121)
      IF (ISTAT(4).EQ.1) CALL BIN(SFQ6/ITMX,161)
      IF (IINIT(1).EQ.1) CALL BIN(SINIT(1)/ITMX,201)
      IF (IINIT(2).EQ.1) CALL BIN((SINIT(2)+SINIT(3))/ITMX,241)
      IF (IINIT(3).EQ.1) CALL BIN((SINIT(4)+SINIT(5))/ITMX,281)
*
      END
*
************************************************************************
* BIN(WT,1) generates histograms for the complete NJET process         *
* Histograms for different subprocesses: 41=0q, 81=2q, 121=4q, 161=6q  *
*            for different init states: 201=gg, 241=gq, 281=qq         *
*  IHIST-IHIST+X    : CM, PT, Mij, POUT, ETSUM, PTNORM, SPHERICITY     *
*                      CM ANGLES (ORDERED TO MAXIMUM PT), WEIGHTS      *
************************************************************************
      SUBROUTINE BIN(WT,IHIST)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /GENERA/ W,RMQ,EPS,NJET,NF,IEFF,IMANNR,
     .                IPPBAR,IHELRN,IQCDDS,IMQ
      COMMON /STRUC/  X1,X2,Q,QCDSCA(6),ITRY(6),IUSE
      COMMON /PROCES/ ISUB(4),ISTAT(4),IINIT(3),IHISTO(40)
      COMMON /MOM/    PLAB(4,10),WHAT
      COMMON /BINVAR/ PT(10),ETA(10),DR(10,10),DRR(10,10),ETOT,POUT
      COMMON /BINVA2/ SUMET,PTNORM,SPH,ANGLE(10,10),Y1,Y2,PTSORT(10)
      COMMON /HISSCA/ CMMAX,PTMAX,RM2MAX,POUMAX,ICM,IPT,IM2,IPOUT,
     .                PETSUM,PPTMAX,PSPHER,PANGLE,PWEIGH,
     .                IETSUM,IPTMAX,ISPHE,IANGLE,IWEIGH
      COMMON /HISSC2/ PTMAX2,IPT2
      SAVE /MOM/,/BINVAR/,/BINVA2/
*
      ICOUNT=0
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        CALL HISTO(IHIST+ICOUNT,1,WHAT,0.D0,CMMAX,WT,1,' ',4,ICM)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        DO 10 J=1,NJET
          CALL HISTO(IHIST+ICOUNT,1,PT(J),0.D0,PTMAX,WT,2,' ',4,IPT)
   10   CONTINUE
      END IF
      ICOUNT=ICOUNT+1
      DO 11 J=1,NJET
       IF (IHISTO(ICOUNT+1).EQ.1)
     . CALL HISTO(IHIST+ICOUNT,1,PTSORT(J),0.D0,PTMAX2,WT,2,' ',4,IPT2)
       ICOUNT=ICOUNT+1
   11 CONTINUE
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        DO 20 I1=1,NJET-1
          DO 20 J1=I1+1,NJET
            PI=(PLAB(4,I1+2)+PLAB(4,J1+2))**2
     .        -(PLAB(1,I1+2)+PLAB(1,J1+2))**2
     .        -(PLAB(2,I1+2)+PLAB(2,J1+2))**2
     .        -(PLAB(3,I1+2)+PLAB(3,J1+2))**2
         CALL HISTO(IHIST+ICOUNT,1,SQRT(PI),0.D0,RM2MAX,WT,1,' ',4,IM2)
   20   CONTINUE
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        CALL HISTO(IHIST+ICOUNT,1,POUT,0D0,POUMAX,WT,1,' ',4,IPOUT)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        CALL HISTO(IHIST+ICOUNT,1,SUMET,0D0,PETSUM,WT,1,' ',4,IETSUM)
      END IF
      ICOUNT=ICOUNT+1
      DO 30 I1=1,NJET-1
        DO 30 I2=I1+1,NJET
        IF (IHISTO(ICOUNT+1).EQ.1)
     .    CALL HISTO(IHIST+ICOUNT,1,ANGLE(I1,I2),-PANGLE,PANGLE
     .                                      ,WT,1,' ',4,IANGLE)
         ICOUNT=ICOUNT+1
   30 CONTINUE
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        IF (WT.LT.1D-30) THEN
          CALL HISTO(IHIST+ICOUNT,1,LOG(1D-30),-PWEIGH,PWEIGH
     .                                      ,1D0,1,' ',4,IWEIGH)
        ELSE
          CALL HISTO(IHIST+ICOUNT,1,LOG10(WT),-PWEIGH,PWEIGH
     .                                      ,1D0,1,' ',4,IWEIGH)
        END IF
      END IF
*
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        CALL HISTO(IHIST+ICOUNT,1,X1,0D0,1D0,WT,1,' ',4,20)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        CALL HISTO(IHIST+ICOUNT,1,X2,0D0,1D0,WT,1,' ',4,20)
      END IF
      ICOUNT=ICOUNT+1
      IF (IHISTO(ICOUNT+1).EQ.1) THEN
        CALL HISTO(IHIST+ICOUNT,1,X1*X2,0D0,1D0,WT,1,' ',4,20)
      END IF
*
      END
