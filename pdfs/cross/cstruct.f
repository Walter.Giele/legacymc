*
* crossing functions
* istruc=0  -> test        x*all(x) = x*(1-x)
* istruc=1  -> mrse        (msbar), Lambda = 0.1  GeV
* istruc=2  -> mrsb        (msbar), Lambda = 0.2  GeV
* istruc=3  -> kmrshb      (msbar), Lambda = 0.19 GeV
* istruc=4  -> kmrsb0      (msbar), Lambda = 0.19 GeV
* istruc=5  -> kmrsb-      (msbar), Lambda = 0.19 GeV
* istruc=6  -> kmrsh- (5)  (msbar), Lambda = 0.19 GeV
* istruc=7  -> kmrsh- (2)  (msbar), Lambda = 0.19 GeV
* istruc=8  -> mts1        (msbar), Lambda = 0.2  GeV
* istruc=9  -> mte1        (msbar), Lambda = 0.2  GeV
* istruc=10 -> mtb1        (msbar), Lambda = 0.2  GeV
* istruc=11 -> mtb2        (msbar), Lambda = 0.2  GeV
* istruc=12 -> mtsn1       (msbar), Lambda = 0.2  GeV
* istruc=13 -> kmrss0      (msbar), Lambda = 0.215 GeV
* istruc=14 -> kmrsd0      (msbar), Lambda = 0.215 GeV
* istruc=15 -> kmrsd-      (msbar), Lambda = 0.215 GeV
* istruc=16 -> mrss0       (msbar), Lambda = 0.230 GeV
* istruc=17 -> mrsd0       (msbar), Lambda = 0.230 GeV
* istruc=18 -> mrsd-       (msbar), Lambda = 0.230 GeV
* istruc=19 -> cteq1l      (msbar), Lambda = 0.168 GeV
* istruc=20 -> cteq1m      (msbar), Lambda = 0.231 GeV
* istruc=21 -> cteq1ml     (msbar), Lambda = 0.322 GeV
* istruc=22 -> cteq1ms     (msbar), Lambda = 0.231 GeV
* istruc=23 -> grv         (msbar), Lambda = 0.200 GeV
* istruc=24 -> cteq2l      (msbar), Lambda = 0.190 GeV
* istruc=25 -> cteq2m      (msbar), Lambda = 0.213 GeV
* istruc=26 -> cteq2ml     (msbar), Lambda = 0.322 GeV
* istruc=27 -> cteq2ms     (msbar), Lambda = 0.208 GeV
* istruc=28 -> cteq2mf     (msbar), Lambda = 0.208 GeV
* istruc=29 -> mrsh        (msbar), Lambda = 0.230 GeV
* istruc=30 -> mrsa        (msbar), Lambda = 0.230 GeV
* istruc=31 -> mrsg        (msbar), Lambda = 0.254 GeV
* istruc=32 -> mrsap       (msbar), Lambda = 0.231 GeV
* istruc=33 -> grv94       (msbar), Lambda = 0.200 GeV
* istruc=34 -> cteq3l      (msbar), Lambda = 0.177 GeV
* istruc=35 -> cteq3m      (msbar), Lambda = 0.239 GeV
*
* extra structure functions with variable as/Lambda
*
* istruc=101 -> mrsap       (msbar), as(Mz) = 0.110 -> Lambda = 0.216
* istruc=102 -> mrsap       (msbar), as(Mz) = 0.115 -> Lambda = 0.284
* istruc=103 -> mrsap       (msbar), as(Mz) = 0.120 -> Lambda = 0.366
* istruc=104 -> mrsap       (msbar), as(Mz) = 0.125 -> Lambda = 0.458
* istruc=105 -> mrsap       (msbar), as(Mz) = 0.130 -> Lambda = 0.564
* istruc=111 -> grv94       (msbar), Lambda = 0.150 GeV
* istruc=112 -> grv94       (msbar), Lambda = 0.200 GeV = istruc33
* istruc=113 -> grv94       (msbar), Lambda = 0.250 GeV
* istruc=114 -> grv94       (msbar), Lambda = 0.300 GeV
* istruc=115 -> grv94       (msbar), Lambda = 0.350 GeV
* istruc=116 -> grv94       (msbar), Lambda = 0.400 GeV
*
*
*
      subroutine cstruct(x,scale,istruc,
     .                                upa,dna,usa,dsa,sta,cha,boa,gla,
     .                                upb,dnb,usb,dsb,stb,chb,bob,glb)
      implicit real*8(a-h,o-z)
      parameter(nx=53,nq=20)
      dimension f1(8,nx,nq),f2(8,nx,nq),g1(8),g2(8),xx(nx),n0(8)
      save xx,xmin,xmax,qsqmin,qsqmax,n0,init
      data xx/1.d-5,2.d-5,4.d-5,6.d-5,8.d-5,
     .        1.d-4,2.d-4,4.d-4,6.d-4,8.d-4,
     .        1.d-3,2.d-3,4.d-3,6.d-3,8.d-3,
     .        1.d-2,1.5d-2,2.d-2,3.d-2,4.d-2,5.d-2,6.d-2,7.d-2,8.d-2,
     .     .1d0,.125d0,.15d0,.175d0,.2d0,.225d0,.25d0,.275d0,
     .     .3d0,.325d0,.35d0,.375d0,.4d0,.425d0,.45d0,.475d0,
     .     .5d0,.525d0,.55d0,.575d0,.6d0,.65d0,.7d0,.75d0,
     .     .8d0,.85d0,.9d0,.95d0,1.d0/
      data xmin,xmax,qsqmin,qsqmax/1.d-5,1.d0,5.d0,1310720.d0/
      data n0/0,0,0,0,0,0,0,0/
      data init/0/
      if(init.ne.0) goto 10
      init=1
        write(*,*)' crossing  functions as of 18/4/95 '
	imax=7
        if(istruc.eq.0)then
*          open(unit=2,file='stfns/CTEST.DAT',status='old')
*          call ctest(x,scale,upa,dna,usa,sta,cha,boa,gla,
*    .                        upb,dnb,usb,stb,chb,bob,glb)
*           return
        ELSEIF(istruc.eq.1)then
          open(unit=2,file='stfns/CMRSEB1.DAT',status='old')
	elseif(istruc.eq.2)then
          open(unit=2,file='stfns/CMRSEB2.DAT',status='old')
        elseif(istruc.eq.3)then
          open(unit=2,file='stfns/CMRSHB.DAT',status='old')
****          open(unit=2,file=' ./stfns/GMRSHB.DAT',status='old')
	elseif(istruc.eq.4)then
          open(unit=2,file='stfns/CMRSB0.DAT',status='old')
	elseif(istruc.eq.5)then
          open(unit=2,file='stfns/CMRSB-.DAT',status='old')
	elseif(istruc.eq.6)then
          open(unit=2,file='stfns/CMRSB-5.DAT',status='old')
	elseif(istruc.eq.7)then
          open(unit=2,file='stfns/CMRSB-2.DAT',status='old')
        elseif(istruc.eq.8)then
          open(unit=2,file='stfns/CMTS1.DAT',status='old')
	elseif(istruc.eq.9)then
          open(unit=2,file='stfns/CMTE1.DAT',status='old')
	elseif(istruc.eq.10)then
          open(unit=2,file='stfns/CMTB1.DAT',status='old')
	elseif(istruc.eq.11)then
          open(unit=2,file='stfns/CMTB2.DAT',status='old')
	elseif(istruc.eq.12)then
          open(unit=2,file='stfns/CMTSN1.DAT',status='old')
        elseif(istruc.eq.13)then
          open(unit=2,file='stfns/CMRSS0.DAT',status='old')
	  imax=8
	elseif(istruc.eq.14)then
          open(unit=2,file='stfns/CMRSD0.DAT',status='old')
	  imax=8
	elseif(istruc.eq.15)then
          open(unit=2,file='stfns/CMRSD-.DAT',status='old')
	  imax=8
        elseif(istruc.eq.16)then
          open(unit=2,file='stfns/CMRSS0P.DAT',status='old')
	  imax=8
	elseif(istruc.eq.17)then
          open(unit=2,file='stfns/CMRSD0P.DAT',status='old')
	  imax=8
	elseif(istruc.eq.18)then
          open(unit=2,file='stfns/CMRSD-P.DAT',status='old')
	  imax=8
        elseif(istruc.eq.19)then
          open(unit=2,file='stfns/CTEQ1L.DAT',status='old')
	  imax=8
	elseif(istruc.eq.20)then
          open(unit=2,file='stfns/CTEQ1M.DAT',status='old')
	  imax=8
	elseif(istruc.eq.21)then
          open(unit=2,file='stfns/CTEQ1ML.DAT',status='old')
	  imax=8
	elseif(istruc.eq.22)then
          open(unit=2,file='stfns/CTEQ1MS.DAT',status='old')
	  imax=8
	elseif(istruc.eq.23)then
          open(unit=2,file='stfns/CGRV.DAT',status='old')
        elseif(istruc.eq.24)then
          open(unit=2,file='stfns/CTEQ2L.DAT',status='old')
	  imax=8
	elseif(istruc.eq.25)then
          open(unit=2,file='stfns/CTEQ2M.DAT',status='old')
	  imax=8
	elseif(istruc.eq.26)then
          open(unit=2,file='stfns/CTEQ2ML.DAT',status='old')
	  imax=8
	elseif(istruc.eq.27)then
          open(unit=2,file='stfns/CTEQ2MS.DAT',status='old')
	  imax=8
	elseif(istruc.eq.28)then
          open(unit=2,file='stfns/CTEQ2MF.DAT',status='old')
	  imax=8
	elseif(istruc.eq.29)then
          open(unit=2,file='stfns/CMRSH.DAT',status='old')
	  imax=8
	elseif(istruc.eq.30)then
          open(unit=2,file='stfns/CMRSA.DAT',status='old')
	  imax=8
	elseif(istruc.eq.31)then
          open(unit=2,file='stfns/CMRSG.DAT',status='old')
	  imax=8
	elseif(istruc.eq.32)then
          open(unit=2,file='stfns/CMRSAP.DAT',status='old')
	  imax=8
	elseif(istruc.eq.33)then
          open(unit=2,file='stfns/CGRV94.DAT',status='old')
	  imax=8
	elseif(istruc.eq.34)then
          open(unit=2,file='stfns/CTEQ3L.DAT',status='old')
	  imax=8
	elseif(istruc.eq.35)then
          open(unit=2,file='stfns/CTEQ3M.DAT',status='old')
	  imax=8
	elseif(istruc.eq.101)then
          open(unit=2,file='stfns/CMRSAP110.DAT',status='old')
	  imax=8
	elseif(istruc.eq.102)then
          open(unit=2,file='stfns/CMRSAP115.DAT',status='old')
	  imax=8
	elseif(istruc.eq.103)then
          open(unit=2,file='stfns/CMRSAP120.DAT',status='old')
	  imax=8
	elseif(istruc.eq.104)then
          open(unit=2,file='stfns/CMRSAP125.DAT',status='old')
	  imax=8
	elseif(istruc.eq.105)then
          open(unit=2,file='stfns/CMRSAP130.DAT',status='old')
	  imax=8
	elseif(istruc.eq.111)then
          open(unit=2,file='stfns/CGRV94150.DAT',status='old')
	  imax=8
	elseif(istruc.eq.112)then
          open(unit=2,file='stfns/CGRV94200.DAT',status='old')
	  imax=8
	elseif(istruc.eq.113)then
          open(unit=2,file='stfns/CGRV94250.DAT',status='old')
	  imax=8
	elseif(istruc.eq.114)then
          open(unit=2,file='stfns/CGRV94300.DAT',status='old')
	  imax=8
	elseif(istruc.eq.115)then
          open(unit=2,file='stfns/CGRV94350.DAT',status='old')
	  imax=8
	elseif(istruc.eq.116)then
          open(unit=2,file='stfns/CGRV94400.DAT',status='old')
	  imax=8
	endif
*
*
        ix0=1
        if(istruc.eq.0.or.istruc.eq.1.or.istruc.eq.2)ix0=6
      do 20 n=ix0,nx-1
      do 20 m=1,nq-1
      read(2,50) (f1(i,n,m),i=1,imax)
      read(2,50) (f2(i,n,m),i=1,imax)
         do 25 i=1,imax
         f1(i,n,m)=f1(i,n,m)/(1.d0-xx(n))**n0(i)
         f2(i,n,m)=f2(i,n,m)/(1.d0-xx(n))**n0(i)
  25  continue
  20  continue
      close(2)
  50  format(8e11.4)
      do 40 i=1,imax
      do 40 m=1,nq-1
      f1(i,nx,m)=0.d0
  40  f2(i,nx,m)=0.d0
  10  continue
      if(x.lt.xmin) then
        write(*,*)' x < xmin in crossing functions ',x
	x=xmin
      endif
      if(x.gt.xmax) then
        write(*,*)' x > xmax in crossing functions ',x
        x=xmax
      endif
      qsq=scale**2
      if(qsq.lt.qsqmin) then
        write(*,*)' q**2 < min q**2 in crossing functions ',qsq
        qsq=qsqmin
      endif
      if(qsq.gt.qsqmax) then
        write(*,*)' q**2 > max q**2 in crossing functions ',qsq
        qsq=qsqmax
      endif
      xxx=x
      n=0
  70  n=n+1
      if(xxx.gt.xx(n+1)) goto 70
      a=(xxx-xx(n))/(xx(n+1)-xx(n))
      if(xxx.lt.1d-2)a=log(xxx/xx(n))/log(xx(n+1)/xx(n))
      rm=log(qsq/qsqmin)/log(2.d0)
      b=rm-int(rm)
      m=1+int(rm)
      do 60 i=1,imax
      g1(i)= (1.d0-a)*(1.d0-b)*f1(i,n,m)+(1.d0-a)*b*f1(i,n,m+1)
     .      + a*(1.d0-b)*f1(i,n+1,m)  + a*b*f1(i,n+1,m+1)
      g2(i)= (1.d0-a)*(1.d0-b)*f2(i,n,m)+(1.d0-a)*b*f2(i,n,m+1)
     .      + a*(1.d0-b)*f2(i,n+1,m)  + a*b*f2(i,n+1,m+1)
      g1(i)=g1(i)*(1.d0-x)**n0(i)
      g2(i)=g2(i)*(1.d0-x)**n0(i)
  60  continue
      upa=g1(1)
      dna=g1(2)
      gla=g1(3)
      usa=g1(4)
      sta=g1(5)
      cha=g1(6)
      boa=g1(7) 

      dsa=usa
      if(imax.eq.8)dsa=g1(8)
*
      upb=g2(1)
      dnb=g2(2)
      glb=g2(3)
      usb=g2(4)
      stb=g2(5)
      chb=g2(6)
      bob=g2(7)
      dsb=usb
      if(imax.eq.8)dsb=g2(8)
      return
      end
*
      subroutine ctest(x,scale,upa,dna,sea,sta,cha,boa,gla,
     .                         upb,dnb,seb,stb,chb,bob,glb)
      implicit real*8(a-h,o-z)
      enc=3d0
      enf=5d0
      pie=4d0*datan(1d0)
      aqtoq=-(1d0-x)/2d0*dlog(x)+(1d0-x)*dlog(1d0-x)-0.25*(1d0-x)
      aqtoq=aqtoq*(1d0-1d0/enc**2)
      agtoq=-(2d0-x-x**2+(1d0+2d0*x)*dlog(x))/2d0
      agtoq=agtoq/enc
      aqtog=((1d0+x-2d0*x**2)/x+(2d0+x)*dlog(x))/2d0
      aqtog=aqtog*(1d0-1d0/enc**2)
      agtog=1d0+1d0/x-x-x**2+(2d0+4d0*x)*dlog(x)
     . +(11d0*enc-2d0*enf)/6d0/enc*(1d0-x)+2d0*(1d0-x)*dlog(1d0-x)
      bqtoq=-1d0+x-0.5d0*(1d0+x)*dlog(x)
     . +((1d0-x)*(rsp(x)-pie**2/6d0)-2d0*(1d0-x)*dlog(1d0-x)
     .            +(1d0-x)-x*dlog(x))/2d0
     . +(1d0-x)*(pie**2/6d0-7d0/4d0)-0.5*dlog(1d0-x)**2*(1d0-x)
      bqtoq=bqtoq*(1d0-1d0/enc**2)
      bgtog=-2d0*(1d0+2d0*x)*(rsp(x)-pie**2/6d0)
     . +(1d0-x)*dlog(1d0-x)*(1d0+x)**2/x+(2d0+x)*dlog(x)
     . +(1d0+3d0*x**2-4d0*x)/2d0
     . +(1d0-x)*(pie**2/3d0-67d0/18d0+5d0*enf/9d0/enc)
     . -(1d0-x)*dlog(1d0-x)**2
      bqtog=((1d0-x)/2d0-(1d0+x/2d0)*(rsp(x)-pie**2/6d0)
     . +(1d0-2d0*x+1d0/x)/2d0*dlog(1-x)+(1d0+x)*dlog(x))
      bqtog=bqtog*(1d0-1d0/enc**2)
      bgtoq=(3d0+2d0*x-5d0*x**2)/4d0-(1d0/2d0+x)*(pie**2/6d0-rsp(x))
     . -(2d0-x-x**2)/2d0*dlog(1d0-x)+0.5d0*x*dlog(x)
      bgtoq=bgtoq/enc
      upa=x*aqtoq
      dna=x*aqtoq
      sea=x*(aqtoq+agtoq)
      sta=x*(aqtoq+agtoq)
      cha=x*(aqtoq+agtoq)
      boa=x*(aqtoq+agtoq)
      gla=x*(12d0*aqtog+agtog)
      upb=x*bqtoq
      dnb=x*bqtoq
      seb=x*(bqtoq+bgtoq)
      stb=x*(bqtoq+bgtoq)
      chb=x*(bqtoq+bgtoq)
      bob=x*(bqtoq+bgtoq)
      glb=x*(12d0*bqtog+bgtog)
      return
      end      

*
*
c
c spence function
c
      block data splint
      implicit real*8 (a-h,o-z)
      common/spint/a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,zeta2
      data a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,zeta2/
     1 -0.250000000000000d0,
     2 -0.111111111111111d0,
     3 -0.010000000000000d0,
     4 -0.017006802721088d0,
     5 -0.019444444444444d0,
     6 -0.020661157024793d0,
     7 -0.021417300648069d0,
     8 -0.021948866377231d0,
     9 -0.022349233811171d0,
     1 -0.022663689135191d0,
     2  1.644934066848226d0/
      end
c
c spence function taking only real arguments
c
      function sp(x)
      implicit real*8(a-h,o-z)
      common/spint/a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,zeta2
      if(x.gt.1d0)then
        sp=-rsp(1d0-x)+zeta2-log(x)*log(x-1d0)
      elseif(x.le.1d0)then
        sp=rsp(x)
      endif
      return
      end
*      

      function rsp(x)
      implicit real*8(a-h,o-z)
      common/spint/a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,zeta2
      x2=x*x
      if(x.gt.1.d0)then
        write(*,*)' argument greater than 1 passed to spence function'
        rsp=0.d0
        return
      endif
      if(x2.gt.1.d0.and.x.gt.0.5d0)then
        y=(x-1.d0)/x
        z=-dlog(1.d0-y)
        z2=z*z
        rsp=z*(1.d0+a1*z*(1.d0+a2*z*(1.d0+a3*z2*(1.d0+a4*z2*
     1 (1.d0+a5*z2*(1.d0+a6*z2*(1.d0+a7*z2*(1.d0+a8*z2*(1.d0+a9*z2*
     2 (1.d0+a10*z2))))))))))
     3 +zeta2-dlog(x)*dlog(1.d0-x)+0.5d0*dlog(x)**2
        return
      elseif(x2.gt.1.d0.and.x.le.0.5d0)then
        y=1.d0/x
        z=-dlog(1.d0-y)
        z2=z*z
        rsp=-z*(1.d0+a1*z*(1.d0+a2*z*(1.d0+a3*z2*(1.d0+a4*z2*
     1 (1.d0+a5*z2*(1.d0+a6*z2*(1.d0+a7*z2*(1.d0+a8*z2*(1.d0+a9*z2*
     2 (1.d0+a10*z2))))))))))
     3 -zeta2-0.5d0*dlog(-x)**2
        return
      elseif(x2.eq.1.d0)then
        rsp=zeta2
        return
      elseif(x2.le.1.d0.and.x.gt.0.5d0)then
        y=1.d0-x
        z=-dlog(1.d0-y)
        z2=z*z
        rsp=-z*(1.d0+a1*z*(1.d0+a2*z*(1.d0+a3*z2*(1.d0+a4*z2*
     1 (1.d0+a5*z2*(1.d0+a6*z2*(1.d0+a7*z2*(1.d0+a8*z2*(1.d0+a9*z2*
     2 (1.d0+a10*z2))))))))))
     3 +zeta2-dlog(x)*dlog(1.d0-x)
       return
      elseif(x2.le.1.d0.and.x.le.0.5d0)then
        y=x
        z=-dlog(1.d0-y)
        z2=z*z
        rsp=z*(1.d0+a1*z*(1.d0+a2*z*(1.d0+a3*z2*(1.d0+a4*z2*
     1 (1.d0+a5*z2*(1.d0+a6*z2*(1.d0+a7*z2*(1.d0+a8*z2*(1.d0+a9*z2*
     2 (1.d0+a10*z2))))))))))
        return
      endif
      end


