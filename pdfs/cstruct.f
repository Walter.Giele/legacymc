      subroutine cstruct(x,scale,
     .                   upa,dna,usa,dsa,sta,cha,boa,gla,
     .                   upb,dnb,usb,dsb,stb,chb,bob,glb) 
      implicit real*8(a-h,o-z)
      real*8 f(2,-5:5)
*
      xk=rn(1.0)
      call cpdf(x,xk,scale,f)
      upa=x*(f(1,2)-f(1,-2))
      dna=x*(f(1,1)-f(1,-1))
      usa=x*f(1,-2)
      dsa=x*f(1,-1)
      sta=x*f(1,3)
      cha=x*f(1,4)
      boa=x*f(1,5)
      gla=x*f(1,0)
      upb=x*(f(2,2)-f(2,-2))
      dnb=x*(f(2,1)-f(2,-1))
      usb=x*f(2,-2)
      dsb=x*f(2,-1)
      stb=x*f(2,3)
      chb=x*f(2,4)
      bob=x*f(2,5) 
      glb=x*f(2,0)
*
      return
      end
*
      subroutine cpdf(x,xk,scale,f)
      implicit none
c     S.Keller, 11/02/2000.
c     calculates the crossing funtions 'online'. 
c     inputs:
c     ip: pdf number
c     x: Bjorken-x
c     xk: variable of integration (goes from 0 to 1)
c     qsq: square of the factorization scale
c     nlf: number of light flavors
c     outputs:
c     f(jmod,npart=-5:5) crossing function with the factor N/(2 pi) not
c     included.  jmod=1 gives the A component, jmod=2 gives the B component.
c     q     npart is the parton number following the pdg convention
c     -6,...,6: tbar,bbar,cbar,sbar,ubar,dbar,g,d,u,s,c,b,t.
c     no contribution from the top quark are allowed.
c     Auxiliary:
c     z: variable of integration (goes from x to 1)
c
c     internal book-keeping:
c        i=1  upv to upv
c        i=2  dnv to dnv
c        i=3  glu to glu  
c        i=4  upsea to upsea  
c        i=5  str to str 
c        i=6  chm to chm 
c        i=7  bot to bot 
c        i=8  dnsea to dnsea 
c        i=9  glu to sea
c        i=10 sum of qrk to glu
c
      real*8 x,xk,mu_f,f(2,-5:5)
      integer nlf
      real*8 z,xz,sc_fac,scale
      real*8 upv1,dnv1,ups1,dns1,str1,chm1,bot1,glu1,qrk1
      real*8 upv,dnv,ups,dns,str,chm,bot,glu,qrk
      integer jmod,i
      real*8 res(10),dqtoq,dgtog
      real*8 pi
      real*8 Nc
      parameter(Nc=3d0,pi=3.1415926535987932d0)
      real*8 xgtoq,xqtoq,xgtog,xqtog
      real*8 thb,thc
c
c     Get the thresholds
c
      call GetThreshold(4,thc)
      call GetThreshold(5,thb)
c      
      if(scale.lt.thc)then
         nlf=3
      else if(scale.lt.thb)then
         nlf=4
      else
         nlf=5
      end if
c
c     we have to add the Jacobian factor
c
      z=(1.d0-x)*xk+x
      sc_fac=(1.d0-x)
c
c     input pdf
c     
      call struct(x,scale,upv1,dnv1,ups1,dns1,str1,chm1,bot1,glu1)
      upv1=upv1/x
      dnv1=dnv1/x
      ups1=ups1/x
      dns1=dns1/x
      str1=str1/x
      chm1=chm1/x
      bot1=bot1/x
      glu1=glu1/x
c
c     adjust the sum over the quarks depending on nlf
c     

      if(nlf.eq.2)then
         qrk1=upv1+dnv1+2d0*ups1+2d0*dns1
      else if(nlf.eq.3) then
         qrk1=upv1+dnv1+2d0*ups1+2d0*dns1+2d0*str1
      else if(nlf.eq.4) then
         qrk1=upv1+dnv1+2d0*ups1+2d0*dns1+2d0*str1+2d0*chm1
      else if(nlf.eq.5) then
         qrk1=upv1+dnv1+2d0*ups1+2d0*dns1+2d0*str1+2d0*chm1+2d0*bot1
      end if

c
c     input pdf
c
      xz=x/z
      call struct(xz,scale,upv,dnv,ups,dns,str,chm,bot,glu)
      upv=upv/xz
      dnv=dnv/xz
      ups=ups/xz
      dns=dns/xz
      str=str/xz
      chm=chm/xz
      bot=bot/xz
      glu=glu/xz
c
c     adjust the sum over the quarks depending on nlf
c     

      if(nlf.eq.2)then
         qrk=upv+dnv+2d0*ups+2d0*dns
      else if(nlf.eq.3) then
         qrk=upv+dnv+2d0*ups+2d0*dns+2d0*str
      else if(nlf.eq.4) then
         qrk=upv+dnv+2d0*ups+2d0*dns+2d0*str+2d0*chm
      else if(nlf.eq.5) then
         qrk=upv+dnv+2d0*ups+2d0*dns+2d0*str+2d0*chm+2d0*bot
      end if


      do jmod=1,2
         res(1)=xqtoq(jmod,upv,upv1,z)
         res(2)=xqtoq(jmod,dnv,dnv1,z)
         res(3)=xgtog(jmod,glu,glu1,z)
         res(4)=xqtoq(jmod,ups,ups1,z)
         res(5)=xqtoq(jmod,str,str1,z)
         res(6)=xqtoq(jmod,chm,chm1,z)
         res(7)=xqtoq(jmod,bot,bot1,z)
         res(8)=xqtoq(jmod,dns,dns1,z)
         res(9)=xgtoq(jmod,glu,glu1,z)
         res(10)=xqtog(jmod,qrk,qrk1,z)

         do i=1,10
            res(i)=res(i)*sc_fac
         end do

          if(jmod.eq.1)then
                dqtoq=+3d0/4d0+dlog(1-x)
                dgtog=(11d0/6d0-nlf/3d0/Nc+2d0*dlog(1d0-x))
          else 
                dqtoq=+pi**2/6d0-7d0/4d0+dlog(1d0-x)**2/2d0
                dgtog=(pi**2/3d0-67d0/18d0+5d0*nlf/9d0/Nc
     .               +dlog(1d0-x)**2)
          end if

          res(1)=res(1)+dqtoq*upv1
          res(2)=res(2)+dqtoq*dnv1
          res(3)=res(3)+dgtog*glu1
          res(4)=res(4)+dqtoq*ups1
          res(5)=res(5)+dqtoq*str1
          res(6)=res(6)+dqtoq*chm1
          res(7)=res(7)+dqtoq*bot1
          res(8)=res(8)+dqtoq*dns1
          

          res(1)=res(1)*(1d0-1d0/Nc**2)
          res(2)=res(2)*(1d0-1d0/Nc**2)
          res(4)=res(4)*(1d0-1d0/Nc**2)
          res(5)=res(5)*(1d0-1d0/Nc**2)
          res(6)=res(6)*(1d0-1d0/Nc**2)
          res(7)=res(7)*(1d0-1d0/Nc**2)
          res(8)=res(8)*(1d0-1d0/Nc**2)
          res(9)=res(9)/Nc
          res(10)=res(10)*(1d0-1d0/Nc**2)

          f(jmod,0)=res(10)+res(3)
          f(jmod,1)=res(2)+res(8)+res(9)
          f(jmod,2)=res(1)+res(4)+res(9)
          f(jmod,3)=res(5)+res(9)
          f(jmod,4)=res(6)+res(9)
          f(jmod,5)=res(7)+res(9)
          f(jmod,-1)=res(8)+res(9)
          f(jmod,-2)=res(4)+res(9)
          f(jmod,-3)=res(5)+res(9)
          f(jmod,-4)=res(6)+res(9)
          f(jmod,-5)=res(7)+res(9)

       end do
       return
       end

c
c--------------------------------------------------------------------- 
c qrk to qrk crossing function
      function xqtoq(i,fxz,fx,z)
      implicit real*8(a-h,o-z)
c contributions away from endpoint
      if(z.lt.1d0)then
        p4qtoq=2d0*(1d0+z**2)/(1d0-z)
        peqtoq=-2d0*(1d0-z)
        if(i.eq.1)then
          aa=+0.25d0*((1d0-z)*p4qtoq*fxz/z-4d0*fx)/(1d0-z)
        elseif(i.eq.2)then
          aa=0.25d0*((1d0-z)*p4qtoq*fxz/z-4d0*fx)*dlog(1d0-z)/(1d0-z)
     .       -0.25d0*peqtoq*fxz/z
        endif
      elseif(z.eq.1d0)then
        if(i.eq.1)then
          aa=0d0
        elseif(i.eq.2)then
          aa=0d0
        endif
      endif
      xqtoq=aa
      return
      end
c--------------------------------------------------------------------- 
c qrk to glu crossing function 

      function xqtog(i,fxz,fx,z)
      implicit real*8(a-h,o-z)
      p4qtog=2d0*(1d0+(1d0-z)**2)/z
      peqtog=-2d0*z
      if(z.lt.1d0)then 

        if(i.eq.1)then
          aa=+0.25d0*p4qtog*fxz/z
        elseif(i.eq.2)then
          aa=(-0.25d0*peqtog+0.25d0*p4qtog*dlog(1d0-z))*fxz/z           
        endif
      elseif(z.eq.1d0)then 

        if(i.eq.1)then
          aa=+0.25d0*p4qtog*fxz/z
        elseif(i.eq.2)then
          aa=-0.25d0*peqtog*fxz/z               
        endif
      endif
      xqtog=aa
      return
      end
c--------------------------------------------------------------------- 
c glu to qrk crossing function
      function xgtoq(i,fxz,fx,z)
      implicit real*8(a-h,o-z)
      p4gtoq=2d0*(z**2+(1d0-z)**2)
      pegtoq=-2d0+p4gtoq
      if(z.lt.1d0)then
        if(i.eq.1)then
          aa=+0.25d0*p4gtoq*fxz/z
        elseif(i.eq.2)then
          aa=(-0.25d0*pegtoq+0.25d0*p4gtoq*dlog(1d0-z))*fxz/z           
        endif
      elseif(z.eq.1d0)then
        if(i.eq.1)then
          aa=+0.25d0*p4gtoq*fxz/z
        elseif(i.eq.2)then
          aa=-0.25d0*pegtoq*fxz/z               
        endif
      endif
      xgtoq=aa
      return
      end
c--------------------------------------------------------------------- 
c glu to glu crossing function 

      function xgtog(i,fxz,fx,z)
      implicit real*8(a-h,o-z)
      if(z.lt.1d0)then
        p4gtog=4d0*((1d0-z)/z+z/(1d0-z)+z*(1d0-z))
        pegtog=0d0
        if(i.eq.1)then
          aa=+0.5d0*((1d0-z)*p4gtog*fxz/z-4d0*fx)/(1d0-z)
        elseif(i.eq.2)then
          pegtog=0d0
          aa= 0.5d0*((1d0-z)*p4gtog*fxz/z-4d0*fx)*dlog(1d0-z)/(1d0-z)
     .       -0.5d0*pegtog*fxz/z        
        endif
      elseif(z.eq.1d0)then
        if(i.eq.1)then
          aa=0d0
        elseif(i.eq.2)then
          aa=0d0
        endif
      endif
      xgtog=aa
      return
      end
