      subroutine struct(x,Q,upv,dnv,ups,dns,str,chm,bot,glu)
      implicit none
      real*8 x,Q,f(-6:6)
      real*8 upv,dnv,ups,dns,str,chm,bot,glu
*
      call evolvePDF(x,Q,f)
      upv=f(2)-f(-2)
      dnv=f(1)-f(-1)
      ups=f(-2)
      dns=f(-1)
      str=f(3)
      chm=f(4)
      bot=f(5)
      glu=f(0)
*
      return
      end
