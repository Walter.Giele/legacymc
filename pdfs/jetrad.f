************************************************************************
*                                                                      *
*      p pbar -> 1 or 2 jets    at O(alphas**3)                        *
*                                                                      *
*      RELEASE 2.0, 01/17/97                                           *
*                                                                      *
************************************************************************
      program jetrad
      implicit real*8 (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      character*5  spp1,spp2
      character*32 sstru,sir,sex,son,sjet
      character*128 namePDFset
      common /thcut/ smin,s0
      common /phypar/ w,ipp1,ipp2,qcdl
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /koppel/ nf,nc,b0
      common /proc/ isub(4)
      common /order/ iorder
      common /normal/exclusive
      logical exclusive
*
* input parameters
*
* number of jets and order : jets = 1, 2 or 3
*
      njets=1
      nloop=1
      exclusive = .false.
      namePDFset='/home/giele/LHAPDF/PDFsets/Alekhin_100.LHpdf'
      call InitPDFset(namePDFset)
      call numberPDF(Npdf)
*
* warm up sweeps (itmax1) and main sweeps (itmax2)
* virtual   shots warm up sweep (nshot1) and main sweep (nshot2)
* radiative shots warm up sweep (nshot3) and main sweep (nshot4)
*
*   usually itmax1 = 5 and itmax2 = 10
*   I usually also have nshot4 = 10*nshot3 
*                       nshot3 =    nshot2
*                       nshot2 = 10*nshot1
*   nshot4 = 10**7 is not unreasonable!
*
      itmax1=10
      itmax2=20 
      nshot1=10000
      nshot2=100000
      nshot3=100000
      nshot4=1000000
*
* experimental jet cuts
*
* counts jets with 
*                      etminj  <      ETjet       < etmaxj (in GeV)
*                 and  rapminj < |pseudorapidity| < rapmaxj
*
      etminj=40d0
      etmaxj=900d0
      rapmaxj=0.7d0
      rapminj=0.1d0
*
* recombination scheme
*       jetalg = 1 ; deltaR(i,j) < delrjj
*       jetalg = 2 ; deltaR(i,jet) < delrjj, deltaR(j,jet) < delrjj
*                    and deltaR(i,j) < rsep*delrjj
*       jetalg = 3 ; deltaR(i,jet) < delrjj, deltaR(j,jet) < delrjj
*                    and deltaR(i,j) < rsep*delrjj
*
*       jetalg = 1 is usual Perturbative algorithm
*       jetalg = 2 is usual EKS jet algorithm
*       jetalg = 3 is usual D0  jet algorithm
*
      delrjj=0.7d0     ! size of cone
      rsep=1.3d0       ! rsep variable in EKS jet algorithems
      jetalg=2
*
* theoretical cut
*
      smin=10d0            ! ask if you want to change smin or safety
      safety=1d3           
      s0=smin/safety       
*
*
      ipp1=0
      ipp2=1
*
* renormalization and factorization scales (irenorm and ifact): 
*             1. total invariant mass                     * c    
*             2. summed ET                                * c 
*             3. ET of highest ET jet                     * c 
*             4. 100d0                                    * c 
*
      call GetRenFac(R)
      ifact=3
      cfact=1d0
      irenorm=ifact
      crenorm=cfact*R
*
* subprocesses to take along, 0, 2, or 4 quarks. 1 = yes, 0 = no
*
      isub(1)=1
      isub(2)=1
      isub(3)=1
      isub(4)=1       ! dont change these
*
* physics parameters :
*
* center of mass energy
*
      w=1800d0
*
* number of colours (nc) and number of light quarks (nf)
*
      nc=3
      nf=5
      b0=(33d0-2d0*nf)/12d0/pi
*
* output settings of monte carlo
*
      write(*,*)
      write(*,*) '*************************************************',
     . '*****************' 
      write(*,*)
      write(*,*) ' release 2.0                                    ',
     . '         01/17/97'
      spp1=' '
      spp2=' '
      if (ipp1.eq.0) spp1='p'
      if (ipp1.eq.1) spp1='pbar'
      if (ipp2.eq.0) spp2='p'
      if (ipp2.eq.1) spp2='pbar'
      if (exclusive) then
        sex ='exclusive'
      else
        sex ='inclusive'
      endif
      if ((spp1.eq.' ').or.(spp2.eq.' ')) then
        write(*,*) '*** input error, ipp1, ipp2 =',ipp1,ipp2
	goto 999
      endif
      norder=max0(njets,2)+nloop
      nloopmx=1
      if(njets.ge.3)nloopmx=0
      if (nloop.gt.nloopmx) then
        write(*,*) '*** input error, njets,nloop =',njets,nloop
        goto 999
      endif
      write(*,11) spp1,spp2,njets,sex,norder,w
   11 format(/,1x,a4,a4,' --> ',i2,' jets ',a9,
     .      ' at O(alphas**',i1,') at ',f6.0,' GeV ')
      write(*,12) delrjj,rsep,etminj,etmaxj,rapminj,rapmaxj
   12 format(/,' jet defining cuts : delta Rjj = ',f4.2,', Rsep = '
     .        ,f4.2,/,/,
     .        '    ',f7.2,' GeV <   ETjet  < ',f7.2,' GeV ',/,/,
     .        '       ',f4.2,'     < |etajet| <    ',f4.2)
      sjet=' '
      if(jetalg.eq.1)sjet=' naive clustering'
      if(jetalg.eq.2)sjet=' CDF   clustering'
      if(jetalg.eq.3)sjet=' D0    clustering'
      write(*,13) sjet
 13   format(/,' jet algorithm : ',a16)
      write(*,15) namePDFset
   15 format(/,' structure function : ',a16)
      write(*,16) smin
   16 format(/,' parton resolution cut = ',g12.5,' GeV^2')
      write(*,17) safety,s0 
   17 format(/,' safety cut = smin / ',f8.2,' = ',g12.5,' GeV^2')
*
      sir=' '
      if (irenorm.eq.1) sir='parton centre of mass energy'
      if (irenorm.eq.2) sir='sum ET of obs. jets'
      if (irenorm.eq.3) sir='ET of max ET jet'
      if (irenorm.eq.4) sir='100 GeV'
      if (sir.eq.' ') then
        write(*,*) '*** input error, irenorm =',irenorm
	goto 999
      endif
      write(*,18) crenorm,sir
   18 format(/,' renormalization scale : ',f5.2,' * ',a32)
*
      sir=' '
      if (ifact.eq.1) sir='parton centre of mass energy'
      if (ifact.eq.2) sir='sum ET of obs. jets'
      if (ifact.eq.3) sir='ET of max ET jet'
      if (ifact.eq.4) sir='100 GeV'
      if (sir.eq.' ') then
        write(*,*) '*** input error, ifact =',ifact
	goto 999
      endif
      write(*,19) cfact,sir
   19 format(/,' factorization scale   : ',f5.2,' * ',a32)
*
      if(isub(1).eq.1)son='on '
      if(isub(1).eq.0)son='off'
      write(*,23)son
   23 format(/,'         zero quark processes : ',a3)
      if(isub(2).eq.1)son='on '
      if(isub(2).eq.0)son='off'
      write(*,24)son
   24 format(/,'          two quark processes : ',a3)
      if(isub(3).eq.1)son='on '
      if(isub(3).eq.0)son='off'
      write(*,25)son
   25 format(/,'  unlike four quark processes : ',a3)
      if(isub(4).eq.1)son='on '
      if(isub(4).eq.0)son='off'
      write(*,26)son
   26 format(/,'    like four quark processes : ',a3)
      write(*,*)
      write(*,*) '=================================================',
     . '=================' 
      write(*,*) 

*
* calculate cross section
*
      do istruc=1,Npdf
         call InitPDF(istruc)
         write(*,*) 'PDF set: ',namePDFset
         write(*,*) 'PDF member: ',istruc
         call GetAlfas(alf,Q)
         write(*,*) 'alpha_S(',Q,') = ',alf
         call cross(sig,sd)
*
* output results and distributions
*
         write(*,30) njets,sig,sd
 30     format(/,'  sigma(',i2,' jets) = ',g12.5,'  +/-',g12.5,' nb ',/)
        call bino(4,0d0,0)
*
        write(*,*)
        write(*,*) '*************************************************',
     .             '*****************' 
        write(*,*)
        enddo
  999 continue

      end
*
************************************************************************
*                                                                      *
* write distributions                                                  *
*                                                                      *
************************************************************************
      subroutine bino(istat,wgt,npar)
      implicit double precision (a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /inppar/ crenorm,cfact,njets,nloop,istruc,irenorm,ifact
      common /fraction/ x1,x2,facscale,renscale
      common /jetmom/ pjet(8,10),jp(10)
      common /parmom/ ppar(4,10)
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj,rsep,jetalg
      common /sin/ s(10,10),th(10,10)
*
* pjet(5,j) = ET
* pjet(6,j) = pseudorapidity
*
* iplot = 1: CDF single jet inclusive distribution
* iplot = 2: D0 single jet inclusive distribution
* iplot = 3: CDF two jet dsigma/det/deta1/deta2 distribution
* iplot = 4: D0 two jet dsigma/det/deta1/deta2 distribution
* iplot = 5: CDF sameside/opposite side jet dsigma/det/deta1/deta2 
* iplot = 6: D0 signed jet dsigma/det/deta1/deta2 distribution
*
*
* init histograms
*
      iplot=1
      nhis=10
      if (istat.eq.0) then
      if(iplot.eq.1)then
*
* CDF single jet inclusive distribution
*
         call histoi(1,0d0,500d0,50)
         call histoi(2,0d0,500d0,50)
         call histoi(3,0d0,500d0,50)
         call histoi(4,0d0,500d0,50)
      elseif(iplot.eq.2)then
*
* D0 single jet inclusive distribution
*
         call histoi(1,0d0,500d0,50)
         call histoi(2,0d0,500d0,50)
         call histoi(3,0d0,500d0,50)
         call histoi(4,0d0,500d0,50)
         call histoi(5,0d0,500d0,50)
         call histoi(6,0d0,500d0,50)
         call histoi(7,0d0,500d0,50)
         call histoi(8,0d0,500d0,50)
      elseif(iplot.eq.3)then
*
* CDF two jet dsigma/det/deta1/deta2 distribution
*
         call histoi(1,-4d0,4d0,40)
         call histoi(2,-4d0,4d0,40)
         call histoi(3,-4d0,4d0,40)
         call histoi(4,-4d0,4d0,40)
         call histoi(5,-4d0,4d0,40)
         call histoi(6,-4d0,4d0,40)
      elseif(iplot.eq.4)then
*
* D0 two jet dsigma/det/deta1/deta2 distribution
*
         call histoi(1,-4d0,4d0,40)
         call histoi(2,-4d0,4d0,40)
         call histoi(3,-4d0,4d0,40)
         call histoi(4,-4d0,4d0,40)
      elseif(iplot.eq.5)then
*
* CDF sameside/opposite side jet dsigma/det/deta1/deta2 distribution
*
         call histoi(1,-4d0,4d0,40)
         call histoi(2,-4d0,4d0,40)
         call histoi(3,-4d0,4d0,40)
         call histoi(4,-4d0,4d0,40)
         call histoi(5,-4d0,4d0,40)
         call histoi(6,-4d0,4d0,40)
         call histoi(7,-4d0,4d0,40)
         call histoi(8,-4d0,4d0,40)
      elseif(iplot.eq.6)then
*
* D0 signed jet dsigma/det/deta1/deta2 distribution
*
         call histoi(1,-4d0,4d0,40)
         call histoi(2,-4d0,4d0,40)
      endif
      endif
*
* write event into histogram
*
      if (istat.eq.1) then
      if(iplot.eq.1)then
*
* CDF single jet inclusive distribution
*
        do i=1,npar          
          j=jp(i)
          if(pjet(4,j).gt.0d0)then
            et=pjet(5,j)
            etaj=abs(pjet(6,j))
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0d0).and.(etaj.lt.1d0))
     .           call histoa(1,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.1d0).and.(etaj.lt.2d0))
     .           call histoa(2,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.2d0).and.(etaj.lt.3d0))
     .           call histoa(3,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.3d0).and.(etaj.lt.4d0))
     .           call histoa(4,et,wgt/2d0)
          endif
        enddo
      elseif(iplot.eq.2)then
*
* D0 single jet inclusive distribution
*
        do i=1,npar          
          j=jp(i)
          if(pjet(4,j).gt.0d0)then
            et=pjet(5,j)
            etaj=abs(pjet(6,j))
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0d0).and.(etaj.lt.4d0))
     .           call histoa(1,et,wgt/8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0d0).and.(etaj.lt.0.9d0))
     .           call histoa(2,et,wgt/1.8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.1d0).and.(etaj.lt.2d0))
     .           call histoa(3,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.2d0).and.(etaj.lt.3d0))
     .           call histoa(4,et,wgt/2d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0.4d0).and.(etaj.lt.0.8d0))
     .           call histoa(5,et,wgt/0.8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.0.8d0).and.(etaj.lt.1.2d0))
     .           call histoa(6,et,wgt/0.8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.1.2d0).and.(etaj.lt.1.6d0))
     .           call histoa(7,et,wgt/0.8d0)
            if ((et.gt.etminj).and.(et.lt.etmaxj).and.
     .          (etaj.gt.1.6d0).and.(etaj.lt.2d0))
     .           call histoa(8,et,wgt/0.8d0)
          endif
        enddo
      elseif(iplot.eq.3)then
*
* CDF two jet dsigma/det/deta1/deta2 distribution
*
        j1=jp(1)
        j2=jp(2)
        if(pjet(4,j1).gt.0d0.and.pjet(4,j2).gt.0d0)then
          et1=pjet(5,j1)
          eta1=abs(pjet(6,j1))
          et2=pjet(5,j2)
          eta2=abs(pjet(6,j2))
          if(et1.gt.etminj.and.et1.lt.etmaxj.and.et2.gt.10d0)then
            if(eta1.gt.0.1d0.and.eta1.lt.0.7d0)then
              if(eta2.gt.0.1d0.and.eta2.lt.0.7d0) 
     .          call histoa(1,et1,wgt/1.2d0/1.2d0)
              if(eta2.gt.0.7d0.and.eta2.lt.1.2d0) 
     .          call histoa(2,et1,wgt/1.2d0/1.0d0)
              if(eta2.gt.1.2d0.and.eta2.lt.1.6d0) 
     .          call histoa(3,et1,wgt/1.2d0/0.8d0)
              if(eta2.gt.1.6d0.and.eta2.lt.2.0d0) 
     .          call histoa(4,et1,wgt/1.2d0/0.8d0)
              if(eta2.gt.2.0d0.and.eta2.lt.3.0d0) 
     .          call histoa(5,et1,wgt/1.2d0/2d0)
              if(eta2.gt.3.0d0.and.eta2.lt.4.0d0) 
     .          call histoa(6,et1,wgt/1.2d0/2d0)
             endif
          endif
          if(et2.gt.etminj.and.et2.lt.etmaxj.and.et1.gt.10d0)then
            if(eta2.gt.0.1d0.and.eta2.lt.0.7d0)then
              if(eta1.gt.0.1d0.and.eta1.lt.0.7d0)
     .          call histoa(1,et2,wgt/1.2d0/1.2d0)
              if(eta1.gt.0.7d0.and.eta1.lt.1.2d0)
     .          call histoa(2,et2,wgt/1.2d0/1.0d0)
              if(eta1.gt.1.2d0.and.eta1.lt.1.6d0)
     .          call histoa(3,et2,wgt/1.2d0/0.8d0)
              if(eta1.gt.1.6d0.and.eta1.lt.2.0d0)
     .          call histoa(4,et2,wgt/1.2d0/0.8d0)
              if(eta1.gt.2.0d0.and.eta1.lt.3.0d0)
     .          call histoa(5,et2,wgt/1.2d0/2d0)
              if(eta1.gt.3.0d0.and.eta1.lt.4.0d0)
     .          call histoa(6,et2,wgt/1.2d0/2d0)
            endif 
          endif
        endif
      elseif(iplot.eq.4)then
*
* D0 two jet dsigma/det/deta1/deta2 distribution
*
        j1=jp(1)
        j2=jp(2)
        if(pjet(4,j1).gt.0d0.and.pjet(4,j2).gt.0d0)then
          et1=pjet(5,j1)
          eta1=abs(pjet(6,j1))
          et2=pjet(5,j2)
          eta2=abs(pjet(6,j2))
          if(et1.gt.etminj.and.et1.lt.etmaxj.and.et2.gt.20d0)then
            if(eta1.gt.0d0.and.eta1.lt.1d0)then
              if(eta2.gt.0d0.and.eta2.lt.1d0) 
     .          call histoa(1,et1,wgt/2d0/2d0)
              if(eta2.gt.1d0.and.eta2.lt.2d0) 
     .          call histoa(2,et1,wgt/2d0/2d0)
              if(eta2.gt.2d0.and.eta2.lt.3d0) 
     .          call histoa(3,et1,wgt/2d0/2d0)
              if(eta2.gt.3d0.and.eta2.lt.4d0) 
     .          call histoa(4,et1,wgt/2d0/2d0)
             endif
          endif
          if(et2.gt.etminj.and.et2.lt.etmaxj.and.et1.gt.20d0)then
            if(eta2.gt.0d0.and.eta2.lt.1d0)then
              if(eta1.gt.0d0.and.eta1.lt.1d0)
     .          call histoa(1,et2,wgt/2d0/2d0)
              if(eta1.gt.1d0.and.eta1.lt.2d0)
     .          call histoa(2,et2,wgt/2d0/2d0)
              if(eta1.gt.2d0.and.eta1.lt.3d0)
     .          call histoa(3,et2,wgt/2d0/2d0)
              if(eta1.gt.3d0.and.eta1.lt.4d0)
     .          call histoa(4,et2,wgt/2d0/2d0)
            endif 
          endif
        endif
      elseif(iplot.eq.5)then
*
* CDF sameside/opposite side jet dsigma/det/deta1/deta2 distribution
*
        j1=jp(1)
        j2=jp(2)
        if(pjet(4,j1).gt.0d0.and.pjet(4,j2).gt.0d0)then
          et1=pjet(5,j1)
          eta1=pjet(6,j1)  
          et2=pjet(5,j2)
          eta2=pjet(6,j2)

          pt1=sqrt(pjet(1,j1)**2+pjet(2,j1)**2)
          pt2=sqrt(pjet(1,j2)**2+pjet(2,j2)**2)
          cs=(pjet(1,j1)*pjet(1,j2)+pjet(2,j1)*pjet(2,j2))/pt1/pt2
          phi12=acos(max(cs,-1d0))

          if (phi12.gt.(pi-0.7d0).and.et2.gt.10d0) then
            if(eta1.gt.0d0)ieta1=eta1/0.2d0+1
            if(eta1.lt.0d0)ieta1=eta1/0.2d0-1
            if(eta2.gt.0d0)ieta2=eta2/0.2d0+1
            if(eta2.lt.0d0)ieta2=eta2/0.2d0-1

            if(et1.gt.27d0.and.et1.lt.60d0)then
              if(ieta1.eq.-ieta2)call histoa(1,eta1,wgt)
              if(ieta1.eq.ieta2)call histoa(2,eta1,wgt)
            endif
            if(et1.gt.60d0.and.et1.lt.80d0)then
              if(ieta1.eq.-ieta2)call histoa(3,eta1,wgt)
              if(ieta1.eq.ieta2)call histoa(4,eta1,wgt)
            endif
            if(et1.gt.80d0.and.et1.lt.110d0)then
              if(ieta1.eq.-ieta2)call histoa(5,eta1,wgt)
              if(ieta1.eq.ieta2)call histoa(6,eta1,wgt)
            endif
            if(et1.gt.110d0.and.et1.lt.350d0)then
              if(ieta1.eq.-ieta2)call histoa(7,eta1,wgt)
              if(ieta1.eq.ieta2)call histoa(8,eta1,wgt)
            endif
            if(et2.gt.27d0.and.et2.lt.60d0)then
              if(ieta2.eq.-ieta1)call histoa(1,eta2,wgt)
              if(ieta2.eq.ieta1)call histoa(2,eta2,wgt)
            endif
            if(et2.gt.60d0.and.et2.lt.80d0)then
              if(ieta2.eq.-ieta1)call histoa(3,eta2,wgt)
              if(ieta2.eq.ieta1)call histoa(4,eta2,wgt)
            endif
            if(et2.gt.80d0.and.et2.lt.110d0)then
              if(ieta2.eq.-ieta1)call histoa(5,eta2,wgt)
              if(ieta2.eq.ieta1)call histoa(6,eta2,wgt)
            endif
            if(et2.gt.110d0.and.et2.lt.350d0)then
              if(ieta2.eq.-ieta1)call histoa(7,eta2,wgt)
              if(ieta2.eq.ieta1)call histoa(8,eta2,wgt)
            endif
          endif
        endif
      elseif(iplot.eq.6)then
*
* D0 signed jet dsigma/det/deta1/deta2 distribution
*
        j1=jp(1)
        j2=jp(2)
        if(pjet(4,j1).gt.0d0.and.pjet(4,j2).gt.0d0)then
          et1=pjet(5,j1)
          eta1=pjet(6,j1)
          et2=pjet(5,j2)
          eta2=pjet(6,j2)
          sign=eta1*eta2/abs(eta1)/abs(eta2)
          if(et1.gt.45d0.and.et1.lt.55d0.and.et2.gt.20d0)then
            if(abs(eta1).gt.0d0.and.abs(eta1).lt.0.5d0)then
              call histoa(1,abs(eta2)*sign,wgt)
            endif
          endif
          if(et2.gt.45d0.and.et2.lt.55d0.and.et1.gt.20d0)then
            if(abs(eta2).gt.0d0.and.abs(eta2).lt.0.5d0)then
              call histoa(1,abs(eta1)*sign,wgt)
            endif
          endif
          if(et1.gt.55d0.and.et1.lt.65d0.and.et2.gt.20d0)then
            if(abs(eta1).gt.2d0.and.abs(eta1).lt.2.5d0)then
              call histoa(2,abs(eta2)*sign,wgt)
            endif
          endif
          if(et2.gt.55d0.and.et2.lt.65d0.and.et1.gt.20d0)then
            if(abs(eta2).gt.2d0.and.abs(eta2).lt.2.5d0)then
              call histoa(2,abs(eta1)*sign,wgt)
            endif
          endif
        endif
      endif
      endif
*
* output distributions
*
      if (istat.eq.4) then
      if(iplot.eq.1)then
          write(*,*) ' CDF single jet inclusive  0.0 - 1.0'
          call histow(1)
          write(*,*) ' CDF single jet inclusive  1.0 - 2.0'
          call histow(2)
          write(*,*) ' CDF single jet inclusive  2.0 - 3.0'
          call histow(3)
          write(*,*) ' CDF single jet inclusive  3.0 - 4.0'
          call histow(4)
      elseif(iplot.eq.2)then
          write(*,*) ' D0 single jet inclusive  0.0 - 4.0'
          call histow(1)
          write(*,*) ' D0 single jet inclusive  0.0 - 0.9'
          call histow(2)
          write(*,*) ' D0 single jet inclusive  1.0 - 2.0'
          call histow(3)
          write(*,*) ' D0 single jet inclusive  2.0 - 3.0'
          call histow(4)
          write(*,*) ' D0 single jet inclusive  0.4 - 0.8'
          call histow(5)
          write(*,*) ' D0 single jet inclusive  0.8 - 1.2'
          call histow(6)
          write(*,*) ' D0 single jet inclusive  1.2 - 1.6'
          call histow(7)
          write(*,*) ' D0 single jet inclusive  1.6 - 2.0'
          call histow(8)
      elseif(iplot.eq.3)then
          write(*,*) ' CDF two jet   0.1 < eta2 < 0.7'
          call histow(1)
          write(*,*) ' CDF two jet   0.7 < eta2 < 1.2'
          call histow(2)
          write(*,*) ' CDF two jet   1.2 < eta2 < 1.6'
          call histow(3)
          write(*,*) ' CDF two jet   1.6 < eta2 < 2.0'
          call histow(4)
          write(*,*) ' CDF two jet   2.0 < eta2 < 3.0'
          call histow(5)
          write(*,*) ' CDF two jet   3.0 < eta2 < 4.0'
          call histow(6)
      elseif(iplot.eq.4)then
          write(*,*) ' D0 two jet    0.0 < eta2 < 1.0'
          call histow(1)
          write(*,*) ' D0 two jet    1.0 < eta2 < 2.0'
          call histow(2)
          write(*,*) ' D0 two jet    2.0 < eta2 < 3.0'
          call histow(3)
          write(*,*) ' D0 two jet    3.0 < eta2 < 4.0'
          call histow(4)
      elseif(iplot.eq.5)then
          write(*,*) ' CDF  opp side   27 < et < 60'
          call histow(1)
          write(*,*) ' CDF same side   27 < et < 60'
          call histow(2)
          write(*,*) ' CDF  opp side   60 < et < 80'
          call histow(3)
          write(*,*) ' CDF same side   60 < et < 80'
          call histow(4)
          write(*,*) ' CDF  opp side   80 < et < 110'
          call histow(5)
          write(*,*) ' CDF same side   80 < et < 110'
          call histow(6)
          write(*,*) ' CDF  opp side   110 < et < 350'
          call histow(7)
          write(*,*) ' CDF same side   110 < et < 350'
          call histow(8)
      elseif(iplot.eq.6)then
          write(*,*) ' D0 signed   45 < et < 55, 0.0 < eta < 0.5'
          call histow(1)
          write(*,*) ' D0 signed   55 < et < 65, 2.0 < eta < 2.5'
          call histow(2)
      endif
      endif
*
* event errors manipulation request, pipe through
*
      if ((istat.eq.2).or.(istat.eq.3)) then
         do i=1,nhis
            call histoe(istat,i)
         enddo
      endif
*
      return
      end
*
*
************************************************************************
*
      subroutine distrib(wtdis,jets)
      implicit real*8(a-h,o-z)
      parameter(pi=3.141592653589793238d0)
      common /jetmom/ pjet(8,10),jp(10)
      data init/0/
*
* Events are generated uniformly relative to the function
* programmed in distrib. If this function is choosen unity
* events will be generated according to cross section
* density.
* Current function set to one-jet inclusinve Et distribution
* at 1800 GeV. If changing beam energies adjust function. If
* not clear what to do change it to unity (comment out the
* last statement wtdis=1). This guarantees correct result
* albeit poor statistics at high Et.
*
      if(init.eq.0)then
        write(*,*)' ***** WARNING ******'
        write(*,*)' Subroutine DISTRIB optimised for
     . one-jet inclusive Et distribution at 1800 GeV'
        write(*,*)' (Change subroutine DISTRIB for other
     . beam energies and/or other observables)'
        init=1
      endif      
      sa=4.1d0
      sb=0.019d0
      pt=0d0
      ptmax=0d0
      do i=1,jets
         j=jp(i)
         if (pjet(4,j).gt.0d0) then
            pti=pjet(5,j)
            if(pti.gt.ptmax)ptmax=pti
         endif
      enddo
      wtdis=1d0/ptmax**sa*exp(-ptmax*sb)
*
*      wtdis=1d0
*
      return
      end
*
